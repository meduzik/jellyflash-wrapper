# -*- coding: utf-8 -*-
#test on python 3.4 ,python of lower version  has different module organization.
import http.server
from http.server import HTTPServer, BaseHTTPRequestHandler
import socketserver
import os

PORT = 9009

Handler = http.server.SimpleHTTPRequestHandler

Handler.extensions_map={
	'.manifest': 'text/cache-manifest',
	'.html': 'text/html',
	'.png': 'image/png',
	'.jpg': 'image/jpg',
	'.svg':	'image/svg+xml',
	'.css':	'text/css',
	'.wasm': 'application/wasm',
	'.js':	'application/x-javascript',
	'': 'application/octet-stream', # Default
    }

#os.chdir(".embuild-debug-unity/app/bin")
os.chdir(".embuild-debug/em_test/bin")
httpd = socketserver.TCPServer(("", PORT), Handler)

print("serving at port", PORT)

httpd.serve_forever()