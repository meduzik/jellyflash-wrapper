(function (exports) {
	var canvas = Module.canvas;
	var canvas_wrapper = canvas.parentElement;

	var initialized = false;

	var RESIZE_EVENT_TIMEOUT = 50;
	var RESIZE_IDLE_CHECK_TIMEOUT = 1000;

	var resize_timer_id = 0;
	var canvas_width = NaN;
	var canvas_height = NaN;
	var canvas_css_width = NaN;
	var canvas_css_height = NaN;
	var canvas_pixel_ratio = NaN;

	canvas.position = "absolute";
	canvas.left = "0";
	canvas.top = "0";
	canvas.width = "100%";
	canvas.height = "100%";
	canvas.overflow = "hidden";

	canvas.width = canvas_wrapper.clientWidth;
	canvas.height = canvas_wrapper.clientHeight;

	start_resize_watcher();

	function start_resize_watcher() {
		window.addEventListener('resize', () => {
			restart_resize_timer(RESIZE_EVENT_TIMEOUT);
		});
		check_resize();
	}

	function check_resize() {
		resize_timer_id = 0;

		do_resize();

		restart_resize_timer(RESIZE_IDLE_CHECK_TIMEOUT);
	}

	function restart_resize_timer(delay) {
		if (resize_timer_id) {
			clearTimeout(resize_timer_id);
		}
		resize_timer_id = setTimeout(check_resize, delay);
	}

	function get_pixel_ratio() {
		return window.devicePixelRatio || 1;
	}

	function do_resize() {
		var pixel_ratio = get_pixel_ratio();
		var wrapper_rect = canvas_wrapper.getBoundingClientRect();
		var width = wrapper_rect.width;
		var height = wrapper_rect.height;
		
		var width_px = pixel_ratio * width;
		var height_px = pixel_ratio * height;
		
		var canvas_width = Math.ceil(width_px);
		var canvas_height = Math.ceil(height_px);
		var canvas_css_width = canvas_width / pixel_ratio;
		var canvas_css_height = canvas_height / pixel_ratio;

		apply_resize(canvas_width, canvas_height, canvas_css_width, canvas_css_height, pixel_ratio);
	}

	function apply_resize(
		new_canvas_width,
		new_canvas_height,
		new_canvas_css_width,
		new_canvas_css_height,
		new_pixel_ratio
	) {
		if(
			canvas_width !== new_canvas_width
			||
			canvas_height !== new_canvas_height
			||
			canvas_css_width !== new_canvas_css_width
			||
			canvas_css_height !== new_canvas_css_height
			||
			canvas_pixel_ratio !== new_pixel_ratio
		) {
			canvas.width = new_canvas_width;
			canvas.height = new_canvas_height;
			canvas.style.width = new_canvas_css_width + "px";
			canvas.style.height = new_canvas_css_height + "px";
			canvas_width = new_canvas_width;
			canvas_height = new_canvas_height;
			canvas_css_width = new_canvas_css_width;
			canvas_css_height = new_canvas_css_height;
			canvas_pixel_ratio = new_pixel_ratio;

			if (Module.onCanvasResize) {
				Module.onCanvasResize(canvas_width, canvas_height, canvas_pixel_ratio); 
			}

			if (initialized) {
				_canvas_cb_on_resize(canvas_width, canvas_height, canvas_pixel_ratio);
			}
		}
	}

	exports._canvas_initialize = function () {
		var contextAttributes = {
			antialias: false,
			depth: true,
			stencil: true,
			alpha: false
		};
		Module.ctx = Browser.createContext(canvas, true, true, contextAttributes);
		if (!Module.ctx) {
			if (Module.onContextFailed) {
				Module.onContextFailed();
			}
			return 0;
		}
		initialized = true;

		function translate_pos(event) {
			var left = 0;
			var top = 0;
			var element = canvas;
			while (element) {
				left += element.offsetLeft;
				top += element.offsetTop;
				element = element.offsetParent;
			}
			return {
				x: (event.pageX - left) * canvas_pixel_ratio,
				y: (event.pageY - top) * canvas_pixel_ratio
			};
		}

		(function () {
			var primary_touch_id = null;
			var touch_pos;

			function cancel_primary_touch() {
				if (primary_touch_id !== null) {
					primary_touch_id = null;
					_canvas_cb_mouse_up(touch_pos.x, touch_pos.y, 0);
					_canvas_cb_mouse_move(-1000, -1000);
				}
			}

			function begin_primary_touch(touch) {
				if (primary_touch_id !== null) {
					return;
				}
				primary_touch_id = touch.identifier;
				touch_pos = translate_pos(touch);
				_canvas_cb_mouse_down(touch_pos.x, touch_pos.y, 0);
			}

			function move_primary_touch(touch) {
				touch_pos = translate_pos(touch);
				_canvas_cb_mouse_move(touch_pos.x, touch_pos.y);
			}

			function end_primary_touch(touch) {
				if (primary_touch_id === touch.identifier) {
					touch_pos = translate_pos(touch);
					_canvas_cb_mouse_up(touch_pos.x, touch_pos.y, 0);
					primary_touch_id = null;
				}
			}

			function update_touch(touches) {
				var touch_found = false;
				for (var i = 0; i < touches.length; i++) {
					if (touches[i].identifier === primary_touch_id) {
						touch_found = true;
						break;
					}
				}
				if (!touch_found) {
					cancel_primary_touch();
				}
			}

			function on_touch_start(evt) {
				update_touch(evt.touches);
				for (var i = 0; i < evt.changedTouches.length; i++) {
					if (primary_touch_id === null) {
						begin_primary_touch(evt.changedTouches[i]);
						break;
					}
				}
			}

			function on_touch_end(evt) {
				update_touch(evt.touches);
				for (var i = 0; i < evt.changedTouches.length; i++) {
					if (primary_touch_id === evt.changedTouches[i].identifier) {
						end_primary_touch(evt.changedTouches[i]);
						break;
					}
				}
				evt.preventDefault();
				return false;
			}

			function on_touch_cancel(evt) {
				update_touch(evt.touches);
				for (var i = 0; i < evt.changedTouches.length; i++) {
					if (primary_touch_id === evt.changedTouches[i].identifier) {
						cancel_primary_touch();
						break;
					}
				}
				evt.preventDefault();
				return false;
			}

			function on_touch_move(evt) {
				update_touch(evt.touches);
				for (var i = 0; i < evt.changedTouches.length; i++) {
					if (primary_touch_id === evt.changedTouches[i].identifier) {
						move_primary_touch(evt.changedTouches[i]);
						break;
					}
				}
				evt.preventDefault();
				return false;
			}

			canvas.addEventListener("touchstart", on_touch_start, false);
			canvas.addEventListener("touchend", on_touch_end, false);
			canvas.addEventListener("touchcancel", on_touch_cancel, false);
			canvas.addEventListener("touchleave", on_touch_cancel, false);
			canvas.addEventListener("touchmove", on_touch_move, false);
		})();

		(function () {
			var touching = false;

			function on_mouse_down(evt) {
				listen_root();
				var pos = translate_pos(evt);
				_canvas_cb_mouse_down(pos.x, pos.y, evt.button);
				evt.preventDefault();
				return false;
			}

			function on_mouse_up(evt) {
				unlisten_root();
				var pos = translate_pos(evt);
				_canvas_cb_mouse_up(pos.x, pos.y, evt.button);
				evt.preventDefault();
			}

			function on_mouse_move(evt) {
				var pos = translate_pos(evt);
				_canvas_cb_mouse_move(pos.x, pos.y);
				evt.preventDefault();
			}

			function on_mouse_move_passive(evt) {
				if (touching) return;
				var pos = translate_pos(evt);
				_canvas_cb_mouse_move(pos.x, pos.y);
				evt.preventDefault();
			}

			function listen_root() {
				if (touching) return;
				touching = true;
				window.addEventListener('mouseup', on_mouse_up);
				window.addEventListener('mousemove', on_mouse_move);
			}

			function unlisten_root() {
				if (!touching) return;
				touching = false;
				window.removeEventListener('mouseup', on_mouse_up);
				window.removeEventListener('mousemove', on_mouse_move);
			}

			function get_mouse_wheel_delta(event) {
				var delta = 0;
				switch (event.type) {
					case 'DOMMouseScroll':
						// 3 lines make up a step
						delta = event.detail / 3;
						break;
					case 'mousewheel':
						// 120 units make up a step
						delta = event.wheelDelta / 120;
						break;
					case 'wheel':
						delta = event.deltaY
						switch (event.deltaMode) {
							case 0:
								// DOM_DELTA_PIXEL: 100 pixels make up a step
								delta /= 100;
								break;
							case 1:
								// DOM_DELTA_LINE: 3 lines make up a step
								delta /= 3;
								break;
							case 2:
								// DOM_DELTA_PAGE: A page makes up 80 steps
								delta *= 80;
								break;
							default:
								throw 'unrecognized mouse wheel delta mode: ' + event.deltaMode;
						}
						break;
					default:
						throw 'unrecognized mouse wheel event: ' + event.type;
				}
				return delta;
			}

			function on_mouse_wheel(evt) {
				var delta = -get_mouse_wheel_delta(evt);
				delta = (delta === 0) ? 0 : (delta > 0 ? Math.max(delta, 1) : Math.min(delta, -1));
				var pos = translate_pos(evt);
				_canvas_cb_mouse_wheel(pos.x, pos.y, delta);
				evt.preventDefault();
				return false;
			}

			canvas.addEventListener('mousedown', on_mouse_down);
			canvas.addEventListener('mousemove', on_mouse_move_passive);
			canvas.addEventListener("wheel", on_mouse_wheel);
		})();

		

		return 1;
	};

	exports._canvas_query_dimensions = function (pwidth, pheight, pscale) {
		setValue(pwidth, canvas_width, "i32");
		setValue(pheight, canvas_height, "i32");
		setValue(pscale, canvas_pixel_ratio, "double");
	};
})(this);
