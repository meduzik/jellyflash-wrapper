(function (exports) {
	var callbacks = {};

	exports._externalinterface_call = function (request_ptr, request_len, out_response_len) {
		var response;
		try {
			var request_string = UTF8ToString(request_ptr, request_len);
			try {
				var request = JSON.parse(request_string);
			} catch (e) {
				throw new Error("failed to parse request string: " + request_string);
			}
			var func = callbacks[request.func];
			if (!func) {
				throw new Error("no such function '" + request.func + "'");
			}
			var result = func.apply(null, request.args);
			response = {
				res: 'ok',
				val: result
			};
		} catch (err) {
			response = {
				res: 'err',
				exc: err + ''
			};
			console.error("external call to javascript " + JSON.stringify(request_string) + " failed with reason: " + err);
		}
		var response_string = JSON.stringify(response);
		var response_bytes = lengthBytesUTF8(response_string);
		var response_ptr = _malloc(response_bytes + 1);
		setValue(out_response_len, response_bytes, 'i32');
		stringToUTF8(response_string, response_ptr, response_bytes + 1);
		return response_ptr;
	};

	window.jfl_call = function (func, ...args) {
		var request_string = JSON.stringify({
			func: func,
			args: args
		});
		var request_bytes = lengthBytesUTF8(request_string);
		var request_ptr = _malloc(request_bytes + 1);
		var response_len_ptr = _malloc(4);
		stringToUTF8(request_string, request_ptr, request_bytes + 1);
		try {
			var response_ptr = _externalinterface_cb_call(request_ptr, request_bytes, response_len_ptr);
			var response_len = getValue(response_len_ptr, 'i32');
			var response_string = UTF8ToString(response_ptr, response_len);
			_externalinterface_cb_dispose_response(response_ptr, response_len);
			try {
				var response = JSON.parse(response_string);
			} catch (err) {
				throw new Error("failed to parse response string '" + JSON.stringify(response_string) + "'");
			}
			if (response.res === 'ok') {
				return response.val;
			} else if (response.res === 'err') {
				throw response.exc;
			} else {
				throw new Error("bad response from external call");
			}
		} catch (err) {
			console.error("external call " + JSON.stringify(request_string) + " to wasm failed with reason: " + err);
			throw err;
		} finally {
			_free(response_len_ptr);
			_free(request_ptr);
		}
	};

	window.jfl_register = function (name, func) {
		callbacks[name] = func;
	};

	exports._externalinterface_dispose_response = function (response_ptr, response_len) {
		_free(response_ptr);
	};
})(this);