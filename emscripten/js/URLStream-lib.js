mergeInto(LibraryManager.library, {
	urlstream_create: function (ud) { },
	urlstream_set_url: function (stream, url) { },
	urlstream_set_method: function (stream, method) { },
	urlstream_set_data: function (stream, data) { },
	urlstream_set_buffer: function (stream, buffer, len) { },
	urlstream_set_header: function (stream, name, value) { },
	urlstream_load: function (stream) { },
	urlstream_destroy: function (stream) { }
});
