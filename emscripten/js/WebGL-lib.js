mergeInto(LibraryManager.library, {
	webgl_load_image: function (cstr_url) { },
	webgl_dispose_image: function (img) { },
	webgl_tex2d_image: function (target, level, x, y, format, type, image) { },
	webgl_image_get_width: function (img) { },
	webgl_image_get_height: function (img) { }
});
