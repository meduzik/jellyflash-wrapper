(function (exports) {
	var streams = [];
	var free_ids = [];
	var next_id = 0;

	function URLStream(ud, idx) {
		this.idx = idx;
		this.ud = ud;
		this.request = new XMLHttpRequest();
		this.url = null;
		this.request.responseType = 'arraybuffer';
		this.request.withCredentials = false;
		this.headers = [];
		this.method = "GET";
		this.data = null;
		this.buffer_ptr = 0;
		this.buffer_len = 0;
	}

	exports._urlstream_set_buffer = function (idx, ptr, len) {
		var stream = streams[idx];
		if (!stream) {
			throw new Error("invalid stream index");
		}
		stream.buffer_ptr = ptr;
		stream.buffer_len = len;
	};

	exports._urlstream_set_header = function (idx, name, value) {
		var stream = streams[idx];
		if (!stream) {
			throw new Error("invalid stream index");
		}
		stream.headers.push(UTF8ToString(name), UTF8ToString(value));
	};

	exports._urlstream_set_url = function (idx, url) {
		var stream = streams[idx];
		if (!stream) {
			throw new Error("invalid stream index");
		}
		stream.url = UTF8ToString(url);
	};

	exports._urlstream_set_data = function (idx, ptr, len) {
		var stream = streams[idx];
		if (!stream) {
			throw new Error("invalid stream index");
		}
		stream.data = new Blob([new Uint8Array(HEAPU8.subarray(ptr, ptr + len))]);
	};

	exports._urlstream_destroy = function (idx) {
		var stream = streams[idx];
		if (!stream) {
			throw new Error("invalid stream index");
		}
		stream.ud = 0;
		streams[idx] = null;
		free_ids.push(idx);
	};

	exports._urlstream_load = function (idx) {
		var stream = streams[idx];
		if (!stream) {
			throw new Error("invalid stream index");
		}

		function report_fail(code, message) {
			if (stream.ud) {
				var message_bytes = lengthBytesUTF8(message);
				var ptr = _malloc(message_bytes + 1);
				stringToUTF8(message, ptr, message_bytes + 1);
				_urlstream_on_error(stream.idx, stream.ud, code, ptr);
				_free(ptr);
			}
		}

		function on_loadstart(event) {
			if (stream.ud) {
				_urlstream_on_open(stream.idx, stream.ud);
			}
		}

		function on_complete(event) {
			if (stream.ud) {
				var request = stream.request;
				var response = request.response;
				if (request.readyState === 4 && request.status === 200 && response) {
					var byteArray = new Uint8Array(response);
					var size = byteArray.length;
					var buffer_ptr = stream.buffer_ptr;
					var buffer_len = stream.buffer_len;

					for (var offset = 0; offset < size; offset += buffer_len) {
						var to_transfer = Math.min(size - offset, buffer_len);
						HEAPU8.subarray(buffer_ptr, buffer_ptr + to_transfer).set(byteArray.subarray(offset, offset + to_transfer));
						_urlstream_on_data(stream.idx, stream.ud, buffer_ptr, to_transfer);
					}
					_urlstream_on_complete(stream.idx, stream.ud);
				} else {
					console.error("Loading " + stream.url + " failed:", request.readyState, request.status);
					report_fail(request.status, "network request failed");
				}
			}
		}

		function on_error(event) {
			if (event.error) {
				console.error("Loading " + stream.url + " failed:", event.error.code, event.error.name + ": " + event.error.message);
				report_fail(event.error.code, event.error.name + ": " + event.error.message);
			} else {
				console.error("Loading " + stream.url + " failed:", stream.request.statusText || "request failed", event);
				report_fail(1000, "stream error");
			}
		}

		function on_abort(event) {
			console.error("Loading " + stream.url + " failed:", "aborted");
			report_fail(300, "aborted");
		}

		function on_progress(event) {
			if (stream.ud) {
				if (event.lengthComputable) {
					_urlstream_on_progress(stream.idx, stream.ud, event.loaded, event.total);
				} else if ( 'loaded' in event ) {
					_urlstream_on_progress(stream.idx, stream.ud, event.loaded, 0);
				}
			}
		}

		stream.request.addEventListener("loadstart", on_loadstart);
		stream.request.addEventListener("progress", on_progress);
		stream.request.addEventListener("error", on_error);
		stream.request.addEventListener("abort", on_abort);
		stream.request.addEventListener("load", on_complete);

		stream.request.open(stream.method, stream.url);
		for (var i = 0; i < stream.headers.length; i += 2) {
			stream.request.setRequestHeader(stream.headers[i], stream.headers[i + 1]);
		}
		stream.request.send(stream.data);
	};

	exports._urlstream_set_method = function (idx, method) {
		var stream = streams[idx];
		if (!stream) {
			throw new Error("invalid stream index");
		}
		switch (method) {
		case 1: {
			stream.method = "GET";
		} break;
		case 2: {
			stream.method = "POST";
		} break;
		default: {
			throw new Error("invalid URLStream method id");
		} break;
		}
	};

	exports._urlstream_create = function (ud) {
		var idx = 0;
		if (free_ids.length === 0) {
			idx = next_id;
			next_id++;
		} else {
			idx = free_ids.pop();
		}
		var stream = new URLStream(ud, idx);
		streams[idx] = stream;
		return idx;
	};
})(this);
