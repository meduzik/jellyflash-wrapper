(function (exports) {
	exports._webutils_open_url = function (ptr, len) {
		var url = UTF8ToString(ptr, len);
		window.open(url, '_blank');
		/*
		const element = document.createElement('a');
		element.setAttribute("style", "display: none;");
		element.setAttribute("target", "_blank");
		element.setAttribute("href", url);
		document.body.appendChild(element);
		element.click();
		document.body.removeChild(element);
		*/
	};

	exports._webutils_generate_bytes = function (ptr, len) {
		var i;
		try {
			if (window.crypto && window.crypto.getRandomValues) {
				var array = new Uint8Array(len);
				window.crypto.getRandomValues(array);
				for (i = 0; i < len; i++) {
					setValue(ptr + i, array[i], 'i8');
				}
			}
		} catch (err) {
			for (i = 0; i < len; i++) {
				setValue(ptr, (Math.random() * 256) & 0xff, 'i8');
			}
		}
	};

	exports._webutils_clipboard_copy = function (ptr, len) {
		var string = UTF8ToString(ptr, len);
		const element = document.createElement('textarea');
		element.value = string;
		document.body.appendChild(element);
		element.select();
		var success = false;
		try {
			success = document.execCommand('copy');
		} catch (err) {
			console.log("Copying to clipboard failed with: " + err);
		}
		document.body.removeChild(element);
		return success ? 1 : 0;
	};

	exports._webutils_has_mouse = function () {
		try {
			return !!window.matchMedia('(pointer:fine)').matches;
		} catch (err) {
			return true;
		}
	};

	window.jfl_uncaught_error = function (err) {
		var message = err + "";
		var message_bytes = lengthBytesUTF8(message);
		var ptr = _malloc(message_bytes + 1);
		stringToUTF8(message, ptr, message_bytes + 1);
		_webutils_on_crash(ptr);
		_free(ptr);
	};
})(this);
