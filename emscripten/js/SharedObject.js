(function (exports) {
	function base64decode(base64) {
		var binstr = atob(base64);
		var buf = new Uint8Array(binstr.length);
		Array.prototype.forEach.call(binstr, function (ch, i) {
			buf[i] = ch.charCodeAt(0);
		});
		return buf;
	}

	function base64encode(bytes) {
		var base64 = '';
		var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

		var byteLength = bytes.byteLength;
		var byteRemainder = byteLength % 3;
		var mainLength = byteLength - byteRemainder;

		var a, b, c, d;
		var chunk;

		// Main loop deals with bytes in chunks of 3
		for (var i = 0; i < mainLength; i = i + 3) {
			// Combine the three bytes into a single integer
			chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

			// Use bitmasks to extract 6-bit segments from the triplet
			a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
			b = (chunk & 258048) >> 12; // 258048   = (2^6 - 1) << 12
			c = (chunk & 4032) >> 6; // 4032     = (2^6 - 1) << 6
			d = chunk & 63;               // 63       = 2^6 - 1

			// Convert the raw binary segments to the appropriate ASCII encoding
			base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
		}

		// Deal with the remaining bytes and padding
		if (byteRemainder === 1) {
			chunk = bytes[mainLength];

			a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

			// Set the 4 least significant bits to zero
			b = (chunk & 3) << 4 // 3   = 2^2 - 1

			base64 += encodings[a] + encodings[b] + '==';
		} else if (byteRemainder === 2) {
			chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

			a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
			b = (chunk & 1008) >> 4; // 1008  = (2^6 - 1) << 4

			// Set the 2 least significant bits to zero
			c = (chunk & 15) << 2; // 15    = 2^4 - 1

			base64 += encodings[a] + encodings[b] + encodings[c] + '=';
		}

		return base64;
	};

	exports._sharedobject_load = function (path, out_ptr) {
		var value = window['pecpoc.com/storage'].getItem(UTF8ToString(path));
		if (value && value.length > 0) {
			var decoded = base64decode(value);
			var ptr = allocate(decoded, ALLOC_NORMAL);
			setValue(out_ptr, decoded.length, "i32");
			return ptr;
		} else {
			setValue(out_ptr, 0, "i32");
			return 0;
		}
	};

	exports._sharedobject_store = function (path, data, len) {
		var value = HEAPU8.subarray(data, data + len);
		window['pecpoc.com/storage'].setItem(UTF8ToString(path), base64encode(value));
	};

	exports._sharedobject_clear = function (path) {
		window['pecpoc.com/storage'].removeItem(UTF8ToString(path));
	};

	exports._sharedobject_load_free = function (ptr) {
		_free(ptr);
	};
})(this);