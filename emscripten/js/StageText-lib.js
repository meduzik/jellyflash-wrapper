mergeInto(LibraryManager.library, {
	stagetext_create: function (ud, multiline) { },
	stagetext_destroy: function (idx) { },
	stagetext_set_visible: function (idx, value) { },
	stagetext_viewport: function (idx, x, y, w, h) { },

	stagetext_assign_focus: function (idx) { },

	stagetext_set_editable: function (idx, value) { },
	stagetext_set_text: function (idx, ptr) { },
	stagetext_get_text: function (idx) { },
	stagetext_get_text_free: function (ptr) { },
	stagetext_set_font_family: function (idx, ptr) { },
	stagetext_set_font_size: function (idx, size) { },
	stagetext_set_font_color: function (idx, color) { },
	stagetext_set_font_bold: function (idx, value) { },
	stagetext_set_font_italic: function (idx, value) { },
	stagetext_set_max_chars: function (idx, chars) { },
	stagetext_set_multiline: function (idx, value) { },

	stagetext_set_text_align: function (idx, align) { }
});
