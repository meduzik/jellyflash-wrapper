(function (exports) {
	var stage_texts = [];
	var free_ids = [];
	var next_id = 0;

	var canvas = Module.canvas;
	var stagetext_root = document.createElement("div");
	canvas.parentElement.insertBefore(stagetext_root, canvas);
	stagetext_root.setAttribute('class', 'stage-text-container');
	stagetext_root.setAttribute('style', 'z-index:1; position:absolute; left:0; top:0;');

	var style = document.createElement('style');
	style.type = 'text/css';
	style.innerHTML = '.stage-text { position: absolute; border-style: none; background-color: transparent; resize: none; }';
	document.getElementsByTagName('head')[0].appendChild(style);

	function StageText(ud, idx, multiline) {
		this.idx = idx;
		this.ud = ud;
		var element = document.createElement(multiline ? "textarea" : "input");
		element.setAttribute('class', 'stage-text');
		element.setAttribute('type', 'text');

		var self = this;

		if (!multiline) {
			element.addEventListener('keydown', function (event) {
				if (self.ud) {
					_stagetext_on_keydown(self.ud, event.keyCode);
				}
			});
		}
		element.addEventListener('blur', function (event) {
			if (self.ud) {
				_stagetext_on_focusout(self.ud);
			}
		});
		element.addEventListener('focus', function (event) {
			if (self.ud) {
				_stagetext_on_focusin(self.ud);
			}
		});

		this.attached = false;
		this.viewport = null;
		this.multiline = multiline;
		this.element = element;
	}

	function get(idx) {
		var stage_text = stage_texts[idx];
		if (!stage_text) {
			throw new Error("invalid stage text index");
		}
		return stage_text;
	}

	function set_text(stage_text, value) {
		if (stage_text.multiline) {
			stage_text.element.value = value;
		} else {
			stage_text.element.setAttribute('value', value);
		}
	}

	function get_text(stage_text) {
		return stage_text.element.value;
	}

	function update_viewport(stage_text) {
		var viewport = stage_text.viewport;
		var style = stage_text.element.style;
		style['left'] = viewport[0] + 'px';
		style['top'] = viewport[1] + 'px';
		style['width'] = viewport[2] + 'px';
		style['height'] = viewport[3] + 'px';
	}

	exports._stagetext_viewport = function (idx, x, y, w, h) {
		var stage_text = get(idx);
		stage_text.viewport = [x, y, w, h];
		update_viewport(stage_text);
	};

	exports._stagetext_assign_focus = function (idx) {
		var stage_text = get(idx);
		stage_text.element.focus();
	};

	exports._stagetext_set_editable = function (idx, value) {
		var stage_text = get(idx);
		stage_text.readonly = value;
		if (value) {
			stage_text.element.removeAttribute('readonly');
		} else {
			stage_text.element.setAttribute('readonly', 'readonly');
		}
	};

	exports._stagetext_set_text = function (idx, ptr) {
		var stage_text = get(idx);
		var text = UTF8ToString(ptr);
		set_text(stage_text, text);
	};

	exports._stagetext_get_text = function (idx) {
		var stage_text = get(idx);
		var text = get_text(stage_text);
		var text_bytes = lengthBytesUTF8(text);
		var ptr = _malloc(text_bytes + 1);
		stringToUTF8(text, ptr, text_bytes + 1);
		return ptr;
	};

	exports._stagetext_get_text_free = function (ptr) {
		_free(ptr);
	};

	exports._stagetext_set_font_family = function (idx, ptr) {
		var stage_text = get(idx);
		var font = UTF8ToString(ptr);
		stage_text.element.style['font-family'] = font;
	};

	exports._stagetext_set_font_size = function (idx, size) {
		var stage_text = get(idx);
		stage_text.element.style['font-size'] = size + "px";
	};

	exports._stagetext_set_font_color = function (idx, color) {
		var stage_text = get(idx);
		stage_text.element.style['color'] = '#' + color.toString(16);
	};

	exports._stagetext_set_font_bold = function (idx, value) {
		var stage_text = get(idx);
		stage_text.element.style['font-weight'] = (value ? "bold" : "normal");
	};

	exports._stagetext_set_font_italic = function (idx, value) {
		var stage_text = get(idx);
		stage_text.element.style['font-style'] = (value ? "italic" : "normal");
	};

	exports._stagetext_set_max_chars = function (idx, chars) {
		var stage_text = get(idx);
		stage_text.maxlength = chars;
		stage_text.element.setAttribute('maxlength', chars);
	};

	exports._stagetext_set_text_align = function (idx, align) {
		var stage_text = get(idx);
		switch (align) {
			case 0: {
				stage_text.element.style['text-align'] = "left";
			} break;
			case 1: {
				stage_text.element.style['text-align'] = "center";
			} break;
			case 2: {
				stage_text.element.style['text-align'] = "right";
			} break;
		}
	};

	exports._stagetext_set_visible = function (idx, value) {
		var stage_text = get(idx);
		var element;
		if (stage_text.attached !== value) {
			stage_text.attached = value;
			if (value) {
				element = stage_text.element;
				stagetext_root.appendChild(element);
				element.focus();
				setTimeout(function () { element.selectionStart = element.selectionEnd = 100000; }, 0);
			} else {
				element = stage_text.element;
				setTimeout(function () {
					if (element && element.parentNode === stagetext_root) {
						stagetext_root.removeChild(element);
					}
				}, 0);
			}
		}
	};

	exports._stagetext_create = function (ud, multiline) {
		var idx = 0;
		if (free_ids.length === 0) {
			idx = next_id;
			next_id++;
		} else {
			idx = free_ids.pop();
		}
		var stage_text = new StageText(ud, idx, multiline);
		stage_texts[idx] = stage_text;
		return idx;
	};

	exports._stagetext_destroy = function (idx) {
		var stage_text = get(idx);
		if (stage_text.attached) {
			var element = stage_text.element;
			setTimeout(function () {
				if (element && element.parentNode === stagetext_root) {
					stagetext_root.removeChild(element);
				}
			}, 0);
		}
		stage_text.element = null;
		stage_text.ud = 0;
		stage_texts[idx] = null;
		free_ids.push(idx);
	};
})(this);

