mergeInto(LibraryManager.library, {
	webaudio_set_volume: function (volume) { },

	webaudio_sound_load: function (ud, ptr, len) { },
	webaudio_sound_play: function (sound_id, ud, offset, volume, pan) { },
	webaudio_sound_destroy: function (sound_id) { },

	webaudio_playback_update_transform: function (sound_id, volume, pan) { },
	webaudio_playback_get_position: function (sound_id) { },
	webaudio_playback_destroy: function (sound_id) { },

	webaudio_music_set_volume: function (volume) { },
	webaudio_music_stop: function () { },
	webaudio_music_play: function (track_url, callback_id) { }
});
