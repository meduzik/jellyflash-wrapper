(function (exports) {
	var webgl = Module.webgl;

	var images = [];
	var images_free = [];

	var active = [];
	var queue = [];
	var max_active = 16;
	var max_attempts = 4;

	function queue_image_load(image) {
		if (active.length < max_active) {
			active.push(image);
			image.load();
		} else {
			queue.push(image);
		}
	}

	function unqueue_image_load(image) {
		var queueIndex = queue.indexOf(image);
		if (queueIndex >= 0) {
			queue.splice(queueIndex, 1);
		}

		var activeIndex = active.indexOf(image);
		if (activeIndex >= 0) {
			active.splice(activeIndex, 1);
			if (queue.length > 0) {
				queue_image_load(queue.shift());
			}
		}
	}

	exports._webgl_load_image = function (cstr_url) {
		var image_idx;
		if (images_free.length > 0) {
			image_idx = images_free.pop();
		} else {
			image_idx = images.length;
		}
		var url = UTF8ToString(cstr_url);
		var image = {
			idx: image_idx,
			url: url,
			attempts: 0,
			loading: false,
			disposed: false
		};
		images[image_idx] = image;

		image.dispose = function () {
			if (image.loading) {
				image.loading = false;
				unqueue_image_load(image);
			}
			if (image.imageElement) {
				image.imageElement.src = '';
				image.imageElement = null;
			}
		};

		image.onload = function () {
			if (image.disposed) {
				return;
			}
			if (!image.loading) {
				return;
			}
			image.loading = false;
			unqueue_image_load(image);
			image.clear();
			
			_webgl_image_load_callback(image.idx, true, 0);
		};
		
		image.onerror = function (reason) {
			if (image.disposed) {
				return;
			}
			if (!image.loading) {
				return;
			}

			if (image.attempts++ <= max_attempts) {
				image.clear();
				if (image.imageElement) {
					image.imageElement.src = '';
					image.imageElement = null;
				}
				return image.load();
			}

			image.loading = false;
			unqueue_image_load(image);
			image.clear();

			var message = String(reason);
			var message_bytes = lengthBytesUTF8(message);
			var ptr = _malloc(message_bytes + 1);
			stringToUTF8(message, ptr, message_bytes + 1);
			_webgl_image_load_callback(image.idx, false, ptr);
		};

		image.load = function () {
			var imageElement = new Image();
			if (imageElement.decode) {
				imageElement.src = url;
				imageElement.decode().then(function () {
					image.onload();
				}, function (reason) {
					image.onerror(reason);
				});
				image.clear = function () { };
			} else {
				function handlerLoad(event) {
					image.onload();
				}
				
				function handlerError(event) {
					image.onerror("failed to load " + image.url);
				}

				image.clear = function () {
					imageElement.removeEventListener('load', handlerLoad);
					imageElement.removeEventListener('error', handlerError);
				};

				imageElement.addEventListener('load', handlerLoad);
				imageElement.addEventListener('error', handlerError);
				imageElement.loading = "eager";
				imageElement.src = url;

			}
			image.imageElement = imageElement;
		}

		image.loading = true;
		queue_image_load(image);

		return image_idx;
	};

	exports._webgl_image_get_width = function (image_idx) {
		var image = images[image_idx];
		if (!image) {
			throw new Error("No such image with id " + image_idx);
		}
		return image.imageElement.naturalWidth;
	};

	exports._webgl_image_get_height = function (image_idx) {
		var image = images[image_idx];
		if (!image) {
			throw new Error("No such image with id " + image_idx);
		}
		return image.imageElement.naturalHeight;
	};

	exports._webgl_dispose_image = function (image_idx) {
		var image = images[image_idx];
		if (!image) {
			throw new Error("No such image with id " + image_idx);
		}
		image.dispose();
		images[image_idx] = null;
		images_free.push(image_idx);
	};

	exports._webgl_tex2d_image = function (target, level, x, y, format, type, image_idx) {
		var image = images[image_idx];
		if (!image) {
			throw new Error("No such image with id " + image_idx);
		}
		webgl.pixelStorei(webgl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
		webgl.texSubImage2D(target, level, x, y, format, type, image.imageElement);
		webgl.pixelStorei(webgl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, false);
	};
})(this);
