(function (exports) {
	var context = new (window.AudioContext || window.webkitAudioContext)();

	if (!context) {
		exports._webaudio_sound_load = function (ud, ptr, size) {
			return 0;
		};
		exports._webaudio_sound_play = function (sound_id, ud, offset, volume, pan) {
			return 0;
		};
		exports._webaudio_playback_get_position = function (sound_id) {
			return 0;
		};
		exports._webaudio_music_play = function (track_url, ud) {
			setTimeout(function () {
				_webaudio_callback_music_complete(0, ud, 2);
			}, 1000);
		};
		return;
	}

	var mute_gain = context.createGain();
	mute_gain.gain.value = 1.0;
	mute_gain.connect(context.destination);

	var global_gain = context.createGain();
	global_gain.connect(mute_gain);

	var music_gain = context.createGain();
	music_gain.connect(mute_gain);

	var sounds = [null];
	var free_sounds = [];

	var playbacks = [null];
	var free_playbacks = [];

	var active_sources = [];

	exports._webaudio_set_volume = function (value) {
		global_gain.gain.value = value;
	};

	exports._webaudio_sound_load = function (ud, ptr, size) {
		var data = HEAPU8.buffer.slice(ptr, ptr + size);
		var idx;
		if (free_sounds.length > 0) {
			idx = free_sounds.pop();
		} else {
			idx = sounds.length;
		}
		var sound = {
			idx: idx,
			alive: true,
			ud: ud,
			buffer: null,
			decoded: false
		};
		sounds[idx] = sound;

		function on_success(buffer) {
			if (!sound.alive) {
				return;
			}
			if (sound.decoded) {
				return;
			}
			sound.decoded = true;
			sound.buffer = buffer;
			_webaudio_callback_sound_loaded(sound.idx, sound.ud);
		}

		function on_fail(err) {
			if (!sound.alive) {
				return;
			}
			if (sound.decoded) {
				return;
			}
			sound.decoded = true;
			_webaudio_callback_sound_error(sound.idx, sound.ud);
		}

		var promise = context.decodeAudioData(
			data,
			on_success,
			on_fail
		);
		if (promise && promise.then) {
			promise.then(on_success, on_fail);
		}
		return idx;
	};

	function teardown_playback(playback) {
		if (playback.source) {
			playback.source.stop();
			playback.source.disconnect();
			playback.source = null;
		}
		if (playback.gain) {
			playback.gain.disconnect();
			playback.gain = null;
		}
	}

	exports._webaudio_sound_play = function (sound_id, ud, offset, volume, pan) {
		if (!sound_id) {
			return 0;
		}
		var sound = sounds[sound_id];
		if (!sound) {
			throw new Error("invalid sound index");
		}
		if (!sound.buffer) {
			return 0;
		}

		var gain = context.createGain();

		var source = context.createBufferSource();
		source.buffer = sound.buffer;
		source.connect(gain);
		gain.connect(global_gain);
		gain.gain.value = volume;
		source.start(0);

		var idx;
		if (free_playbacks.length > 0) {
			idx = free_playbacks.pop();
		} else {
			idx = playbacks.length;
		}

		var playback = {
			alive: true,
			ud: ud,
			idx: idx,
			source: source,
			gain: gain
		};

		source.addEventListener("ended", function (evt) {
			if (!playback.alive) {
				return;
			}
			teardown_playback(playback);
			_webaudio_callback_playback_complete(playback.idx, playback.ud);
		});

		playbacks[idx] = playback;
		return idx;
	};
	exports._webaudio_sound_destroy = function (sound_id) {
		var sound = sounds[sound_id];
		if (!sound) {
			throw new Error("invalid sound index");
		}
		sounds[sound_id] = null;
		free_sounds.push(sound_id);
		sound.buffer = null;
		sound.alive = false;
		sound.ud = null;
	};

	exports._webaudio_playback_update_transform = function (sound_id, volume, pan) {
		var playback = playbacks[sound_id];
		if (!playback) {
			throw new Error("invalid playback index");
		}
		if (playback.gain) {
			playback.gain.gain.value = volume;
		}
	};
	exports._webaudio_playback_get_position = function (sound_id) {
		var playback = playbacks[sound_id];
		if (!playback) {
			throw new Error("invalid playback index");
		}
		return 0;
	};
	exports._webaudio_playback_destroy = function (sound_id) {
		var playback = playbacks[sound_id];
		if (!playback) {
			throw new Error("invalid playback index");
		}
		playbacks[sound_id] = null;
		free_playbacks.push(sound_id);
		teardown_playback(playback);
		playback.alive = false;
		playback.ud = null;
	};

	// webaudio_callback_music_complete

	exports._webaudio_music_set_volume = function (volume) {
		music_gain.gain.value = volume;
	};

	var active_track = null;

	exports._webaudio_music_stop = function () {
		if (active_track) {
			var track = active_track;
			active_track = null;
			track.stop();
		}
	};

	exports._webaudio_music_play = function (track_url, ud) {
		var track = {
			finished: false,
			cleanedup: false,
			url: UTF8ToString(track_url),
			saved_time: 0,
			ud: ud
		};
		
		let gain_node = context.createGain();
		gain_node.gain.value = 1;
		gain_node.connect(music_gain);
		track.gain_node = gain_node;

		function start_audio(track) {
			if (track.audio) {
				track.audio.stop();
			}

			let audio_element = new Audio();
			if (!audio_element) {
				return null;
			}

			let source = document.createElement('source');
			source.setAttribute('type', 'audio/mpeg');
			source.setAttribute('src', track.url);
			audio_element.appendChild(source);

			let media_source_node = context.createMediaElementSource(audio_element);
			if (!media_source_node) {
				return null;
			}
			media_source_node.connect(track.gain_node);

			let object = {
				audio_element: audio_element,
				media_source_node: media_source_node
			};

			object.stop = function () {
				track.saved_time = audio_element.currentTime;
				audio_element.removeEventListener("error", track.on_audio_error);
				audio_element.removeEventListener("ended", track.on_audio_finished);
				audio_element.removeChild(source);
				audio_element.src = ".";
				audio_element.pause();
				if (media_source_node) {
					media_source_node.disconnect();
					media_source_node = null;
				}
				if (track.audio === object) {
					track.audio = null;
				}
			};

			object.play = function () {
				let playback = audio_element.play();
				if (playback) {
					playback.catch(function (err) {
						// do nothing
						// this can be caused by permission error
					});
				}
			}

			audio_element.addEventListener("error", track.on_audio_error);
			audio_element.addEventListener("ended", track.on_audio_finished);
			
			track.audio = object;
			object.play();
			let start_time = track.saved_time;
			if (typeof start_time === "number" && !isNaN(start_time)) {
				audio_element.currentTime = start_time;
			}

			return object;
		}

		active_sources[active_sources.length] = track;

		if (active_track) {
			var stop_track = active_track;
			active_track = null;
			stop_track.stop();
		}

		track.cleanup = function () {
			if (track.cleanedup) {
				return;
			}
			if (track.audio) {
				track.audio.stop();
			}
			track.cleanedup = true;
			if (gain_node) {
				gain_node.disconnect();
				gain_node = null;
			}
			if (active_track === track) {
				active_track = null;
			}
			var index = active_sources.indexOf(track);
			if (index >= 0) {
				active_sources.splice(index, 1);
			}
		}

		track.on_audio_error = function () {
			track.finish(1);
			track.cleanup();
		};
		track.on_audio_finished = function () {
			track.finish(0);
			track.cleanup();
		};

		track.finish = function (status) {			
			if (!track.finished) {
				track.finished = true;
				setTimeout(function () {
					_webaudio_callback_music_complete(0, ud, status);
				}, 0);
			}
		};

		track.try_resume = function () {
			if (track.audio) {
				track.audio.play();
			}
		};

		track.pause = function () {
			if (track.audio) {
				track.audio.stop();
			}
		};

		track.resume = function () {
			if (!track.audio) {
				start_audio(track);
			}
		};

		track.stop = function () {
			track.finish(2);
			var t = context.currentTime;
			if (gain_node.gain.exponentialRampToValueAtTime) {
				gain_node.gain.setValueAtTime(1, t);
				gain_node.gain.exponentialRampToValueAtTime(0.001, t + 1);
				gain_node.gain.setValueAtTime(0, t + 1);
			} else {
				gain_node.gain.setValueAtTime(0, t);
			}
			setTimeout(function () {
				track.cleanup();
			}, 3000);
		};

		start_audio(track);

		active_track = track;
	};

	var started = false;

	function on_touch_body() {
		if (!started) {
			context.resume().then(function () {
				started = true;
				if (active_track) {
					active_track.try_resume();
				}
			}, function () {
			});
		}
	}

	function update_global_volume() {
		var global_mute = false;

		if (typeof window.get_global_mute !== "undefined") {
			global_mute = window.get_global_mute();
		}
		mute_gain.gain.value = global_mute ? 0.0 : 1.0;
	}

	if (typeof window.set_global_mute !== "undefined") {
		var old_set_global_mute = window.set_global_mute;
		window.set_global_mute = function (flag) {
			old_set_global_mute(flag);
			update_global_volume();
		};
		window.set_global_mute(window.get_global_mute());
	}

	function on_visibility_change() {
		var index;
		update_global_volume();
		if (document.hidden) {
			for (index in active_sources) {
				(function (track) {
					track.pause();
				})(active_sources[index]);
			}
		} else {
			for (index in active_sources) {
				(function (track) {
					track.resume();
				})(active_sources[index]);
			}
		}
	}

	document.body.addEventListener('touchend', on_touch_body);
	document.body.addEventListener('click', on_touch_body);
	document.addEventListener("visibilitychange", on_visibility_change, false);
})(this);
