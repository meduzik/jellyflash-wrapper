#pragma once
#include "emscripten.h"

EM_EXPORT void webgl_image_load_callback(int imageID, bool success, const char* message);

namespace em {

using ImageID = int;
using ImageCallback = std::function<void(bool, std::string_view)>;

ImageID webgl_load_image(std::string_view url, ImageCallback&& on_completed);
void webgl_dispose_image(ImageID imageID);
void webgl_tex2d_image(int target, int level, int x, int y, int format, int type, ImageID image);
int webgl_image_get_width(ImageID imageID);
int webgl_image_get_height(ImageID imageID);

}



