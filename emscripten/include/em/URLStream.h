#pragma once
#include "emscripten.h"

EM_EXPORT void urlstream_on_open(int stream, void* ud);
EM_EXPORT void urlstream_on_progress(int stream, void* ud, int loaded, int total);
EM_EXPORT void urlstream_on_error(int stream, void* ud, int code, const char* message);
EM_EXPORT void urlstream_on_data(int stream, void* ud, void* ptr, size_t len);
EM_EXPORT void urlstream_on_complete(int stream, void* ud);

namespace em{

class URLStream{
public:
	enum class Method{
		GET,
		POST
	};

	URLStream();
	~URLStream();

	void set_open_callback(std::function<void()> fn);
	void set_complete_callback(std::function<void()> fn);
	void set_error_callback(std::function<void(int code, std::string message)> fn);
	void set_progress_callback(std::function<void(int loaded, int total)> fn);
	void set_data_callback(std::function<void(const uint8_t* data, size_t len)> fn);

	void set_url(std::string url);
	void set_data(const uint8_t* data, size_t size);
	void set_header(std::string name, std::string value);
	void set_method(Method method);
	void load();
	void dispose();
private:
	friend void ::urlstream_on_open(int stream, void* ud);
	friend void ::urlstream_on_progress(int stream, void* ud, int loaded, int total);
	friend void ::urlstream_on_error(int stream, void* ud, int code, const char* message);
	friend void ::urlstream_on_data(int stream, void* ud, void* ptr, size_t len);
	friend void ::urlstream_on_complete(int stream, void* ud);

	int handle;
	std::function<void()> open_callback;
	std::function<void()> complete_callback;
	std::function<void(int code, std::string message)> error_callback;
	std::function<void(int loaded, int total)> progress_callback;
	std::function<void(const uint8_t* data, size_t len)> data_callback;

	uint8_t buffer[8192];
};

}

