#pragma once
#include "emscripten.h"

namespace em{

void webutils_open_url(std::string_view url);
bool webutils_clipboard_copy(std::string_view text);
bool webutils_has_mouse();
void webutils_generate_bytes(uint8_t* buffer, size_t len);

}

