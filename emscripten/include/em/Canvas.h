#pragma once
#include "emscripten.h"

namespace em{

bool canvas_initialize();
void canvas_query_dimensions(int* width, int* height, double* scale_factor);

}

