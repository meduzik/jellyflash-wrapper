#pragma once

#if defined(__EMSCRIPTEN__)
#	include <emscripten.h>
#else
#	define EMSCRIPTEN_KEEPALIVE 
#endif

#include "std.h"

#define EM_IMPORT extern "C"
#define EM_EXPORT extern "C" EMSCRIPTEN_KEEPALIVE


#if defined(_MSC_VER)
#define EM_STUB { abort(); }
#else
#define EM_STUB ;
#endif


