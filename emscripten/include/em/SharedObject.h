#pragma once
#include "emscripten.h"

namespace em {

void SharedObjectStore(const char* path, const void* data, size_t len);
void SharedObjectLoad(const char* path, void* ud, void(*writer)(void* ud, void* buffer, size_t len));
void SharedObjectClear(const char* path);

}

