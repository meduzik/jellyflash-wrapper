#pragma once
#include "emscripten.h"

using WebAudioSound = int;
using WebAudioPlayback = int;

#define WEBAUDIO_STATUS_FINISHED 0
#define WEBAUDIO_STATUS_ERROR 1
#define WEBAUDIO_STATUS_FADED 2
#define WEBAUDIO_STATUS_NOT_STARTED 3

EM_IMPORT WebAudioSound webaudio_sound_load(void* ud, const uint8_t* data, size_t len);
EM_IMPORT WebAudioPlayback webaudio_sound_play(WebAudioSound sound, void* ud, double position, double volume, double pan);
EM_IMPORT void webaudio_sound_destroy(WebAudioSound sound);

EM_IMPORT void webaudio_set_volume(double volume);

EM_IMPORT double webaudio_playback_get_position(WebAudioPlayback sound);
EM_IMPORT void webaudio_playback_update_transform(WebAudioPlayback sound, double volume, double pan);
EM_IMPORT void webaudio_playback_destroy(WebAudioPlayback sound);

EM_EXPORT void webaudio_callback_sound_loaded(WebAudioSound sound, void* ud);
EM_EXPORT void webaudio_callback_sound_error(WebAudioSound sound, void* ud);

EM_EXPORT void webaudio_callback_playback_complete(WebAudioPlayback sound, void* ud);

EM_IMPORT void webaudio_music_set_volume(double volume);
EM_IMPORT void webaudio_music_stop();
EM_IMPORT int webaudio_music_play(const char* uri, void* ud);

EM_EXPORT void webaudio_callback_music_complete(int track, void* ud, int status);

namespace em{



}

