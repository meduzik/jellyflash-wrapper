#pragma once
#include "emscripten.h"

namespace em {

void ExternalInterfaceSend(std::string_view request, std::string& response);

// client code must provide the implementation for this function
void ExternalInterfaceRecv(std::string_view request, std::string& response);

}

#pragma once
