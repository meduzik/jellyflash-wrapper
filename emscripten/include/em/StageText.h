#pragma once
#include "emscripten.h"

namespace em{

struct StageText;

enum class TextAlign{
	Left = 0,
	Center = 1,
	Right = 2
};

struct StageTextListener{
	void* ud;
	void(*on_focusin)(void* ud, StageText*);
	void(*on_focusout)(void* ud, StageText*);
	void(*on_keydown)(void* ud, StageText*, int key_code);
};

StageText* stagetext_create(const StageTextListener* listener, bool multiline);
void stagetext_destroy(StageText* stage_text);
void stagetext_set_visible(StageText* stage_text, bool visible);
void stagetext_viewport(StageText* stage_text, int x, int y, int width, int height);
void stagetext_assign_focus(StageText* stage_text);
void stagetext_set_editable(StageText* stage_text, bool editable);
void stagetext_set_text(StageText* stage_text, const char* text);
void stagetext_get_text(StageText* stage_text, std::string& out);
void stagetext_set_font_family(StageText* stage_text, const char* family);
void stagetext_set_font_size(StageText* stage_text, int size);
void stagetext_set_font_color(StageText* stage_text, uint32_t color);
void stagetext_set_font_bold(StageText* stage_text, bool bold);
void stagetext_set_font_italic(StageText* stage_text, bool italic);
void stagetext_set_max_chars(StageText* stage_text, int maxchars);
void stagetext_set_multiline(StageText* stage_text, bool multiline);
void stagetext_set_text_align(StageText* stage_text, TextAlign align);

}

