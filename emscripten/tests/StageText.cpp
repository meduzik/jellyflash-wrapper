#include "Catch2.h"
#include <em/StageText.h>
using namespace em;

TEST_CASE("StageText", "[testing stage text]") {
	StageText* text = stagetext_create();
	stagetext_viewport(text, 50, 50, 300, 50);
	stagetext_set_font_bold(text, true);
	stagetext_set_font_color(text, 0xff00ff);
	stagetext_set_font_family(text, "helvetica");
	stagetext_set_font_size(text, 64);
	stagetext_set_text_align(text, TextAlign::Center);
	stagetext_set_text(text, "TEXT INPUT");
	stagetext_set_visible(text, true); 
	
	
}
