#include "Catch2.h"
#include <em/SharedObject.h>
using namespace em;

#define WRITE_DATA "this is a test data"

TEST_CASE("SharedObject", "[testing shared object]") {
	SharedObjectStore("key1", WRITE_DATA, sizeof(WRITE_DATA) - 1);
	SharedObjectLoad("key1", nullptr, [](void*, void* buffer, size_t len){
		std::cout << "data read (" << len << " bytes): " << std::string_view((const char*)buffer, len) << std::endl;
		const uint8_t* raw = (const uint8_t*)buffer;
		std::cout << "few raw bytes: " << (uint32_t)raw[0] << " " << (uint32_t)raw[1] << " " << (uint32_t)raw[2] << " " << (uint32_t)raw[3] << std::endl;
	});
}