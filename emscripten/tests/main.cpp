#define CATCH_CONFIG_RUNNER
#include "Catch2.h"

int main(int argc, char* argv[]) {
	// global setup...

	std::cout << " Starting application... " << std::endl;
	
	int result = Catch::Session().run(argc, argv);

	std::cout << " Exiting application... " << std::endl;

	// global clean-up...

	return result;
}