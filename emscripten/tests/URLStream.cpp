#include "Catch2.h"
#include <em/URLStream.h>
using namespace em;

TEST_CASE("URLStream", "[testing url stream]") {
	URLStream* stream = new URLStream();
	stream->set_url("em_test.wasm.map");
	stream->set_open_callback([&](){
		std::cout << "stream opened" << std::endl; 
	}); 
	stream->set_data_callback([&](const uint8_t* data, size_t len) {
		std::cout << "stream data: " << std::string_view((const char*)data, std::min((size_t)20, len)) << std::endl;
	});
	stream->set_error_callback([&](int code, std::string err) {
		std::cout << "stream error: " << code << " " << err << std::endl;
	});
	stream->set_progress_callback([&](int total, int loaded) {
		std::cout << "stream progress: " << loaded << "/" << total << std::endl;
	});
	stream->set_complete_callback([&]() {
		std::cout << "stream completed" << std::endl;
	});
	stream->load();
}