#include "Catch2.h"
#include <em/ExternalInterface.h>
using namespace em;

TEST_CASE("ExternalInterface", "[testing external interface]") {
	std::string ss;
	ExternalInterfaceSend("{\"func\":\"aaa\",\"args\":[]}", ss);
	std::cout << "external interface response: " << ss << std::endl;
}

namespace em{
void ExternalInterfaceRecv(std::string_view request, std::string& response){
	std::cout << "we got external call: " << request << std::endl;
	response = "{\"res\":\"ok\",\"val\":42}";
}
}


