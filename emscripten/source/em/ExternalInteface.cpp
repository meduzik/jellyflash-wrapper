#include <em/ExternalInterface.h>

namespace em{

EM_IMPORT const void* externalinterface_call(const void* request_data, size_t request_len, size_t* response_len);
EM_IMPORT void externalinterface_dispose_response(const void* response_data, size_t response_len);


void ExternalInterfaceSend(std::string_view request, std::string& response){
	size_t response_len = 0;
	std::string zrequest(request);
	const void* response_data = externalinterface_call(zrequest.data(), zrequest.size(), &response_len);
	response += std::string_view((const char*)response_data, response_len);
	externalinterface_dispose_response(response_data, response_len);
}

EM_EXPORT const void* externalinterface_cb_call(void* request_data, size_t request_len, size_t* response_len){
	std::string str;
	ExternalInterfaceRecv(std::string_view((const char*)request_data, request_len), str);
	char* mem = (char*)malloc(str.size() + 1);
	memcpy(mem, str.data(), str.size());
	mem[str.size()] = 0;
	*response_len = str.size();
	return mem;
}

EM_EXPORT void externalinterface_cb_dispose_response(void* response_data, size_t response_len){
	free(response_data);
}


}
