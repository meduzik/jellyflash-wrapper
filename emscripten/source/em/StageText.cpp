#include <em/StageText.h>


#if defined(_MSC_VER)
#define EM_STUB { abort(); }
#else
#define EM_STUB ;
#endif

EM_IMPORT int stagetext_create(void* ud, bool multiline) EM_STUB;
EM_IMPORT void stagetext_destroy(int handle) EM_STUB;
EM_IMPORT void stagetext_set_visible(int handle, int visible) EM_STUB;
EM_IMPORT void stagetext_assign_focus(int handle) EM_STUB;
EM_IMPORT void stagetext_viewport(int handle, int x, int y, int width, int height) EM_STUB;
EM_IMPORT void stagetext_set_editable(int handle, bool editable);
EM_IMPORT void stagetext_set_text(int handle, const char* text);
EM_IMPORT void* stagetext_get_text(int handle);
EM_IMPORT void stagetext_get_text_free(void* ptr);
EM_IMPORT void stagetext_set_font_family(int handle, const char* family);
EM_IMPORT void stagetext_set_font_size(int handle, int size);
EM_IMPORT void stagetext_set_font_color(int handle, uint32_t color);
EM_IMPORT void stagetext_set_font_bold(int handle, bool bold);
EM_IMPORT void stagetext_set_font_italic(int handle, bool italic);
EM_IMPORT void stagetext_set_max_chars(int handle, int maxchars);
EM_IMPORT void stagetext_set_multiline(int handle, bool multiline);
EM_IMPORT void stagetext_set_text_align(int handle, int align);

EM_EXPORT void stagetext_on_focusin(void* ud);
EM_EXPORT void stagetext_on_focusout(void* ud);
EM_EXPORT void stagetext_on_keydown(void* ud, int key);

namespace em{

struct StageText{
	int handle;
	StageTextListener listener;
};

StageText* stagetext_create(const StageTextListener* listener, bool multiline){
	StageText* stage_text = new(malloc(sizeof(StageText)))StageText();
	stage_text->listener = *listener;
	stage_text->handle = ::stagetext_create(stage_text, multiline);
	return stage_text;
}

void stagetext_assign_focus(StageText* stage_text){
	::stagetext_assign_focus(stage_text->handle);
}

void stagetext_destroy(StageText* stage_text){
	::stagetext_destroy(stage_text->handle);
	free(stage_text);
}

void stagetext_set_visible(StageText* stage_text, bool visible){
	::stagetext_set_visible(stage_text->handle, visible);
}

void stagetext_viewport(StageText* stage_text, int x, int y, int width, int height){
	::stagetext_viewport(stage_text->handle, x, y, width, height);
}

void stagetext_set_editable(StageText* stage_text, bool editable){
	::stagetext_set_editable(stage_text->handle, editable);
}

void stagetext_set_text(StageText* stage_text, const char* text){
	::stagetext_set_text(stage_text->handle, text);
}

void stagetext_get_text(StageText* stage_text, std::string& out) {
	const char* text = (const char*)::stagetext_get_text(stage_text->handle);
	out = text;
	::stagetext_get_text_free((void*)text);
}

void stagetext_set_font_family(StageText* stage_text, const char* family) {
	::stagetext_set_font_family(stage_text->handle, family);
}

void stagetext_set_font_size(StageText* stage_text, int size) {
	::stagetext_set_font_size(stage_text->handle, size);
}

void stagetext_set_font_color(StageText* stage_text, uint32_t color) {
	::stagetext_set_font_color(stage_text->handle, color);
}

void stagetext_set_font_bold(StageText* stage_text, bool bold) {
	::stagetext_set_font_bold(stage_text->handle, bold);
}

void stagetext_set_font_italic(StageText* stage_text, bool italic) {
	::stagetext_set_font_italic(stage_text->handle, italic);
}

void stagetext_set_max_chars(StageText* stage_text, int maxchars) {
	::stagetext_set_max_chars(stage_text->handle, maxchars);
}

void stagetext_set_multiline(StageText* stage_text, bool multiline) {
	::stagetext_set_multiline(stage_text->handle, multiline);
}

void stagetext_set_text_align(StageText* stage_text, TextAlign align) {
	::stagetext_set_text_align(stage_text->handle, (int)align);
}

EM_EXPORT void stagetext_on_focusin(void* ud){
	StageText* stage_text = (StageText*)ud;
	stage_text->listener.on_focusin(stage_text->listener.ud, stage_text);
}

EM_EXPORT void stagetext_on_focusout(void* ud){
	StageText* stage_text = (StageText*)ud;
	stage_text->listener.on_focusout(stage_text->listener.ud, stage_text);
}

EM_EXPORT void stagetext_on_keydown(void* ud, int key){
	StageText* stage_text = (StageText*)ud;
	stage_text->listener.on_keydown(stage_text->listener.ud, stage_text, key);
}

}


