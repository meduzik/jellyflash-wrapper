#include "em/WebGL.h"

EM_IMPORT int webgl_load_image(const char* url) EM_STUB;
EM_IMPORT void webgl_dispose_image(int image_id) EM_STUB;
EM_IMPORT void webgl_tex2d_image(int target, int level, int x, int y, int format, int type, int image_id) EM_STUB;

EM_IMPORT int webgl_image_get_width(int image_id) EM_STUB;
EM_IMPORT int webgl_image_get_height(int image_id) EM_STUB;

namespace em {

struct ImageData {
	ImageCallback callback;
	int id;

	ImageData(ImageCallback&& callback, int id) :
		callback(std::move(callback)),
		id(id) {
	}
};
std::unordered_map<int, std::unique_ptr<ImageData>> images;

ImageID webgl_load_image(std::string_view url, ImageCallback&& on_completed) {
	std::string ss(url);

	int image_id = ::webgl_load_image(ss.c_str());

	std::unique_ptr<ImageData> imageData(new ImageData(std::move(on_completed), image_id));

	images.insert(std::make_pair(image_id, std::move(imageData)));

	return image_id;
}

void webgl_dispose_image(ImageID imageID) {
	auto it = images.find(imageID);
	if (it != images.end()) {
		images.erase(it);
	}
	::webgl_dispose_image(imageID);
}

void webgl_tex2d_image(int target, int level, int x, int y, int format, int type, ImageID image) {
	::webgl_tex2d_image(target, level, x, y, format, type, image);
}

int webgl_image_get_width(ImageID imageID) {
	return ::webgl_image_get_width(imageID);
}

int webgl_image_get_height(ImageID imageID) {
	return ::webgl_image_get_height(imageID);
}

}

EM_EXPORT void webgl_image_load_callback(int image_id, bool success, const char* message) {
	auto it = em::images.find(image_id);
	if (it != em::images.end()) {
		auto image_data = std::move(it->second);
		em::images.erase(it);
		image_data->callback(success, message);
	}
}
