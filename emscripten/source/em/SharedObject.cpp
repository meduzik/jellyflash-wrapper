#include <em/SharedObject.h>

namespace em{

EM_IMPORT void* sharedobject_load(const char* path, size_t* len);
EM_IMPORT void sharedobject_load_free(void* data);
EM_IMPORT void sharedobject_store(const char* path, const void* data, size_t len);
EM_IMPORT void sharedobject_clear(const char* path);

void SharedObjectStore(const char* path, const void* data, size_t len){
	sharedobject_store(path, data, len);
}

void SharedObjectLoad(const char* path, void* ud, void(*writer)(void* ud, void* buffer, size_t len)){
	size_t len = 0;
	void* data = sharedobject_load(path, &len);
	writer(ud, data, len);
	sharedobject_load_free(data);
}

void SharedObjectClear(const char* path){
	sharedobject_clear(path);
}


}
