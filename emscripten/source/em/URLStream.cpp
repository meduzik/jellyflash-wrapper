#include <em/URLStream.h>


#if defined(_MSC_VER)
#define EM_STUB { abort(); }
#else
#define EM_STUB ;
#endif

EM_IMPORT int urlstream_create(void* ud) EM_STUB
EM_IMPORT void urlstream_set_buffer(int stream, void* buffer, size_t len) EM_STUB;
EM_IMPORT void urlstream_set_url(int stream, const char* str) EM_STUB
EM_IMPORT void urlstream_set_method(int stream, int method) EM_STUB
EM_IMPORT void urlstream_set_header(int stream, const char* key, const char* value) EM_STUB
EM_IMPORT void urlstream_set_data(int stream, const void* data, size_t len) EM_STUB
EM_IMPORT void urlstream_load(int stream) EM_STUB
EM_IMPORT void urlstream_destroy(int stream) EM_STUB

namespace em{

URLStream::URLStream(){
	handle = urlstream_create(this);
	urlstream_set_buffer(handle, buffer, sizeof(buffer));
}

URLStream::~URLStream(){
	urlstream_destroy(handle);
}

void URLStream::set_open_callback(std::function<void()> fn){
	open_callback = std::move(fn);
}

void URLStream::set_complete_callback(std::function<void()> fn){
	complete_callback = std::move(fn);
}

void URLStream::set_error_callback(std::function<void(int code, std::string message)> fn){
	error_callback = std::move(fn);
}

void URLStream::set_progress_callback(std::function<void(int bytes_total, int bytes_loaded)> fn){
	progress_callback = std::move(fn);
}

void URLStream::set_data_callback(std::function<void(const uint8_t* data, size_t len)> fn){
	data_callback = std::move(fn);
}

void URLStream::set_url(std::string url){
	urlstream_set_url(handle, url.c_str());
}

void URLStream::set_header(std::string name, std::string value){
	urlstream_set_header(handle, name.c_str(), value.c_str());
}

void URLStream::set_data(const uint8_t* data, size_t size){
	urlstream_set_data(handle, data, size);
}

void URLStream::set_method(Method method){
	switch(method){
	case Method::GET:{
		urlstream_set_method(handle, 1);
	}break;
	case Method::POST:{
		urlstream_set_method(handle, 2);
	}break;
	}
}

void URLStream::load(){
	urlstream_load(handle);
}

void URLStream::dispose(){
	urlstream_destroy(handle);
}

}

using namespace em;

EM_EXPORT void urlstream_on_open(int, void* ud){
	URLStream* stream = (URLStream*)ud;
	stream->open_callback();
}

EM_EXPORT void urlstream_on_progress(int, void* ud, int loaded, int total){
	URLStream* stream = (URLStream*)ud;
	stream->progress_callback(loaded, total);
}

EM_EXPORT void urlstream_on_error(int, void* ud, int code, const char* message){
	URLStream* stream = (URLStream*)ud;
	stream->error_callback(code, message);
}

EM_EXPORT void urlstream_on_data(int, void* ud, void* ptr, size_t len){
	URLStream* stream = (URLStream*)ud;
	stream->data_callback((const uint8_t*)ptr, len);
}

EM_EXPORT void urlstream_on_complete(int, void* ud){
	URLStream* stream = (URLStream*)ud;
	stream->complete_callback();
}
