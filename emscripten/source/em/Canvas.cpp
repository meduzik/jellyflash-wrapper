#include <em/StageText.h>


#if defined(_MSC_VER)
#define EM_STUB { abort(); }
#else
#define EM_STUB ;
#endif

EM_IMPORT int canvas_initialize();
EM_IMPORT void canvas_query_dimensions(int* width, int* height, double* scale_factor);

namespace em{

bool canvas_initialize() {
	return ::canvas_initialize();
}

void canvas_query_dimensions(int* width, int* height, double* scale_factor){
	::canvas_query_dimensions(width, height, scale_factor);
}

}


