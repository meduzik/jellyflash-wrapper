#include <em/StageText.h>

EM_IMPORT void webutils_open_url(const uint8_t* data, size_t len) EM_STUB;
EM_IMPORT int webutils_clipboard_copy(const uint8_t* data, size_t len) EM_STUB;
EM_IMPORT int webutils_has_mouse() EM_STUB;
EM_IMPORT void webutils_generate_bytes(uint8_t* buffer, size_t len) EM_STUB;

void webutils_on_crash_impl(const char* message);

EM_EXPORT void webutils_on_crash(const char* message){
	webutils_on_crash_impl(message);
}

namespace em{

void webutils_open_url(std::string_view url){
	std::string ss(url);
	::webutils_open_url((const uint8_t*)ss.data(), ss.size());
}

bool webutils_clipboard_copy(std::string_view text){
	std::string ss(text);
	return ::webutils_clipboard_copy((const uint8_t*)ss.data(), ss.size());
}

void webutils_generate_bytes(uint8_t* buffer, size_t len){
	::webutils_generate_bytes(buffer, len);
}

bool webutils_has_mouse() {
	return ::webutils_has_mouse();
}


}


