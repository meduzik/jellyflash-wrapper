#include <fl/flash/display/DisplayObject.h>
#include <fl/flash/display/LoaderInfo.h>
#include <fl/flash/display/Stage.h>
#include <fl/com/meduzik/BuildID.h>
#include <fl/com/meduzik/Main.h>
#include <jfl/app/application.h>

#define PACKAGE_ID "com.meduzik.jellyflash.matcharena"
static const uint8_t PackageID[] = PACKAGE_ID;
static const jfl::StringObject PackageIDString { 
	&jfl::StringObject::jfl_VT, 
	jfl::static_object,
	sizeof(PackageID) - 1, 
	PackageID
};

namespace jfl{
jfl::String ApplicationPackageID = jfl::String((jfl::StringObject*)&PackageIDString);
}

#if defined(__ANDROID__)
extern "C" void Android_main(){
	jfl::ApplicationDescriptor descriptor;
	descriptor.initialWidth = 960;
	descriptor.initialHeight = 640;
	jfl::Application* app = jfl::InitializeApplication(&descriptor);
	app->start();
	fl::com::meduzik::Main* object = fl::com::meduzik::Main::jfl_New();
	app->setMainSprite((fl::flash::display::DisplayObject*)object);
}
#else
int main(int argc, char *argv[]){
	jfl::ApplicationDescriptor descriptor;
	descriptor.initialWidth = 960;
	descriptor.initialHeight = 640;
	jfl::Application* app = jfl::InitializeApplication(&descriptor);
	if (argc > 1){
		app->start(argv[1]);
	}else{
		app->start("");
	}
	fl::com::meduzik::Main* object = fl::com::meduzik::Main::jfl_New();
	app->setMainSprite((fl::flash::display::DisplayObject*)object);
	app->loop();
	return 0;
}
#endif
