#pragma once

#include <jfl/render/OpenGL.h>

namespace jfl::render::opengl {

inline GLenum ToGL(BufferUsage usage) {
	switch (usage) {
	case BufferUsage::Dynamic: return GL_DYNAMIC_DRAW;
	case BufferUsage::Static: return GL_STATIC_DRAW;
	default: jfl_unreachable;
	}
}

inline GLenum ToGL(BlendFactor factor) {
	switch (factor) {
	case BlendFactor::DestAlpha: return GL_DST_ALPHA;
	case BlendFactor::DestColor: return GL_DST_COLOR;
	case BlendFactor::One: return GL_ONE;
	case BlendFactor::OneMinusDestAlpha: return GL_ONE_MINUS_DST_ALPHA;
	case BlendFactor::OneMinusDestColor: return GL_ONE_MINUS_DST_COLOR;
	case BlendFactor::OneMinusSrcAlpha: return GL_ONE_MINUS_SRC_ALPHA;
	case BlendFactor::OneMinusSrcColor: return GL_ONE_MINUS_SRC_COLOR;
	case BlendFactor::SrcAlpha: return GL_SRC_ALPHA;
	case BlendFactor::SrcColor: return GL_SRC_COLOR;
	case BlendFactor::Zero: return GL_ZERO;
	default: jfl_unreachable;
	}
}

inline GLenum ToGL(CompareMode cmp) {
	switch (cmp) {
	case CompareMode::Always: return GL_ALWAYS;
	case CompareMode::Equal: return GL_EQUAL;
	case CompareMode::Greater: return GL_GREATER;
	case CompareMode::GreaterEqual: return GL_GEQUAL;
	case CompareMode::Less: return GL_LESS;
	case CompareMode::LessEqual: return GL_LEQUAL;
	case CompareMode::Never: return GL_NEVER;
	case CompareMode::NotEqual: return GL_NOTEQUAL;
	default: jfl_unreachable;
	}
}

inline GLenum ToGL(StencilAction action) {
	switch (action) {
	case StencilAction::Zero: return GL_ZERO;
	case StencilAction::Set: return GL_REPLACE;
	case StencilAction::Keep: return GL_KEEP;
	case StencilAction::Invert: return GL_INVERT;
	case StencilAction::IncrementWrap: return GL_INCR_WRAP;
	case StencilAction::IncrementSaturate: return GL_INCR;
	case StencilAction::DecrementWrap: return GL_DECR_WRAP;
	case StencilAction::DecrementSaturate: return GL_DECR;
	default: jfl_unreachable;
	}
}

}
