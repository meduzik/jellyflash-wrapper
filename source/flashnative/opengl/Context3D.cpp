#include "Context3D.h"

namespace jfl::render::opengl {

OpenGLContext3D::OpenGLContext3D(){
#if defined(GL_VAO_REQUIRED)
	CHECK(glGenVertexArrays(1, &vao));
	CHECK(glBindVertexArray(vao));
#endif
	memset(vertex_constants, 0, sizeof(vertex_constants));
	memset(fragment_constants, 0, sizeof(fragment_constants));
}

void OpenGLContext3D::setWindowSize(int width, int height){
	window_width = width;
	window_height = height;
	must_sync_viewport = true;
}

void OpenGLContext3D::setErrorChecking(bool value){
#if !defined(OVERRIDE_GL_CHECK)
	error_checking = value;
#endif
}

bool OpenGLContext3D::getErrorChecking(){
	return error_checking;
}

jfl::String OpenGLContext3D::getDriverInfo(){
	if ( driver.tag == jfl::type_tag::Null ){
		const GLubyte* vendor = glGetString(GL_VENDOR);
		const GLubyte* renderer = glGetString(GL_RENDERER);
		const GLubyte* version = glGetString(GL_VERSION);
		std::string s;
		s += (const char*)vendor;
		s += " ";
		s += (const char*)renderer;
		s += " ";
		s += (const char*)version;
		driver = jfl::String::Make(s);
		if (driver.tag == jfl::type_tag::BigString) {
			driver_str.reset((jfl::StringObject*)driver.sb.object);
		}
	}
	return driver;
}

void OpenGLContext3D::configureBackBuffer(int width, int height, bool depthAndStencil){
	backbuffer_width = width;
	backbuffer_height = height;
	
	if ( !rendering_to_texture ){
		buffer_width = backbuffer_width;
		buffer_height = backbuffer_height;
		must_sync_viewport = true;
	}
}

VertexBuffer* OpenGLContext3D::createVertexBuffer(int numVertices, int dataPerVertex, BufferUsage usage){
	OpenGLVertexBuffer* vbo = new OpenGLVertexBuffer(this, numVertices, dataPerVertex, usage);
	return vbo;
}

IndexBuffer* OpenGLContext3D::createIndexBuffer(int numIndices, BufferUsage usage){
	OpenGLIndexBuffer* ibo = new OpenGLIndexBuffer(this, numIndices, usage);
	return ibo;
}

Program* OpenGLContext3D::createProgram(){
	OpenGLProgram* program = new OpenGLProgram(this);
	return program;
}

Texture2D* OpenGLContext3D::createTexture2D(int width, int height, TextureFormat format, bool optimizeForRender, bool mipmaps){
	OpenGLTexture2D* texture = new OpenGLTexture2D(this);
	texture->allocate(width, height, format, optimizeForRender, mipmaps);
	return texture;
}

int OpenGLContext3D::getBackBufferWidth(){
	return buffer_width;
}

int OpenGLContext3D::getBackBufferHeight(){
	return buffer_height;
}

void OpenGLContext3D::clear(double red, double green, double blue, double alpha, double depth, uint32_t stencil, ClearMask mask){
	CHECK(glClearColor((GLclampf)red, (GLclampf)green, (GLclampf)blue, (GLclampf)alpha));
	CHECK(glClearStencil(stencil));
#if defined(JFL_OPENGL_GLES) || defined(JFL_OPENGL_WEBGL)
	CHECK(glClearDepthf(depth));
#else
	CHECK(glClearDepth(depth));
#endif
	uint32_t mask_value = (uint32_t)mask;
	GLbitfield gl_mask = 0;
	if ( mask_value & (uint32_t)ClearMask::Color ){
		gl_mask |= GL_COLOR_BUFFER_BIT;
	}
	if ( mask_value & (uint32_t)ClearMask::Depth ) {
		gl_mask |= GL_DEPTH_BUFFER_BIT;
	}
	if ( mask_value & (uint32_t)ClearMask::Stencil ) {
		gl_mask |= GL_STENCIL_BUFFER_BIT;
	}
	CHECK(glClear(gl_mask));
}

void OpenGLContext3D::setDepthTest(bool write, CompareMode compare){
	bool need_depth_test = write || compare != CompareMode::Always;
	if ( need_depth_test ){
		if (gl_depth_mask != write){
			CHECK(glDepthMask(write));
			gl_depth_mask = write;
		}
		GLenum func = ToGL(compare);
		if (gl_depth_func != func){
			CHECK(glDepthFunc(func));
			gl_depth_func = func;
		}
	}
	if (gl_depth_enabled != need_depth_test){
		if (need_depth_test){
			CHECK(glEnable(GL_DEPTH_TEST));
		}else{
			CHECK(glDisable(GL_DEPTH_TEST));
		}
		gl_depth_enabled = need_depth_test;
	}
}

void OpenGLContext3D::syncProgram(){
	CHECK(glUseProgram(active_program->name));
	for ( auto& uniform : active_program->uniforms ){
		float* constants;
		if ( uniform.shader_type == GL_VERTEX_SHADER ){
			constants = vertex_constants;
		}else{
			constants = fragment_constants;
		}
		if ( memcmp(uniform.cached_data.get(), constants + uniform.first_index * 4, uniform.used_indices * 4) ){
			memcpy(uniform.cached_data.get(), constants + uniform.first_index * 4, uniform.used_indices * 4);
			switch ( uniform.type ){
			case OpenGLProgram::Uniform::Type::Float:{
				CHECK(glUniform1fv(uniform.location, 1, constants + uniform.first_index * 4));
			}break;
			case OpenGLProgram::Uniform::Type::Float2: {
				CHECK(glUniform2fv(uniform.location, 1, constants + uniform.first_index * 4));
			}break;
			case OpenGLProgram::Uniform::Type::Float3: {
				CHECK(glUniform3fv(uniform.location, 1, constants + uniform.first_index * 4));
			}break;
			case OpenGLProgram::Uniform::Type::Float4: {
				CHECK(glUniform4fv(uniform.location, 1, constants + uniform.first_index * 4));
			}break;
			case OpenGLProgram::Uniform::Type::Mat2: {
				float mat[2][2];
				for (int i = 0; i < 2; i++) {
					memcpy(mat[i], constants + uniform.first_index * 4 + i * 4, sizeof(float) * 2);
				}
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < i; j++) {
						std::swap(mat[i][j], mat[j][i]);
					}
				}
				CHECK(glUniformMatrix2fv(uniform.location, 1, false, (const GLfloat*)mat));
			}break;
			case OpenGLProgram::Uniform::Type::Mat3: {
				float mat[3][3];
				for (int i = 0; i < 3; i++) {
					memcpy(mat[i], constants + uniform.first_index * 4 + i * 4, sizeof(float) * 3);
				}
				for (int i = 0; i < 3; i++){
					for (int j = 0; j < i; j++) {
						std::swap(mat[i][j], mat[j][i]);
					}
				}
				CHECK(glUniformMatrix3fv(uniform.location, 1, false, (const GLfloat*)mat));
			}break;
			case OpenGLProgram::Uniform::Type::Mat4: {
				float mat[4][4];
				memcpy(mat, constants + uniform.first_index * 4, sizeof(mat));
				for ( int i = 0; i < 4; i++ ){
					for ( int j = 0; j < i; j++ ){
						std::swap(mat[i][j], mat[j][i]);
					}
				}
				CHECK(glUniformMatrix4fv(uniform.location, 1, false, (const GLfloat*)mat));
			}break;
			default:{
				jfl_unreachable;
			}break;
			}
		}
	}
}

void OpenGLContext3D::syncStencil(){
	if ( stencil.touched ){
		bool enable = stencil.front.requiresStencilTest() || stencil.back.requiresStencilTest();
	
		if (enable != stencil.gl_enabled){
			if (enable){
				CHECK(glEnable(GL_STENCIL_TEST));
			}else{
				CHECK(glDisable(GL_STENCIL_TEST));
			}
			stencil.gl_enabled = enable;
		}

		if ( enable ){
			auto& front = stencil.front;
			auto& back = stencil.back;

			bool front_needs_sync = front.needsSync(stencil.read_mask, stencil.ref);
			bool back_needs_sync = back.needsSync(stencil.read_mask, stencil.ref);
			if ( 
				front_needs_sync
				&&
				back_needs_sync
				&&
				front.func == back.func 
				&& 
				front.op_depth_fail == back.op_depth_fail
				&&
				front.op_depth_pass == back.op_depth_pass
				&&
				front.op_stencil_fail == back.op_stencil_fail
			){
				front.sync(this, GL_FRONT_AND_BACK, stencil.read_mask, stencil.ref);
				back = front;
			}else{
				if ( front_needs_sync ){
					front.sync(this, GL_FRONT, stencil.read_mask, stencil.ref);
				}
				if ( back_needs_sync ){
					front.sync(this, GL_BACK, stencil.read_mask, stencil.ref);
				}
			}

			if (stencil.gl_write_mask != stencil.write_mask) {
				CHECK(glStencilMask(stencil.write_mask));
				stencil.gl_write_mask = stencil.write_mask;
			}
		}

		stencil.touched = false;
	}
}

void OpenGLContext3D::StencilData::StencilFace::sync(OpenGLContext3D* owner, GLenum face, uint32_t read_mask, uint32_t ref){
	CHECK(glStencilFuncSeparate(face, func, (GLint)ref, read_mask));
	gl_func = func;
	gl_ref = ref;
	gl_read_mask = read_mask;
	CHECK(glStencilOpSeparate(face, op_stencil_fail, op_depth_fail, op_depth_pass));
	gl_op_stencil_fail = op_stencil_fail;
	gl_op_depth_fail = op_depth_fail;
	gl_op_depth_pass = op_depth_pass;
}

bool OpenGLContext3D::StencilData::StencilFace::needsSync(uint32_t read_mask, uint32_t ref){
	return (
		(func ^ gl_func) 
		|
		(op_depth_fail ^ gl_op_depth_fail)
		|
		(op_depth_pass ^ gl_op_depth_pass)
		|
		(op_stencil_fail ^ gl_op_stencil_fail)
		|
		(ref ^ gl_ref)
		|
		(read_mask ^ gl_read_mask)
	);
}

bool OpenGLContext3D::StencilData::StencilFace::requiresStencilTest(){
	return op_depth_fail != GL_KEEP || op_depth_pass != GL_KEEP || op_stencil_fail != GL_KEEP || func != GL_ALWAYS;
}

void OpenGLContext3D::syncState(){
	if ( must_sync_viewport ){	
		CHECK(glViewport(0, 0, buffer_width, buffer_height));
		if(gl_scissor){
			if ( rendering_to_texture ){
				CHECK(glScissor(scissor_x, buffer_height - scissor_y - scissor_height, scissor_width, scissor_height));
			}else{
				CHECK(glScissor(scissor_x, scissor_y, scissor_width, scissor_height));
			}
		}
		must_sync_viewport = false;
	}

	syncProgram();
	syncSamplers();
	syncStencil();
}

void OpenGLContext3D::syncSamplers(){
	for (int i = 0; i < MaxSamplers; i++){
		auto& sampler_state = samplers[i];
		if ( sampler_state.bound_texture ){
			OpenGLTexture* texture = sampler_state.bound_texture;
			if ( texture->target == GL_TEXTURE_2D ){
				texture->bind(i);
				texture->set_sampler_state(sampler_state);
			}else{
				texture->bind(i);
				texture->set_sampler_state(sampler_state);
			}
		}
	}
}

void OpenGLContext3D::changeActiveTexture(int sampler){
	if (gl_active_unit != sampler){
		gl_active_unit = sampler;
		CHECK(glActiveTexture(GL_TEXTURE0 + sampler));
	}
}

void OpenGLContext3D::drawTriangles(IndexBuffer* ibo_, int firstIndex, int numTriangles){
	if ( !active_program ){
		jfl::raise_message("no active program");
	}

	syncState();
	
	OpenGLIndexBuffer* ibo = (OpenGLIndexBuffer*)ibo_;
	CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo->buffer));
	CHECK(glDrawElements(GL_TRIANGLES, numTriangles * 3, GL_UNSIGNED_SHORT, (const void*)(firstIndex * 2)));
}

void OpenGLContext3D::present(){
	GetApplication()->present();
}

void OpenGLContext3D::setBlendFactors(BlendFactor source, BlendFactor dest){
	GLenum src = ToGL(source);
	GLenum dst = ToGL(dest);
	bool need_blend = !(src == GL_ONE && dst == GL_ZERO);
	if (need_blend){
		if (src != gl_blend_src || dst != gl_blend_dst){
			CHECK(glBlendFunc(src, dst));
		}
	}

	if (gl_blend_enabled != need_blend){
		if (need_blend) {
			CHECK(glEnable(GL_BLEND));
		} else {
			CHECK(glDisable(GL_BLEND));
		}
		gl_blend_enabled = need_blend;
	}
}

void OpenGLContext3D::setColorMask(bool red, bool green, bool blue, bool alpha){
	uint32_t mask = (red * (1 << 3)) | (green * (1 << 3)) | (blue * (1 << 3)) | (alpha * (1 << 3));
	if (gl_color_mask != mask){
		CHECK(glColorMask(red, green, blue, alpha));
	}
}

void OpenGLContext3D::setCulling(TriangleFace face){
	jfl_notimpl;
}

void OpenGLContext3D::setProgram(Program* program_){
	OpenGLProgram* program = (OpenGLProgram*)program_;
	active_program = program;
}

void OpenGLContext3D::setProgramConstant(ProgramType type, int reg, const double data[4]){
	switch ( type ){
	case ProgramType::Vertex:{
		for ( int i = 0; i < 4; i++ ){
			vertex_constants[reg * 4 + i] = (float)data[i];
			
		}
	}break;
	case ProgramType::Fragment: {
		for (int i = 0; i < 4; i++) {
			fragment_constants[reg * 4 + i] = (float)data[i];
		}
	}break;
	default:{
		jfl_unreachable;
	}break;
	}
}

void OpenGLContext3D::setRenderToBackBuffer(){
	CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
	buffer_width = backbuffer_width;
	buffer_height = backbuffer_height;
	rendering_to_texture = false;
	must_sync_viewport = true;
}

void OpenGLContext3D::hintFramebuffer(int width, int height){
	framebuffer_requested_width = std::max(width, framebuffer_requested_width);
	framebuffer_requested_height = std::max(height, framebuffer_requested_height);
}

void OpenGLContext3D::initFramebuffer(int width, int height, bool depth_and_stencil){
	if ( !framebuffer ){
		CHECK(glGenFramebuffers(1, &framebuffer));
	}
	CHECK(glBindFramebuffer(GL_FRAMEBUFFER, framebuffer));
	if ( depth_and_stencil ){
		if (!renderbuffers[0]) {
			CHECK(glGenRenderbuffers(2, renderbuffers));
		}
		if (framebuffer_width < width || framebuffer_height < height){
			framebuffer_width = std::max(width, framebuffer_requested_width);
			framebuffer_height = std::max(height, framebuffer_requested_height);
		#if defined(JFL_OPENGL_GLES)
			CHECK(glBindRenderbuffer(GL_RENDERBUFFER, renderbuffers[0]));
			CHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, framebuffer_width, framebuffer_height));
			CHECK(glBindRenderbuffer(GL_RENDERBUFFER, renderbuffers[1]));
			CHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_STENCIL_INDEX8, framebuffer_width, framebuffer_height));
		#else
			CHECK(glBindRenderbuffer(GL_RENDERBUFFER, renderbuffers[0]));
			#if defined(JFL_OPENGL_WEBGL)
				CHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_STENCIL, framebuffer_width, framebuffer_height));
			#else
				CHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, framebuffer_width, framebuffer_height));
			#endif
		#endif
		}
		#if defined(JFL_OPENGL_GLES)
			CHECK(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderbuffers[0]));
			CHECK(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuffers[1]));
		#else
			CHECK(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuffers[0]));
		#endif
	}else{
		CHECK(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0));
		CHECK(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, 0));
	}
}

void OpenGLContext3D::setRenderToTexture(TextureBase* texture_, bool depth_and_stencil, int antialias, int surface, int color_output){
	OpenGLTexture* texture = dynamic_cast<OpenGLTexture*>(texture_);
	if (texture->target == GL_TEXTURE_2D){
		OpenGLTexture2D* texture2D = (OpenGLTexture2D*)texture;
		initFramebuffer(texture2D->width, texture2D->height, depth_and_stencil);
		CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture2D->texture, 0));
		buffer_width = texture2D->width;
		buffer_height = texture2D->height;
		rendering_to_texture = true;
		must_sync_viewport = true;
	}else{
		jfl_notimpl;
	}
}

void OpenGLContext3D::setSamplerStateAt(int sampler, Wrap wrap, TextureFilter texture_filter, MipFilter mip_filter){
	auto& sampler_state = samplers[sampler];

	switch (wrap){
	case Wrap::Clamp:{
		sampler_state.wrap_u = GL_CLAMP_TO_EDGE;
		sampler_state.wrap_v = GL_CLAMP_TO_EDGE;
	}break;
	case Wrap::ClampURepeatV: {
		sampler_state.wrap_u = GL_CLAMP_TO_EDGE;
		sampler_state.wrap_v = GL_REPEAT;
	}break;
	case Wrap::RepeatUClampV: {
		sampler_state.wrap_u = GL_REPEAT;
		sampler_state.wrap_v = GL_CLAMP_TO_EDGE;
	}break;
	case Wrap::Repeat: {
		sampler_state.wrap_u = GL_REPEAT;
		sampler_state.wrap_v = GL_REPEAT;
	}break;
	default:{
		jfl_unreachable;
	}break;
	}

	switch (texture_filter){
	case TextureFilter::Linear:{
		sampler_state.magfilter = GL_LINEAR;
		switch (mip_filter){
		case MipFilter::None:{
			sampler_state.minfilter = GL_LINEAR;
		}break;
		case MipFilter::Nearest:{
			sampler_state.minfilter = GL_LINEAR_MIPMAP_NEAREST;
		}break;
		case MipFilter::Linear:{
			sampler_state.minfilter = GL_LINEAR_MIPMAP_LINEAR;
		}break;
		default:{
			jfl_unreachable;
		}break;
		}
	}break;
	case TextureFilter::Nearest:{
		sampler_state.magfilter = GL_NEAREST;
		switch (mip_filter) {
		case MipFilter::None: {
			sampler_state.minfilter = GL_NEAREST;
		}break;
		case MipFilter::Nearest: {
			sampler_state.minfilter = GL_NEAREST_MIPMAP_NEAREST;
		}break;
		case MipFilter::Linear: {
			sampler_state.minfilter = GL_NEAREST_MIPMAP_LINEAR;
		}break;
		default: {
			jfl_unreachable;
		}break;
		}
	}break;
	default:{
		jfl_notimpl;
	}break;
	}
	
}

void OpenGLContext3D::setScissor(int x, int y, int width, int height){
	if (!gl_scissor){
		CHECK(glEnable(GL_SCISSOR_TEST));
		gl_scissor = true;
	}
	scissor_x = x;
	scissor_y = y;
	scissor_width = width;
	scissor_height = height;
	must_sync_viewport = true;
}

void OpenGLContext3D::unsetScissor() {
	if(gl_scissor){
		CHECK(glDisable(GL_SCISSOR_TEST));
		gl_scissor = false;
	}
}

void OpenGLContext3D::setStencilReference(uint32_t ref, uint32_t read_mask, uint32_t write_mask){
	stencil.touched |= stencil.ref ^ ref;
	stencil.touched |= stencil.read_mask ^ read_mask;
	stencil.touched |= stencil.write_mask ^ write_mask;
	stencil.ref = ref;
	stencil.read_mask = read_mask;
	stencil.write_mask = write_mask;
}

void OpenGLContext3D::setStencilForFace(StencilData::StencilFace& face, CompareMode compare, StencilAction pass, StencilAction depthFail, StencilAction fail){
	GLenum func = ToGL(compare);
	GLenum op_stencil_fail = ToGL(fail);
	GLenum op_depth_fail = ToGL(fail);
	GLenum op_depth_pass = ToGL(fail);
	
	stencil.touched |= (op_stencil_fail ^ face.op_stencil_fail);
	stencil.touched |= (op_depth_fail ^ face.op_depth_fail);
	stencil.touched |= (op_depth_pass ^ face.op_depth_pass);
	stencil.touched |= (func ^ face.func);
	face.func = func;
	face.op_stencil_fail = op_depth_fail;
	face.op_depth_pass = op_depth_pass;
	face.op_stencil_fail = op_stencil_fail;
}

void OpenGLContext3D::setStencilActions(TriangleFace face, CompareMode compare, StencilAction pass, StencilAction depthFail, StencilAction fail){
	switch (face){
	case TriangleFace::None: {
		// ignore
	}break;
	case TriangleFace::Back: {
		setStencilForFace(stencil.back, compare, pass, depthFail, fail);
	}break;
	case TriangleFace::Front: {
		setStencilForFace(stencil.front, compare, pass, depthFail, fail);
	}break;
	case TriangleFace::FrontAndBack: {
		setStencilForFace(stencil.front, compare, pass, depthFail, fail);
		setStencilForFace(stencil.back, compare, pass, depthFail, fail);
	}break;
	}
}

void OpenGLContext3D::setTextureAt(int sampler, TextureBase* texture_){
	if ( texture_ ){
		OpenGLTexture* texture = dynamic_cast<OpenGLTexture*>(texture_);
		samplers[sampler].bound_texture = texture;
	}else{
		samplers[sampler].bound_texture = nullptr;
	}
}

void OpenGLContext3D::setVertexBufferAt(int index, VertexBuffer* buffer, int buffer_offset, BufferFormat format){
	OpenGLVertexBuffer* vbo = (OpenGLVertexBuffer*)buffer;
	if ( buffer ){
		CHECK(glEnableVertexAttribArray(index));
		GLenum type;
		GLint size;
		GLboolean normalized = false;
		switch ( format ){
		case BufferFormat::Bytes4:{
			size = 4;
			type = GL_UNSIGNED_BYTE;
			normalized = true;
		}break;
		case BufferFormat::Float1:{
			size = 1;
			type = GL_FLOAT;
		}break;
		case BufferFormat::Float2: {
			size = 2;
			type = GL_FLOAT;
		}break;
		case BufferFormat::Float3: {
			size = 3;
			type = GL_FLOAT;
		}break;
		case BufferFormat::Float4: {
			size = 4;
			type = GL_FLOAT;
		}break;
		default:{
			jfl_unreachable;
		}break;
		}
		CHECK(glBindBuffer(GL_ARRAY_BUFFER, vbo->buffer));
		CHECK(glVertexAttribPointer(index, size, type, normalized, vbo->bytesPerVertex, (const void*)(buffer_offset * 4)));
	}else{
		CHECK(glDisableVertexAttribArray(index));
	}
}

void OpenGLContext3D::checkError(){
	GLenum err = glGetError();
	if ( err != GL_NO_ERROR ){
		char buffer[256];
		sprintf(buffer, "GL error: %x", err);
		raise_message(buffer);
	}
}

}


namespace jfl::render{

Context3D* CreateContext3DImpl(){
	return new opengl::OpenGLContext3D();
}

}
