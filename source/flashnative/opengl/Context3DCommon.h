#pragma once
#include <jfl/types.h>
#include <jfl/error.h>
#include <jfl/render/OpenGL.h>

#if defined(JFL_USES_GLFW)
	#include <GLFW/glfw3.h>
#endif

#define OVERRIDE_GL_CHECK 0

#if defined(OVERRIDE_GL_CHECK) 
#define CHECK(fn) \
		do { fn; if ( OVERRIDE_GL_CHECK ) { owner->checkError(); } } while (false)
#else
#define CHECK(fn) \
		do { fn; if ( owner->error_checking ) { owner->checkError(); } } while(false)
#endif

namespace jfl::render::opengl {

struct BinaryDataParser {
public:
	BinaryDataParser(const uint8_t* ptr, size_t size) :
		ptr(ptr),
		end(ptr + size) {
	}

	uint8_t read_u8() {
		require(1);
		uint8_t byte = *ptr;
		ptr++;
		return byte;
	}

	uint16_t read_u16() {
		require(2);
		uint16_t value;
		memcpy(&value, ptr, sizeof(value));
		ptr += 2;
		return value;
	}

	std::string_view read_utf() {
		uint16_t len = read_u16();
		require(len);
		std::string_view ret((const char*)ptr, len);
		ptr += len;
		return ret;
	}

	const uint8_t* get_ptr() {
		return ptr;
	}

	void require(size_t count) {
		if (!check(count)) {
			raise_message("unexpected data end");
		}
	}

	bool check(size_t count) {
		if ((size_t)(end - ptr) < count) {
			return false;
		}
		return true;
	}
private:
	const uint8_t* ptr;
	const uint8_t* end;
};


class OpenGLContext3D;
class OpenGLResource;
class OpenGLVertexBuffer;
class OpenGLIndexBuffer;
class OpenGLProgram;
class OpenGLTexture;
class OpenGLTexture2D;
struct SamplerState;


}

