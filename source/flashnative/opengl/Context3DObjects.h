#pragma once
#include <jfl/render/Context3D.h>
#include <jfl/app/application.h>
#include <jfl/render/OpenGL.h>
#include <jfl/defer.h>

#include "Context3DCommon.h"
#include "Context3DEnums.h"

namespace jfl::render::opengl {

class OpenGLContext3D;

struct SamplerState {
	GLenum minfilter = GL_NEAREST_MIPMAP_LINEAR;
	GLenum magfilter = GL_LINEAR;
	GLenum wrap_u = GL_REPEAT;
	GLenum wrap_v = GL_REPEAT;
	OpenGLTexture* bound_texture = nullptr;
	GLuint gl_texture_2d = 0;
	GLuint gl_texture_cube = 0;
};

class OpenGLResource {
public:
	OpenGLResource(OpenGLContext3D* owner);
protected:
	OpenGLContext3D* owner;
};

class OpenGLVertexBuffer : public OpenGLResource, public VertexBuffer {
public:
	OpenGLVertexBuffer(OpenGLContext3D* owner, int numVertices, int dataPerVertex, BufferUsage usage);

	void upload(const void* data, int firstVertex, int numVertices);
	void dispose();
	size_t getBytesPerVertex();
private:
	GLuint buffer;
	size_t totalVertices;
	size_t bytesPerVertex;

	friend class OpenGLContext3D;
};

class OpenGLIndexBuffer : public OpenGLResource, public IndexBuffer {
public:
	OpenGLIndexBuffer(OpenGLContext3D* owner, int numIndices, BufferUsage usage);
	void dispose();
	void upload(const void* data, int firstIndex, int numIndices);

private:
	GLuint buffer;
	size_t totalIndices;

	friend class OpenGLContext3D;
};

class OpenGLProgram : public OpenGLResource, public Program {
public:
	OpenGLProgram(OpenGLContext3D* owner);
	void upload(array_view<uint8_t> vertex_data, array_view<uint8_t> fragment_data);
	GLuint uploadShader(GLenum type, array_view<uint8_t> data);
	void link(GLuint vertex, GLuint fragment);
	void dispose();
private:
	GLuint name = 0;

	struct Attrib {
		uint16_t location;
		std::string_view name;
	};
	std::vector<Attrib> attribs;

	struct Uniform {
		GLenum shader_type;

		std::string_view name;
		// opengl location of the uniform (GL)
		uint16_t location;
		// first index of the constant data (Context3D)
		uint16_t first_index;
		// how many values it uses from the constant data 
		uint16_t used_indices;

		std::unique_ptr<float[]> cached_data;

		enum class Type {
			Float,
			Float2,
			Float3,
			Float4,
			Mat2,
			Mat3,
			Mat4
		} type;
	};
	std::vector<Uniform> uniforms;

	struct Sampler {
		uint16_t location;
		std::string_view name;
	};
	std::vector<Sampler> samplers;

	friend class OpenGLContext3D;
};

class OpenGLTexture : public OpenGLResource, virtual public TextureBase {
public:
	OpenGLTexture(OpenGLContext3D* owner, GLenum target);
	void bind(int sampler);
	void set_sampler_state(const SamplerState& state);
	friend class OpenGLContext3D;
protected:
	GLenum target;
	GLuint texture = 0;
	GLenum gl_minfilter = GL_NEAREST_MIPMAP_LINEAR;
	GLenum gl_magfilter = GL_LINEAR;
	GLenum gl_wrap_u = GL_REPEAT;
	GLenum gl_wrap_v = GL_REPEAT;
};

class OpenGLTexture2D : public OpenGLTexture, virtual public Texture2D {
public:
	OpenGLTexture2D(OpenGLContext3D* owner);
	void dispose();
	void allocate(int width, int height, TextureFormat format, bool optimizeForRender, bool mipmaps);
	void upload(int level, int width, int height, bool has_alpha, int stride, uint8_t* data);
	void uploadPremultiply(int level, int width, int height, uint8_t* data);
	void uploadExternalImage(int level, int width, int height, int imageID);
private:
	int width, height;
	TextureFormat format;
	GLenum gl_format;
	bool mipmaps;

	friend class OpenGLContext3D;
};

}

