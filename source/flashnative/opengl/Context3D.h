#pragma once
#include <jfl/render/Context3D.h>
#include <jfl/app/application.h>
#include <jfl/render/OpenGL.h>
#include <jfl/defer.h>
#include <jfl/types.h>

#include "Context3DCommon.h"
#include "Context3DEnums.h"
#include "Context3DObjects.h"

namespace jfl::render::opengl {

class OpenGLContext3D : public Context3D {
public:
	OpenGLContext3D();
	void setWindowSize(int width, int height);
	void setErrorChecking(bool value);
	bool getErrorChecking();
	jfl::String getDriverInfo();
	void configureBackBuffer(int width, int height, bool depthAndStencil);
	VertexBuffer* createVertexBuffer(int numVertices, int dataPerVertex, BufferUsage usage);
	IndexBuffer* createIndexBuffer(int numIndices, BufferUsage usage);
	Program* createProgram();
	Texture2D* createTexture2D(int width, int height, TextureFormat format, bool optimizeForRender, bool mipmaps);
	int getBackBufferWidth();
	int getBackBufferHeight();
	void clear(double red, double green, double blue, double alpha, double depth, uint32_t stencil, ClearMask mask);
	void setDepthTest(bool write, CompareMode compare);
	void syncProgram();
	void syncState();
	void syncSamplers();
	void syncStencil();
	void drawTriangles(IndexBuffer* ibo_, int firstIndex, int numTriangles);
	void present();
	void setBlendFactors(BlendFactor source, BlendFactor dest);
	void setColorMask(bool red, bool green, bool blue, bool alpha);
	void setCulling(TriangleFace face);
	void setProgram(Program* program_);
	void setProgramConstant(ProgramType type, int reg, const double data[4]);
	void setRenderToBackBuffer();
	void setRenderToTexture(TextureBase* texture, bool depth_and_stencil, int antialias, int surface, int color_output);
	void setSamplerStateAt(int sampler, Wrap wrap, TextureFilter texture_filter, MipFilter mip_filter);
	void setScissor(int x, int y, int width, int height);
	void unsetScissor();
	void setStencilReference(uint32_t ref, uint32_t read_mask, uint32_t write_mask);
	void setStencilActions(TriangleFace face, CompareMode compare, StencilAction pass, StencilAction depthFail, StencilAction fail);
	void setTextureAt(int sampler, TextureBase* texture_);
	void setVertexBufferAt(int index, VertexBuffer* buffer, int buffer_offset, BufferFormat format);
	void checkError();

	void hintFramebuffer(int width, int height);
	void initFramebuffer(int min_width, int min_height, bool depth_and_stencil);

	void changeActiveTexture(int sampler);

	bool must_sync_viewport = false;

	int window_width, window_height;
	int buffer_width, buffer_height;
	int backbuffer_width, backbuffer_height;

	int framebuffer_requested_width = 0;
	int framebuffer_requested_height = 0;

	GLuint framebuffer = 0;
	GLuint renderbuffers[2] = {0, 0};
	int framebuffer_width = 0;
	int framebuffer_height = 0;

	bool rendering_to_texture = false;

	friend class OpenGLTexture;

	OpenGLProgram* active_program = nullptr;

	bool gl_depth_mask = false;
	bool gl_depth_enabled = false;
	GLenum gl_depth_func = GL_ALWAYS;
	bool gl_blend_enabled = false;
	GLenum gl_blend_src = GL_ONE, gl_blend_dst = GL_ZERO;
	uint32_t gl_color_mask = 0b1111;
	bool gl_scissor = false;
	int scissor_x, scissor_y, scissor_width, scissor_height;

	static const size_t MaxSamplers = 8;
	SamplerState samplers[MaxSamplers];
	GLenum gl_active_unit = 0;


	struct StencilData{
		bool gl_enabled = false;
		uint32_t touched = 0;

		uint32_t read_mask = 0xff, write_mask = 0xff;
		uint32_t ref = 0;

		uint32_t gl_write_mask = 0xffffffffu;
		
		struct StencilFace{
			GLenum func = GL_ALWAYS;
			GLenum op_stencil_fail = GL_KEEP;
			GLenum op_depth_fail = GL_KEEP;
			GLenum op_depth_pass = GL_KEEP;

			GLenum gl_func = GL_ALWAYS;
			GLenum gl_op_stencil_fail = GL_ALWAYS;
			GLenum gl_op_depth_fail = GL_ALWAYS;
			GLenum gl_op_depth_pass = GL_ALWAYS;

			uint32_t gl_read_mask = 0xffffffffu;
			uint32_t gl_ref = 0xffffffffu;

			bool requiresStencilTest();
			bool needsSync(uint32_t read_mask, uint32_t ref);
			void sync(OpenGLContext3D* owner, GLenum face, uint32_t read_mask, uint32_t ref);
		} front, back;
	} stencil;

	void setStencilForFace(StencilData::StencilFace& face, CompareMode compare, StencilAction pass, StencilAction depthFail, StencilAction fail);

	static const size_t MaxVertexConstants = 128;
	static const size_t MaxFragmentConstants = 28;
	float vertex_constants[MaxVertexConstants * 4];
	float fragment_constants[MaxFragmentConstants * 4];

	jfl::String driver = nullptr;
	jfl::gc_ptr<jfl::StringObject> driver_str;

#if defined(OVERRIDE_GL_CHECK)
	bool error_checking = OVERRIDE_GL_CHECK;
#else
	bool error_checking = false;
#endif
	OpenGLContext3D* owner = this;

#if defined(GL_VAO_REQUIRED)
	GLuint vao;
#endif
};

}

