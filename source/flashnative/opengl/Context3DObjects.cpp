#include "Context3D.h"
#include <jfl/common.h>

#if __INTELLISENSE__
	#define JFL_PLATFORM_EM
#endif

#if defined(JFL_PLATFORM_EM)
	#include <em/WebGL.h>
#endif

namespace jfl::render::opengl {

OpenGLResource::OpenGLResource(OpenGLContext3D* owner) :
	owner(owner) {
}

size_t OpenGLVertexBuffer::getBytesPerVertex() {
	return bytesPerVertex;
}

void OpenGLVertexBuffer::dispose() {
	if (buffer) {
		CHECK(glDeleteBuffers(1, &buffer));
		buffer = 0;
	}
}

OpenGLProgram::OpenGLProgram(OpenGLContext3D* owner) :
	OpenGLResource(owner) {
}

void OpenGLProgram::dispose(){
	if (name) {
		CHECK(glDeleteProgram(name));
		name = 0;
	}
}

OpenGLTexture::OpenGLTexture(OpenGLContext3D* owner, GLenum target) :
	OpenGLResource(owner),
	target(target)
{
}

void OpenGLTexture::bind(int sampler){
	SamplerState& state = owner->samplers[sampler];
	bool need_rebind = false;
	if ( target == GL_TEXTURE_2D ){
		if (state.gl_texture_2d != texture){
			need_rebind = true;
		}
	}else{
		if (state.gl_texture_cube != texture) {
			need_rebind = true;
		}
	}
	if (need_rebind){
		owner->changeActiveTexture(sampler);
		CHECK(glBindTexture(target, texture));
	}
}

void OpenGLTexture::set_sampler_state(const SamplerState& state){
	if (
		state.minfilter != gl_minfilter
		||
		state.magfilter != gl_magfilter
		||
		state.wrap_u != gl_wrap_u
		||
		state.wrap_v != gl_wrap_v
	){
		bind(owner->gl_active_unit);
		if (state.minfilter != gl_minfilter){
			CHECK(glTexParameteri(target, GL_TEXTURE_MIN_FILTER, state.minfilter));
			gl_minfilter = state.minfilter;
		}
		if (state.magfilter != gl_magfilter) {
			CHECK(glTexParameteri(target, GL_TEXTURE_MAG_FILTER, state.magfilter));
			gl_magfilter = state.magfilter;
		}
		if (state.wrap_u != gl_wrap_u) {
			CHECK(glTexParameteri(target, GL_TEXTURE_WRAP_S, state.wrap_u));
			gl_wrap_u = state.wrap_u;
		}
		if (state.wrap_v != gl_wrap_v) {
			CHECK(glTexParameteri(target, GL_TEXTURE_WRAP_T, state.wrap_v));
			gl_wrap_v = state.wrap_v;
		}
	}
}

OpenGLTexture2D::OpenGLTexture2D(OpenGLContext3D* owner) :
	OpenGLTexture(owner, GL_TEXTURE_2D) 
{
}

OpenGLVertexBuffer::OpenGLVertexBuffer(OpenGLContext3D* owner, int numVertices, int dataPerVertex, BufferUsage usage) :
	OpenGLResource(owner) {
	bytesPerVertex = dataPerVertex * 4;
	CHECK(glGenBuffers(1, &buffer));
	CHECK(glBindBuffer(GL_ARRAY_BUFFER, buffer));
	CHECK(glBufferData(GL_ARRAY_BUFFER, bytesPerVertex * numVertices, nullptr, ToGL(usage)));
}

void OpenGLVertexBuffer::upload(const void* data, int firstVertex, int numVertices) {
	CHECK(glBindBuffer(GL_ARRAY_BUFFER, buffer));
	CHECK(glBufferSubData(GL_ARRAY_BUFFER, firstVertex * bytesPerVertex, numVertices * bytesPerVertex, data));
}

OpenGLIndexBuffer::OpenGLIndexBuffer(OpenGLContext3D* owner, int numIndices, BufferUsage usage) :
	OpenGLResource(owner) {
	CHECK(glGenBuffers(1, &buffer));
	CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer));
	CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(uint16_t), nullptr, ToGL(usage)));
}

void OpenGLIndexBuffer::dispose() {
	if (buffer) {
		CHECK(glDeleteBuffers(1, &buffer));
		buffer = 0;
	}
}

void OpenGLIndexBuffer::upload(const void* data, int firstIndex, int numIndices) {
	CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer));
	CHECK(glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, firstIndex * sizeof(uint16_t), numIndices * sizeof(uint16_t), data));
}

void OpenGLProgram::upload(array_view<uint8_t> vertex_data, array_view<uint8_t> fragment_data) {
	GLuint vertex_shader = uploadShader(GL_VERTEX_SHADER, vertex_data);
	auto free_vs = defer([&]() {
		CHECK(glDeleteShader(vertex_shader));
	});
	GLuint fragment_shader = uploadShader(GL_FRAGMENT_SHADER, fragment_data);
	auto free_fs = defer([&]() {
		CHECK(glDeleteShader(fragment_shader));
	});
	link(vertex_shader, fragment_shader);
}

#if defined(JFL_OPENGL_GLES) || defined(JFL_OPENGL_WEBGL)
static const char GLESVertexShaderPrefix[] = "precision highp float;";
static const char GLESFragmentShaderPrefix[] = "precision mediump float;";
#endif

GLuint OpenGLProgram::uploadShader(GLenum shader_type, array_view<uint8_t> data) {
	BinaryDataParser parser(data.data(), data.size());
	const uint8_t* data_ptr = nullptr;
	size_t data_length = 0;
	while (true) {
		if (!parser.check(1)) {
			break;
		}
		switch (parser.read_u8()) {
		case 0x1: {
			// attribute
			uint8_t type = parser.read_u8();
			uint16_t stream = parser.read_u16();
			std::string_view name = parser.read_utf();
			attribs.push_back({ stream, name });
		}break;
		case 0x2: {
			// sampler
			uint8_t type = parser.read_u8();
			uint16_t stream = parser.read_u16();
			std::string_view name = parser.read_utf();
			samplers.push_back({ stream, name });
		}break;
		case 0x3: {
			// uniform
			uint8_t type = parser.read_u8();
			uint16_t stream = parser.read_u16();
			std::string_view name = parser.read_utf();

			Uniform::Type uniform_type;
			uint16_t used_count = 0;

			switch (type) {
			case 1: {
				uniform_type = Uniform::Type::Float;
				used_count = 1;
			}break;
			case 2: {
				uniform_type = Uniform::Type::Float2;
				used_count = 2;
			}break;
			case 3: {
				uniform_type = Uniform::Type::Float3;
				used_count = 3;
			}break;
			case 4: {
				uniform_type = Uniform::Type::Float4;
				used_count = 4;
			}break;
			case 22: {
				uniform_type = Uniform::Type::Mat2;
				// uses two rows
				used_count = 8;
			}break;
			case 23: {
				uniform_type = Uniform::Type::Mat3;
				// uses three rows
				used_count = 12;
			}break;
			case 24: {
				uniform_type = Uniform::Type::Mat4;
				used_count = 16;
			}break;
			default: {
				jfl::raise_message("invalid uniform type");
			}break;
			}

			std::unique_ptr<float[]> cached_values = std::make_unique<float[]>((size_t)used_count);
			memset(cached_values.get(), 0, sizeof(float) * used_count);

			uniforms.push_back(Uniform{
				shader_type,
				name,
				0,
				stream,
				used_count,
				std::move(cached_values),
				uniform_type
			});
		}break;
		case 0xff: {
			data_length = parser.read_u16();
			data_ptr = parser.get_ptr();
		}break;
		}
	}

	if (!data_ptr) {
		raise_message("source tag is missing");
	}

	GLuint name;
	CHECK(name = glCreateShader(shader_type));
#if defined(JFL_OPENGL_GLES) || defined(JFL_OPENGL_WEBGL)
	const GLchar* strings[] = {
		(shader_type == GL_VERTEX_SHADER) ? GLESVertexShaderPrefix : GLESFragmentShaderPrefix,
		(const GLchar*)data_ptr
	};
	GLint lengths[] = {
		(GLint)(
			(shader_type == GL_VERTEX_SHADER)
			?
			sizeof(GLESVertexShaderPrefix) - 1
			:
			sizeof(GLESFragmentShaderPrefix) - 1
		),
		(GLint)data_length
	};
#else
	const GLchar* strings[] = {
		(const GLchar*)data_ptr
	};
	GLint lengths[] = {
		(GLint)data_length
	};
#endif
	CHECK(glShaderSource(name, sizeof(lengths) / sizeof(*lengths), strings, lengths));
	CHECK(glCompileShader(name));
	GLint status = 0;
	CHECK(glGetShaderiv(name, GL_COMPILE_STATUS, &status));
	if (!status || owner->error_checking) {
		GLint logLength = 0;
		CHECK(glGetShaderiv(name, GL_INFO_LOG_LENGTH, &logLength));
		GLsizei maxLength = logLength;
		GLsizei length = 0;
		std::string log;
		log.resize(maxLength);
		CHECK(glGetShaderInfoLog(name, logLength, &length, log.data()));
		log.resize(length);
		if (!status) {
			jfl::raise_message(log);
		} else if (length > 0) {
			std::cerr << "Shader compilation warning:\n\t" << log << std::endl;
		}
	}
	return name;
}

void OpenGLProgram::link(GLuint vertex, GLuint fragment) {
	CHECK(name = glCreateProgram());
	CHECK(glAttachShader(name, vertex));
	CHECK(glAttachShader(name, fragment));

	for (auto& attrib : attribs) {
		CHECK(glBindAttribLocation(name, attrib.location, std::string(attrib.name).c_str()));
	}

	CHECK(glLinkProgram(name));

	GLint status = 0;
	CHECK(glGetProgramiv(name, GL_LINK_STATUS, &status));
	if (!status || owner->error_checking) {
		GLint logLength = 0;
		CHECK(glGetProgramiv(name, GL_INFO_LOG_LENGTH, &logLength));
		GLsizei maxLength = logLength;
		GLsizei length = 0;
		std::string log;
		log.resize(maxLength);
		CHECK(glGetProgramInfoLog(name, logLength, &length, log.data()));
		log.resize(length);
		if (!status) {
			jfl::raise_message(log);
		} else if (length > 0) {
			std::cerr << "Shader link warning:\n\t" << log << std::endl;
		}
	}


	CHECK(glUseProgram(name));
	for (auto& sampler : samplers) {
		CHECK(glUniform1i(glGetUniformLocation(name, std::string(sampler.name).c_str()), sampler.location));
	}
	for (auto& uniform : uniforms) {
		CHECK(uniform.location = glGetUniformLocation(name, std::string(uniform.name).c_str()));
	}
}

void OpenGLTexture2D::allocate(int width, int height, TextureFormat format, bool optimizeForRender, bool mipmaps) {
	this->width = width;
	this->height = height;
	this->format = format;
	this->mipmaps = mipmaps;

	if ( optimizeForRender ){
		owner->hintFramebuffer(width, height);
	}

	CHECK(glGenTextures(1, &texture));
	bind(owner->gl_active_unit);

	switch (format) {
	case TextureFormat::BGR: {
		gl_format = GL_RGB;
	}break;
	case TextureFormat::BGRA: {
		gl_format = GL_RGBA;
	}break;
	default: {
		jfl_unreachable;
	}break;
	}

	CHECK(glTexImage2D(
		GL_TEXTURE_2D,
		0,
		gl_format,
		width,
		height,
		0,
		gl_format,
		GL_UNSIGNED_BYTE,
		nullptr
	));
}

#if defined(_MSC_VER)
	#define RESTRICT __restrict 
#else
	#define RESTRICT __restrict__
#endif

static uint8_t mult_byte(uint8_t x, uint8_t y){
	return (((uint32_t)x) * ((uint32_t)y) + 127) / 255;
}


void OpenGLTexture2D::uploadExternalImage(int level, int width, int height, int imageID) {
#if defined(JFL_PLATFORM_EM)
	bind(owner->gl_active_unit);

	CHECK(em::webgl_tex2d_image(
		GL_TEXTURE_2D,
		level,
		0,
		0,
		gl_format,
		GL_UNSIGNED_BYTE,
		imageID
	));
#else
	jfl_notimpl;
#endif
}

void OpenGLTexture2D::uploadPremultiply(int level, int width, int height, uint8_t* RESTRICT data){
	bind(owner->gl_active_unit);

	uint8_t* buffer = GetScratchSpace(width * height * 4);

	uint32_t bytes_per_row = 4 * width;

	uint32_t processed_pixels = height * width;
	uint8_t* source = data;

	for (uint32_t i = 0; i < processed_pixels; i++){
		uint8_t r = source[i * 4 + 0];
		uint8_t g = source[i * 4 + 1];
		uint8_t b = source[i * 4 + 2];
		uint8_t a = source[i * 4 + 3];

		buffer[i * 4 + 0] = mult_byte(r, a);
		buffer[i * 4 + 1] = mult_byte(g, a);
		buffer[i * 4 + 2] = mult_byte(b, a);
		buffer[i * 4 + 3] = a;
	}

	CHECK(glTexSubImage2D(
		GL_TEXTURE_2D,
		level,
		0,
		0,
		width,
		height,
		gl_format,
		GL_UNSIGNED_BYTE,
		buffer
	));
}

void OpenGLTexture2D::upload(int level, int width, int height, bool has_alpha, int stride, uint8_t* data) {
	bind(owner->gl_active_unit);
	if (has_alpha) {
		CHECK(glTexSubImage2D(
			GL_TEXTURE_2D,
			level,
			0,
			0,
			width,
			height,
			gl_format,
			GL_UNSIGNED_BYTE,
			data
		));
	}else {
		uint8_t* buffer = GetScratchSpace(width * height * 4);

		int32_t bytes_per_row = (3 * width + 3) & ~3;
		uint8_t* source = data;

		for (int32_t y = 0; y < height; y++) {
			for (int32_t x = 0; x < width; x++) {
				uint8_t* source_row = source + bytes_per_row * y;
				uint8_t r = source_row[x * 3 + 0];
				uint8_t g = source_row[x * 3 + 1];
				uint8_t b = source_row[x * 3 + 2];
				uint8_t a = 255;

				uint8_t* target_row = buffer + y * width * 4;

				target_row[x * 4 + 0] = r;
				target_row[x * 4 + 1] = g;
				target_row[x * 4 + 2] = b;
				target_row[x * 4 + 3] = a;
			}
		}

		CHECK(glTexSubImage2D(
			GL_TEXTURE_2D,
			level,
			0,
			0,
			width,
			height,
			gl_format,
			GL_UNSIGNED_BYTE,
			buffer
		));
	}
}

void OpenGLTexture2D::dispose() {
	if (texture) {
		CHECK(glDeleteTextures(1, &texture));
		texture = 0;
	}
}

}