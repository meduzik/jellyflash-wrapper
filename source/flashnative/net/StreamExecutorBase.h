#pragma once

#include <jfl/net/StreamExecutor.h>

namespace jfl {

using fl::flash::net::URLStream;

class StreamExecutorBase: public IStreamExecutor{
public:
	StreamExecutorBase(URLStream* stream);
	~StreamExecutorBase();

	void start(std::shared_ptr<IStreamExecutor> own_ptr);
protected:
	// executes in async context
	virtual void process() = 0;
	
	// schedules process for execution
	// process is only ever executed once
	// if process execution starts and schedule is invoked,
	// process guaranteeed to execute once more
	void schedule();

	// Following functions communicate events on the stream to the 
	// URLStream object. They are safe to call when is_detached is true
	// and will not actually report any evens.
	// Events are always reported in the same order they were posted.
	void report_open();
	void report_progress(int total_bytes, int loaded_bytes, const uint8_t* buffer, size_t count);
	void report_error(int id, std::string message);
	void report_complete();

	// checks if stream is detached
	// the response is authoritative only if VM lock
	// is being held
	// otherwise the function is safe to call but 
	// should only be used as a hint (true response
	// means you can cancel further processing)
	bool is_detached();

	// detaches from the URLStream
	// no new events will be reported
	// stops the stream loading if possible
	// must be invoked within VM context
	void detach();
private:
	void schedule_report_progress();
	void report_progress();

	std::shared_ptr<IStreamExecutor> self;

	gc_ptr<URLStream> stream;

	enum class State{
		Init,
		Opened,
		Completed,
		Errored
	} state{State::Init};

	int bytes_total = 0, bytes_loaded = 0;
	bool opened = false, completed = false, error = false, progress = false;
	jfl::um_vec<uint8_t> buffer;
	jfl::um_string error_message;
	int error_code = 0;
	
	bool report_scheduled = false, progress_reporting = false;

	std::atomic_bool detached{false};

	std::mutex data_transfer_lock;
};

}

