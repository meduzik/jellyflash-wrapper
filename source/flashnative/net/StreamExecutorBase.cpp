#include "StreamExecutorBase.h"
#include <jfl/app/Loop.h>
#include <fl/flash/net/URLStream.h>
#include <fl/flash/events/ProgressEvent.h>
#include <fl/flash/events/IOErrorEvent.h>

namespace jfl{
using fl::flash::events::ProgressEvent;
using fl::flash::events::IOErrorEvent;


static int count = 0;

StreamExecutorBase::StreamExecutorBase(URLStream* stream):
	stream(stream)
{
}

StreamExecutorBase::~StreamExecutorBase(){
}

bool StreamExecutorBase::is_detached(){
	return detached.load();
}

void StreamExecutorBase::detach(){
	std::lock_guard lock(data_transfer_lock);
	detached.store(true);
	if ( stream ){
		stream->connected = false;
		stream->executor.reset();
	}
	self.reset();
}

void StreamExecutorBase::start(std::shared_ptr<IStreamExecutor> own_ptr){
	self = own_ptr;
	RunAsyncTask([&, own_ptr=std::move(own_ptr)](){
		process();
	});
}

void StreamExecutorBase::report_progress(){
	int bytes_total, bytes_loaded;
	bool opened = false, completed = false, error = false, progress = false;
	int error_code;
	std::string error_message;

	if ( is_detached() ){
		// do not report anything if we are detached
		report_scheduled = false;
		return;
	}

	{
		std::lock_guard lock(data_transfer_lock);
		std::swap(opened, this->opened);
		std::swap(completed, this->completed);
		std::swap(progress, this->progress);
		std::swap(error, this->error);
		if ( error ){
			error_code = this->error_code;
			error_message = std::move(this->error_message);
		}
		if ( progress ){
			size_t buffer_size = buffer.size();
			if (buffer_size > 0) {
				size_t old_size = stream->buffer.size();
				stream->buffer.resize(old_size + buffer_size);
				std::memcpy(stream->buffer.data() + old_size, buffer.data(), buffer_size);
				buffer.clear();
			}
			bytes_total = this->bytes_total;
			bytes_loaded = this->bytes_loaded;
		}
		// we copied everything from the stream object, reset the reporting
		// flag
		report_scheduled = false;
	}

	if ( opened ){
		assert(state == State::Init);
		state = State::Opened;
		stream->connected = true;
		fl::flash::events::Event* event = fl::flash::events::Event::jfl_New(
			fl::flash::events::Event::jfl_S_OPEN,
			false,
			false
		);
		fl::flash::events::EventDispatcher::dispatchEvent(
			(fl::flash::events::EventDispatcher*)stream.get(),
			(fl::flash::events::Event*)event
		);
	}

	if ( progress ){
		if ( state == State::Errored ){
			// its okay to report progress in error state
		}else{
			assert(state == State::Opened);
			if (is_detached()) {
				return;
			}
			fl::flash::events::ProgressEvent* event = fl::flash::events::ProgressEvent::jfl_New(
				fl::flash::events::ProgressEvent::jfl_S_PROGRESS,
				false,
				false,
				(Number)bytes_loaded,
				(Number)bytes_total
			);
			fl::flash::events::EventDispatcher::dispatchEvent(
				(fl::flash::events::EventDispatcher*)stream.get(),
				(fl::flash::events::Event*)event
			);
		}
	}

	if ( error ){
		if ( state == State::Completed || state == State::Errored ){
			// we ignore errors in finished states
		}else{
			if (is_detached()) {
				return;
			}
			fl::flash::events::IOErrorEvent* event = fl::flash::events::IOErrorEvent::jfl_New(
				fl::flash::events::IOErrorEvent::jfl_S_IO_ERROR,
				false,
				false,
				jfl::String::Make(error_message),
				error_code
			);
			fl::flash::events::EventDispatcher::dispatchEvent(
				(fl::flash::events::EventDispatcher*)stream.get(),
				(fl::flash::events::Event*)event
			);
			// after error event, we detach from the stream
			detach();
		}
	}

	if (completed) {
		if (state == State::Errored) {
			// we ignore completion events in error state
		} else {
			if (is_detached()) {
				return;
			}
			assert(state == State::Opened);
			fl::flash::events::Event* event = fl::flash::events::Event::jfl_New(
				fl::flash::events::Event::jfl_S_COMPLETE,
				false,
				false
			);
			fl::flash::events::EventDispatcher::dispatchEvent(
				(fl::flash::events::EventDispatcher*)stream.get(),
				(fl::flash::events::Event*)event
			);
			// after complete event, we detach from the stream
			detach();
		}
	}
}

void StreamExecutorBase::schedule_report_progress(){
	if ( !report_scheduled){
		report_scheduled = true;
		if ( !progress_reporting ){
			progress_reporting = true;
			PostMainLoopTask([&, own_ptr=self](){
				// spin until no more reporting is required
			do_reporting:
				report_progress();
				{
					std::lock_guard lock(data_transfer_lock);
					if (report_scheduled) {
						goto do_reporting;
					}
					progress_reporting = false;
				}
			});
		}
	}
}

void StreamExecutorBase::report_progress(int bytes_total, int bytes_loaded, const uint8_t* buffer, size_t count){
	std::lock_guard lock(data_transfer_lock);
	this->bytes_loaded = bytes_loaded;
	this->bytes_total = bytes_total;
	size_t old_size = this->buffer.size();
	this->buffer.resize(old_size + count);
	std::memcpy(this->buffer.data() + old_size, buffer, count);
	progress = true;
	schedule_report_progress();
}

void StreamExecutorBase::report_open(){
	std::lock_guard lock(data_transfer_lock);
	opened = true;
	schedule_report_progress();
}

void StreamExecutorBase::report_error(int id, std::string message){
	std::lock_guard lock(data_transfer_lock);
	error = true;
	error_code = id;
	error_message = std::move(message);
	schedule_report_progress();
}

void StreamExecutorBase::report_complete(){
	std::lock_guard lock(data_transfer_lock);
	completed = true;
	schedule_report_progress();
}


}