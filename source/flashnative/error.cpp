#include <jfl/common_impl.h>
#include <jfl/error.h>
#include <fl/Error.h>

namespace jfl{

[[noreturn]]
jfl_noinline
void raise_arg_underflow(size_t given, size_t expected){
	raise_message("not enough arguments");
}

[[noreturn]]
jfl_noinline
void raise_arg_overflow(size_t given, size_t expected){
	raise_message("too many arguments");
}

[[noreturn]]
jfl_noinline
void raise_cast_error(const Class* from_type, const Class* to_type){
	Arena arena;
	arena_string str(arena);
	str += "bad cast from ";
	str += from_type->name.to_string_view();
	str += " to ";
	str += to_type->name.to_string_view();
	raise_message(str);
}

[[noreturn]]
jfl_noinline
void raise_set_error(const Class* from_type, Any key){
	raise_message("set error");
}

[[noreturn]]
jfl_noinline
void raise_get_error(const Class* from_type, Any key){
	raise_message("get error");
}

[[noreturn]]
jfl_noinline
void raise_del_error(const Class* from_type, Any key){
	raise_message("del error");
}

[[noreturn]]
jfl_noinline
void raise_in_error(const Class* from_type, Any key){
	raise_message("in error");
}

[[noreturn]]
jfl_noinline
void raise_iter_error(const Class* type){
	raise_message("iter error");
}

[[noreturn]]
jfl_noinline
void raise_call_error(const Class* type){
	raise_message("call error");
}

[[noreturn]]
jfl_noinline
void raise_npe(const char* file, int line){
	Arena arena;
	arena_string str(arena);
	str += "npe @ ";
	str += file;
	str += " : ";
	str += jfl::int_to_String(line).to_string_view();
	raise_message(str);
}

[[noreturn]]
jfl_noinline
void raise_npe(){
	raise_message("npe");
}

[[noreturn]]
jfl_noinline
void raise_nullarg(){
	raise_message("arg not supposed to be null");
}

[[noreturn]]
jfl_noinline
void exc_throw(Any exc){
	std::cerr << "exc_throw with " << any_class_of(exc)->name.to_string_view() << std::endl;
	fl::Error* error = (fl::Error*)any_as_Class(exc, &fl::Error::jfl_Class);
	// it is better to call abort for now, as we don't ready to catch the exception anyway
	abort();
}

[[noreturn]]
jfl_noinline
void exc_resume(){
	// it is better to call abort for now, as we don't ready to catch the exception anyway
	abort();
}

[[noreturn]]
jfl_noinline
void raise_message(std::string_view message){
	std::cout << "raise_message: " << message << std::endl;
	fl::Error* err = fl::Error::jfl_New(jfl::String::Make(message), 42u);
	exc_throw((jfl::Object*)err);
}

[[noreturn]]
jfl_noinline
void raise_readonly(std::string_view property){
	Arena arena;
	arena_string message(arena);
	message += "attempt to set read only property ";
	message += property;
	raise_message(message);
}

[[noreturn]]
jfl_noinline
void raise_writeonly(std::string_view property){
	Arena arena;
	arena_string message(arena);
	message += "attempt to get write only property ";
	message += property;
	raise_message(message);
}

[[noreturn]]
jfl_noinline
void raise_notimpl(const char* location) {
	Arena arena;
	arena_string message(arena);
	message += "not implemented: ";
	message += location;
	raise_message(message);
}

[[noreturn]]
jfl_noinline
void raise_unreachable(const char* location) {
	Arena arena;
	arena_string message(arena);
	message += "unreachable: ";
	message += location;
	raise_message(message);
}

[[noreturn]]
jfl_noinline
void raise_system_error(int errorcode, jfl::String message){
	message.ensure_not_null();
	auto [ptr, len] = message.to_view();
	std::cout << "System error(" << errorcode << "): " << std::string_view((const char*)ptr, len) << std::endl;
	fl::Error* err = fl::Error::jfl_New(message, errorcode);
	exc_throw((jfl::Object*)err);
}

jfl_noinline
Any exc_catchpad() {
	jfl_notimpl;
}

}
