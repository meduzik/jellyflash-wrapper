#include <jfl/std.h>
#include <jfl/text/TextRendering.h>
#include <ft2build.h>
#include FT_FREETYPE_H

namespace jfl{

struct FontObject{
	FT_Face face;
	uint8_t* data;
	size_t data_len;
};

static FT_Library FTLibrary;
static um_map< um_string, FontObject*, std::less<> > FontObjects;
static um_map< um_string, um_unique_ptr<FontRequest>, std::less<> > FontRequests;

void FontRegister(std::string_view full_name, FontObject* font){
	FontObjects.insert({um_string(full_name), font});
	auto it = FontRequests.find(full_name);
	if (it != FontRequests.end()){
		it->second->register_font(font);
	}
}

FontRequest::FontRequest(um_string full_name, um_string font_name, bool bold, bool italic):
	full_name(std::move(full_name)),
	font_name(std::move(font_name)),
	bold(bold),
	italic(italic),
	refs(0),
	font(nullptr)
{
}

void FontRequest::register_font(FontObject* font){
	this->font = font;
}

void FontRequest::add_ref(){
	refs++;
}

void FontRequest::remove_ref(){
	// do not delete any font requests yet
	refs--;
}

FontReference FontReference::Get(const FontDescriptor& descriptor){
	Arena arena;
	arena_string full_name(descriptor.font_name, arena);
	if (descriptor.bold) {
		full_name += " Bold";
	}
	if (descriptor.italic) {
		full_name += " Italic";
	}
	auto it = FontRequests.find(std::string_view(full_name));
	if (it == FontRequests.end()){
		FontRequest* mem = (FontRequest*)unmanaged_allocate(sizeof(FontRequest));
		um_unique_ptr<FontRequest> request(new(mem)FontRequest(
			um_string(full_name),
			um_string(descriptor.font_name),
			descriptor.bold,
			descriptor.italic
		));
		{
			auto it = FontObjects.find(std::string_view(full_name));
			if (it != FontObjects.end()){
				mem->register_font(it->second);
			}
		}
		FontRequests.insert({um_string(full_name), std::move(request)});
		return mem;
	}
	return it->second.get();
}

static const char* GetFreeTypeErrorMessage(FT_Error err){
	#undef __FTERRORS_H__
	#define FT_ERRORDEF( e, v, s )  case e: return s;
	#define FT_ERROR_START_LIST     switch (err) {
	#define FT_ERROR_END_LIST       }
	#include FT_ERRORS_H
	return "(Unknown error)";
}

void TextSubsystem_Init(){
	FT_Error err = FT_Init_FreeType(&FTLibrary);
	if (err){
		std::cerr<<"FreeType init error: " << GetFreeTypeErrorMessage(err) << std::endl;
		jfl_notimpl;
	}
}

void FontParse(std::string_view name, const uint8_t* data, size_t len) {
	FontObject* font = new(unmanaged_allocate(sizeof(FontObject)))FontObject();
	font->data = (uint8_t*)unmanaged_allocate(len);
	memcpy(font->data, data, len);
	font->data_len = len;
	FT_Error err = FT_New_Memory_Face(
		FTLibrary,
		font->data,
		(FT_Long)font->data_len,
		0,
		&font->face
	);
	if (err) {
		std::cerr << "FreeType read file error: " << GetFreeTypeErrorMessage(err) << std::endl;
		jfl_notimpl;
	}
	FontRegister(name, font);
}

TextLayout::TextLayout():
	valid(false),
	width(100),
	height(100)
{
}

void TextLayout::update_matrix(const matrix2d& matrix){
	if ( memcmp(&matrix, &this->matrix, sizeof(matrix2d)) ){
		this->matrix = matrix;
		invalidate();
	}
}

void TextLayout::set_plain_text(const std::string_view& text){
	reset_text();
	if (text.empty()){
		return;
	}
	TextRun run{
		format,
		um_string(text)
	};
	runs.emplace_back(std::move(run));
	invalidate();
}

void TextLayout::append_plain_text(const std::string_view& text){
	if (text.empty()){
		return;
	}
	if (runs.empty()){
		set_plain_text(text);
	}else{
		runs.back().text += text;
		invalidate();
	}
}

static uint32_t swap_rb(uint32_t color) {
	uint32_t r = (color >> 0) & 0xff;
	uint32_t g = (color >> 8) & 0xff;
	uint32_t b = (color >> 16) & 0xff;
	return (r << 16) | (g << 8) | (b << 0);
}
struct HTMLParser{
	static constexpr int32_t EndOfStream = -1;

	HTMLParser(Arena& arena, const TextFormat& format, um_vec<TextRun>& runs):
		arena(arena),
		runs(runs),
		tags(arena),
		formats(arena),
		calls(arena),
		begin_formats(0),
		end_formats(0),
		begin_tags(0),
		end_tags(0)	
	{
		formats.push_back(format);
		begin_formats++;
	}

	void run(std::string_view text){
		cur = (const uint8_t*)text.data();
		end = cur + text.size();
		parse();
	}

	void parse(){
		while (true){
			int32_t ch = peek();
			switch (ch){
			case '<':{
				parse_tag();
			}break;
			case '&':{
				parse_entity();
			}break;
			case EndOfStream:{
				return;
			}break;
			default:{
				parse_text();
			}break;
			}
		}
	}

	void parse_tag(){
		advance();
		skip_ws();
		if (peek() == '/') {
			parse_closing_tag();
		}else{
			parse_opening_tag();
		}
		skip_ws();
		skip_gt();
	}

	jfl_noinline
	int32_t parse_int(std::string_view s) {
		int32_t acc = 0;
		for (auto ch : s) {
			if (ch >= '0' && ch <= '9') {
				acc = acc * 10 + (int32_t)(ch - '0');
			} else {
				// bad character
				add_run("#badint");
			}
		}
		return acc;
	}

	uint32_t parse_hex(std::string_view s){
		uint32_t acc = 0;
		for (auto ch: s){
			if (ch >= '0' && ch <= '9'){
				acc = acc * 16 + ch - '0';
			} else if (ch >= 'a' && ch <= 'f') {
				acc = acc * 16 + ch - 'a' + 10;
			} else if (ch >= 'A' && ch <= 'F') {
				acc = acc * 16 + ch - 'A' + 10;
			} else {
				// bad character
				add_run("#badhex");
			}
		}
		return acc;
	}

	void parse_opening_tag(){
		auto id = parse_id();
		calls.push_back('T');
		if (id == "font"){
			calls.push_back('H');
			skip_ws();
			TextFormat new_format = formats.back();
			bool autoclose = false;
			while (true) {
				int32_t ch = peek();
				if (ch == '/') {
					advance();
					skip_ws();
					autoclose = true;
					break;;
				} else if (ch == EndOfStream) {
					break;
				} else if (ch == '>') {
					break;
				} else if ( isalnum(ch) || ch == '-' || ch == '_' ){
					std::string_view key = parse_id();
					std::string_view value;
					skip_ws();
					if ( peek() == '=' ){
						advance();
						skip_ws();
						value = parse_string();
						skip_ws();
					}
					if (key == "color" && value.size() > 0) {
						if (value[0] == '#'){
							new_format.color = parse_hex(value.substr(1));
						}else{
							add_run("#badcolor");
						}
					}else if (key == "size" && value.size() > 0) {
						int32_t int_value;
						int32_t old_size = new_format.size;
						if (value[0] == '+' || value[0] == '-'){
							int_value = parse_int(value.substr(1));
							if (value[0] == '+'){
								new_format.size += int_value;
							}else{
								new_format.size -= int_value;
							}
						}else{
							int_value = parse_int(value);
							new_format.size = int_value;
						}
						if (new_format.size < 0) {
							std::cerr << "Formats in stack: " << formats.size() << "\n";
							for (const auto& format: formats) {
								std::cerr << "{Color: " << format.color
									<< ", Size: " << format.size
									<< "}\n";
							}
							std::cerr << "Call Sequence: " << std::string_view(calls.data(), calls.size()) << "\n";
							std::cerr << "Begin/end pairs: " << begin_tags << "/" << end_tags << "; " << begin_formats << "/" << end_formats << "\n";
							std::cerr << "String source: " << std::string_view((const char*)cur, end - cur) << "\n";
							std::cerr << "Invalid format composition: " << old_size << " becomes " << new_format.size << " after parsing " << value << std::endl;
							// new_format.size = 0;
						}
					}else{
						add_run(key);
						add_run("=");
						add_run(value);
					}
				} else{
					break;
				}
			}
			if (!autoclose){
				calls.push_back('A');
				text_format_changed = true;
				formats.push_back(new_format);
				begin_formats++;
				begin_tags++;
				tags.push_back(id);
			}
			calls.push_back('h');
		}else{
			calls.push_back('E');
			add_run("<");
			add_run(id);
			add_run(">");
			calls.push_back('e');
		}
	}

	std::string_view parse_string(){
		std::string_view result;
		skip_quote();
		const uint8_t* beg = cur;
		while (true) {
			int32_t ch = peek();
			if (ch == '"' || ch == EndOfStream) {
				break;
			} else {
				advance();
			}
		}
		result = {(const char*)beg, (size_t)(cur - beg)};
		skip_quote();
		return result;
	}

	void skip_quote(){
		if (peek() == '"') {
			advance();
		}
	}

	void parse_closing_tag(){
		advance();
		skip_ws();
		auto id = parse_id();
		calls.push_back('C');
		for (size_t i = 0; i < tags.size(); i++){
			if (tags[tags.size() - i - 1] == id){
				// discard all the things up to the tag
				for (size_t j = 0; j <= i; j++) {
					end_tags++;
					calls.push_back('D');
					auto closing_tag = tags.back();
					tags.pop_back();
					if (closing_tag == "font") {
						calls.push_back('F');
						end_formats++;
						formats.pop_back();
						text_format_changed = true;
					}
				}
				break;
			}
		}
		calls.push_back('c');
	}

	void skip_gt(){
		if (peek() == '>') {
			advance();
		}
	}

	void skip_ws(){
		while (true) {
			int32_t ch = peek();
			if (ch == ' ' || ch == '\n' || ch == '\r') {
				advance();
			}else{
				break;
			}
		}
	}

	std::string_view parse_id(){
		const uint8_t* beg = cur;
		while (true) {
			int32_t ch = peek();
			if (isalnum(ch) || ch == '-' || ch == '_') {
				advance();
			}else{
				break;
			}
		}
		return {(const char*)beg, (size_t)(cur - beg) };
	}

	void parse_entity(){
		advance();
		const uint8_t* beg = cur;
		while (true) {
			int32_t ch = peek();
			if (ch == ';') {
				advance();
				break;
			}
			if (ch == EndOfStream) {
				break;
			}
			advance();
		}
		std::string_view name((const char*)beg, cur - beg);
		if (name == "lt;"){
			add_run("<");
		} else if (name == "gt;") {
			add_run(">");
		} else if (name == "amp;") {
			add_run("&");
		} else if (name == "quot;") {
			add_run("\"");
		} else if (name == "apos;") {
			add_run("'");
		} else {
			add_run("&");
			add_run(name);
		}
	}

	void parse_text(){
		const uint8_t* beg = cur;
		while (true) {
			int32_t ch = peek();
			if (ch == '<' || ch == '&' || ch == EndOfStream){
				break;
			}
			advance();
		}
		add_run({(const char*)beg, (size_t)(cur - beg) });
	}

	void add_run(std::string_view text){
		if (text_format_changed){
			TextRun run{
				formats.back(),
				um_string(text)
			};
			runs.emplace_back(std::move(run));
			text_format_changed = false;
		}else{
			runs.back().text += text;
		}
	}

	int32_t peek(){
		if (cur == end){
			return EndOfStream;
		}
		return *cur;
	}

	void advance(){
		cur++;
	}

	Arena& arena;
	um_vec<TextRun>& runs;
	arena_vec<std::string_view> tags;
	arena_vec<TextFormat> formats;
	arena_vec<char> calls;
	bool text_format_changed = true;
	int end_tags;
	int begin_tags;
	int begin_formats;
	int end_formats;

	const uint8_t* cur;
	const uint8_t* end;
};

void TextLayout::set_html_text(const std::string_view& text){
	reset_text();
	Arena arena;
	HTMLParser parser(arena, format, runs);
	parser.run(text);
}

void TextRenderFail(std::string_view message, FT_Error err){
	std::cerr << "Text rendering failed (" << message << "): " << GetFreeTypeErrorMessage(err) << std::endl;
	jfl_notimpl;
}

struct UTFDecoder{
	static constexpr uint32_t EndOfStream = ~0u;

	void feed(std::string_view data){
		cur = (const uint8_t*)data.data();
		end = cur + data.size();
	}

	uint32_t advance(){
		if (cur == end){
			return EndOfStream;
		}
		uint8_t ch = *cur;
		cur++;
		if (ch <= 127){
			if (ch == '\r'){
				if (cur != end && *cur == '\n'){
					cur++;
				}
				return '\n';
			}
			return ch;
#define DECODE_TRAILING_BYTE \
	if (cur == end){return 0;} \
	x = (x << 6) | ((*(cur++)) & 0b0011'1111); 

		}else if (ch <= 0b1110'0000){
			uint32_t x = ch & 0b0001'1111;
			DECODE_TRAILING_BYTE;
			return x;
		} else if (ch <= 0b1111'0000) {
			uint32_t x = ch & 0b0000'1111;
			DECODE_TRAILING_BYTE;
			DECODE_TRAILING_BYTE;
			return x;
		} else if (ch <= 0b1111'1000) {
			uint32_t x = ch & 0b0000'0111;
			DECODE_TRAILING_BYTE;
			DECODE_TRAILING_BYTE;
			DECODE_TRAILING_BYTE;
			return x;
		} else {
			// invalid character
			return 0;
		}
	}
private:
	const uint8_t* cur;
	const uint8_t* end;
};

static bool is_space(uint32_t codepoint){
	if (codepoint == ' '){
		return true;
	}
	return false;
}

struct LayoutCommand{
	enum class Tag{
		ChangeFont,
		Space,
		Linebreak,
		Word
	} tag;

	LayoutCommand(Tag tag):
		tag(tag)
	{
	}
};

struct LayoutCommandChangeFont: public LayoutCommand {
	FT_Face face;
	const TextFormat* format;
	uint32_t pt_width;
	uint32_t pt_height;

	int32_t ascend, descend;
	int32_t line_gap;

	LayoutCommandChangeFont(FT_Face face, const TextFormat* format):
		LayoutCommand(LayoutCommand::Tag::ChangeFont),
		face(face),
		format(format)
	{
	}
};

struct LayoutCommandSpace : public LayoutCommand {
	uint32_t size;

	LayoutCommandSpace(uint32_t size) :
		LayoutCommand(LayoutCommand::Tag::Space),
		size(size)
	{
	}
};

struct LayoutCommandLinebreak : public LayoutCommand {
	LayoutCommandLinebreak() :
		LayoutCommand(LayoutCommand::Tag::Linebreak)
	{
	}
};

struct LayoutGlyph{
	uint32_t glyph_index;
	// offset is calculated from the word's origin
	uint32_t offset;
};

struct LayoutCommandWord : public LayoutCommand {
	arena_vec<LayoutGlyph> glyphs;
	// total width of the word
	uint32_t width;
	// total advance the word makes
	uint32_t advance;

	LayoutCommandWord(Arena& arena) :
		LayoutCommand(LayoutCommand::Tag::Word),
		glyphs(arena),
		width(0),
		advance(0)
	{
	}
};

struct LayoutLineWord{
	LayoutCommandChangeFont* font;
	arena_vec<LayoutGlyph>* glyphs;
	int32_t offset;
};

struct LayoutLine{
	LayoutCommandChangeFont* font;
	arena_vec<LayoutLineWord> words;
	int32_t ascend, descend;
	int32_t line_gap;

	int32_t vertical_offset;

	LayoutLine(LayoutCommandChangeFont* font, Arena& arena):
		words(arena),
		font(font){
	}
};

static int32_t ft2pixels(FT_Pos x){
	return (x + 32) / 64;
}

jfl_noinline
double wrapceil(double x) {
	return std::ceil(x);
}

struct LayoutOperation {
	LayoutOperation(Arena& arena, TextLayout& text):
		arena(arena),
		text(text),
		commands(arena),
		lines(arena)
	{
	}

	template<class V>
	void report(V v, const char* file, int line, const char* msg) {
		char buf[512];
		sprintf(
			buf, 
			"%s(%d): %s [%f]\n  context: size = %f x %f, matrix.a = %f, format.size = %f", 
			file, 
			line, 
			msg,
			(double)v, 
			text.width, 
			text.height, 
			text.matrix.a,
			active_format ? active_format->size : 0.0
		);
		raise_message(buf);
	}

	template<class T, class V>
	T checked_to_int(V v, const char* file, int line) {
		if (isnan(v)) {
			report(v, file, line, "nan");
		}
		if (v < (V)std::numeric_limits<T>::min() || v > (V)std::numeric_limits<T>::max()) {
			report(v, file, line, "too large");
		}
		return (T)v;
	}

	void layout(const um_vec<TextRun>& runs){
	#define CHECKED_FLOAT_TO_INT(type, val) checked_to_int<type>(val, __FILE__, __LINE__)

		matrix2d matrix = text.matrix;
		bool word_wrap = text.word_wrap;

		if (text.width <= 4 || text.height <= 4){
			text_width = 4;
			text_height = 4;
			return;
		}
		
		if (matrix.b != 0 || matrix.c != 0 || matrix.a != matrix.d) {
			raise_message("attempting to render text with bad transform matrix");
		}

		double scale = matrix.a;

		int32_t width = CHECKED_FLOAT_TO_INT(int32_t, text.width * scale);
		int32_t height = CHECKED_FLOAT_TO_INT(int32_t, text.height * scale);

		FT_Error err;
		UTFDecoder decoder;

		LayoutCommandWord* word = nullptr;

		auto force_newline = [&]() {
		};

		auto break_opportunity = [&]() -> bool {
			return false;
		};

		// prepare commands
		for (auto& run : runs) {
			auto& format = run.format;
			active_format = &format;
			FontObject* object = format.font.get_font();
			if (!object) {
				continue;
			}

			FT_Face face = object->face;
			FT_GlyphSlot glyph_slot = face->glyph;

			if (word) {
				word = nullptr;
			}
			LayoutCommandChangeFont* change_font = new(arena.allocate(sizeof(LayoutCommandChangeFont)))LayoutCommandChangeFont(
				face,
				&format
			);
			commands.push_back(change_font);

			#if defined(JF_ROUND_FONT_SIZE)
			uint32_t pt_width = CHECKED_FLOAT_TO_INT(uint32_t, ceil(format.size * scale * 64) + 32) & ~63);
			uint32_t pt_height = CHECKED_FLOAT_TO_INT(uint32_t, ceil(format.size * scale * 64) + 32) & ~63);
			#else
			uint32_t pt_width = CHECKED_FLOAT_TO_INT(uint32_t, wrapceil(format.size * scale * 64.0));
			uint32_t pt_height = CHECKED_FLOAT_TO_INT(uint32_t, wrapceil(format.size * scale * 64.0));
			#endif

			change_font->pt_width = pt_width;
			change_font->pt_height = pt_height;

			err = FT_Set_Char_Size(object->face, pt_width, pt_height, 72, 72);
			if (err) {
				TextRenderFail("set size", err);
			}

			change_font->ascend = ft2pixels(object->face->size->metrics.ascender);
			change_font->descend = ft2pixels(object->face->size->metrics.descender);
			change_font->line_gap = ft2pixels(object->face->size->metrics.height);

			decoder.feed(run.text);
			uint32_t codepoint;
			while ((codepoint = decoder.advance()) != UTFDecoder::EndOfStream) {
				if (codepoint == '\n') {
					word = nullptr;
					LayoutCommandLinebreak* linebreak = new(arena.allocate(sizeof(LayoutCommandLinebreak)))LayoutCommandLinebreak();
					commands.push_back(linebreak);
				} else {
					FT_UInt glyph_index = FT_Get_Char_Index(face, codepoint);
					if ( !glyph_index ){
						continue;
					}
					err = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);
					if (err) {
						TextRenderFail("load glyph", err);
					}

					uint32_t advance_x = glyph_slot->advance.x >> 6;
					uint32_t glyph_width = glyph_slot->metrics.width >> 6;

					if (is_space(codepoint)) {
						word = nullptr;
						LayoutCommandSpace* space = new(arena.allocate(sizeof(LayoutCommandSpace)))LayoutCommandSpace(
							advance_x
						);
						commands.push_back(space);
					}else{
						if (!word){
							word = new(arena.allocate(sizeof(LayoutCommandWord)))LayoutCommandWord(arena);
							commands.push_back(word);
						}
						LayoutGlyph glyph;
						glyph.glyph_index = glyph_index;
						glyph.offset = word->advance;
						word->advance += advance_x;
						word->width = glyph.offset + glyph_width;
						word->glyphs.push_back(glyph);
					}
				}
			}
		}

		if (commands.empty()) {
			text_width = 4.0 / scale;
			text_height = 4.0 / scale;
			return;
		}

		// split into lines
		size_t line_begin = 0;
		const TextFormat* current_format = nullptr;
		FT_Face current_face;

		LayoutCommandChangeFont* font_command = nullptr;

		int32_t line_ascend = 0, line_descend = 0;
		int32_t line_gap = 0;

		auto visit_font = [&](LayoutCommandChangeFont* change_font){
			current_format = change_font->format;
			font_command = change_font;
		};

		LayoutCommandChangeFont* line_font = nullptr;

		auto apply_line_height = [&]() {
			line_ascend = std::max(line_ascend, line_font->ascend);
			line_descend = std::min(line_descend, line_font->descend);
			line_gap = std::max(line_gap, line_font->line_gap);
		};

		size_t command_idx = 0;
		for (; command_idx < commands.size(); command_idx++){
			auto command = commands[command_idx];
			if (command->tag == LayoutCommand::Tag::ChangeFont) {
				visit_font((LayoutCommandChangeFont*)command);
			} else {
				break;
			}
		}

		int32_t min_y = 0;
		int32_t max_y = 0;
		int32_t min_x = INT_MAX, max_x = INT_MIN;
		int32_t pos_y = 2;

		while (command_idx < commands.size()){
			TextAlign align = current_format->align;
			const TextFormat* line_format = current_format;
			// flash player adds 2 pixel default padding for every text field
			uint32_t total_width = (width - 4 - line_format->right_margin - line_format->left_margin);
			uint32_t current_advance = 0;
			uint32_t current_width = 0;
			uint32_t content_width = 0;
			
			line_begin = command_idx;

			line_font = font_command;

			apply_line_height();

			for (; command_idx < commands.size(); command_idx++){
				auto command = commands[command_idx];
				if (command->tag == LayoutCommand::Tag::Linebreak) {
					// consume the linebreak and finish the line
					command_idx++;
					break;
				} else if (command->tag == LayoutCommand::Tag::Space) {
					LayoutCommandSpace* space = (LayoutCommandSpace*)command;
					// we dont care about the overflowing with spaces -
					// we'll trim all the trailing spaces at the end
					current_advance += space->size;
					current_width = current_advance;
				} else if (command->tag == LayoutCommand::Tag::ChangeFont) {
					visit_font((LayoutCommandChangeFont*)command);
				} else if (command->tag == LayoutCommand::Tag::Word) {
					LayoutCommandWord* word = (LayoutCommandWord*)command;
					if (word_wrap && current_advance + word->width >= total_width && command_idx > line_begin) {
						// the word does not fit on the line
						break;
					}else{
						// the word fits on the line, continue
						current_width = current_advance + word->width;
						current_advance += word->advance;
						content_width = current_width;
						apply_line_height();
					}
				} else {
					jfl_unreachable;
				}
			}

			// discard the trailing spaces
			size_t line_end = command_idx;
			for (; line_end >= line_begin; line_end--){
				auto command = commands[line_end - 1];
				if (command->tag == LayoutCommand::Tag::Word){
					break;
				}
			}

			// a line is ready - now we need to render it
			LayoutLine* line = new(arena.allocate(sizeof(LayoutLine)))LayoutLine(line_font, arena);
			line->ascend = line_ascend;
			line->descend = line_descend;
			line->line_gap = line_gap;

			lines.push_back(line);
			int32_t free_space = total_width - content_width;
			int32_t offset = 2 + line_format->left_margin;
			switch (align){
			case TextAlign::Left:{
				// do nothing
			}break;
			case TextAlign::Right:{
				offset += free_space;
			}break;
			case TextAlign::Center:{
				offset += free_space / 2;
			}break;
			default:{
				jfl_notimpl;
			}break;
			}

			min_x = std::min(min_x, offset);
			
			LayoutCommandChangeFont* current_font = (LayoutCommandChangeFont*)line_font;
			for (size_t idx = line_begin; idx < line_end; idx++) {
				auto command = commands[idx];
				if (command->tag == LayoutCommand::Tag::Linebreak) {
					// do nothing - should not end up here anyway
				} else if (command->tag == LayoutCommand::Tag::Space) {
					LayoutCommandSpace* space = (LayoutCommandSpace*)command;
					offset += space->size;
				} else if (command->tag == LayoutCommand::Tag::ChangeFont) {
					current_font = (LayoutCommandChangeFont*)command;
				} else if (command->tag == LayoutCommand::Tag::Word) {
					LayoutCommandWord* word = (LayoutCommandWord*)command;
					LayoutLineWord line_word;
					line_word.font = current_font;
					line_word.glyphs = &word->glyphs;
					line_word.offset = offset;
					line->words.push_back(line_word);
					offset += word->advance;
				} else {
					jfl_unreachable;
				}
			}

			line->vertical_offset = pos_y + line->ascend;
			int32_t vertical_size = line->vertical_offset - line->descend + 2;
			pos_y = pos_y + line->line_gap + line_format->leading;
			max_y = std::max(max_y, vertical_size);

			offset += 2 + line_format->right_margin;
			max_x = std::max(max_x, offset);

			line_ascend = 0;
			line_descend = 0;
			line_gap = 0;
		}

		if (min_x == INT_MAX){
			text_width = 4.0 / scale;
			text_height = 4.0 / scale;
		}else{
			text_width = (max_x - min_x) / scale;
			text_height = (max_y - min_y) / scale;
		}
	}

	void render(TextRenderingCanvas& canvas){
		if (text.width <= 4 || text.height <= 4) {
			return;
		}

		LayoutCommandChangeFont* font = nullptr;
		int32_t canvas_width = canvas.width;
		int32_t canvas_height = canvas.height;
		FT_Face face = nullptr;
		uint32_t color = 0;

		for (auto line: lines) {
			int32_t line_y = line->vertical_offset;
			for (auto& word: line->words){
				if (word.font != font) {
					font = word.font;
					face = font->face;
					color = swap_rb(font->format->color);
					FT_Set_Char_Size(face, font->pt_width, font->pt_height, 72, 72);
				}
				int32_t word_x = word.offset;
				for (auto& glyph: *word.glyphs) {
					FT_Load_Glyph(face, glyph.glyph_index, FT_LOAD_TARGET_LIGHT);
					FT_Render_Glyph(face->glyph, FT_RENDER_MODE_LIGHT);

					auto slot = face->glyph;
					auto& bitmap = slot->bitmap;

					int32_t glyph_x = glyph.offset + word_x;
					int32_t glyph_y = line_y;
					
					int32_t canvas_x = glyph_x + slot->bitmap_left;
					int32_t canvas_y = glyph_y - slot->bitmap_top + bitmap.rows;

					int32_t bitmap_rows = bitmap.rows;

					int32_t start_x = canvas_x;
					int32_t end_x = canvas_x + bitmap.width;
					
					int32_t start_y = canvas_y - bitmap.rows;
					int32_t end_y = canvas_y;

					int32_t source_x_offset = start_x;
					int32_t source_y_offset = start_y;

					if (start_x > canvas_width || end_x <= 0) {
						continue;
					}
					if (start_y > canvas_height || end_y <= 0) {
						continue;
					}
					
					start_x = std::max(0, start_x);
					end_x = std::min(canvas_width, end_x);
					start_y = std::max(0, start_y);
					end_y = std::min(canvas_height, end_y);

					for (int32_t row = start_y; row < end_y; row++){
						int32_t source_y = row - source_y_offset;
						int32_t target_y = row;

						uint8_t* source_row = bitmap.buffer + source_y * bitmap.pitch;
						uint8_t* target_row = canvas.data + target_y * canvas.bytesPerRow;

						for(int32_t x = start_x; x < end_x; x++){
							int32_t source_x = x - source_x_offset;
							int32_t target_x = x;

							uint8_t pixel = source_row[source_x];

							uint32_t pixel_color = color | (pixel << 24);

							*(uint32_t*)(target_row + target_x * 4) = pixel_color;
						}
					}
				}
			}
		}
	}

	void finalize(){
	}

	Arena& arena;
	TextLayout& text;
	const TextFormat* active_format;

	double text_width, text_height;

	arena_vec<LayoutCommand*> commands;

	arena_vec<LayoutLine*> lines;
};

void TextLayout::ensure_layout(){
	if ( !valid ){
		valid = true;
		Arena arena;
		LayoutOperation operation(arena, *this);
		operation.layout(runs);
		operation.finalize();

		text_width = operation.text_width;
		text_height = operation.text_height;
	}
}

String TextLayout::get_plain_text() const{
	Arena arena;
	arena_string buffer(arena);
	for (auto& run: runs){
		buffer += run.text;
	}
	return String::Make(buffer);
}

void TextLayout::render(const matrix2d& matrix, TextRenderingCanvas& canvas){
	update_matrix(matrix);
	Arena arena;
	LayoutOperation operation(arena, *this);
	operation.layout(runs);
	operation.finalize();
	operation.render(canvas);
}

void TextLayout::invalidate(){
	valid = false;
}

}


