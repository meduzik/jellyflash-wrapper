#include <jfl/rest_params.h>
#include <jfl/operator_to.h>
#include <jfl/any.h>
#include <fl/Array.h>

namespace jfl{

RestParams rest_slice(RestParams* params, UInt offset) {
	if ( params->tag == type_tag::Rest ){
		return RestParams(params->ptr + offset, params->size - offset);
	}else{
		RestParams params_slice(*params);
		fl::Array* array = fl::Array::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_slice((fl::Array*)params->object, offset, 0x7fffffffu);
		params_slice.tag = type_tag::Object;
		params_slice.object = (jfl::Object*)array;
		return params_slice;
	}
}


UInt rest_array_size(RestParams* params){
	return fl::Array::jfl_g_length((fl::Array*)params->object);
}

Any rest_array_get(RestParams* params, UInt idx){
	Any key(idx);
	Any value;
	fl::Array::jfl_Dynamic(params->object, &key, &value, dynamic_op::get);
	return value;
}

fl::Array* rest_to_array(RestParams* params){
	if ( params->tag == type_tag::Object) {
		return (fl::Array*)params->object;
	}
	fl::Array* array = fl::Array::jfl_New(jfl::params({}));
	array->jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(array, params);
	params->tag = type_tag::Object;
	params->object = (jfl::Object*)array;
	return array;
}

}