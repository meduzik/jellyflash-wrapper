#include <jfl/vector.h>

namespace jfl{



template<> Boolean Vector<Boolean>::Default(false);
template<> Int Vector<Int>::Default(0);
template<> UInt Vector<UInt>::Default(0);
template<> Number Vector<Number>::Default(NaN);
template<> String Vector<String>::Default(nullptr);
template<> Function Vector<Function>::Default(nullptr);
template<> Any Vector<Any>::Default(nullptr);
template<> Object* Vector<Object*>::Default(nullptr);
template<> Interface Vector<Interface>::Default(nullptr);


template<class T>
Boolean Vector<T>::eq(T lhs, T rhs) {
	return lhs == rhs;
}

template<>
Boolean Vector<Interface>::eq(Interface lhs, Interface rhs) {
	return lhs.o == rhs.o;
}

template<>
Boolean Vector<Any>::eq(Any lhs, Any rhs) {
	return any_eq(lhs, rhs);
}

template<>
Boolean Vector<Function>::eq(Function lhs, Function rhs) {
	return func_eq(lhs, rhs);
}

template<>
Boolean Vector<String>::eq(String lhs, String rhs) {
	return str_eq(lhs, rhs);
}


template<>
Any Vector<Any>::cast(Any val) {
	return val;
}

template<>
Boolean Vector<Boolean>::cast(Any val) {
	return any_to_Boolean(val);
}

template<>
Int Vector<Int>::cast(Any val) {
	return any_to_int(val);
}

template<>
UInt Vector<UInt>::cast(Any val) {
	return any_to_uint(val);
}

template<>
Number Vector<Number>::cast(Any val) {
	return any_to_Number(val);
}

template<>
String Vector<String>::cast(Any val) {
	return any_to_String(val);
}

template<>
Function Vector<Function>::cast(Any val) {
	return any_to_Function(val);
}

template<>
Object* Vector<Object*>::cast(Any val) {
	return any_to_Class(val, elty);
}

template<>
Interface Vector<Interface>::cast(Any val) {
	return any_to_Interface(val, elty);
}


template<class T>
Vector<T>* Vector<T>::Cons(const Vector_VT* instance, const Class* elty, fl::Array* array) {
	UInt len = fl::Array::jfl_g_length(array);
	Vector<T>* vec = New(instance, elty, len, false);
	for ( UInt i = 0; i < len; i++ ){
		vec->ptr[i] = vec->cast(fl::Array::jfl_get(array, i));
	}
	return vec;
}


template<class T>
Vector<T>* Vector<T>::New(const Vector_VT* instance, const Class* elty, UInt length, Boolean fixed) {
	Vector<T>* vec = gc_new< Vector<T> >();
	vec->jfl_vtable = instance;
	vec->elty = elty;
	vec->capacity = length;
	vec->length = length;
	if (length > 0) {
		vec->ptr = (T*)unmanaged_allocate(sizeof(T) * vec->capacity);
		for (size_t i = 0; i < vec->length; i++) {
			vec->ptr[i] = Vector<T>::Default;
		}
	}else{
		vec->ptr = nullptr;
	}
	return vec;
}

template<class T>
void Vector<T>::jfl_s_length(UInt val) {
	if (val < length) {
		length = val;
	} else {
		if (val > capacity) {
			grow(val);
		}
		for (size_t i = length; i < val; i++) {
			ptr[i] = Vector<T>::Default;
		}
		length = val;
	}
}

template<class T>
void Vector<T>::grow(UInt minlength) {
	if (capacity >= minlength) {
		return;
	}
	size_t newcapacity = capacity;
	while (newcapacity < minlength) {
		newcapacity = std::max((size_t)10u, newcapacity * 2u);
	}
	T* newptr = (T*)unmanaged_allocate(sizeof(T) * newcapacity);
	if (ptr){
		for (size_t i = 0; i < length; i++) {
			memcpy(newptr, ptr, sizeof(T) * length);
		}
		unmanaged_free(ptr, sizeof(T) * capacity);
	}
	capacity = (UInt)newcapacity;
	ptr = newptr;
}

template<class T>
void Vector<T>::set(UInt index, T value) {
	if (index > length) {
		char buf[256];
		sprintf(buf, "Writing out of bounds index %d (vector length is %d)", index, length);
		raise_message(buf);
	}
	if (index == length) {
		if (capacity <= index) {
			grow((UInt)(capacity + 1));
		}
		length++;
	}
	ptr[index] = value;
}

template<class T>
T Vector<T>::get(UInt index) {
	if (index >= length) {
		char buf[256];
		sprintf(buf, "Accessing out of bounds index %d (vector length is %d)", index, length);
		raise_message(buf);
	}
	return ptr[index];
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_splice(Int startIndex, Int deleteCount, RestParams* items) {
	UInt addCount = (UInt)rest_size(items);
	if ( (int64_t)startIndex > (int64_t)length ) {
		jfl_notimpl;
	}
	deleteCount = (Int)std::min((size_t)length - startIndex, (size_t)deleteCount);
	grow((UInt)(length + addCount - deleteCount));
	T* rest_begin_before = &ptr[startIndex + deleteCount];
	T* rest_begin_after = &ptr[startIndex + addCount];
	Int rest_length = (Int)(length - (startIndex + deleteCount));

	if (rest_begin_before != rest_begin_after && rest_length > 0) {
		memmove(rest_begin_after, rest_begin_before, sizeof(T) * rest_length);
	}
	for (UInt i = 0; i < addCount; i++) {
		ptr[startIndex + i] = Default;
	}
	length = length + addCount - deleteCount;
	for (UInt i = 0; i < addCount; i++) {
		ptr[startIndex + i] = cast(rest_get(items, i));
	}
	return this;
}

template<class T>
UInt Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(RestParams* args) {
	jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_splice((Int)length, 0, args);
	return (UInt)length;
}

template<class T>
UInt Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_unshift(RestParams* args) {
	jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_splice(0, 0, args);
	return (UInt)length;
}

template<class T>
T Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_shift() {
	if (length <= 0) {
		jfl_notimpl;
	}
	T ret = ptr[0];
	memmove(ptr, ptr + 1, (length - 1) * sizeof(T));
	length--;
	return ret;
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_reverse() {
	jfl_notimpl;
	return this;
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_sort(Any behavior) {
	if ( behavior.tag >= type_tag::FunctionBit ){
		Function func = *(Function*)&behavior;
		std::sort(ptr, ptr + length, [&](const T& val1, const T& val2){
			return any_to_int(func.invoke(params({val1, val2}))) < 0;
		});
	}else{
		jfl_notimpl;
	}
	return this;
}

template<class T>
Int Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_indexOf(T searchElement, Int fromIndex) {
	for (UInt i = fromIndex; i < length; i++) {
		if (eq(ptr[i], searchElement)) {
			return i;
		}
	}
	return -1;
}

template<class T>
Int Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_lastIndexOf(T searchElement, Int fromIndex) {
	if (fromIndex >= length) {
		fromIndex = length - 1;
	}
	for (UInt i = fromIndex; i < length; i--) {
		if (eq(ptr[i], searchElement)) {
			return i;
		}
	}
	return -1;
}




template<class T>
T Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_pop() {
	if (length <= 0) {
		jfl_notimpl;
	}
	return ptr[--length];
}

template<class T>
T Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_removeAt(Int index) {
	if (index < 0 || (uint64_t)index >= (uint64_t)length) {
		jfl_notimpl;
	}
	T ret = ptr[index];
	memmove(ptr + index, ptr + index + 1, sizeof(T) * (length - index - 1));
	length--;
	return ret;
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_slice(Int startIndex, Int endIndex) {
	if ( startIndex < 0 ){
		startIndex = 0;
	}
	if ( (UInt)startIndex > length ){
		startIndex = (UInt)length;
	}
	if ( endIndex < startIndex ){
		endIndex = startIndex;
	}
	if ( (UInt)endIndex > length ){
		endIndex = (UInt)length;
	}
	Vector<T>* copy = Vector<T>::New(this->jfl_vtable, this->elty, endIndex - startIndex, false);
	for ( Int i = startIndex; i < endIndex; i++ ){
		copy->ptr[i - startIndex] = ptr[i];
	}
	return copy;
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_concat(RestParams* args) {
	jfl_notimpl;
}

template<class T>
String Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_join(String sep) {
	jfl_notimpl;
}

template<class T>
Boolean VectorDynamic(Object* self, Any* key, Any* value, dynamic_op op){
	Vector<T>* vec = (Vector<T>*)self;
	switch ( op ){
	case dynamic_op::get:{
		if ( any_is_String(*key) ){
			String str = any_as_String(*key);
			if ( str_eq(str, StringSmall{ 6, "length" }) ){
				*value = vec->length;
				return true;
			}
		}
		if ( any_is_uint(*key) ){
			UInt idx = any_to_uint(*key);
			*value = vec->get(idx);
			return true;
		}
		jfl_notimpl;
	}break;
	case dynamic_op::set: {
		if (any_is_String(*key)) {
			String str = any_as_String(*key);
			if (!str_eq(str, StringSmall{ 6, "length" } )) {
				vec->jfl_s_length(any_to_uint(*value));
				return true;
			}
		}
		if (any_is_uint(*key)) {
			UInt idx = any_to_uint(*key);
			vec->set(idx, vec->cast(*value));
			return true;
		}
		jfl_notimpl;
	}break;
	default:{
		jfl_notimpl;
	}break;
	}
}

template<class T> 
void VectorFinalize(Object* self){
	Vector<T>* vec = (Vector<T>*)self;
	if (vec->ptr){
		unmanaged_free(vec->ptr, vec->capacity * sizeof(T));
	}
}

template<class T>
void VectorGC(Object* self, void*) {
}

template<>
void VectorGC<Any>(Object* self, void*) {
	Vector<Any>* vec = (Vector<Any>*)self;
	if (vec->ptr) {
		for(size_t i = 0; i < vec->length; i++){
			gc_visit_any(vec->ptr[i]);
		}
	}
}

template<>
void VectorGC<String>(Object* self, void*) {
	Vector<String>* vec = (Vector<String>*)self;
	if (vec->ptr) {
		for (size_t i = 0; i < vec->length; i++) {
			gc_visit_string(vec->ptr[i]);
		}
	}
}

template<>
void VectorGC<Object*>(Object* self, void*) {
	Vector<Object*>* vec = (Vector<Object*>*)self;
	if (vec->ptr) {
		for (size_t i = 0; i < vec->length; i++) {
			gc_visit(vec->ptr[i]);
		}
	}
}

template<>
void VectorGC<Function>(Object* self, void*) {
	Vector<Function>* vec = (Vector<Function>*)self;
	if (vec->ptr) {
		for (size_t i = 0; i < vec->length; i++) {
			gc_visit_function(vec->ptr[i]);
		}
	}
}

template<>
void VectorGC<Interface>(Object* self, void*) {
	Vector<Interface>* vec = (Vector<Interface>*)self;
	if (vec->ptr) {
		for (size_t i = 0; i < vec->length; i++) {
			gc_visit(vec->ptr[i].o);
		}
	}
}

template struct Vector<Any>;
template struct Vector<Boolean>;
template struct Vector<Int>;
template struct Vector<UInt>;
template struct Vector<Number>;
template struct Vector<String>;
template struct Vector<Function>;
template struct Vector<Object*>;
template struct Vector<Interface>;

template Boolean VectorDynamic<Any>(Object* self, Any* key, Any* value, dynamic_op op);
template Boolean VectorDynamic<Boolean>(Object* self, Any* key, Any* value, dynamic_op op);
template Boolean VectorDynamic<Int>(Object* self, Any* key, Any* value, dynamic_op op);
template Boolean VectorDynamic<UInt>(Object* self, Any* key, Any* value, dynamic_op op);
template Boolean VectorDynamic<Number>(Object* self, Any* key, Any* value, dynamic_op op);
template Boolean VectorDynamic<String>(Object* self, Any* key, Any* value, dynamic_op op);
template Boolean VectorDynamic<Function>(Object* self, Any* key, Any* value, dynamic_op op);
template Boolean VectorDynamic<Object*>(Object* self, Any* key, Any* value, dynamic_op op);
template Boolean VectorDynamic<Interface>(Object* self, Any* key, Any* value, dynamic_op op);

template void VectorFinalize<Any>(Object* self);
template void VectorFinalize<Boolean>(Object* self);
template void VectorFinalize<Int>(Object* self);
template void VectorFinalize<UInt>(Object* self);
template void VectorFinalize<Number>(Object* self);
template void VectorFinalize<String>(Object* self);
template void VectorFinalize<Function>(Object* self);
template void VectorFinalize<Object*>(Object* self);
template void VectorFinalize<Interface>(Object* self);

//template void VectorGC<Any>(Object* self, void*);
//template void VectorGC<String>(Object* self, void*);
//template void VectorGC<Function>(Object* self, void*);
//template void VectorGC<Object*>(Object* self, void*);
//template void VectorGC<Interface>(Object* self, void*);
template void VectorGC<Boolean>(Object* self, void*);
template void VectorGC<Int>(Object* self, void*);
template void VectorGC<UInt>(Object* self, void*);
template void VectorGC<Number>(Object* self, void*);

}