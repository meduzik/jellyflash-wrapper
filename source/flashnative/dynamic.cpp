#include <jfl/common_impl.h>
#include <jfl/dynamic.h>

namespace jfl{


struct DynamicIterator {
	Dynamic* object;
	Dynamic::map_t::iterator it;
};

Boolean Dynamic::iterate(Dynamic* dyn, Iterator* iterator) {
	DynamicIterator* iter = (DynamicIterator*)iterator->payload;
	new(&iter->it)Dynamic::map_t::iterator(dyn->map.begin());
	iter->object = dyn;
	iterator->next = [](Iterator* iterator, Any* key, Any* value) -> Boolean {
		DynamicIterator* iter = (DynamicIterator*)iterator->payload;
		if (iter->it == iter->object->map.end()) {
			std::destroy_at(&iter->it);
			return false;
		}
		*key = iter->it->first;
		*value = iter->it->second;
		iter->it++;
		return true;
	};
	return true;
}

Boolean Dynamic::index(Any key, Any* value, dynamic_op op) {
	switch (op) {
	case dynamic_op::set: {
		map[key] = *value;
		return true;
	}break;
	case dynamic_op::get: {
		auto it = map.find(key);
		if (it != map.end()) {
			*value = it->second;
			return true;
		}
		*value = Any(undefined);
		return true;
	}break;
	case dynamic_op::in: {
		return map.count(key);
	}break;
	case dynamic_op::del: {
		map.erase(key);
		return true;
	}break;
	default: {
		return false;
	}break;
	}
}


}