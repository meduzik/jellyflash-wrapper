#include <jfl/common_impl.h>
#include <jfl/types.h>
#include <jfl/class.h>
#include <jfl/math/murmur.h>

namespace jfl{


static Any bad_invoke(Object* object, RestParams* params){
	jfl_notimpl;
}

const Class Class_Null{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{4, "null"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_Undefined{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{5, "undef"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_Rest{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{4, "Rest"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_Any{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{3, "any"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_Object{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{6, "Object"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_Boolean{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{4, "Bool"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_Int{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{3, "int"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_UInt{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{4, "uint"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_Number{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{6, "Number"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_String{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{6, "String"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

const Class Class_Function{
	&Class::jfl_VT,
	static_object,
	nullptr,
	StringSmall{4, "Func"},
	0,
	0,
	class_kind_t::kind_primitive,
	bad_invoke
};

static const void* Class_TI_Values[] = {
	&Class_Class,
	nullptr,
	nullptr
};

static const TypeInfo Class_TI{
	1,
	1,
	Class_TI_Values
};

const Class Class_Class{
	&Class::jfl_VT,
	static_object,
	(const TypeInfo*)&Class_TI,
	StringSmall{5, "Class"},
	0,
	0,
	class_kind_t::kind_class,
	bad_invoke
};

static const void* MethodClosure_TI_Values[] = {
	&Class_MethodClosure,
	nullptr,
	nullptr
};

const TypeInfo MethodClosure_TI{
	1,
	1,
	MethodClosure_TI_Values
};

const Class Class_MethodClosure{
	&Class::jfl_VT,
	static_object,
	(const TypeInfo*)&MethodClosure_TI,
	StringSmall{2, "MC"},
	0,
	0,
	class_kind_t::kind_class,
	bad_invoke
};


static const void* StringObject_TI_Values[] = {
	&Class_String,
	nullptr,
	nullptr
};

const TypeInfo StringObject_TI{
	1,
	1,
	StringObject_TI_Values
};


static jfl::Boolean Class_Dynamic(jfl::Object* self, jfl::Any* key, jfl::Any* value, jfl::dynamic_op op) {
	jfl::Class* class_ = (jfl::Class*)self;
	return class_->dynamic(self, key, value, op);
}

Boolean static_index(Object* self, Any* key, Any* value, dynamic_op op){
	switch ( op ){
	case dynamic_op::del:{
		jfl_notimpl;
	}break;
	case dynamic_op::set: {
		jfl_notimpl;
	}break;
	case dynamic_op::get: {
		jfl_notimpl;
	}break;
	case dynamic_op::in: {
		return false;
	}break;
	case dynamic_op::to_string: {
		*value = class_of(self)->name;
		return false;
	}break;
	case dynamic_op::value_of: {
		*value = class_of(self)->name;
		return false;
	}break;
	default:{
		jfl_unreachable;
	}break;
	}
}

Boolean proplist_index(Object* self, const ClassProperty** plist, uint32_t plist_mask, Any* key, Any* value, dynamic_op op){
	if ( op == dynamic_op::to_string || op == dynamic_op::value_of ){
		return false;
	}
	String prop_name = any_to_String(*key);
	auto view = prop_name.to_string_view();
	uint32_t hash = murmur3_32((const uint8_t*)view.data(), view.size(), 0xAFCE5F03u);
	hash &= plist_mask;
	while ( true ){
		const ClassProperty* prop = plist[hash];
		if ( prop ){
			if ( prop->str == view ){
				switch ( op ){
				case dynamic_op::get:{
					*value = prop->get(self);
					return true;
				}break;
				case dynamic_op::set: {
					prop->set(self, *value);
					return true;
				}break;
				case dynamic_op::in:{
					return true;
				}break;
				default:{
					jfl_notimpl;
				}break;
				}
			}else{
				hash = (hash + 1) & plist_mask;
			}
		}else{
			return false;
		}
	}
}

const Class_VT Class::jfl_VT{
	{
		(const TypeInfo*)&Class_TI,
		sizeof(Class),
		nullptr,
		nullptr,
		Class_Dynamic,
		nullptr
	}
};

}