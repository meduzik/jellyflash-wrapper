#include <jfl/common_impl.h>

namespace jfl{
/*
Any GenericFuncOffsetBase(Object* object, RestParams params){
	return Any(nullptr);
}

struct DynamicIterator{
	Dynamic* object;
	Dynamic::map_t::iterator it;
};

Boolean Dynamic::iterate(Dynamic* dyn, Iterator* iterator){
	DynamicIterator* iter = (DynamicIterator*)iterator->payload;
	new(&iter->it)Dynamic::map_t::iterator(std::move(dyn->map.begin()));
	iter->object = dyn;
	iterator->next = [](Iterator* iterator, Any* key, Any* value) -> Boolean{
		DynamicIterator* iter = (DynamicIterator*)iterator->payload;
		if ( iter->it == iter->object->map.end() ){
			std::destroy_at(&iter->it);
			return false;
		}
		*key = iter->it->first;
		*value = iter->it->second;
		iter->it++;
		return true;
	};
	return true;
}

const TypeInfo T_Undefined;
const TypeInfo T_Null;
const TypeInfo T_Any;
const TypeInfo T_Boolean;
const TypeInfo T_Int;
const TypeInfo T_UInt;
const TypeInfo T_Number;
const TypeInfo T_String;
const TypeInfo T_Class;
const TypeInfo T_Function;

const ObjectVT StringVT{&T_String};

struct VecIterator{
	Object* object;
	UInt i;
};

template<class T>
Boolean vec_iter_next (Iterator* iterator, Any* key, Any* value) {
	VecIterator* vec_it = (VecIterator*)iterator->payload;
	Vector<T>* vec = (Vector<T>*)vec_it->object;
	if (vec_it->i >= vec->length) {
		return false;
	}
	*key = vec_it->i;
	*value = vec->ptr[vec_it->i];
	vec_it->i++;
	return true;
}

template<class T>
const TypeInfo Vector<T>::TInfo {
	nullptr,
	nullptr,
	[](Object* object, Iterator* iterator) -> Boolean{
		Vector<T>* vec = (Vector<T>*)object;
		VecIterator* vec_it = (VecIterator*)iterator->payload;
		vec_it->object = object;
		vec_it->i = 0;
		iterator->next = &vec_iter_next<T>;
		return true;
	}
};

template<class T>
const VectorVT Vector<T>::VT {&Vector<T>::TInfo };

template<> Boolean Vector<Boolean>::Default(false);
template<> Int Vector<Int>::Default(0);
template<> UInt Vector<UInt>::Default(0);
template<> Number Vector<Number>::Default(NaN);
template<> String Vector<String>::Default(nullptr);
template<> Function Vector<Function>::Default(nullptr);
template<> Interface Vector<Interface>::Default(nullptr);
template<> Any Vector<Any>::Default(nullptr);
template<> Object* Vector<Object*>::Default(nullptr);

template<class T>
Boolean Vector<T>::eq(T lhs, T rhs){
	return lhs == rhs;
}

template<>
Boolean Vector<Interface>::eq(Interface lhs, Interface rhs) {
	return lhs.o == rhs.o;
}

template<>
Boolean Vector<Any>::eq(Any lhs, Any rhs) {
	return dyn_eq(lhs, rhs);
}

template<>
Boolean Vector<Function>::eq(Function lhs, Function rhs) {
	return func_eq(lhs, rhs);
}

template<>
Boolean Vector<String>::eq(String lhs, String rhs) {
	return str_eq(lhs, rhs);
}


template<class T>
Vector<T>* Vector<T>::New(const TypeInfo* elty, UInt length, Boolean fixed){
	Vector<T>* vec = Allocate< Vector<T> >();
	vec->jfl_vtable = &Vector<T>::VT;
	vec->elty = elty;
	vec->capacity = length;
	vec->length = length;
	if ( length > 0 ){
		vec->ptr = (T*)AllocateSpace(sizeof(T) * vec->capacity);
		for ( size_t i = 0; i < vec->length; i++ ){
			vec->ptr[i] = Vector<T>::Default;
		}
	}
	return vec;
}

template<class T>
void Vector<T>::jfl_s_length(UInt val){
	if ( val < length ){
		length = val;
	}else{
		if ( val > capacity ){
			grow(val);
		}
		for ( size_t i = length; i < val; i++ ){
			ptr[i] = Vector<T>::Default;
		}
		length = val;
	}
}

template<class T>
void Vector<T>::grow(UInt minlength){
	if ( capacity >= minlength ){
		return;
	}
	size_t newcapacity = capacity;
	while ( newcapacity < minlength ){
		newcapacity = std::max((size_t)10u, newcapacity * 2u);
	}
	T* newptr = (T*)AllocateSpace(sizeof(T) * newcapacity);
	for (size_t i = 0; i < length; i++) {
		memcpy(newptr, ptr, sizeof(T) * length);
	}
	capacity = (UInt)newcapacity;
	ptr = newptr;
}

template<class T>
void Vector<T>::set(UInt index, T value){
	if ( index > length ) {
		jfl_notimpl;
	}
	if ( index == length ){
		if ( capacity <= index ){
			grow((UInt)(capacity + 1));
		}
		length++;
	}
	ptr[index] = value;
}

template<class T>
T Vector<T>::get(UInt index){
	if ( index >= length ){
		jfl_notimpl;
	}
	return ptr[index];
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_splice(Int startIndex, Int deleteCount, RestParams items){
	Int addCount = (Int)items.size();
	if ( startIndex > length ){
		jfl_notimpl;
	}
	deleteCount = (Int)std::min((size_t)length - startIndex, (size_t)deleteCount);
	grow((UInt)(capacity + addCount - deleteCount));
	T* rest_begin_before = &ptr[startIndex + deleteCount];
	T* rest_begin_after = &ptr[startIndex + addCount];
	Int rest_length = (Int)(length - (startIndex + deleteCount));
	
	if (rest_begin_before != rest_begin_after && rest_length > 0){
		memmove(rest_begin_after, rest_begin_before, sizeof(T) * rest_length);
	}
	for ( size_t i = 0; i < addCount; i++ ){
		ptr[startIndex + i] = Default;
	}
	length = length + addCount - deleteCount;
	for ( size_t i = 0; i < addCount; i++ ){
		if ( !anycast(items.at(i), elty, &ptr[startIndex + i]) ){
			jfl_notimpl;
		}
	}
	return this;
}

template<class T>
UInt Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(RestParams args){
	jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_splice((Int)length, 0, args);
	return (UInt)length;
}

template<class T>
UInt Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_unshift(RestParams args){
	jfl_notimpl;
}

template<class T>
T Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_shift(){
	if (length <= 0) {
		jfl_notimpl;
	}
	T ret = ptr[0];
	memmove(ptr, ptr + sizeof(T), (length - 1) * sizeof(T));
	length--;
	return ret;
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_reverse(){
	jfl_notimpl;
	return this;
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_sort(Any behavior){
	jfl_notimpl;
	return this;
}

template<class T>
Int Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_indexOf(T searchElement, Int fromIndex){
	for ( UInt i = fromIndex; i < length; i++ ){
		if ( eq(ptr[i], searchElement) ){
			return i;
		}
	}
	return -1;
}

template<class T>
T Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_pop(){
	if ( length <= 0 ){
		jfl_notimpl;
	}
	return ptr[--length];
}

template<class T>
T Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_removeAt(Int index){
	if ( index < 0 || index >= length ){
		jfl_notimpl;
	}
	T ret = ptr[index];
	memmove(ptr + index, ptr + index + 1, sizeof(T) * (length - index - 1));
	length--;
	return ret;
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_slice(Int startIndex, Int endIndex){
	jfl_notimpl;
}

template<class T>
Vector<T>* Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_concat(RestParams args){
	jfl_notimpl;
}

template<class T>
String Vector<T>::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_join(String sep){
	jfl_notimpl;
}

template Vector<Boolean>;
template Vector<Int>;
template Vector<UInt>;
template Vector<Number>;
template Vector<String>;
template Vector<Function>;
template Vector<Interface>;
template Vector<Any>;
template Vector<Object*>;

inline Boolean to_boolean(Any any){
	switch (any.tag) {
	case type_tag::Null:
	case type_tag::Undefined: {
		return false;
	}break;
	case type_tag::Boolean: {
		return any.bval;
	}break;
	case type_tag::Int: {
		return any.ival;
	}break;
	case type_tag::UInt: {
		return any.uval;
	}break;
	case type_tag::Number: {
		return any.nval;
	}break;
	case type_tag::Function: {
		return true;
	}break;
	case type_tag::Object: {
		return true;
	}break;
	case type_tag::Rest: {
		return true;
	}break;
	default: {
		if (any.tag <= type_tag::StringEnd) {
			return any.tag != type_tag::StringBegin;
		}
		jfl_notimpl;
	}break;
	}
}

inline Number to_number(Any any){
	switch (any.tag) {
	case type_tag::Null:{
		return 0;
	}break;
	case type_tag::Undefined: {
		return NaN;
	}break;
	case type_tag::Boolean: {
		return any.bval ? 1 : 0;
	}break;
	case type_tag::Int: {
		return any.ival;
	}break;
	case type_tag::UInt: {
		return any.uval;
	}break;
	case type_tag::Number: {
		return any.nval;
	}break;
	case type_tag::Function: {
		return NaN;
	}break;
	case type_tag::Object: {
		return NaN;
	}break;
	case type_tag::Rest: {
		return NaN;
	}break;
	default: {
		if (any.tag <= type_tag::StringEnd) {
			double val = 0;
			std::string_view s = ((String*)&any)->to_view();
			auto result = std::from_chars(s.data(), s.data() + s.size(), val, std::chars_format::general);
			if ( result.ec == std::errc() ){
				return NaN;
			}
			return val;
		}
		jfl_notimpl;
	}break;
	}
}

inline String to_string(Any any){
	char buf[64];
#define CSTR(s) String((uint8_t)(sizeof(s) - 1), (const uint8_t*)s)
	switch (any.tag) {
	case type_tag::Null: {
		return CSTR("null");
	}break;
	case type_tag::Undefined: {
		return CSTR("undefined");
	}break;
	case type_tag::Boolean: {
		return any.bval ? CSTR("true") : CSTR("false");
	}break;
	case type_tag::Int:{
		auto result = std::to_chars(buf, buf + sizeof(buf), any.ival);
		if ( result.ec != std::errc() ){
			jfl_notimpl;
		}
		return String((uint8_t)(result.ptr - buf), (const uint8_t*)buf);
	}break;
	case type_tag::UInt:{
		auto result = std::to_chars(buf, buf + sizeof(buf), any.uval);
		if (result.ec != std::errc()) {
			jfl_notimpl;
		}
		return String((uint8_t)(result.ptr - buf), (const uint8_t*)buf);
	}break;
	case type_tag::Number:{
		int result = snprintf(buf, sizeof(buf), "%g", any.nval);
		if ( result < 0 || result >= sizeof(buf) ){
			jfl_notimpl;
		}
		size_t len = strlen(buf);
		if ( len > (size_t)type_tag::SmallStringEnd ){
			// TODO allocate string object
			jfl_notimpl;
		}else{
			return String((uint8_t)len, (const uint8_t*)buf);
		}
	}break;
	case type_tag::Function:{
		return CSTR("function");
	}break;
	case type_tag::Object: {
		return CSTR("object");
	}break;
	default: {
		if (any.tag <= type_tag::StringEnd) {
			return *(String*)&any;
		}
		jfl_notimpl;
	}break;
	}
}

Boolean anycast(Any any, const TypeInfo* ty, void* out){
	// double dispatch on any.tag and ty
	if ( ty == &T_String ){
		*(String*)out = to_string(any);
		return true;
	}else if ( ty == &T_Boolean ){
		*(Boolean*)out = to_boolean(any);
		return true;
	}else if ( ty == &T_Number ){
		*(Number*)out = to_number(any);
		return true;
	}else{
		if ( any.tag == type_tag::Object ){
			return any.object->jfl_vtable->jfl_tinfo->typetest(any.object, ty, out);
		}
	}
	jfl_notimpl;
}

Boolean dyn_in(Any object, Any key){
	switch( object.tag ){
	case type_tag::Object:{
		return object.object->jfl_vtable->jfl_tinfo->index(object.object, key, nullptr, 2);
	}break;
	}
	jfl_notimpl;
}

Any dyn_get(Any object, Any key){
	switch (object.tag) {
	case type_tag::Object: {
		Any out;
		object.object->jfl_vtable->jfl_tinfo->index(object.object, key, &out, 1);
		return out;
	}break;
	}
	jfl_notimpl;
}

void dyn_set(Any object, Any key, Any value){
	switch (object.tag) {
	case type_tag::Object: {
		object.object->jfl_vtable->jfl_tinfo->index(object.object, key, &value, 0);
		return;
	}break;
	}
	jfl_notimpl;
}

void dyn_del(Any object, Any key) {
	switch (object.tag) {
	case type_tag::Object: {
		object.object->jfl_vtable->jfl_tinfo->index(object.object, key, nullptr, 3);
		return;
	}break;
	}
	jfl_notimpl;
}

Boolean func_eq(Function lhs, Function rhs){
	return lhs.tag == rhs.tag && lhs.offset == rhs.offset && lhs.object == rhs.object;
}

size_t dyn_hash(Any key) {
	switch ( key.tag ){
	case type_tag::Null:
	case type_tag::Undefined:{
		return 0;
	}break;
	case type_tag::Boolean:
	case type_tag::UInt:
	case type_tag::Int:
	case type_tag::Number:{
		return dyn_hash(to_string(key));
	}break;
	case type_tag::Function:{
		Function* func = (Function*)&key;
		return ((size_t)func->object) ^ func->offset;
	}break;
	case type_tag::Object:{
		return (size_t)key.object;
	}break;
	default:{
		if ( key.tag <= type_tag::StringEnd ){
			return std::hash<std::string_view>()(((String*)&key)->to_view());
		}
		jfl_notimpl;
	}break;
	}
}

Boolean dyn_eq(Any lhs, Any rhs) {
	if ( lhs.tag == type_tag::Object ){
		if ( rhs.tag == type_tag::Object ){
			return lhs.object == rhs.object;
		}
		return false;
	}
	if (lhs.tag == type_tag::Function) {
		if (rhs.tag == type_tag::Function) {
			return func_eq(*(Function*)&lhs, *(Function*)&rhs);
		}
		return false;
	}

	if ( lhs.tag <= type_tag::StringEnd && rhs.tag <= type_tag::StringEnd ){
		return ((String*)&lhs)->to_view() == ((String*)&rhs)->to_view();
	}

	jfl_notimpl;
}

Boolean dyn_cmp(Any lhs, Any rhs, cmp_op op){
	if ( op == cmp_op::eq || op == cmp_op::seq ){
		return dyn_eq(lhs, rhs);
	}else{
		return !dyn_eq(lhs, rhs);
	}
}

Any Function::invoke(RestParams params){
	GenericFunc func = (GenericFunc)(((intptr_t)GenericFuncOffsetBase) + this->offset);
	return func(object, params);
}

void any_iter(Any any, Iterator* iter){
	switch ( any.tag ){
	case type_tag::Object:{
		Object* object = any.object;
		object->jfl_vtable->jfl_tinfo->iterate(object, iter);
	}break;
	default:{
		jfl_notimpl;
	}break;
	}
}

void exc_resume(){
	jfl_notimpl;
}

void exc_throw(Any any){
	jfl_notimpl;
}

fl::Array* rest_to_array(RestParams params){
	fl::Array* arr = fl::Array::jfl_New({});
	for ( UInt i = 0; i < params.size(); i++ ){
		arr->jfl_dynamic.map.insert({i, params.at(i)});
	}
	return arr;
}
*/
}
