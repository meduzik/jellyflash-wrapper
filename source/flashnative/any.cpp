#include <jfl/common_impl.h>
#include <jfl/any.h>
#include <jfl/math/murmur.h>
#include <jfl/object.h>
#include <jfl/type_conversions.h>
#include <jfl/rest_params.h>
#include <jfl/rest_ops.h>

namespace jfl{

const jfl::VTable NumberBox_VT = {
	{
		nullptr,
		(sizeof(NumberBox) + 31) & ~31,
		nullptr,
		nullptr,
		nullptr,
		nullptr
	}
};

NumberBox* NumberBox::Make(Number value) {
	NumberBox* object = jfl::gc_new<NumberBox>();
	object->jfl_vtable = &NumberBox_VT;
	object->value = value;
	return object;
}

jfl_noinline
Any any_get(Any object, Any key) {
	switch (object.tag) {
	case type_tag::Object: {
		Any value;
		object.object->jfl_vtable->jfl_vth.dynamic(object.object, &key, &value, dynamic_op::get);
		return value;
	}break;
	case type_tag::Rest:{
		Any uint_key = any_as_uint(key);
		if (uint_key.tag != type_tag::UInt ){
			raise_get_error(any_class_of(key), key);
		}
		return rest_get(object.rest, uint_key.uval);
	}break;
	default: {
		raise_get_error(any_class_of(object), key);
	}break;
	}
}

jfl_noinline
void any_set(Any object, Any key, Any value) {
	switch (object.tag) {
	case type_tag::Object: {
		object.object->jfl_vtable->jfl_vth.dynamic(object.object, &key, &value, dynamic_op::set);
	}break;
	default: {
		raise_set_error(any_class_of(object), key);
	}break;
	}
}

jfl_noinline
void any_del(Any object, Any key){
	switch (object.tag) {
	case type_tag::Object: {
		Any value;
		object.object->jfl_vtable->jfl_vth.dynamic(object.object, &key, &value, dynamic_op::del);
	}break;
	default: {
		raise_del_error(any_class_of(object), key);
	}break;
	}
}

Boolean any_in(Any object, Any key){
	switch (object.tag) {
	case type_tag::Object: {
		Any value;
		return object.object->jfl_vtable->jfl_vth.dynamic(object.object, &key, &value, dynamic_op::in);
	}break;
	default: {
		raise_in_error(any_class_of(object), key);
	}break;
	}
}

jfl_noinline
void any_iter(Any object, Iterator* iter){
	switch (object.tag) {
	case type_tag::Null:
	case type_tag::Undefined:{
		iter->next = [](Iterator* iterator, Any* key, Any* value) -> Boolean {
			return false;
		};
	}break;
	case type_tag::Object: {
		object.object->jfl_vtable->jfl_vth.iterate(object.object, iter);
	}break;
	default: {
		raise_iter_error(any_class_of(object));
	}break;
	}
}

jfl_noinline
void null_iter(Iterator* iter){
	iter->next = [](Iterator* iterator, Any* key, Any* value) -> Boolean {
		return false;
	};
}

jfl_noinline
Boolean any_seq(Any lhs, Any rhs){
	bool lhs_numeric = (
		lhs.tag == type_tag::Int 
		||
		lhs.tag == type_tag::UInt 
		||
		lhs.tag == type_tag::NumberBox
		||
		lhs.tag == type_tag::Float
	);
	bool rhs_numeric = (
		rhs.tag == type_tag::Int
		||
		rhs.tag == type_tag::UInt
		||
		rhs.tag == type_tag::NumberBox
		||
		rhs.tag == type_tag::Float
	);
	if ( lhs_numeric && rhs_numeric ){
		return any_to_Number(lhs) == any_to_Number(rhs);
	}else if ( lhs.tag != rhs.tag ){
		return false;
	}else{
		switch ( lhs.tag ){
		case type_tag::Null:
		case type_tag::Undefined: return true;
		case type_tag::Boolean: return lhs.bval == rhs.bval;
		case type_tag::Rest: return lhs.rest == rhs.rest;
		case type_tag::Object: return lhs.object == rhs.object;
		case type_tag::SmallStringBegin: return true;
		case type_tag::BigString: return str_eq(*(String*)&lhs, *(String*)&rhs);
		default:{
			if ( lhs.tag <= type_tag::SmallStringEnd ){
				return !memcmp((*(String*)&lhs).ss.value, (*(String*)&rhs).ss.value, (size_t)lhs.tag);
			} else if ((uint8_t)lhs.tag & (uint8_t)type_tag::FunctionBit) {
				return func_eq(*(Function*)&lhs, *(Function*)&rhs);
			}else{
				jfl_unreachable;
			}
		}break;
		}
	}
}

jfl_noinline
Boolean any_eq(Any lhs, Any rhs){
	if (lhs.tag == type_tag::Object) {
		if (rhs.tag == type_tag::Object) {
			return lhs.object == rhs.object;
		}
		return false;
	}
	if (lhs.tag >= type_tag::FunctionBit) {
		if (rhs.tag >= type_tag::FunctionBit) {
			return func_eq(*(Function*)&lhs, *(Function*)&rhs);
		}
		return false;
	}
	if (lhs.tag <= type_tag::StringEnd && rhs.tag <= type_tag::StringEnd) {
		return str_eq(*(String*)&lhs, *(String*)&rhs);
	}

	bool lhs_null = (lhs.tag == type_tag::Null || lhs.tag == type_tag::Undefined);
	bool rhs_null = (rhs.tag == type_tag::Null || rhs.tag == type_tag::Undefined);
	if ( lhs_null || rhs_null ){
		return lhs_null == rhs_null;
	}

	bool lhs_number = (lhs.tag == type_tag::Int || lhs.tag == type_tag::UInt || lhs.tag == type_tag::NumberBox || lhs.tag == type_tag::Float);
	bool rhs_number = (rhs.tag == type_tag::Int || rhs.tag == type_tag::UInt || rhs.tag == type_tag::NumberBox || rhs.tag == type_tag::Float);
	if ( lhs_number && rhs_number ){
		Number lhs_value = any_to_Number(lhs);
		Number rhs_value = any_to_Number(rhs);
		return lhs_value == rhs_value;
	}

	if ( (lhs.tag == type_tag::Object) != (rhs.tag == type_tag::Object) ){
		return false;
	}
	if (lhs.tag == type_tag::Boolean) {
		if (rhs.tag == type_tag::Boolean) {
			return lhs.bval == rhs.bval;
		}
		return false;
	}
	jfl_notimpl;
}

const Class* any_class_of(Any val){
	switch ( val.tag ){
	case type_tag::Null: return &Class_Null;
	case type_tag::Undefined: return &Class_Undefined;
	case type_tag::Boolean: return &Class_Boolean;
	case type_tag::Int: return &Class_Int;
	case type_tag::UInt: return &Class_UInt;
	case type_tag::NumberBox: return &Class_Number;
	case type_tag::Float: return &Class_Number;
	case type_tag::Rest: return &Class_Rest;
	case type_tag::Object: return class_of(val.object);
	default:{
		if ( val.tag <= type_tag::StringEnd ){
			return &Class_String;
		} else if (val.tag >= type_tag::FunctionBit) {
			return &Class_Function;
		}else{
			jfl_unreachable;
		}
	}break;
	}
}

std::unordered_map<std::string_view, Any(*)()>& get_definition_registry() {
	static std::unordered_map<std::string_view, Any(*)()> registry;
	return registry;
}

void register_definition(std::string_view name, Any(*accessor)()){
	get_definition_registry().insert({name, accessor});
}


}

fl::Object* fl::Object::jfl_New() {
	fl::Object* object = jfl::gc_new<fl::Object>();
	object->jfl_vtable = &fl::jfl_V_Object;
	return object;
}
