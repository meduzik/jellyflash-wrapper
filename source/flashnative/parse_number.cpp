#include <jfl/common_impl.h>
#include <jfl/types.h>
#include <fl/Number.h>

namespace jfl{
uint8_t decode_digit(uint8_t ch) {
	if (ch >= '0' && ch <= '9') {
		return ch - '0';
	}
	if (ch >= 'A' && ch <= 'Z') {
		return ch - 'A' + 10;
	}
	if (ch >= 'a' && ch <= 'z') {
		return ch - 'a' + 10;
	}
	return 0xffu;
}

Number parseInt_impl(const uint8_t* buffer, size_t length, jfl::Int radix) {
	Number acc = 0;
	bool had_digit = false;
	for (size_t i = 0; i < length; i++) {
		uint8_t ch = buffer[i];
		uint8_t digit = decode_digit(ch);
		if (digit >= radix) {
			if (!had_digit || !std::isfinite(acc)) {
				return jfl::NaN;
			} else {
				return acc;
			}
		} else {
			had_digit = true;
			acc = acc * radix + digit;
		}
	}
	if (!had_digit || !std::isfinite(acc)) {
		return jfl::NaN;
	}
	return acc;
}

Number parseInt(String str, Int radix) {
	auto[buffer, length] = str.to_view();
	if (radix == 0) {
		if (length >= 2 && buffer[0] == '0' && buffer[1] == 'x') {
			return parseInt_impl(buffer + 2, length - 2, 16);
		} else {
			return parseInt_impl(buffer, length, 10);
		}
	} else {
		return parseInt_impl(buffer, length, radix);
	}
}

String NumberToString(Number val){
	return fl::Number::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toString(val, 10);
}


static char NumberBuffer[256];

Number parseNumber(std::string_view sv, bool* err){
	if ( sv.size() > sizeof(NumberBuffer) - 1 ){
		*err = true;
		return NaN;
	}
	if (sv.size() == 0) {
		*err = true;
		return NaN;
	}
	memcpy(NumberBuffer, sv.data(), sv.size());
	NumberBuffer[sv.size()] = 0;
	double val = atof(NumberBuffer);
	*err = false;
	return val;
}


}