#include <jfl/common_impl.h>
#include <jfl/function.h>

namespace jfl{

Any GenericFuncOffsetBase(Object* object, RestParams params){
	jfl_notimpl;
}

Any Function::invoke(RestParams* params){
	if (!has_function()){
		raise_call_error(any_class_of(*(Any*)this));
	}
	uint32_t decoded_offset = (offset & 0x7fu) | ((offset & 0xffffff00u) >> 1);
	int32_t ioffset = decoded_offset;
	if (offset & 0x80000000u){
		ioffset |= 0x80000000u;
	}
	GenericFunc* func = (GenericFunc*)(((intptr_t)(&GenericFuncOffsetBase)) + (intptr_t)ioffset);
	return func(object, params);
}

Function::Function(Object* object, GenericFunc func) :
	object(object)
{
	uint32_t funcoffset = (uint32_t)(int32_t)((intptr_t)func - (intptr_t)GenericFuncOffsetBase);
	uint32_t encoded_offset = (funcoffset & 0x7fu) | ((funcoffset & 0x7fffff80u) << 1);
	
	offset = encoded_offset | ((uint32_t)type_tag::FunctionBit);
}

}