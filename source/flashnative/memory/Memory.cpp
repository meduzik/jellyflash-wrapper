#include <jfl/memory/Memory.h>
#include "MemoryInternal.h"
#include <jfl/error.h>

namespace jfl{

#if defined(_MSC_VER) && defined(JFL_PROFILE_MEMORY)
	VSHeapTracker::CHeapTracker* UnmanagedTracker = new VSHeapTracker::CHeapTracker("Unmanaged");
	VSHeapTracker::CHeapTracker* ManagedTracker = new VSHeapTracker::CHeapTracker("Managed");
#endif

#if defined(JFL_MEMORY_CANARY)
#define JFL_CANARY_SIZE 8
#define JFL_CANARY_FILL_PRE 0xDC
#define JFL_CANARY_FILL_POST 0xCD
#define JFL_FREED_FILL 0xDE
#define JFL_ALLOCATED_FILL 0x9E

std::map<void*, bool> AllAllocations;

JFL_ALLOCATOR
void* unmanaged_allocate(size_t size){
	size_t total_size = size + JFL_CANARY_SIZE * 2;
	uint8_t* block = (uint8_t*)malloc(total_size);
	
	void* ptr = block + JFL_CANARY_SIZE;
	memset(block, JFL_CANARY_FILL_PRE, JFL_CANARY_SIZE);
	memset(block + JFL_CANARY_SIZE, JFL_ALLOCATED_FILL, size);
	memset(block + size + JFL_CANARY_SIZE, JFL_CANARY_FILL_POST, JFL_CANARY_SIZE);
	JFL_ALLOCATE_EVENT(Unmanaged, ptr, size);
	return ptr;
}

static const char hex[] = "0123456789abcdef";

void print_bytes(uint8_t* ptr, size_t size) {
	for (size_t offset = 0; offset < size; offset += 16) {
		for (size_t i = 0; i < std::min((size_t)16u, size - offset); i++) {
			std::cout << hex[ptr[i + offset] >> 4] << hex[ptr[i + offset] & 15] << ' ';
		}
		std::cout << "\n";
	}
}

static uint8_t test_block[256];

bool check_canary(uint8_t* block, uint8_t ch);

void report_canary_error(uint8_t* ptr, size_t size) {
	memcpy(test_block, ptr, JFL_CANARY_SIZE);
	memcpy(test_block + JFL_CANARY_SIZE, ptr + size - JFL_CANARY_SIZE, JFL_CANARY_SIZE);
	if (!check_canary(ptr, JFL_CANARY_FILL_PRE)) {
		std::cout << "Canary 1 corrupted!\n";
	}
	if (!check_canary(ptr + size - JFL_CANARY_SIZE, JFL_CANARY_FILL_POST)) {
		std::cout << "Canary 2 corrupted!\n";
	}
	if (!check_canary(test_block, JFL_CANARY_FILL_PRE)) {
		std::cout << "Canary 1 copy corrupted!\n";
	}
	if (!check_canary(test_block + JFL_CANARY_SIZE, JFL_CANARY_FILL_POST)) {
		std::cout << "Canary 2 copy corrupted!\n";
	}
	std::cout << "Canary corrupted! Memory block of size " << size << " around " << (void*)ptr << ":\n";
	std::cout << "Canary 1:\n";
	print_bytes(ptr, JFL_CANARY_SIZE);
	std::cout << "Canary 1 copy:\n";
	print_bytes(test_block, JFL_CANARY_SIZE);
	std::cout << "Contents:\n";
	print_bytes(ptr + JFL_CANARY_SIZE, size - 2 * JFL_CANARY_SIZE);
	std::cout << "Canary 2:\n";
	print_bytes(ptr + size - JFL_CANARY_SIZE, JFL_CANARY_SIZE);
	std::cout << "Canary 2 copy:\n";
	print_bytes(test_block + JFL_CANARY_SIZE, JFL_CANARY_SIZE);
	uint8_t* end = ptr + size;
	std::cout << "Some bytes around the problem:\n";
	constexpr size_t extra_block_size = 16;
	std::cout << "+" << "\n";
	for (int offset = 0; offset < 16; offset++) {
		print_bytes(end + offset * extra_block_size, extra_block_size);
	}
	std::cout << "-" << "\n";
	for (int offset = 0; offset < 16; offset++) {
		print_bytes(ptr + (-16 + offset) * extra_block_size, extra_block_size);
	}
	raise_message("canary corrupted");
}

bool check_canary(uint8_t* block, uint8_t ch) {
	for (size_t i = 0; i < JFL_CANARY_SIZE; i++) {
		if (block[i] != ch) {
			return false;
		}
	}
	return true;
}

void unmanaged_free(void* ptr, size_t size){
	if (!ptr) {
		return;
	}

	uint8_t* block = ((uint8_t*)ptr) - JFL_CANARY_SIZE;
	JFL_DEALLOCATE_EVENT(Unmanaged, ptr, size);
	if (!(check_canary(block, JFL_CANARY_FILL_PRE) && check_canary(block + JFL_CANARY_SIZE + size, JFL_CANARY_FILL_POST))) {
		report_canary_error(block, size + JFL_CANARY_SIZE * 2);
	}
	memset(block, JFL_FREED_FILL, size + 2 * JFL_CANARY_SIZE);
	free(block);
}
#else
JFL_ALLOCATOR
	void* unmanaged_allocate(size_t size) {
	void* ptr = malloc(size);
	JFL_ALLOCATE_EVENT(Unmanaged, ptr, size);
	return ptr;
}

void unmanaged_free(void* ptr, size_t size) {
	JFL_DEALLOCATE_EVENT(Unmanaged, ptr, size);
	free(ptr);
}
#endif

static const size_t ArenaPoolSize = 2 * 1024 * 1024 - sizeof(void*) * 4;
static const size_t ArenaLargeBlockSize = 64 * 1024;

struct ArenaPool{
	ArenaPool* prev;
	ArenaPool* next;

	alignas(std::max_align_t)
	uint8_t mem[ArenaPoolSize];
};

struct ArenaLargeBlock{
	alignas(std::max_align_t)
	ArenaLargeBlock* prev;
	size_t size;

	alignas(std::max_align_t)
	uint8_t payload[16];

	ArenaLargeBlock(size_t size);
};

static ArenaPool BasePool;
static ArenaPool* ArenaTopPool = &BasePool;
static ArenaLargeBlock* ArenaTopLargeBlock = nullptr;

static ArenaControlBlock ArenaCurrentBlock{ BasePool.mem, BasePool.mem + ArenaPoolSize };

static Arena GlobalArena(nullptr);
static Arena* TopArena = &GlobalArena;

ArenaLargeBlock::ArenaLargeBlock(size_t size):
	prev(ArenaTopLargeBlock),
	size(size)
{
	ArenaTopLargeBlock = this;
}

Arena::Arena(nullptr_t) :
	next(nullptr),
	mylevel(0),
	maxlevel(0),
	myblock(nullptr),
	mypool(ArenaTopPool),
	reset{nullptr, nullptr}
{
}

Arena::Arena():
	next(TopArena),
	mylevel(TopArena->mylevel + 1),
	maxlevel(mylevel),
	myblock(ArenaTopLargeBlock),
	mypool(ArenaTopPool),
	reset(ArenaCurrentBlock)
{
	TopArena = this;
}

Arena::~Arena(){
	if (maxlevel == mylevel){
		// stack barrier, release everything
		while (ArenaTopLargeBlock != myblock){
			auto prev = ArenaTopLargeBlock->prev;
			free(ArenaTopLargeBlock);
			ArenaTopLargeBlock = prev;
		}
		ArenaTopPool = mypool;
		ArenaCurrentBlock = reset;
	}else{
		// didn't release, propagate to the parent
		next->maxlevel = std::min(next->maxlevel, maxlevel);
	}
	TopArena = next;
}

JFL_ALLOCATOR
void* Arena::allocate(size_t size){
	if ( TopArena != this ){
		TopArena->maxlevel = std::min(TopArena->maxlevel, this->mylevel);
	}
	if (size >= ArenaLargeBlockSize){
		ArenaLargeBlock* new_block = (ArenaLargeBlock*)malloc(sizeof(ArenaLargeBlock) + size);
		new(new_block)ArenaLargeBlock(size);
		return new_block->payload;
	}
	size_t mem_consumed = (size + 15) & ~15;
	if ((size_t)(ArenaCurrentBlock.end - ArenaCurrentBlock.cur) < mem_consumed){
		if (!ArenaTopPool->next){
			ArenaPool* new_pool = (ArenaPool*)malloc(sizeof(ArenaPool));
			new_pool->prev = ArenaTopPool;
			new_pool->next = nullptr;
			ArenaTopPool->next = new_pool;
		}
		ArenaTopPool = ArenaTopPool->next;
		ArenaCurrentBlock = {ArenaTopPool->mem, ArenaTopPool->mem + ArenaPoolSize};
	}
	void* result = ArenaCurrentBlock.cur;
	ArenaCurrentBlock.cur += mem_consumed;
	return result;
}

static uint8_t* ScratchSpace = nullptr;
static size_t ScratchSpaceSize = 0;

uint8_t* GetScratchSpace(size_t size){
	if (ScratchSpaceSize < size) {
		free(ScratchSpace);
		ScratchSpaceSize = size;
		ScratchSpace = (uint8_t*)malloc(size);
	}
	return ScratchSpace;
}

}
