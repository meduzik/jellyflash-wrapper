#pragma once
#include <jfl/memory/Memory.h>

namespace jfl {

#if defined(_MSC_VER) && defined(JFL_PROFILE_MEMORY)
	extern VSHeapTracker::CHeapTracker* UnmanagedTracker;
	extern VSHeapTracker::CHeapTracker* ManagedTracker;
	#define JFL_ALLOCATE_EVENT(heap, ptr, size) heap##Tracker->AllocateEvent(ptr, size)
	#define JFL_DEALLOCATE_EVENT(heap, ptr, size) heap##Tracker->DeallocateEvent(ptr)
#else
	#define JFL_ALLOCATE_EVENT(heap, ptr, size)
	#define JFL_DEALLOCATE_EVENT(heap, ptr, size)
#endif

}
