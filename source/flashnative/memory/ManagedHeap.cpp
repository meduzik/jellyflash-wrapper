#include <jfl/memory/ManagedHeap.h>
#include <jfl/vtable.h>
#include <jfl/object.h>
#include <jfl/class.h>
#include "MemoryInternal.h"
#include <jfl/app/Loop.h>
#include <jfl/dynamic.h>
#include <fl/jellyflash/system/GC.h>

#define MANAGED_HEAP_DEBUG 1
#define MANAGED_HEAP_MIN_SIZE (32 * 1024 * 1024)
#define MINIMUM_GC_STEP (8 * 1024 * 1024)

// #define MANAGED_HEAP_SNAPSHOT 1

namespace jfl{

static constexpr size_t GCNodeAlignment = 32;

void* const FreeSpaceToken = (void*)0xF0000001;
void* const MarkedObjectToken = (void*)0xF0000002;

class ManagedHeap{
public:
	//
	constexpr ManagedHeap():
		heap_size(0),
		heap_space(nullptr),
		first_node(nullptr),
		last_node(nullptr)
	{}

	ManagedHeap(uint8_t* heap_space, size_t heap_size);

	void* allocate(size_t size);
	void reclaim();

	struct FreeNode {
		void* ptr;
		FreeNode* prev;
		FreeNode* next;
		size_t size;
	};

	size_t heap_size;
	uint8_t* heap_space;
	FreeNode* first_node;
	FreeNode* last_node;
};

alignas(GCNodeAlignment)
static uint8_t PreallocatedHeap[MANAGED_HEAP_MIN_SIZE];
static ManagedHeap MainHeap(PreallocatedHeap, sizeof(PreallocatedHeap));

static size_t TotalHeaps = 1;
static constexpr size_t HeapSizes[] = {
	MANAGED_HEAP_MIN_SIZE, 
	MANAGED_HEAP_MIN_SIZE, MANAGED_HEAP_MIN_SIZE * 2, MANAGED_HEAP_MIN_SIZE * 4
};
static constexpr size_t MaxHeaps = sizeof(HeapSizes) / sizeof(HeapSizes[0]);
static ManagedHeap Heaps[MaxHeaps] = {
	{PreallocatedHeap, sizeof(PreallocatedHeap)}
};

// the total amount of memory across all the managed heaps
static size_t TotalMemory = sizeof(PreallocatedHeap);
// the amount of memory currently in use
static size_t UsedMemory = 0;

// the amount of used memory when the next collection should trigger
static size_t GCThreshold = TotalMemory * 8 / 10;
// the amount of used memory after the last gc cycle
static size_t GCMark = 0;

JFL_ALLOCATOR
void* gc_allocate(size_t size) {
	size = (size + 31) & ~31;
	void* ptr = nullptr;
	for (size_t i = 0; i < TotalHeaps; i++) {
		ptr = Heaps[i].allocate(size);
		if (ptr){
			break;
		}
	}
	if (!ptr){
		if (TotalHeaps >= MaxHeaps){
			raise_message("managed heap exhausted");
		}

		size_t heap_size = HeapSizes[TotalHeaps];
		uint8_t* memory = (uint8_t*)unmanaged_allocate(heap_size);
		Heaps[TotalHeaps] = ManagedHeap(memory, heap_size);
		GCThreshold += heap_size;
		ptr = Heaps[TotalHeaps].allocate(size);
		TotalHeaps++;
	}
	JFL_ALLOCATE_EVENT(Managed, ptr, size);
	UsedMemory += size;
	return ptr;
}

ManagedHeap::ManagedHeap(uint8_t* heap_space, size_t heap_size):
	heap_size(heap_size),
	heap_space(heap_space),
	first_node(nullptr),
	last_node(nullptr)
{
	assert(heap_size % GCNodeAlignment == 0);
	assert((intptr_t)heap_space % GCNodeAlignment == 0);

#if defined(MANAGED_HEAP_DEBUG)
	memset(heap_space, 0xCC, heap_size);
#endif

	first_node = (FreeNode*)heap_space;
	first_node->ptr = FreeSpaceToken;
	first_node->next = nullptr;
	first_node->prev = nullptr;
	first_node->size = heap_size;
	last_node = first_node;
}

void* ManagedHeap::allocate(size_t size){
	assert(size % GCNodeAlignment == 0);
	FreeNode* node = last_node;
	while ( node ){
		if ( node->size < size ){
			node = node->prev;
		}else{
			if ( node->size > size ){
				// split the node (allocate from the end)
				uint8_t* space = ((uint8_t*)node) + node->size - size;
				node->size -= size;
				assert(node->size >= GCNodeAlignment);
				assert(node->size % GCNodeAlignment == 0);
			#if defined(MANAGED_HEAP_DEBUG)
				memset(space, 0xA1, size);
			#endif
				return space;
			}else{
				// consume the entire node
				if ( node->prev ){
					node->prev->next = node->next;
				}else{
					first_node = node->next;
				}
				if ( node->next ){
					node->next->prev = node->prev;
				}else{
					last_node = node->prev;
				}
			#if defined(MANAGED_HEAP_DEBUG)
				memset(node, 0xA1, size);
			#endif
				return node;
			}
		}
	}
	// heap exhausted
	node = first_node;
	size_t fragmented_size = 0;
	while (node) {
		fragmented_size += node->size;
		node = node->next;
	}
	std::cout << "Cannot allocate " << size << ", total fragmented size " << fragmented_size << std::endl;
	assert(false);
	return nullptr;
}

#if defined(MANAGED_HEAP_SNAPSHOT)
struct hash_t{
	uint64_t data[3];

	bool operator<(const hash_t& other) const{
		return memcmp(data, other.data, sizeof(data)) < 0;
	}
};

struct GraphNode {
	Object* repr;
	GraphNode* parent;
	std::string_view name;
	size_t count;
	std::unordered_map<std::string_view, GraphNode*> edges;
};

std::unordered_map< Object*, GraphNode* > Graph;
std::vector<std::unique_ptr<GraphNode>> Nodes;

const Class* safe_class_of(Object* object) {
	const VTableHeader& vth = object->jfl_vtable->jfl_vth;
	if (vth.ti) {
		return (const Class*)vth.ti->values[vth.ti->num_classes - 1];
	}
	return nullptr;
}
#endif

struct GCContext {
#if defined(MANAGED_HEAP_SNAPSHOT)
	void start() {
		reclaimed = 0;
		kept = 0;
		Graph.clear();
		Nodes.clear();
	}

	void finish() {
		// std::ostringstream oss;
		std::ofstream oss;
		oss.open("heap_snapshot.dot", std::ios::binary);
		oss << "digraph heap{\n";
		for (const auto& node: Nodes) {
			oss << "p" << (void*)node.get()
				<< " [label=\"" << node->name << "(" << node->count << ")" << "\""
				<< ", color=";

			if (node->count == 1) {
				oss << "grey";
			} else if (node->count <= 5) {
				oss << "green";
			} else if (node->count <= 20) {
				oss << "yellow";
			} else {
				oss << "red";
			}

			oss << ", width=" << node->count
				<< "];\n";
		}
		for (const auto& node : Nodes) {
			for (auto& pair : node->edges) {
				oss << "p" << node.get() << " -> " << "p" << (void*)pair.second << "\n";
			}
		}
		oss << "}\n";
		//std::cerr << "======= GRAPH BEGIN =======\n" << oss.str() << "======= GRAPH END =======\n" << std::endl;
		/*
		std::map<hash_t, Object*> repr;
		std::map<Object*, Object*> map;
		std::map<Object*, size_t> counts;

		for (auto& node: Graph){
			hash_t h = hash_object(node.first, node.second);
			auto it = repr.find(h);
			if (it == repr.end()){
				repr.insert({h, node.first});
				map.insert({node.first, node.first});
			}else{
				map.insert({ node.first, it->second });
			}
			counts[repr.at(h)]++;
		}

		out.open("heap_snapshot.dot", std::ios::binary);
		out << "digraph heap{\n";
		for (auto& pair: repr){
			auto node = pair.second;
			size_t count = counts.at(node);
			out << "p" << (void*)node
				<< " [label=\"" << class_of(node)->name.to_string_view() << "(" << count << ")" << "\""
				<< ", color=";

			if ( count == 1 ){
				out << "grey";
			}else if ( count <= 5 ){
				out << "green";
			}else if ( count <= 20 ){
				out << "yellow";
			}else{
				out << "red";
			}

			out << ", width=" << count
				<<"];\n";
		}
		out << "root" << " [label=\"ROOT\", color=magenta];\n";
		for (auto root: Roots){
			out << "p" << (void*)root << " [label=\"root_" << (void*)root << "\", color=blue];\n";
		}
		for (auto root : Roots) {
			out << "root -> p" << (void*)root << ";\n";
		}
		for (auto it : Graph) {
			for (auto& edge: it.second){
				if ( Roots.count(edge) ){
					out << "p" << edge << " -> " << "p" << (void*)map.at(it.first) << "\n";
				}else{
					out << "p" << (void*)map.at((Object*)edge) << " -> " << "p" << (void*)map.at(it.first) << "\n";
				}
			}
		}
		out << "}\n";
		out.close();
		*/
	}

	void select(Object* object){
		cur = object;
	}

	void select(GCRoot* root) {
		cur = nullptr;
	}

	void draw(Object* object){
		if (!object){
			return;
		}
		if (Graph.count(object) > 0) {
			return;
		}
		GraphNode* parent;
		if (cur) {
			parent = Graph.at(cur);
		} else {
			parent = nullptr;
		}
		GraphNode* node;
		const Class* clss = safe_class_of(object);
		std::string_view name = clss ? clss->name.to_string_view() : "<?>";
		if (parent == nullptr) {
			node = new GraphNode;
			node->parent = parent;
			node->repr = object;
			node->name = name;
			node->count = 1;
			Nodes.push_back(std::unique_ptr<GraphNode>(node));
		} else {
			auto it = parent->edges.find(name);
			if (it == parent->edges.end()) {
				node = new GraphNode;
				node->parent = parent;
				node->repr = object;
				node->name = name;
				node->count = 1;
				parent->edges.insert(std::make_pair(name, node));
				Nodes.push_back(std::unique_ptr<GraphNode>(node));
			} else {
				node = it->second;
				node->count++;
			}
		}
		Graph.insert(std::make_pair(object, node));
	}

	void collect(Object* object){
		reclaimed++;
	}

	void survive(Object* object) {
		kept++;
	}

	std::ofstream out;
	Object* cur;
#else
	void start() {
		reclaimed = 0;
		kept = 0;
	}

	void finish() {
	}

	void select(Object* object) {
	}

	void select(GCRoot* root) {
	}

	void draw(Object* object) {
	}

	void collect(Object* object) {
		reclaimed++;
	}

	void survive(Object* object) {
		kept++;
	}
#endif
	size_t reclaimed, kept;
};

static GCContext GCCtx;

#define GC_REFS_WEAK_BIT (1 << 30)

void ManagedHeap::reclaim(){
	uint8_t* end = heap_space + heap_size;
	uint8_t* ptr = heap_space;
	FreeNode first_free_node{nullptr, nullptr, nullptr, 0};
	FreeNode* last_free_node = &first_free_node;
	while ( ptr != end ){
		void* vtable = *(void**)ptr;
		if (vtable == FreeSpaceToken){
			// found free space node
			FreeNode* next_node = (FreeNode*)ptr;
			size_t node_size = next_node->size;
			if (((uint8_t*)last_free_node) + last_free_node->size == ptr) {
				// merge into the last
				last_free_node->size += node_size;
			}else{
				// start a new node
				next_node->prev = last_free_node;
				last_free_node->next = next_node;
				last_free_node = next_node;
			}
			assert(((uint8_t*)last_free_node) + last_free_node->size <= heap_space + heap_size);
			ptr += node_size;
		}else{
			// found object
			Object* object = (Object*)ptr;
			size_t object_size = object->jfl_vtable->jfl_vth.size;
			assert(object_size % GCNodeAlignment == 0);
			assert(object_size + ptr <= end);
			assert(object_size + ptr > ptr);
			if ( object->jfl_header.gcptr ){
				// live, skip to the next
				GCCtx.survive(object);

				object->jfl_header.gcptr = nullptr;
			}else{
				// dead, either merge into the last FreeNode,
				// or start a new free node
				GCCtx.collect(object);

				// run finalizer first
				auto finalizer = object->jfl_vtable->jfl_vth.finalize;
				if (finalizer){
					finalizer(object);
				}

				// record deallocation
				JFL_DEALLOCATE_EVENT(Managed, ptr, object_size);
				UsedMemory -= object_size;

			#if defined(MANAGED_HEAP_DEBUG)
				memset(ptr, 0xCD, object_size);
			#endif

				// then reclaim the memory
				if ( ((uint8_t*)last_free_node) + last_free_node->size == ptr ){
					last_free_node->size += object_size;
				}else{
					FreeNode* next_node = (FreeNode*)ptr;
					next_node->prev = last_free_node;
					next_node->next = nullptr;
					next_node->ptr = FreeSpaceToken;
					next_node->size = object_size;
					last_free_node->next = next_node;
					last_free_node = next_node;
				}
				assert(((uint8_t*)last_free_node) + last_free_node->size <= heap_space + heap_size);
			}
			ptr += object_size;
		}
	}
	if ( last_free_node == &first_free_node ){
		first_node = nullptr;
		last_node = nullptr;
	}else{
		first_node = first_free_node.next;
		last_free_node->next = nullptr;
		first_node->prev = nullptr;
		last_node = last_free_node;
	}

}

void gc_visit_dynamic(Dynamic& dynamic){
	for (auto it: dynamic.map){
		gc_visit_any(it.first);
		gc_visit_any(it.second);
	}
}

Object* gc_any_get_object(Any val){
	switch (val.tag){
	case type_tag::Object: {
		return val.object;
	}break;
	default:{
		if (val.tag >= type_tag::FunctionBit) {
			return val.object;
		}
		return nullptr;
	}break;
	}
}

struct WeakRefID{
	Dynamic* owner;
	Dynamic::map_t::iterator it;
	Any value;
};

std::unordered_multimap< Object*, WeakRefID > GCWeakRefs;

void gc_visit_weak_dynamic(Dynamic& dynamic){
	bool had_weak_keys = false;
	auto& map = dynamic.map;
	for (auto it = map.begin(); it != map.end(); it++) {
		Object* key_object = gc_any_get_object(it->first);
		if (!key_object){
			// non collectable object
			gc_visit_any(it->second);
		}else{
			if(key_object->jfl_header.gcptr){
				// already marked
				gc_visit_any(it->second);
			}else{
				// not marked - the decision must be delayed
				had_weak_keys = true;
				key_object->jfl_header.refs |= GC_REFS_WEAK_BIT;
				GCWeakRefs.insert({key_object, WeakRefID{&dynamic, it}});
			}
		}
	}
}

void gc_sweep_weak_dynamics(){
	for(auto pair: GCWeakRefs){
		auto& ref = pair.second;
		ref.owner->map.erase(ref.it);
	}
	GCWeakRefs.clear();
}

static GCRoot* FirstRoot = nullptr;

static void* QueueStart = nullptr;
static void* QueueEnd = nullptr;
static void* MarkQueue = &QueueStart;

void gc_visit(Object* object){
	GCCtx.draw(object);
	if ( object && !object->jfl_header.gcptr ){
		*(void**)MarkQueue = &object->jfl_header.gcptr;
		MarkQueue = &object->jfl_header.gcptr;
		object->jfl_header.gcptr = &QueueEnd;
	}
}

void gc_mark(){
	QueueStart = &QueueEnd;
	QueueEnd = nullptr;
	MarkQueue = &QueueStart;
	GCRoot* root = FirstRoot;
	while ( root ){
		GCCtx.select(root);
		root->gcfunc(root->udata);
		root = root->next;
	}
	void* iter = QueueStart;
	while ( iter != &QueueEnd ){
		Object* object = (Object*)(((uint8_t*)iter) - offsetof(Object, jfl_header.gcptr));
		GCCtx.select(object);

		if (object->jfl_header.refs & GC_REFS_WEAK_BIT) {
			auto range = GCWeakRefs.equal_range(object);
			assert(range.first != range.second);
			for (auto it = range.first; it != range.second; it++) {
				gc_visit_any(it->second.it->second);
			}
			GCWeakRefs.erase(object);
			object->jfl_header.refs &= ~GC_REFS_WEAK_BIT;
		}

		auto gc = object->jfl_vtable->jfl_vth.gc;
		if (gc){			
			gc(object, nullptr);
		}
		iter = *(void**)iter;
	}
}

void gc_real_collect(){
	GCCtx.start();
	auto start = std::chrono::high_resolution_clock::now();
	gc_mark();
	auto mark_finish = std::chrono::high_resolution_clock::now();

	for (size_t i = 0; i < TotalHeaps; i++){
		Heaps[i].reclaim();
	}

	gc_sweep_weak_dynamics();
	auto reclaim_finish = std::chrono::high_resolution_clock::now();
	GCCtx.finish();

	GCMark = UsedMemory;
	GCThreshold = std::max(GCMark + MINIMUM_GC_STEP, TotalMemory * 8 / 10);

	std::cout
		<< "GC performed"
		<< "\n  mark    =" << (mark_finish - start).count() / 1000000 << "ms. "
		<< "\n  reclaim =" << (reclaim_finish - mark_finish).count() / 1000000 << "ms. "
		<< "\n  freed " << GCCtx.reclaimed << " objects " 
		<< "(" << GCCtx.reclaimed * 100.0 / (GCCtx.kept + GCCtx.reclaimed) << "%)"
		<< "\n  " << GCCtx.kept << " objects alive" 
		<< "\nMemory used: " << UsedMemory / 1024 << " / " << TotalMemory / 1024 << "KB"
		<< std::endl;
}

void gc_full_collect(){
	gc_real_collect();
}

void gc_hint(double threshold){
	if ((UsedMemory - GCMark) / (GCThreshold - GCMark) >= threshold){
		gc_full_collect();
	}
}

static bool GCPending = false;

void gc_perform(){
	if ( !GCPending ){
		GCPending = true;
		PostMainLoopTask([](){
			using namespace fl::jellyflash::system;
			GC::jfl_p_notifyPreGC(GC::jfl_S_Instance);
			gc_full_collect();
			GCPending = false;
			GC::jfl_p_notifyPostGC(GC::jfl_S_Instance);
		});
	}
}

static void verify_gc_list(){
	GCRoot* hare = FirstRoot;
	GCRoot* turtle = FirstRoot;
	while ( hare && turtle ){
		hare = hare->next;
		if ( !hare ){
			return;
		}
		hare = hare->next;
		turtle = turtle->next;
		if ( hare == turtle ){
			jfl_notimpl;
		}
	}
}

void gc_add_root(GCRoot* root){
	if (FirstRoot){
		FirstRoot->prev = root;
	}
	root->prev = nullptr;
	root->next = FirstRoot;
	FirstRoot = root;
#if defined(VERIFY_GC_LIST)
	verify_gc_list();
#endif
}

void gc_remove_root(GCRoot* root){
	if (root->prev){
		root->prev->next = root->next;
	}else{
		FirstRoot = root->next;
	}
	if (root->next){
		root->next->prev = root->prev;
	}
#if defined(VERIFY_GC_LIST)
	verify_gc_list();
#endif
}

}


