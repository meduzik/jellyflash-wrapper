#include <jfl/string.h>

namespace jfl{

const StringObject_VT StringObject::jfl_VT{
	{
		&StringObject_TI,
		(sizeof(StringObject) + 31) & ~31,
		nullptr,
		[](Object* object){
			StringObject* string = (StringObject*)object;
			unmanaged_free((void*)string->data, string->length);
		},
		nullptr,
		nullptr
	}
};

StringObject* StringObject::New(UInt length){
	StringObject* string = gc_new<StringObject>();
	string->jfl_vtable = &StringObject::jfl_VT;
	string->length = length;
	uint8_t* data = (uint8_t*)unmanaged_allocate(length);
	string->data = data;
	return string;
}

String String::Make(const uint8_t* ptr, UInt len){
	if (len <= (UInt)type_tag::SmallStringEnd) {
		String string;
		string.tag = (type_tag)len;
		memcpy(string.ss.value, ptr, len);
		return string;
	} else {
		StringObject* string = StringObject::New(len);
		memcpy((void*)string->data, ptr, len);
		return string;
	}
}

String str_concat(String lhs, String rhs){
	if ( lhs.tag == type_tag::Null ){
		lhs = StringSmall{4, "null"};
	}
	if ( rhs.tag == type_tag::Null ){
		rhs = StringSmall{ 4, "null"};
	}
	auto [ptr1, len1] = lhs.to_view();
	auto [ptr2, len2] = rhs.to_view();
	UInt len = (UInt)(len1 + len2);
	if ( len <= (UInt)type_tag::SmallStringEnd ){
		String string;
		string.tag = (type_tag)len;
		memcpy(string.ss.value, ptr1, len1);
		memcpy(string.ss.value + len1, ptr2, len2);
		return string;
	}else{
		StringObject* string = StringObject::New(len);
		string->jfl_vtable = &StringObject::jfl_VT;
		string->length = len;
		memcpy((uint8_t*)string->data, ptr1, len1);
		memcpy((uint8_t*)string->data + len1, ptr2, len2);
		return string;
	}
}

}