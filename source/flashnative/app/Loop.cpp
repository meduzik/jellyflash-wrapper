#include <jfl/app/Loop.h>
#include <jfl/memory/ManagedHeap.h>
#include <jfl/config.h>

namespace jfl{

static std::mutex VMLock;
static std::mutex MainLoopQueueLock;

static std::queue< std::function<void()> > PendingTasks;

std::lock_guard<std::mutex> UnsafeLockVM(){
	return std::lock_guard<std::mutex>(VMLock);
}

void PostMainLoopTask(std::function<void()>&& func){
	std::lock_guard<std::mutex> lock(MainLoopQueueLock);
	PendingTasks.push(std::move(func));
}

void WaitMainLoopTask(std::function<void()>&& func){
#if defined(JFL_MULTITHREADED)
	bool completed = false;
	std::condition_variable cvar;
	std::mutex mutex;
	std::unique_lock lock(mutex);
	PostMainLoopTask([&](){
		{
			std::lock_guard<std::mutex> lock(mutex);
			func();
			completed = true;
		}
		cvar.notify_all();
	});
	cvar.wait(lock, [&](){ return completed; });
#else
	func();
#endif
}

void RunAsyncTask(std::function<void()>&& func){
#if defined(JFL_MULTITHREADED)
	std::thread thread(std::move(func));
	thread.detach(); 
#else
	PostMainLoopTask(std::move(func));
#endif
}

void ProcessMainLoop(){
	while ( true ){
		std::function<void()> func;

		{
			auto lock = std::lock_guard<std::mutex>(MainLoopQueueLock);
			if ( PendingTasks.empty() ){
				break;
			}
			func = std::move(PendingTasks.front());
			PendingTasks.pop();
		}

		{
			auto lock = UnsafeLockVM();
			func();
		}
	}

	auto lock = UnsafeLockVM();
	gc_hint(1.0);
}

}
