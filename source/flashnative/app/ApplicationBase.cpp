#include <jfl/memory/ManagedHeap.h>
#include <jfl/config.h>
#include <jfl/app/ApplicationBase.h>
#include <jfl/string.h>
#include <fl/flash/display/Stage.h>
#include <fl/flash/display/Stage3D.h>
#include <fl/flash/display/LoaderInfo.h>
#include <fl/flash/display/DisplayObjectContainer.h>
#include <fl/flash/display3D/Context3D.h>
#include <fl/flash/events/UncaughtErrorEvents.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/events/MouseEvent.h>
#include <fl/flash/events/EventDispatcher.h>
#include <fl/flash/utils/Timer.h>
#include <jfl/render/Context3D.h>
#include <jfl/text/TextRendering.h>
#include <jfl/app/Loop.h>
#include <jfl/json.h>

#if defined(JFL_PLATFORM_EM)
	#include <emscripten.h>
#endif


namespace jfl{

using fl::flash::display::Stage;
using fl::flash::display::Stage3D;
using fl::flash::display::LoaderInfo;
using fl::flash::display::DisplayObject;
using fl::flash::display::DisplayObjectContainer;
using fl::flash::display3D::Context3D;
using fl::flash::events::UncaughtErrorEvents;
using fl::flash::events::Event;
using fl::flash::events::MouseEvent;
using fl::flash::events::EventDispatcher;
using fl::flash::utils::Timer;

void ApplicationBase::initialize(ApplicationDescriptor* descriptor) {
	TextSubsystem_Init();
	init(descriptor);
}

void ApplicationBase::createTimer(Timer* timer, double delay) {
	timers.insert({timer, TimerInfo(delay, getTimer())});
}

void ApplicationBase::destroyTimer(Timer* timer) {
	timers.erase(gc_ptr<Timer>(timer));
}

static void EmEventLoop() {
	GetApplication()->loopEvent();
}

void ApplicationBase::loop() {
#if defined(JFL_PLATFORM_EM)
	emscripten_set_main_loop(EmEventLoop, 0, 1);
#else
	while (!shouldQuit()) {
		loopEvent();
	}
#endif
}

void ApplicationBase::loopEvent() {
	processEvents();

	// process timers
	std::vector<Timer*> triggered_timers;
	double time = getTimer();
	for (auto& timer : timers) {
		if (timer.second.next <= time) {
			timer.second.next = std::max(time, timer.second.next + timer.second.delay);
			triggered_timers.push_back(timer.first);
		}
	}
	if (!triggered_timers.empty()) {
		auto lock = UnsafeLockVM();
		for (auto timer : triggered_timers) {
			Timer::jfl_p_tick(timer);
		}
	}

	ProcessMainLoop();

	// fire enter frame
	{
		auto lock = UnsafeLockVM();
		DisplayObjectContainer::jfl_pS_DispatchToHierarchy((DisplayObject*)stage.get(), Event::jfl_S_ENTER_FRAME);
	}

	ProcessMainLoop();
}

void ApplicationBase::start(std::string_view params_string) {
	loaderInfo = LoaderInfo::jfl_New();
	fl::flash::events::jfl_CInit_UncaughtErrorEvents();
	loaderInfo->jfl_p__uncaughtErrorEvents = UncaughtErrorEvents::jfl_pS_Instance;

	if (params_string.size() > 0) {
		loaderInfo->jfl_p__params = jfl::JSONDecode(params_string);
	}

	stage = Stage::jfl_New();
	stage->jfl_nflash_2einternal_n__loaderInfo = loaderInfo;
	int width, height;
	double scale;
	queryStageDimensions(&width, &height, &scale);
	stage->jfl_p__stageWidth = width;
	stage->jfl_p__stageHeight = height;
	stage->jfl_p__contentsScaleFactor = scale;

	stage->jfl_p__stage3D = Stage3D::jfl_New();
}

void ApplicationBase::setMainSprite(fl::flash::display::DisplayObject* object) {
	object->jfl_nflash_2einternal_n__loaderInfo = loaderInfo;
	fl::flash::display::DisplayObjectContainer::addChild((fl::flash::display::DisplayObjectContainer*)stage.get(), (fl::flash::display::DisplayObject*)object);
}

Stage* ApplicationBase::getStage() {
	return stage;
}

void ApplicationBase::requestContext3D() {
	if (context3D) {
		return;
	}
	context3D = Context3D::jfl_New();
	int width, height;
	double scale;
	queryStageDimensions(&width, &height, &scale);
	context3D->impl->setWindowSize(width, height);
	PostMainLoopTask([&]() {
		stage->jfl_p__stage3D->jfl_p__context = context3D;
		Event* event = Event::jfl_New(Event::jfl_S_CONTEXT3D_CREATE, false, false);
		EventDispatcher::dispatchEvent((EventDispatcher*)stage->jfl_p__stage3D, event);
	});
}

void ApplicationBase::onResize(int width, int height, double scaleFactor) {
	auto lock = UnsafeLockVM();

	if (stage) {
		stage->jfl_p__stageWidth = width;
		stage->jfl_p__stageHeight = height;
		stage->jfl_p__contentsScaleFactor = scaleFactor;

		if (context3D) {
			context3D->impl->setWindowSize(width, height);
		}

		Event* event = Event::jfl_New(
			Event::jfl_S_RESIZE,
			false,
			false
		);
		EventDispatcher::dispatchEvent((EventDispatcher*)stage.get(), (Event*)event);
	}
}

void ApplicationBase::onMouseMove(double x, double y) {
	auto lock = UnsafeLockVM();

	MouseEvent* event = MouseEvent::jfl_New(
		MouseEvent::jfl_S_MOUSE_MOVE,
		true,
		false,
		x,
		y,
		(fl::flash::display::InteractiveObject*)stage.get(),
		false,
		false,
		false,
		false,
		0,
		false,
		false,
		0
	);
	EventDispatcher::dispatchEvent((EventDispatcher*)stage.get(), (Event*)event);
}

void ApplicationBase::onMouseButton(double x, double y, MouseButtonID button, MouseButtonAction action) {
	auto lock = UnsafeLockVM();

	jfl::String type = nullptr;

	switch (button) {
	case MouseButtonID::Left: {
		switch (action) {
		case MouseButtonAction::Down: {
			type = MouseEvent::jfl_S_MOUSE_DOWN;
		}break;
		case MouseButtonAction::Up: {
			type = MouseEvent::jfl_S_MOUSE_UP;
		}break;
		}
	}break;
	case MouseButtonID::Right: {
		switch (action) {
		case MouseButtonAction::Down: {
			type = MouseEvent::jfl_S_RIGHT_MOUSE_DOWN;
		}break;
		case MouseButtonAction::Up: {
			type = MouseEvent::jfl_S_RIGHT_MOUSE_UP;
		}break;
		}
	}break;
	}

	if (type.tag == type_tag::Null) {
		return;
	}

	MouseEvent* event = MouseEvent::jfl_New(
		type,
		true,
		false,
		x,
		y,
		(fl::flash::display::InteractiveObject*)stage.get(),
		false,
		false,
		false,
		false,
		0,
		false,
		false,
		0
	);
	EventDispatcher::dispatchEvent((EventDispatcher*)stage.get(), (Event*)event);
}

void ApplicationBase::onMouseWheel(double x, double y, double delta) {
	auto lock = UnsafeLockVM();

	MouseEvent* event = MouseEvent::jfl_New(
		MouseEvent::jfl_S_MOUSE_WHEEL,
		true,
		false,
		x,
		y,
		(fl::flash::display::InteractiveObject*)stage.get(),
		false,
		false,
		false,
		false,
		(int)delta,
		false,
		false,
		0
	);
	EventDispatcher::dispatchEvent((EventDispatcher*)stage.get(), (Event*)event);
}


}

