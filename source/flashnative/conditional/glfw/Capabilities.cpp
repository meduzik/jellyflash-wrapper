#if __has_include(<fl/flash/system/Capabilities.h>)
#include <jfl/common_impl.h>
#include <fl/flash/system/Capabilities.h>
#include <GLFW/glfw3.h>

namespace fl::flash::system {

jfl::Number Capabilities::jfl_Sg_screenDPI() {
	return 72;
}
jfl::Number Capabilities::jfl_Sg_screenResolutionX() {
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	if (mode){
		return mode->width;
	}
	return 0;
}
jfl::Number Capabilities::jfl_Sg_screenResolutionY() {
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	if (mode) {
		return mode->height;
	}
	return 0;
}
}
#endif
