#include <jfl/std.h>
#include <jfl/render/OpenGL.h>
#include <jfl/app/ApplicationBase.h>
#include <jfl/app/Loop.h>
#include <jfl/string.h>
#include <fl/flash/display/Stage.h>
#include <fl/flash/display/Stage3D.h>
#include <fl/flash/display/LoaderInfo.h>
#include <fl/flash/display/DisplayObjectContainer.h>
#include <fl/flash/display3D/Context3D.h>
#include <fl/flash/events/UncaughtErrorEvents.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/events/MouseEvent.h>
#include <fl/flash/events/EventDispatcher.h>
#include <fl/flash/utils/Timer.h>
#include <jfl/render/Context3D.h>
#include <jfl/text/TextRendering.h>
#include <GLFW/glfw3.h>
#include <jfl/json.h>

namespace jfl{
using fl::flash::display::Stage;
using fl::flash::display::Stage3D;
using fl::flash::display::LoaderInfo;
using fl::flash::display::DisplayObject;
using fl::flash::display::DisplayObjectContainer;
using fl::flash::display3D::Context3D;
using fl::flash::events::UncaughtErrorEvents;
using fl::flash::events::Event;
using fl::flash::events::MouseEvent;
using fl::flash::events::EventDispatcher;
using fl::flash::utils::Timer;

class GLFWApplication: public ApplicationBase{
public:
	GLFWApplication();

	void init(ApplicationDescriptor* descriptor);

	double getTimer();

	void processEvents();
	bool shouldQuit();
	void present();
	void queryStageDimensions(int* width, int* height, double* scaleFactor);
private:
	static void MouseButtonFun(GLFWwindow* window, int button, int action, int mods){
		((GLFWApplication*)glfwGetWindowUserPointer(window))->glfwMouseButton(button, action, mods);
	}

	static void CursorPosFun(GLFWwindow* window, double xpos, double ypos) {
		((GLFWApplication*)glfwGetWindowUserPointer(window))->onMouseMove(xpos, ypos);
	}

	static void ScrollFun(GLFWwindow* window, double xoff, double yoff) {
		// glfw does not take OS scroll value into account
		// so we multiply it by the default (x3), which is better than doing nothing
		static const double SCROLL_MULTIPLIER = 3.0;

		((GLFWApplication*)glfwGetWindowUserPointer(window))->glfwScroll(yoff * SCROLL_MULTIPLIER);
	}

	static void SizeFun(GLFWwindow* window, int width, int height){
		((GLFWApplication*)glfwGetWindowUserPointer(window))->onResize(width, height, 1);
	}

	void glfwScroll(double delta) {
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		onMouseWheel(xpos, ypos, delta);
	}

	void glfwMouseButton(int button, int action, int mods) {
		MouseButtonID button_id;
		switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT: {
			button_id = MouseButtonID::Left;
		} break;
		case GLFW_MOUSE_BUTTON_RIGHT: {
			button_id = MouseButtonID::Right;
		} break;
		default: {
			return;
		} break;
		}
		MouseButtonAction action_id;
		switch (action) {
		case GLFW_PRESS: {
			action_id = MouseButtonAction::Down;
		} break;
		case GLFW_RELEASE: {
			action_id = MouseButtonAction::Up;
		} break;
		default: {
			return;
		} break;
		}

		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		onMouseButton(xpos, ypos, button_id, action_id);
	}

	GLFWwindow* window;
};

static GLFWApplication* app;

Application* InitializeApplication(ApplicationDescriptor* descriptor){
	app = new GLFWApplication();
	app->initialize(descriptor);
	return app;
}

Application* GetApplication(){
	return app;
}

GLFWApplication::GLFWApplication(){
}

void GLFWApplication::init(ApplicationDescriptor* descriptor){
	glfwSetErrorCallback([](int code, const char* desc){
		std::cout << "GLFW error " << code << ": " << desc << std::endl;
	});

	if ( !glfwInit() ){
		std::cout << "GLFW Init failed " << std::endl;
		jfl_notimpl;
	}

#if !defined(__EMSCRIPTEN__)
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_STENCIL_BITS, 8);
	glfwWindowHint(GLFW_DEPTH_BITS, 16);
	glfwWindowHint(GLFW_DOUBLEBUFFER, 1);
#endif

	Arena arena;
	jfl::arena_string application_package_id(ApplicationPackageID.to_string_view(), arena);
	window = glfwCreateWindow(
		descriptor->initialWidth, 
		descriptor->initialHeight, 
		application_package_id.c_str(),
		nullptr, 
		nullptr
	);
	if ( !window ){
		std::cout << "GLFW window creation failed " << std::endl;
		jfl_notimpl;
	}

	glfwSetWindowUserPointer(window, this);
	glfwSetMouseButtonCallback(window, MouseButtonFun);
	glfwSetCursorPosCallback(window, CursorPosFun);
	glfwSetWindowSizeCallback(window, SizeFun);
	glfwSetScrollCallback(window, ScrollFun);

	glfwMakeContextCurrent(window);
#if defined(JFL_USE_GLAD)
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
#endif
	glfwSwapInterval(1);
}

double GLFWApplication::getTimer() {
	return glfwGetTime();
}

void GLFWApplication::processEvents() {
	glfwPollEvents();
}

bool GLFWApplication::shouldQuit() {
	return glfwWindowShouldClose(window);
}

void GLFWApplication::present() {
	glfwSwapBuffers(window);
}

void GLFWApplication::queryStageDimensions(int* width, int* height, double* scaleFactor) {
	glfwGetWindowSize(window, width, height);
	*scaleFactor = 1;
}

}

