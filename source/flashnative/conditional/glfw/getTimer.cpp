#if __has_include(<fl/flash/utils/getTimer.h>)
#include <jfl/common_impl.h>
#include <fl/flash/utils/getTimer.h>
#include <GLFW/glfw3.h>
namespace fl::flash::utils {
jfl::Int getTimer() {
	return (jfl::Int)(glfwGetTime() * 1000);
}
}
#endif
