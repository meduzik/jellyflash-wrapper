#if __has_include(<fl/flash/utils/getTimer.h>)
#include <jfl/common_impl.h>
#include <time.h>
#include <fl/flash/utils/getTimer.h>
namespace fl::flash::utils {
jfl::Int getTimer() {
	struct timespec res;
	clock_gettime(CLOCK_MONOTONIC, &res);
	return (res.tv_sec * 1000) + res.tv_nsec / 1000000;
}
}
#endif
