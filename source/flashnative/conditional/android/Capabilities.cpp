#if __has_include(<fl/flash/system/Capabilities.h>)
#include <jfl/common_impl.h>
#include <fl/flash/system/Capabilities.h>
namespace fl::flash::system {

jfl::Number Capabilities::jfl_Sg_screenDPI() {
	return 72;
}
jfl::Number Capabilities::jfl_Sg_screenResolutionX() {
	return 480;
}
jfl::Number Capabilities::jfl_Sg_screenResolutionY() {
	return 800;
}
}
#endif
