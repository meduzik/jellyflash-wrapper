#include <jfl/std.h>
#include <jfl/render/OpenGL.h>
#include <jfl/app/application.h>
#include <jfl/app/Loop.h>
#include <jfl/string.h>
#include <fl/flash/display/Stage.h>
#include <fl/flash/display/Stage3D.h>
#include <fl/flash/display/LoaderInfo.h>
#include <fl/flash/display/DisplayObjectContainer.h>
#include <fl/flash/display3D/Context3D.h>
#include <fl/flash/events/UncaughtErrorEvents.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/events/MouseEvent.h>
#include <fl/flash/events/EventDispatcher.h>
#include <fl/flash/utils/Timer.h>
#include <jfl/render/Context3D.h>
#include "android.h"
#include <time.h>

namespace jfl {
using fl::flash::display::Stage;
using fl::flash::display::Stage3D;
using fl::flash::display::LoaderInfo;
using fl::flash::display::DisplayObject;
using fl::flash::display::DisplayObjectContainer;
using fl::flash::display3D::Context3D;
using fl::flash::events::UncaughtErrorEvents;
using fl::flash::events::Event;
using fl::flash::events::MouseEvent;
using fl::flash::events::EventDispatcher;
using fl::flash::utils::Timer;

double getTimer() {
	struct timespec res;
	clock_gettime(CLOCK_MONOTONIC, &res);
	return (res.tv_sec * 1000.0) + res.tv_nsec / 1000.0;
}

struct TimerInfo {
	double delay;
	double next;

	TimerInfo(double delay, double timestamp) :
		delay(delay),
		next(timestamp + delay) {
	}
};

class AndroidApplication : public Application {
public:
	AndroidApplication();

	void initialize(ApplicationDescriptor* descriptor);
	void setMainSprite(fl::flash::display::DisplayObject*);
	void start();
	void loop();
	void loopEvent();
	void present();
	Stage* getStage();

	void createTimer(Timer* timer, double delay);
	void destroyTimer(Timer* timer);

	void requestContext3D();

	void onSurfaceCreated();
	void onSurfaceResized(int width, int height);

	void reportContext3D();
private:
	bool has_size = false;
	bool was_created = false;
	int width, height;
	
	void onResize(int width, int height) {
		auto lock = UnsafeLockVM();

		this->width = width;
		this->height = height;

		if (stage) {
			stage->jfl_p__stageWidth = width;
			stage->jfl_p__stageHeight = height;

			if (context3D) {
				context3D->impl->setWindowSize(width, height);
			}

			Event* event = Event::jfl_New(
				Event::jfl_S_RESIZE,
				false,
				false
			);
			EventDispatcher::dispatchEvent((EventDispatcher*)stage.get(), (Event*)event);
		}
	}

	void onMouseMove(double xpos, double ypos) {
		auto lock = UnsafeLockVM();

		MouseEvent* event = MouseEvent::jfl_New(
			MouseEvent::jfl_S_MOUSE_MOVE,
			true,
			false,
			xpos,
			ypos,
			(fl::flash::display::InteractiveObject*)stage.get(),
			false,
			false,
			false,
			false,
			0,
			false,
			false,
			0
		);
		EventDispatcher::dispatchEvent((EventDispatcher*)stage.get(), (Event*)event);
	}

	void onMouseButton(int button, int action, int mods) {
		auto lock = UnsafeLockVM();

		double xpos, ypos;
		

		jfl::String type = nullptr;

		/*
		switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT: {
			switch (action) {
			case GLFW_PRESS: {
				type = MouseEvent::jfl_S_MOUSE_DOWN;
			}break;
			case GLFW_RELEASE: {
				type = MouseEvent::jfl_S_MOUSE_UP;
			}break;
			}
		}break;
		case GLFW_MOUSE_BUTTON_RIGHT: {
			switch (action) {
			case GLFW_PRESS: {
				type = MouseEvent::jfl_S_RIGHT_MOUSE_DOWN;

				jfl::gc_perform();
			}break;
			case GLFW_RELEASE: {
				type = MouseEvent::jfl_S_RIGHT_MOUSE_UP;
			}break;
			}
		}break;
		}
		*/
		if (type.tag == type_tag::Null) {
			return;
		}

		MouseEvent* event = MouseEvent::jfl_New(
			type,
			true,
			false,
			xpos,
			ypos,
			(fl::flash::display::InteractiveObject*)stage.get(),
			false,
			false,
			false,
			false,
			0,
			false,
			false,
			0
		);
		EventDispatcher::dispatchEvent((EventDispatcher*)stage.get(), (Event*)event);
	}

	gc_ptr<fl::flash::display::Stage> stage;
	LoaderInfo* loaderInfo;
	std::unordered_map< gc_ptr<Timer>, TimerInfo> timers;
	Context3D* context3D = nullptr;
};

static AndroidApplication* app;

Application* InitializeApplication(ApplicationDescriptor* descriptor) {
	app = new AndroidApplication();
	app->initialize(descriptor);
	return app;
}

Application* GetApplication() {
	return app;
}

AndroidApplication::AndroidApplication() {
}

void AndroidApplication::initialize(ApplicationDescriptor* descriptor) {
	
}

void AndroidApplication::onSurfaceCreated(){
	if (was_created){
		// the context is reset
		reportContext3D();
	}else{
		// this is the first time the surface is created
		was_created = true;
		reportContext3D();
	}
}

void AndroidApplication::onSurfaceResized(int width, int height){
	has_size = true;
	onResize(width, height);
}

void AndroidApplication::start() {
	loaderInfo = LoaderInfo::jfl_New();
	fl::flash::events::jfl_CInit_UncaughtErrorEvents();
	loaderInfo->jfl_p__uncaughtErrorEvents = UncaughtErrorEvents::jfl_pS_Instance;
	jfl::any_set(loaderInfo->jfl_p__params, jfl::String::Make("resid"), jfl::String::Make("8a189122e8a18cfe"));
	stage = Stage::jfl_New();
	stage->jfl_nflash_2einternal_n__loaderInfo = loaderInfo;
	int width, height;
	stage->jfl_p__stageWidth = 0;
	stage->jfl_p__stageHeight = 0;
	stage->jfl_p__stage3D = Stage3D::jfl_New();
}

void AndroidApplication::setMainSprite(fl::flash::display::DisplayObject* object) {
	object->jfl_nflash_2einternal_n__loaderInfo = loaderInfo;
	fl::flash::display::DisplayObjectContainer::addChild((fl::flash::display::DisplayObjectContainer*)stage.get(), (fl::flash::display::DisplayObject*)object);
}

Stage* AndroidApplication::getStage() {
	return stage;
}

void AndroidApplication::loopEvent() {
	// process timers
	std::vector<Timer*> triggered_timers;
	double time = getTimer();
	for (auto& timer : timers) {
		if (timer.second.next <= time) {
			timer.second.next = std::max(time, timer.second.next + timer.second.delay);
			triggered_timers.push_back(timer.first);
		}
	}
	if (!triggered_timers.empty()) {
		auto lock = UnsafeLockVM();
		for (auto timer : triggered_timers) {
			Timer::jfl_p_tick(timer);
		}
	}

	ProcessMainLoop();

	// fire enter frame
	{
		auto lock = UnsafeLockVM();
		DisplayObjectContainer::jfl_pS_DispatchToHierarchy((DisplayObject*)stage.get(), Event::jfl_S_ENTER_FRAME);
	}

	ProcessMainLoop();
}

void AndroidApplication::requestContext3D() {
	if (context3D) {
		return;
	}
	context3D = Context3D::jfl_New();
	context3D->impl->setWindowSize(width, height);
	reportContext3D();
}

void AndroidApplication::createTimer(Timer* timer, double delay) {
	timers.insert({ timer, TimerInfo(delay, getTimer()) });
}

void AndroidApplication::destroyTimer(Timer* timer) {
	timers.erase(gc_ptr<Timer>(timer));
}

void AndroidApplication::present() {
}

void AndroidApplication::reportContext3D(){
	if (was_created && context3D) {
		PostMainLoopTask([&]() {
			stage->jfl_p__stage3D->jfl_p__context = context3D;
			Event* event = Event::jfl_New(Event::jfl_S_CONTEXT3D_CREATE, false, false);
			EventDispatcher::dispatchEvent((EventDispatcher*)stage->jfl_p__stage3D, event);
		});
	}
}

}


extern "C" {
	JNIEXPORT void JNICALL Java_com_meduzik_jellyflashwrapper_JelyflashLib_on_1surface_1created(JNIEnv * env, jobject obj);
	JNIEXPORT void JNICALL Java_com_meduzik_jellyflashwrapper_JelyflashLib_on_1surface_1changed(JNIEnv * env, jobject obj, jint width, jint height);
	JNIEXPORT void JNICALL Java_com_meduzik_jellyflashwrapper_JelyflashLib_on_1frame(JNIEnv * env, jobject obj);
};

JNIEXPORT void JNICALL Java_com_meduzik_jellyflashwrapper_JelyflashLib_on_1surface_1created(JNIEnv* env, jobject obj) {
	jfl::app->onSurfaceCreated();
}

JNIEXPORT void JNICALL Java_com_meduzik_jellyflashwrapper_JelyflashLib_on_1surface_1changed(JNIEnv* env, jobject obj, jint width, jint height) {
	jfl::app->onSurfaceResized(width, height);
}

JNIEXPORT void JNICALL Java_com_meduzik_jellyflashwrapper_JelyflashLib_on_1frame(JNIEnv* env, jobject obj) {
	jfl::app->loopEvent();
}

