#include "android.h"


extern "C" {
	JNIEXPORT void JNICALL Java_com_meduzik_jellyflashwrapper_JelyflashLib_on_1initialize(JNIEnv * env, jobject obj, jobject context);
};

extern "C" void Android_main();

JNIEXPORT void JNICALL Java_com_meduzik_jellyflashwrapper_JelyflashLib_on_1initialize(JNIEnv * env, jobject obj, jobject context) {
	Android_main();
}

