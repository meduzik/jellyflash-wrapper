#if __has_include(<fl/flash/filesystem/File.h>)
#include <jfl/common_impl.h>
#include <fl/flash/filesystem/File.h>
#include <jfl/app/application.h>
#include "android.h"


namespace fl::flash::filesystem {
jfl::String File::jfl_pS_initSystemCharset() {
	return jfl::StringSmall{ 5, "UTF-8" };
}
jfl::String File::jfl_pS_initSeparator() {
	return jfl::StringSmall{ 1, "/" };
}
jfl::String File::jfl_pS_initLineEnding() {
	return jfl::StringSmall{ 2, "\n" };
}
jfl::String File::jfl_pS_initUserDirectory() {
	return 
}
jfl::String File::jfl_pS_initDocumentsDirectory() {
	return 
}
jfl::String File::jfl_pS_initDesktopDirectory() {
	return 
}
jfl::String File::jfl_pS_initApplicationStorageDirectory() {
	return 
}
jfl::String File::jfl_pS_initApplicationDirectory() {
	return 
}
jfl::String File::jfl_pS_initCacheDirectory() {
	return 
}
}
#endif
