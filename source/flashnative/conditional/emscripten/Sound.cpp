#include <jfl/media/Sound.h>
#include <fl/flash/media/Sound.h>
#include <fl/flash/media/SoundChannel.h>
#include <fl/flash/media/SoundTransform.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/events/IOErrorEvent.h>
#include <jfl/app/Loop.h>
#include <em/WebAudio.h>

namespace jfl::media {

struct SoundImpl {
	gc_ptr<fl::flash::media::Sound> object;
	WebAudioSound sound;
};

struct SoundChannelImpl {
	gc_ptr<fl::flash::media::SoundChannel> object;
	WebAudioPlayback playback;
};

SoundImpl* SoundImplCreate(fl::flash::media::Sound* sound) {
	SoundImpl* impl = new(jfl::unmanaged_allocate(sizeof(SoundImpl)))SoundImpl;
	sound->impl = impl;
	impl->sound = 0;
	impl->object = sound;
	return impl;
}

void SoundImplDecode(SoundImpl* impl, const uint8_t* data, size_t len) {
	impl->sound = webaudio_sound_load(impl, data, len);
}

void SoundImplDestroy(SoundImpl* impl) {
	if (impl->sound) {
		webaudio_sound_destroy(impl->sound);
		impl->sound = 0;
	}
	impl->~SoundImpl();
	jfl::unmanaged_free(impl, sizeof(SoundImpl));
}

SoundChannelImpl* SoundImplPlay(SoundImpl* impl, fl::flash::media::SoundChannel* channel, double position, fl::flash::media::SoundTransform* transform) {
	SoundChannelImpl* channel_impl = new(jfl::unmanaged_allocate(sizeof(SoundChannelImpl)))SoundChannelImpl;
	WebAudioPlayback playback = webaudio_sound_play(
		impl->sound,
		channel_impl,
		position,
		transform->jfl_p__volume,
		fl::flash::media::SoundTransform::jfl_g_pan(transform)
	);
	if (!playback) {
		channel_impl->~SoundChannelImpl();
		jfl::unmanaged_free(channel_impl, sizeof(SoundChannelImpl));
		return nullptr;
	}
	channel_impl->object = channel;
	channel->impl = channel_impl;
	channel_impl->playback = playback;
	return channel_impl;
}

void SoundChannelImplDestroy(SoundChannelImpl* impl) {
	if (impl->playback) {
		webaudio_playback_destroy(impl->playback);
		impl->playback = 0;
	}
	impl->~SoundChannelImpl();
	jfl::unmanaged_free(impl, sizeof(SoundChannelImpl));
}

double SoundChannelImplGetPosition(SoundChannelImpl* impl) {
	if (impl->playback) {
		return webaudio_playback_get_position(impl->playback);
	}
	return 0;
}

void SoundChannelImplUpdateTransform(SoundChannelImpl* impl, fl::flash::media::SoundTransform* transform) {
	webaudio_playback_update_transform(
		impl->playback,
		transform->jfl_p__volume,
		fl::flash::media::SoundTransform::jfl_g_pan(transform)
	);
}

void SoundSetGlobalVolume(double value) {
	webaudio_set_volume(value);
}

}

using namespace jfl::media;

EM_IMPORT void webaudio_callback_sound_loaded(WebAudioSound sound, void* ud) {
	SoundImpl* impl = (SoundImpl*)ud;
	if (impl->sound == sound) {		
		jfl::PostMainLoopTask([&, sound_object = impl->object]() {
			if (sound_object->impl) {
				fl::flash::events::Event* event = fl::flash::events::Event::jfl_New(
					fl::flash::events::Event::jfl_S_COMPLETE,
					false,
					false
				);
				fl::flash::events::EventDispatcher::dispatchEvent(
					(fl::flash::events::EventDispatcher*)sound_object.get(),
					(fl::flash::events::Event*)event
				);
			}
		});
	}
}

EM_IMPORT void webaudio_callback_sound_error(WebAudioSound sound, void* ud) {
	SoundImpl* impl = (SoundImpl*)ud;
	if (impl->sound == sound) {
		jfl::PostMainLoopTask([&, sound_object = impl->object]() {
			if (sound_object->impl) {
				fl::flash::events::IOErrorEvent* event = fl::flash::events::IOErrorEvent::jfl_New(
					fl::flash::events::IOErrorEvent::jfl_S_IO_ERROR,
					false,
					false,
					jfl::String::Make("failed to parse audio"),
					2011
				);
				fl::flash::events::EventDispatcher::dispatchEvent(
					(fl::flash::events::EventDispatcher*)sound_object.get(),
					(fl::flash::events::Event*)event
				);
			}
		});
	}
}

EM_IMPORT void webaudio_callback_playback_complete(WebAudioPlayback sound, void* ud) {
	SoundChannelImpl* impl = (SoundChannelImpl*)ud;
	if (impl->playback == sound) {
		jfl::PostMainLoopTask([&, playback_object = impl->object]() {
			if (playback_object->impl) {
				playback_object->last_known_position = webaudio_playback_get_position(playback_object->impl->playback);
				SoundChannelImplDestroy(playback_object->impl);
				playback_object->impl = nullptr;

				fl::flash::events::Event* event = fl::flash::events::Event::jfl_New(
					fl::flash::events::Event::jfl_S_SOUND_COMPLETE,
					false,
					false
				);
				fl::flash::events::EventDispatcher::dispatchEvent(
					(fl::flash::events::EventDispatcher*)playback_object.get(),
					(fl::flash::events::Event*)event
				);
			}
		});
	}
}

