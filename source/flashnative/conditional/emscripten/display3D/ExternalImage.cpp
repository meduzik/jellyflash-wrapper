#if __has_include(<fl/jellyflash/display3D/ExternalImage.decl.h>)
#include <jfl/common_impl.h>
#include <fl/jellyflash/display3D/ExternalImage.decl.h>

#include <em/WebGL.h>

namespace fl::jellyflash::display3D{

void ExternalImage::jfl_p_load(fl::jellyflash::display3D::ExternalImage* jfl_t){
	jfl::gc_ptr<ExternalImage> self_ptr = jfl_t;

	jfl_t->jfl_p_handle = em::webgl_load_image(jfl_t->jfl_p_url.to_string_view(), [self_ptr](bool success, std::string_view message) {
		if (success) {
			self_ptr->jfl_p__width = em::webgl_image_get_width(self_ptr->jfl_p_handle);
			self_ptr->jfl_p__height = em::webgl_image_get_height(self_ptr->jfl_p_handle);
		}

		if (success) {
			ExternalImage::jfl_p_reportSuccess(self_ptr.get());
		} else {
			ExternalImage::jfl_p_reportError(self_ptr.get(), jfl::String::Make(message));
		}

		if (!success) {
			ExternalImage::dispose(self_ptr);
		}
	});
}
void ExternalImage::jfl_p__dispose(fl::jellyflash::display3D::ExternalImage* jfl_t) {
	if (jfl_t->jfl_p_handle != 0) {
		em::webgl_dispose_image(jfl_t->jfl_p_handle);
		jfl_t->jfl_p_handle = 0;
	}
}


}

#endif
