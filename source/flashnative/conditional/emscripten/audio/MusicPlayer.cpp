#if __has_include(<fl/com/meduzik/audio/MusicPlayer.decl.h>)
#include <jfl/common_impl.h>
#include <fl/com/meduzik/audio/MusicPlayer.decl.h>
#include <jfl/memory.h>
#include <jfl/memory/ManagedHeap.h>
#include <em/WebAudio.h>

struct MusicTrackCallback {
	jfl::gc_ptr<jfl::Object> holder;
	jfl::Function callback;
};

#if !defined(EM_EXPORT)
	#define EM_EXPORT
	#define WEBAUDIO_STATUS_FINISHED 0
	#define WEBAUDIO_STATUS_ERROR 1
	#define WEBAUDIO_STATUS_FADED 2
	#define WEBAUDIO_STATUS_NOT_STARTED 3
#endif

EM_EXPORT void webaudio_callback_music_complete(int track, void* ud, int status) {
	MusicTrackCallback* cb = (MusicTrackCallback*)ud;
	if (cb->callback.has_function()) {		
		jfl::Any val;
		switch (status) {
		case WEBAUDIO_STATUS_FINISHED: {
			val = fl::com::meduzik::audio::MusicPlayer::jfl_S_TRACK_COMPLETE_FINISHED;
		} break;
		case WEBAUDIO_STATUS_FADED: {
			val = fl::com::meduzik::audio::MusicPlayer::jfl_S_TRACK_COMPLETE_FADED;
		} break;
		case WEBAUDIO_STATUS_ERROR: {
			val = fl::com::meduzik::audio::MusicPlayer::jfl_S_TRACK_COMPLETE_ERROR;
		} break;
		case WEBAUDIO_STATUS_NOT_STARTED: {
			val = fl::com::meduzik::audio::MusicPlayer::jfl_S_TRACK_COMPLETE_NOT_STARTED;
		} break;
		}
		jfl::RestParams params(&val, 1);
		cb->callback.invoke(&params);
	}
	cb->~MusicTrackCallback();
	jfl::unmanaged_free(cb, sizeof(MusicTrackCallback));
}

namespace fl::com::meduzik::audio{

void MusicPlayer::set_volume(fl::com::meduzik::audio::MusicPlayer* jfl_t, jfl::Number volume){
	webaudio_music_set_volume(volume);
}

void MusicPlayer::play_track(fl::com::meduzik::audio::MusicPlayer* jfl_t, jfl::String uri, jfl::Function on_complete){
	jfl::Arena arena;
	jfl::arena_string str(uri.to_string_view(), arena);

	MusicTrackCallback* cb = new (jfl::unmanaged_allocate(sizeof(MusicTrackCallback))) MusicTrackCallback();
	cb->holder = on_complete.object;
	cb->callback = on_complete;

	webaudio_music_play(str.c_str(), (void*)cb);
}

void MusicPlayer::stop(fl::com::meduzik::audio::MusicPlayer* jfl_t){
	webaudio_music_stop();
}

}
#endif
