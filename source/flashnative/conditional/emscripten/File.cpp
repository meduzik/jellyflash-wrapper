#if __has_include(<fl/flash/filesystem/File.h>)
#include <jfl/common_impl.h>
#include <fl/flash/filesystem/File.h>
#include <jfl/app/application.h>

namespace fl::flash::filesystem {
jfl::String File::jfl_pS_initSystemCharset() {
	return jfl::String::Make("BAD");
}
jfl::String File::jfl_pS_initSeparator() {
	return jfl::String::Make("/");
}
jfl::String File::jfl_pS_initLineEnding() {
	return jfl::String::Make("\n");
}
jfl::String File::jfl_pS_initUserDirectory() {
	return jfl::String::Make("%user%");
}
jfl::String File::jfl_pS_initDocumentsDirectory() {
	return jfl::String::Make("%documents%");
}
jfl::String File::jfl_pS_initDesktopDirectory() {
	return jfl::String::Make("%desktop%");
}
jfl::String File::jfl_pS_initApplicationStorageDirectory() {
	return jfl::String::Make("%storage%");
}
jfl::String File::jfl_pS_initApplicationDirectory() {
	return jfl::String::Make("%app%");
}
jfl::String File::jfl_pS_initCacheDirectory() {
	return jfl::String::Make("%cache%");
}
}
#endif
