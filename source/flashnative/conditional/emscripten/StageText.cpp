#if __has_include(<fl/flash/text/StageText.h>)
#include <jfl/common_impl.h>
#include <fl/flash/text/StageText.h>
#include <fl/flash/text/StageTextInitOptions.h>
#include <fl/flash/geom/Rectangle.h>
#include <fl/flash/events/FocusEvent.h>
#include <fl/flash/events/KeyboardEvent.h>
#include <em/StageText.h>
#include <jfl/app/Loop.h>

namespace fl::flash::text {
using flash::events::Event;
using flash::events::FocusEvent;
using flash::events::KeyboardEvent;
using flash::events::EventDispatcher;

StageText::~StageText() {
	StageText::dispose(this);
}
void StageText::jfl_GC_user(fl::flash::text::StageText* self){
	jfl::gc_visit_string(self->font_family);
	jfl::gc_visit_string(self->font_weight);
	jfl::gc_visit_string(self->font_posture);
	jfl::gc_visit_string(self->text_align);
	jfl::gc_visit((jfl::Object*)self->stage);
}
void StageText::jfl_p_init(fl::flash::text::StageText* self, fl::flash::text::StageTextInitOptions* opts) {
	em::StageTextListener listener{
		self,
		[](void* ud, em::StageText*){
			StageText* self = (StageText*)ud;
			jfl::WaitMainLoopTask([&](){
				FocusEvent* event = FocusEvent::jfl_New(FocusEvent::jfl_S_FOCUS_IN, false, false, nullptr, false, false, nullptr);
				EventDispatcher::dispatchEvent((EventDispatcher*)self, (Event*)event);
			});
		},
		[](void* ud, em::StageText*){
			StageText* self = (StageText*)ud;
			jfl::WaitMainLoopTask([&]() {
				FocusEvent* event = FocusEvent::jfl_New(FocusEvent::jfl_S_FOCUS_OUT, false, false, nullptr, false, false, nullptr);
				EventDispatcher::dispatchEvent((EventDispatcher*)self, (Event*)event);
			});
		},
		[](void* ud, em::StageText*, int key){
			StageText* self = (StageText*)ud;
			jfl::WaitMainLoopTask([&]() {
				KeyboardEvent* event = KeyboardEvent::jfl_New(KeyboardEvent::jfl_S_KEY_DOWN, false, false, 0, key, 0, false, false, false, false, false);
				EventDispatcher::dispatchEvent((EventDispatcher*)self, (Event*)event);
			});
		}
	};

	self->stage_text = em::stagetext_create(&listener, opts->jfl_p__multiline);
	self->editable = true;
	self->visible = true;
	self->multiline = opts->jfl_p__multiline;
	self->font_family = jfl::String::Make("");
	self->font_weight = jfl::String::Make("normal");
	self->font_posture = jfl::String::Make("normal");
	self->text_align = jfl::String::Make("left");
	self->font_size = 0;
	self->font_color = 0;
	self->max_chars = 0;
}
void StageText::jfl_s_editable(fl::flash::text::StageText* self, jfl::Boolean editable) {
	em::stagetext_set_editable(self->stage_text, editable);
	self->editable = editable;
}
jfl::Boolean StageText::jfl_g_editable(fl::flash::text::StageText* self) {
	return self->editable;
}
void StageText::jfl_s_text(fl::flash::text::StageText* self, jfl::String text) {
	jfl::Arena arena;
	jfl::arena_string str(text.to_string_view(), arena);
	em::stagetext_set_text(self->stage_text, str.c_str());
}
jfl::String StageText::jfl_g_text(fl::flash::text::StageText* self) {
	std::string str;
	em::stagetext_get_text(self->stage_text, str);
	return jfl::String::Make(str);
}
void StageText::jfl_s_clearButtonMode(fl::flash::text::StageText* self, jfl::String jfl_a_param1) {
	jfl_notimpl;
}
void StageText::jfl_s_fontFamily(fl::flash::text::StageText* self, jfl::String family) {
	jfl::Arena arena;
	jfl::arena_string str(family.to_string_view(), arena);
	em::stagetext_set_font_family(self->stage_text, str.c_str());
	self->font_family = jfl::String::Make(family.to_string_view());
}
jfl::String StageText::jfl_g_fontFamily(fl::flash::text::StageText* self) {
	return self->font_family;
}
void StageText::jfl_s_fontSize(fl::flash::text::StageText* self, jfl::Int size) {
	em::stagetext_set_font_size(self->stage_text, size);
	self->font_size = size;
}
jfl::Int StageText::jfl_g_fontSize(fl::flash::text::StageText* self) {
	return self->font_size;
}
void StageText::jfl_s_color(fl::flash::text::StageText* self, jfl::UInt color) {
	em::stagetext_set_font_color(self->stage_text, color);
	self->font_color = color;
}
jfl::UInt StageText::jfl_g_color(fl::flash::text::StageText* self) {
	return self->font_color;
}
void StageText::jfl_s_fontWeight(fl::flash::text::StageText* self, jfl::String weight_name) {
	bool bold = weight_name.to_string_view() == "bold";
	em::stagetext_set_font_bold(self->stage_text, bold);
	self->font_weight = weight_name;
}
jfl::String StageText::jfl_g_fontWeight(fl::flash::text::StageText* self) {
	return self->font_weight;
}
void StageText::jfl_s_fontPosture(fl::flash::text::StageText* self, jfl::String posture_name) {
	bool italic = posture_name.to_string_view() == "italic";
	em::stagetext_set_font_italic(self->stage_text, italic);
	self->font_posture = posture_name;
}
jfl::String StageText::jfl_g_fontPosture(fl::flash::text::StageText* self) {
	return self->font_posture;
}
jfl::Boolean StageText::jfl_g_displayAsPassword(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
void StageText::jfl_s_displayAsPassword(fl::flash::text::StageText* self, jfl::Boolean jfl_a_param1) {
	jfl_notimpl;
}
void StageText::jfl_s_maxChars(fl::flash::text::StageText* self, jfl::Int max_chars) {
	em::stagetext_set_font_color(self->stage_text, max_chars);
	self->max_chars = max_chars;
}
jfl::Int StageText::jfl_g_maxChars(fl::flash::text::StageText* self) {
	return self->max_chars;
}
jfl::Boolean StageText::jfl_g_multiline(fl::flash::text::StageText* self) {
	return self->multiline;
}
void StageText::jfl_s_restrict(fl::flash::text::StageText* self, jfl::String jfl_a_param1) {
	jfl_notimpl;
}
jfl::String StageText::jfl_g_restrict(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
void StageText::jfl_s_returnKeyLabel(fl::flash::text::StageText* self, jfl::String jfl_a_param1) {
	jfl_notimpl;
}
jfl::String StageText::jfl_g_returnKeyLabel(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
void StageText::jfl_s_textAlign(fl::flash::text::StageText* self, jfl::String align_name) {
	auto align = align_name.to_string_view();
	em::TextAlign align_mode;
	if (align == "left") {
		align_mode = em::TextAlign::Left;
	} else if (align == "center") {
		align_mode = em::TextAlign::Center;
	} else if (align == "right") {
		align_mode = em::TextAlign::Right;
	} else {
		jfl::raise_message("unsupported textAlign");
	}
	em::stagetext_set_text_align(self->stage_text, align_mode);
	self->text_align = align_name;
}
jfl::String StageText::jfl_g_textAlign(fl::flash::text::StageText* self) {
	return self->text_align;
}
jfl::String StageText::jfl_g_softKeyboardType(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
void StageText::jfl_s_softKeyboardType(fl::flash::text::StageText* self, jfl::String jfl_a_param1) {
	jfl_notimpl;
}
void StageText::jfl_s_autoCapitalize(fl::flash::text::StageText* self, jfl::String jfl_a_param1) {
	jfl_notimpl;
}
jfl::String StageText::jfl_g_autoCapitalize(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
void StageText::jfl_s_autoCorrect(fl::flash::text::StageText* self, jfl::Boolean jfl_a_param1) {
	jfl_notimpl;
}
jfl::Boolean StageText::jfl_g_autoCorrect(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
void StageText::jfl_s_locale(fl::flash::text::StageText* self, jfl::String jfl_a_param1) {
	jfl_notimpl;
}
jfl::String StageText::jfl_g_locale(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
void StageText::assignFocus(fl::flash::text::StageText* self) {
	em::stagetext_assign_focus(self->stage_text);
}
void StageText::selectRange(fl::flash::text::StageText* self, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2) {
	jfl_notimpl;
}
jfl::Int StageText::jfl_g_selectionActiveIndex(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
jfl::Int StageText::jfl_g_selectionAnchorIndex(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
void StageText::jfl_s_stage(fl::flash::text::StageText* self, fl::flash::display::Stage* stage) {
	self->stage = stage;
	em::stagetext_set_visible(self->stage_text, self->stage && self->visible);
}
fl::flash::display::Stage* StageText::jfl_g_stage(fl::flash::text::StageText* self) {
	return self->stage;
}
void StageText::jfl_s_viewPort(fl::flash::text::StageText* self, fl::flash::geom::Rectangle* rect) {
	em::stagetext_viewport(self->stage_text, rect->x, rect->y, rect->width, rect->height);
}
fl::flash::geom::Rectangle* StageText::jfl_g_viewPort(fl::flash::text::StageText* self) {
	jfl_notimpl;
}
void StageText::drawViewPortToBitmapData(fl::flash::text::StageText* self, fl::flash::display::BitmapData* jfl_a_param1) {
	jfl_notimpl;
}
void StageText::dispose(fl::flash::text::StageText* self) {
	if (self->stage_text) {
		em::stagetext_destroy(self->stage_text);
		self->stage_text = nullptr;
	}
}
jfl::Boolean StageText::jfl_g_visible(fl::flash::text::StageText* self) {
	return self->visible;
}
void StageText::jfl_s_visible(fl::flash::text::StageText* self, jfl::Boolean visible) {
	self->visible = visible;
	em::stagetext_set_visible(self->stage_text, self->stage && self->visible);
}
}
#endif
