#include <jfl/std.h>
#include <jfl/render/OpenGL.h>
#include <jfl/app/ApplicationBase.h>
#include <jfl/app/Loop.h>
#include <jfl/string.h>
#include <fl/flash/display/Stage.h>
#include <fl/flash/display/Stage3D.h>
#include <fl/flash/display/LoaderInfo.h>
#include <fl/flash/display/DisplayObjectContainer.h>
#include <fl/flash/display3D/Context3D.h>
#include <fl/flash/events/UncaughtErrorEvents.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/events/MouseEvent.h>
#include <fl/flash/events/EventDispatcher.h>
#include <fl/flash/utils/Timer.h>
#include <jfl/render/Context3D.h>
#include <jfl/text/TextRendering.h>
#include <em/Canvas.h>
#include <jfl/json.h>


namespace jfl {
using fl::flash::display::Stage;
using fl::flash::display::Stage3D;
using fl::flash::display::LoaderInfo;
using fl::flash::display::DisplayObject;
using fl::flash::display::DisplayObjectContainer;
using fl::flash::display3D::Context3D;
using fl::flash::events::UncaughtErrorEvents;
using fl::flash::events::Event;
using fl::flash::events::MouseEvent;
using fl::flash::events::EventDispatcher;
using fl::flash::utils::Timer;

class EMApplication : public ApplicationBase {
public:
	EMApplication();

	void init(ApplicationDescriptor* descriptor);

	double getTimer();

	void processEvents();
	bool shouldQuit();
	void present();
	void queryStageDimensions(int* width, int* height, double* scaleFactor);
};

static EMApplication* app;

Application* InitializeApplication(ApplicationDescriptor* descriptor) {
	app = new EMApplication();
	app->initialize(descriptor);
	return app;
}

Application* GetApplication() {
	return app;
}

EMApplication::EMApplication() {
}

void EMApplication::init(ApplicationDescriptor* descriptor) {
	if (!em::canvas_initialize()) {
		jfl::raise_message("cannot initialize webgl context");
	}
}

double EMApplication::getTimer() {
	return emscripten_get_now() / 1000;
}

void EMApplication::processEvents() {
}

bool EMApplication::shouldQuit() {
	return false;
}

void EMApplication::present() {
}

void EMApplication::queryStageDimensions(int* width, int* height, double* scaleFactor) {
	em::canvas_query_dimensions(width, height, scaleFactor);
}


}

static jfl::MouseButtonID translate_button(int button) {
	switch (button) {
	case 0: {
		return jfl::MouseButtonID::Left;
	}break;
	case 1: {
		return jfl::MouseButtonID::Middle;
	}break;
	case 2: {
		return jfl::MouseButtonID::Right;
	}break;
	default: {
		return (jfl::MouseButtonID)-1;
	}break;
	}
}


EM_EXPORT void canvas_cb_on_resize(int width, int height, double scale_factor) {
	using namespace jfl;

	app->onResize(width, height, scale_factor);
}

EM_EXPORT void canvas_cb_mouse_down(double x, double y, int button) {
	using namespace jfl;

	app->onMouseButton(x, y, translate_button(button), MouseButtonAction::Down);
}

EM_EXPORT void canvas_cb_mouse_up(double x, double y, int button) {
	using namespace jfl;

	app->onMouseButton(x, y, translate_button(button), MouseButtonAction::Up);
}

EM_EXPORT void canvas_cb_mouse_move(double x, double y) {
	using namespace jfl;

	app->onMouseMove(x, y);
}

EM_EXPORT void canvas_cb_mouse_wheel(double x, double y, double delta) {
	using namespace jfl;
	app->onMouseWheel(x, y, delta * 3);
}


