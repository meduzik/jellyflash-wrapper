#if __has_include(<fl/flash/crypto/generateRandomBytes.h>)
#include <jfl/common_impl.h>
#include <fl/flash/crypto/generateRandomBytes.h>
#include <fl/flash/utils/ByteArray.h>
#include <em/WebUtils.h>

namespace fl::flash::crypto{

using fl::flash::utils::ByteArray;

fl::flash::utils::ByteArray* generateRandomBytes(jfl::UInt length){
	ByteArray* bytes = ByteArray::jfl_New();
	ByteArray::jfl_s_length(bytes, length);
	em::webutils_generate_bytes(bytes->data.data(), length);
	return bytes;
}

}
#endif
