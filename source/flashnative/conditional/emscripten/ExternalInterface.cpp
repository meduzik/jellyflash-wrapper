#if __has_include(<fl/flash/external/ExternalInterface.h>)
#include <jfl/common_impl.h>
#include <fl/flash/external/ExternalInterface.h>
#include <em/ExternalInterface.h>

namespace fl::flash::external {
using namespace jfl;

jfl::Boolean ExternalInterface::jfl_Sg_available() {
	return true;
}
jfl::String ExternalInterface::jfl_pS__submit(jfl::String request) {
	std::string response;
	em::ExternalInterfaceSend(request.to_string_view(), response);
	return jfl::String::Make(response);
}
}

namespace em{
void ExternalInterfaceRecv(std::string_view request, std::string& response) {
	response = fl::flash::external::ExternalInterface::jfl_pS_callback(jfl::String::Make(request)).to_string_view();
}
}

#endif
