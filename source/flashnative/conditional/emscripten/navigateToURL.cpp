#if __has_include(<fl/flash/net/navigateToURL.h>)
#include <jfl/common_impl.h>
#include <fl/flash/net/navigateToURL.h>
#include <fl/flash/net/URLRequest.h>
#include <em/WebUtils.h>

namespace fl::flash::net{
void navigateToURL(fl::flash::net::URLRequest* request, jfl::String target){
	em::webutils_open_url(request->jfl_p__url.to_string_view());
}
}
#endif
