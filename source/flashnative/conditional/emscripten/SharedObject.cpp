#include <fl/flash/net/SharedObject.h>
#include <em/SharedObject.h>

namespace jfl::net {

struct SharedObjectImpl {
	um_string key;
};

SharedObjectImpl* SharedObjectImplCreate(std::string_view name, std::string_view path) {
	SharedObjectImpl* impl = new(unmanaged_allocate(sizeof(SharedObjectImpl)))SharedObjectImpl();
	impl->key = name;
	impl->key += '@';
	impl->key += path;
	return impl;
}

void SharedObjectImplDestroy(SharedObjectImpl* impl) {
	std::destroy_at(impl);
	unmanaged_free(impl, sizeof(SharedObjectImpl));
}

void SharedObjectImplLoad(SharedObjectImpl* impl, arena_vec<uint8_t>& data) {
	em::SharedObjectLoad(impl->key.c_str(), &data, [](void* data_ptr, void* buffer, size_t len){
		arena_vec<uint8_t>* data = (arena_vec<uint8_t>*)data_ptr;
		data->resize(len);
		memcpy(data->data(), buffer, len);
	});
}

void SharedObjectImplSave(SharedObjectImpl* impl, const arena_vec<uint8_t>& data) {
	em::SharedObjectStore(impl->key.c_str(), data.data(), data.size());
}

void SharedObjectImplClear(SharedObjectImpl* impl) {
	em::SharedObjectClear(impl->key.c_str());
}

}
