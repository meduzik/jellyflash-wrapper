#if __has_include(<fl/flash/utils/getTimer.h>)
#include <jfl/common_impl.h>
#include <fl/flash/utils/getTimer.h>
#include <jfl/app/application.h>

namespace fl::flash::utils {
jfl::Int getTimer() {
	return (jfl::Int)(jfl::GetApplication()->getTimer() * 1000);
}
}
#endif
