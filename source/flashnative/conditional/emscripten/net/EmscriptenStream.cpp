#include <jfl/net/StreamExecutor.h>
#include <fl/flash/filesystem/File.h>
#include <fl/flash/net/URLStream.h>
#include <fl/flash/net/URLRequest.h>
#include <fl/flash/net/URLRequestHeader.h>
#include <fl/flash/utils/ByteArray.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/events/ProgressEvent.h>
#include <jfl/app/Loop.h>
#include "../../../net/StreamExecutorBase.h"
#include <jfl/defer.h>
#include <em/URLStream.h>

namespace jfl {
using fl::flash::net::URLRequest;
using fl::flash::net::URLRequestHeader;
using fl::flash::utils::ByteArray;

class EmscriptenStream : public StreamExecutorBase {
public:
	EmscriptenStream(URLStream* stream, URLRequest* request) :
		StreamExecutorBase(stream)
	{
		url = request->jfl_p__url.to_string_view();
		ByteArray* data = URLRequest::jfl_p_encodeData(request);
		if (data) {
			payload.swap(data->data);
		}
		std::string_view method_name = request->jfl_p__method.to_string_view();
		if (method_name == "GET") {
			method = Method::GET;
		} else if (method_name == "POST") {
			method = Method::POST;
		} else {
			raise_message("bad request method");
		}
		fl::Array* headers_array = request->jfl_p__headers;
		if (headers_array) {
			jfl::UInt length = fl::Array::jfl_g_length(headers_array);
			for (jfl::UInt i = 0; i < length; i++) {
				jfl::Any header = fl::Array::jfl_get(headers_array, i);
				URLRequestHeader* requestHeader = (URLRequestHeader*)jfl::any_as_Class(header, &URLRequestHeader::jfl_Class);
				headers.push_back(std::make_pair(
					std::string(requestHeader->name.to_string_view()),
					std::string(requestHeader->value.to_string_view())
				));
			}
		}
	}

	void process() {
		stream.reset(new em::URLStream());
		switch ( method ){
		case Method::GET:{
			stream->set_method(em::URLStream::Method::GET);
		}break;
		case Method::POST: {
			stream->set_method(em::URLStream::Method::POST);
		}break;
		}
		stream->set_url(std::string(url));
		for (const auto& header : headers) {
			stream->set_header(header.first, header.second);
		}
		if ( !payload.empty() ){
			stream->set_data(payload.data(), payload.size());
		}
		stream->set_open_callback([&](){
			report_open();
		});
		stream->set_progress_callback([&](int loaded, int total) {
			bytes_loaded = loaded;
			bytes_total = total;
			report_progress(bytes_total, bytes_loaded, nullptr, 0);
		});
		stream->set_error_callback([&](int code, std::string message) {
			report_error(code, std::move(message));
		});
		stream->set_data_callback([&](const uint8_t* data, size_t len) {
			report_progress(bytes_total, bytes_loaded, data, len);
		});
		stream->set_complete_callback([&]() {
			report_complete();
		});
		stream->load();
	}
private:
	jfl::um_string url;
	jfl::um_vec<uint8_t> payload;
	jfl::um_vec<std::pair<std::string, std::string>> headers;
	int bytes_total = 0;
	int bytes_loaded = 0;

	std::unique_ptr<em::URLStream> stream;

	enum class Method {
		GET,
		POST
	} method;

};

std::shared_ptr<IStreamExecutor> CreateHttpStream(fl::flash::net::URLStream* target, fl::flash::net::URLRequest* request) {
	std::shared_ptr<EmscriptenStream> stream = std::make_shared<EmscriptenStream>(target, request);
	stream->start(stream);
	return stream;
}

std::shared_ptr<IStreamExecutor> CreateLocalFileStream(fl::flash::net::URLStream* target, PathBase base, std::string_view path) {
	jfl_notimpl;
}


}

