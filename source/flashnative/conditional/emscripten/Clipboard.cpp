#if __has_include(<fl/flash/desktop/Clipboard.h>)
#include <jfl/common_impl.h>
#include <fl/flash/desktop/Clipboard.h>
#include <fl/flash/desktop/ClipboardFormats.h>
#include <em/WebUtils.h>
namespace fl::flash::desktop{
jfl::Boolean Clipboard::setData(fl::flash::desktop::Clipboard* jfl_t, jfl::String format, jfl::Any data, jfl::Boolean serializable){
	if (format.to_string_view() != ClipboardFormats::jfl_S_TEXT_FORMAT.to_string_view() || serializable) {
		jfl::raise_message("unsupported clipboard format");
	}
	jfl::String string_data = jfl::any_to_String(data);
	return em::webutils_clipboard_copy(string_data.to_string_view());
}
jfl::Any Clipboard::getData(fl::flash::desktop::Clipboard* jfl_t, jfl::String jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
fl::Array* Clipboard::jfl_g_formats(fl::flash::desktop::Clipboard* jfl_t){
	jfl_notimpl;
}
void Clipboard::clear(fl::flash::desktop::Clipboard* jfl_t){
	em::webutils_clipboard_copy("");
}
void Clipboard::clearData(fl::flash::desktop::Clipboard* jfl_t, jfl::String jfl_a_param1){
	em::webutils_clipboard_copy("");
}
jfl::Boolean Clipboard::hasFormat(fl::flash::desktop::Clipboard* jfl_t, jfl::String jfl_a_format){
	jfl_notimpl;
}
}
#endif
