#include <em/emscripten.h>
/*
static uint8_t STATIC_MEM[768 * 1024 * 1024];
static uint8_t* mem_ptr = STATIC_MEM;
constexpr size_t NOMANSLAND_SIZE = 32;

void check_heap() {
	uint8_t* ptr = STATIC_MEM;
	while (ptr != mem_ptr) {
		uint32_t* hdr = (uint32_t*)ptr;
		size_t size = hdr[0];
		size_t alloc_size = (16 + size + NOMANSLAND_SIZE + 15) & ~15;
		for (size_t i = size + 16; i < alloc_size; i++) {
			if (ptr[i] != 0xCC) {
				std::cerr << "memory around " << (void*)ptr << " is corrupted 1" << std::endl;
				abort();
			}
		}
		if (hdr[1] != 1) {
			for (size_t i = 16; i < size; i++) {
				if (ptr[i] != 0xCC) {
					std::cerr << "memory around " << (void*)ptr << " is corrupted 2" << std::endl;
					abort();
				}
			}
		}
		ptr += alloc_size;
	}
}

EM_EXPORT void* malloc(size_t size){
	uint8_t* ptr = mem_ptr;
	uint8_t* end = STATIC_MEM + sizeof(STATIC_MEM);
	size_t alloc_size = (16 + size + NOMANSLAND_SIZE + 15) & ~15;
	if (end - ptr < alloc_size) {
		std::cerr << "memory exhausted!" << std::endl;
		abort();
	}
	mem_ptr += alloc_size;
	memset(ptr, 0xCC, alloc_size);
	uint32_t* hdr = (uint32_t*)ptr;
	hdr[0] = size;
	hdr[1] = 1;
	return ptr + 16;
}

EM_EXPORT void free(void* p){
	if (!p){
		return;
	}
	uint8_t* ptr = (uint8_t*)p;
	uint8_t* orig = (uint8_t*)(ptr - 16);
	size_t size = *(uint32_t*)orig;
	size_t alloc_size = (16 + size + NOMANSLAND_SIZE + 15) & ~15;
	for ( size_t i = size + 16; i < alloc_size; i++ ){
		if (orig[i] != 0xCC ){
			std::cerr << "memory around " << (void*)ptr << " is corrupted 3" << std::endl;
			abort();
		}
	}
	for (size_t i = 4; i < size + 16; i++) {
		orig[i] = 0xCC;
	}
}

EM_EXPORT void* realloc(void *p, size_t len) {
	if (len == 0) {
		free(p);
		return NULL;
	} else if (!p) {
		return malloc(len);
	} else {
		uint8_t* ptr = (uint8_t*)p;
		uint8_t* orig = (uint8_t*)(ptr - 16);
		size_t size = *(uint32_t*)orig;
		if (len <= size){
			return ptr;
		}else{
			void *ptrNew = malloc(len);
			if (ptrNew) {
				memcpy(ptrNew, ptr, size);
				free(ptr);
			}
			return ptrNew;
		}
	}
}
*/
