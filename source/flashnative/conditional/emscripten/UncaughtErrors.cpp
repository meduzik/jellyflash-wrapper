#include <em/emscripten.h>
#include <em/WebUtils.h>
#include <fl/jellyflash/system/CrashDebug.h>

using namespace fl::jellyflash::system;

void webutils_on_crash_impl(const char* message){
	if (!jfl_CInitGuard_CrashDebug){
		return;
	}
	CrashDebug::onCrash(CrashDebug::jfl_S_Instance, jfl::String::Make(message));
}
