#if __has_include(<fl/com/meduzik/audio/MusicPlayer.decl.h>)
#include <jfl/common_impl.h>
#include <fl/com/meduzik/audio/MusicPlayer.decl.h>
#include <jfl/memory.h>
#include <jfl/memory/ManagedHeap.h>

namespace fl::com::meduzik::audio{

void MusicPlayer::set_volume(fl::com::meduzik::audio::MusicPlayer* jfl_t, jfl::Number volume){
}

void MusicPlayer::play_track(fl::com::meduzik::audio::MusicPlayer* jfl_t, jfl::String uri, jfl::Function on_complete){
	if (on_complete.has_function()) {
		jfl::Any val = fl::com::meduzik::audio::MusicPlayer::jfl_S_TRACK_COMPLETE_NOT_STARTED;
		jfl::RestParams params(&val, 1);
		on_complete.invoke(&params);
	}
}

void MusicPlayer::stop(fl::com::meduzik::audio::MusicPlayer* jfl_t){
}

}
#endif
