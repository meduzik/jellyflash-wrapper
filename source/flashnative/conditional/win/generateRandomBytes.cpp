#if __has_include(<fl/flash/crypto/generateRandomBytes.h>)
#include <jfl/common_impl.h>
#include <fl/flash/crypto/generateRandomBytes.h>
#include <fl/flash/utils/ByteArray.h>

namespace fl::flash::crypto{

using fl::flash::utils::ByteArray;

static std::random_device device;
static std::seed_seq seed{
	device(),
	device(),
	device(),
	device(),
	(unsigned)clock(),
	(unsigned)std::chrono::high_resolution_clock::now().time_since_epoch().count()
};
static std::default_random_engine engine(seed);

fl::flash::utils::ByteArray* generateRandomBytes(jfl::UInt length){
	ByteArray* bytes = ByteArray::jfl_New();
	ByteArray::jfl_s_length(bytes, length);
	std::generate(bytes->data.begin(), bytes->data.end(), engine);
	return bytes;
}

}
#endif
