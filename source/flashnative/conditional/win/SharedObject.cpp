#include <fl/flash/net/SharedObject.h>
#include <fl/flash/filesystem/File.h>


namespace jfl::net{
using namespace fl::flash::filesystem;

struct SharedObjectImpl{
	std::filesystem::path path;
};

SharedObjectImpl* SharedObjectImplCreate(std::string_view name, std::string_view path){
	if (!jfl_CInitGuard_File) {
		jfl_CInit_File();
	}
	SharedObjectImpl* impl = new(unmanaged_allocate(sizeof(SharedObjectImpl)))SharedObjectImpl();
	impl->path = File::jfl_Sg_applicationStorageDirectory()->path / "#SharedObject";
	while (!path.empty() && path[0] == '/'){
		path = path.substr(1);
	}
	if (path.size() > 0){
		impl->path /= path;
	}
	impl->path.append(name);
	impl->path += ".jfso";
	return impl;
}

void SharedObjectImplDestroy(SharedObjectImpl* impl){
	std::destroy_at(impl);
	unmanaged_free(impl, sizeof(SharedObjectImpl));
}

void SharedObjectImplLoad(SharedObjectImpl* impl, arena_vec<uint8_t>& data){
	std::ifstream file(impl->path, std::ios::binary);
	if (!file) {
		data.clear();
		return;
	}
	file.seekg(0, std::ios::end);
	if (!file) {
		data.clear();
		return;
	}
	std::streampos size = file.tellg();
	file.seekg(0, std::ios::beg);
	size_t file_size = (size_t)size;
	data.resize(file_size);
	file.read((char*)data.data(), file_size);
	if (!file) {
		data.clear();
		return;
	}
}

void SharedObjectImplSave(SharedObjectImpl* impl, const arena_vec<uint8_t>& data){
	std::error_code ec;
	std::filesystem::create_directories(impl->path.parent_path(), ec);
	std::ofstream file(impl->path, std::ios::binary);
	file.write((const char*)data.data(), data.size());
}

void SharedObjectImplClear(SharedObjectImpl* impl){
	std::error_code ec;
	std::filesystem::remove(impl->path, ec);
	// ignore the error
}

}
