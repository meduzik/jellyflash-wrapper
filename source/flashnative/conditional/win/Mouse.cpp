#if __has_include(<fl/flash/ui/Mouse.h>)
#include <jfl/common_impl.h>
#include <fl/flash/ui/Mouse.h>

namespace fl::flash::ui{
void Mouse::jfl_S_hide(){
	jfl_notimpl;
}
void Mouse::jfl_S_show(){
	jfl_notimpl;
}
jfl::Boolean Mouse::jfl_Sg_supportsCursor(){
	return true;
}
jfl::String Mouse::jfl_Sg_cursor(){
	jfl_notimpl;
}
void Mouse::jfl_Ss_cursor(jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean Mouse::jfl_Sg_supportsNativeCursor(){
	jfl_notimpl;
}
}
#endif
