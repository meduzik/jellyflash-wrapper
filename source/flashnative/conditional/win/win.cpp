#include <jfl/win/win.h>
#include <jfl/string.h>

namespace jfl {

std::wstring Widen(jfl::String string){
	string.ensure_not_null();
	auto [ptr, len] = string.to_view();
	int wide_len = MultiByteToWideChar(
		CP_UTF8,
		0,
		(LPCCH)ptr,
		(int)len,
		nullptr,
		0
	);
	if (!wide_len) {
		CheckLastError();
	}
	std::wstring ws;
	ws.resize(wide_len);
	if ( !MultiByteToWideChar(
		CP_UTF8,
		0,
		(LPCCH)ptr,
		(int)len,
		ws.data(),
		wide_len
	)){
		CheckLastError();
	}
	return ws;
}

jfl::String Narrow(std::wstring_view string){
	int len = WideCharToMultiByte(
		CP_UTF8,
		0,
		string.data(),
		(int)string.size(),
		nullptr,
		0,
		nullptr,
		nullptr
	);
	if ( !len ){
		CheckLastError();
	}
	if ( len <= 15 ){
		jfl::String ss;
		ss.tag = (type_tag)len;
		if (!WideCharToMultiByte(
			CP_UTF8,
			0,
			string.data(),
			(int)string.size(),
			(LPSTR)ss.ss.value,
			len,
			nullptr,
			nullptr
		)){
			CheckLastError();
		}
		return ss;
	}else{
		jfl::StringObject* string_object = jfl::StringObject::New(len);
		if ( !WideCharToMultiByte(
			CP_UTF8,
			0,
			string.data(),
			(int)string.size(),
			(LPSTR)string_object->data,
			len,
			nullptr,
			nullptr
		) ){
			CheckLastError();
		}
		return string_object;
	}
}

jfl::String GetErrorMessage(DWORD code){
	LPWSTR str = nullptr;
	if ( !FormatMessageW(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL, 
		code, 
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPWSTR)&str,
		0,
		NULL
	) ){
		CheckLastError();
	}
	String string = Narrow(str);
	LocalFree(str);
	return string;
}

}