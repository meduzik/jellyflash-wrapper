#if __has_include(<fl/flash/text/StageText.h>)
#include <jfl/common_impl.h>
#include <fl/flash/text/StageText.h>
namespace fl::flash::text{
StageText::~StageText() {
}
void StageText::jfl_GC_user(fl::flash::text::StageText* self) {
}
void StageText::jfl_p_init(fl::flash::text::StageText* self, fl::flash::text::StageTextInitOptions* opts){
	jfl_notimpl;
}
void StageText::jfl_s_editable(fl::flash::text::StageText* self, jfl::Boolean editable){
	jfl_notimpl;
}
jfl::Boolean StageText::jfl_g_editable(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_text(fl::flash::text::StageText* self, jfl::String text){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_text(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_clearButtonMode(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void StageText::jfl_s_fontFamily(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_fontFamily(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_fontSize(fl::flash::text::StageText* self, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int StageText::jfl_g_fontSize(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_color(fl::flash::text::StageText* self, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
jfl::UInt StageText::jfl_g_color(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_fontWeight(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_fontWeight(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_fontPosture(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_fontPosture(fl::flash::text::StageText* self){
	jfl_notimpl;
}
jfl::Boolean StageText::jfl_g_displayAsPassword(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_displayAsPassword(fl::flash::text::StageText* self, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
void StageText::jfl_s_maxChars(fl::flash::text::StageText* self, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int StageText::jfl_g_maxChars(fl::flash::text::StageText* self){
	jfl_notimpl;
}
jfl::Boolean StageText::jfl_g_multiline(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_restrict(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_restrict(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_returnKeyLabel(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_returnKeyLabel(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_textAlign(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_textAlign(fl::flash::text::StageText* self){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_softKeyboardType(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_softKeyboardType(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void StageText::jfl_s_autoCapitalize(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_autoCapitalize(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_autoCorrect(fl::flash::text::StageText* self, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean StageText::jfl_g_autoCorrect(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_locale(fl::flash::text::StageText* self, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String StageText::jfl_g_locale(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::assignFocus(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::selectRange(fl::flash::text::StageText* self, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	jfl_notimpl;
}
jfl::Int StageText::jfl_g_selectionActiveIndex(fl::flash::text::StageText* self){
	jfl_notimpl;
}
jfl::Int StageText::jfl_g_selectionAnchorIndex(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_stage(fl::flash::text::StageText* self, fl::flash::display::Stage* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::display::Stage* StageText::jfl_g_stage(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_viewPort(fl::flash::text::StageText* self, fl::flash::geom::Rectangle* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* StageText::jfl_g_viewPort(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::drawViewPortToBitmapData(fl::flash::text::StageText* self, fl::flash::display::BitmapData* jfl_a_param1){
	jfl_notimpl;
}
void StageText::dispose(fl::flash::text::StageText* self){
	jfl_notimpl;
}
jfl::Boolean StageText::jfl_g_visible(fl::flash::text::StageText* self){
	jfl_notimpl;
}
void StageText::jfl_s_visible(fl::flash::text::StageText* self, jfl::Boolean visible){
	jfl_notimpl;
}
}
#endif
