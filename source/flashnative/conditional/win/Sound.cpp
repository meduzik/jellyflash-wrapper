#pragma once
#include <jfl/media/Sound.h>
#include <fl/flash/media/Sound.decl.h>
#include <fl/flash/media/SoundChannel.decl.h>
#include <fl/flash/media/SoundTransform.decl.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/events/IOErrorEvent.h>
#include <jfl/app/Loop.h>

namespace jfl::media {

void SoundSetGlobalVolume(double value) {
}

struct SoundImpl {
	gc_ptr<fl::flash::media::Sound> object;
};

struct SoundChannelImpl {
};


SoundImpl* SoundImplCreate(fl::flash::media::Sound* sound) {
	SoundImpl* impl = new(jfl::unmanaged_allocate(sizeof(SoundImpl)))SoundImpl;
	sound->impl = impl;
	impl->object = sound;
	return impl;
}

void SoundImplDecode(SoundImpl* impl, const uint8_t* data, size_t len) {
	jfl::PostMainLoopTask([&, sound_object = impl->object]() {
		if (sound_object->impl) {
			fl::flash::events::IOErrorEvent* event = fl::flash::events::IOErrorEvent::jfl_New(
				fl::flash::events::IOErrorEvent::jfl_S_IO_ERROR,
				false,
				false,
				jfl::String::Make("audio not implemented"),
				2011
			);
			fl::flash::events::EventDispatcher::dispatchEvent(
				(fl::flash::events::EventDispatcher*)sound_object.get(),
				(fl::flash::events::Event*)event
			);
		}
	});
}

void SoundImplDestroy(SoundImpl* impl) {
	impl->~SoundImpl();
	jfl::unmanaged_free(impl, sizeof(SoundImpl));
}


SoundChannelImpl* SoundImplPlay(SoundImpl* impl, fl::flash::media::SoundChannel* channel, double position, fl::flash::media::SoundTransform* transform) {
	return nullptr;
}

void SoundChannelImplDestroy(SoundChannelImpl* impl) {
}

double SoundChannelImplGetPosition(SoundChannelImpl* impl) {
	return 0;
}

void SoundChannelImplUpdateTransform(SoundChannelImpl* impl, fl::flash::media::SoundTransform* transform) {
}

}
