#if __has_include(<fl/flash/desktop/Clipboard.h>)
#include <jfl/common_impl.h>
#include <fl/flash/desktop/Clipboard.h>
namespace fl::flash::desktop{
jfl::Boolean Clipboard::setData(fl::flash::desktop::Clipboard* jfl_t, jfl::String jfl_a_format, jfl::Any jfl_a_data, jfl::Boolean jfl_a_serializable){
	jfl_notimpl;
}
jfl::Any Clipboard::getData(fl::flash::desktop::Clipboard* jfl_t, jfl::String jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
fl::Array* Clipboard::jfl_g_formats(fl::flash::desktop::Clipboard* jfl_t){
	jfl_notimpl;
}
void Clipboard::clear(fl::flash::desktop::Clipboard* jfl_t){
	jfl_notimpl;
}
void Clipboard::clearData(fl::flash::desktop::Clipboard* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean Clipboard::hasFormat(fl::flash::desktop::Clipboard* jfl_t, jfl::String jfl_a_format){
	jfl_notimpl;
}
}
#endif
