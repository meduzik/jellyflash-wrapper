#include <jfl/net/StreamExecutor.h>
#include <fl/flash/filesystem/File.h>
#include <fl/flash/net/URLStream.h>
#include <fl/flash/net/URLRequest.h>
#include <fl/flash/utils/ByteArray.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/events/ProgressEvent.h>
#include <jfl/app/Loop.h>
#include "../../../net/StreamExecutorBase.h"
#include <curl/curl.h>
#include <jfl/defer.h>

namespace jfl {
using fl::flash::net::URLRequest;
using fl::flash::utils::ByteArray;

static std::mutex IOLock;
static bool curl_init = false;

class RemoteStream : public StreamExecutorBase {
public:
	RemoteStream(URLStream* stream, URLRequest* request):
		StreamExecutorBase(stream),
		request(request),
		curl(nullptr),
		bytes_loaded(0),
		open_reported(false)
	{
		url = request->jfl_p__url.to_string_view();
		ByteArray* data = URLRequest::jfl_p_encodeData(request);
		if (data){
			payload.swap(data->data);
		}
		std::string_view method_name = request->jfl_p__method.to_string_view();
		if (method_name == "GET"){
			method = Method::GET;
		}else if (method_name == "POST") {
			method = Method::POST;
		}else{
			raise_message("bad request method");
		}
	}

	static size_t write_callback(void *contents, size_t size, size_t nmemb, void* ptr) {
		RemoteStream* stream = (RemoteStream*)ptr;
		size_t data_len = size * nmemb;
		stream->bytes_loaded += (int)data_len;
		if (!stream->open_reported) {
			stream->open_reported = true;
			stream->report_open();
		}
		stream->update_progress();
		stream->report_progress(stream->bytes_total, stream->bytes_loaded, (const uint8_t*)contents, data_len);
		return size * nmemb;
	}

	void update_progress(){
		curl_off_t bytes_download_total;
		curl_off_t bytes_upload_total;
		curl_off_t bytes_downloaded;
		curl_off_t bytes_uploaded;
		curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, &bytes_download_total);
		curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_UPLOAD_T, &bytes_upload_total);
		if (bytes_upload_total == -1) {
			bytes_upload_total = 0;
		}
		if (bytes_download_total == -1) {
			bytes_download_total = 0;
		}
		curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD_T, &bytes_downloaded);
		curl_easy_getinfo(curl, CURLINFO_SIZE_UPLOAD_T, &bytes_uploaded);
		int new_loaded = (int)(bytes_uploaded + bytes_downloaded);
		int new_total = (int)(bytes_download_total + bytes_upload_total);
		bytes_loaded = new_loaded;
		bytes_total = new_total;
	}

	static int progress_callback(
		void* ptr,
		curl_off_t dltotal,
		curl_off_t dlnow,
		curl_off_t ultotal,
		curl_off_t ulnow
	)
	{
		RemoteStream* stream = (RemoteStream*)ptr;
		if ( !stream->open_reported ){
			stream->open_reported = true;
			stream->report_open();
		}
		stream->update_progress();
		stream->report_progress(stream->bytes_total, stream->bytes_loaded, nullptr, 0);
		if ( stream->is_detached() ){
			return 1;
		}
		return 0;
	}
	
	void process(){
		CURLcode res;
		curl = curl_easy_init();
		auto cleanup_curl = defer([&](){ curl_easy_cleanup(curl); });
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 5);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 30000);
		curl_easy_setopt(curl, CURLOPT_FAILONERROR, true);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)this);
		curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progress_callback);
		curl_easy_setopt(curl, CURLOPT_XFERINFODATA, (void*)this);
		switch ( method ){
		case Method::GET:{
			// get is by default
		}break;
		case Method::POST:{
			curl_easy_setopt(curl, CURLOPT_POST, 1); 
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload.data());
			curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)payload.size());
		}break;
		}

		res = curl_easy_perform(curl);
		if (res != CURLE_OK){
			report_error(res, curl_easy_strerror(res));
			return;
		}else{
			if (!open_reported) {
				report_open();
			}
			report_complete();
		}
	}
private:
	fl::flash::net::URLRequest* request;
	CURL* curl = nullptr;
	int bytes_loaded;
	int bytes_total;
	bool open_reported;

	jfl::um_string url;
	jfl::um_vec<uint8_t> payload;

	enum class Method{
		GET,
		POST
	} method;
	
};

std::shared_ptr<IStreamExecutor> CreateHttpStream(fl::flash::net::URLStream* target, fl::flash::net::URLRequest* request) {
	if (!curl_init) {
		curl_init = true;
		curl_global_init(CURL_GLOBAL_ALL);
	}

	std::shared_ptr<RemoteStream> stream = std::make_shared<RemoteStream>(target, request);
	stream->start(stream);
	return stream;
}

}

