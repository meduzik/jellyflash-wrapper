#include <jfl/net/StreamExecutor.h>
#include <fl/flash/filesystem/File.h>
#include <fl/flash/net/URLStream.h>
#include <fl/flash/net/URLRequest.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/events/ProgressEvent.h>
#include <jfl/app/Loop.h>
#include "../../../net/StreamExecutorBase.h"

namespace jfl {

static std::mutex IOLock;

class LocalFileStream : public StreamExecutorBase {
public:
	LocalFileStream(fl::flash::net::URLStream* target, std::filesystem::path&& path) :
		StreamExecutorBase(target),
		path(std::move(path)) 
	{
	}

	~LocalFileStream() {
	}

	void process(){
		int bytes_loaded = 0;
		std::ifstream input(path, std::ios::binary);
		if (!input.is_open()) {
			return report_error(404, strerror(errno));
		}
		report_open();
		input.seekg(0, std::ios::end);
		std::streampos size = input.tellg();
		size_t bytes_total = (size_t)size;
		input.seekg(0, std::ios::beg);
		report_progress((int)bytes_total, 0, nullptr, 0);
		constexpr size_t ChunkSize = 8192;
		uint8_t buffer[ChunkSize];
		for (size_t pos = 0; pos < bytes_total; pos += ChunkSize) {
			if (is_detached()) {
				break;
			}
			size_t chunk_size = std::min(bytes_total - pos, ChunkSize);
			input.read((char*)buffer, chunk_size);
			if (is_detached()) {
				break;
			}
			if (!input) {
				report_error(1005, "read error");
				return;
			}
			bytes_loaded += chunk_size;
			report_progress((int)bytes_total, (int)bytes_loaded, buffer, chunk_size);
		}
		report_complete();
	}
private:
	std::filesystem::path path;
	fl::flash::net::URLStream* target;
};

std::string_view remove_leading_slashes(std::string_view s) {
	for (size_t i = 0; i < s.size(); i++) {
		if (s[i] != '/') {
			return s.substr(i);
		}
	}
	return "";
}

std::shared_ptr<IStreamExecutor> CreateLocalFileStream(fl::flash::net::URLStream* target, PathBase base, std::string_view path) {
	if(!fl::flash::filesystem::jfl_CInitGuard_File){
		fl::flash::filesystem::jfl_CInit_File();
	}

	std::shared_ptr<LocalFileStream> stream;
	switch (base) {
	case PathBase::AppPackage: {
		stream.reset(new LocalFileStream(target, fl::flash::filesystem::File::jfl_pS__applicationDirectory->path / remove_leading_slashes(path)));
	}break;
	case PathBase::AppStorage: {
		stream.reset(new LocalFileStream(target, fl::flash::filesystem::File::jfl_pS__applicationStorageDirectory->path / remove_leading_slashes(path)));
	}break;
	default: {
		jfl_notimpl;
	}break;
	}

	stream->start(stream);

	return stream;
}

}

