#if __has_include(<fl/flash/external/ExternalInterface.h>)
#include <jfl/common_impl.h>
#include <fl/flash/external/ExternalInterface.h>
namespace fl::flash::external {
jfl::Boolean ExternalInterface::jfl_Sg_available() {
	return false;
}
jfl::String ExternalInterface::jfl_pS__submit(jfl::String jfl_a_request) {
	jfl::raise_message("ExternalInterface is not supported");
}
}
#endif
