#if __has_include(<fl/flash/filesystem/File.h>)
#include <jfl/common_impl.h>
#include <fl/flash/filesystem/File.h>
#include <jfl/win/file.h>
#include <jfl/app/application.h>
#include <ShlObj.h>
#include <Shlwapi.h>

namespace fl::flash::filesystem {
jfl::String File::jfl_pS_initSystemCharset() {
	return jfl::StringSmall{3, "BAD"};
}
jfl::String File::jfl_pS_initSeparator() {
	return jfl::StringSmall{ 1, "\\" };
}
jfl::String File::jfl_pS_initLineEnding() {
	return jfl::StringSmall{ 2, "\r\n" };
}
jfl::String File::jfl_pS_initUserDirectory() {
	WCHAR path[MAX_PATH];
	jfl::CheckWinAPI(SHGetFolderPathW(nullptr, CSIDL_PROFILE, nullptr, 0, path));
	return jfl::Narrow(path);
}
jfl::String File::jfl_pS_initDocumentsDirectory() {
	WCHAR path[MAX_PATH];
	jfl::CheckWinAPI(SHGetFolderPathW(nullptr, CSIDL_MYDOCUMENTS, nullptr, 0, path));
	return jfl::Narrow(path);
}
jfl::String File::jfl_pS_initDesktopDirectory() {
	WCHAR path[MAX_PATH];
	jfl::CheckWinAPI(SHGetFolderPathW(nullptr, CSIDL_DESKTOPDIRECTORY, nullptr, 0, path));
	return jfl::Narrow(path);
}
jfl::String File::jfl_pS_initApplicationStorageDirectory() {
	WCHAR path[MAX_PATH];
	jfl::CheckWinAPI(SHGetFolderPathW(nullptr, CSIDL_APPDATA, nullptr, 0, path));
	if ( !PathAppendW(path, jfl::Widen(jfl::ApplicationPackageID).c_str()) ){
		jfl::CheckLastError();
	}
	return jfl::Narrow(path);
}
jfl::String File::jfl_pS_initApplicationDirectory() {
	/*
	WCHAR path[MAX_PATH];
	DWORD len = GetModuleFileNameW(GetModuleHandle(nullptr), path, MAX_PATH);
	if( !PathRemoveFileSpecW(path) ){
		jfl::CheckLastError();
	}
	return jfl::Narrow({path, (size_t)lstrlenW(path)});
	*/
	WCHAR path[MAX_PATH];
	jfl::CheckWinAPI(GetCurrentDirectoryW(MAX_PATH, path));
	return jfl::Narrow(path);
}
jfl::String File::jfl_pS_initCacheDirectory() {
	WCHAR path[MAX_PATH];
	if ( !GetTempPathW(MAX_PATH, path) ){
		jfl::CheckLastError();
	}
	return jfl::Narrow(path);
}
}
#endif
