#include <jfl/math/transform2d.h>

namespace jfl{

void transform2d::_decompose(){
	jfl_notimpl;
}

void transform2d::_recompose() {
	Number cosA = cos(decomp.rotation);
	Number sinA = sin(decomp.rotation);

	matrix.a = decomp.sx * cosA;
	matrix.c = decomp.sy * decomp.skew * cosA - sinA * decomp.sy;
	matrix.b = sinA * decomp.sx;
	matrix.d = decomp.sy * decomp.skew * sinA + cosA * decomp.sy;
}

}
