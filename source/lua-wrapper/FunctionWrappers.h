#pragma once
#include <jfl/common_impl.h>
#include <lua.hpp>

namespace jfl {

lua_CFunction RegisterFunction(jfl::Function function);
void UnregisterFunction(jfl::Function function);
lua_CFunction GetFunctionPtr(jfl::Function function);

}
