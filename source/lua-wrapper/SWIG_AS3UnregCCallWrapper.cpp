#if __has_include(<fl/crossbridge/lua/swig/SWIG_AS3UnregCCallWrapper.h>)
#include <jfl/common_impl.h>
#include <fl/crossbridge/lua/swig/SWIG_AS3UnregCCallWrapper.h>
#include "FunctionWrappers.h"
namespace fl::crossbridge::lua::swig{
void SWIG_AS3UnregCCallWrapper(jfl::Function func){
	jfl::UnregisterFunction(func);
}
}
#endif
