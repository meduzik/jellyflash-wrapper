#if __has_include(<fl/crossbridge/lua/CModule.h>)
#include <jfl/common_impl.h>
#include <fl/crossbridge/lua/CModule.h>
#include <fl/flash/utils/IDataInput.h>
#include <fl/flash/utils/ByteArray.h>
namespace fl::crossbridge::lua{
using namespace jfl;

fl::flash::utils::ByteArray* CModule::jfl_Sg_ram(){
	abort();
}
jfl::Boolean CModule::jfl_Sg_throwWhenOutOfMemory(){
	abort();
}
void CModule::jfl_Ss_throwWhenOutOfMemory(jfl::Boolean jfl_a_param1){
	abort();
}
jfl::Boolean CModule::jfl_Sg_canUseWorkers(){
	abort();
}
void CModule::jfl_S_resolveWeaks(jfl::Function jfl_a_param1){
	abort();
}
jfl::Boolean CModule::jfl_S_runningAsWorker(){
	abort();
}
jfl::Int CModule::jfl_S_getPublicSymbol(jfl::String jfl_a_param1){
	abort();
}
fl::Array* CModule::jfl_S_getSymsUsingMD(){
	abort();
}
void CModule::jfl_S_prepForThreadedExec(){
	abort();
}
void CModule::jfl_S_explicitlyInitAllModules(){
	abort();
}
jfl::Int CModule::jfl_S_allocFunPtrs(jfl::String jfl_a_param1, jfl::Int jfl_a_param2, jfl::Int jfl_a_param3){
	abort();
}
jfl::Int CModule::jfl_S_allocTextSect(jfl::String jfl_a_param1, jfl::String jfl_a_param2, jfl::Int jfl_a_param3){
	abort();
}
jfl::Int CModule::jfl_S_allocDataSect(jfl::String jfl_a_param1, jfl::String jfl_a_param2, jfl::Int jfl_a_param3, jfl::Int jfl_a_param4){
	abort();
}
void CModule::jfl_S_write8(jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	abort();
}
void CModule::jfl_S_write16(jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	abort();
}
void CModule::jfl_S_write32(jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	abort();
}
void CModule::jfl_S_writeFloat(jfl::Int jfl_a_param1, jfl::Number jfl_a_param2){
	abort();
}
void CModule::jfl_S_writeDouble(jfl::Int jfl_a_param1, jfl::Number jfl_a_param2){
	abort();
}
void CModule::jfl_S_writeBytes(jfl::Int block, jfl::UInt length, jfl::I<fl::flash::utils::IDataInput> input){
	void* ptr = (void*)block;
	input.v->readBytes(input.o, CModule::jfl_pS_TmpByteArray, 0, length);
	memcpy(ptr, CModule::jfl_pS_TmpByteArray->data.data(), length);
}
jfl::Int CModule::jfl_S_read8(jfl::Int jfl_a_param1){
	abort();
}
jfl::Int CModule::jfl_S_read16(jfl::Int jfl_a_param1){
	abort();
}
jfl::Int CModule::jfl_S_read32(jfl::Int jfl_a_param1){
	abort();
}
jfl::Number CModule::jfl_S_readFloat(jfl::Int jfl_a_param1){
	abort();
}
jfl::Number CModule::jfl_S_readDouble(jfl::Int jfl_a_param1){
	abort();
}
void CModule::jfl_S_readBytes(jfl::Int block, jfl::UInt length, jfl::I<fl::flash::utils::IDataOutput> output){
	void* ptr = (void*)block;
	CModule::jfl_pS_TmpByteArray->data.resize(length);
	memcpy(CModule::jfl_pS_TmpByteArray->data.data(), ptr, length);
	output.v->writeBytes(output.o, CModule::jfl_pS_TmpByteArray, 0, length);
}
void CModule::jfl_S_push32(jfl::Int jfl_a_param1){
	abort();
}
jfl::Int CModule::jfl_S_pop32(){
	abort();
}
jfl::Int CModule::jfl_S_malloc(jfl::Int size){
	return (Int)malloc(size);
}
void CModule::jfl_S_free(jfl::Int block){
	free((void*)block);
}
jfl::Int CModule::jfl_S_alloca(jfl::Int jfl_a_param1){
	abort();
}
jfl::Int CModule::jfl_S_writeString(jfl::Int jfl_a_param1, jfl::String jfl_a_param2){
	abort();
}
jfl::String CModule::jfl_S_readString(jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	abort();
}
jfl::Int CModule::jfl_S_writeLatin1String(jfl::Int jfl_a_param1, jfl::String jfl_a_param2){
	abort();
}
jfl::String CModule::jfl_S_readLatin1String(jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	abort();
}
jfl::Vector<jfl::Int>* CModule::jfl_S_readIntVector(jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	abort();
}
void CModule::jfl_S_writeIntVector(jfl::Int jfl_a_param1, jfl::Vector<jfl::Int>* jfl_a_param2){
	abort();
}
jfl::Int CModule::jfl_S_mallocString(jfl::String jfl_a_param1){
	abort();
}
jfl::Int CModule::jfl_S_allocaString(jfl::String jfl_a_param1){
	abort();
}
jfl::Int CModule::jfl_S_mallocLatin1String(jfl::String jfl_a_param1){
	abort();
}
jfl::Int CModule::jfl_S_allocaLatin1String(jfl::String jfl_a_param1){
	abort();
}
void CModule::jfl_S_regFun(jfl::Int jfl_a_param1, jfl::Function jfl_a_param2){
	abort();
}
jfl::Int CModule::jfl_S_callI(jfl::Int jfl_a_param1, jfl::Vector<jfl::Int>* jfl_a_param2, jfl::Int jfl_a_param3, jfl::Boolean jfl_a_param4){
	abort();
}
jfl::Number CModule::jfl_S_callN(jfl::Int jfl_a_param1, jfl::Vector<jfl::Int>* jfl_a_param2, jfl::Int jfl_a_param3, jfl::Boolean jfl_a_param4){
	abort();
}
fl::crossbridge::lua::CModule* CModule::jfl_S_regModule(jfl::Any jfl_a_param1, jfl::Function jfl_a_param2, fl::Array* jfl_a_param3, jfl::String jfl_a_param4){
	abort();
}
fl::crossbridge::lua::CModule* CModule::jfl_S_getModuleByPackage(jfl::String jfl_a_param1){
	abort();
}
jfl::Vector<jfl::Object*>* CModule::jfl_S_getModuleVector(){
	abort();
}
void CModule::jfl_S_sendMetric(jfl::String jfl_a_param1, jfl::Any jfl_a_param2){
	abort();
}
void CModule::jfl_S_sendSpanMetric(jfl::String jfl_a_param1, jfl::Number jfl_a_param2, jfl::Any jfl_a_param3){
	abort();
}
jfl::Any CModule::jfl_Sg_kernel(){
	abort();
}
void CModule::jfl_Ss_kernel(jfl::Any jfl_a_param1){
	abort();
}
void CModule::jfl_S_runCtors(){
	abort();
}
void CModule::jfl_S_runDtors(){
	abort();
}
void CModule::jfl_S_serviceUIRequests(){
	abort();
}
jfl::Int CModule::jfl_S_start(jfl::Any jfl_a_param1, jfl::Vector<jfl::String>* jfl_a_param2, jfl::Vector<jfl::String>* jfl_a_param3, jfl::Boolean jfl_a_param4){
	abort();
}
void CModule::jfl_S_startAsync(jfl::Any jfl_a_param1, jfl::Vector<jfl::String>* jfl_a_param2, jfl::Vector<jfl::String>* jfl_a_param3, jfl::Boolean jfl_a_param4){
	abort();
}
void CModule::jfl_S_startBackground(jfl::Any jfl_a_param1, jfl::Vector<jfl::String>* jfl_a_param2, jfl::Vector<jfl::String>* jfl_a_param3, jfl::Int jfl_a_param4){
	abort();
}
}
#endif
