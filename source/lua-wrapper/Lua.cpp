#if __has_include(<fl/Lua.h>)
#include <jfl/common_impl.h>
#include <fl/Lua.h>
#include <lua.hpp>
#include "FunctionWrappers.h"
namespace fl{
using namespace jfl;

jfl::Int Lua::jfl_S_lua_newstate(jfl::Function p1, jfl::Int p2){
	jfl_notimpl;
}
void Lua::jfl_S_lua_close(jfl::Int p1){
	lua_close((lua_State*)p1);
}
jfl::Int Lua::jfl_S_lua_newthread(jfl::Int p1){
	return (Int)lua_newthread((lua_State*)p1);
}
jfl::Function Lua::jfl_S_lua_atpanic(jfl::Int p1, jfl::Function p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_version(jfl::Int p1){
	return (Int)lua_version((lua_State*)p1);
}
jfl::Int Lua::jfl_S_lua_absindex(jfl::Int p1, jfl::Int p2){
	return lua_absindex((lua_State*)p1, p2);
}
jfl::Int Lua::jfl_S_lua_gettop(jfl::Int p1){
	return lua_gettop((lua_State*)p1);
}
void Lua::jfl_S_lua_settop(jfl::Int p1, jfl::Int p2){
	lua_settop((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_pushvalue(jfl::Int p1, jfl::Int p2){
	lua_pushvalue((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_remove(jfl::Int p1, jfl::Int p2){
	lua_remove((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_insert(jfl::Int p1, jfl::Int p2){
	lua_insert((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_replace(jfl::Int p1, jfl::Int p2){
	lua_replace((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_copy(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	lua_copy((lua_State*)p1, p2, p3);
}
jfl::Int Lua::jfl_S_lua_checkstack(jfl::Int p1, jfl::Int p2){
	return lua_checkstack((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_xmove(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	lua_xmove((lua_State*)p1, (lua_State*)p2, p3);
}
jfl::Int Lua::jfl_S_lua_isnumber(jfl::Int p1, jfl::Int p2){
	return lua_isnumber((lua_State*)p1, p2);
}
jfl::Int Lua::jfl_S_lua_isstring(jfl::Int p1, jfl::Int p2){
	return lua_isstring((lua_State*)p1, p2);
}
jfl::Int Lua::jfl_S_lua_iscfunction(jfl::Int p1, jfl::Int p2){
	return lua_iscfunction((lua_State*)p1, p2);
}
jfl::Int Lua::jfl_S_lua_isuserdata(jfl::Int p1, jfl::Int p2){
	return lua_isuserdata((lua_State*)p1, p2);
}
jfl::Int Lua::jfl_S_lua_type(jfl::Int p1, jfl::Int p2){
	return lua_type((lua_State*)p1, p2);
}
jfl::String Lua::jfl_S_lua_typename(jfl::Int p1, jfl::Int p2){
	const char* s = lua_typename((lua_State*)p1, p2);
	return jfl::String::Make({s, strlen(s)});
}
jfl::Number Lua::jfl_S_lua_tonumberx(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	return lua_tonumberx((lua_State*)p1, p2, (int*)p3);
}
jfl::Any Lua::jfl_S_lua_tointegerx(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_tounsignedx(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	return lua_tounsignedx((lua_State*)p1, p2, (int*)p3);
}
jfl::Int Lua::jfl_S_lua_toboolean(jfl::Int p1, jfl::Int p2){
	return lua_toboolean((lua_State*)p1, p2);
}
jfl::String Lua::jfl_S_lua_tolstring(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	size_t len = 0;
	const char* s = lua_tolstring((lua_State*)p1, p2, &len);
	return jfl::String::Make({s, len});
}
jfl::Any Lua::jfl_S_lua_rawlen(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Function Lua::jfl_S_lua_tocfunction(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_touserdata(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_tothread(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_topointer(jfl::Int p1, jfl::Int p2){
	return (jfl::Int)lua_topointer((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_arith(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_rawequal(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_compare(jfl::Int p1, jfl::Int p2, jfl::Int p3, jfl::Int p4){
	jfl_notimpl;
}
void Lua::jfl_S_lua_pushnil(jfl::Int p1){
	lua_pushnil((lua_State*)p1);
}
void Lua::jfl_S_lua_pushnumber(jfl::Int p1, jfl::Number p2){
	lua_pushnumber((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_pushinteger(jfl::Int p1, jfl::Any p2){
	jfl_notimpl;
}
void Lua::jfl_S_lua_pushunsigned(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_lua_pushlstring(jfl::Int p1, jfl::String p2, jfl::Any p3){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_lua_pushstring(jfl::Int p1, jfl::String p2){
	if ( p2.tag == type_tag::Null ){
		lua_pushnil((lua_State*)p1);
		return p2;
	}else{
		auto view = p2.to_string_view();
		lua_pushlstring((lua_State*)p1, view.data(), view.size());
		return p2;
	}
}
jfl::String Lua::jfl_S_lua_pushvfstring(jfl::Int p1, jfl::String p2, jfl::Any p3){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_lua_pushfstring(jfl::Int p1, jfl::String p2, jfl::Any p3){
	jfl_notimpl;
}
void Lua::jfl_S_lua_pushcclosure(jfl::Int p1, jfl::Function p2, jfl::Int p3){
	lua_pushcclosure((lua_State*)p1, RegisterFunction(p2), p3);
}
void Lua::jfl_S_lua_pushboolean(jfl::Int p1, jfl::Int p2){
	lua_pushboolean((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_pushlightuserdata(jfl::Int p1, jfl::Int p2){
	lua_pushlightuserdata((lua_State*)p1, (void*)p2);
}
jfl::Int Lua::jfl_S_lua_pushthread(jfl::Int p1){
	return lua_pushthread((lua_State*)p1);
}
void Lua::jfl_S_lua_getglobal(jfl::Int p1, jfl::String p2){
	std::string s(p2.to_string_view());
	lua_getglobal((lua_State*)p1, s.c_str());
}
void Lua::jfl_S_lua_gettable(jfl::Int p1, jfl::Int p2){
	lua_gettable((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_getfield(jfl::Int p1, jfl::Int p2, jfl::String p3){
	std::string s(p3.to_string_view());
	lua_getfield((lua_State*)p1, p2, s.c_str());
}
void Lua::jfl_S_lua_rawget(jfl::Int p1, jfl::Int p2){
	lua_rawget((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_rawgeti(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	lua_rawgeti((lua_State*)p1, p2, p3);
}
void Lua::jfl_S_lua_rawgetp(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	lua_rawgetp((lua_State*)p1, p2, (const void*)p3);
}
void Lua::jfl_S_lua_createtable(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	lua_createtable((lua_State*)p1, p2, p3);
}
jfl::Int Lua::jfl_S_lua_newuserdata(jfl::Int p1, jfl::Any p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_getmetatable(jfl::Int p1, jfl::Int p2){
	return lua_getmetatable((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_getuservalue(jfl::Int p1, jfl::Int p2){
	lua_getuservalue((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_setglobal(jfl::Int p1, jfl::String p2){
	std::string s(p2.to_string_view());
	lua_setglobal((lua_State*)p1, s.c_str());
}
void Lua::jfl_S_lua_settable(jfl::Int p1, jfl::Int p2){
	lua_settable((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_setfield(jfl::Int p1, jfl::Int p2, jfl::String p3){
	std::string s(p3.to_string_view());
	lua_setfield((lua_State*)p1, p2, s.c_str());
}
void Lua::jfl_S_lua_rawset(jfl::Int p1, jfl::Int p2){
	lua_rawset((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_rawseti(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	lua_rawseti((lua_State*)p1, p2, p3);
}
void Lua::jfl_S_lua_rawsetp(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	lua_rawsetp((lua_State*)p1, p2, (const void*)p3);
}
jfl::Int Lua::jfl_S_lua_setmetatable(jfl::Int p1, jfl::Int p2){
	return lua_setmetatable((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_setuservalue(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
void Lua::jfl_S_lua_callk(jfl::Int p1, jfl::Int p2, jfl::Int p3, jfl::Int p4, jfl::Function p5){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_precall(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_getctx(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_pcallk(jfl::Int p1, jfl::Int p2, jfl::Int p3, jfl::Int p4, jfl::Int p5, jfl::Function p6){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_load(jfl::Int p1, jfl::Function p2, jfl::Int p3, jfl::String p4, jfl::String p5){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_dump(jfl::Int p1, jfl::Function p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_yieldk(jfl::Int p1, jfl::Int p2, jfl::Int p3, jfl::Function p4){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_resume(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	return lua_resume((lua_State*)p1, (lua_State*)p2, p3);
}
jfl::Int Lua::jfl_S_lua_status(jfl::Int p1){
	return lua_status((lua_State*)p1);
}
jfl::Int Lua::jfl_S_lua_gc(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	return lua_gc((lua_State*)p1, p2, p3);
}
jfl::Int Lua::jfl_S_lua_error(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_next(jfl::Int p1, jfl::Int p2){
	return lua_next((lua_State*)p1, p2);
}
void Lua::jfl_S_lua_concat(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
void Lua::jfl_S_lua_len(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Function Lua::jfl_S_lua_getallocf(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
void Lua::jfl_S_lua_setallocf(jfl::Int p1, jfl::Function p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_getstack(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_getinfo(jfl::Int p1, jfl::String p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_lua_getlocal(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_lua_setlocal(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_lua_getupvalue(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	const char* s = lua_getupvalue((lua_State*)p1, p2, p3);
	if ( !s ){
		return jfl::String::Make("");
	}
	return jfl::String::Make(s);
}
jfl::String Lua::jfl_S_lua_setupvalue(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	const char* s = lua_setupvalue((lua_State*)p1, p2, p3);
	if ( !s ){
		return jfl::String::Make("");
	}
	return jfl::String::Make(s);
}
jfl::Int Lua::jfl_S_lua_upvalueid(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	return (Int)lua_upvalueid((lua_State*)p1, p2, p3);
}
void Lua::jfl_S_lua_upvaluejoin(jfl::Int p1, jfl::Int p2, jfl::Int p3, jfl::Int p4, jfl::Int p5){
	lua_upvaluejoin((lua_State*)p1, p2, p3, p4, p5);
}
jfl::Int Lua::jfl_S_lua_sethook(jfl::Int p1, jfl::Function p2, jfl::Int p3, jfl::Int p4){
	jfl_notimpl;
}
jfl::Function Lua::jfl_S_lua_gethook(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_gethookmask(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_lua_gethookcount(jfl::Int p1){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_checkversion_(jfl::Int p1, jfl::Number p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_getmetafield(jfl::Int p1, jfl::Int p2, jfl::String p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_callmeta(jfl::Int p1, jfl::Int p2, jfl::String p3){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_luaL_tolstring(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	size_t n = 0;
	const char* s = luaL_tolstring((lua_State*)p1, p2, &n);
	return jfl::String::Make({s, n});
}
jfl::Int Lua::jfl_S_luaL_argerror(jfl::Int p1, jfl::Int p2, jfl::String p3){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_luaL_checklstring(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_luaL_optlstring(jfl::Int p1, jfl::Int p2, jfl::String p3, jfl::Int p4){
	jfl_notimpl;
}
jfl::Number Lua::jfl_S_luaL_checknumber(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Number Lua::jfl_S_luaL_optnumber(jfl::Int p1, jfl::Int p2, jfl::Number p3){
	jfl_notimpl;
}
jfl::Any Lua::jfl_S_luaL_checkinteger(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Any Lua::jfl_S_luaL_optinteger(jfl::Int p1, jfl::Int p2, jfl::Any p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_checkunsigned(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_optunsigned(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_checkstack(jfl::Int p1, jfl::Int p2, jfl::String p3){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_checktype(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_checkany(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_newmetatable(jfl::Int p1, jfl::String p2){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_setmetatable(jfl::Int p1, jfl::String p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_testudata(jfl::Int p1, jfl::Int p2, jfl::String p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_checkudata(jfl::Int p1, jfl::Int p2, jfl::String p3){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_where(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_error(jfl::Int p1, jfl::String p2, jfl::Any p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_checkoption(jfl::Int p1, jfl::Int p2, jfl::String p3, jfl::Int p4){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_fileresult(jfl::Int p1, jfl::Int p2, jfl::String p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_execresult(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_ref(jfl::Int p1, jfl::Int p2){
	return luaL_ref((lua_State*)p1, p2);
}
void Lua::jfl_S_luaL_unref(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	luaL_unref((lua_State*)p1, p2, p3);
}
jfl::Int Lua::jfl_S_luaL_loadfilex(jfl::Int p1, jfl::String p2, jfl::String p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_loadbufferx(jfl::Int p1, jfl::String p2, jfl::Any p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_loadstring(jfl::Int p1, jfl::String p2){
	auto view = p2.to_string_view();
	return luaL_loadnamedbuffer((lua_State*)p1, view.data(), (int)view.size(), "[string]");
}

namespace{
extern "C" void* lua_Allocf(void* ud, void* ptr, size_t osize, size_t nsize){
	if ( nsize == 0 ){
		jfl::unmanaged_free(ptr, osize);
		return nullptr;
	}else if ( ptr != nullptr ){
		void* newptr = jfl::unmanaged_allocate(nsize);
		size_t overlap = std::min(nsize, osize);
		if ( overlap > 0 ){
			std::memcpy(newptr, ptr, overlap);
		}
		jfl::unmanaged_free(ptr, osize);
		return newptr;
	}else{
		return jfl::unmanaged_allocate(nsize);
	}
}
}

jfl::Int Lua::jfl_S_luaL_newstate(){
	return (Int)lua_newstate(lua_Allocf, nullptr);
}
jfl::Int Lua::jfl_S_luaL_len(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_luaL_gsub(jfl::Int p1, jfl::String p2, jfl::String p3, jfl::String p4){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_setfuncs(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaL_getsubtable(jfl::Int p1, jfl::Int p2, jfl::String p3){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_traceback(jfl::Int p1, jfl::Int p2, jfl::String p3, jfl::Int p4){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_requiref(jfl::Int p1, jfl::String p2, jfl::Function p3, jfl::Int p4){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_buffinit(jfl::Int p1, jfl::Int p2){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_luaL_prepbuffsize(jfl::Int p1, jfl::Any p2){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_addlstring(jfl::Int p1, jfl::String p2, jfl::Any p3){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_addstring(jfl::Int p1, jfl::String p2){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_addvalue(jfl::Int p1){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_pushresult(jfl::Int p1){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_pushresultsize(jfl::Int p1, jfl::Any p2){
	jfl_notimpl;
}
jfl::String Lua::jfl_S_luaL_buffinitsize(jfl::Int p1, jfl::Int p2, jfl::Any p3){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_base(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_coroutine(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_table(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_io(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_os(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_string(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_bit32(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_math(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_debug(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_package(jfl::Int p1){
	jfl_notimpl;
}
jfl::Int Lua::jfl_S_luaopen_flash(jfl::Int p1){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_openlibs(jfl::Int p1){
	luaL_openlibs((lua_State*)p1);
}
void Lua::jfl_S_lua_pop(jfl::Int p1, jfl::Int p2){
	lua_pop((lua_State*)p1, p2);
}
jfl::Int Lua::jfl_S_luaL_yield(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	return luaL_yield((lua_State*)p1, p2, p3);
}
jfl::Int Lua::jfl_S_push_flashref(jfl::Int p1){
	jfl_notimpl;
}
void Lua::jfl_S_luaL_pushcfunction(jfl::Int p1, jfl::Int p2){
	luaL_pushcfunction((lua_State*)p1, (void*)p2);
}
jfl::Int Lua::jfl_S_luaL_loadnamedstring(jfl::Int p1, jfl::String p2, jfl::String p3){
	return luaL_loadnamedstring((lua_State*)p1, std::string(p2.to_string_view()).c_str(), std::string(p3.to_string_view()).c_str());
}
jfl::Int Lua::jfl_S_luaL_loadnamedbuffer(jfl::Int p1, jfl::Int p2, jfl::Int p3, jfl::String p4){
	std::string s(p4.to_string_view());
	return luaL_loadnamedbuffer((lua_State*)p1, (const void*)p2, p3, s.c_str());
}
void Lua::jfl_S_luaL_call(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	luaL_call((lua_State*)p1, p2, p3);
}
jfl::Int Lua::jfl_S_luaL_pcall(jfl::Int p1, jfl::Int p2, jfl::Int p3, jfl::Int p4){
	return luaL_pcall((lua_State*)p1, p2, p3, p4);
}
jfl::Int Lua::jfl_S_luaL_tocfunction(jfl::Int p1, jfl::Int p2){
	return (Int)luaL_tocfunction((lua_State*)p1, p2);
}
jfl::Int Lua::jfl_S_lua_gettracebackfunc(){
	return (Int)lua_gettracebackfunc();
}
void Lua::jfl_S_luaL_makecallwrapclosure(jfl::Int p1){
	luaL_makecallwrapclosure((lua_State*)p1);
}
void Lua::jfl_S_luaL_openlibs_s(jfl::Int p1){
	luaL_openlibs_s((lua_State*)p1);
}
void Lua::jfl_S_luaL_opendebug(jfl::Int p1){
	luaL_opendebug((lua_State*)p1);
}
void Lua::jfl_S_luaL_openlib_byname(jfl::Int p1, jfl::String p2){
	luaL_openlib_byname((lua_State*)p1, std::string(p2.to_string_view()).c_str());
}
jfl::Int Lua::jfl_S_luaL_setupvalue(jfl::Int p1, jfl::Int p2, jfl::Int p3){
	return luaL_setupvalue((lua_State*)p1, p2, p3);
}
void Lua::jfl_S_luaL_pushuserdata(jfl::Int p1, jfl::Int p2){
	luaL_pushuserdata((lua_State*)p1, (void*)p2);
}
jfl::Int Lua::jfl_S_luaL_touserdata(jfl::Int p1, jfl::Int p2){
	return (Int)luaL_touserdata((lua_State*)p1, p2);
}
jfl::Int Lua::jfl_S_luaL_debug_funcname(jfl::Int p1){
	return luaL_debug_funcname((lua_State*)p1);
}
jfl::Int Lua::jfl_S_luaL_debug_upvalues(jfl::Int p1){
	return luaL_debug_upvalues((lua_State*)p1);
}
jfl::Int Lua::jfl_S_luaL_upvalueindex(jfl::Int p1, jfl::Int p2){
	return luaL_upvalueindex((lua_State*)p1, p2);
}
jfl::Int Lua::jfl_S_luaL_chunk_dump(jfl::Int p1){
	Int result = (Int)luaL_chunk_dump((lua_State*)p1);

	return result;
}
jfl::Int Lua::jfl_S_luaL_chunk_get_length(jfl::Int p1){
	if ( !p1 ){
		return 0;
	}
	return luaL_chunk_get_length((LuaChunk*)p1);
}
jfl::Int Lua::jfl_S_luaL_chunk_get_data(jfl::Int p1){
	if ( !p1 ){
		return 0;
	}
	return (Int)luaL_chunk_get_data((LuaChunk*)p1);
}
void Lua::jfl_S_luaL_chunk_free(jfl::Int p1){
	luaL_chunk_free((LuaChunk*)p1);
}
}
#endif
