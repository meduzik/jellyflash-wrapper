#if __has_include(<fl/crossbridge/lua/swig/SWIG_AS3GetCCallPtr.h>)
#include <jfl/common_impl.h>
#include <fl/crossbridge/lua/swig/SWIG_AS3GetCCallPtr.h>
#include "FunctionWrappers.h"
namespace fl::crossbridge::lua::swig{
jfl::Int SWIG_AS3GetCCallPtr(jfl::Function func){
	return (jfl::Int)jfl::GetFunctionPtr(func);
}
}
#endif
