#include "FunctionWrappers.h"

namespace jfl {

static const size_t MaxRegistrations = 256;
jfl::Function functions[MaxRegistrations];
static size_t NextRegistration = 0;

template<size_t Idx>
int call_wrapper(lua_State* L) {
	return any_to_int(functions[Idx].invoke(jfl::params({ (Int)L })));
}
using ConstFunc = const lua_CFunction;
extern ConstFunc DispatchTable[MaxRegistrations] = {
#include "dispatch_table.inc"
};

struct FuncHasher {
	size_t operator()(jfl::Function func) const {
		if (!func.object) {
			return 0;
		}
		return ((size_t)func.object) + ((size_t)func.offset);
	}
};

struct FuncEq {
	bool operator()(jfl::Function lhs, jfl::Function rhs) const {
		return jfl::func_eq(lhs, rhs);
	}
};

static std::unordered_map<jfl::Function, Int, FuncHasher, FuncEq> RegisteredFunctions;
static std::vector<Int> FreeIDs;

lua_CFunction RegisterFunction(jfl::Function func) {
	auto it = RegisteredFunctions.find(func);
	if (it == RegisteredFunctions.end()) {
		Int id;
		if (!FreeIDs.empty()) {
			id = FreeIDs.back();
			FreeIDs.pop_back();
		}else{
			id = NextRegistration;
			if (id >= MaxRegistrations) {
				abort();
			}
			NextRegistration++;
		}
		functions[id] = func;
		RegisteredFunctions.insert({ func, id });
		return DispatchTable[id];
	} else {
		return DispatchTable[it->second];
	}
}

void UnregisterFunction(jfl::Function func){
	auto it = RegisteredFunctions.find(func);
	if (it != RegisteredFunctions.end()) {
		functions[it->second] = nullptr;
		FreeIDs.push_back(it->second);
		RegisteredFunctions.erase(it);
	}
}

lua_CFunction GetFunctionPtr(jfl::Function func){
	auto it = RegisteredFunctions.find(func);
	if (it != RegisteredFunctions.end()) {
		return DispatchTable[it->second];
	}
	return nullptr;
}

}
