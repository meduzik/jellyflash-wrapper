import os
import build.cpp as cpp
import build.context as context
import build.utils as utils
import libs
import logging
import asyncio
import sys
from optparse import OptionParser


parser = OptionParser()
parser.add_option("--unity", action="store_true", dest="unity", default=False)
parser.add_option("--debug", action="store_true", dest="debug", default=False)
parser.add_option("--optimize", action="store_true", dest="optimize", default=False)
parser.add_option("--out", dest="outdir", metavar="DIR", default=None)
parser.add_option("--in", dest="indir", metavar="DIR", default=None)

options, args = parser.parse_args()

if len(args) != 1:
	raise RuntimeError("exactly one positional argument (target) is required")

sys.setrecursionlimit(10000)


logging.basicConfig(level=logging.DEBUG)


WORKSPACE_DIR = os.path.dirname(os.path.abspath(__file__))

unity = options.unity
debug = options.debug
asmjs = False
sdk = "em"
target = args[0]

env = cpp.Environment(None, None)

if debug:
	env.debug = cpp.DebugInfo.Enabled
else:
	env.debug = cpp.DebugInfo.Disabled

if options.optimize:
	env.opt = cpp.Opt.PreferSize
else:
	env.opt = cpp.Opt.Disabled

env.pch = None

if sdk == "em":
	import build.em as compiler
else:
	import build.ndk as compiler

path = options.outdir
if path is None:
	raise RuntimeError("no output dir")

ctx = context.Context(path)

ctx.config["asmjs"] = asmjs
ctx.config["in"] = options.indir

ctx.provide("lua", compiler.StaticLib(libs.lua, env, unity=False))
ctx.provide("zlib", compiler.StaticLib(libs.zlib, env, unity=False))
ctx.provide("libpng", compiler.StaticLib(libs.libpng, env, unity=False))
ctx.provide("libjpeg", compiler.StaticLib(libs.libjpeg, env, unity=False))
ctx.provide("libwebp", compiler.StaticLib(libs.libwebp, env, unity=False))
ctx.provide("freetype", compiler.StaticLib(libs.freetype, env, unity=False))
ctx.provide("fl_native", compiler.StaticLib(libs.fl_native, env, unity=unity))
ctx.provide("fl_lua", compiler.StaticLib(libs.fl_lua, env, unity=unity))
ctx.provide("fl_project", compiler.StaticLib(libs.fl_project, env, unity=unity))
ctx.provide("native", compiler.StaticLib(libs.native, env, unity=unity))
ctx.provide("app", compiler.Application(libs.app, env, unity=unity))
ctx.provide("em", compiler.StaticLib(libs.em, env, unity=unity))
ctx.provide("em_test", compiler.Application(libs.em_test, env, unity=unity))

action = target

ctx.start_build(concurrency=12)
app = ctx.require(action)
loop = asyncio.ProactorEventLoop()
loop.run_until_complete(app.build())
ctx.finish_build()

