import build.cpp as cpp
import build.context as context
import build.utils as utils
from build.em import EMManifest
from build.ndk import NdkManifest
import os


def lua(module: cpp.Module, ctx: context.Context):
	module.set_base_path("thirdparty/lua")
	module.add_public_include("src")

	env = module.get_env().inherit()
	env.std = cpp.Std.C11

	cppenv = module.get_env().inherit()
	cppenv.std = cpp.Std.Cpp17

	module.add_group(cpp.SourceGroup(
		env,
		"src",
		cpp.c_files(exclude=[
			"lua.c",
			"luac.c"
		])
	))
	module.add_group(cpp.SourceGroup(
		cppenv,
		"src",
		cpp.cpp_files()
	))


def zlib(module: cpp.Module, ctx: context.Context):
	if EMManifest in module:
		module.set_base_path("thirdparty/zlib-em")
	else:
		module.set_base_path("thirdparty/zlib")
	module.add_public_include(".")

	env = module.get_env().inherit()
	env.std = cpp.Std.C11

	module.add_group(cpp.SourceGroup(
		env,
		".",
		cpp.c_files(shallow=True)
	))


def freetype(module: cpp.Module, ctx: context.Context):
	module.set_base_path("thirdparty/freetype-2.9.1")
	module.add_public_include("include")

	env = module.get_env().inherit()
	env.std = cpp.Std.C11
	env.add_include("include")
	env.defines["FT2_BUILD_LIBRARY"] = "1"

	module.add_dependency(cpp.DependencyType.Private, ctx.require('zlib'))

	module.add_group(cpp.SourceGroup(
		env,
		"src",
		utils.files_list([
			"base/ftbase.c",
			"base/ftbbox.c",
			"base/ftdebug.c",
			"base/ftglyph.c",
			"base/ftinit.c",
			"base/ftsystem.c",
			"truetype/truetype.c",
			"sfnt/sfnt.c",
			"psnames/psnames.c",
			"smooth/smooth.c",
			"base/ftbitmap.c",
			"autofit/autofit.c",
			"gzip/ftgzip.c",
		])
	))


def libpng(module: cpp.Module, ctx: context.Context):
	module.set_base_path("thirdparty/libpng")
	module.add_public_include(".")

	module.add_dependency(cpp.DependencyType.Private, ctx.require('zlib'))

	env = module.get_env().inherit()
	env.std = cpp.Std.C11
	env.defines["PNG_ARM_NEON_OPT"] = "0"	
	
	module.add_group(cpp.SourceGroup(
		env,
		".",
		cpp.c_files(shallow=True, exclude=["example.c", "pngtest.c"])
	))
	
	
def libwebp(module: cpp.Module, ctx: context.Context):
	module.set_base_path("thirdparty/libwebp")
	module.add_public_include(".")

	env = module.get_env().inherit()
	env.std = cpp.Std.C11
	env.add_include(".")
	
	module.add_group(cpp.SourceGroup(
		env,
		"src",
		cpp.c_files(shallow=False, exclude=[])
	))


def libjpeg(module: cpp.Module, ctx: context.Context):
	module.set_base_path("thirdparty/libjpeg")
	module.add_public_include(".")

	module.add_dependency(cpp.DependencyType.Private, ctx.require('zlib'))

	env = module.get_env().inherit()
	env.std = cpp.Std.C11

	module.add_group(cpp.SourceGroup(
		env,
		".",
		cpp.c_files(
			shallow=True,
			exclude=[
				"jmemdos.c", "jmemmac.c", "jmemname.c",
				"cjpeg.c", "ckconfig.c", "djpeg.c", "rdjpgcom.c", "wrjpgcom.c",
				"jpegtran.c", "jmemmac.c", "jmemansi.c"
			]
		)
	))


def fl_native(module: cpp.Module, ctx: context.Context):
	module.set_base_path(".")
	module.add_public_include("include")
	module.add_public_include(os.path.join(ctx.config["in"], "builtin/include"))

	module.add_dependency(cpp.DependencyType.Private, ctx.require('zlib'))
	module.add_dependency(cpp.DependencyType.Private, ctx.require('libjpeg'))
	module.add_dependency(cpp.DependencyType.Private, ctx.require('libpng'))
	module.add_dependency(cpp.DependencyType.Private, ctx.require('libwebp'))
	module.add_dependency(cpp.DependencyType.Private, ctx.require('lua'))

	env = module.get_env().inherit()
	env.std = cpp.Std.Cpp17
	env.warnings["unused-variable"] = False
	env.add_include("include")
	env.add_include(os.path.join(ctx.config["in"], "builtin/include"))
	env.set_pch("source/fl_native/pch.h")

	module.add_group(cpp.SourceGroup(
		env,
		os.path.join(ctx.config["in"], "builtin/source"),
		cpp.cpp_files()
	))

	module.add_group(cpp.SourceGroup(
		env,
		"native",
		cpp.cpp_files()
	))


def fl_project(module: cpp.Module, ctx: context.Context):
	module.set_base_path(".")
	module.add_public_include("include")
	module.add_public_include(os.path.join(ctx.config["in"], "project/include"))
	module.add_dependency(cpp.DependencyType.Transitive, ctx.require('native'))
	module.add_dependency(cpp.DependencyType.Transitive, ctx.require('fl_lua'))

	env = module.get_env().inherit()
	env.std = cpp.Std.Cpp17
	env.warnings["unused-variable"] = False
	env.add_include("include")
	env.add_include(os.path.join(ctx.config["in"], "project/include"))
	env.set_pch("source/fl_project/pch.h")

	module.add_group(cpp.SourceGroup(
		env,
		os.path.join(ctx.config["in"], "project/source"),
		cpp.cpp_files()
	))


def fl_lua(module: cpp.Module, ctx: context.Context):
	module.set_base_path(".")
	module.add_public_include("include")
	module.add_public_include(os.path.join(ctx.config["in"], "lua/include"))

	env = module.get_env().inherit()
	env.std = cpp.Std.Cpp17
	env.warnings["unused-variable"] = False
	env.add_include("include")
	env.add_include(os.path.join(ctx.config["in"], "lua/include"))
	env.set_pch("source/lua-wrapper/pch.h")

	module.add_dependency(cpp.DependencyType.Private, ctx.require('lua'))
	module.add_dependency(cpp.DependencyType.Private, ctx.require('fl_native'))

	module.add_group(cpp.SourceGroup(
		env,
		os.path.join(ctx.config["in"], "lua/source"),
		cpp.cpp_files()
	))
	module.add_group(cpp.SourceGroup(
		env,
		"source/lua-wrapper",
		cpp.cpp_files()
	))


def em(module: cpp.Module, ctx: context.Context):
	module.set_base_path(".")
	module.add_public_include("emscripten/include")

	env = module.get_env().inherit()
	env.std = cpp.Std.Cpp17
	env.add_include("emscripten/include")

	module[EMManifest].add_pre_js("emscripten/js/support.js")
	module[EMManifest].add_pre_js("emscripten/js/URLStream.js")
	module[EMManifest].add_js_library("emscripten/js/URLStream-lib.js")
	module[EMManifest].add_pre_js("emscripten/js/SharedObject.js")
	module[EMManifest].add_js_library("emscripten/js/SharedObject-lib.js")
	module[EMManifest].add_pre_js("emscripten/js/ExternalInterface.js")
	module[EMManifest].add_js_library("emscripten/js/ExternalInterface-lib.js")
	module[EMManifest].add_pre_js("emscripten/js/StageText.js")
	module[EMManifest].add_js_library("emscripten/js/StageText-lib.js")
	module[EMManifest].add_pre_js("emscripten/js/WebAudio.js")
	module[EMManifest].add_js_library("emscripten/js/WebAudio-lib.js")
	module[EMManifest].add_pre_js("emscripten/js/WebUtils.js")
	module[EMManifest].add_js_library("emscripten/js/WebUtils-lib.js")
	module[EMManifest].add_pre_js("emscripten/js/Canvas.js")
	module[EMManifest].add_js_library("emscripten/js/Canvas-lib.js")
	module[EMManifest].add_pre_js("emscripten/js/WebGL.js")
	module[EMManifest].add_js_library("emscripten/js/WebGL-lib.js")

	module.add_group(cpp.SourceGroup(
		env,
		"emscripten/source",
		cpp.cpp_files()
	))
	env.set_pch("emscripten/source/pch.h")


def em_test(module: cpp.Module, ctx: context.Context):
	module.set_base_path(".")

	env = module.get_env().inherit()
	env.std = cpp.Std.Cpp17
	module.add_dependency(cpp.DependencyType.Transitive, ctx.require('em'))

	env_pch = env.inherit()
	module.add_group(cpp.SourceGroup(
		env_pch,
		"emscripten/tests",
		cpp.cpp_files(exclude=["main.cpp"])
	))
	env_pch.set_pch("emscripten/tests/pch.h")

	module[EMManifest].add_linker_opt("DEMANGLE_SUPPORT=1")

	module.add_group(cpp.SourceGroup(
		env,
		"emscripten/tests",
		cpp.cpp_files(include=["main.cpp"])
	))


def native(module: cpp.Module, ctx: context.Context):
	module.set_base_path(".")
	module.add_public_include("include")
	module.add_dependency(cpp.DependencyType.Transitive, ctx.require('fl_native'))
	module.add_dependency(cpp.DependencyType.Private, ctx.require('freetype'))

	if EMManifest in module:
		module.add_dependency(cpp.DependencyType.Transitive, ctx.require('em'))

	env = module.get_env().inherit()
	env.std = cpp.Std.Cpp17
	env.add_include("include")

	excludes = ["conditional"]

	conditional_groups = []

	if EMManifest in module:
		conditional_groups.append("emscripten")

	module.add_group(cpp.SourceGroup(
		env,
		"source/flashnative",
		cpp.cpp_files(exclude=excludes)
	))

	for dir in conditional_groups:
		module.add_group(cpp.SourceGroup(
			env,
			"source/flashnative/conditional/" + dir,
			cpp.cpp_files()
		))
	env.set_pch("source/flashnative/pch.h")


def app(module: cpp.Module, ctx: context.Context):
	module.set_base_path(".")
	module.add_public_include("include")
	module.add_dependency(cpp.DependencyType.Private, ctx.require('native'))
	module.add_dependency(cpp.DependencyType.Private, ctx.require('fl_project'))

	if EMManifest in module:
		debug = module.get_env().resolve("debug")

		module[EMManifest].add_linker_opt("NO_FILESYSTEM=1")

		if debug == cpp.DebugInfo.Enabled:
			module[EMManifest].add_linker_opt("ASSERTIONS=1")

			#opt = module.get_env().resolve("opt")
			#if opt == cpp.Opt.Disabled:
			#	module[EMManifest].add_linker_opt("DEMANGLE_SUPPORT=1")
			#	module[EMManifest].add_linker_opt("SAFE_HEAP=1")
			#	module[EMManifest].add_linker_opt("ALIASING_FUNCTION_POINTERS=0")

		if ctx.config["asmjs"]:
			module[EMManifest].add_linker_opt("WASM=0")
			module[EMManifest].add_linker_opt("TOTAL_MEMORY=256MB")
			module[EMManifest].add_linker_opt("NO_EXIT_RUNTIME=1")
			module[EMManifest].add_linker_opt("EVAL_CTORS=0")
		else:
			module[EMManifest].add_linker_opt("TOTAL_MEMORY=150MB")
			module[EMManifest].add_linker_opt("ALLOW_MEMORY_GROWTH=1")
			module[EMManifest].add_linker_opt("MALLOC=emmalloc")

		module[EMManifest].add_linker_opt("MAX_WEBGL_VERSION=1")
		module[EMManifest].add_linker_opt("GL_PREINITIALIZED_CONTEXT=1")

		module[EMManifest].enable_lto = True
		module[EMManifest].enable_closure = True

	env = module.get_env().inherit()
	env.std = cpp.Std.Cpp17
	env.add_include("include")
	env.set_pch("source/flashapp/pch.h")

	module.add_group(cpp.SourceGroup(
		env,
		"source/flashapp",
		cpp.cpp_files()
	))
