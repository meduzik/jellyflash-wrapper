import subprocess
import functools
import sys
import json
import re
import os
import hashlib
import asyncio


def sha1(s):
	return hashlib.sha1(s.encode('utf-8')).hexdigest()


simple_arg_re = re.compile("[a-zA-Z0-9_+\-/.]+")


class FileRef:
	def __init__(self, base_path, path):
		self.path = os.path.join(base_path, path)


def operation(s):
	def foo(func):
		@functools.wraps(func)
		def wrapper(*args, **kwargs):
			print(s, end="... ")
			try:
				res = func(*args, **kwargs)
				print("[DONE]")
				return res
			except:
				print("[FAILED!]")
				raise
		return wrapper
	return foo


def escape(s):
	if not simple_arg_re.match(s):
		return json.dumps(s)
	else:
		return s


def run_command(cmd, *args):
	cmdline = (cmd,) + args
	print("$ " + " ".join(map(escape, cmdline)))
	result = subprocess.run(
		cmdline,
		stdout=sys.stdout,
		stderr=sys.stderr,
		check=True
	)
	return result.returncode


async def run_command_async(idx, cmd, *args):
	cmdline = (cmd,) + args

	print("[" + str(idx) + "] " + " ".join(map(escape, cmdline)))
	process = await asyncio.create_subprocess_exec(
		*cmdline,
		stdout=subprocess.PIPE,
		stderr=subprocess.PIPE
	)
	stdout, stderr = await process.communicate()

	if stdout:
		print("[" + str(idx) + "]:\n" + stdout.decode('utf-8'))

	if stderr:
		print("[" + str(idx) + "]:\n" + stderr.decode('utf-8'), file=sys.stderr)

	if process.returncode != 0:
		raise RuntimeError(f"Process exit with code {process.returncode}")


def files_list(names):
	names_list = list(names)

	def fn(base_path):
		for name in names_list:
			yield FileRef(base_path, name)

	return fn


def files_by_ext(*extensions: str, shallow=False, exclude=None, include=None):
	ext_set = frozenset(extensions)

	def fn(base_path):
		for root, dirs, files in os.walk(base_path, topdown=True):
			for name in files:
				if (exclude is not None) and (name in exclude):
					continue
				if (include is not None) and name not in include:
					continue
				if os.path.splitext(name)[1] in ext_set:
					yield FileRef(root, name)
			if shallow:
				dirs.clear()
			else:
				if exclude:
					to_remove = []
					for name in dirs:
						if name in exclude:
							to_remove.append(name)
					for name in to_remove:
						dirs.remove(name)

	return fn


def command_to_string(*cmd):
	return json.dumps(list(cmd))
