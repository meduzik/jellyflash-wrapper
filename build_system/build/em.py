import build.cpp as cpp
from build.utils import *
from build.context import ensure_dir
import logging
import asyncio
import shlex


log = logging.getLogger("em")


initialized = False


def initialize():
	global initialized
	if initialized:
		return
	setup_env()
	initialized = True


@operation("Setting up the environment")
def setup_env():
	with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../env.txt"), "r") as fp:
		for line in fp.readlines():
			line = line.strip()
			if len(line) <= 0:
				continue
			key, value = line.split("=", 1)
			os.environ[key] = value


def run_c(*args):
	if not initialized:
		initialize()
	run_command("emcc.bat", *args)


async def run_cpp_async(idx, *args):
	if not initialized:
		initialize()
	return await run_command_async(idx, "em++.bat", *args)


WASM_BACKEND = False


EM_SDK = "D:/Soft/emsdk/"

LLVM_ROOT = f"{EM_SDK}upstream/bin/"

EM_CC = f"{LLVM_ROOT}clang++.exe"
EM_AR = f"{LLVM_ROOT}llvm-ar.exe"
EM_LINK = f"emcc.bat"
LLVM_LINK = f"{LLVM_ROOT}llvm-link.exe"

em_opts = [
	"-cc1",
	"-triple", "wasm32-unknown-emscripten",
	"-emit-llvm-bc",
	"-mrelax-all",
	"-disable-free",
	"-mrelocation-model", "static",
	"-mthread-model", "posix",
	"-mframe-pointer=none",
	"-fno-rounding-math",
	"-mconstructor-aliases",
	"-target-cpu", "generic",
	"-fvisibility", "hidden",
	"-debugger-tuning=gdb",
	"-resource-dir", "D:\\Soft\\emsdk\\upstream\\lib\\clang\\12.0.0",
	"-D", "__EMSCRIPTEN_major__=1", "-D", "__EMSCRIPTEN_minor__=39", "-D", "__EMSCRIPTEN_tiny__=5",
	"-D", "_LIBCPP_ABI_VERSION=2",
	"-D", "unix", "-D", "__unix", "-D", "__unix__", "-D", "EMSCRIPTEN",
	"-internal-isystem", "/include/wasm32-emscripten/c++/v1",
	"-internal-isystem", "/include/c++/v1",
	"-internal-isystem", "D:\\Soft\\emsdk\\upstream\\lib\\clang\\12.0.0\\include",
	"-internal-isystem", "/include/wasm32-emscripten",
	"-internal-isystem", "/include",
	"-Werror=implicit-function-declaration",
	"-fdeprecated-macro",
	"-ferror-limit", "19", "-fgnuc-version=4.2.1",
	"-fobjc-runtime=gnustep",
	"-fno-common",
	"-fcolor-diagnostics",
	"-nostdsysteminc",
	"-isystemD:\\Soft\\emsdk\\upstream\\emscripten\\system\\include\\libcxx",
	"-isystemD:\\Soft\\emsdk\\upstream\\emscripten\\system\\lib\\libcxxabi\\include",
	"-isystemD:\\Soft\\emsdk\\upstream\\emscripten\\system\\include\\compat",
	"-isystemD:\\Soft\\emsdk\\upstream\\emscripten\\system\\include",
	"-isystemD:\\Soft\\emsdk\\upstream\\emscripten\\system\\include\\libc",
	"-isystemD:\\Soft\\emsdk\\upstream\\emscripten\\system\\lib\\libc\\musl\\arch\\emscripten",
	"-isystemD:\\Soft\\emsdk\\upstream\\emscripten\\system\\local\\include",
	"-isystemC:\\Users\\meduzik\\.emscripten_cache\\wasm-obj\\include",
	"-isystemD:\\Soft\\emsdk\\upstream\\emscripten\\system\\include\\SDL",
	"-mllvm", "-combiner-global-alias-analysis=false",
	"-mllvm", "-enable-emscripten-sjlj",
	"-mllvm", "-disable-lsr",
]


async def run_clang(idx, *args):
	command = [*em_opts, *args]
	return await run_command_async(idx, EM_CC, *command)


spaces_re = re.compile('\\s+')
backslash_re = re.compile('\\\\[\n\r]')


def read_deps(path):
	with open(path, "r") as fp:
		contents = fp.read()
	no_slashes = backslash_re.sub(' ', contents)
	only_deps = no_slashes[no_slashes.index(':') + 1:]
	return [x for x in spaces_re.split(only_deps) if len(x) > 0]


def StaticLib(*args, **kwargs):
	return Builder("staticlib", *args, **kwargs)


def Application(*args, **kwargs):
	return Builder("app", *args, **kwargs)


class EMManifest:
	def __init__(self, module: cpp.Module):
		self.module = module
		self.own_pre_js = []
		self.own_post_js = []
		self.own_js_library = []
		self.own_linker_opts = []
		self.own_artifacts = []
		self.enable_lto = False
		self.enable_closure = False

		self.pre_js = []
		self.post_js = []
		self.js_library = []
		self.linker_opts = []
		self.artifacts = []

		self.merges = set()

	def add_pre_js(self, path):
		self.own_pre_js.append(self.module.get_env().resolve_path(path))

	def add_post_js(self, path):
		self.own_post_js.append(self.module.get_env().resolve_path(path))

	def add_js_library(self, path):
		self.own_js_library.append(self.module.get_env().resolve_path(path))

	def add_linker_opt(self, opt):
		self.own_linker_opts.append(opt)

	def add_artifact(self, path):
		self.own_artifacts.append(path)

	def merge_from(self, other: 'EMManifest'):
		if other in self.merges:
			return
		self.merges.add(other)
		for transitive in sorted(other.merges, key=lambda manifest: manifest.module.name):
			self.merge_from(transitive)
		self.pre_js.extend(other.own_pre_js)
		self.post_js.extend(other.own_post_js)
		self.js_library.extend(other.own_js_library)
		self.linker_opts.extend(other.own_linker_opts)
		self.artifacts.extend(other.own_artifacts)


class Builder:
	def __init__(self, target, builder, env: cpp.Environment, unity=False):
		self.builder = builder
		self.env = env
		self.module = None
		self.assembly = None
		self.context = None
		self.target = target
		self.manifest: EMManifest = None
		self.pch_objects = dict()
		self.unity = unity

	async def prepare(self, assembly):
		self.module = cpp.Module(assembly.name, self.env)
		self.manifest = EMManifest(self.module)
		self.module.put_extra(self.manifest)
		self.builder(self.module, assembly.context)
		self.assembly = assembly
		self.context = assembly.context
		return self.module

	def compute_manifest(self):
		self.manifest.merge_from(self.manifest)
		for (type, assembly) in self.module.deps:
			their_manifest = assembly.get_module()[EMManifest]
			self.manifest.merge_from(their_manifest)

	async def build(self):
		deps = []
		for (type, assembly) in self.module.deps:
			deps.append(assembly.build())

		await asyncio.gather(*deps)

		self.module.compute_import()
		self.compute_manifest()

		print("start building " + self.module.name)

		object_files = []

		groups = []
		for group in self.module.groups:
			async def process_group(group):
				base_group_path = sha1(group.base_path)[0:16]

				env: cpp.Environment = group.env
				group_path = env.path

				std = env.resolve('std')

				group_includes = self.module.import_includes[:]
				cpp.merge_includes(self.module, group_includes, env.resolve('includes'))

				group_defines = dict()
				cpp.merge_defines(self.module, group_defines, self.module.import_defines)
				# TODO:: cpp.merge_defines(self.module, group_defines, env.resolve('defines'))

				command_basic_opts = []
				# warning level
				command_basic_opts.append("-Wall")

				for k, v in sorted(env.resolve('warnings').items(), key=lambda p: p[0]):
					if v is False:
						command_basic_opts.append("-Wno-" + k)
					else:
						command_basic_opts.append("-W" + k)

				# c standard
				if std == cpp.Std.C11:
					command_basic_opts.append("-std=c11")
					command_basic_opts.append("-x")
					command_basic_opts.append("c")
					run_as_c = True
				elif std == cpp.Std.Cpp17:
					command_basic_opts.append("-std=c++17")
					command_basic_opts.append("-x")
					command_basic_opts.append("c++")
					run_as_c = False
				else:
					raise RuntimeError(f"unsupported language standard {std}")

				defines = env.resolve('defines')
				for k, v in defines.items():
					if v is not None:
						command_basic_opts.append("-D" + k + "=" + v)
					else:
						command_basic_opts.append("-D" + k)

				# debug info
				debug = env.resolve('debug')
				if debug == cpp.DebugInfo.Disabled:
					# do nothing, debug info is not enabled by default
					pass
				elif debug == cpp.DebugInfo.Enabled:
					# command_basic_opts.append("-debug-info-kind=limited")
					# command_basic_opts.append("-dwarf-version=4")
					pass

				# optimizations
				opt = env.resolve('opt')
				if opt == cpp.Opt.Disabled:
					command_basic_opts.append("-O0")
				elif opt == cpp.Opt.Fast:
					command_basic_opts.append("-O1")
				elif opt == cpp.Opt.Full:
					command_basic_opts.append("-O2")
				elif opt == cpp.Opt.Aggressive:
					command_basic_opts.append("-O3")
				elif opt == cpp.Opt.PreferSize:
					command_basic_opts.append("-Os")

				for include in group_includes:
					command_basic_opts.append("-I" + include.path)

				for k, v in sorted(group_defines, key=lambda p: p[0]):
					if v is None:
						command_basic_opts.append("-D" + k)
					else:
						command_basic_opts.append("-D" + k + "=" + v)

				pch_object = None
				pch_ref = env.resolve('pch')
				if pch_ref is not None:
					async def worker(idx):
						nonlocal pch_object
						pch = pch_ref.path
						relpath = os.path.relpath(pch, group.path).replace('..', 'dotdot')
						pch_base = os.path.join(self.assembly.build_path, "obj", base_group_path, relpath)
						dep_file = pch_base + ".d"
						pch_path = pch_base + ".pch"

						command = command_basic_opts[:]

						command.append("-emit-pch")
						command.append("-o")
						command.append(pch_path)

						command.append("-dependency-file")
						command.append(dep_file)

						command.append(pch)
						command.append("-MT")
						command.append(pch)

						command_string = command_to_string(command)
						if not self.assembly.build_cache.check_artifact(pch_path, command_string):
							log.info(f"Building pch file for {pch}...")
							ensure_dir(os.path.dirname(pch_path))
							await run_clang(idx, *command)
							computed_deps = read_deps(dep_file)
							self.assembly.build_cache.commit_artifact(pch_path, command_string, computed_deps)
						pch_object = pch_path

					await self.context.execution_pool.execute(worker)

				sources = []

				async def process_source(source_ref):
					async def worker(idx):
						source = source_ref.path
						source_file = source
						relpath = os.path.relpath(source, group.path).replace('..', 'dotdot')
						object_base = os.path.join(
							self.assembly.build_path,
							"obj",
							base_group_path,
							relpath
						)
						object_file = object_base + ".bc"
						dep_file = object_base + ".d"

						command = command_basic_opts[:]

						# build object file and stop
						# command.append("-c")

						# dependency file
						command.append("-dependency-file")
						command.append(dep_file)

						# object output
						command.append("-o")
						command.append(object_file)

						if pch_object:
							command.append("-include-pch")
							command.append(pch_object)

						# input file
						command.append(source_file)

						command.append("-MT")
						command.append(source_file)

						command_string = command_to_string(command)
						if not self.assembly.build_cache.check_artifact(object_file, command_string):
							log.info(f"[{idx}] Building object file for {source_file}...")
							self.assembly.build_cache.invalidate_artifact(object_file)
							ensure_dir(os.path.dirname(object_file))

							await run_clang(idx, *command)
							command[command.index(object_file)] = object_file + ".ll"
							# await run_clang(idx, *command, '-emit-llvm')

							computed_deps = read_deps(dep_file)
							self.assembly.build_cache.commit_artifact(object_file, command_string, computed_deps)

						object_files.append(object_file)

					await self.context.execution_pool.execute(worker)

				if self.unity:
					if std == cpp.Std.C11:
						ext = "c"
					else:
						ext = "cpp"
					unity_base = os.path.join(self.assembly.build_path, "unity", base_group_path)
					unity_source = os.path.join(unity_base, "unity." + ext)
					source_files = list(map(lambda file: file.path, group.resolve_files(group_path)))
					command_string = command_to_string(["make_unity3", source_files])
					if not self.assembly.build_cache.check_artifact(unity_source, command_string):
						log.info(f"Building unity file...")
						ensure_dir(os.path.dirname(unity_source))
						with open(unity_source, "w") as fp:
							for file in source_files:
								include_path = os.path.normpath(os.path.relpath(file, unity_base)).replace('\\', '/')
								fp.write(f"#include \"{include_path}\"\n")
						self.assembly.build_cache.commit_artifact(unity_source, command_string, [])
					sources.append(process_source(FileRef(unity_base, "unity." + ext)))
				else:
					for source in group.resolve_files(group_path):
						sources.append(process_source(source))
				await asyncio.gather(*sources)

			groups.append(process_group(group))

		await asyncio.gather(*groups)

		# linking
		async def worker(idx):
			nonlocal object_files

			if self.target == "staticlib":
				ext = ".lib.bc"
				base_dir = "lib"
			elif self.target == "app":
				ext = ".html"
				base_dir = "bin"
				object_files.extend(self.manifest.artifacts)
			else:
				raise RuntimeError(f"invalid target {self.target}")

			object_files = list(frozenset(object_files))
			object_files.sort()

			command_file = os.path.join(self.assembly.build_path, base_dir, self.assembly.name + ".inp")
			result_file = os.path.join(self.assembly.build_path, base_dir, self.assembly.name + ext)

			command = []
			other_deps = []

			if self.target == "staticlib":
				command.append("-o")
				command.append(result_file)
				command.append("@" + command_file)
				# command.append("-s")
				# command.append("TOTAL_MEMORY=128MB")
			elif self.target == "app":
				opt = self.module.get_env().resolve('opt')
		
				if opt == cpp.Opt.Disabled:
					command.append("-O0")
				elif opt == cpp.Opt.Fast:
					command.append("-O1")
				elif opt == cpp.Opt.Full:
					command.append("-O2")
				elif opt == cpp.Opt.Aggressive:
					command.append("-O3")
				elif opt == cpp.Opt.PreferSize:
					command.append("-Oz")

				if self.manifest.enable_lto:
					command.append("--lvm-lto=2")
					
				command.append("-v")

				for pre_js in self.manifest.pre_js:
					command.append("--pre-js")
					command.append(pre_js.path)
					other_deps.append(pre_js.path)
				for post_js in self.manifest.post_js:
					command.append("--post_js")
					command.append(post_js.path)
					other_deps.append(post_js.path)
				for lib_js in self.manifest.js_library:
					command.append("--js-library")
					command.append(lib_js.path)
					other_deps.append(lib_js.path)
				for opt in self.manifest.linker_opts:
					command.append("-s")
					command.append(opt)
				debug = self.module.get_env().resolve('debug')
				if debug != cpp.DebugInfo.Disabled:
					# command.append("-g4")
					pass
				command.append("--emit-symbol-map")
				command.append("-o")
				command.append(result_file)
				command.append("@" + command_file)

			command_string = command_to_string(command) + str(object_files)
			if not self.assembly.build_cache.check_artifact(result_file, command_string):
				self.assembly.build_cache.invalidate_artifact(result_file)
				log.info(f"[{idx}] Linking {result_file}...")
				ensure_dir(os.path.dirname(result_file))
				with open(command_file, "w") as fp:
					for file in object_files:
						fp.write(json.dumps(file) + " ")
				if self.target == "staticlib":
					if not initialized:
						initialize()
					await run_command_async(idx, LLVM_LINK, *command)
				elif self.target == "app":
					await run_cpp_async(idx, *command)
				self.assembly.build_cache.commit_artifact(result_file, command_string, object_files + other_deps)
			self.manifest.add_artifact(result_file)

		await self.context.execution_pool.execute(worker)

		self.module.compute_export()

		print("end building " + self.module.name)
