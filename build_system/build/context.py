import os
import logging
import shutil
from build.utils import *
import secrets
import inspect


log = logging.getLogger("context")


def ensure_dir(path):
	os.makedirs(path, exist_ok=True)


class BuildCache:
	def __init__(self, build_path):
		self.build_path = build_path
		self.cache_path = os.path.join(self.build_path, "cache")
		self.data_cache = dict()
		self._load()

	def _load(self):
		pass

	def invalidate(self):
		pass

	def save(self):
		pass

	def load_marker(self, path):
		if path in self.data_cache:
			return self.data_cache[path]
		# if it is not yet in the data_cache, then it must be input file
		try:
			ts = os.path.getmtime(path)
		except IOError:
			ts = None
		marker = ("input", ts)
		self.data_cache[path] = marker
		return marker

	def get_input_marker(self, path):
		self.load_marker(path)
		if self.data_cache[path][0] == "input":
			return self.data_cache[path][1]
		return None

	def get_artifact_marker(self, path):
		self.load_marker(path)
		if self.data_cache[path][0] == "artifact":
			return self.data_cache[path][1]
		return None

	def parse_input_marker(self, data):
		return data['ts']

	def get_artifact_cache_path(self, path):
		hash = sha1(path)
		dir = hash[:2]
		file = hash[2:]
		return os.path.join(self.cache_path, dir, file)

	def parse_artifact_marker(self, data):
		return data['hash']

	def check_artifact(self, target, command):
		try:
			cache_path = self.get_artifact_cache_path(target)
			with open(cache_path, "r") as fp:
				cached_data = json.load(fp)
		except IOError:
			return False
		except json.JSONDecodeError:
			return False
		if cached_data['command'] != command:
			log.debug(f"was built with different command\n\t{command}\nvs\n\t{cached_data['command']}")
			return False

		for dep in cached_data['deps']:
			if dep['type'] == 'input':
				our = self.get_input_marker(dep['path'])
				their = self.parse_input_marker(dep)
				if our != their:
					log.debug(f"failed due to {dep}")
					return False
			elif dep['type'] == 'artifact':
				our = self.get_artifact_marker(dep['path'])
				their = self.parse_artifact_marker(dep)
				if our != their:
					return False
			else:
				raise RuntimeError(f"unknown dependency node {dep['type']}")

		self.data_cache[target] = ('artifact', cached_data['hash'])
		return True

	def invalidate_artifact(self, target):
		try:
			os.unlink(self.get_artifact_cache_path(target))
		except IOError:
			pass
		if target in self.data_cache:
			del self.data_cache[target]

	def commit_artifact(self, target, command, deps):
		deps_data = []
		for dep in deps:
			marker = self.load_marker(dep)
			if marker[0] == "input":
				deps_data.append({"type": "input", "path": dep, "ts": marker[1]})
			elif marker[0] == "artifact":
				deps_data.append({"type": "artifact", "path": dep, "hash": marker[1]})
			else:
				raise RuntimeError(f"invalid dependency type {marker[0]}")

		cache_path = self.get_artifact_cache_path(target)
		os.makedirs(os.path.dirname(cache_path), exist_ok=True)
		hash = secrets.token_hex(16)
		with open(cache_path, "w") as fp:
			json.dump(
				{
					"hash": hash,
					"command": command,
					"deps": deps_data
				},
				fp
			)
		self.data_cache[target] = ('artifact', hash)


class Assembly:
	def __init__(self, name: str, context: 'Context', builder):
		self.name = name
		self.context = context
		self._builder = builder
		self._module = None
		self.build_path = os.path.join(context.build_path, name)
		self.build_cache = BuildCache(self.build_path)
		self._built = False
		self._module_future = None
		self._build_future = None

	def get_module(self):
		if not self._module:
			raise RuntimeError(f"module {self.name} requested but not prepared")
		return self._module

	async def build(self):
		if self._build_future is None:
			self._build_future = asyncio.Future()
			try:
				await self.prepare()
				await self._builder.build()
				self._built = True
				self._build_future.set_result(True)
			except:
				self._build_future.set_exception(sys.exc_info()[1])
		return await self._build_future

	async def _prepare(self):
		try:
			self._module = await self._builder.prepare(self)
			if self._module is None:
				self._module = True
			self._module_future.set_result(True)
		except:
			self._module_future.set_exception(sys.exc_info()[1])

	async def prepare(self):
		if self._module_future is None:
			self._module_future = asyncio.Future()
			await self._prepare()
		await self._module_future


class BuildRun:
	def __init__(self):
		pass


class ExecutionPool:
	def __init__(self, size):
		self.size = size
		self.queue = []
		self.free_ids = list(range(size, 0, -1))

	async def execute(self, fn):
		future = asyncio.Future()

		async def worker(idx):
			try:
				result = await fn(idx)
				future.set_result(result)
			except:
				future.set_exception(sys.exc_info()[1])
			finally:
				await self._release(idx)

		self.queue.append(worker)
		await self._spin()
		return await future

	async def _spin(self):
		if len(self.free_ids) > 0 and len(self.queue) > 0:
			idx = self.free_ids.pop()
			worker = self.queue.pop()
			await worker(idx)

	async def _release(self, idx):
		self.free_ids.append(idx)
		await self._spin()


class Context:
	def __init__(self, build_path):
		self.collection = dict()
		self.build_path = build_path
		self.current_build = None
		self.execution_pool: ExecutionPool = None
		self.config = dict()

	def require(self, id: str):
		return self.collection[id]

	def provide(self, id: str, builder):
		self.collection[id] = Assembly(id, self, builder)

	def start_build(self, concurrency):
		log.info("Build started")
		self.current_build = BuildRun()
		self.execution_pool = ExecutionPool(concurrency)

	def finish_build(self):
		for assembly in self.collection.values():
			assembly.build_cache.save()
		log.info("Build finished")
