from enum import Enum
import build.utils as utils
from typing import Dict, List, Tuple, Optional, Any, Set, Generic, TypeVar, Type
import os
import functools
import build.context as context
import inspect


T = TypeVar('T')


inherit = object()


def default_resolver(key, self, parent):
	mine = getattr(self, key)
	if mine is inherit:
		if parent:
			return parent.resolve(key)
		raise RuntimeError(f"no default value for {key}")
	return mine


def file_resolver(key, self, parent):
	mine = getattr(self, key)
	if mine is inherit:
		if parent:
			return parent.resolve(key)
		raise RuntimeError(f"no default value for {key}")
	if mine is None:
		return None
	if not isinstance(mine, utils.FileRef):
		raise RuntimeError(f"field {key} must be FileRef, {type(mine).__name__} found")
	return mine


def files_resolver(key, self, parent):
	mine = getattr(self, key)
	if parent:
		parents = parent.resolve(key)
		result = parents[:]
	else:
		result = []
	result.extend(mine)
	for entry in result:
		if not isinstance(entry, utils.FileRef):
			raise RuntimeError(f"field {key} must contain FileRef's, {type(entry).__name__} found")
	return result


def dict_resolver(key, self, parent):
	mine = getattr(self, key)
	if parent:
		parents = parent.resolve(key)
		result = dict(parents.items())
	else:
		result = dict()
	for k, v in mine.items():
		result[k] = v
	return result


def list_resolver(key, self, parent):
	mine = getattr(self, key)
	if parent:
		parents = parent.resolve(key)
		result = parents[:]
	else:
		result = []
	result.extend(mine)
	return result


class Environment:
	properties = {
		"std": default_resolver,
		"pch": file_resolver,
		"includes": files_resolver,
		"warnings": dict_resolver,
		"defines": dict_resolver,
		"opt": default_resolver,
		"debug": default_resolver
	}

	def __init__(self, parent: 'Environment', path: str):
		self.path = path
		self.parent = parent
		self.std = inherit
		self.pch = inherit
		self.debug = inherit
		self.defines = dict()
		self.opt = inherit
		self.includes = []
		self.warnings = dict()
		self._resolved = dict()

	def add_include(self, path):
		self.includes.append(self.resolve_path(path))

	def set_pch(self, path):
		self.pch = self.resolve_path(path)

	def resolve_path(self, path):
		if isinstance(path, str):
			return utils.FileRef(self.path, path)
		if isinstance(path, utils.FileRef):
			return path
		raise RuntimeError(f"invalid path type {type(path)}")

	def resolve(self, key):
		if key not in self._resolved:
			resolver = Environment.properties[key]
			self._resolved[key] = resolver(key, self, self.parent)
		return self._resolved[key]

	def inherit(self):
		return Environment(self, self.path)


class Opt(Enum):
	Disabled = "disabled"
	Fast = "fast"
	Full = "full"
	Aggressive = "aggressive"
	PreferSize = "size"


class Std(Enum):
	C11 = "c11"
	Cpp17 = "cpp17"


class DebugInfo(Enum):
	Disabled = "disabled"
	Enabled = "enabled"


class DependencyType(Enum):
	Private = "private"
	Transitive = "transitive"


def merge_defines(module: 'Module', target, source):
	for k, v in source:
		if k in target and target[k] != v:
			raise RuntimeError(f"conflicting defines for {k} in {module.name}: {v} vs {target[k]}")
		target[k] = v


def merge_includes(module: 'Module', target, source, source_path=None):
	target.extend(source)


class SourceGroup:
	def __init__(self, env: Environment, base_path: str, *files):
		self.files = list(files)
		self.base_path = base_path
		self.path = os.path.join(env.path, base_path)
		self.env = env.inherit()

	def add_file(self, path: str):
		self.files.append(path)

	def resolve_files(self, base_path):
		path = os.path.join(base_path, self.base_path)
		for file in self.files:
			for f in file(path):
				yield f



class Module:
	def __init__(self, name: str, env: Environment):
		self.groups: List[SourceGroup] = []
		self.name = name
		self.env = Environment(env, None)
		self.includes: List[utils.FileRef] = []
		self.defines: Dict[str, str] = dict()

		self.export_includes = []
		self.export_defines = dict()

		self.import_includes = []
		self.import_defines = dict()

		self.deps: List[Tuple[DependencyType, 'context.Assembly']] = []

		self._extra = dict()
		self._extra_banned = dict()

	def put_extra(self, object):
		for t in inspect.getmro(type(object)):
			if t not in self._extra:
				self._extra[t] = set()
			self._extra[t].add(object)

	def __contains__(self, item):
		return item in self._extra

	def __getitem__(self, item: Type) -> Any:
		if item in self._extra:
			objects: Set[Any] = self._extra[item]
			if len(objects) > 1:
				raise KeyError(f"multiple candidates on {item}")
			for object in objects:
				return object
		else:
			raise KeyError(f"assembly does not have {item}")

	def get_env(self) -> Environment:
		return self.env

	def set_base_path(self, path: str):
		self.env.path = path

	def add_public_include(self, path: str):
		self.includes.append(utils.FileRef(self.env.path, path))

	def add_public_define(self, key: str, value: str):
		self.defines[key] = value

	def add_group(self, group: SourceGroup):
		self.groups.append(group)

	def add_dependency(self, type: DependencyType, assembly: 'context.Assembly'):
		self.deps.append((type, assembly))

	def compute_import(self):
		for (type, assembly) in self.deps:
			self._import_module(assembly._module)

	def _import_module(self, module: 'Module'):
		merge_includes(self, self.import_includes, module.export_includes)
		merge_defines(self, self.import_defines, module.export_defines)

	def _export_module(self, module: 'Module'):
		merge_includes(self, self.export_includes, module.export_includes)
		merge_defines(self, self.export_defines, module.export_defines)

	def compute_export(self):
		merge_includes(self, self.export_includes, self.includes, self.env.path)
		merge_defines(self, self.export_defines, self.defines)
		for (type, assembly) in self.deps:
			if type == DependencyType.Transitive:
				self._export_module(assembly.get_module())


def c_files(**kwargs):
	return utils.files_by_ext(".c", **kwargs)


def cpp_files(**kwargs):
	return utils.files_by_ext(".cpp", **kwargs)

