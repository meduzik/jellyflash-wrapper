import build.cpp as cpp
from build.utils import *
from build.context import ensure_dir
import logging
import asyncio
import shlex


log = logging.getLogger("ndk")


__initialized = False


def initialize():
	global __initialized
	if __initialized:
		return
	setup_env()
	__initialized = True


@operation("Setting up the environment")
def setup_env():
	with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../env.txt"), "r") as fp:
		for line in fp.readlines():
			line = line.strip()
			if len(line) <= 0:
				continue
			key, value = line.split("=", 1)
			os.environ[key] = value


NDK_SDK = "D:/Soft/ndk-15"
LLVM_ROOT = f"{NDK_SDK}/bin"

EM_CC = f"{LLVM_ROOT}\\clang60++.exe"
EM_AR = f"{LLVM_ROOT}\\arm-linux-androideabi-ar.exe"
EM_LINK = f"{LLVM_ROOT}\\llvm-link.exe"
EM_LD = f"{NDK_SDK}\\arm-linux-androideabi\\bin\\ld.exe"

LLC_BIN = f"F:\\LLVMBuild\\Release\\bin\\llc.exe"
OPT_BIN = "F:\\LLVMBuild\\Release\\bin\\opt.exe"

common_opts = [
	"-cc1",
	"-emit-obj",
	"-triple", "armv7-none-linux-android15",
	"-mrelax-all",
	"-disable-free",
	"-disable-llvm-verifier",
	"-discard-value-names",
	"-mrelocation-model", "pic",
	"-pic-level", "2",
	"-pic-is-pie",
	"-D__ANDROID__",
	"-mthread-model", "posix",
	"-mdisable-fp-elim",
	"-fmath-errno",
	"-masm-verbose",
	"-mconstructor-aliases",
	"-ffunction-sections",
	"-fdata-sections",
	"-fvisibility", "hidden",
	"-fuse-init-array",
	"-target-cpu", "generic",
	"-target-feature", "+soft-float-abi",
	"-target-abi", "aapcs-linux",
	"-mfloat-abi", "soft",
	"-fallow-half-arguments-and-returns",
	"-dwarf-column-info",
	"-debugger-tuning=gdb",
	"-resource-dir", f"{NDK_SDK}\\lib64\\clang\\7.0.2",
	"-isysroot", f"{NDK_SDK}\\sysroot",
	"-internal-isystem", "D:\\Soft\\ndk-15\\bin/../lib/gcc/arm-linux-androideabi/4.9.x/../../../../include/c++/4.9.x",
	"-internal-isystem", "D:\\Soft\\ndk-15\\bin\\..\\sysroot/usr/local/include",
	"-internal-isystem", "D:\\Soft\\ndk-15\\lib64\\clang\\6.0.2\\include",
	"-internal-externc-isystem", "D:\\Soft\\ndk-15\\bin\\..\\sysroot/usr/include",
	"-fdeprecated-macro",
	"-ferror-limit", "19",
	"-fmessage-length", "80",
	"-fno-signed-char",
	"-fobjc-runtime=gcc",
	"-fcxx-exceptions",
	"-fexceptions",
	"-fdiagnostics-show-option",
	"-fcolor-diagnostics",
]

static_link_opts = [
	"-target", "armv7-none-linux-android21"
]


async def run_clang(idx, *args):
	command = [*common_opts, *args]
	return await run_command_async(idx, EM_CC, *command)


linker_preopts = [
	f"--sysroot={NDK_SDK}\\sysroot",
	"-X", "--enable-new-dtags",
	"--eh-frame-hdr",
	"-m", "armelf_linux_eabi",
	"-shared",
	f"{NDK_SDK}\\sysroot/usr/lib\\crtbegin_so.o",
	f"-L{NDK_SDK}\\lib64\\clang\\6.0.2\\lib\\linux\\arm",
	f"-L{NDK_SDK}\\lib/gcc/arm-linux-androideabi/4.9.x/armv7-a",
	f"-L{NDK_SDK}\\arm-linux-androideabi//lib/armv7-a",
	f"-L{NDK_SDK}\\sysroot/usr/lib",
	"--gc-sections",
	"-Bsymbolic",
]

linker_postopts = [
	"-lGLESv2",
	"-Bstatic",
	"-lstdc++",
	"-Bdynamic",
	"-lm",
	"-lgcc",
	"-ldl",
	"-lc",
	"-lgcc",
	"-ldl",
	f"{NDK_SDK}\\sysroot/usr/lib\\crtend_so.o",
]


async def run_linker(idx, *args):
	command = [*linker_preopts, *args, *linker_postopts]
	return await run_command_async(idx, EM_LD, *command)


spaces_re = re.compile('\s+')
backslash_re = re.compile('\\\\[\n\r]')


def read_deps(path):
	with open(path, "r") as fp:
		contents = fp.read()
	no_slashes = backslash_re.sub(' ', contents)
	only_deps = no_slashes[no_slashes.index(':') + 1:]
	return [x for x in spaces_re.split(only_deps) if len(x) > 0]


def StaticLib(*args, **kwargs):
	return Builder("staticlib", *args, **kwargs)


def Application(*args, **kwargs):
	return Builder("app", *args, **kwargs)


class NdkManifest:
	def __init__(self, module: cpp.Module):
		self.module = module
		self.own_artifacts = []

		self.artifacts = []

		self.merges = set()

	def add_artifact(self, path):
		self.own_artifacts.append(path)

	def finalize(self):
		for other in self.merges:
			self.artifacts.extend(other.own_artifacts)
			self.artifacts.extend(other.artifacts)

	def merge_from(self, other: 'NdkManifest'):
		self.merges.add(other)


class Builder:
	def __init__(self, target, builder, env: cpp.Environment, unity=False):
		self.builder = builder
		self.env = env
		self.module = None
		self.assembly = None
		self.context = None
		self.target = target
		self.manifest: NdkManifest = None
		self.pch_objects = dict()
		self.unity = unity

	async def prepare(self, assembly):
		self.module = cpp.Module(assembly.name, self.env)
		self.manifest = NdkManifest(self.module)
		self.module.put_extra(self.manifest)
		self.builder(self.module, assembly.context)
		self.assembly = assembly
		self.context = assembly.context
		return self.module

	def compute_manifest(self):
		self.manifest.merge_from(self.manifest)
		for (type, assembly) in self.module.deps:
			their_manifest = assembly.get_module()[NdkManifest]
			self.manifest.merge_from(their_manifest)
		self.manifest.finalize()

	async def build(self):
		deps = []
		for (type, assembly) in self.module.deps:
			deps.append(assembly.build())

		await asyncio.gather(*deps)

		self.module.compute_import()
		self.compute_manifest()

		print("start building " + self.module.name)

		object_files = []

		groups = []
		for group in self.module.groups:
			async def process_group(group):
				base_group_path = sha1(group.base_path)[0:16]

				env: cpp.Environment = group.env
				group_path = env.path

				std = env.resolve('std')

				group_includes = self.module.import_includes[:]
				cpp.merge_includes(self.module, group_includes, env.resolve('includes'))

				group_defines = dict()
				cpp.merge_defines(self.module, group_defines, self.module.import_defines)
				# TODO:: cpp.merge_defines(self.module, group_defines, env.resolve('defines'))

				command_basic_opts = []
				# warning level
				command_basic_opts.append("-Wall")

				for k, v in sorted(env.resolve('warnings').items(), key=lambda p: p[0]):
					if v is False:
						command_basic_opts.append("-Wno-" + k)
					else:
						command_basic_opts.append("-W" + k)

				# c standard
				if std == cpp.Std.C11:
					command_basic_opts.append("-std=c11")
					command_basic_opts.append("-x")
					command_basic_opts.append("c")
					run_as_c = True
				elif std == cpp.Std.Cpp17:
					command_basic_opts.append("-std=c++17")
					command_basic_opts.append("-x")
					command_basic_opts.append("c++")
					run_as_c = False
				else:
					raise RuntimeError(f"unsupported language standard {std}")

				defines = env.resolve('defines')
				for k, v in defines.items():
					if v is not None:
						command_basic_opts.append("-D" + k + "=" + v)
					else:
						command_basic_opts.append("-D" + k)

				# debug info
				debug = env.resolve('debug')
				if debug == cpp.DebugInfo.Disabled:
					# do nothing, debug info is not enabled by default
					pass
				elif debug == cpp.DebugInfo.Enabled:
					command_basic_opts.append("-debug-info-kind=limited")
					command_basic_opts.append("-dwarf-version=4")

				# optimizations
				opt = env.resolve('opt')
				if opt == cpp.Opt.Disabled:
					command_basic_opts.append("-O0")
				elif opt == cpp.Opt.Fast:
					command_basic_opts.append("-O1")
				elif opt == cpp.Opt.Full:
					command_basic_opts.append("-O2")
				elif opt == cpp.Opt.Aggressive:
					command_basic_opts.append("-O3")
				elif opt == cpp.Opt.PreferSize:
					command_basic_opts.append("-Os")

				for include in group_includes:
					command_basic_opts.append("-I" + include.path)

				for k, v in sorted(group_defines, key=lambda p: p[0]):
					if v is None:
						command_basic_opts.append("-D" + k)
					else:
						command_basic_opts.append("-D" + k + "=" + v)

				pch_object = None
				pch_ref = env.resolve('pch')
				if pch_ref is not None:
					async def worker(idx):
						nonlocal pch_object
						pch = pch_ref.path
						pch_base = os.path.join(self.assembly.build_path, "obj", base_group_path, os.path.relpath(pch, group.path))
						dep_file = pch_base + ".d"
						pch_path = pch_base + ".pch"

						command = command_basic_opts[:]

						command.append("-emit-pch")
						command.append("-o")
						command.append(pch_path)

						command.append("-dependency-file")
						command.append(dep_file)

						command.append(pch)
						command.append("-MT")
						command.append(pch)

						command_string = command_to_string(command)
						if not self.assembly.build_cache.check_artifact(pch_path, command_string):
							log.info(f"Building pch file for {pch}...")
							ensure_dir(os.path.dirname(pch_path))
							await run_clang(idx, *command)
							computed_deps = read_deps(dep_file)
							self.assembly.build_cache.commit_artifact(pch_path, command_string, computed_deps)
						pch_object = pch_path

					await self.context.execution_pool.execute(worker)

				sources = []

				async def process_source(source_ref):
					async def worker(idx):
						source = source_ref.path
						source_file = source
						relpath = os.path.relpath(source, group.path).replace('..', 'dotdot')
						object_base = os.path.join(
							self.assembly.build_path,
							"obj",
							base_group_path,
							relpath
						)
						object_file = object_base + ".o"
						dep_file = object_base + ".d"

						command = command_basic_opts[:]

						# build object file and stop
						#command.append("-c")

						# dependency file
						command.append("-dependency-file")
						command.append(dep_file)

						# object output
						command.append("-o")
						command.append(object_file)

						if pch_object:
							command.append("-include-pch")
							command.append(pch_object)

						# input file
						command.append(source_file)

						command.append("-MT")
						command.append(source_file)

						command_string = command_to_string(command)
						if not self.assembly.build_cache.check_artifact(object_file, command_string):
							log.info(f"[{idx}] Building object file for {source_file}...")
							self.assembly.build_cache.invalidate_artifact(object_file)
							ensure_dir(os.path.dirname(object_file))

							await run_clang(idx, *command)

							computed_deps = read_deps(dep_file)
							self.assembly.build_cache.commit_artifact(object_file, command_string, computed_deps)

						object_files.append(object_file)

					await self.context.execution_pool.execute(worker)

				if self.unity:
					if std == cpp.Std.C11:
						ext = "c"
					else:
						ext = "cpp"
					unity_base = os.path.join(self.assembly.build_path, "unity", base_group_path)
					unity_source = os.path.join(unity_base, "unity." + ext)
					source_files = list(map(lambda file: file.path, group.resolve_files(group_path)))
					command_string = command_to_string(["make_unity3", source_files])
					if not self.assembly.build_cache.check_artifact(unity_source, command_string):
						log.info(f"Building unity file...")
						ensure_dir(os.path.dirname(unity_source))
						with open(unity_source, "w") as fp:
							for file in source_files:
								include_path = os.path.normpath(os.path.relpath(file, unity_base)).replace('\\', '/')
								fp.write(f"#include \"{include_path}\"\n")
						self.assembly.build_cache.commit_artifact(unity_source, command_string, [])
					sources.append(process_source(FileRef(unity_base, "unity." + ext)))
				else:
					for source in group.resolve_files(group_path):
						sources.append(process_source(source))
				await asyncio.gather(*sources)

			groups.append(process_group(group))

		await asyncio.gather(*groups)

		# linking
		async def worker(idx):
			nonlocal object_files

			prefix = "lib"
			if self.target == "staticlib":
				ext = ".a"
				base_dir = "lib"
			elif self.target == "app":
				ext = ".so"
				base_dir = "bin"
				object_files.extend(self.manifest.artifacts)
			else:
				raise RuntimeError(f"invalid target {self.target}")

			#object_files = list(frozenset(object_files))
			#object_files.sort()
			#object_files.reverse()

			command_file = os.path.join(self.assembly.build_path, base_dir, self.assembly.name + ".inp")
			result_file = os.path.join(self.assembly.build_path, base_dir, prefix + self.assembly.name + ext)

			command = []
			other_deps = []

			opt = self.module.get_env().resolve('opt')
			opt_level = None
			if opt == cpp.Opt.Disabled:
				opt_level = "-O0"
			elif opt == cpp.Opt.Fast:
				opt_level = "-O1"
			elif opt == cpp.Opt.Full:
				opt_level = "-O2"
			elif opt == cpp.Opt.Aggressive:
				opt_level = "-O3"
			elif opt == cpp.Opt.PreferSize:
				opt_level = "-Os"

			if self.target == "staticlib":
				command.append("rc")
				command.append(result_file)
				command.append("@" + command_file)
			elif self.target == "app":
				command.append("-o")
				command.append(result_file)
				command.append("-z")
				command.append("defs")
				command.append("@" + command_file)

			command_string = command_to_string(command + [opt_level] + object_files + ["c"])
			if not self.assembly.build_cache.check_artifact(result_file, command_string):
				self.assembly.build_cache.invalidate_artifact(result_file)
				log.info(f"[{idx}] Linking...")
				ensure_dir(os.path.dirname(result_file))
				with open(command_file, "w") as fp:
					for file in object_files:
						fp.write(json.dumps(file) + " ")
				if self.target == "staticlib":
					await run_command_async(idx, EM_AR, *command)
				elif self.target == "app":
					await run_linker(idx, *command)

				self.assembly.build_cache.commit_artifact(result_file, command_string, object_files + other_deps)
			self.manifest.add_artifact(result_file)

		await self.context.execution_pool.execute(worker)

		self.module.compute_export()

		print("end building " + self.module.name)
