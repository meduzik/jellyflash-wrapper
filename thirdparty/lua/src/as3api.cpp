#include <stdlib.h>
#include <stdio.h>
#include <string.h>


extern "C" {

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"


#undef lua_pop
void lua_pop(lua_State *L, int n) { lua_settop(L, -(n)-1); };

LUA_API int luaL_setupvalue(lua_State* L, int funcindex, int n) {
	return (lua_setupvalue(L, funcindex, n) != 0) ? 1 : 0;
}

void luaL_pushcfunction(lua_State *L, void* fn) {
	lua_pushcfunction(L, (lua_CFunction)fn);
}

LUA_API int luaL_loadnamedstring(lua_State* L, const char* buff, const char* name) {
	return luaL_loadbufferx(L, buff, strlen(buff), name, NULL);
}

LUA_API int luaL_loadnamedbuffer(lua_State* L, const void* buff, int length, const char* name) {
	return luaL_loadbufferx(L, (const char*)buff, length, name, NULL);
}


LUA_API void luaL_call(lua_State *L, int nargs, int nresults) {
	lua_callk(L, nargs, nresults, 0, NULL);
}

LUA_API int luaL_yield(lua_State *L, int nresults, int ctx) {
	return lua_yieldk(L, nresults, ctx, NULL);
}

LUA_API int luaL_pcall(lua_State *L, int nargs, int nresults, int errfn) {
	return lua_pcallk(L, nargs, nresults, errfn, 0, NULL);
}

LUA_API void* luaL_tocfunction(lua_State *L, int n) {
	return (void*)lua_tocfunction(L, n);
}

static lua_State *getthread(lua_State *L, int *arg) {
	if (lua_isthread(L, 1)) {
		*arg = 1;
		return lua_tothread(L, 1);
	} else {
		*arg = 0;
		return L;
	}
}

static int traceback_func(lua_State* L) {
	int arg;
	lua_State *L1 = getthread(L, &arg);
	const char *msg = lua_tostring(L, arg + 1);
	if (msg == NULL && !lua_isnoneornil(L, arg + 1))  /* non-string 'msg'? */
		lua_pushvalue(L, arg + 1);  /* return it untouched */
	else {
		int level = luaL_optint(L, arg + 2, (L == L1) ? 1 : 0);
		luaL_traceback(L, L1, msg, level);
	}
	return 1;
}

LUA_API void* lua_gettracebackfunc() {
	return (void*)traceback_func;
}

static int callwrapper(lua_State* L) {
	lua_CFunction wrapper = lua_tocfunction(L, lua_upvalueindex(1));
	int index = (int)lua_tointeger(L, lua_upvalueindex(2));
	lua_pushnumber(L, index);
	return wrapper(L);
}

LUA_API void luaL_makecallwrapclosure(lua_State* L) {
	lua_pushcclosure(L, callwrapper, 2);
}

LUA_API int luaL_debug_funcname(lua_State* L) {
	lua_Debug ar;
	int ret = lua_getinfo(L, ">S", &ar);
	if (!ret) {
		return 0;
	}
	lua_pushstring(L, ar.what);
	lua_pushstring(L, ar.short_src);
	lua_pushinteger(L, ar.linedefined);
	return ret;
}

LUA_API int luaL_debug_upvalues(lua_State* L) {
	lua_Debug ar;
	int ret = lua_getinfo(L, ">u", &ar);
	if (!ret) {
		return 0;
	}
	lua_pushinteger(L, ar.nups);
	return ret;
}

static const luaL_Reg loadedlibs[] = {
  {"_G", luaopen_base},
  {LUA_LOADLIBNAME, luaopen_package},
  {LUA_COLIBNAME, luaopen_coroutine},
  {LUA_TABLIBNAME, luaopen_table},
  {LUA_STRLIBNAME, luaopen_string},
  {LUA_MATHLIBNAME, luaopen_math},
  {LUA_DBLIBNAME, luaopen_debug},
#if defined(LUA_COMPAT_BITLIB)
  {LUA_BITLIBNAME, luaopen_bit32},
#endif
  {NULL, NULL}
};

LUA_API void luaL_openlib_byname(lua_State *L, const char* name) {
	const luaL_Reg *lib;

	for (lib = loadedlibs; lib->func; lib++) {
		if (!strcmp(lib->name, name)) {
			luaL_requiref(L, lib->name, lib->func, 1);
			lua_pop(L, 1);
		}
	}
}

LUALIB_API void luaL_openlibs_s(lua_State *L) {
	const luaL_Reg *lib;
	/* "require" functions from 'loadedlibs' and set results to global table */
	for (lib = loadedlibs; lib->func; lib++) {
		luaL_requiref(L, lib->name, lib->func, 1);
		lua_pop(L, 1);  /* remove lib */
	}
}

LUA_API void luaL_opendebug(lua_State* L) {
	luaL_requiref(L, LUA_DBLIBNAME, luaopen_debug, 1);
	lua_pop(L, 1);  /* remove lib */
}

LUA_API void luaL_pushuserdata(lua_State* L, void* ud) {
	void** addr = (void**)lua_newuserdata(L, sizeof(void*));
	*addr = ud;
}

LUA_API void* luaL_touserdata(lua_State* L, int idx) {
	switch (lua_type(L, idx)) {
	case LUA_TLIGHTUSERDATA:
	{
		void* addr = lua_touserdata(L, idx);
		return addr;
	}break;
	case LUA_TUSERDATA:
	{
		void* addr = lua_touserdata(L, idx);
		return *(void**)addr;
	}break;
	default: {
		return 0;
	}break;
	}
}

LUA_API int luaL_upvalueindex(lua_State* L, int idx) {
	return lua_upvalueindex(idx);
}

typedef struct {
	int capacity;
	int length;
	char data[];
} LuaChunk;

static int chunk_writer(lua_State *L, const void* p, size_t sz, void* ud) {
	LuaChunk** chunkPtr = (LuaChunk**)ud;
	LuaChunk* chunk = *chunkPtr;

	int length = chunk ? chunk->length : 0;
	int requiredCapacity = length + (int)sz;

	if (!chunk || chunk->capacity < requiredCapacity) {
		int newCapacity = 64;

		if (chunk) {
			newCapacity = chunk->capacity * 2;
		}

		if (newCapacity < requiredCapacity) {
			newCapacity = requiredCapacity;
		}

		chunk = (LuaChunk*)realloc(chunk, sizeof(LuaChunk) + newCapacity);
		chunk->capacity = newCapacity;
		chunk->length = length;
	}

	memcpy(chunk->data + chunk->length, p, sz);
	chunk->length += (int)sz;

	*chunkPtr = chunk;

	return 0;
}

LUA_API LuaChunk* luaL_chunk_dump(lua_State* L) {
	LuaChunk* chunkPtr = NULL;

	lua_dump(L, chunk_writer, &chunkPtr);

	return chunkPtr;
}

LUA_API int luaL_chunk_get_length(LuaChunk* chunk) {
	return chunk->length;
}

LUA_API void* luaL_chunk_get_data(LuaChunk* chunk) {
	return chunk->data;
}

LUA_API void luaL_chunk_free(LuaChunk* chunk) {
	free(chunk);
}

}
