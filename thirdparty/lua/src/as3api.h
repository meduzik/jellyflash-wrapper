extern "C" {

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

struct LuaChunk;

#undef lua_pop
LUA_API void lua_pop(lua_State *L, int n);

LUA_API int luaL_yield(lua_State *L, int nresults, int ctx);

LUA_API int push_flashref(lua_State *L);

LUA_API void luaL_pushcfunction(lua_State *L, void* fn);

LUA_API int luaL_loadnamedstring(lua_State* L, const char* buff, const char* name);
LUA_API int luaL_loadnamedbuffer(lua_State* L, const void* buff, int length, const char* name);
LUA_API void luaL_call(lua_State *L, int nargs, int nresults);
LUA_API int luaL_pcall(lua_State *L, int nargs, int nresults, int errfn);

LUA_API void* luaL_tocfunction(lua_State *L, int n);

LUA_API void* lua_gettracebackfunc();
LUA_API void luaL_makecallwrapclosure(lua_State* L);

LUA_API void luaL_openlibs_s(lua_State* L);
LUA_API void luaL_opendebug(lua_State* L);
LUA_API void luaL_openlib_byname (lua_State *L, const char* name);

LUA_API int luaL_setupvalue(lua_State* L, int funcindex, int n);

LUA_API void luaL_pushuserdata(lua_State* L, void* ud);
LUA_API void* luaL_touserdata(lua_State* L, int idx);

LUA_API int luaL_debug_funcname(lua_State* L);
LUA_API int luaL_debug_upvalues(lua_State* L);

LUA_API int luaL_upvalueindex(lua_State* L, int idx);

LUA_API LuaChunk* luaL_chunk_dump(lua_State* L);
LUA_API int luaL_chunk_get_length(LuaChunk* chunk);
LUA_API void* luaL_chunk_get_data(LuaChunk* chunk);
LUA_API void luaL_chunk_free(LuaChunk* chunk);

}
