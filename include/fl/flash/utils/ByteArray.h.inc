#define flash_utils_ByteArray_PAYLOAD \
	jfl::um_vec<uint8_t> data; \
	size_t pos; \
	bool native_endian; \
	static void check_size(fl::flash::utils::ByteArray* bytes, size_t size); \
	static void ensure(fl::flash::utils::ByteArray* bytes, size_t size); \
	static void ensure_size(fl::flash::utils::ByteArray* bytes, size_t size); \
	static void write_raw(fl::flash::utils::ByteArray* bytes, const void* data, size_t size); \
	static void read_raw(fl::flash::utils::ByteArray* bytes, void* data, size_t size); \
	template<class T> \
	static void write(fl::flash::utils::ByteArray* bytes, T&& value); \
	static void check_read(fl::flash::utils::ByteArray* bytes, size_t size); 

