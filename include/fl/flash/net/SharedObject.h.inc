#define flash_net_SharedObject_PAYLOAD \
	jfl::net::SharedObjectImpl* impl; \
	jfl::UInt size; \

#define flash_net_SharedObject_PAYLOAD_OWN \
	

namespace jfl::net{
	struct SharedObjectImpl;

	SharedObjectImpl* SharedObjectImplCreate(std::string_view name, std::string_view path);
	void SharedObjectImplDestroy(SharedObjectImpl*);
	void SharedObjectImplLoad(SharedObjectImpl*, arena_vec<uint8_t>& data);
	void SharedObjectImplSave(SharedObjectImpl*, const arena_vec<uint8_t>& data);
	void SharedObjectImplClear(SharedObjectImpl*);
}

