#define flash_net_URLStream_PAYLOAD \
	jfl::um_vec<uint8_t> buffer; \
	size_t begin; \
	std::shared_ptr<jfl::IStreamExecutor> executor; \
	bool connected; \
	bool nativeEndian; 

#define flash_net_URLStream_PAYLOAD_OWN \
	static void ensure(URLStream* self, size_t size); \
	static const uint8_t* read_raw(URLStream* self, size_t size); 

#include <jfl/net/StreamExecutor.h>

