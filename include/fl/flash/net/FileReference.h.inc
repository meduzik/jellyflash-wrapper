#if defined(__ANDROID__)
	#define flash_net_FileReference_PAYLOAD \
		std::string path;
#else 
	#define flash_net_FileReference_PAYLOAD \
		std::filesystem::path path;
#endif

