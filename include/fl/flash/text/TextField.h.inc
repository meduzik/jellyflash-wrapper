#define flash_text_TextField_PAYLOAD \
	jfl::TextLayout textLayout;

#define flash_text_TextField_PAYLOAD_OWN \
	static void jfl_renderTo(TextField* self, fl::flash::display::BitmapData* bitmap, const jfl::matrix2d& mat); \
	static jfl::matrix2d jfl_computeGlobalMatrix(TextField* self);

#include <fl/flash/display/BitmapData.h>
#include <jfl/text/TextRendering.h>
