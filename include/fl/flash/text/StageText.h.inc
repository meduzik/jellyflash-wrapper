#if defined(__EMSCRIPTEN__)
#define flash_text_StageText_PAYLOAD \
	em::StageText* stage_text; \
	fl::flash::display::Stage* stage; \
	jfl::String font_family, font_weight, text_align, font_posture; \
	jfl::Int font_size, max_chars; \
	jfl::UInt font_color; \
	bool editable, multiline, visible; \

namespace em{
	struct StageText;
}
namespace fl::flash::display{
	struct Stage;
}
#endif

	
