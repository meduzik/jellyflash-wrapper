#define Array_PAYLOAD \
	jfl::ArrayContents contents; \
	static jfl::Any jfl_get(Array* jfl_this, jfl::UInt index); \

#define Array_PAYLOAD_OWN \
	static void jfl_set(Array* jfl_this, jfl::UInt index, jfl::Any value); \
	static void jfl_del(Array* jfl_this, jfl::UInt index); \
	static void push(Array* jfl_this, jfl::Any value); 

	
namespace jfl{

struct ArrayContents{
	bool is_contiguous;
	union{
		um_vec<Any> vec;
		um_map<UInt, Any> map;
	};
	UInt len;

	ArrayContents() {}
	~ArrayContents() {}
};

}
