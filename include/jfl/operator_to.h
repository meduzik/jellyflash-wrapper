#pragma once
#include "types.h"
#include "macro.h"
#include "string.h"
#include "function.h"
#include "object.h"
#include "any.h"
#include "error.h"
#include "type_inspection.h"
#include "number.h"
#include "error.h"
#include <limits>

namespace jfl{
Number parseInt(String str, Int radix);
String NumberToString(Number value);

inline Int primitive_to_int(Any val);
inline UInt primitive_to_uint(Any val);
inline Number primitive_to_Number(Any val);
inline String primitive_to_String(Any val);


inline Boolean Boolean_to_Boolean(Boolean val) { return val; }
inline Boolean int_to_Boolean(Int val) { return val != 0; }
inline Boolean uint_to_Boolean(UInt val) { return val != 0u; }
inline Boolean Number_to_Boolean(Number val) { return !std::isnan(val) && val != 0; }
inline Boolean String_to_Boolean(String val) { return val.tag > type_tag::StringBegin && val.tag <= type_tag::StringEnd; }
inline Boolean Function_to_Boolean(Function val) { return val.has_function(); }
inline Boolean Object_to_Boolean(Object* val) { return val != nullptr; }


inline UInt Boolean_to_uint(Boolean val) { return val ? 1u : 0u; }
inline UInt int_to_uint(Int val) { return (UInt)val; }
inline UInt uint_to_uint(UInt val) { return val; }

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable:4146)
#endif

jfl_noinline
inline UInt Number_to_uint(Number val) {
	if ( std::isnan(val) || !std::isfinite(val) ){
		return 0;
	}
	if ( val < 0 ){
		double intpart;
		modf(-val, &intpart);
		if (intpart > (double)std::numeric_limits<uint64_t>::max()) {
			jfl_notimpl;
		} else {
			return -(UInt)intpart;
		}
	}else{
		double intpart;
		modf(val, &intpart);
		if (intpart > (double)std::numeric_limits<uint64_t>::max() ){
			jfl_notimpl;
		}else{
			return (UInt)intpart;
		}
	}
}

#if defined(_MSC_VER)
#pragma warning(pop)
#endif

inline UInt String_to_uint(String val) {
	return Number_to_uint(parseInt(val, 10));
}
inline UInt Function_to_uint(Function val) { return 0; }
jfl_noinline
inline UInt Object_to_uint(Object* val) {
	if (!val) {
		return 0u;
	}
	Any any;
	val->jfl_vtable->jfl_vth.dynamic(val, nullptr, &any, dynamic_op::value_of);
	return primitive_to_uint(any);
}



inline Int Boolean_to_int(Boolean val) { return val ? 1 : 0; }
inline Int int_to_int(Int val) { return val; }
inline Int uint_to_int(UInt val) { return (Int)val; }
inline Int Number_to_int(Number val) {
	return (Int)Number_to_uint(val);
}
inline Int String_to_int(String val) {
	return Number_to_int(parseInt(val, 10));
}
inline Int Function_to_int(Function val) { return 0; }
jfl_noinline
inline Int Object_to_int(Object* val) {
	if (!val) {
		return 0;
	}
	Any any;
	val->jfl_vtable->jfl_vth.dynamic(val, nullptr, &any, dynamic_op::value_of);
	return primitive_to_int(any);
}

Number parseNumber(std::string_view sv, bool* err);

inline Number Boolean_to_Number(Boolean val) { return val ? 1u : 0u; }
inline Number int_to_Number(Int val) { return (UInt)val; }
inline Number uint_to_Number(UInt val) { return val; }
inline Number Number_to_Number(Number val) { return val; }
jfl_noinline
inline Number String_to_Number(String val) {
	auto view = val.to_string_view();
	bool err;
	return parseNumber(view, &err);
}
jfl_noinline
inline Number Function_to_Number(Function val) { return 0; }
jfl_noinline
inline Number Object_to_Number(Object* val) {
	if (!val) {
		return 0.0;
	}
	Any any;
	val->jfl_vtable->jfl_vth.dynamic(val, nullptr, &any, dynamic_op::value_of);
	return primitive_to_Number(any);
}


jfl_noinline
inline String Boolean_to_String(Boolean val) { return val ? StringSmall{4, "true"} : StringSmall{5, "false"}; }

jfl_noinline
inline String int_to_String(Int val) {
	char buf[16];
	sprintf(buf, "%d", val);
	return String::Make(buf);
}

jfl_noinline
inline String uint_to_String(UInt val) {
	char buf[16];
	sprintf(buf, "%d", val);
	return String::Make(buf);
}

jfl_noinline
inline String Number_to_String(Number val) {
	return NumberToString(val);
}

jfl_noinline
inline String String_to_String(String val) { return val; }

jfl_noinline
inline String Function_to_String(Function val) { return String::Make("[Fun]"); }

jfl_noinline
inline String Object_to_String(Object* val) {
	if (!val) {
		return StringSmall{ 4, "null" };
	}
	Any any;
	val->jfl_vtable->jfl_vth.dynamic(val, nullptr, &any, dynamic_op::to_string);
	return primitive_to_String(any);
}

jfl_noinline
inline Object* Object_to_Class(Object* val, const Class* class_) {
	if ( !val ){
		return nullptr;
	}
	if ( !ti_extends(val->jfl_vtable->jfl_vth.ti, class_) ){
		raise_cast_error(class_of(val), class_);
	}
	return val;
}

jfl_noinline
inline Interface Object_to_Interface(Object* val, const Class* class_) {
	if ( !val ){
		return nullptr;
	}
	const void* impl;
	if (!ti_get_implement(val->jfl_vtable->jfl_vth.ti, class_, &impl)) {
		raise_cast_error(class_of(val), class_);
	}
	return {val, impl};
}

jfl_noinline
inline Int primitive_to_int(Any val){
	switch ( val.tag ){
	case type_tag::Undefined:
	case type_tag::Null: return 0;
	case type_tag::Boolean: return val.bval ? 1 : 0;
	case type_tag::Int: return val.ival;
	case type_tag::UInt: return (Int)val.uval;
	case type_tag::NumberBox: return Number_to_int(val.nval->value);
	case type_tag::Float: return Number_to_int(val.fval);
	default:{
		if ( val.tag <= type_tag::StringEnd ){
			return String_to_int(*(String*)&val);
		}else{
			return 0;
		}
	}break;
	}
}

jfl_noinline
inline UInt primitive_to_uint(Any val){
	switch (val.tag) {
	case type_tag::Undefined:
	case type_tag::Null: return 0u;
	case type_tag::Boolean: return val.bval ? 1u : 0u;
	case type_tag::Int: return (UInt)val.ival;
	case type_tag::UInt: return val.uval;
	case type_tag::NumberBox: return Number_to_uint(val.nval->value);
	case type_tag::Float: return Number_to_uint(val.fval);
	default: {
		if (val.tag <= type_tag::StringEnd) {
			return String_to_uint(*(String*)&val);
		} else {
			return 0u;
		}
	}break;
	}
}

jfl_noinline
inline Number primitive_to_Number(Any val){
	switch (val.tag) {
	case type_tag::Undefined: return NaN;
	case type_tag::Null: return 0.0;
	case type_tag::Boolean: return val.bval ? 1.0 : 0.0;
	case type_tag::Int: return val.ival;
	case type_tag::UInt: return val.uval;
	case type_tag::NumberBox: return val.nval->value;
	case type_tag::Float: return val.fval;
	default: {
		if (val.tag <= type_tag::StringEnd) {
			return String_to_Number(*(String*)&val);
		} else {
			return 0u;
		}
	}break;
	}
}

jfl_noinline
inline String primitive_to_String(Any val){
	switch (val.tag) {
	case type_tag::Undefined: return nullptr;
	case type_tag::Null: return nullptr;
	case type_tag::Boolean: return val.bval ? StringSmall{4, "true"} : StringSmall{5, "false"};
	case type_tag::Int: return int_to_String(val.ival);
	case type_tag::UInt: return uint_to_String(val.uval);
	case type_tag::NumberBox: return Number_to_String(val.nval->value);
	case type_tag::Float: return Number_to_String(val.fval);
	case type_tag::Rest: return StringSmall{4, "Rest"};
	default: {
		if (val.tag <= type_tag::StringEnd) {
			return *(String*)&val;
		} else if ((uint8_t)val.tag & (uint8_t)type_tag::FunctionBit) {
			return Function_to_String(*(Function*)&val);
		} else {
			const TypeInfo* ti = val.object->jfl_vtable->jfl_vth.ti;
			return ((Class*)ti->values[ti->num_classes - 1])->name;
		}
	}break;
	}
}

/*
inline Boolean Boolean_to_Boolean(Boolean val) { return val; }
inline Boolean int_to_Boolean(Int val) { return val != 0; }
inline Boolean uint_to_Boolean(UInt val) { return val != 0u; }
inline Boolean Number_to_Boolean(Number val) { return ; }
inline Boolean String_to_Boolean(String val) { return val.tag > type_tag::StringBegin && val.tag <= type_tag::StringEnd; }
inline Boolean Function_to_Boolean(Function val) { return val.tag == type_tag::Function; }
inline Boolean Object_to_Boolean(Object* val) { return val != nullptr; }
*/
jfl_noinline
inline Boolean any_to_Boolean(Any val){
	switch ( val.tag ){
	case type_tag::Null:
	case type_tag::Undefined:{
		return false;
	}break;
	case type_tag::Boolean: {
		return val.bval != 0;
	}break;
	case type_tag::Int: {
		return val.ival != 0;
	}break;
	case type_tag::UInt: {
		return val.uval != 0;
	}break;
	case type_tag::NumberBox: {
		Number nval = val.nval->value;
		return !std::isnan(nval) && nval != 0;
	}break;
	case type_tag::Float: {
		return !std::isnan(val.fval) && val.fval != 0;
	}break;
	case type_tag::Object:
	case type_tag::Rest: {
		return true;
	}break;
	case type_tag::StringBegin:{
		return false;
	}break;
	default:{
		if ( val.tag <= type_tag::StringEnd ){
			return true;
		}
		if ((uint8_t)val.tag & (uint8_t)type_tag::FunctionBit) {
			return true;
		}
		jfl_unreachable;
	}break;
	}
}

jfl_noinline
inline Int any_to_int(Any val){
	if ( val.tag == type_tag::Object ){
		return Object_to_int(val.object);
	}else{
		return primitive_to_int(val);
	}
}

jfl_noinline
inline UInt any_to_uint(Any val){
	if (val.tag == type_tag::Object) {
		return Object_to_uint(val.object);
	} else {
		return primitive_to_uint(val);
	}
}

jfl_noinline
inline Number any_to_Number(Any val){
	if (val.tag == type_tag::Object) {
		return Object_to_Number(val.object);
	} else {
		return primitive_to_Number(val);
	}
}

jfl_noinline
inline String any_to_String(Any val){
	if (val.tag == type_tag::Object) {
		return Object_to_String(val.object);
	} else {
		return primitive_to_String(val);
	}
}
jfl_noinline
inline Function any_to_Function(Any val){
	if ( val.tag == type_tag::Null || val.tag == type_tag::Undefined ){
		return nullptr;
	}
	if ((uint8_t)val.tag & (uint8_t)type_tag::FunctionBit ){
		return *(Function*)&val;
	}
	raise_cast_error(any_class_of(val), &Class_Function);
}

jfl_noinline
inline Object* any_to_Class(Any val, const Class* class_){
	if ( val.tag == type_tag::Null || val.tag == type_tag::Undefined ){
		return nullptr;
	}
	if ( val.tag == type_tag::Object && ti_extends(class_of(val.object)->jfl_ti, class_) ){
		return val.object;
	}
	raise_cast_error(any_class_of(val), class_);
}

jfl_noinline
inline Interface any_to_Interface(Any val, const Class* iface){
	if (val.tag == type_tag::Null || val.tag == type_tag::Undefined) {
		return nullptr;
	}
	const void* impl = nullptr;
	if (val.tag == type_tag::Object && ti_get_implement(class_of(val.object)->jfl_ti, iface, &impl)) {
		return {val.object, impl};
	}
	raise_cast_error(any_class_of(val), iface);
}

jfl_noinline
inline Any any_to_dyn(Any val, const Class* type) {
	jfl_notimpl;
}

}
