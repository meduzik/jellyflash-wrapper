#pragma once

#include <jfl/types.h>
#include "application.h"
#include <fl/flash/display/LoaderInfo.h>
#include <fl/flash/display3D/Context3D.h>

namespace fl::flash::display{
struct Stage;
struct DisplayObject;
}

namespace fl::flash::utils {
struct Timer;
}

namespace jfl{

struct TimerInfo {
	double delay;
	double next;

	TimerInfo(double delay, double timestamp) :
		delay(delay),
		next(timestamp + delay) {
	}
};

enum class MouseButtonID {
	Left,
	Right,
	Middle,
	X4,
	X5
};

enum class MouseButtonAction {
	Down,
	Up
};

class ApplicationBase: public Application {
public:
	void initialize(ApplicationDescriptor* descriptor);
	void start(std::string_view params_string);
	
	void loop();
	void loopEvent();

	void setMainSprite(fl::flash::display::DisplayObject*);
	fl::flash::display::Stage* getStage();

	void createTimer(fl::flash::utils::Timer* timer, double delay);
	void destroyTimer(fl::flash::utils::Timer* timer);

	void requestContext3D();

	virtual void processEvents() = 0;
	virtual bool shouldQuit() = 0;
	virtual void present() = 0;
	virtual void queryStageDimensions(int* width, int* height, double* scaleFactor) = 0;

	void onResize(int width, int height, double scaleFactor);
	void onMouseMove(double x, double y);
	void onMouseButton(double x, double y, MouseButtonID id, MouseButtonAction action);
	void onMouseWheel(double x, double y, double delta);
protected:
	virtual void init(ApplicationDescriptor* descriptor) = 0;

	
private:
	gc_ptr<fl::flash::display::Stage> stage;
	fl::flash::display::LoaderInfo* loaderInfo;
	std::unordered_map< gc_ptr<fl::flash::utils::Timer>, TimerInfo> timers;
	fl::flash::display3D::Context3D* context3D = nullptr;
};


}

