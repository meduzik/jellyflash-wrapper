#pragma once

#include <jfl/types.h>

namespace fl::flash::display{
struct Stage;
struct DisplayObject;
}

namespace fl::flash::utils {
struct Timer;
}

namespace jfl{

extern jfl::String ApplicationPackageID; 

class Application{
public:
	virtual void start(std::string_view params_string) = 0;
	virtual void loop() = 0;
	virtual void loopEvent() = 0;

	virtual void present() = 0;

	virtual void setMainSprite(fl::flash::display::DisplayObject*) = 0;
	virtual fl::flash::display::Stage* getStage() = 0;

	virtual void createTimer(fl::flash::utils::Timer* timer, double delay) = 0;
	virtual void destroyTimer(fl::flash::utils::Timer* timer) = 0;

	virtual void requestContext3D() = 0;

	virtual double getTimer() = 0;
};

struct ApplicationDescriptor{
	int32_t initialWidth, initialHeight;
};

Application* InitializeApplication(ApplicationDescriptor* descriptor);
Application* GetApplication();

}

