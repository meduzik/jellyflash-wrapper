#pragma once
#include <jfl/std.h>

namespace jfl{

// Obtains a global VM lock.
// Only one thread can interact with the vm at a time.
std::lock_guard<std::mutex> UnsafeLockVM();

// Schedules execution of a function the next time control flow returns back to the runtime
// All functions scheduled to run on the main loop are serialized, so if you post multiple
// functions from the same thread, they will be executed in this exact order.
void PostMainLoopTask(std::function<void()>&& func);

// Runs the function on main loop and returns only when the function completes
void WaitMainLoopTask(std::function<void()>&& func);

// Schedules execution of a function in a background worker thread
// If multithreading isn't supported, this task is executed on the main thread
// and will introduce UI delay.
void RunAsyncTask(std::function<void()>&& func);

void ProcessMainLoop();

}