#pragma once

namespace jfl{

Any GenericFuncOffsetBase(Object* object, RestParams params);

// This heavily assumes little endian
struct alignas(Object) Function {
	uint32_t offset;
	Object* object;

	Function() = default;

	Function(nullptr_t) :
		offset((uint32_t)type_tag::Null) {
	}

	Function(Object* object, GenericFunc func);

	bool has_function() const {
		return offset != (uint32_t)type_tag::Null;
	}

	Any invoke(RestParams* params);
};

#define JFL_PREPARE_CLOSURE(Ty, ar, ar_embed) \
	if ( (ar) == (ar_embed) ){ \
		ar = (Ty*)jfl::gc_new<Ty>();\
		new(ar)Ty(*ar_embed); \
	} \

}