#pragma once
#include <cstdlib>

#include "config.h"

#if defined(JFL_COMPILER_MSVC)
	#define jfl_notimpl jfl::raise_notimpl(__FILE__ " " __FUNCSIG__)
	#define jfl_unreachable jfl::raise_unreachable(__FUNCSIG__)
	#define jfl_noinline __declspec(noinline)
	#define jfl_unlikely(c) c
	#define JFL_FORCEINLINE inline
	#define JFL_INLINE inline
#elif defined(JFL_COMPILER_CLANG)
	#define jfl_notimpl_proxy() jfl::raise_notimpl("unknown")
	#define jfl_notimpl jfl_notimpl_proxy()
	#define jfl_unreachable jfl::raise_unreachable("unknown")
	#define jfl_noinline __attribute__((noinline))
	#define jfl_unlikely(c) c
	#define JFL_FORCEINLINE __attribute__((always_inline)) inline
	#define JFL_INLINE inline
#else
	#error Cannot define basic features
#endif


#define JFL_ARGMINCHECK(given, expected) if ( given < expected ) jfl::raise_arg_underflow(given, expected);
// turns out flash does not check for extra arguments
#define JFL_ARGMAXCHECK(given, expected) //if ( given > expected ) jfl::raise_arg_overflow(given, expected);

#define JFL_RAISE_NPE() jfl::raise_npe()
#define JFL_CHECKNULL(cond) if ( jfl_unlikely(cond) ) JFL_RAISE_NPE()
#define JFL_CHECKNULLTAG(tag) if ( jfl_unlikely(jfl::is_null_tag(tag)) ) JFL_RAISE_NPE()