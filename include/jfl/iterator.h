#pragma once
#include "types.h"

namespace jfl{

struct Iterator {
	Boolean (*next) (Iterator* iterator, Any* key, Any* value);

	alignas(std::max_align_t)
	uint8_t payload[4 * 8];
};

void null_iter(Iterator* iter);

template<class T>
struct I {
	Object* o;
	const T* v;

	I() = default;

	I(nullptr_t) :
		o(nullptr) {
	}

	I(Interface iface) :
		o(iface.o),
		v((const T*)iface.v) {
	}

	I(const T* v, Object* o) :
		o(o),
		v(v)
	{
	}

	operator Interface() {
		return { o, v };
	}
};

}
