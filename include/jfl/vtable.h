#pragma once

#include "types.h"

namespace jfl{

struct VTable;

inline char StaticObjectHeaderMarker = 0;

struct static_object_header_t{};
inline constexpr static_object_header_t static_object;

struct ObjectHeader {
	int refs;
	void* gcptr;

	constexpr ObjectHeader():
		refs(0),
		gcptr(nullptr)
	{
	}

	constexpr ObjectHeader(static_object_header_t):
		refs(0),
		gcptr(&StaticObjectHeaderMarker)
	{
	}
};

struct TypeInfo{
	// number of Class entries in the values array
	// corresponding to the inheritance hierarchy
	UInt num_classes;
	// (1 << exp_ifaces) entries is a hash table 
	// for interfaces implemented by the type
	UInt exp_ifaces;
	// +----                         
	// | Class* base_class           <|
	// +----                          |
	// | Class* derived_class_1       |
	// +----                          |
	// | Class* derived_class_2       | (total num_classes entries)
	// +----                          |
	// | ...                          |
	// +----                          |
	// | Class* most_derived_class   <|
	// +----                         
	// | null                        <|
	// +----                          | hash table of 2^exp_ifaces entries
	// | Class* interface_2           | uses open addressing with interface->offset
	// +----                          | as a base address, and interface->shift
	// | null                         | as a shift
	// +----                          |
	// | Class* interface_1          <|
	// +----
	// | null                        <|
	// +----                          | pointers to actual implementations
	// | Interface2* interface_2      | 
	// +----                          | 
	// | null                         | 
	// +----                          |
	// | Interface1* interface_1     <|
	// +----

	const void** values;
};

enum dynamic_op: int{
	get,
	set,
	in,
	del,
	value_of,
	to_string
};

using GCFunc = void(Object*,void*);
using FinalizeFunc = void(Object*);
using DynamicFunc = Boolean(Object*,Any*,Any*,dynamic_op);
using IterFunc = Boolean(Object*,Iterator*);
using InvokeFunc = GenericFunc;
using CreateFunc = GenericFunc;

struct VTableHeader{
	// the table of classes and interfaces in the inheritance chain
	const TypeInfo* ti;

	// full size of an object
	size_t size;

	// the function is used for marking/collecting this object
	// the exact interface is TBD
	GCFunc* gc;

	FinalizeFunc* finalize;

	// reflects the object to dynamically access its properties
	// op controls the operation:
	//  0) read object[key]
	//  1) write object[key] = value
	//  2) check if object[key] exists
	//  3) delete object[key]
	//  4) invoke valueOf and set *value to the returned value (not implemented)
	//  5) invoke toString and set *value to the returned value (not implemented)
	DynamicFunc *dynamic;
	// starts iterating over the object
	IterFunc* iterate;
};

struct VTable{
	VTableHeader jfl_vth;
};

extern const TypeInfo MethodClosure_TI;
extern const TypeInfo StringObject_TI;

}