#pragma once

#include "types.h"

namespace jfl{


struct Interface {
	Object* o;
	const void* v;
	
	Interface() = default;

	Interface(nullptr_t) :
		o(nullptr) {
	}

	Interface(Object* o, const void* v) :
		o(o),
		v(v) {
	}

	operator Object*() {
		return o;
	}
};



}