#pragma once
#include "types.h"

namespace jfl{

[[noreturn]]
void raise_cast_error(const Class* from_type, const Class* to_type);

[[noreturn]]
void raise_set_error(const Class* from_type, Any key);

[[noreturn]]
void raise_get_error(const Class* from_type, Any key);

[[noreturn]]
void raise_del_error(const Class* from_type, Any key);

[[noreturn]]
void raise_in_error(const Class* from_type, Any key);

[[noreturn]]
void raise_iter_error(const Class* type);

[[noreturn]]
void raise_call_error(const Class* type);

[[noreturn]]
void raise_system_error(int errorcode, String message);

[[noreturn]]
void raise_npe();

[[noreturn]]
void raise_npe(const char* file, int line);

[[noreturn]]
void raise_nullarg();

[[noreturn]]
void exc_throw(Any exc);

[[noreturn]]
void raise_arg_underflow(size_t given, size_t expected);

[[noreturn]]
void raise_arg_overflow(size_t given, size_t expected);

[[noreturn]]
void raise_message(std::string_view message);

[[noreturn]]
void raise_readonly(std::string_view property);

[[noreturn]]
void raise_writeonly(std::string_view property);

[[noreturn]]
void raise_notimpl(const char* location);

[[noreturn]]
void raise_unreachable(const char* location);

[[noreturn]]
void exc_resume();

Any exc_catchpad();

}