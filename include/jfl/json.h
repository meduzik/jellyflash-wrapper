#pragma once
#include "any.h"
#include "memory/Memory.h"

namespace jfl{

Any JSONDecode(std::string_view data);
void JSONEncode(Any value, arena_string& data);

}

