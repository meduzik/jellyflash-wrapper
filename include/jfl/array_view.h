#pragma once
#include "types.h"

namespace jfl{

template<class T>
class array_view {
public:
	using iterator = const T *;
	using const_iterator = const T *;
	using size_type = size_t;
	using reverse_iterator = std::reverse_iterator<iterator>;

	template<class U>
	friend class array_view;

	array_view(nullptr_t)
		: _size(0)
		, ptr(nullptr) {
	}

	array_view(const T* ptr, size_t size)
		: _size(size)
		, ptr(ptr) {
	}

	array_view(const T* begin, const T* end)
		: ptr(begin)
		, _size(end - begin) {
	}

	array_view(const std::initializer_list<T>& vals)
		: ptr(vals.begin())
		, _size(vals.size()) {
	}

	template<class A>
	array_view(const std::vector<T, A>& vec)
		: ptr(vec.data())
		, _size(vec.size()) {
	}

	template<size_t N>
	array_view(const std::array<T, N>& array)
		: ptr(array.data())
		, _size(N) {
	}

	template<size_t N>
	array_view(const T(&arr)[N])
		: _size(N)
		, ptr(arr) {
	}

	array_view(const T& val)
		: ptr(&val)
		, _size(1) {
	}

	template<typename TIn, typename = std::enable_if_t< std::is_convertible_v<TIn*, T*> > >
	array_view(const array_view<TIn>& other)
		: ptr(other.ptr)
		, _size(other.size) {
	}

	size_t size() const {
		return _size;
	}

	const T* data() const {
		return ptr;
	}

	const T& get(size_t index) const {
		return ptr[index];
	}

	const T& operator[](size_t index) const {
		return get(index);
	}

	iterator begin() const {
		return data();
	}

	iterator end() const {
		return data() + size();
	}

	array_view<T> slice(size_t begin, size_t end) const {
		return { ptr + begin, end - begin };
	}
private:
	const T* ptr;
	size_t _size;
};


}
