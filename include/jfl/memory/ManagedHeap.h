#pragma once

#include "Memory.h"
#include <jfl/any.h>

namespace jfl{

JFL_ALLOCATOR
void* gc_allocate(size_t size);

template<class T>
T* gc_new() {
	T* val = (T*)gc_allocate(sizeof(T));
	new(val) T();
	return val;
}

struct GCRoot {
	GCRoot* prev;
	GCRoot* next;
	void* udata;
	void(*gcfunc)(void* udata);

	constexpr GCRoot():
		prev(),
		next(),
		udata(),
		gcfunc()
	{}

	constexpr GCRoot(void(*gcfunc)(void* udata), void* udata):
		prev(nullptr),
		next(nullptr),
		udata(udata),
		gcfunc(gcfunc)
	{
	}
};

inline void gc_visit_any(Any any);

void gc_visit_dynamic(Dynamic& dynamic);

void gc_visit_weak_dynamic(Dynamic& dynamic);

void gc_visit(Object* object);

inline void gc_visit_rest(RestParams* rest){
	if (rest->tag == type_tag::Rest){
		for(size_t i = 0; i < rest->size; i++){
			gc_visit_any(rest->ptr[i]);
		}
	}else{
		gc_visit(rest->object);
	}
}

inline void gc_visit_any(Any any){
	switch(any.tag){
	case type_tag::Object:
	case type_tag::BigString: {
		gc_visit(any.object);
	}break;
	case type_tag::NumberBox: {
		gc_visit((Object*)any.nval);
	} break;
	case type_tag::Rest:{
		gc_visit_rest(any.rest);
	}break;
	default:{
		if ((uint8_t)any.tag & (uint8_t)type_tag::FunctionBit) {
			gc_visit(any.object);
		} else {
			// do nothing
		}
	}break;
	}
}

inline void gc_visit_string(String string){
	if (string.tag == type_tag::BigString){
		gc_visit((Object*)string.sb.object);
	}
}

inline void gc_visit_function(Function func) {
	if (func.has_function()) {
		gc_visit((Object*)func.object);
	}
}

// Register a GC root.
void gc_add_root(GCRoot* root);
// Unregister a GC root.
void gc_remove_root(GCRoot* root);

void gc_hint(double threshold);
void gc_perform();

template<class T>
class gc_ptr{
public:
	gc_ptr():
		gc_ptr(nullptr)
	{
	}

	gc_ptr(nullptr_t):
		root(_visit, this),
		t(nullptr)
	{
	}

	gc_ptr(T* t):
		root(_visit, this),
		t(t)
	{
		if (t){
			gc_add_root(&root);
		}
	}

	gc_ptr(const gc_ptr<T>& other):gc_ptr(other.t){
	}

	gc_ptr(gc_ptr<T>&& other): gc_ptr(other.t) {
	}

	gc_ptr<T>& operator=(gc_ptr<T>&& other){
		reset(other.t);
		return *this;
	}

	gc_ptr<T>& operator=(const gc_ptr<T>& other) {
		reset(other.t);
		return *this;
	}

	gc_ptr<T>& operator=(T* val) {
		reset(val);
		return *this;
	}

	~gc_ptr(){
		reset();
	}

	void reset(T* new_t = nullptr){
		if ((t == nullptr) != (new_t == nullptr)) {
			if (new_t) {
				gc_add_root(&root);
			} else {
				gc_remove_root(&root);
			}
		}
		t = new_t;
	}

	bool operator==(const gc_ptr<T>& other){
		return t == other.t;
	}

	bool operator!=(const gc_ptr<T>& other) {
		return t != other.t;
	}

	operator T*() const{
		return t;
	}

	T* operator->() const{
		return t;
	}

	T* get() const{
		return t;
	}
private:
	static void _visit(void* ud){
		gc_ptr<T>* self = (gc_ptr<T>*)ud;
		if (self->t){
			gc_visit((Object*)self->t);
		}
	}

	GCRoot root;
	T* t;
};

}

namespace std {
	template <class T>
	struct hash< jfl::gc_ptr<T> > {
		size_t operator()(const jfl::gc_ptr<T>& x) const {
			return hash<T*>()(x.get());
		}
	};
}


