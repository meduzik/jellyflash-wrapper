#pragma once
#include <jfl/types.h>

#if !defined(NDEBUG)
#  define JFL_PROFILE_MEMORY
#endif
// #define JFL_MEMORY_CANARY

#if defined(_MSC_VER) && defined(JFL_PROFILE_MEMORY)
	#include <VSCustomNativeHeapEtwProvider.h>
	#define JFL_ALLOCATOR __declspec(allocator)
#else
	#define JFL_ALLOCATOR
#endif

namespace jfl{

JFL_ALLOCATOR
void* unmanaged_allocate(size_t size);
void unmanaged_free(void* ptr, size_t size);

struct ArenaPool;
struct ArenaLargeBlock;

struct ArenaControlBlock{
	uint8_t* cur;
	uint8_t* end;
};

struct Arena{
	Arena(nullptr_t);
	Arena();
	~Arena();

	JFL_ALLOCATOR
	void* allocate(size_t size);

private:
	Arena* next;
	int mylevel, maxlevel;
	ArenaPool* mypool;
	ArenaLargeBlock* myblock;
	ArenaControlBlock reset;
};

template<class T>
struct ArenaAllocator : public std::allocator<T> {
	typedef size_t size_type;
	typedef T* pointer;
	typedef const T* const_pointer;

	template<typename _Tp1>
	struct rebind {
		typedef ArenaAllocator<_Tp1> other;
	};

	pointer allocate(size_type n, const void *hint = 0) {
		return (pointer)arena->allocate(sizeof(T) * n);
	}

	void deallocate(pointer p, size_type n) {
	}

	ArenaAllocator(Arena& arena) throw() : arena(&arena) {}
	ArenaAllocator(const ArenaAllocator &a) throw() : arena(a.arena) {}
	template <class U>
	ArenaAllocator(const ArenaAllocator<U> &a) throw() : arena(a.arena) {}
	~ArenaAllocator() throw() {}
private:
	template<class U>
	friend struct ArenaAllocator;

	Arena* arena;
};

template<class T>
using arena_vec = std::vector< T, ArenaAllocator<T> >;

using arena_string = std::basic_string< char, std::char_traits<char>, ArenaAllocator< char > >;


template<class T>
struct UnmanagedAllocator : public std::allocator<T> {
	typedef size_t size_type;
	typedef T* pointer;
	typedef const T* const_pointer;

	template<typename _Tp1>
	struct rebind {
		typedef UnmanagedAllocator<_Tp1> other;
	};

	pointer allocate(size_type n, const void *hint = 0) {
		return (pointer)unmanaged_allocate(sizeof(T) * n);
	}

	void deallocate(pointer p, size_type n) {
		unmanaged_free(p, sizeof(T) * n);
	}

	UnmanagedAllocator() throw()  {}
	UnmanagedAllocator(const UnmanagedAllocator &a) throw() {}
	template <class U>
	UnmanagedAllocator(const UnmanagedAllocator<U> &a) throw() {}
	~UnmanagedAllocator() throw() {}
private:
	template<class U>
	friend struct UnmanagedAllocator;
};

template<class T>
struct unmanaged_deleter{
	void operator()(T* t){
		std::destroy_at(t);
		unmanaged_free(t, sizeof(T));
	}
};

template<class T>
using um_vec = std::vector< T, UnmanagedAllocator<T> >;

using um_string = std::basic_string< char, std::char_traits<char>, UnmanagedAllocator< char > >;

template<class T>
using um_unique_ptr = std::unique_ptr< T, unmanaged_deleter<T> >;

template<class K, class V, class Hash = std::hash<K>, class Eq = std::equal_to<K> >
using um_umap = std::unordered_map< K, V, Hash, Eq, UnmanagedAllocator< std::pair<const K, V> >>;

template<class K, class V, class Pred = std::less<K> >
using um_map = std::map< K, V, Pred, UnmanagedAllocator< std::pair<const K, V> >>;

uint8_t* GetScratchSpace(size_t size);

}

