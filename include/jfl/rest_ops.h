#pragma once
#include "any.h"
#include "rest_params.h"

namespace fl{
struct Array;
}

namespace jfl {

// this is needed to cast from initilizer_list to RestParams* (which we do often)
inline RestParams* params(RestParams&& params) {
	return (RestParams*)&params;
}

jfl_noinline
RestParams rest_slice(RestParams* params, UInt offset);

jfl_noinline
UInt rest_array_size(RestParams* params);

jfl_noinline
Any rest_array_get(RestParams* params, UInt idx);

jfl_noinline
fl::Array* rest_to_array(RestParams* params);

inline UInt rest_size(RestParams* params) {
	if (params->tag == type_tag::Object) {
		return rest_array_size(params);
	}
	return params->size;
}

inline Any rest_get(RestParams* params, UInt idx) {
	if (params->tag == type_tag::Object) {
		return rest_array_get(params, idx);
	}
	return params->ptr[idx];
}

}