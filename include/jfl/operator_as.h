#pragma once
#include "types.h"
#include "class.h"
#include "type_inspection.h"
#include "operator_is.h"
#include "operator_to.h"

namespace jfl {


inline Object* Object_as_Class(Object* val, const Class* class_) {
	if (!val) {
		return nullptr;
	}
	if (!ti_extends(val->jfl_vtable->jfl_vth.ti, class_)) {
		return nullptr;
	}
	return val;
}

inline Interface Object_as_Interface(Object* val, const Class* class_) {
	if (!val) {
		return nullptr;
	}
	const void* impl;
	if (!ti_get_implement(val->jfl_vtable->jfl_vth.ti, class_, &impl)) {
		return nullptr;
	}
	return { val, impl };
}



inline Any Number_as_uint(Number val){
	Number ipart;
	if ( modf(val, &ipart) != 0 || val < 0 || val >= (Number)std::numeric_limits<UInt>::max() ){
		return nullptr;
	}
	return (UInt)val;
}


inline Any any_as_Boolean(Any val){
	if ( val.tag == type_tag::Boolean ){
		return val;
	}
	return nullptr;
}

inline Any any_as_Number(Any val) {
	if (
		val.tag == type_tag::NumberBox
		||
		val.tag == type_tag::Float
		|| 
		val.tag == type_tag::Int
		|| 
		val.tag == type_tag::UInt
	) {
		return val;
	}
	return nullptr;
}

inline Any any_as_uint(Any val) {
	if ( any_is_uint(val) ){
		return any_to_uint(val);
	}else{
		return nullptr;
	}
}

inline Any any_as_int(Any val) {
	if (any_is_int(val)) {
		return any_to_int(val);
	} else {
		return nullptr;
	}
}


inline String any_as_String(Any val) {
	if ( val.tag <= type_tag::StringEnd ) {
		return *(String*)&val;
	}
	return nullptr;
}

inline Function any_as_Function(Any val) {
	if ((uint8_t)val.tag & (uint8_t)type_tag::FunctionBit) {
		return *(Function*)&val;
	}
	return nullptr;
}

inline Object* any_as_Class(Any val, const Class* class_){
	if ( val.tag != type_tag::Object ){
		return nullptr;
	}
	if ( ti_extends(class_of(val.object)->jfl_ti, class_) ){
		return val.object;
	}
	return nullptr;
}

inline Interface any_as_Interface(Any val, const Class* iface){
	if (val.tag != type_tag::Object) {
		return nullptr;
	}
	const void* impl = nullptr;
	if ( ti_get_implement(class_of(val.object)->jfl_ti, iface, &impl) ){
		return {val.object, impl};
	}
	return nullptr;
}



}
