#pragma once
#include <jfl/string.h>

namespace fl::flash::media {
struct SoundTransform;
struct SoundChannel;
struct Sound;
}

namespace jfl::media {

struct SoundImpl;
struct SoundChannelImpl;

SoundImpl* SoundImplCreate(fl::flash::media::Sound* sound);
void SoundImplDecode(SoundImpl* impl, const uint8_t* data, size_t len);
void SoundImplDestroy(SoundImpl* impl);

SoundChannelImpl* SoundImplPlay(SoundImpl* impl, fl::flash::media::SoundChannel* channel, double position, fl::flash::media::SoundTransform* transform);
void SoundChannelImplDestroy(SoundChannelImpl* impl);
double SoundChannelImplGetPosition(SoundChannelImpl* impl);
void SoundChannelImplUpdateTransform(SoundChannelImpl* impl, fl::flash::media::SoundTransform* transform);

void SoundSetGlobalVolume(double value);

}
