#pragma once

#include "types.h"

namespace jfl{

inline UInt ashr(UInt x, UInt shift){
	shift = shift & 31;
	return (UInt)((Int)x >> (Int)shift);
}

inline UInt lshr(UInt x, UInt shift){
	shift = shift & 31;
	return (x >> shift);
}

inline Int mod(Int x, Int y){
	return x % y;
}
inline UInt mod(UInt x, UInt y){
	return x % y;
}
inline Int mod(Int x, UInt y) {
	return x % (Int)y;
}
inline Int mod(UInt x, Int y) {
	return (Int)x % y;
}
inline Number mod(Number x, Number y){
	return fmod(x, y);
}
inline Number mod(Int x, Number y) {
	return fmod(x, y);
}
inline Number mod(UInt x, Number y) {
	return fmod(x, y);
}
inline Number mod(Number x, Int y) {
	return fmod(x, y);
}
inline Number mod(Number x, UInt y) {
	return fmod(x, y);
}
}
