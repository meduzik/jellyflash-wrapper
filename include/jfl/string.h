#pragma once
#include "types.h"
#include "vtable.h"
#include "object.h"

namespace jfl{

struct alignas(Object) StringSmall {
	uint8_t length;
	uint8_t value[7];
};

struct StringObject_VT{
	VTableHeader jfl_vth;
};


struct alignas(Object) StringObject{
	static const StringObject_VT jfl_VT;

	const StringObject_VT* jfl_vtable;
	ObjectHeader jfl_header;

	UInt length;
	const uint8_t* data;

	static StringObject* New(UInt length);
};

struct alignas(Object) StringBig {
	type_tag tag;
	uint8_t padding[3];
	const StringObject* object;

	constexpr StringBig(const StringObject* object) :
		tag(type_tag::BigString),
		padding{},
		object(object)
	{
	}
};

struct alignas(Object) String {
	union {
		type_tag tag;
		StringSmall ss;
		StringBig sb;
	};

	String() {}

	constexpr String(nullptr_t):
		tag(type_tag::Null)
	{
	}

	constexpr String(const StringObject* object) :
		sb{object} {
	}

	constexpr String(StringSmall ss) :
		ss(ss){
	}

	std::pair<const uint8_t*, size_t> to_view() const {
		if ( tag == type_tag::Null ){
			return {(const uint8_t*)"null", 4};
		}else if ( tag <= type_tag::SmallStringEnd ){
			return {ss.value, (size_t)ss.length};
		}else{
			return {sb.object->data, sb.object->length};
		}
	}

	std::string_view to_string_view() const{
		auto [ptr, len] = to_view();
		return {(const char*)ptr, len};
	}

	static String Make(const uint8_t* ptr, UInt len);
	static String Make(std::string_view string){
		return Make((const uint8_t*)string.data(), (UInt)string.size());
	}

	void ensure_not_null(){
		if ( tag == type_tag::Null ){
			*this = StringSmall{4, "null"};
		}
	}
};

inline constexpr String StringEmpty{ StringSmall{0, "\0"} };

String str_concat(String lhs, String rhs);

}

