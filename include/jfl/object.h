#pragma once

#include "vtable.h"

namespace jfl{

struct alignas(8) Object {
	const VTable* jfl_vtable;
	ObjectHeader jfl_header;
};

inline const Class* class_of(Object* object){
	return (const Class*)object->jfl_vtable->jfl_vth.ti->values[object->jfl_vtable->jfl_vth.ti->num_classes - 1];
}


}
