#pragma once
#include <algorithm>
#include "types.h"

namespace jfl{

Boolean any_eq(Any lhs, Any rhs);
Boolean any_seq(Any lhs, Any rhs);

jfl_noinline
inline Boolean func_eq(Function lhs, Function rhs){
	if (lhs.has_function() != rhs.has_function()){
		return false;
	}
	if (!lhs.has_function()) {
		return true;
	}
	return (
		lhs.has_function()
		&& 
		lhs.object == rhs.object 
		&& 
		lhs.offset == rhs.offset
	);
}

jfl_noinline
inline Boolean str_eq(String lhs, String rhs){
	if ( lhs.tag != rhs.tag ){
		return false;
	}
	if ( lhs.tag == type_tag::Null ){
		return true;
	}
	if ( lhs.tag <= type_tag::SmallStringEnd ){
		return !memcmp(lhs.ss.value, rhs.ss.value, (size_t)lhs.tag);
	}else{
		UInt lhs_length = lhs.sb.object->length;
		UInt rhs_length = rhs.sb.object->length;
		if ( lhs_length != rhs_length ){
			return false;
		}
		return !memcmp(lhs.sb.object->data, rhs.sb.object->data, lhs_length);
	}
}

jfl_noinline
inline Boolean str_lt(String lhs, String rhs){
	if ( lhs.tag == type_tag::Null && rhs.tag == type_tag::Null ){
		return false;
	}
	if ( lhs.tag == type_tag::Null ){
		return true;
	}
	if ( rhs.tag == type_tag::Null ) {
		return false;
	}
	auto view1 = lhs.to_view();
	auto view2 = rhs.to_view();
	auto min_len = std::min(view1.second, view2.second);
	int cmp = memcmp(view1.first, view2.first, min_len);
	if ( cmp == 0 ){
		return view1.second < view2.second;
	}else{
		return cmp < 0;
	}
}


}