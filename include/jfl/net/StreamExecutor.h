#pragma once
#include <jfl/string.h>

namespace fl::flash::net{
struct URLRequest;
struct URLStream;
};

namespace jfl{

class IStreamExecutor{
public:
	virtual ~IStreamExecutor() {}
	virtual void detach() = 0;
};

enum class PathBase{
	AppPackage,
	AppStorage
};

std::shared_ptr<IStreamExecutor> CreateLocalFileStream(fl::flash::net::URLStream* target, PathBase base, std::string_view path);
std::shared_ptr<IStreamExecutor> CreateHttpStream(fl::flash::net::URLStream* target, fl::flash::net::URLRequest* request);


}

