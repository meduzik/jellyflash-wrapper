#pragma once

#include <cstdlib>
#include <cstring>

#include "macro.h"
#include "math/murmur.h"

#include "types.h"
#include "string.h"
#include "function.h"
#include "interface.h"
#include "rest_params.h"
#include "error.h"

namespace jfl{

struct alignas(Object) NumberBox {
	const VTable* jfl_vtable;
	jfl::ObjectHeader jfl_header;

	Number value;

	static NumberBox* Make(Number value);
};

struct alignas(8) Any {
	type_tag tag;
	union {
		Boolean bval;
		Int ival;
		UInt uval;
		float fval;
		NumberBox* nval;
		Object* object;
		RestParams* rest;
	};

	Any() = default;
	Any(const Any&) = default;
	Any(Any&&) = default;
	Any& operator=(const Any&) = default;
	Any& operator=(Any&&) = default;

	Any(nullptr_t) :
		tag(type_tag::Null) {
	}

	Any(undefined_t) :
		tag(type_tag::Undefined) {
	}

	Any(Boolean val) :
		tag(type_tag::Boolean),
		bval(val) {
	}

	// do not accidently convert any other type to Any
	Any(const void*) = delete;

	Any(Int val) :
		tag(type_tag::Int),
		ival(val) {
	}

	Any(UInt val) :
		tag(type_tag::UInt),
		uval(val) {
	}

	jfl_noinline
	Any(Number val) {
		float fval = (float)val;
		if (std::isnan(val) || (Number)fval == val) {
			tag = type_tag::Float;
			this->fval = fval;
		} else {
			tag = type_tag::NumberBox;
			nval = NumberBox::Make(val);
		}
	}

	Any(String string) {
		memcpy(this, &string, sizeof(Any));
	}

	Any(Function func) {
		memcpy(this, &func, sizeof(Any));
	}

	Any(Object* object) :
		tag(object ? type_tag::Object : type_tag::Null),
		object(object) 
	{
	}

	Any(RestParams* rest) :
		tag(type_tag::Rest),
		rest(rest)
	{
	}

	Any(Interface iface) :
		tag(iface.o ? type_tag::Object : type_tag::Null),
		object(iface.o)
	{
	}
};

inline bool is_null_tag(type_tag tag){
	return tag == type_tag::Undefined ||  tag == type_tag::Null;
}

std::unordered_map<std::string_view, Any(*)()>& get_definition_registry();
void register_definition(std::string_view name, Any(*accessor)());

inline Any any_undef_to_null(Any val){
	if (val.tag == type_tag::Undefined) {
		return nullptr;
	}
	return val;
}

const Class* any_class_of(Any val);

jfl_noinline
Any any_get(Any object, Any key);
jfl_noinline
void any_set(Any object, Any key, Any value);
jfl_noinline
void any_del(Any object, Any key);
jfl_noinline
Boolean any_in(Any object, Any key);

template<class A, class T>
jfl_noinline
void array_set(A array, Int index, T object) {
	any_set(Any(array), Any(index), Any(object));
}

template<class A, class T>
jfl_noinline
void named_field_set(A array, std::string_view index, T object) {
	any_set(Any(array), Any(String::Make(index)), Any(object));
}

jfl_noinline
void any_iter(Any val, Iterator* iter);

jfl_noinline
Boolean any_seq(Any lhs, Any rhs);

jfl_noinline
Boolean any_eq(Any lhs, Any rhs);

inline size_t hash_ptr(const void* ptr) {
	return (size_t)ptr;
}

jfl_noinline
inline size_t any_hash(Any any) {
	switch (any.tag) {
	case type_tag::Null: return 0;
	case type_tag::Undefined: return 1;
	case type_tag::Boolean: return any.bval + 11417;
	case type_tag::Int: return (size_t)any.ival;
	case type_tag::UInt: return (size_t)any.uval;
	case type_tag::Float: {
		union pun_t {
			float number;
			Int ival;
		} pun;
		pun.number = any.fval;
		return pun.ival;
	} break;
	case type_tag::NumberBox: {
		union pun_t {
			Number number;
			Int halfs[2];
		} pun;
		pun.number = any.nval->value;
		return pun.halfs[0] ^ pun.halfs[1];
	}break;
	case type_tag::Rest: {
		return hash_ptr(any.object);
	}break;
	case type_tag::Object: {
		return hash_ptr(any.object);
	}break;
	default: {
		if (any.tag <= type_tag::StringEnd) {
			auto view = ((String*)&any)->to_string_view();
			return murmur3_32((const uint8_t*)view.data(), view.size(), 0xAFCE5F03);
		} else if ((uint8_t)any.tag & (uint8_t)type_tag::FunctionBit) {
			Function* func = (Function*)&any;
			return func->offset ^ hash_ptr(func->object);
		} else {
			jfl_unreachable;
		}
	}break;
	}
}


}
