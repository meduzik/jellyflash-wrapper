#pragma once
#include <jfl/config.h>

#if defined(JFL_PLATFORM_EM)
	#include <GLES2/gl2.h>
	#include <GLES2/gl2ext.h>
	#include <GLES2/gl2platform.h>
	#undef GL_VAO_REQUIRED
	#define JFL_OPENGL_WEBGL
	#define JFL_USES_GLFW
#elif defined(JFL_PLATFORM_ANDROID)
	#include <GLES2/gl2.h>
	#include <GLES2/gl2ext.h>
	#include <GLES2/gl2platform.h>
	#undef GL_VAO_REQUIRED
	#define JFL_OPENGL_GLES
#elif defined(JFL_PLATFORM_WIN)
	#include <glad/glad.h>
	#define GL_VAO_REQUIRED
	#undef OPENGL_ES
	#define JFL_USE_GLAD
	#define OPENGL_DESKTOP
	#define JFL_USES_GLFW
#else
	#error Cannot determine OpenGL mode
#endif

