#pragma once
#include <jfl/common.h>


namespace jfl::render{

enum class BufferUsage{
	Static,
	Dynamic
};

enum class BlendFactor{
	DestAlpha,
	DestColor,
	One,
	OneMinusDestAlpha,
	OneMinusDestColor,
	OneMinusSrcAlpha,
	OneMinusSrcColor,
	SrcAlpha,
	SrcColor,
	Zero
};

enum class ProgramType{
	Vertex,
	Fragment
};

enum class TriangleFace{
	Front,
	Back,
	FrontAndBack,
	None
};

enum class Wrap{
	Clamp,
	ClampURepeatV,
	Repeat,
	RepeatUClampV
};

enum class TextureFilter{
	Anisotropic16x,
	Anisotropic8x,
	Anisotropic4x,
	Anisotropic2x,
	Linear,
	Nearest
};

enum class MipFilter {
	Linear,
	Nearest,
	None
};

enum class BufferFormat{
	Bytes4,
	Float1,
	Float2,
	Float3,
	Float4
};

enum class ClearMask{
	Color = 1 << 0,
	Depth = 1 << 1,
	Stencil = 1 << 2
};

enum class CompareMode{
	Always,
	Equal,
	Greater,
	GreaterEqual,
	Less,
	LessEqual,
	Never,
	NotEqual
};

enum class TextureFormat{
	BGRA,
	BGR
};

enum class StencilAction{
	DecrementSaturate,
	DecrementWrap,
	IncrementSaturate,
	IncrementWrap,
	Invert,
	Keep,
	Set,
	Zero
};

class IndexBuffer{
public:
	virtual void dispose() = 0;
	virtual void upload(const void* data, int firstIndex, int numIndices) = 0;
};

class VertexBuffer{
public:
	virtual void dispose() = 0;
	virtual void upload(const void* data, int firstVertex, int numVertices) = 0;
	virtual size_t getBytesPerVertex() = 0;
};

class Program{
public:
	virtual void dispose() = 0;
	virtual void upload(jfl::array_view<uint8_t> vertex_data, jfl::array_view<uint8_t> fragment_data) = 0;
};

class TextureBase{
public:
	virtual void dispose() = 0;
	virtual ~TextureBase(){}
};

class Texture2D: public virtual TextureBase {
public:
	virtual void upload(int level, int width, int height, bool has_alpha, int stride, uint8_t* data) = 0;
	virtual void uploadPremultiply(int level, int width, int height, uint8_t* data) = 0;
	virtual void uploadExternalImage(int level, int width, int height, int imageID) = 0;
};

class TextureCube: public virtual TextureBase{
public:
};

class Context3D{
public:
	virtual void setWindowSize(int width, int height) = 0;
	virtual jfl::String getDriverInfo() = 0;
	virtual void setErrorChecking(bool value) = 0;
	virtual bool getErrorChecking() = 0;
	virtual void configureBackBuffer(int width, int height, bool depthAndStencil) = 0;

	virtual VertexBuffer* createVertexBuffer(int numVertices, int dataPerVertex, BufferUsage usage) = 0;
	virtual IndexBuffer* createIndexBuffer(int numIndices, BufferUsage usage) = 0;
	virtual Program* createProgram() = 0;

	virtual Texture2D* createTexture2D(int width, int height, TextureFormat format, bool optimizeForRender, bool mipmaps) = 0;

	virtual int getBackBufferWidth() = 0;
	virtual int getBackBufferHeight() = 0;

	virtual void clear(double red, double green, double blue, double alpha, double depth, uint32_t stencil, ClearMask mask) = 0;

	virtual void setDepthTest(bool write, CompareMode compare) = 0;
	virtual void drawTriangles(IndexBuffer* ibo, int firstIndex, int numTriangles) = 0;
	virtual void present() = 0;
	virtual void setBlendFactors(BlendFactor source, BlendFactor dest) = 0;
	virtual void setColorMask(bool red, bool green, bool blue, bool alpha) = 0;
	virtual void setCulling(TriangleFace face) = 0;

	virtual void setProgram(Program* program) = 0;
	virtual void setProgramConstant(ProgramType type, int reg, const double data[4]) = 0;

	virtual void setRenderToBackBuffer() = 0;
	virtual void setRenderToTexture(TextureBase* texture, bool depth_and_stencil, int antialias, int surface, int color_output) = 0;
	virtual void setSamplerStateAt(int sampler, Wrap wrap, TextureFilter texture_filter, MipFilter mip_filter) = 0;
	virtual void setScissor(int x, int y, int width, int height) = 0;
	virtual void unsetScissor() = 0;
	virtual void setStencilReference(uint32_t ref, uint32_t read_mask, uint32_t write_mask) = 0;
	virtual void setStencilActions(TriangleFace face, CompareMode compare, StencilAction pass, StencilAction depthFail, StencilAction fail) = 0;
	virtual void setTextureAt(int sampler, TextureBase* texture) = 0;
	virtual void setVertexBufferAt(int index, VertexBuffer* buffer, int buffer_offset, BufferFormat format) = 0;

};

Context3D* CreateContext3DImpl();

}
