#pragma once

#include "common.h"

#include <fl/String.h>
#include <fl/Array.h>
#include <fl/Class.h>
#include <fl/Function.h>
#include <fl/Boolean.h>
#include <fl/int.h>
#include <fl/uint.h>
#include <fl/Object.h>

