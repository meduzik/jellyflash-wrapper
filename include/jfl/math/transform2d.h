#pragma once

#include <jfl/std.h>
#include <jfl/types.h>

namespace jfl{

struct decomp2d{
	Number sx, sy, rotation, skew;

	decomp2d():
		sx(1),
		sy(1),
		rotation(0),
		skew(0)
	{
	}
};

struct matrix2d{
	Number a, c, tx;
	Number b, d, ty;

	matrix2d():
		a(1),
		c(0),
		tx(0),
		b(0),
		d(1),
		ty(0)
	{
	}

	void append(const matrix2d& other) {
		Number a1 = a * other.a + c * other.b;
		Number c1 = a * other.c + c * other.d;
		Number tx1 = a * other.tx + c * other.ty + tx;
		Number b1 = b * other.a + d * other.b;
		Number d1 = b * other.c + d * other.d;
		Number ty1 = b * other.tx + d * other.ty + ty;

		a = a1;
		c = c1;
		tx = tx1;
		b = b1;
		d = d1;
		ty = ty1;
	}

	void prepend(const matrix2d& other){
		matrix2d result = other;
		result.append(*this);
		*this = result;
	}
};



struct transform2d{
	static constexpr uint32_t FlagDecomposed = 0x1;
	static constexpr uint32_t FlagComposed = 0x2;

	decomp2d decomp;
	matrix2d matrix;
	uint32_t flags;

	transform2d():
		flags(FlagComposed | FlagDecomposed)
	{
	}

	void decompose(){
		if (flags & FlagDecomposed) {
			return;
		}
		_decompose();
		flags |= FlagDecomposed;
	}

	void recompose(){
		if (flags & FlagComposed){
			return;
		}
		_recompose();
		flags |= FlagComposed;
	}

	void reset_matrix(){
		flags = FlagDecomposed;
	}

	void reset_decomposition() {
		flags = FlagComposed;
	}

private:
	void _decompose();
	void _recompose();
};

}
