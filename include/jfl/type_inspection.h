#pragma once
#include "types.h"
#include "class.h"

namespace jfl{

// checks if type info belongs to a class extending given class
// class parameter must point to a class
inline Boolean ti_extends(const TypeInfo* ti, const Class* class_) {
	return class_->offset <= ti->num_classes && ti->values[class_->offset] == class_;
}

// checks if type info belongs to a class extending given interface
// class parameter must point to an interface
jfl_noinline
inline Boolean ti_implements(const TypeInfo* ti, const Class* iface) {
	UInt size = ti->exp_ifaces;
	UInt base_offset = ti->num_classes;
	UInt offset = iface->offset % (1 << size);
	while (true) {
		const void* value = ti->values[base_offset + offset];
		if (value == iface) {
			return true;
		} else if (value == nullptr) {
			return false;
		} else {
			offset = (offset + iface->shift) % (1 << size);
		}
	}
}

// checks if type info relates to a given class type
inline Boolean ti_inherits(const TypeInfo* ti, const Class* type) {
	if (type->kind == class_kind_t::kind_class) {
		return ti_extends(ti, type);
	} else if (type->kind == class_kind_t::kind_interface) {
		return ti_implements(ti, type);
	} else {
		return false;
	}
}

jfl_noinline
inline Boolean ti_get_implement(const TypeInfo* ti, const Class* iface, const void** impl) {
	UInt size = ti->exp_ifaces;
	UInt base_offset = ti->num_classes;
	UInt offset = iface->offset % (1 << size);
	while (true) {
		const void* value = ti->values[base_offset + offset];
		if (value == iface) {
			*impl = ti->values[base_offset + (1 << size) + offset];
			return true;
		} else if (value == nullptr) {
			return false;
		} else {
			offset = (offset + iface->shift) % (1 << size);
		}
	}
}


}