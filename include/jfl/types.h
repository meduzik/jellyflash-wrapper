#pragma once

#include "std.h"

namespace jfl{

enum class cmp_op_t: uint8_t{
	eq,
	neq,
	seq,
	sneq,
	lt,
	le,
	gt,
	ge
};

enum class type_tag : uint8_t {
	StringBegin = 0,
	SmallStringBegin = 0,
	SmallStringEnd = 7,
	BigString = SmallStringEnd + 1,
	StringEnd = BigString,
	Null,
	Undefined,
	Boolean,
	Int,
	UInt,
	NumberBox,
	Float,
	Object,
	Rest,
	FunctionBit = 0x80u,
};

// "any" type is a thing that can hold any primitive type
struct Any;

// special values
struct undefined_t{};
inline const undefined_t undefined{};

// primitive types
// Boolean
using Boolean = bool;
// Int
using Int = int32_t;
// UInt
using UInt = uint32_t;
// Number
using Number = double;
// String
struct String;
// Function
struct Function;


// reference types
// Object
struct Object;
// Interface
struct Interface;
template<class T> struct I;

// Class
struct Class;

// special types
// RestParams
struct RestParams;
// Iterator
struct Iterator;
// Dynamic
struct Dynamic;
// Function Callback
using GenericFunc = Any(Object*, RestParams*);

using GetFunc = Any(Object*);
using SetFunc = void(Object*,Any);

// Box
template<class T> struct Box;

extern const Class Class_Null;
extern const Class Class_Undefined;
extern const Class Class_Rest;
extern const Class Class_Any;
extern const Class Class_Object;
extern const Class Class_Boolean;
extern const Class Class_Int;
extern const Class Class_UInt;
extern const Class Class_Number;
extern const Class Class_String;
extern const Class Class_Function;
extern const Class Class_Class;
extern const Class Class_MethodClosure;

}



