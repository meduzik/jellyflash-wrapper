#pragma once
#include "types.h"
#include "vtable.h"
#include "memory.h"

namespace jfl{

template<class T>
struct Box{
	static const VTable jfl_VT;

	const VTable* jfl_vt;
	T payload;

	static Object* Wrap(T val) {
		Box<T>* object = jfl::gc_new< Box<T> >();
		object->jfl_vt = &jfl_VT;
		object->payload = val;
		return object;
	}
	
	static T Unwrap(Object* object){
		return ((Box<T>*)object)->payload;
	}
};

extern template struct Box<Any>;
extern template struct Box<Boolean>;
extern template struct Box<Int>;
extern template struct Box<UInt>;
extern template struct Box<Number>;
extern template struct Box<String>;
extern template struct Box<Function>;

}