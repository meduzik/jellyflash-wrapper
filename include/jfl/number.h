#pragma once

#include "types.h"

namespace jfl{

inline const Number NaN = std::numeric_limits<Number>::quiet_NaN();
inline const Number PositiveInfinity = std::numeric_limits<Number>::infinity();
inline const Number NegativeInfinity = -std::numeric_limits<Number>::infinity();


}

