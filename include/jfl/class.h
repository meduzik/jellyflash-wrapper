#pragma once
#include "types.h"
#include "vtable.h"
#include "string.h"

namespace jfl{

enum class class_kind_t : Int{
	kind_interface,
	kind_class,
	kind_primitive
};

struct Class_VT{
	VTableHeader jfl_th;
};

struct Class {
	static const Class_VT jfl_VT;

	const Class_VT* jfl_vtable;
	ObjectHeader jfl_header;

	const TypeInfo* jfl_ti;

	String name;
	UInt offset;
	UInt shift;
	class_kind_t kind;
	GenericFunc* create;
	DynamicFunc* dynamic;
};


inline const Class* super_of(const Class* class_) {
	if (class_->offset > 0) {
		return (const Class*)class_->jfl_ti->values[class_->offset - 1];
	}
	return nullptr;
}

struct ClassProperty{
	std::string_view str;
	GetFunc* get;
	SetFunc* set;
};

Boolean proplist_index(Object* self, const ClassProperty** plist, uint32_t plist_mask, Any* key, Any* value, dynamic_op op);
Boolean static_index(Object* self, Any* key, Any* value, dynamic_op op);



}
