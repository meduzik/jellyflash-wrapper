#pragma once

#include "types.h"
#include "vtable.h"
#include "macro.h"
#include "class.h"
#include "rest_ops.h"
#include "error.h"

namespace fl{
struct Array;
}

namespace jfl{


struct Vector_VT {
	const VTableHeader jfl_th;
};

struct VectorDef{
	static const Vector_VT jfl_VT;
	static const Class jfl_Class;
};

template<class T>
extern const Class VectorClass;

template<class T>
const void* VectorTI_Values[] = {
	&VectorClass<T>
};

template<class T>
const TypeInfo VectorTI {
	1,
	0,
	VectorTI_Values<T>
};

template<class T>
const Class VectorClass{
	&Class::jfl_VT,
	{},
	&VectorTI<T>,
	jfl::StringSmall{6, "Vec.<>"},
	0,
	0,
	class_kind_t::kind_class,
	[](Object* object, RestParams* params) -> Any{
		std::size_t size = rest_size(params);
		JFL_ARGMINCHECK(size, 1);
		JFL_ARGMAXCHECK(size, 1);
		return Any((Object*)any_to_Class(rest_get(params, 0), &VectorClass<T>));
	}
};

template<class T>
extern Boolean VectorDynamic(Object* self, Any* key, Any* value, dynamic_op op);
template<class T>
extern void VectorFinalize(Object* self);
template<class T>
extern void VectorGC(Object* self, void*);

template<class T>
struct alignas(Object) Vector {
	const Vector_VT* jfl_vtable;
	ObjectHeader jfl_header;
	const Class* elty;
	UInt length;
	UInt capacity;
	T* ptr;

	static T Default;


	jfl_noinline
	static Vector<T>* New(const Vector_VT* instance, const Class* elty, UInt length, Boolean fixed);
	
	jfl_noinline
	static Vector<T>* Cons(const Vector_VT* instance, const Class* elty, fl::Array* array);

	UInt jfl_g_length() {
		return (UInt)length;
	}
	void jfl_s_length(UInt val);

	T cast(Any val);

	Vector<T>* jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_splice(Int startIndex, Int deleteCount, RestParams* items);
	UInt jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(RestParams* args);
	UInt jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_unshift(RestParams* args);
	T jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_shift();
	Vector<T>* jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_reverse();
	Vector<T>* jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_sort(Any behavior);
	Int jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_indexOf(T searchElement, Int fromIndex);
	Int jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_lastIndexOf(T searchElement, Int fromIndex);
	T jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_pop();
	T jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_removeAt(Int index);
	Vector<T>* jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_slice(Int startIndex, Int endIndex);
	Vector<T>* jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_concat(RestParams* args);
	String jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_join(String sep);

	void set(UInt index, T value);
	T get(UInt index);

	void grow(UInt minlength);

	Boolean eq(T lhs, T rhs);
};

template<class T, class Base>
const Vector_VT VectorVT{
	{
		(const TypeInfo*)&VectorTI<T>,
		(sizeof(Vector<T>) + 31) & ~31,
		&VectorGC<Base>,
		&VectorFinalize<Base>,
		&VectorDynamic<Base>,
		nullptr
	}
};

#if !defined(_MSC_VER)
template<> Any Vector<Any>::Default;
template<> Boolean Vector<Boolean>::Default;
template<> Int Vector<Int>::Default;
template<> UInt Vector<UInt>::Default;
template<> Number Vector<Number>::Default;
template<> String Vector<String>::Default;
template<> Function Vector<Function>::Default;
template<> Object* Vector<Object*>::Default;
template<> Interface Vector<Interface>::Default;
#endif

template<>
Boolean Vector<Interface>::eq(Interface lhs, Interface rhs);
template<>
Boolean Vector<Any>::eq(Any lhs, Any rhs);
template<>
Boolean Vector<Function>::eq(Function lhs, Function rhs);
template<>
Boolean Vector<String>::eq(String lhs, String rhs);



template<>
Any Vector<Any>::cast(Any val);
template<>
Boolean Vector<Boolean>::cast(Any val);
template<>
Int Vector<Int>::cast(Any val);
template<>
UInt Vector<UInt>::cast(Any val);
template<>
Number Vector<Number>::cast(Any val);
template<>
String Vector<String>::cast(Any val);
template<>
Function Vector<Function>::cast(Any val);
template<>
Object* Vector<Object*>::cast(Any val);
template<>
Interface Vector<Interface>::cast(Any val);

template<>
void VectorGC<Any>(Object*, void*);
template<>
void VectorGC<Function>(Object*, void*);
template<>
void VectorGC<String>(Object*, void*);
template<>
void VectorGC<Object*>(Object*, void*);
template<>
void VectorGC<Interface>(Object*, void*);


extern template struct Vector<Any>;
extern template struct Vector<Boolean>;
extern template struct Vector<Int>;
extern template struct Vector<UInt>;
extern template struct Vector<Number>;
extern template struct Vector<String>;
extern template struct Vector<Function>;
extern template struct Vector<Object*>;
extern template struct Vector<Interface>;

}