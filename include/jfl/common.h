#pragma once
#include "std.h"
#include "types.h"
#include "memory.h"

#include "number.h"

#include "any.h"
#include "dynamic.h"
#include "function.h"
#include "interface.h"
#include "iterator.h"
#include "object.h"
#include "rest_params.h"
#include "string.h"
#include "vector.h"
#include "vtable.h"
#include "arith.h"
#include "macro.h"
#include "rest_ops.h"
#include "error.h"
#include "compare.h"
#include "box.h"

#include "class.h"

#include "type_conversions.h"
#include "array_view.h"

#include "memory/Memory.h"

#include <fl/String.decl.h>
#include <fl/Array.decl.h>
#include <fl/Class.decl.h>
#include <fl/Function.decl.h>
#include <fl/Boolean.decl.h>
#include <fl/int.decl.h>
#include <fl/uint.decl.h>
#include <fl/Object.decl.h>

