#pragma once
#include <initializer_list>
#include "types.h"

namespace jfl{

struct RestParams {
public:
	RestParams() :
		tag(type_tag::Rest),
		size(0),
		ptr(nullptr)
	{
	}

	RestParams(std::initializer_list<Any> anyvals) :
		tag(type_tag::Rest),
		size((UInt)anyvals.size()),
		ptr((Any*)anyvals.begin())
	{
	}

	RestParams(Any* begin, UInt size) :
		tag(type_tag::Rest),
		size((UInt)size),
		ptr((Any*)begin)
	{
	}

	type_tag tag;
	uint8_t byte[3];
	UInt size;
	union{
		Any* ptr;
		Object* object;
	};
};



}
