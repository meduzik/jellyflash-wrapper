#pragma once
#include <jfl/common.h>
#include <jfl/math/transform2d.h>

namespace jfl{
void TextSubsystem_Init();

void FontParse(std::string_view name, const uint8_t* data, size_t len);

enum class TextAlign: uint8_t{
	Left,
	Center,
	Right,
	Justified
};

struct FontObject;

struct FontReference;

struct FontRequest{
	FontRequest(um_string full_name, um_string font_name, bool bold, bool italic);
	void add_ref();
	void remove_ref();
	void register_font(FontObject* font);
private:
	

	um_string full_name;
	um_string font_name;
	bool bold;
	bool italic;
	size_t refs;
	FontObject* font;
	friend struct FontReference;
};

struct FontDescriptor {
	std::string_view font_name;
	bool bold;
	bool italic;
};

struct FontReference{
	FontReference():
		request(nullptr)
	{
	}

	FontReference(FontRequest* request) :
		request(request) {
		if (request){
			request->add_ref();
		}
	}

	FontReference(const FontReference& other):
		request(other.request)
	{
		if (request){
			request->add_ref();
		}
	}

	FontReference(FontReference&& other) :
		request(other.request) {
		other.request = nullptr;
	}

	~FontReference(){
		reset();
	}

	FontObject* get_font() const{
		if (!request) {
			return nullptr;
		}
		return request->font;
	}

	FontReference& operator=(const FontReference& other){
		if (&other == this) {
			return *this;
		}
		if (other.request){
			other.request->add_ref();
		}
		reset();
		request = other.request;
		return *this;
	}

	FontReference& operator=(FontReference&& other) {
		if (&other == this){
			return *this;
		}
		reset();
		request = other.request;
		other.request = nullptr;
		return *this;
	}

	void reset(){
		if (request){
			request->remove_ref();
			request = nullptr;
		}
	}

	static FontReference Get(const FontDescriptor& descriptor);
private:
	FontRequest* request;
};

struct TextFormat{
	FontReference font;
	TextAlign align;
	int32_t size;
	uint32_t color;
	int32_t left_margin, right_margin;
	int32_t indent, leading;
	double letter_spacing;
};

struct TextRun{
	TextFormat format;
	um_string text;
};

struct TextRenderingCanvas{
	uint8_t* data;
	uint32_t width, height;
	uint32_t bytesPerRow;
};

struct LayoutOperation;

class TextLayout{
public:
	TextLayout();

	void set_default_format(const TextFormat& format){
		this->format = format;
		invalidate();
	}

	const TextFormat& get_default_format() const{
		return format;
	}

	void update_matrix(const matrix2d& matrix);

	void set_multiline(bool flag){
		if (flag != multiline){
			multiline = flag;
			invalidate();
		}
	}
	void set_word_wrap(bool flag){
		if (flag != word_wrap) {
			word_wrap = flag;
			invalidate();
		}
	}

	bool get_multiline() const{
		return multiline;
	}

	bool get_word_wrap() const{
		return word_wrap;
	}

	void set_width(double width){
		this->width = width;
		invalidate();
	}

	void set_height(double height) {
		this->height = height;
		invalidate();
	}

	double get_width() const{
		return width;
	}

	double get_height() const {
		return height;
	}

	double get_text_width(){
		ensure_layout();
		return text_width;
	}

	double get_text_height() {
		ensure_layout();
		return text_height;
	}

	void set_plain_text(const std::string_view& text);
	void append_plain_text(const std::string_view& text);
	void set_html_text(const std::string_view& text);

	jfl::String get_plain_text() const;

	void render(const matrix2d& matrix, TextRenderingCanvas& canvas);
private:
	friend struct LayoutOperation;

	void reset_text() {
		runs.clear();
		invalidate();
	}

	void invalidate();
	void ensure_layout();

	bool multiline, word_wrap;
	TextFormat format;

	double width, height;
	double text_width, text_height;
	matrix2d matrix;

	um_vec<TextRun> runs;

	bool valid;
};


}

