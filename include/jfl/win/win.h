#pragma once
#include <jfl/std.h>
#include <jfl/types.h>
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

namespace jfl{

std::wstring Widen(jfl::String string);
jfl::String Narrow(std::wstring_view string);

String GetErrorMessage(DWORD code);

inline void CheckLastError(){
	DWORD code = GetLastError();
	if ( code != ERROR_SUCCESS ) {
		raise_system_error(code, GetErrorMessage(code));
	}
}

inline void CheckWinAPI(HRESULT result){
	if ( !SUCCEEDED(result) ){
		DWORD code = GetLastError();
		raise_system_error(code, GetErrorMessage(code));
	}
}

}


