#pragma once
#include "types.h"
#include "any.h"
#include "macro.h"
#include "vtable.h"
#include "object.h"
#include "type_inspection.h"
#include <limits>

namespace jfl {

// functions commented with a minus sign
// - 
// should never be used and inlined by the codegen instead

// - inline Boolean Boolean_is_Boolean(Boolean val) { return true; }
// - inline Boolean Boolean_is_int(Boolean val) { return false; }
// - inline Boolean Boolean_is_uint(Boolean val) { return false; }
// - inline Boolean Boolean_is_Number(Boolean val) { return false; }
// - inline Boolean Boolean_is_String(Boolean val) { return false; }
// - inline Boolean Boolean_is_Function(Boolean val) { return false; }
// - inline Boolean Boolean_is_Class(Boolean val, const Class* class_) { return false; }
// - inline Boolean Boolean_is_Interface(Boolean val, const Class* iface) { return false; }
inline Boolean Boolean_is_type(Boolean val, const Class* type){
	return type == &Class_Boolean;
}

// - inline Boolean int_is_Boolean(Int val) { return false; }
// - inline Boolean int_is_int(Int val) { return true; }
// - inline Boolean int_is_uint(Int val) { return val >= 0; }
// - inline Boolean int_is_Number(Int val) { return true; }
// - inline Boolean int_is_String(Int val) { return false; }
// - inline Boolean int_is_Function(Int val) { return false; }
// - inline Boolean int_is_Class(Int val, const Class* class_) { return false; }
// - inline Boolean int_is_Interface(Int val, const Class* iface) { return false; }
inline Boolean int_is_type(Int val, const Class* type){
	return (
		type == &Class_Int 
		|| 
		type == &Class_Number 
		||
		(type == &Class_UInt && val >= 0)
	);
}

// - inline Boolean uint_is_Boolean(UInt val) { return false; }
// - inline Boolean uint_is_int(UInt val) { return val <= (UInt)std::numeric_limits<Int>::max(); }
// - inline Boolean uint_is_uint(UInt val) { return true; }
// - inline Boolean uint_is_Number(UInt val) { return true; }
// - inline Boolean uint_is_String(UInt val) { return false; }
// - inline Boolean uint_is_Function(UInt val) { return false; }
// - inline Boolean uint_is_Class(UInt val, const Class* class_) { return false; }
// - inline Boolean uint_is_Interface(UInt val, const Class* iface) { return false; }
inline Boolean uint_is_type(UInt val, const Class* type){
	return (
		type == &Class_UInt 
		|| 
		type == &Class_Number 
		|| 
		(type == &Class_Int &&  val <= (UInt)std::numeric_limits<Int>::max())
	);
}

// - inline Boolean Number_is_Boolean(Number val) { return false; }
inline Boolean Number_is_int(Number val){
	return (
		val >= (Number)std::numeric_limits<Int>::min() 
		&&
		val <= (Number)std::numeric_limits<Int>::max()
		&&
		std::trunc(val) == val
	);
}
inline Boolean Number_is_uint(Number val){
	return (
		val >= (Number)std::numeric_limits<UInt>::min()
		&&
		val <= (Number)std::numeric_limits<UInt>::max()
		&&
		std::trunc(val) == val
	);
}
// - inline Boolean Number_is_Number(Number val) { return true; }
// - inline Boolean Number_is_String(Number val) { return false; }
// - inline Boolean Number_is_Function(Number val) { return false; }
// - inline Boolean Number_is_Class(Number val, const Class* class_) { return false; }
// - inline Boolean Number_is_Interface(Number val, const Class* iface) { return false; }
inline Boolean Number_is_type(Number val, const Class* type){
	if ( type == &Class_Number ){ return true; }
	if ( type == &Class_Int ){ return Number_is_int(val); }
	if ( type == &Class_UInt ){ return Number_is_uint(val); }
	return false;
}

// - inline Boolean String_is_Boolean(String val) { return false; }
// - inline Boolean String_is_int(String val) { return false; }
// - inline Boolean String_is_uint(String val) { return false; }
// - inline Boolean String_is_Number(String val) { return false; }
inline Boolean String_is_String(String val) { return val.tag <= type_tag::StringEnd; }
// - inline Boolean String_is_Function(String val) { return false; }
// - inline Boolean String_is_Class(String val, const Class* class_) { return false; }
// - inline Boolean String_is_Interface(String val, const Class* iface) { return false; }
inline Boolean String_is_type(String val, const Class* type) { 
	return (type == &Class_String) && val.tag == type_tag::StringEnd;
}

// - inline Boolean Function_is_Boolean(Function val) { return false; }
// - inline Boolean Function_is_int(Function val) { return false; }
// - inline Boolean Function_is_uint(Function val) { return false; }
// - inline Boolean Function_is_Number(Function val) { return false; }
// - inline Boolean Function_is_String(Function val) { return false; }
// - inline Boolean Function_is_Function(Function val) { return true; }
// - inline Boolean Function_is_Class(Function val, const Class* class_) { return false; }
// - inline Boolean Function_is_Interface(Function val, const Class* iface) { return false; }
inline Boolean Function_is_type(Function val, const Class* type){ return type == &Class_Function; }

// - inline Boolean Object_is_Boolean(Object* val) { return false; }
// - inline Boolean Object_is_int(Object* val) { return false; }
// - inline Boolean Object_is_uint(Object* val) { return false; }
// - inline Boolean Object_is_Number(Object* val) { return false; }
// - inline Boolean Object_is_String(Object* val) { return false; }
// - inline Boolean Object_is_Function(Object* val) { return false; }
inline Boolean Object_is_Class(Object* val, const Class* class_) {
	if ( !val ){
		return false;
	}
	return ti_extends(val->jfl_vtable->jfl_vth.ti, class_);
}
inline Boolean Object_is_Interface(Object* val, const Class* iface) {
	if (!val) {
		return false;
	}
	return ti_implements(val->jfl_vtable->jfl_vth.ti, iface);
}
inline Boolean Object_is_type(Object* val, const Class* type){
	if (!val) {
		return false;
	}
	return ti_inherits(val->jfl_vtable->jfl_vth.ti, type);
}

inline Boolean any_is_Boolean(Any val){
	return val.tag == type_tag::Boolean;
}
inline Boolean any_is_int(Any val){
	switch ( val.tag ){
	case type_tag::Int: return true;
	case type_tag::UInt: return (val.uval <= (UInt)std::numeric_limits<Int>::max());
	case type_tag::NumberBox: return Number_is_int(val.nval->value);
	case type_tag::Float: return Number_is_int(val.fval);
	default: return false;
	}
}
inline Boolean any_is_uint(Any val){
	switch (val.tag) {
	case type_tag::Int: return (val.ival >= 0);
	case type_tag::UInt: return true;
	case type_tag::NumberBox: return Number_is_uint(val.nval->value);
	case type_tag::Float: return Number_is_int(val.fval);
	default: return false;
	}
}
inline Boolean any_is_Number(Any val){
	switch (val.tag) {
	case type_tag::Int:
	case type_tag::UInt:
	case type_tag::NumberBox:
	case type_tag::Float: return true;
	default: return false;
	}
}
inline Boolean any_is_String(Any val){
	return val.tag <= type_tag::StringEnd;
}
inline Boolean any_is_Function(Any val){
	return (uint8_t)val.tag & (uint8_t)type_tag::FunctionBit;
}
inline Boolean any_is_Class(Any val, const Class* class_){
	return val.tag == type_tag::Object && Object_is_Class(val.object, class_);
}
inline Boolean any_is_Interface(Any val, const Class* iface){
	return val.tag == type_tag::Object && Object_is_Interface(val.object, iface);
}
inline Boolean any_is_dyn(Any val, const Class* type){
	switch ( val.tag ){
	case type_tag::Null:
	case type_tag::Undefined: return false;
	case type_tag::Boolean: return type == &Class_Boolean;
	case type_tag::Int: return int_is_type(val.ival, type);
	case type_tag::UInt: return uint_is_type(val.uval, type);
	case type_tag::NumberBox: return Number_is_type(val.nval->value, type);
	case type_tag::Float: return Number_is_type(val.fval, type);
	case type_tag::Object: return Object_is_type(val.object, type);
	default:{
		if ( val.tag <= type_tag::StringEnd ){
			return type == &Class_String;
		}else if ((uint8_t)val.tag & (uint8_t)type_tag::FunctionBit) {
			return type == &Class_Function;
		}else{
			jfl_unreachable;
		}
	}break;
	}
}


}

#undef DISABLE

