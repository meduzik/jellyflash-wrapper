#pragma once
#include <unordered_map>
#include "types.h"
#include "any.h"

namespace jfl{

struct Dynamic;


struct DynHash {
	size_t operator()(Any key) const {
		return any_hash(key);
	}
};

struct DynEq {
	Boolean operator()(Any lhs, Any rhs) const {
		return any_eq(lhs, rhs);
	}
};


struct Dynamic {
	using map_t = um_umap< Any, Any, DynHash, DynEq >;

	map_t map;

	static Boolean iterate(Dynamic* dyn, Iterator* iterator);

	Boolean index(Any key, Any* value, dynamic_op op);
};




}
