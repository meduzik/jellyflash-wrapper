#pragma once

#if defined(_WIN32)
	#define JFL_PLATFORM_WIN
	#define JFL_MULTITHREADED
#elif defined(__EMSCRIPTEN__)
	#define JFL_PLATFORM_EM
#else
	#error Unknown platform
#endif

#if defined(_MSC_VER)
	#define JFL_COMPILER_MSVC
#elif defined(__clang__)
	#define JFL_COMPILER_CLANG
#else
	#error Unknown compiler
#endif
