#pragma once
#include <jfl/std.h>

namespace jfl{

template<class Fn>
class Defer{
public:
	Defer(Fn&& fn):
		fn(std::move(fn)){
	}

	Defer(const Defer<Fn>&) = delete;
	Defer(Defer<Fn>&&) = delete;
	Defer<Fn>& operator=(Defer<Fn>&&) = delete;
	Defer<Fn>& operator=(const Defer<Fn>&) = delete;

	~Defer(){
		fn();
	}
private:
	Fn fn;
};

template<class Fn>
Defer<Fn> defer(Fn&& fn){
	return Defer<Fn>(std::move(fn));
}

}

