#if __has_include(<fl/flash/filesystem/File.h>)
#include <jfl/common_impl.h>
#include <fl/flash/filesystem/File.h>

namespace fl::flash::filesystem {
void File::jfl_p__init(fl::flash::filesystem::File* file) {
}
void File::jfl_p__initURL(fl::flash::filesystem::File* jfl_t, jfl::Int jfl_a_scheme, jfl::String jfl_a_path) {
	jfl_notimpl;
}
void File::jfl_p__initPath(fl::flash::filesystem::File* file, jfl::String path) {
#if defined(__ANDROID__)
	jfl_notimpl;
#else
	if (path.tag == jfl::type_tag::Null) {
		jfl::raise_npe();
	}
	auto [ptr, len] = path.to_view();
	file->path.assign(std::string_view((const char*)ptr, len));
#endif
}
jfl::String File::toString(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
jfl::Boolean File::jfl_g_exists(fl::flash::filesystem::File* file) {
#if defined(__ANDROID__)
	jfl_notimpl;
#else
	std::error_code errc;
	bool result = std::filesystem::exists(file->path, errc);
	if (errc != std::error_code()) {
		jfl_notimpl;
	}
	return result;
#endif
}
jfl::Boolean File::jfl_g_isHidden(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
jfl::Boolean File::jfl_g_isDirectory(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
jfl::Boolean File::jfl_g_isPackage(fl::flash::filesystem::File* jfl_t) {
	return false;
}
jfl::Boolean File::jfl_g_isSymbolicLink(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
void File::cancel(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
fl::flash::filesystem::File* File::resolvePath(fl::flash::filesystem::File* file, jfl::String path) {
#if defined(__ANDROID__)
	jfl_notimpl;
#else
	if (path.tag == jfl::type_tag::Null) {
		jfl::raise_npe();
	}

	File* copy = File::jfl_New(jfl::StringEmpty);
	copy->path = file->path;
	copy->path.append(path.to_string_view());
	return copy;
#endif
}
jfl::String File::getRelativePath(fl::flash::filesystem::File* jfl_t, fl::flash::net::FileReference* jfl_a_ref, jfl::Boolean jfl_a_useDotDot) {
	jfl_notimpl;
}
fl::flash::filesystem::File* File::jfl_g_parent(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
jfl::String File::jfl_g_nativePath(fl::flash::filesystem::File* file) {
#if defined(__ANDROID__)
	jfl_notimpl;
#else
	return jfl::String::Make(file->path.u8string());
#endif
}
void File::jfl_s_nativePath(fl::flash::filesystem::File* jfl_t, jfl::String jfl_a_value) {
	jfl_notimpl;
}
void File::canonicalize(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
jfl::String File::jfl_g_url(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
void File::jfl_s_url(fl::flash::filesystem::File* jfl_t, jfl::String jfl_a_value) {
	jfl_notimpl;
}
void File::browseForOpen(fl::flash::filesystem::File* jfl_t, jfl::String jfl_a_param1, fl::Array* jfl_a_param2) {
	jfl_notimpl;
}
void File::browseForOpenMultiple(fl::flash::filesystem::File* jfl_t, jfl::String jfl_a_param1, fl::Array* jfl_a_param2) {
	jfl_notimpl;
}
void File::browseForSave(fl::flash::filesystem::File* jfl_t, jfl::String jfl_a_param1) {
	jfl_notimpl;
}
void File::browseForDirectory(fl::flash::filesystem::File* jfl_t, jfl::String jfl_a_param1) {
	jfl_notimpl;
}
void File::deleteFile(fl::flash::filesystem::File* file) {
#if defined(__ANDROID__)
	jfl_notimpl;
#else
	std::filesystem::remove(file->path);
#endif
}
void File::deleteFileAsync(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
void File::deleteDirectory(fl::flash::filesystem::File* jfl_t, jfl::Boolean jfl_a_param1) {
	jfl_notimpl;
}
void File::deleteDirectoryAsync(fl::flash::filesystem::File* jfl_t, jfl::Boolean jfl_a_param1) {
	jfl_notimpl;
}
void File::copyTo(fl::flash::filesystem::File* jfl_t, fl::flash::net::FileReference* jfl_a_param1, jfl::Boolean jfl_a_param2) {
	jfl_notimpl;
}
void File::copyToAsync(fl::flash::filesystem::File* jfl_t, fl::flash::net::FileReference* jfl_a_param1, jfl::Boolean jfl_a_param2) {
	jfl_notimpl;
}
void File::moveTo(fl::flash::filesystem::File* file, fl::flash::net::FileReference* target, jfl::Boolean overwrite) {
#if defined(__ANDROID__)
	jfl_notimpl;
#else
	if ( overwrite ){
		std::error_code ec;
		std::filesystem::remove(target->path, ec);
	}
	std::filesystem::rename(file->path, target->path);
#endif
}
void File::moveToAsync(fl::flash::filesystem::File* jfl_t, fl::flash::net::FileReference* jfl_a_param1, jfl::Boolean jfl_a_param2) {
	jfl_notimpl;
}
void File::moveToTrash(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
void File::moveToTrashAsync(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
void File::createDirectory(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
fl::Array* File::getDirectoryListing(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
void File::getDirectoryListingAsync(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
fl::flash::filesystem::File* File::clone(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
fl::flash::desktop::Icon* File::jfl_g_icon(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
jfl::Number File::jfl_g_spaceAvailable(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
void File::openWithDefaultApplication(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
jfl::Boolean File::jfl_g_downloaded(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
void File::jfl_s_downloaded(fl::flash::filesystem::File* jfl_t, jfl::Boolean jfl_a_value) {
	jfl_notimpl;
}
jfl::Boolean File::jfl_g_preventBackup(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
void File::jfl_s_preventBackup(fl::flash::filesystem::File* jfl_t, jfl::Boolean jfl_a_param1) {
	jfl_notimpl;
}
void File::requestPermission(fl::flash::filesystem::File* jfl_t) {
	jfl_notimpl;
}
fl::flash::filesystem::File* File::jfl_S_createTempFile() {
	jfl_notimpl;
}
fl::flash::filesystem::File* File::jfl_S_createTempDirectory() {
	jfl_notimpl;
}
fl::Array* File::jfl_S_getRootDirectories() {
	jfl_notimpl;
}
}
#endif
