#if __has_include(<fl/flash/filesystem/FileStream.h>)
#include <jfl/common_impl.h>
#include <fl/flash/filesystem/FileStream.h>
#include <fl/flash/filesystem/File.h>
#include <fl/flash/utils/ByteArray.h>
#include <fl/Error.h>
namespace fl::flash::filesystem{
using namespace jfl;
void FileStream::open(fl::flash::filesystem::FileStream* self, fl::flash::filesystem::File* file, jfl::String mode){
#if defined(__ANDROID__)
	jfl_notimpl;
#else
	if ( self->stream.is_open() ){
		close(self);
	}
	int mode_val;
	bool ensure_dirs = false;
	auto mode_view = mode.to_string_view();
	if ( mode_view == "append" ){
		mode_val = std::ios::app;
		ensure_dirs = true;
	}else if ( mode_view == "read" ) {
		mode_val = std::ios::in;
	} else {
		jfl_notimpl;
	}
	if ( ensure_dirs ){
		std::error_code errc;
		std::filesystem::create_directories(file->path.parent_path(), errc);
		if ( errc != std::error_code() ){
			jfl_notimpl;
		}
	}
	self->stream.open(file->path, mode_val | std::ios::binary);
	if ( !self->stream ){
		jfl_notimpl;
	}
#endif
}
void FileStream::openAsync(fl::flash::filesystem::FileStream* jfl_t, fl::flash::filesystem::File* jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
void FileStream::truncate(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::Number FileStream::jfl_g_position(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
void FileStream::jfl_s_position(fl::flash::filesystem::FileStream* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void FileStream::close(fl::flash::filesystem::FileStream* self){
	self->stream.close();
}
jfl::Number FileStream::jfl_g_readAhead(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
void FileStream::jfl_s_readAhead(fl::flash::filesystem::FileStream* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::UInt FileStream::jfl_g_bytesAvailable(fl::flash::filesystem::FileStream* self){
	auto back = self->stream.tellg();
	self->stream.seekg(0, std::ios::end);
	auto end = self->stream.tellg();
	self->stream.seekg(back);
	return (jfl::UInt)(end - back);
}
jfl::String FileStream::jfl_g_endian(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
void FileStream::jfl_s_endian(fl::flash::filesystem::FileStream* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::UInt FileStream::jfl_g_objectEncoding(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
void FileStream::jfl_s_objectEncoding(fl::flash::filesystem::FileStream* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean FileStream::readBoolean(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::Int FileStream::readByte(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
void FileStream::readBytes(fl::flash::filesystem::FileStream* self, fl::flash::utils::ByteArray* bytes, jfl::UInt offset, jfl::UInt length){
	if ( length == 0 ){
		length = jfl_g_bytesAvailable(self);
	}
	bytes->ensure_size(bytes, offset + length);
	self->stream.read((char*)bytes->data.data(), length);
	if ( !self->stream ){
		raise_message("io error");
	}
}
jfl::Number FileStream::readDouble(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::Number FileStream::readFloat(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::Int FileStream::readInt(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::String FileStream::readMultiByte(fl::flash::filesystem::FileStream* jfl_t, jfl::UInt jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
jfl::Any FileStream::readObject(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::Int FileStream::readShort(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::UInt FileStream::readUnsignedByte(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::UInt FileStream::readUnsignedInt(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::UInt FileStream::readUnsignedShort(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::String FileStream::readUTF(fl::flash::filesystem::FileStream* jfl_t){
	jfl_notimpl;
}
jfl::String FileStream::readUTFBytes(fl::flash::filesystem::FileStream* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeBoolean(fl::flash::filesystem::FileStream* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeByte(fl::flash::filesystem::FileStream* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeBytes(fl::flash::filesystem::FileStream* stream, fl::flash::utils::ByteArray* bytes, jfl::UInt offset, jfl::UInt length){
	if ( !bytes ){
		raise_nullarg();
	}
	if ( offset > bytes->data.size() ){
		offset = (UInt)bytes->data.size();
	}
	UInt maxlen = (UInt)bytes->data.size() - offset;
	if ( length == 0 ){
		length = maxlen;
	}else if ( length > maxlen ){
		length = maxlen;
	}
	stream->stream.write((const char*)bytes->data.data() + offset, length);
	if ( stream->stream.bad() ){
		jfl_notimpl;
	}
}
void FileStream::writeDouble(fl::flash::filesystem::FileStream* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeFloat(fl::flash::filesystem::FileStream* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeInt(fl::flash::filesystem::FileStream* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeMultiByte(fl::flash::filesystem::FileStream* jfl_t, jfl::String jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
void FileStream::writeObject(fl::flash::filesystem::FileStream* jfl_t, jfl::Any jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeShort(fl::flash::filesystem::FileStream* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeUnsignedInt(fl::flash::filesystem::FileStream* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeUTF(fl::flash::filesystem::FileStream* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void FileStream::writeUTFBytes(fl::flash::filesystem::FileStream* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
}
#endif
