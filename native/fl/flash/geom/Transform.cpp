#if __has_include(<fl/flash/geom/Transform.h>)
#include <jfl/common_impl.h>
#include <fl/flash/geom/Transform.h>
namespace fl::flash::geom{
fl::flash::geom::Matrix* Transform::jfl_g_matrix(fl::flash::geom::Transform* jfl_t){
	jfl_notimpl;
}
void Transform::jfl_s_matrix(fl::flash::geom::Transform* jfl_t, fl::flash::geom::Matrix* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::ColorTransform* Transform::jfl_g_colorTransform(fl::flash::geom::Transform* jfl_t){
	jfl_notimpl;
}
void Transform::jfl_s_colorTransform(fl::flash::geom::Transform* jfl_t, fl::flash::geom::ColorTransform* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Matrix* Transform::jfl_g_concatenatedMatrix(fl::flash::geom::Transform* jfl_t){
	jfl_notimpl;
}
fl::flash::geom::ColorTransform* Transform::jfl_g_concatenatedColorTransform(fl::flash::geom::Transform* jfl_t){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* Transform::jfl_g_pixelBounds(fl::flash::geom::Transform* jfl_t){
	jfl_notimpl;
}
void Transform::jfl_p_ctor(fl::flash::geom::Transform* jfl_t, fl::flash::display::DisplayObject* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Matrix3D* Transform::jfl_g_matrix3D(fl::flash::geom::Transform* jfl_t){
	jfl_notimpl;
}
void Transform::jfl_s_matrix3D(fl::flash::geom::Transform* jfl_t, fl::flash::geom::Matrix3D* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Matrix3D* Transform::getRelativeMatrix3D(fl::flash::geom::Transform* jfl_t, fl::flash::display::DisplayObject* jfl_a_param1){
	jfl_notimpl;
}
}
#endif
