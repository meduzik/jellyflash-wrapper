#if __has_include(<fl/flash/geom/Matrix3D.h>)
#include <jfl/common_impl.h>
#include <fl/flash/geom/Matrix3D.h>
namespace fl::flash::geom{
void Matrix3D::jfl_p_ctor(fl::flash::geom::Matrix3D* self, jfl::Vector<jfl::Number>* vec){
	if ( !vec ){
		identity(self);
	}else{
		if ( vec->length != 16 ){
			jfl::raise_message("invalid Vector: Matrix3D can only be initialized from a 16-element Vector");
		}
		for ( int i = 0; i < 16; i++ ){
			self->v[i] = vec->ptr[i];
		}
	}
}
fl::flash::geom::Matrix3D* Matrix3D::clone(fl::flash::geom::Matrix3D* jfl_t){
	jfl_notimpl;
}
void Matrix3D::copyToMatrix3D(fl::flash::geom::Matrix3D* jfl_t, fl::flash::geom::Matrix3D* jfl_a_param1){
	jfl_notimpl;
}
jfl::Vector<jfl::Number>* Matrix3D::jfl_g_rawData(fl::flash::geom::Matrix3D* jfl_t){
	jfl_notimpl;
}
void Matrix3D::jfl_s_rawData(fl::flash::geom::Matrix3D* jfl_t, jfl::Vector<jfl::Number>* jfl_a_param1){
	jfl_notimpl;
}
void Matrix3D::append(fl::flash::geom::Matrix3D* jfl_t, fl::flash::geom::Matrix3D* jfl_a_param1){
	jfl_notimpl;
}
void Matrix3D::prepend(fl::flash::geom::Matrix3D* jfl_t, fl::flash::geom::Matrix3D* jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean Matrix3D::invert(fl::flash::geom::Matrix3D* jfl_t){
	jfl_notimpl;
}
void Matrix3D::identity(fl::flash::geom::Matrix3D* self){
	memset(self->v, 0, sizeof(self->v));
}
jfl::Vector<jfl::Object*>* Matrix3D::decompose(fl::flash::geom::Matrix3D* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean Matrix3D::recompose(fl::flash::geom::Matrix3D* jfl_t, jfl::Vector<jfl::Object*>* jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
fl::flash::geom::Vector3D* Matrix3D::jfl_g_position(fl::flash::geom::Matrix3D* jfl_t){
	jfl_notimpl;
}
void Matrix3D::jfl_s_position(fl::flash::geom::Matrix3D* jfl_t, fl::flash::geom::Vector3D* jfl_a_param1){
	jfl_notimpl;
}
void Matrix3D::appendTranslation(fl::flash::geom::Matrix3D* jfl_t, jfl::Number jfl_a_param1, jfl::Number jfl_a_param2, jfl::Number jfl_a_param3){
	jfl_notimpl;
}
void Matrix3D::appendRotation(fl::flash::geom::Matrix3D* jfl_t, jfl::Number jfl_a_param1, fl::flash::geom::Vector3D* jfl_a_param2, fl::flash::geom::Vector3D* jfl_a_param3){
	jfl_notimpl;
}
void Matrix3D::appendScale(fl::flash::geom::Matrix3D* jfl_t, jfl::Number jfl_a_param1, jfl::Number jfl_a_param2, jfl::Number jfl_a_param3){
	jfl_notimpl;
}
void Matrix3D::prependTranslation(fl::flash::geom::Matrix3D* jfl_t, jfl::Number jfl_a_param1, jfl::Number jfl_a_param2, jfl::Number jfl_a_param3){
	jfl_notimpl;
}
void Matrix3D::prependRotation(fl::flash::geom::Matrix3D* jfl_t, jfl::Number jfl_a_param1, fl::flash::geom::Vector3D* jfl_a_param2, fl::flash::geom::Vector3D* jfl_a_param3){
	jfl_notimpl;
}
void Matrix3D::prependScale(fl::flash::geom::Matrix3D* jfl_t, jfl::Number jfl_a_param1, jfl::Number jfl_a_param2, jfl::Number jfl_a_param3){
	jfl_notimpl;
}
fl::flash::geom::Vector3D* Matrix3D::transformVector(fl::flash::geom::Matrix3D* jfl_t, fl::flash::geom::Vector3D* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Vector3D* Matrix3D::deltaTransformVector(fl::flash::geom::Matrix3D* jfl_t, fl::flash::geom::Vector3D* jfl_a_param1){
	jfl_notimpl;
}
void Matrix3D::transformVectors(fl::flash::geom::Matrix3D* jfl_t, jfl::Vector<jfl::Number>* jfl_a_param1, jfl::Vector<jfl::Number>* jfl_a_param2){
	jfl_notimpl;
}
jfl::Number Matrix3D::jfl_g_determinant(fl::flash::geom::Matrix3D* jfl_t){
	jfl_notimpl;
}
void Matrix3D::transpose(fl::flash::geom::Matrix3D* jfl_t){
	jfl_notimpl;
}
void Matrix3D::pointAt(fl::flash::geom::Matrix3D* jfl_t, fl::flash::geom::Vector3D* jfl_a_param1, fl::flash::geom::Vector3D* jfl_a_param2, fl::flash::geom::Vector3D* jfl_a_param3){
	jfl_notimpl;
}
void Matrix3D::interpolateTo(fl::flash::geom::Matrix3D* jfl_t, fl::flash::geom::Matrix3D* jfl_a_param1, jfl::Number jfl_a_param2){
	jfl_notimpl;
}
void Matrix3D::copyFrom(fl::flash::geom::Matrix3D* jfl_t, fl::flash::geom::Matrix3D* jfl_a_param1){
	jfl_notimpl;
}
void Matrix3D::copyRawDataTo(fl::flash::geom::Matrix3D* jfl_t, jfl::Vector<jfl::Number>* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::Boolean jfl_a_param3){
	jfl_notimpl;
}
void Matrix3D::copyRawDataFrom(fl::flash::geom::Matrix3D* self, jfl::Vector<jfl::Number>* vec, jfl::UInt index, jfl::Boolean transpose){
	if ( vec->length < index + 16 ){
		jfl::raise_message("not enough elements");
	}
	memcpy(self->m, vec->ptr + index, sizeof(self->m));
	if ( transpose ){
		Matrix3D::transpose(self);
	}
}
void Matrix3D::copyRowTo(fl::flash::geom::Matrix3D* jfl_t, jfl::UInt jfl_a_param1, fl::flash::geom::Vector3D* jfl_a_param2){
	jfl_notimpl;
}
void Matrix3D::copyColumnTo(fl::flash::geom::Matrix3D* jfl_t, jfl::UInt jfl_a_param1, fl::flash::geom::Vector3D* jfl_a_param2){
	jfl_notimpl;
}
void Matrix3D::copyRowFrom(fl::flash::geom::Matrix3D* jfl_t, jfl::UInt jfl_a_param1, fl::flash::geom::Vector3D* jfl_a_param2){
	jfl_notimpl;
}
void Matrix3D::copyColumnFrom(fl::flash::geom::Matrix3D* jfl_t, jfl::UInt jfl_a_param1, fl::flash::geom::Vector3D* jfl_a_param2){
	jfl_notimpl;
}
fl::flash::geom::Matrix3D* Matrix3D::jfl_S_interpolate(fl::flash::geom::Matrix3D* jfl_a_param1, fl::flash::geom::Matrix3D* jfl_a_param2, jfl::Number jfl_a_param3){
	jfl_notimpl;
}
}
#endif
