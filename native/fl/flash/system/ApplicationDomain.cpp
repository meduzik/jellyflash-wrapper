#if __has_include(<fl/flash/system/ApplicationDomain.h>)
#include <jfl/common_impl.h>
#include <fl/flash/system/ApplicationDomain.h>
namespace fl::flash::system{
jfl::Any ApplicationDomain::getDefinition(fl::flash::system::ApplicationDomain* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean ApplicationDomain::hasDefinition(fl::flash::system::ApplicationDomain* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Vector<jfl::String>* ApplicationDomain::getQualifiedDefinitionNames(fl::flash::system::ApplicationDomain* jfl_t){
	jfl_notimpl;
}
}
#endif
