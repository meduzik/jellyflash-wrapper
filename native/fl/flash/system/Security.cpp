#if __has_include(<fl/flash/system/Security.h>)
#include <jfl/common_impl.h>
#include <fl/flash/system/Security.h>
namespace fl::flash::system{
void Security::jfl_S_allowDomain(jfl::RestParams* jfl_a_rest){
	jfl_notimpl;
}
void Security::jfl_S_allowInsecureDomain(jfl::RestParams* jfl_a_rest){
	jfl_notimpl;
}
void Security::jfl_S_loadPolicyFile(jfl::String jfl_a_param1){
	// do nothing
	return;
}
jfl::Boolean Security::jfl_Sg_exactSettings(){
	jfl_notimpl;
}
void Security::jfl_Ss_exactSettings(jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean Security::jfl_Sg_disableAVM1Loading(){
	jfl_notimpl;
}
void Security::jfl_Ss_disableAVM1Loading(jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
fl::Array* Security::jfl_nflash_2einternal_nS_duplicateSandboxBridgeInputArguments(jfl::Any jfl_a_param1, fl::Array* jfl_a_param2){
	jfl_notimpl;
}
jfl::Any Security::jfl_nflash_2einternal_nS_duplicateSandboxBridgeOutputArgument(jfl::Any jfl_a_param1, jfl::Any jfl_a_param2){
	jfl_notimpl;
}
void Security::jfl_S_showSettings(jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String Security::jfl_Sg_sandboxType(){
	return jfl::String::Make("application");
}
jfl::String Security::jfl_Sg_pageDomain(){
	jfl_notimpl;
}
}
#endif
