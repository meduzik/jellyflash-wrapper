#if __has_include(<fl/flash/system/System.h>)
#include <jfl/common_impl.h>
#include <fl/flash/system/System.h>
#include <jfl/app/Loop.h>
namespace fl::flash::system{
fl::flash::system::IME* System::jfl_Sg_ime(){
	jfl_notimpl;
}
void System::jfl_S_setClipboard(jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Number System::jfl_Sg_totalMemoryNumber(){
	jfl_notimpl;
}
jfl::Number System::jfl_Sg_freeMemory(){
	jfl_notimpl;
}
jfl::Number System::jfl_Sg_privateMemory(){
	jfl_notimpl;
}
jfl::Number System::jfl_Sg_processCPUUsage(){
	jfl_notimpl;
}
jfl::Boolean System::jfl_Sg_useCodePage(){
	jfl_notimpl;
}
void System::jfl_Ss_useCodePage(jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::String System::jfl_Sg_vmVersion(){
	jfl_notimpl;
}
void System::jfl_S_pause(){
	jfl_notimpl;
}
void System::jfl_S_resume(){
	jfl_notimpl;
}
void System::jfl_S_exit(jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
void System::jfl_S_gc(){
	jfl::gc_perform();
}
void System::jfl_S_pauseForGCIfCollectionImminent(jfl::Number jfl_a_param1){
	jfl::gc_perform();
}
}
#endif
