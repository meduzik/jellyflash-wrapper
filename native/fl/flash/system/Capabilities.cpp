#if __has_include(<fl/flash/system/Capabilities.h>)
#include <jfl/common_impl.h>
#include <fl/flash/system/Capabilities.h>

#define TARGET_DESKTOP 1
#define TARGET_EM 2

#if defined(__EMSCRIPTEN__)
	#define TARGET TARGET_EM
#else
	#define TARGET TARGET_DESKTOP
#endif

namespace jfl::cstr {
inline const jfl::StringObject jfl_8adc866d505c5b74c4d0eb3d9dce0c5e66481a83{ &jfl::StringObject::jfl_VT, jfl::static_object, 18, (const uint8_t*)"Jellyflash-Desktop" };
}

namespace fl::flash::system{

jfl::Boolean Capabilities::jfl_Sg_isEmbeddedInAcrobat(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasEmbeddedVideo(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasAudio(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_avHardwareDisable(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasAccessibility(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasAudioEncoder(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasMP3(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasPrinting(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasScreenBroadcast(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasScreenPlayback(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasStreamingAudio(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasStreamingVideo(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasVideoEncoder(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_isDebugger(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_localFileReadDisable(){
	jfl_notimpl;
}
jfl::String Capabilities::jfl_Sg_language(){
	return jfl::StringSmall{2, "en"};
}
jfl::String Capabilities::jfl_Sg_manufacturer(){
	return jfl::String::Make("unknown");
}
jfl::String Capabilities::jfl_Sg_os(){
#if defined(JFL_PLATFORM_EM)
	return jfl::String::Make("Unknown");
#elif defined(JFL_PLATFORM_WIN)
	return jfl::String::Make("Windows");
#else
	#error Cannot get OS
#endif
}
jfl::String Capabilities::jfl_Sg_cpuArchitecture(){
	jfl_notimpl;
}
jfl::String Capabilities::jfl_Sg_playerType(){
#if defined(JFL_PLATFORM_EM)
	return jfl::String::Make("Jellyflash-Em");
#elif defined(JFL_PLATFORM_WIN)
	return (jfl::StringObject*)&jfl::cstr::jfl_8adc866d505c5b74c4d0eb3d9dce0c5e66481a83;
#else
	#error Cannot get player type
#endif
}
jfl::String Capabilities::jfl_Sg_serverString(){
	jfl_notimpl;
}
jfl::String Capabilities::jfl_Sg_version(){
#if defined(JFL_PLATFORM_EM)
	return jfl::String::Make("EM 0.0.1");
#elif defined(JFL_PLATFORM_WIN)
	return jfl::String::Make("WIN 0.0.1");
#else
	#error Cannot get version
#endif
}
jfl::String Capabilities::jfl_Sg_screenColor(){
	jfl_notimpl;
}
jfl::Number Capabilities::jfl_Sg_pixelAspectRatio(){
	jfl_notimpl;
}
jfl::String Capabilities::jfl_Sg_touchscreenType(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasIME(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_hasTLS(){
	jfl_notimpl;
}
jfl::String Capabilities::jfl_Sg_maxLevelIDC(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_supports32BitProcesses(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_Sg_supports64BitProcesses(){
	jfl_notimpl;
}
jfl::UInt Capabilities::jfl_Sg__internal(){
	jfl_notimpl;
}
jfl::Boolean Capabilities::jfl_S_hasMultiChannelAudio(jfl::String jfl_a_param1){
	jfl_notimpl;
}
fl::Array* Capabilities::jfl_Sg_languages(){
	jfl_notimpl;
}
}
#endif
