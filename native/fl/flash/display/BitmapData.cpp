#if __has_include(<fl/flash/display/BitmapData.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display/BitmapData.h>
#include <fl/flash/utils/ByteArray.h>
#include <fl/flash/display/DisplayObjectContainer.h>
#include <fl/flash/filters/DropShadowFilter.h>
#include <fl/flash/text/TextField.h>
#include <fl/flash/geom/Rectangle.h>
#include <fl/flash/geom/Point.h>
#include <fl/flash/geom/Matrix.h>
#include <jfl/math/transform2d.h>
namespace fl::flash::display{
using namespace jfl;
using fl::flash::text::TextField;
using namespace fl::flash::filters;

void BitmapData::jfl_p_ctor(fl::flash::display::BitmapData* self, jfl::Int width, jfl::Int height, jfl::Boolean transparent, jfl::UInt fill){
	self->width = width;
	self->height = height;
	self->transparent = transparent;
	self->bytesPerPixel = transparent ? 4 : 3;
	self->bytesPerRow = ((self->width * self->bytesPerPixel) + 3) & ~3;
	self->data = (uint8_t*)jfl::unmanaged_allocate(self->bytesPerRow * self->height);
	for ( jfl::Int y = 0; y < height; y++ ){
		uint8_t* row_ptr = self->data + self->bytesPerRow * y;
		for ( jfl::Int x = 0; x < width; x++ ){
			if ( transparent ){
				*(uint32_t*)(row_ptr + self->bytesPerPixel * x) = fill;
			}else{
				uint8_t* pixel = row_ptr + self->bytesPerPixel * x;
				pixel[0] = fill >> 0;
				pixel[1] = fill >> 8;
				pixel[2] = fill >> 16;
			}
		}
	}
}
BitmapData::~BitmapData(){
	dispose(this);
}
fl::flash::display::BitmapData* BitmapData::clone(fl::flash::display::BitmapData* jfl_t){
	jfl_notimpl;
}
jfl::Int BitmapData::jfl_g_width(fl::flash::display::BitmapData* self){
	return self->width;
}
jfl::Int BitmapData::jfl_g_height(fl::flash::display::BitmapData* self){
	return self->height;
}
jfl::Boolean BitmapData::jfl_g_transparent(fl::flash::display::BitmapData* self){
	return self->transparent;
}
jfl::UInt BitmapData::getPixel(fl::flash::display::BitmapData* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	jfl_notimpl;
}
jfl::UInt BitmapData::getPixel32(fl::flash::display::BitmapData* self, jfl::Int x, jfl::Int y){
	if ( x < 0 || y < 0 || x >= self->width || y >= self->height ){
		jfl::raise_message("bad pixel offset");
	}
	const uint8_t* row = self->data + self->bytesPerRow * y;
	if ( self->transparent ){
		return *((uint32_t*)row + x);
	}else{
		uint32_t b = row[x * 3 + 0];
		uint32_t g = row[x * 3 + 1];
		uint32_t r = row[x * 3 + 2];
		return (0xffu << 24u) | (r << 16) | (g << 8) | (b << 0);
	}
}
void BitmapData::setPixel(fl::flash::display::BitmapData* self, jfl::Int x, jfl::Int y, jfl::UInt color){
	setPixel32(self, x, y, color | (0xff << 24));
}
void BitmapData::setPixel32(fl::flash::display::BitmapData* self, jfl::Int x, jfl::Int y, jfl::UInt color){
	if (x < 0 || y < 0 || x >= self->width || y >= self->height) {
		jfl::raise_message("bad pixel offset");
	}
	uint8_t* row = self->data + self->bytesPerRow * y;
	if (self->transparent) {
		*((uint32_t*)row + x) = color;
	} else {
		uint32_t r = (color >> 16) & 0xff;
		uint32_t g = (color >> 8) & 0xff;
		uint32_t b = (color >> 0) & 0xff;
		row[x * 3 + 0] = b;
		row[x * 3 + 1] = g;
		row[x * 3 + 2] = r;
	}
}
void BitmapData::applyFilter(fl::flash::display::BitmapData* jfl_t, fl::flash::display::BitmapData* jfl_a_param1, fl::flash::geom::Rectangle* jfl_a_param2, fl::flash::geom::Point* jfl_a_param3, fl::flash::filters::BitmapFilter* jfl_a_param4){
	jfl_notimpl;
}
void BitmapData::colorTransform(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1, fl::flash::geom::ColorTransform* jfl_a_param2){
	jfl_notimpl;
}
jfl::Any BitmapData::compare(fl::flash::display::BitmapData* jfl_t, fl::flash::display::BitmapData* jfl_a_param1){
	jfl_notimpl;
}

static uint32_t channel2offset(fl::flash::display::BitmapData* self, jfl::UInt channel){
	if (self->transparent) {
		switch (channel) {
		case 8: {
			return 3;
		}break;
		case 1: {
			return 2;
		}break;
		case 2: {
			return 1;
		}break;
		case 4: {
			return 0;
		}break;
		}
	}else{
		switch (channel){
		case 1:{
			return 2;
		}break;
		case 2: {
			return 1;
		}break;
		case 4: {
			return 0;
		}break;
		}
	}
	jfl::raise_message("bad channel");
}

void BitmapData::copyChannel(
	fl::flash::display::BitmapData* self, 
	fl::flash::display::BitmapData* source, 
	fl::flash::geom::Rectangle* rect,
	fl::flash::geom::Point* offset, 
	jfl::UInt channel_from, 
	jfl::UInt channel_to
){
	int32_t source_x1 = jfl::Number_to_int(rect->x);
	int32_t source_x2 = source_x1 + jfl::Number_to_int(rect->width);
	int32_t source_y1 = jfl::Number_to_int(rect->y);
	int32_t source_y2 = source_y1 + jfl::Number_to_int(rect->height);

	int32_t target_x1 = jfl::Number_to_int(offset->x);
	int32_t target_y1 = jfl::Number_to_int(offset->y);
	int32_t target_x2 = target_x1 + jfl::Number_to_int(rect->width);
	int32_t target_y2 = target_y1 + jfl::Number_to_int(rect->height);

	if (source_x1 < 0){
		target_x1 -= source_x1;
		source_x1 = 0;
	}
	if (source_y1 < 0) {
		target_y1 -= source_y1;
		source_y1 = 0;
	}
	if (target_x1 < 0) {
		source_x1 -= target_x1;
		target_x1 = 0;
	}
	if (target_y1 < 0) {
		source_y1 -= target_y1;
		target_y1 = 0;
	}
	if (source_x2 > source->width){
		target_x2 -= (source_x2 - source->width);
		source_x2 = source->width;
	}
	if (source_y2 > source->height) {
		target_y2 -= (source_y2 - source->height);
		source_y2 = source->height;
	}
	if (target_x2 > self->width) {
		source_x2 -= (target_x2 - self->width);
		target_x2 = self->width;
	}
	if (target_y2 > self->height) {
		source_y2 -= (target_y2 - self->height);
		target_y2 = self->height;
	}
	uint32_t channel_source = channel2offset(source, channel_from);
	uint32_t channel_target = channel2offset(self, channel_to);
	int32_t rows = source_y2 - source_y1;
	int32_t cols = source_x2 - source_x1;

	size_t source_stride = source->bytesPerPixel;
	size_t target_stride = self->bytesPerPixel;

	for (int32_t y = 0; y < rows; y++){
		uint8_t* source_row = source->data + (source_y1 + y) * source->bytesPerRow;
		uint8_t* target_row = self->data + (target_y1 + y) * self->bytesPerRow;
		for (int32_t x = 0; x < cols; x++){
			target_row[channel_target] = source_row[channel_source];
			target_row += target_stride;
			source_row += source_stride;
		}
	}
}
void BitmapData::copyPixels(fl::flash::display::BitmapData* jfl_t, fl::flash::display::BitmapData* jfl_a_param1, fl::flash::geom::Rectangle* jfl_a_param2, fl::flash::geom::Point* jfl_a_param3, fl::flash::display::BitmapData* jfl_a_param4, fl::flash::geom::Point* jfl_a_param5, jfl::Boolean jfl_a_param6){
	jfl_notimpl;
}
void BitmapData::dispose(fl::flash::display::BitmapData* self){
	if ( self->data ){
		jfl::unmanaged_free(self->data, self->height * self->bytesPerRow);
		self->data = nullptr;
	}
	self->width = 0;
	self->height = 0;
}
void BitmapData::draw(fl::flash::display::BitmapData* jfl_t, jfl::I<fl::flash::display::IBitmapDrawable> jfl_a_param1, fl::flash::geom::Matrix* jfl_a_param2, fl::flash::geom::ColorTransform* jfl_a_param3, jfl::String jfl_a_param4, fl::flash::geom::Rectangle* jfl_a_param5, jfl::Boolean jfl_a_param6){
	jfl_notimpl;
}

static void BitmapExactSample(BitmapData* source, int32_t x, int32_t y, int32_t q, uint32_t color[4]) {
	if (x < 0 || x >= source->width || y < 0 || y >= source->height){
		return;
	}
	uint8_t* pixel = source->data + source->bytesPerRow * y + x * source->bytesPerPixel;
	if (source->transparent){
		color[0] += pixel[0] * q;
		color[1] += pixel[1] * q;
		color[2] += pixel[2] * q;
		color[3] += pixel[3] * q;
	}else{
		color[0] += pixel[0] * q;
		color[1] += pixel[1] * q;
		color[2] += pixel[2] * q;
		color[3] += 255 * q; 
	}
	return ;
}

static void BitmapAASample(BitmapData* source, int32_t x, int32_t y, uint32_t color[4]){
	int32_t ix = x >> 16;
	int32_t iy = y >> 16;
	uint32_t px = x & 0xffff;
	uint32_t py = y & 0xffff;
	uint32_t qx = 0x10000u - px;
	uint32_t qy = 0x10000u - py;

	int32_t q1 = (qx * qy) >> 16;
	int32_t q2 = (qx * py) >> 16;
	int32_t q3 = (px * qy) >> 16;
	int32_t q4 = (px * py) >> 16;

	BitmapExactSample(source, ix, iy, q1, color);
	BitmapExactSample(source, ix, iy + 1, q2, color);
	BitmapExactSample(source, ix + 1, iy, q3, color);
	BitmapExactSample(source, ix + 1, iy + 1, q4, color);

	for (int i = 0; i < 4; i++){
		color[i] = color[i] >> 16;
	}
}

static void BitmapAADraw(BitmapData* self, BitmapData* source, const jfl::matrix2d& mat){
	if (mat.b != 0 || mat.c != 0){
		jfl::raise_message("draw with rotation is not supported");
	}

	int32_t x1 = (int32_t)(mat.tx * 0x10000);
	int32_t y1 = (int32_t)(mat.ty * 0x10000);
	int32_t x2 = (int32_t)((mat.tx + mat.a * source->width) * 0x10000);
	int32_t y2 = (int32_t)((mat.ty + mat.d * source->height) * 0x10000);
	
	int32_t source_x1 = x1;
	int32_t source_x2 = x2;
	int32_t source_y1 = y1;
	int32_t source_y2 = y2;

	int32_t target_width = self->width * 0x10000;
	int32_t target_height = self->height * 0x10000;

	int32_t source_width = source->width * 0x10000;
	int32_t source_height = source->height * 0x10000;
	int32_t draw_width = (x2 - x1);
	int32_t draw_height = (y2 - y1);

	if (x1 < 0){
		x1 = 0;
	}
	if (y1 < 0){
		y1 = 0;
	}
	if (x2 > target_width){
		x2 = target_width;
	}
	if (y2 > target_height){
		y2 = target_height;
	}

	int32_t from_y = (y1 / 0x10000);
	int32_t to_y = ((y2 + 0xffff) / 0x10000);

	int32_t from_x = (x1 / 0x10000);
	int32_t to_x = ((x2 + 0xffff) / 0x10000);

	bool transparent = self->transparent;

	uint32_t color[4] = {0, 0, 0, 0};
	for (int32_t y = from_y; y < to_y; y++){
		uint8_t* target_row = self->data + self->bytesPerRow * y;
		int32_t center_y = (y * 0x10000 + 0x8000);
		int32_t source_y = (int32_t)((int64_t)(center_y - source_y1) * source_height / draw_height);
		for (int32_t x = from_x; x < to_x; x++){
			int32_t center_x = (x * 0x10000 + 0x8000);
			int32_t source_x = (int32_t)((int64_t)(center_x - source_x1) * source_width / draw_width);
			BitmapAASample(source, source_x - 0x8000, source_y - 0x8000, color);
			uint32_t src_a = color[3];
			uint32_t src_r = color[2];
			uint32_t src_g = color[1];
			uint32_t src_b = color[0];

			uint32_t target_a, target_r, target_g, target_b;

			uint8_t* target_pixel = target_row + x * self->bytesPerPixel;
			if (self->transparent){
				target_a = target_pixel[3];
				target_r = target_pixel[2];
				target_g = target_pixel[1];
				target_b = target_pixel[0];
			}else{
				target_a = 0xff;
				target_r = target_pixel[2];
				target_g = target_pixel[1];
				target_b = target_pixel[0];
			}

			uint32_t p = src_a;
			uint32_t q = 0xff - p;

			uint32_t a = target_a + (src_a * (0xff - target_a) + 0x7f) / 0xff;
			uint32_t r = (target_r * q + src_r * p + 0x7f) / 0xff;
			uint32_t g = (target_g * q + src_g * p + 0x7f) / 0xff;
			uint32_t b = (target_b * q + src_b * p + 0x7f) / 0xff;

			if (self->transparent) {
				target_pixel[3] = a;
				target_pixel[2] = r;
				target_pixel[1] = g;
				target_pixel[0] = b;
			} else {
				target_pixel[2] = r;
				target_pixel[1] = g;
				target_pixel[0] = b;
			}
		}
	}
}

struct DropShadowDescriptor{
	int32_t off_x, off_y;
	int32_t blur_x, blur_y;
	uint32_t color;
	int32_t repeats;
	uint32_t alpha;
	uint32_t strength;
};

static uint32_t BlurAccum[128];

template<bool Horizontal, bool BlurOdd>
static void BitmapBlurAxis(uint32_t* field, int32_t width, int32_t height, int32_t blur){
	int32_t axis_size = Horizontal ? width : height;
	int32_t xaxis_size = Horizontal ? height : width;
	int32_t mod = blur;
	int32_t half = blur >> 1;
	if (!BlurOdd){
		mod++;
	}
#define pixel(i, j) (Horizontal ? field[(i) * width + (j)] : field[(j) * width + (i)])
	for (int32_t i = 0; i < xaxis_size; i++) {
		uint32_t acc = 0;
		uint32_t edges = 0;
		memset(BlurAccum, 0, sizeof(uint32_t) * mod);
		for (int32_t j = 0; j <= half; j++) {
			BlurAccum[j + half] = pixel(i, j);
			acc += BlurAccum[j + half];
		}
		if (!BlurOdd) {
			edges = BlurAccum[mod - 1];
		}
		int32_t p = mod - 1;
		for (int32_t j = 0; j < axis_size; j++){
			uint32_t next;
			int32_t sample = j + half + 1;
			if (sample >= axis_size){
				next = 0;
			}else{
				next = pixel(i, sample);
			}
			if (BlurOdd) {
				pixel(i, j) = acc / mod;
				p++;
				if (p >= mod) {
					p -= mod;
				}
				acc -= BlurAccum[p];
				BlurAccum[p] = next;
				acc += next;
			}else{
				pixel(i, j) = (acc - (edges >> 1)) / mod;
				edges += next;
				edges -= BlurAccum[p];
				p++;
				if (p >= mod) {
					p -= mod;
				}
				edges -= BlurAccum[p];

				acc -= BlurAccum[p];
				BlurAccum[p] = next;
				acc += next;

				int32_t q = p + 1;
				if (q >= mod) {
					q -= mod;
				}
				edges += BlurAccum[q];
			}
			
		}
	}
#undef pixel
}

static void BitmapToField(BitmapData* target, int32_t off_x, int32_t off_y, uint32_t* field){
	int32_t source_x1 = 0;
	int32_t source_x2 = target->width;
	int32_t source_y1 = 0;
	int32_t source_y2 = target->height;

	int32_t dest_x1 = off_x;
	int32_t dest_x2 = target->width + off_x;
	int32_t dest_y1 = off_y;
	int32_t dest_y2 = target->height + off_y;

	if (dest_x1 < 0){
		int32_t off = -dest_x1;
		dest_x1 += off;
		source_x1 += off;
	}

	if (dest_x2 > target->width) {
		int32_t off = target->width - dest_x2;
		dest_x2 += off;
		source_x2 += off;
	}

	if (dest_y1 < 0) {
		int32_t off = -dest_y1;
		dest_y1 += off;
		source_y1 += off;
	}

	if (dest_y2 > target->height) {
		int32_t off = target->height - dest_y2;
		dest_y2 += off;
		source_y2 += off;
	}

	for (int32_t y = source_y1; y < source_y2; y++) {
		uint8_t* source_row = target->data + y * target->bytesPerRow;
		uint32_t* dest_row = field + (y + off_y) * target->width;
		for (int32_t x = source_x1; x < source_x2; x++) {
			uint32_t alpha = source_row[x * 4 + 3];
			dest_row[x + off_x] = alpha * 0x10000u / 255u;
		}
	}
}

static uint8_t mul_fixed_0_8(uint8_t x, uint8_t y) {
	return (((uint32_t)x) * y  + 0x7fu) / 0xffu;
}

static uint32_t mul_fixed_16_16(uint32_t x, uint32_t y) {
	return (uint32_t)(((uint64_t)x * y) >> 16u);
}

static void BitmapImprintField(uint32_t* field, BitmapData* target, uint32_t strength, uint32_t alpha, uint32_t color){
	int32_t n = target->width * target->height;
	uint8_t* source = target->data;

	uint32_t src_b = (color >> 0) & 0xff;
	uint32_t src_g = (color >> 8) & 0xff;
	uint32_t src_r = (color >> 16) & 0xff;

	for (int32_t i = 0; i < n; i++) {
		uint32_t dst_r = source[i * 4 + 0];
		uint32_t dst_g = source[i * 4 + 1];
		uint32_t dst_b = source[i * 4 + 2];
		uint32_t dst_a = source[i * 4 + 3];

		uint32_t imprint = std::min(0x10000u, mul_fixed_16_16(field[i], strength));
		uint32_t src_a = mul_fixed_0_8(255 - dst_a, (mul_fixed_16_16(imprint, alpha) * 255) >> 16u);
		uint32_t mix_a = dst_a + src_a;
		
		uint32_t dst_factor;
		if (mix_a == 0) {
			dst_factor = 255;
		}else{
			dst_factor = dst_a * 255u / mix_a;
		}
		uint32_t mix_dst = dst_a + mul_fixed_0_8(255u - dst_a, dst_factor);
		uint32_t mix_src = 255u - mix_dst;

		source[i * 4 + 0] = mul_fixed_0_8(src_r, mix_src) + mul_fixed_0_8(dst_r, mix_dst);
		source[i * 4 + 1] = mul_fixed_0_8(src_g, mix_src) + mul_fixed_0_8(dst_g, mix_dst);
		source[i * 4 + 2] = mul_fixed_0_8(src_b, mix_src) + mul_fixed_0_8(dst_b, mix_dst);
		source[i * 4 + 3] = mix_a;
	}
}

static void BitmapApplyDropShadow(BitmapData* target, DropShadowDescriptor* filter){
	size_t field_size = target->width * target->height * sizeof(uint32_t);
	uint32_t* field = (uint32_t*)jfl::GetScratchSpace(field_size);
	memset(field, 0, field_size);
	BitmapToField(target, filter->off_x, filter->off_y, field);
	for (int i = 0; i < filter->repeats; i++) {
		if (filter->blur_x > 1) {
			if (filter->blur_x & 1) {
				BitmapBlurAxis<true, true>(field, target->width, target->height, filter->blur_x);
			} else {
				BitmapBlurAxis<true, false>(field, target->width, target->height, filter->blur_x);
			}
		}
		if (filter->blur_y > 1) {
			if (filter->blur_y & 1) {
				BitmapBlurAxis<false, true>(field, target->width, target->height, filter->blur_y);
			} else {
				BitmapBlurAxis<false, false>(field, target->width, target->height, filter->blur_y);
			}
		}
	}
	BitmapImprintField(field, target, filter->strength, filter->alpha, filter->color);
}

static void BitmapApplyFilter(fl::flash::display::BitmapData* bitmap, fl::flash::filters::BitmapFilter* filter){
	if ( jfl::Object_is_Class((jfl::Object*)filter, &DropShadowFilter::jfl_Class) ){
		DropShadowFilter* drop_shadow = (DropShadowFilter*)filter;
		DropShadowDescriptor desc;
		desc.off_x = (int32_t)round(cos(drop_shadow->jfl_p__angle) * drop_shadow->jfl_p__distance);
		desc.off_y = (int32_t)round(sin(drop_shadow->jfl_p__angle) * drop_shadow->jfl_p__distance);
		desc.blur_x = (int32_t)round(drop_shadow->jfl_p__blurX);
		desc.blur_y = (int32_t)round(drop_shadow->jfl_p__blurY);
		desc.color = drop_shadow->jfl_p__color;
		desc.repeats = drop_shadow->jfl_p__quality;
		desc.alpha = (uint32_t)(drop_shadow->jfl_p__alpha * 0x10000u);
		desc.strength = (uint32_t)(drop_shadow->jfl_p__strength * 0x10000u);
		BitmapApplyDropShadow(bitmap, &desc);
	}else{
		jfl::raise_message("invalid filter applied");
	}
}

void BitmapData::drawWithQuality(
	fl::flash::display::BitmapData* self,
	jfl::I<fl::flash::display::IBitmapDrawable> drawable, 
	fl::flash::geom::Matrix* matrix, 
	fl::flash::geom::ColorTransform* colorTransform,
	jfl::String blendMode, 
	fl::flash::geom::Rectangle* clipRect, 
	jfl::Boolean smoothing, 
	jfl::String quality
){
	if (colorTransform){
		raise_message("colorTransform is not supported");
	}
	if (blendMode.tag != type_tag::Null) {
		raise_message("blendMode is not supported");
	}
	if (clipRect) {
		raise_message("clipRect is not supported");
	}
	// smoothing is ignored
	// quality is ignored

	matrix2d draw_matrix;
	if (matrix) {
		draw_matrix.a = matrix->a;
		draw_matrix.b = matrix->b;
		draw_matrix.tx = matrix->tx;
		draw_matrix.c = matrix->c;
		draw_matrix.d = matrix->d;
		draw_matrix.ty = matrix->ty;
	}

	jfl::Object* displayObject = Object_as_Class(drawable.o, &DisplayObject::jfl_Class);
	if (displayObject){
		auto draw_display_object = [&](const auto& yy, DisplayObject* displayObject, const matrix2d& matrix) -> void{
			// do actual drawing
			TextField* textField = (TextField*)Object_as_Class((jfl::Object*)displayObject, &TextField::jfl_Class);
			if (textField){
				TextField::jfl_renderTo(textField, self, matrix);
			}

			DisplayObjectContainer* container = (DisplayObjectContainer*)Object_as_Class((jfl::Object*)displayObject, &DisplayObjectContainer::jfl_Class);
			if (container){
				auto children = container->jfl_p_children;
				for (UInt i = 0; i < children->length; i++){
					auto child = (DisplayObject*)children->ptr[i];
					matrix2d child_matrix = matrix;
					child->tform.recompose();
					child_matrix.prepend(child->tform.matrix);
					yy(yy, (DisplayObject*)children->ptr[i], child_matrix);
				}
			}

			if (displayObject->jfl_p_filter) {
				BitmapApplyFilter(self, displayObject->jfl_p_filter);
			}
		};

		draw_display_object(draw_display_object, (DisplayObject*)displayObject, draw_matrix);
	}else{
		jfl::Object* bitmapData = Object_as_Class(drawable.o, &BitmapData::jfl_Class);
		if (bitmapData){
			BitmapAADraw(self, (BitmapData*)bitmapData, draw_matrix);
		}else{
			jfl::raise_message("unsupported drawable");
		}
	}
}
void BitmapData::fillRect(fl::flash::display::BitmapData* self, fl::flash::geom::Rectangle* rect, jfl::UInt color){
	if (!rect){
		raise_nullarg();
	}
	Int x1 = Number_to_int(rect->x);
	Int y1 = Number_to_int(rect->y);
	Int x2 = Number_to_int(rect->x + rect->width);
	Int y2 = Number_to_int(rect->y + rect->height);

	uint8_t r = (color >> 0) & 0xff;
	uint8_t g = (color >> 8) & 0xff;
	uint8_t b = (color >> 16) & 0xff;
	uint8_t a = (color >> 24) & 0xff;

	for (Int y = y1; y < y2; y++){
		uint8_t* row_ptr = self->data + self->bytesPerRow * y;
		for (Int x = x1; x < x2; x++){
			if (self->bytesPerPixel == 4){
				row_ptr[x * 4] = r;
				row_ptr[x * 4 + 1] = g;
				row_ptr[x * 4 + 2] = b;
				row_ptr[x * 4 + 3] = a;
			}else if (self->bytesPerPixel == 3){
				row_ptr[x * 3] = r;
				row_ptr[x * 3 + 1] = g;
				row_ptr[x * 3 + 2] = b;
			}else{
				jfl_unreachable;
			}
		}
	}
}
void BitmapData::floodFill(fl::flash::display::BitmapData* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2, jfl::UInt jfl_a_param3){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* BitmapData::generateFilterRect(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1, fl::flash::filters::BitmapFilter* jfl_a_param2){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* BitmapData::getColorBoundsRect(fl::flash::display::BitmapData* jfl_t, jfl::UInt jfl_a_param1, jfl::UInt jfl_a_param2, jfl::Boolean jfl_a_param3){
	jfl_notimpl;
}
fl::flash::utils::ByteArray* BitmapData::getPixels(fl::flash::display::BitmapData* bitmap, fl::flash::geom::Rectangle* rect){
	using fl::flash::utils::ByteArray;
	ByteArray* byteArray = ByteArray::jfl_New();

	auto& data = byteArray->data;
	Int x_begin = (Int)(std::max(0.0, rect->x));
	Int y_begin = (Int)(std::max(0.0, rect->y));
	Int x_end = std::max(x_begin, std::min(bitmap->width, (Int)(rect->x + rect->width)));
	Int y_end = std::max(y_begin, std::min(bitmap->height, (Int)(rect->y + rect->height)));
	Int pixels_count = (x_end - x_begin) * (y_end - y_begin);
	data.resize(pixels_count * 4);
	uint8_t* buffer = data.data();
	for (Int y = y_begin; y < y_end; y++) {
		for (Int x = x_begin; x < x_end; x++) {
			UInt pixel = BitmapData::getPixel32(bitmap, x, y);
			uint8_t r = (pixel >> 0) & 0xffu;
			uint8_t g = (pixel >> 8) & 0xffu;
			uint8_t b = (pixel >> 16) & 0xffu;
			uint8_t a = (pixel >> 24) & 0xffu;
			buffer[0] = a;
			buffer[1] = r;
			buffer[2] = g;
			buffer[3] = b;
			buffer += 4;
		}
	}

	return byteArray;
}
void BitmapData::copyPixelsToByteArray(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1, fl::flash::utils::ByteArray* jfl_a_param2){
	jfl_notimpl;
}
jfl::Vector<jfl::UInt>* BitmapData::getVector(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean BitmapData::hitTest(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Point* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::Any jfl_a_param3, fl::flash::geom::Point* jfl_a_param4, jfl::UInt jfl_a_param5){
	jfl_notimpl;
}
void BitmapData::merge(fl::flash::display::BitmapData* jfl_t, fl::flash::display::BitmapData* jfl_a_param1, fl::flash::geom::Rectangle* jfl_a_param2, fl::flash::geom::Point* jfl_a_param3, jfl::UInt jfl_a_param4, jfl::UInt jfl_a_param5, jfl::UInt jfl_a_param6, jfl::UInt jfl_a_param7){
	jfl_notimpl;
}
void BitmapData::noise(fl::flash::display::BitmapData* jfl_t, jfl::Int jfl_a_param1, jfl::UInt jfl_a_param2, jfl::UInt jfl_a_param3, jfl::UInt jfl_a_param4, jfl::Boolean jfl_a_param5){
	jfl_notimpl;
}
void BitmapData::paletteMap(fl::flash::display::BitmapData* jfl_t, fl::flash::display::BitmapData* jfl_a_param1, fl::flash::geom::Rectangle* jfl_a_param2, fl::flash::geom::Point* jfl_a_param3, fl::Array* jfl_a_param4, fl::Array* jfl_a_param5, fl::Array* jfl_a_param6, fl::Array* jfl_a_param7){
	jfl_notimpl;
}
void BitmapData::perlinNoise(fl::flash::display::BitmapData* jfl_t, jfl::Number jfl_a_param1, jfl::Number jfl_a_param2, jfl::UInt jfl_a_param3, jfl::Int jfl_a_param4, jfl::Boolean jfl_a_param5, jfl::Boolean jfl_a_param6, jfl::UInt jfl_a_param7, jfl::Boolean jfl_a_param8, fl::Array* jfl_a_param9){
	jfl_notimpl;
}
jfl::Int BitmapData::pixelDissolve(fl::flash::display::BitmapData* jfl_t, fl::flash::display::BitmapData* jfl_a_param1, fl::flash::geom::Rectangle* jfl_a_param2, fl::flash::geom::Point* jfl_a_param3, jfl::Int jfl_a_param4, jfl::Int jfl_a_param5, jfl::UInt jfl_a_param6){
	jfl_notimpl;
}
void BitmapData::scroll(fl::flash::display::BitmapData* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	jfl_notimpl;
}
void BitmapData::setPixels(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1, fl::flash::utils::ByteArray* jfl_a_param2){
	jfl_notimpl;
}
void BitmapData::setVector(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1, jfl::Vector<jfl::UInt>* jfl_a_param2){
	jfl_notimpl;
}
jfl::UInt BitmapData::threshold(fl::flash::display::BitmapData* jfl_t, fl::flash::display::BitmapData* jfl_a_param1, fl::flash::geom::Rectangle* jfl_a_param2, fl::flash::geom::Point* jfl_a_param3, jfl::String jfl_a_param4, jfl::UInt jfl_a_param5, jfl::UInt jfl_a_param6, jfl::UInt jfl_a_param7, jfl::Boolean jfl_a_param8){
	jfl_notimpl;
}
void BitmapData::lock(fl::flash::display::BitmapData* jfl_t){
	// do nothing
}
void BitmapData::unlock(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1){
	// do nothing
}
jfl::Vector<jfl::Object*>* BitmapData::histogram(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::utils::ByteArray* BitmapData::encode(fl::flash::display::BitmapData* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1, jfl::Any jfl_a_param2, fl::flash::utils::ByteArray* jfl_a_param3){
	jfl_notimpl;
}
}
#endif
