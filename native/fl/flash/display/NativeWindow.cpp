#if __has_include(<fl/flash/display/NativeWindow.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display/NativeWindow.h>
namespace fl::flash::display{
void NativeWindow::jfl_p__init(fl::flash::display::NativeWindow* jfl_t, fl::flash::display::NativeWindowInitOptions* jfl_a_param1, jfl::Boolean jfl_a_param2){
	jfl_notimpl;
}
fl::flash::display::Stage* NativeWindow::jfl_g_stage(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::close(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_g_closed(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pg__minimized(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_p__orderToFront(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_p__orderToBack(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_p__orderInFrontOf(fl::flash::display::NativeWindow* jfl_t, fl::flash::display::NativeWindow* jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_p__orderInBackOf(fl::flash::display::NativeWindow* jfl_t, fl::flash::display::NativeWindow* jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_g_alwaysInFront(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_s_alwaysInFront(fl::flash::display::NativeWindow* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Point* NativeWindow::globalToScreen(fl::flash::display::NativeWindow* jfl_t, fl::flash::geom::Point* jfl_a_param1){
	jfl_notimpl;
}
void NativeWindow::notifyUser(fl::flash::display::NativeWindow* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
fl::flash::display::NativeWindow* NativeWindow::jfl_pg__owner(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__owner(fl::flash::display::NativeWindow* jfl_t, fl::flash::display::NativeWindow* jfl_a_param1){
	jfl_notimpl;
}
jfl::Vector<jfl::Object*>* NativeWindow::jfl_pg__ownedWindows(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Vector<jfl::Object*>* NativeWindow::jfl_p__listDisplayedWindowsInFrontToBackZOrder(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::String NativeWindow::jfl_pg__renderMode(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__title(fl::flash::display::NativeWindow* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String NativeWindow::jfl_pg__title(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__bounds(fl::flash::display::NativeWindow* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* NativeWindow::jfl_pg__bounds(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__x(fl::flash::display::NativeWindow* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__y(fl::flash::display::NativeWindow* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__width(fl::flash::display::NativeWindow* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__height(fl::flash::display::NativeWindow* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__minSize(fl::flash::display::NativeWindow* jfl_t, fl::flash::geom::Point* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Point* NativeWindow::jfl_pg__minSize(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__maxSize(fl::flash::display::NativeWindow* jfl_t, fl::flash::geom::Point* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Point* NativeWindow::jfl_pg__maxSize(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::String NativeWindow::jfl_pg__displayState(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_p__minimize(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_p__maximize(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_p__restore(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pg__active(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_p__activate(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::String NativeWindow::jfl_pg__type(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::String NativeWindow::jfl_pg__systemChrome(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pg__transparent(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
void NativeWindow::jfl_ps__visible(fl::flash::display::NativeWindow* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pg__visible(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pg__minimizable(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pg__maximizable(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pg__resizable(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_p__startMove(fl::flash::display::NativeWindow* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_p__startResize(fl::flash::display::NativeWindow* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pS__checkSupported(){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pS_initSupportsMenu(){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pS_initSupportsNotifyUser(){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pSg__supportsTransparency(){
	jfl_notimpl;
}
fl::flash::geom::Point* NativeWindow::jfl_pS_initSystemMinSize(){
	jfl_notimpl;
}
fl::flash::geom::Point* NativeWindow::jfl_Sg_systemMaxSize(){
	jfl_notimpl;
}
void NativeWindow::jfl_pS__checkAccess(){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pS__isRootPlayer(){
	jfl_notimpl;
}
jfl::Boolean NativeWindow::jfl_pS__restoreIsAsync(){
	jfl_notimpl;
}
}
#endif
