#if __has_include(<fl/flash/display/DisplayObject.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display/DisplayObject.h>
namespace fl::flash::display{
jfl::Number DisplayObject::jfl_g_x(fl::flash::display::DisplayObject* self){
	return self->tform.matrix.tx;
}
void DisplayObject::jfl_s_x(fl::flash::display::DisplayObject* self, jfl::Number value){
	self->tform.matrix.tx = value;
}
jfl::Number DisplayObject::jfl_g_y(fl::flash::display::DisplayObject* self){
	return self->tform.matrix.ty;
}
void DisplayObject::jfl_s_y(fl::flash::display::DisplayObject* self, jfl::Number value){
	self->tform.matrix.ty = value;
}
jfl::Number DisplayObject::jfl_g_z(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_z(fl::flash::display::DisplayObject* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_scaleX(fl::flash::display::DisplayObject* self){
	self->tform.decompose();
	return self->tform.decomp.sx;
}
void DisplayObject::jfl_s_scaleX(fl::flash::display::DisplayObject* self, jfl::Number value){
	self->tform.decompose();
	self->tform.decomp.sx = value;
	self->tform.reset_matrix();
}
jfl::Number DisplayObject::jfl_g_scaleY(fl::flash::display::DisplayObject* self){
	self->tform.decompose();
	return self->tform.decomp.sy;
}
void DisplayObject::jfl_s_scaleY(fl::flash::display::DisplayObject* self, jfl::Number value){
	self->tform.decompose();
	self->tform.decomp.sy = value;
	self->tform.reset_matrix();
}
jfl::Number DisplayObject::jfl_g_scaleZ(fl::flash::display::DisplayObject* self){
	jfl_notimpl;
}
void DisplayObject::jfl_s_scaleZ(fl::flash::display::DisplayObject* self, jfl::Number value){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_mouseX(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_mouseY(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_rotation(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_rotation(fl::flash::display::DisplayObject* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_rotationX(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_rotationX(fl::flash::display::DisplayObject* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_rotationY(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_rotationY(fl::flash::display::DisplayObject* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_rotationZ(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_rotationZ(fl::flash::display::DisplayObject* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_alpha(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_alpha(fl::flash::display::DisplayObject* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_width(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_width(fl::flash::display::DisplayObject* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number DisplayObject::jfl_g_height(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_height(fl::flash::display::DisplayObject* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean DisplayObject::jfl_g_cacheAsBitmap(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_cacheAsBitmap(fl::flash::display::DisplayObject* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Any DisplayObject::jfl_g_opaqueBackground(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_opaqueBackground(fl::flash::display::DisplayObject* jfl_t, jfl::Any jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* DisplayObject::jfl_g_scrollRect(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_scrollRect(fl::flash::display::DisplayObject* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1){
	jfl_notimpl;
}
jfl::String DisplayObject::jfl_g_blendMode(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_blendMode(fl::flash::display::DisplayObject* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Transform* DisplayObject::jfl_g_transform(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_transform(fl::flash::display::DisplayObject* jfl_t, fl::flash::geom::Transform* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* DisplayObject::jfl_g_scale9Grid(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_scale9Grid(fl::flash::display::DisplayObject* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Point* DisplayObject::globalToLocal(fl::flash::display::DisplayObject* jfl_t, fl::flash::geom::Point* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Point* DisplayObject::localToGlobal(fl::flash::display::DisplayObject* jfl_t, fl::flash::geom::Point* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* DisplayObject::getBounds(fl::flash::display::DisplayObject* jfl_t, fl::flash::display::DisplayObject* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* DisplayObject::getRect(fl::flash::display::DisplayObject* jfl_t, fl::flash::display::DisplayObject* jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean DisplayObject::jfl_p__hitTest(fl::flash::display::DisplayObject* jfl_t, jfl::Boolean jfl_a_param1, jfl::Number jfl_a_param2, jfl::Number jfl_a_param3, jfl::Boolean jfl_a_param4, fl::flash::display::DisplayObject* jfl_a_param5){
	jfl_notimpl;
}
fl::flash::accessibility::AccessibilityProperties* DisplayObject::jfl_g_accessibilityProperties(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_accessibilityProperties(fl::flash::display::DisplayObject* jfl_t, fl::flash::accessibility::AccessibilityProperties* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Vector3D* DisplayObject::globalToLocal3D(fl::flash::display::DisplayObject* jfl_t, fl::flash::geom::Point* jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Point* DisplayObject::local3DToGlobal(fl::flash::display::DisplayObject* jfl_t, fl::flash::geom::Vector3D* jfl_a_param1){
	jfl_notimpl;
}
jfl::Any DisplayObject::jfl_g_metaData(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_metaData(fl::flash::display::DisplayObject* jfl_t, jfl::Any jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Matrix* DisplayObject::jfl_g_cacheAsBitmapMatrix(fl::flash::display::DisplayObject* jfl_t){
	jfl_notimpl;
}
void DisplayObject::jfl_s_cacheAsBitmapMatrix(fl::flash::display::DisplayObject* jfl_t, fl::flash::geom::Matrix* jfl_a_param1){
	jfl_notimpl;
}
}
#endif
