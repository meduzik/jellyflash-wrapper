#if __has_include(<fl/flash/display/NativeWindowInitOptions.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display/NativeWindowInitOptions.h>
namespace fl::flash::display{
jfl::String NativeWindowInitOptions::jfl_g_systemChrome(fl::flash::display::NativeWindowInitOptions* jfl_t){
	jfl_notimpl;
}
void NativeWindowInitOptions::jfl_s_systemChrome(fl::flash::display::NativeWindowInitOptions* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean NativeWindowInitOptions::jfl_g_transparent(fl::flash::display::NativeWindowInitOptions* jfl_t){
	jfl_notimpl;
}
void NativeWindowInitOptions::jfl_s_transparent(fl::flash::display::NativeWindowInitOptions* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::String NativeWindowInitOptions::jfl_g_type(fl::flash::display::NativeWindowInitOptions* jfl_t){
	jfl_notimpl;
}
void NativeWindowInitOptions::jfl_s_type(fl::flash::display::NativeWindowInitOptions* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean NativeWindowInitOptions::jfl_g_minimizable(fl::flash::display::NativeWindowInitOptions* jfl_t){
	jfl_notimpl;
}
void NativeWindowInitOptions::jfl_s_minimizable(fl::flash::display::NativeWindowInitOptions* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean NativeWindowInitOptions::jfl_g_maximizable(fl::flash::display::NativeWindowInitOptions* jfl_t){
	jfl_notimpl;
}
void NativeWindowInitOptions::jfl_s_maximizable(fl::flash::display::NativeWindowInitOptions* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean NativeWindowInitOptions::jfl_g_resizable(fl::flash::display::NativeWindowInitOptions* jfl_t){
	jfl_notimpl;
}
void NativeWindowInitOptions::jfl_s_resizable(fl::flash::display::NativeWindowInitOptions* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
fl::flash::display::NativeWindow* NativeWindowInitOptions::jfl_g_owner(fl::flash::display::NativeWindowInitOptions* jfl_t){
	jfl_notimpl;
}
void NativeWindowInitOptions::jfl_s_owner(fl::flash::display::NativeWindowInitOptions* jfl_t, fl::flash::display::NativeWindow* jfl_a_param1){
	jfl_notimpl;
}
jfl::String NativeWindowInitOptions::jfl_g_renderMode(fl::flash::display::NativeWindowInitOptions* jfl_t){
	jfl_notimpl;
}
void NativeWindowInitOptions::jfl_s_renderMode(fl::flash::display::NativeWindowInitOptions* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
}
#endif
