#if __has_include(<fl/flash/display/Stage.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display/Stage.h>
namespace fl::flash::display{
jfl::Number Stage::jfl_g_frameRate(fl::flash::display::Stage* jfl_t){
	return 60;
}
void Stage::jfl_s_frameRate(fl::flash::display::Stage* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void Stage::invalidate(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::Boolean Stage::jfl_g_showDefaultContextMenu(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_showDefaultContextMenu(fl::flash::display::Stage* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
fl::flash::display::InteractiveObject* Stage::jfl_g_focus(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_focus(fl::flash::display::Stage* jfl_t, fl::flash::display::InteractiveObject* jfl_a_param1){
	jfl_notimpl;
}
jfl::String Stage::jfl_g_colorCorrection(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_colorCorrection(fl::flash::display::Stage* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String Stage::jfl_g_colorCorrectionSupport(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::Boolean Stage::isFocusInaccessible(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::Boolean Stage::jfl_g_stageFocusRect(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_stageFocusRect(fl::flash::display::Stage* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::String Stage::jfl_g_quality(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_quality(fl::flash::display::Stage* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String Stage::jfl_g_displayState(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_displayState(fl::flash::display::Stage* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* Stage::jfl_g_fullScreenSourceRect(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_fullScreenSourceRect(fl::flash::display::Stage* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean Stage::jfl_g_mouseLock(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_mouseLock(fl::flash::display::Stage* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Vector<jfl::Object*>* Stage::jfl_g_stageVideos(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::UInt Stage::jfl_g_color(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_color(fl::flash::display::Stage* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
jfl::UInt Stage::jfl_g_fullScreenWidth(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::UInt Stage::jfl_g_fullScreenHeight(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::Boolean Stage::jfl_g_wmodeGPU(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* Stage::jfl_g_softKeyboardRect(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::Boolean Stage::jfl_g_allowsFullScreen(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::Boolean Stage::jfl_g_allowsFullScreenInteractive(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
fl::flash::display::DisplayObject* Stage::removeChildAt(fl::flash::display::Stage* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
void Stage::swapChildrenAt(fl::flash::display::Stage* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	jfl_notimpl;
}
jfl::Number Stage::jfl_g_browserZoomFactor(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::String Stage::jfl_g_displayContextInfo(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_p__assignFocus(fl::flash::display::Stage* jfl_t, fl::flash::display::InteractiveObject* jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
fl::flash::display::NativeWindow* Stage::jfl_pg__nativeWindow(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::String Stage::jfl_g_orientation(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_ps__orientation(fl::flash::display::Stage* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void Stage::jfl_ps__aspectRatio(fl::flash::display::Stage* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String Stage::jfl_g_deviceOrientation(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::Boolean Stage::jfl_g_autoOrients(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_p_setAutoOrients(fl::flash::display::Stage* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Vector<jfl::String>* Stage::jfl_g_supportedOrientations(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
jfl::Boolean Stage::jfl_g_vsyncEnabled(fl::flash::display::Stage* jfl_t){
	jfl_notimpl;
}
void Stage::jfl_s_vsyncEnabled(fl::flash::display::Stage* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean Stage::jfl_Sg_supportsOrientationChange(){
	jfl_notimpl;
}
}
#endif
