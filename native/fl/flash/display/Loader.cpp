#if __has_include(<fl/flash/display/Loader.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display/Loader.h>
#include <fl/flash/display/Bitmap.h>
#include <fl/flash/display/BitmapData.h>
#include <fl/flash/display/PixelSnapping.h>
#include <fl/flash/events/EventDispatcher.h>
#include <fl/flash/events/IOErrorEvent.h>
#include <fl/flash/events/Event.h>
#include <fl/flash/utils/ByteArray.h>
#include <jfl/app/Loop.h>
#include <jfl/defer.h>
extern "C"{
#include <jpeglib.h>
#include <png.h>
#include <src/webp/decode.h>
}
namespace fl::flash::display{
using namespace events;
using namespace jfl;

static const size_t PNGSignatureLength = 8;
static const size_t JPEGSignatureLength = 4;
static const size_t WEBPSignatureLength = 12;

static const uint8_t JPEGSignatureJFIF[4] = { 0xff, 0xd8, 0xff, 0xe0 };
static const uint8_t WEBPSignature1[4] = { 'R', 'I', 'F', 'F' };
static const uint8_t WEBPSignature2[4] = { 'W', 'E', 'B', 'P' };

static png_voidp malloc_fn(png_structp png_ptr, png_size_t size){
	void* ptr = jfl::unmanaged_allocate(size + sizeof(size_t));
	*((size_t*)ptr) = size;
	return ((size_t*)ptr) + 1;
}

static void free_fn(png_structp png_ptr, png_voidp ptr){
	void* orig_ptr = ((size_t*)ptr) - 1;
	jfl::unmanaged_free(orig_ptr, sizeof(size_t) + *((size_t*)orig_ptr));
}

struct ImageLoaderData{
	ImageLoaderData(Loader* loader, jfl::um_vec<uint8_t>&& data):
		loader(loader),
		data(std::move(data)),
		ptr(this->data.data())
	{
	}

	void load(){
		if (is_png()) {
			return load_png();
		} else if (is_jpeg()) {
			return load_jpeg();
		} else if (is_webp()) {
			return load_webp();
		}else{
			raiseIOError("unknown image format");
		}
	}

	bool is_jpeg(){
		if (!check(JPEGSignatureLength)) {
			return false;
		}
		if ( !memcmp(ptr, JPEGSignatureJFIF, sizeof(JPEGSignatureJFIF)) ){
			return true;
		}
		return false;
	}

	void load_jpeg(){
		jpeg_decompress_struct jpeg_struct;
		jpeg_error_mgr jpeg_err;
		jpeg_struct.err = jpeg_std_error(&jpeg_err);
		jpeg_create_decompress(&jpeg_struct);
		auto destroy = defer([&](){
			jpeg_destroy_decompress(&jpeg_struct);
		});
		jpeg_mem_src(&jpeg_struct, data.data(), data.size());
		int ret = jpeg_read_header(&jpeg_struct, TRUE);
		if ( ret != 1 ) {
			raiseIOError("invalid jpeg header");
		}
		jpeg_struct.out_color_space = JCS_RGB;
		jpeg_start_decompress(&jpeg_struct);
		int width = jpeg_struct.output_width;
		int height = jpeg_struct.output_height;
		int pixel_size = jpeg_struct.output_components;

		size_t bytesPerPixel = 3;
		size_t bytesPerRow = (width * bytesPerPixel + 3) & ~3;
		uint8_t* data = (uint8_t*)unmanaged_allocate(height * bytesPerRow);

		for (int rowIdx = 0; rowIdx < height; ++rowIdx) {
			uint8_t* row_base = data + rowIdx * bytesPerRow;
			jpeg_read_scanlines(&jpeg_struct, &row_base, 1);
		}

		jpeg_finish_decompress(&jpeg_struct);
		jpeg_destroy_decompress(&jpeg_struct);

		WaitMainLoopTask([&](){
			BitmapData* bitmapData = jfl::gc_new<BitmapData>();
			bitmapData->jfl_vtable = &jfl_V_BitmapData;
			bitmapData->width = width;
			bitmapData->height = height;
			bitmapData->transparent = false;
			bitmapData->bytesPerPixel = bytesPerPixel;
			bitmapData->bytesPerRow = bytesPerRow;
			bitmapData->data = data;

			Bitmap* bitmap = Bitmap::jfl_New(bitmapData, PixelSnapping::jfl_S_NEVER, true);
			loader->jfl_p__content = (DisplayObject*)bitmap;
			DisplayObjectContainer::addChild((DisplayObjectContainer*)loader.get(), loader->jfl_p__content);
			Event* event = Event::jfl_New(Event::jfl_S_COMPLETE, false, false);
			EventDispatcher::dispatchEvent(
				(EventDispatcher*)loader->jfl_p__contentLoaderInfo,
				event
			);
		});
	}

	bool is_webp() {
		if (!check(WEBPSignatureLength)) {
			return false;
		}
		if (memcmp(ptr, WEBPSignature1, sizeof(WEBPSignature1))) {
			return false;
		}
		if (memcmp(ptr + 8, WEBPSignature2, sizeof(WEBPSignature2))) {
			return false;
		}
		return true;
	}

	void load_webp() {
		int width = 0, height = 0;
		if (!WebPGetInfo(data.data(), data.size(), &width, &height)) {
			return raiseIOError("invalid webp header");
		}

		WebPBitstreamFeatures features {};
		if (WebPGetFeatures(data.data(), data.size(), &features)) {
			return raiseIOError("invalid webp data");
		}
		bool transparent = features.has_alpha;
		size_t bytesPerPixel = (transparent ? 4 : 3);
		size_t bytesPerRow = (width * bytesPerPixel + 3) & ~3;
		uint8_t* output = (uint8_t*)unmanaged_allocate(height * bytesPerRow);
		if (transparent) {
			WebPDecodeRGBAInto(data.data(), data.size(), output, height * bytesPerRow, bytesPerRow);
		} else {
			WebPDecodeRGBInto(data.data(), data.size(), output, height * bytesPerRow, bytesPerRow);
		}

		WaitMainLoopTask([&]() {
			BitmapData* bitmapData = jfl::gc_new<BitmapData>();
			bitmapData->jfl_vtable = &jfl_V_BitmapData;
			bitmapData->width = width;
			bitmapData->height = height;
			bitmapData->transparent = transparent;
			bitmapData->bytesPerPixel = bytesPerPixel;
			bitmapData->bytesPerRow = bytesPerRow;
			bitmapData->data = output;
			Bitmap* bitmap = Bitmap::jfl_New(bitmapData, PixelSnapping::jfl_S_NEVER, true);
			loader->jfl_p__content = (DisplayObject*)bitmap;
			DisplayObjectContainer::addChild((DisplayObjectContainer*)loader.get(), loader->jfl_p__content);
			Event* event = Event::jfl_New(Event::jfl_S_COMPLETE, false, false);
			EventDispatcher::dispatchEvent(
				(EventDispatcher*)loader->jfl_p__contentLoaderInfo,
				event
			);
		});
	}

	bool is_png(){
		if (!check(PNGSignatureLength)) {
			return false;
		}
		if (!png_check_sig(ptr, PNGSignatureLength)) {
			return false;
		}
		return true;
	}

	void load_png(){
		if ( !check(PNGSignatureLength) ){
			return raiseIOError("image file is too short");
		}
		if ( !png_check_sig(ptr, PNGSignatureLength) ){
			return raiseIOError("invalid signature");
		}
		ptr += PNGSignatureLength;
		png_structp png_ptr = NULL;
		png_ptr = png_create_read_struct_2(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL, nullptr, malloc_fn, free_fn);
		if (png_ptr == NULL){
			return raiseIOError("failed to create read struct");
		}
		auto d1 = defer([&]() { png_destroy_read_struct(&png_ptr, NULL, NULL); });
		png_set_error_fn(png_ptr, this, error_func, NULL);

        png_infop info_ptr = NULL;
        info_ptr = png_create_info_struct(png_ptr);
        if (info_ptr == NULL) {
            return raiseIOError("failed to create info struct");
        }
        auto d2 = defer([&]() { png_destroy_info_struct(png_ptr, &info_ptr); });

        png_set_read_fn(png_ptr, this, read_func);
        png_set_sig_bytes(png_ptr, PNGSignatureLength);
        png_read_info(png_ptr, info_ptr);

        png_uint_32 width = 0;
        png_uint_32 height = 0;
        int bitDepth = 0;
        int colorType = -1;
        png_uint_32 retval = png_get_IHDR(
            png_ptr,
            info_ptr,
            &width,
            &height,
            &bitDepth,
            &colorType,
            NULL,
            NULL,
            NULL
        );
        if (retval != 1){
            return raiseIOError("IHDR failed");
        }
        if (bitDepth != 8){
            return raiseIOError("invalid bit depth");
        }
        bool transparent;
        switch (colorType) {
        case PNG_COLOR_TYPE_RGB:{
            transparent = false;
        }break;
        case PNG_COLOR_TYPE_RGB_ALPHA:{
            transparent = true;
        }break;
        case PNG_COLOR_TYPE_GRAY_ALPHA:{
            transparent = true;
            png_set_expand_gray_1_2_4_to_8(png_ptr);
            png_set_gray_to_rgb(png_ptr);
        }break;
        default:{
            return raiseIOError("invalid image color type");
        }break;
        }

        size_t bytesPerPixel = (transparent ? 4 : 3);
        size_t bytesPerRow = (width * bytesPerPixel + 3) & ~3;
        uint8_t* data = (uint8_t*)unmanaged_allocate(height * bytesPerRow);
        for ( uint32_t rowIdx = 0; rowIdx < height; ++rowIdx) {
            uint8_t* row_base = data + rowIdx * bytesPerRow;
            png_read_row(png_ptr, (png_bytep)row_base, NULL);
        }

        WaitMainLoopTask([&](){
            BitmapData* bitmapData = jfl::gc_new<BitmapData>();
            bitmapData->jfl_vtable = &jfl_V_BitmapData;
            bitmapData->width = width;
            bitmapData->height = height;
            bitmapData->transparent = transparent;
            bitmapData->bytesPerPixel = bytesPerPixel;
            bitmapData->bytesPerRow = bytesPerRow;
            bitmapData->data = data;
            Bitmap* bitmap = Bitmap::jfl_New(bitmapData, PixelSnapping::jfl_S_NEVER, true);
            loader->jfl_p__content = (DisplayObject*)bitmap;
            DisplayObjectContainer::addChild((DisplayObjectContainer*)loader.get(), loader->jfl_p__content);
            Event* event = Event::jfl_New(Event::jfl_S_COMPLETE, false, false);
            EventDispatcher::dispatchEvent(
                (EventDispatcher*)loader->jfl_p__contentLoaderInfo,
                event
            );
        });
	}

	static void error_func(png_structp png_ptr, const char* msg){
		png_voidp io_ptr = png_get_io_ptr(png_ptr);
		ImageLoaderData* self = (ImageLoaderData*)io_ptr;
		// throw std::runtime_error(msg);
		abort();
	}

	static void read_func(png_structp png_ptr, png_bytep out, size_t bytes_to_read){
		png_voidp io_ptr = png_get_io_ptr(png_ptr);
		ImageLoaderData* self = (ImageLoaderData*)io_ptr;
		if ( !self->check(bytes_to_read) ){
			png_error(png_ptr, "unexpected end of image data");
		}
		memcpy(out, self->ptr, bytes_to_read);
		self->ptr += bytes_to_read;
	}

	bool check(size_t n){
		if ( (size_t)(data.data() + data.size() - ptr) < n ){
			return false;
		}
		return true;
	}

	void raiseIOError(const char* message){
		WaitMainLoopTask([&]() {
			IOErrorEvent* event = IOErrorEvent::jfl_New(IOErrorEvent::jfl_S_IO_ERROR, false, false, jfl::String::Make(std::string_view(message)), 502);
			EventDispatcher::dispatchEvent(
				(EventDispatcher*)loader->jfl_p__contentLoaderInfo,
				(Event*)event
			);
		});
	}

	gc_ptr<Loader> loader;
	jfl::um_vec<uint8_t> data;
	uint8_t* ptr = 0;
};



void Loader::load(fl::flash::display::Loader* jfl_t, fl::flash::net::URLRequest* jfl_a_request, fl::flash::system::LoaderContext* jfl_a_context){
	jfl_notimpl;
}
void Loader::loadBytes(fl::flash::display::Loader* self, fl::flash::utils::ByteArray* bytes, fl::flash::system::LoaderContext* context){
	RunAsyncTask([&, loader=ImageLoaderData(self, std::move(bytes->data))]() mutable{
		loader.load();
	});
	bytes->data.clear();
}
void Loader::close(fl::flash::display::Loader* jfl_t){
	jfl_notimpl;
}
void Loader::unload(fl::flash::display::Loader* jfl_t){
	jfl_notimpl;
}
void Loader::unloadAndStop(fl::flash::display::Loader* jfl_t, jfl::Boolean jfl_a_gc){
	jfl_notimpl;
}
}
#endif
