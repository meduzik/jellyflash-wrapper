#if __has_include(<fl/flash/display/InteractiveObject.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display/InteractiveObject.h>
namespace fl::flash::display{
fl::flash::geom::Rectangle* InteractiveObject::jfl_g_softKeyboardInputAreaOfInterest(fl::flash::display::InteractiveObject* jfl_t){
	jfl_notimpl;
}
void InteractiveObject::jfl_s_softKeyboardInputAreaOfInterest(fl::flash::display::InteractiveObject* jfl_t, fl::flash::geom::Rectangle* jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean InteractiveObject::jfl_g_needsSoftKeyboard(fl::flash::display::InteractiveObject* jfl_t){
	jfl_notimpl;
}
void InteractiveObject::jfl_s_needsSoftKeyboard(fl::flash::display::InteractiveObject* jfl_t, jfl::Boolean jfl_a_param1){
}
jfl::Boolean InteractiveObject::requestSoftKeyboard(fl::flash::display::InteractiveObject* jfl_t){
	jfl_notimpl;
}
void InteractiveObject::jfl_s_softKeyboard(fl::flash::display::InteractiveObject* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String InteractiveObject::jfl_g_softKeyboard(fl::flash::display::InteractiveObject* jfl_t){
	jfl_notimpl;
}
}
#endif
