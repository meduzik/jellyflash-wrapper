#if __has_include(<fl/flash/display/Stage3D.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display/Stage3D.h>
#include <jfl/app/application.h>
namespace fl::flash::display{
void Stage3D::requestContext3D(fl::flash::display::Stage3D* jfl_t, jfl::String jfl_a_param1, jfl::String jfl_a_param2){
	jfl::GetApplication()->requestContext3D();
}
void Stage3D::requestContext3DMatchingProfiles(fl::flash::display::Stage3D* jfl_t, jfl::Vector<jfl::String>* jfl_a_param1){
	jfl::GetApplication()->requestContext3D();
}
}
#endif
