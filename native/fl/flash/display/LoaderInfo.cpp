#if __has_include(<fl/flash/display/LoaderInfo.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display/LoaderInfo.h>
namespace fl::flash::display{
jfl::String LoaderInfo::jfl_g_loaderURL(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::String LoaderInfo::jfl_g_url(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::Boolean LoaderInfo::jfl_g_isURLInaccessible(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::UInt LoaderInfo::jfl_g_bytesLoaded(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::UInt LoaderInfo::jfl_g_bytesTotal(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
fl::flash::system::ApplicationDomain* LoaderInfo::jfl_g_applicationDomain(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::UInt LoaderInfo::jfl_g_swfVersion(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::UInt LoaderInfo::jfl_g_actionScriptVersion(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::Number LoaderInfo::jfl_g_frameRate(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::Int LoaderInfo::jfl_g_width(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::Int LoaderInfo::jfl_g_height(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::String LoaderInfo::jfl_g_contentType(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
fl::flash::events::EventDispatcher* LoaderInfo::jfl_g_sharedEvents(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::Any LoaderInfo::jfl_g_parentSandboxBridge(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
void LoaderInfo::jfl_s_parentSandboxBridge(fl::flash::display::LoaderInfo* jfl_t, jfl::Any jfl_a_param1){
	jfl_notimpl;
}
jfl::Any LoaderInfo::jfl_g_childSandboxBridge(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
void LoaderInfo::jfl_s_childSandboxBridge(fl::flash::display::LoaderInfo* jfl_t, jfl::Any jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean LoaderInfo::jfl_g_sameDomain(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::Boolean LoaderInfo::jfl_g_childAllowsParent(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::Boolean LoaderInfo::jfl_g_parentAllowsChild(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
fl::flash::display::Loader* LoaderInfo::jfl_g_loader(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
fl::flash::display::DisplayObject* LoaderInfo::jfl_g_content(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
fl::flash::utils::ByteArray* LoaderInfo::jfl_g_bytes(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
jfl::Any LoaderInfo::jfl_p__getArgs(fl::flash::display::LoaderInfo* jfl_t){
	jfl_notimpl;
}
fl::flash::display::LoaderInfo* LoaderInfo::jfl_S_getLoaderInfoByDefinition(jfl::Any jfl_a_param1){
	jfl_notimpl;
}
}
#endif
