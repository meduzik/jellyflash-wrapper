#if __has_include(<fl/flash/events/MouseEvent.h>)
#include <jfl/common_impl.h>
#include <fl/flash/events/MouseEvent.h>
namespace fl::flash::events{
jfl::Number MouseEvent::jfl_g_localX(fl::flash::events::MouseEvent* jfl_t){
	jfl_notimpl;
}
jfl::Number MouseEvent::jfl_g_localY(fl::flash::events::MouseEvent* jfl_t){
	jfl_notimpl;
}
void MouseEvent::updateAfterEvent(fl::flash::events::MouseEvent* jfl_t){
	jfl_notimpl;
}
jfl::Number MouseEvent::jfl_g_movementX(fl::flash::events::MouseEvent* jfl_t){
	jfl_notimpl;
}
void MouseEvent::jfl_s_movementX(fl::flash::events::MouseEvent* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number MouseEvent::jfl_g_movementY(fl::flash::events::MouseEvent* jfl_t){
	jfl_notimpl;
}
void MouseEvent::jfl_s_movementY(fl::flash::events::MouseEvent* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
}
#endif
