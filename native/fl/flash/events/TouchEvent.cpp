#if __has_include(<fl/flash/events/TouchEvent.h>)
#include <jfl/common_impl.h>
#include <fl/flash/events/TouchEvent.h>
namespace fl::flash::events{
jfl::Number TouchEvent::jfl_g_localX(fl::flash::events::TouchEvent* jfl_t){
	jfl_notimpl;
}
void TouchEvent::jfl_s_localX(fl::flash::events::TouchEvent* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number TouchEvent::jfl_g_localY(fl::flash::events::TouchEvent* jfl_t){
	jfl_notimpl;
}
void TouchEvent::jfl_s_localY(fl::flash::events::TouchEvent* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void TouchEvent::updateAfterEvent(fl::flash::events::TouchEvent* jfl_t){
	jfl_notimpl;
}
jfl::Number TouchEvent::jfl_p_getStageX(fl::flash::events::TouchEvent* jfl_t){
	jfl_notimpl;
}
jfl::Number TouchEvent::jfl_p_getStageY(fl::flash::events::TouchEvent* jfl_t){
	jfl_notimpl;
}
jfl::Boolean TouchEvent::isToolButtonDown(fl::flash::events::TouchEvent* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::UInt TouchEvent::getSamples(fl::flash::events::TouchEvent* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::Boolean jfl_a_param2){
	jfl_notimpl;
}
jfl::UInt TouchEvent::jfl_p_privateGetSamples(fl::flash::events::TouchEvent* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1){
	jfl_notimpl;
}
void TouchEvent::jfl_p_setSamples(fl::flash::events::TouchEvent* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1){
	jfl_notimpl;
}
}
#endif
