#if __has_include(<fl/flash/media/SoundMixer.decl.h>)
#include <jfl/common_impl.h>
#include <fl/flash/media/SoundMixer.decl.h>
#include <fl/flash/media/SoundTransform.h>
#include <jfl/media/Sound.h>

namespace fl::flash::media{
void SoundMixer::jfl_S_stopAll(){
	abort();
}
void SoundMixer::jfl_S_computeSpectrum(fl::flash::utils::ByteArray* jfl_a_param1, jfl::Boolean jfl_a_param2, jfl::Int jfl_a_param3){
	abort();
}
jfl::Int SoundMixer::jfl_Sg_bufferTime(){
	abort();
}
void SoundMixer::jfl_Ss_bufferTime(jfl::Int jfl_a_param1){
	abort();
}
void SoundMixer::jfl_pS_updateTransform(){
	jfl::media::SoundSetGlobalVolume(SoundMixer::jfl_pS_transform->jfl_p__volume);
}
jfl::Boolean SoundMixer::jfl_S_areSoundsInaccessible(){
	abort();
}
jfl::String SoundMixer::jfl_Sg_audioPlaybackMode(){
	abort();
}
void SoundMixer::jfl_Ss_audioPlaybackMode(jfl::String jfl_a_param1){
	abort();
}
jfl::Boolean SoundMixer::jfl_Sg_useSpeakerphoneForVoice(){
	abort();
}
void SoundMixer::jfl_Ss_useSpeakerphoneForVoice(jfl::Boolean jfl_a_param1){
	abort();
}
}
#endif
