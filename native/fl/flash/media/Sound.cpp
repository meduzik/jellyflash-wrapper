#if __has_include(<fl/flash/media/Sound.decl.h>)
#include <jfl/common_impl.h>
#include <fl/flash/media/Sound.decl.h>
#include <fl/flash/media/SoundChannel.decl.h>
#include <fl/flash/utils/ByteArray.decl.h>
#include <jfl/media/Sound.h>

namespace fl::flash::media{
Sound::~Sound() {
	if (impl) {
		jfl::media::SoundImplDestroy(impl);
		impl = nullptr;
	}
}

void Sound::jfl_p_init(fl::flash::media::Sound* self) {
	self->impl = jfl::media::SoundImplCreate(self);
}

fl::flash::media::SoundChannel* Sound::play(fl::flash::media::Sound* self, jfl::Number startTime, jfl::Int jfl_a_loops, fl::flash::media::SoundTransform* transform){
	SoundChannel* channel = SoundChannel::jfl_New();
	channel->last_known_position = 0;
	jfl::media::SoundChannelImpl* channel_impl = jfl::media::SoundImplPlay(self->impl, channel, startTime, transform);
	if (!channel_impl) {
		return nullptr;
	}
	return channel;
}

void Sound::loadCompressedDataFromByteArray(fl::flash::media::Sound* self, fl::flash::utils::ByteArray* data, jfl::UInt length){
	utils::ByteArray::check_read(data, length);
	jfl::media::SoundImplDecode(self->impl, data->data.data() + data->pos, length);
}

jfl::UInt Sound::jfl_g_bytesLoaded(fl::flash::media::Sound* jfl_t){
	abort();
}

jfl::Int Sound::jfl_g_bytesTotal(fl::flash::media::Sound* jfl_t){
	abort();
}

fl::flash::media::ID3Info* Sound::jfl_g_id3(fl::flash::media::Sound* jfl_t){
	abort();
}

void Sound::close(fl::flash::media::Sound* jfl_t){
	abort();
}

}
#endif
