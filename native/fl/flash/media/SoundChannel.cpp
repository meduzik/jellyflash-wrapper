#if __has_include(<fl/flash/media/SoundChannel.decl.h>)
#include <jfl/common_impl.h>
#include <fl/flash/media/SoundChannel.decl.h>
#include <jfl/media/Sound.h>

namespace fl::flash::media{

SoundChannel::~SoundChannel() {
	if (impl) {
		jfl::media::SoundChannelImplDestroy(impl);
		impl = nullptr;
	}
}

jfl::Number SoundChannel::jfl_g_position(fl::flash::media::SoundChannel* self){
	if (self->impl) {
		return jfl::media::SoundChannelImplGetPosition(self->impl);
	}
	return self->last_known_position;
}

void SoundChannel::stop(fl::flash::media::SoundChannel* self){
	if (self->impl) {
		jfl::media::SoundChannelImplDestroy(self->impl);
		self->impl = nullptr;
	}
}

void SoundChannel::jfl_p_updateTransfrom(fl::flash::media::SoundChannel* self){
	if (self->impl) {
		jfl::media::SoundChannelImplUpdateTransform(self->impl, self->jfl_p_transform);
	}
}

}
#endif
