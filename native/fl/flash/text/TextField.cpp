#if __has_include(<fl/flash/text/TextField.h>)
#include <jfl/common_impl.h>
#include <fl/flash/text/TextField.h>
#include <fl/flash/text/TextFormat.h>
#include <jfl/text/TextRendering.h>

namespace fl::flash::text{
using display::DisplayObject;

jfl::matrix2d TextField::jfl_computeGlobalMatrix(TextField* self){
	jfl::matrix2d mat;
	DisplayObject* displayObject = (DisplayObject*)self;
	while ( displayObject ){
		displayObject->tform.recompose();
		mat.append(displayObject->tform.matrix);
		displayObject = (DisplayObject*)displayObject->jfl_nflash_2einternal_n__parent;
	}
	return mat;
}

jfl::Number TextField::jfl_g_width(fl::flash::text::TextField* self){
	self->tform.recompose();
	return self->textLayout.get_width() * self->tform.decomp.sx;
}

void TextField::jfl_s_width(fl::flash::text::TextField* self, jfl::Number value){
	self->textLayout.set_width(value);
}

jfl::Number TextField::jfl_g_height(fl::flash::text::TextField* self){
	self->tform.recompose();
	return self->textLayout.get_height() * self->tform.decomp.sy;
}

void TextField::jfl_s_height(fl::flash::text::TextField* self, jfl::Number value){
	self->textLayout.set_height(value);
}

jfl::Boolean TextField::jfl_g_multiline(fl::flash::text::TextField* self){
	return self->textLayout.get_multiline();
}

void TextField::jfl_s_multiline(fl::flash::text::TextField* self, jfl::Boolean value){
	self->textLayout.set_multiline(value);
}

jfl::Boolean TextField::jfl_g_wordWrap(fl::flash::text::TextField* self){
	return self->textLayout.get_word_wrap();
}

void TextField::jfl_s_wordWrap(fl::flash::text::TextField* self, jfl::Boolean value){
	self->textLayout.set_word_wrap(value);
}

jfl::String TextField::jfl_g_htmlText(fl::flash::text::TextField* self){
	jfl_notimpl;
}

void TextField::jfl_s_htmlText(fl::flash::text::TextField* self, jfl::String value){
	self->textLayout.set_html_text(value.to_string_view());
}

jfl::String TextField::jfl_g_text(fl::flash::text::TextField* self){
	return self->textLayout.get_plain_text();
}

void TextField::jfl_s_text(fl::flash::text::TextField* self, jfl::String value){
	self->textLayout.set_plain_text(value.to_string_view());
}

void TextField::appendText(fl::flash::text::TextField* self, jfl::String text){
	self->textLayout.append_plain_text(text.to_string_view());
}


void TextField::jfl_p__updateNativeFormat(fl::flash::text::TextField* self){
	auto as_format = self->jfl_p__defaultFormat;

	jfl::TextFormat format;
	jfl::FontDescriptor descriptor;
	descriptor.font_name = as_format->jfl_p__font.to_string_view();
	descriptor.bold = as_format->jfl_p__bold;
	descriptor.italic = as_format->jfl_p__italic;
	format.font = jfl::FontReference::Get(descriptor);
	auto align = as_format->jfl_p__align.to_string_view();
	if (align == "left"){
		format.align = jfl::TextAlign::Left;
	} else if (align == "center") {
		format.align = jfl::TextAlign::Center;
	} else if (align == "right") {
		format.align = jfl::TextAlign::Right;
	} else if (align == "justified") {
		format.align = jfl::TextAlign::Justified;
	}else{
		jfl::raise_message("unsupported text align");
	}
	format.color = as_format->jfl_p__color;
	format.indent = as_format->jfl_p__indent;
	format.leading = as_format->jfl_p__leading;
	format.left_margin = as_format->jfl_p__leftMargin;
	format.right_margin = as_format->jfl_p__rightMargin;
	format.letter_spacing = as_format->jfl_p__letterSpacing;
	format.size = as_format->jfl_p__size;
	self->textLayout.set_default_format(format);
}

void TextField::jfl_renderTo(TextField* self, fl::flash::display::BitmapData* bitmap, const jfl::matrix2d& mat){
	if (bitmap->bytesPerPixel != 4){
		jfl::raise_message("cannot render text into a non-transparent bitmap");
	}
	jfl::TextRenderingCanvas canvas;
	canvas.data = bitmap->data;
	canvas.width = bitmap->width;
	canvas.height = bitmap->height;
	canvas.bytesPerRow = bitmap->bytesPerRow;
	self->textLayout.render(mat, canvas);
}
jfl::Boolean TextField::jfl_g_alwaysShowSelection(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_alwaysShowSelection(fl::flash::text::TextField* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::String TextField::jfl_g_antiAliasType(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_antiAliasType(fl::flash::text::TextField* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String TextField::jfl_g_autoSize(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_autoSize(fl::flash::text::TextField* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_g_background(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_background(fl::flash::text::TextField* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::UInt TextField::jfl_g_backgroundColor(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_backgroundColor(fl::flash::text::TextField* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_g_border(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_border(fl::flash::text::TextField* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::UInt TextField::jfl_g_borderColor(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_borderColor(fl::flash::text::TextField* jfl_t, jfl::UInt jfl_a_param1){
}
jfl::Int TextField::jfl_g_bottomScrollV(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_caretIndex(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_g_condenseWhite(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_condenseWhite(fl::flash::text::TextField* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::String TextField::jfl_g_gridFitType(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_gridFitType(fl::flash::text::TextField* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_length(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::String TextField::jfl_g_textInteractionMode(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_maxChars(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_maxChars(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_maxScrollH(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_maxScrollV(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_g_mouseWheelEnabled(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_mouseWheelEnabled(fl::flash::text::TextField* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_numLines(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_g_displayAsPassword(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_displayAsPassword(fl::flash::text::TextField* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::String TextField::jfl_g_restrict(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_restrict(fl::flash::text::TextField* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_scrollH(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_scrollH(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_scrollV(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_scrollV(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_g_selectable(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_selectable(fl::flash::text::TextField* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_selectionBeginIndex(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::Int TextField::jfl_g_selectionEndIndex(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::Number TextField::jfl_g_sharpness(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_sharpness(fl::flash::text::TextField* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number TextField::jfl_g_textHeight(fl::flash::text::TextField* self){
	self->textLayout.update_matrix(jfl_computeGlobalMatrix(self));
	return self->textLayout.get_text_height();
}
jfl::Number TextField::jfl_g_textWidth(fl::flash::text::TextField* self){
	self->textLayout.update_matrix(jfl_computeGlobalMatrix(self));
	return self->textLayout.get_text_width();
}
jfl::Number TextField::jfl_g_thickness(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_thickness(fl::flash::text::TextField* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::String TextField::jfl_g_type(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_type(fl::flash::text::TextField* jfl_t, jfl::String jfl_a_param1){
}
fl::flash::geom::Rectangle* TextField::getCharBoundaries(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::getCharIndexAtPoint(fl::flash::text::TextField* jfl_t, jfl::Number jfl_a_param1, jfl::Number jfl_a_param2){
	jfl_notimpl;
}
jfl::Int TextField::jfl_p_getCharIndexNearestPoint(fl::flash::text::TextField* jfl_t, jfl::Number jfl_a_param1, jfl::Number jfl_a_param2){
	jfl_notimpl;
}
jfl::Int TextField::getFirstCharInParagraph(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::getLineIndexAtPoint(fl::flash::text::TextField* jfl_t, jfl::Number jfl_a_param1, jfl::Number jfl_a_param2){
	jfl_notimpl;
}
jfl::Int TextField::getLineIndexOfChar(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::getLineLength(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
fl::flash::text::TextLineMetrics* TextField::getLineMetrics(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::getLineOffset(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::String TextField::getLineText(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::getParagraphLength(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
fl::flash::text::TextFormat* TextField::getTextFormat(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	jfl_notimpl;
}
fl::Array* TextField::getTextRuns(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	jfl_notimpl;
}
jfl::String TextField::getRawText(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::replaceSelectedText(fl::flash::text::TextField* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void TextField::replaceText(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2, jfl::String jfl_a_param3){
	jfl_notimpl;
}
void TextField::setSelection(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	jfl_notimpl;
}
void TextField::setTextFormat(fl::flash::text::TextField* jfl_t, fl::flash::text::TextFormat* jfl_a_param1, jfl::Int jfl_a_param2, jfl::Int jfl_a_param3){
	jfl_notimpl;
}
fl::flash::display::DisplayObject* TextField::getImageReference(fl::flash::text::TextField* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_g_useRichTextClipboard(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_s_useRichTextClipboard(fl::flash::text::TextField* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_pg_dragCaretVisible(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_ps_dragCaretVisible(fl::flash::text::TextField* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Int TextField::jfl_pg_dragCaretIndex(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
void TextField::jfl_ps_dragCaretIndex(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
void TextField::jfl_p_setSelectionToDragCaret(fl::flash::text::TextField* jfl_t){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_p_isDragCaretSurrogateTrail(fl::flash::text::TextField* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean TextField::jfl_S_isFontCompatible(jfl::String jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
fl::flash::geom::Rectangle* TextField::getBounds(fl::flash::text::TextField* self, fl::flash::display::DisplayObject* target){
	jfl_notimpl;
}
}
#endif
