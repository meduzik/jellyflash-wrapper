#if __has_include(<fl/flash/ui/Keyboard.h>)
#include <jfl/common_impl.h>
#include <fl/flash/ui/Keyboard.h>
namespace fl::flash::ui{
jfl::Boolean Keyboard::jfl_Sg_capsLock(){
	jfl_notimpl;
}
jfl::Boolean Keyboard::jfl_Sg_numLock(){
	jfl_notimpl;
}
jfl::Boolean Keyboard::jfl_S_isAccessible(){
	jfl_notimpl;
}
jfl::Boolean Keyboard::jfl_Sg_hasVirtualKeyboard(){
	jfl_notimpl;
}
jfl::String Keyboard::jfl_Sg_physicalKeyboardType(){
	jfl_notimpl;
}
}
#endif
