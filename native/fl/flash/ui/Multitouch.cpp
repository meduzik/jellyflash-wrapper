#if __has_include(<fl/flash/ui/Multitouch.h>)
#include <jfl/common_impl.h>
#include <fl/flash/ui/Multitouch.h>
namespace fl::flash::ui{
jfl::String Multitouch::jfl_Sg_inputMode(){
	return jfl::String::Make("touchPoint");
}
void Multitouch::jfl_Ss_inputMode(jfl::String inputMode){
	if ( !jfl::str_eq(inputMode, jfl::String::Make("touchPoint")) ){
		jfl_notimpl;
	}
}
jfl::Boolean Multitouch::jfl_Sg_supportsTouchEvents(){
	return false;
}
jfl::Boolean Multitouch::jfl_Sg_supportsGestureEvents(){
	jfl_notimpl;
}
jfl::Vector<jfl::String>* Multitouch::jfl_Sg_supportedGestures(){
	jfl_notimpl;
}
jfl::Int Multitouch::jfl_Sg_maxTouchPoints(){
	jfl_notimpl;
}
jfl::Boolean Multitouch::jfl_Sg_mapTouchToMouse(){
	jfl_notimpl;
}
void Multitouch::jfl_Ss_mapTouchToMouse(jfl::Boolean jfl_a_param1){
}
}
#endif
