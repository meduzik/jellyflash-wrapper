#if __has_include(<fl/flash/utils/ByteArray.h>)
#include <jfl/common_impl.h>
#include <fl/flash/utils/ByteArray.h>
#include <zlib.h>
namespace fl::flash::utils{
using namespace jfl;

static uint16_t byte_swap(uint16_t x){
	union Pun{
		uint16_t large;
		uint8_t bytes[2];
	};
	Pun pun;
	pun.large = x;
	std::swap(pun.bytes[0], pun.bytes[1]);
	return pun.large;
}

static uint32_t byte_swap(uint32_t x) {
	union Pun {
		uint32_t large;
		uint8_t bytes[4];
	};
	Pun pun;
	pun.large = x;
	std::swap(pun.bytes[0], pun.bytes[3]);
	std::swap(pun.bytes[1], pun.bytes[2]);
	return pun.large;
}

static uint64_t byte_swap(uint64_t x) {
	union Pun {
		uint64_t large;
		uint8_t bytes[8];
	};
	Pun pun;
	pun.large = x;
	std::swap(pun.bytes[0], pun.bytes[7]);
	std::swap(pun.bytes[1], pun.bytes[6]);
	std::swap(pun.bytes[2], pun.bytes[5]);
	std::swap(pun.bytes[3], pun.bytes[4]);
	return pun.large;
}

void ByteArray::check_read(fl::flash::utils::ByteArray* bytes, size_t size) {
	if ( bytes->pos + size > bytes->data.size() ){
		jfl::raise_message("ByteArray does not have enough elements");
	}
}

void ByteArray::check_size(fl::flash::utils::ByteArray* bytes, size_t size) {
	if (bytes->data.size() < size) {
		jfl::raise_message("ByteArray does not have enough elements");
	}
}

void ByteArray::ensure(fl::flash::utils::ByteArray* bytes, size_t size){
	size_t target_size = size + bytes->pos;
	ensure_size(bytes, target_size);
}

void ByteArray::ensure_size(fl::flash::utils::ByteArray* bytes, size_t target_size) {
	if (bytes->data.size() < target_size) {
		bytes->data.resize(target_size);
	}
}

void ByteArray::write_raw(fl::flash::utils::ByteArray* bytes, const void* data, size_t size) {
	ensure(bytes, size);
	memcpy(bytes->data.data() + bytes->pos, data, size);
	bytes->pos += size;
}

void ByteArray::read_raw(fl::flash::utils::ByteArray* bytes, void* data, size_t size) {
	check_read(bytes, size);
	memcpy(data, bytes->data.data() + bytes->pos, size);
	bytes->pos += size;
}

template<class T>
void ByteArray::write(fl::flash::utils::ByteArray* bytes, T&& value) {
	write_raw(bytes, &value, sizeof(value));
}

void ByteArray::jfl_p_init(fl::flash::utils::ByteArray* bytes){
	bytes->pos = 0;
	bytes->native_endian = false;
}

void ByteArray::readBytes(fl::flash::utils::ByteArray* self, fl::flash::utils::ByteArray* bytes, jfl::UInt offset, jfl::UInt length){
	if (!bytes) {
		raise_nullarg();
	}
	if ( length == 0 ){
		length = jfl_g_bytesAvailable(self);
	}
	check_read(self, length);
	ensure_size(bytes, offset + length);
	memcpy(bytes->data.data() + offset, self->data.data() + self->pos, length);
	self->pos += length;
}
void ByteArray::writeBytes(fl::flash::utils::ByteArray* self, fl::flash::utils::ByteArray* bytes, jfl::UInt offset, jfl::UInt length) {
	if (!bytes) {
		raise_nullarg();
	}
	if (offset > bytes->data.size()) {
		offset = (UInt)bytes->data.size();
	}
	UInt maxlen = (UInt)bytes->data.size() - offset;
	if (length == 0) {
		length = maxlen;
	} else if (length > maxlen) {
		length = maxlen;
	}
	if (self == bytes) {
		ensure(bytes, length);
		memmove(bytes->data.data() + bytes->pos, bytes->data.data() + offset, length);
		bytes->pos += length;
	}else{
		write_raw(self, bytes->data.data() + offset, length);
	}
}
void ByteArray::writeBoolean(fl::flash::utils::ByteArray* bytes, jfl::Boolean value){
	writeByte(bytes, value ? 1 : 0);
}
void ByteArray::writeByte(fl::flash::utils::ByteArray* bytes, jfl::Int value){
	write(bytes, (uint8_t)value);
}
void ByteArray::writeShort(fl::flash::utils::ByteArray* bytes, jfl::Int value){
	write(bytes, bytes->native_endian ? (uint16_t)value : byte_swap((uint16_t)value));
}
void ByteArray::writeInt(fl::flash::utils::ByteArray* bytes, jfl::Int value){
	write(bytes, bytes->native_endian ? (uint32_t)value : byte_swap((uint32_t)value));
}
void ByteArray::writeUnsignedInt(fl::flash::utils::ByteArray* bytes, jfl::UInt value){
	write(bytes, bytes->native_endian ? (uint32_t)value : byte_swap((uint32_t)value));
}
void ByteArray::writeFloat(fl::flash::utils::ByteArray* self, jfl::Number value){
	float float_value = (float)value;
	uint32_t val_bytes;
	memcpy(&val_bytes, &float_value, sizeof(float_value));
	if (!self->native_endian) {
		val_bytes = byte_swap(val_bytes);
	}
	write_raw(self, &val_bytes, sizeof(val_bytes));
}
void ByteArray::writeDouble(fl::flash::utils::ByteArray* self, jfl::Number value){
	uint64_t val_bytes;
	memcpy(&val_bytes, &value, sizeof(value));
	if (!self->native_endian) {
		val_bytes = byte_swap(val_bytes);
	}
	write_raw(self, &val_bytes, sizeof(val_bytes));
}
void ByteArray::writeMultiByte(fl::flash::utils::ByteArray* jfl_t, jfl::String jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
void ByteArray::writeUTF(fl::flash::utils::ByteArray* bytes, jfl::String string){
	if (string.tag == type_tag::Null) {
		raise_npe();
	}
	auto view = string.to_string_view();
	writeShort(bytes, (Int)view.size());
	write_raw(bytes, view.data(), view.size());
}
void ByteArray::writeUTFBytes(fl::flash::utils::ByteArray* bytes, jfl::String string){
	if ( string.tag == type_tag::Null ){
		raise_npe();
	}
	auto view = string.to_string_view();
	write_raw(bytes, view.data(), view.size());
}
jfl::Boolean ByteArray::readBoolean(fl::flash::utils::ByteArray* self){
	return readByte(self) != 0;
}
jfl::Int ByteArray::readByte(fl::flash::utils::ByteArray* self){
	int8_t val;
	read_raw(self, &val, sizeof(val));
	return (jfl::Int)val;
}
jfl::UInt ByteArray::readUnsignedByte(fl::flash::utils::ByteArray* self){
	uint8_t val;
	read_raw(self, &val, sizeof(val));
	return (jfl::UInt)val;
}
jfl::Int ByteArray::readShort(fl::flash::utils::ByteArray* self){
	int16_t val;
	read_raw(self, &val, sizeof(val));
	if (!self->native_endian) {
		val = byte_swap((uint16_t)val);
	}
	return (jfl::Int)val;
}
jfl::UInt ByteArray::readUnsignedShort(fl::flash::utils::ByteArray* self){
	uint16_t val;
	read_raw(self, &val, sizeof(val));
	if (!self->native_endian) {
		val = byte_swap(val);
	}
	return (jfl::UInt)val;
}
jfl::Int ByteArray::readInt(fl::flash::utils::ByteArray* self){
	uint32_t val;
	read_raw(self, &val, sizeof(val));
	if ( !self->native_endian ){
		val = byte_swap(val);
	}
	return (jfl::Int)val;
}
jfl::UInt ByteArray::readUnsignedInt(fl::flash::utils::ByteArray* self){
	uint32_t val;
	read_raw(self, &val, sizeof(val));
	if (!self->native_endian) {
		val = byte_swap(val);
	}
	return (jfl::UInt)val;
}
jfl::Number ByteArray::readFloat(fl::flash::utils::ByteArray* self){
	float val;
	uint32_t val_bytes;
	read_raw(self, &val_bytes, sizeof(val));
	if (!self->native_endian) {
		val_bytes = byte_swap(val_bytes);
	}
	memcpy(&val, &val_bytes, sizeof(val));
	return val;
}
jfl::Number ByteArray::readDouble(fl::flash::utils::ByteArray* self){
	double val;
	uint64_t val_bytes;
	read_raw(self, &val_bytes, sizeof(val));
	if (!self->native_endian) {
		val_bytes = byte_swap(val_bytes);
	}
	memcpy(&val, &val_bytes, sizeof(val));
	return val;
}
jfl::String ByteArray::readMultiByte(fl::flash::utils::ByteArray* jfl_t, jfl::UInt jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}

jfl::String ByteArray::readUTF(fl::flash::utils::ByteArray* self){
	jfl::UInt len = readUnsignedShort(self);
	return readUTFBytes(self, len);
}

jfl::String ByteArray::readUTFBytes(fl::flash::utils::ByteArray* bytes, jfl::UInt length){
	check_read(bytes, length);
	jfl::String s = jfl::String::Make(bytes->data.data() + bytes->pos, length);
	bytes->pos += length;
	return s;
}
jfl::UInt ByteArray::jfl_g_length(fl::flash::utils::ByteArray* bytes){
	return (UInt)bytes->data.size();
}
void ByteArray::jfl_s_length(fl::flash::utils::ByteArray* bytes, jfl::UInt length){
	bytes->data.resize(length);
	if ( bytes->pos > bytes->data.size() ){
		bytes->pos = bytes->data.size();
	}
}
void ByteArray::writeObject(fl::flash::utils::ByteArray* jfl_t, jfl::Any jfl_a_param1){
	jfl_notimpl;
}
jfl::Any ByteArray::readObject(fl::flash::utils::ByteArray* jfl_t){
	jfl_notimpl;
}
void ByteArray::jfl_p__compress(fl::flash::utils::ByteArray* self, jfl::String algo){
	auto view = algo.to_string_view();
	if (view == "zlib") {
		const size_t ChunkSize = 2048;
		z_stream stream;
		uint8_t out[ChunkSize];
		jfl::um_vec<uint8_t> result;
		stream.zalloc = Z_NULL;
		stream.zfree = Z_NULL;
		stream.opaque = Z_NULL;
		stream.avail_in = 0;
		stream.next_in = Z_NULL;
		int ret;
		ret = deflateInit(&stream, 9);
		if (ret != Z_OK) {
			jfl::raise_message("compress failed: init");
		}
		stream.avail_in = self->data.size();
		stream.next_in = self->data.data();
		do {
			stream.avail_out = ChunkSize;
			stream.next_out = out;
			ret = ::deflate(&stream, Z_FINISH);
			switch (ret) {
			case Z_OK:
			case Z_STREAM_END: {
			}break;
			default: {
				(void)deflateEnd(&stream);
				jfl::raise_message("compress failed: error");
			}break;
			}
			int have = ChunkSize - stream.avail_out;
			result.resize(result.size() + have);
			memcpy(result.data() + result.size() - have, out, have);
		} while ( stream.avail_out == 0 );

		deflateEnd(&stream);
		if ( ret != Z_STREAM_END || stream.avail_in > 0) {
			jfl::raise_message("compress failed: unexpected end of stream");
		}

		self->data = std::move(result);
		self->pos = 0;
	}else{
		jfl::raise_message("invalid algorithm for compress");
	}
}

void ByteArray::jfl_p__uncompress(fl::flash::utils::ByteArray* self, jfl::String algo){
	auto view = algo.to_string_view();
	if ( view == "deflate" || view == "zlib" ){
		const size_t ChunkSize = 2048;
		z_stream stream;
		uint8_t out[ChunkSize];
		jfl::um_vec<uint8_t> result;
		stream.zalloc = Z_NULL;
		stream.zfree = Z_NULL;
		stream.opaque = Z_NULL;
		stream.avail_in = 0;
		stream.next_in = Z_NULL;
		int ret;
		if ( view == "zlib" ){
			ret = inflateInit(&stream);
		}else{
			ret = inflateInit2(&stream, -15);
		}
		if ( ret != Z_OK ){
			jfl::raise_message("uncompress failed: init");
		}

		stream.avail_in = self->data.size();
		stream.next_in = self->data.data();
		do{
			do {
				stream.avail_out = ChunkSize;
				stream.next_out = out;
				ret = ::inflate(&stream, Z_NO_FLUSH);
				switch (ret) {
				case Z_OK:
				case Z_STREAM_END: {
				}break;
				default:{
					(void)inflateEnd(&stream);
					jfl::raise_message("uncompress failed: error");
				}break;
				}
				int have = ChunkSize - stream.avail_out;
				result.resize(result.size() + have);
				memcpy(result.data() + result.size() - have, out, have);
			} while ( stream.avail_out == 0 );
		}while ( ret != Z_STREAM_END);
		
		inflateEnd(&stream);
		if ( ret != Z_STREAM_END || stream.avail_in > 0 ){
			jfl::raise_message("uncompress failed: unexpected end of stream");
		}

		self->data = std::move(result);
		self->pos = 0;
	}else{
		jfl::raise_message("invalid algorithm for uncompress");
	}
}
jfl::String ByteArray::jfl_p__toString(fl::flash::utils::ByteArray* bytes){
	return jfl::String::Make(bytes->data.data(), bytes->data.size());
}
jfl::UInt ByteArray::jfl_g_bytesAvailable(fl::flash::utils::ByteArray* bytes){
	if ( bytes->pos >= bytes->data.size() ){
		return 0;
	}
	return (jfl::UInt)(bytes->data.size() - bytes->pos);
}
jfl::UInt ByteArray::jfl_g_position(fl::flash::utils::ByteArray* self){
	return self->pos;
}
void ByteArray::jfl_s_position(fl::flash::utils::ByteArray* bytes, jfl::UInt pos){
	bytes->pos = pos;
}
jfl::String ByteArray::jfl_g_endian(fl::flash::utils::ByteArray* self){
	if (self->native_endian) {
		return jfl::String::Make("littleEndian");
	}else{
		return jfl::String::Make("bigEndian");
	}
}
void ByteArray::jfl_s_endian(fl::flash::utils::ByteArray* self, jfl::String endian){
	if ( jfl::str_eq(endian, jfl::String::Make("littleEndian")) ){
		self->native_endian = true;
	}else if (jfl::str_eq(endian, jfl::String::Make("bigEndian"))) {
		self->native_endian = false;
	}else{
		raise_message("invalid ByteArray.endian set");
	}
}
void ByteArray::clear(fl::flash::utils::ByteArray* self){
	self->data.clear();
	self->data.shrink_to_fit();
	self->pos = 0;
}
jfl::Int ByteArray::atomicCompareAndSwapIntAt(fl::flash::utils::ByteArray* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2, jfl::Int jfl_a_param3){
	jfl_notimpl;
}
jfl::Int ByteArray::atomicCompareAndSwapLength(fl::flash::utils::ByteArray* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2){
	jfl_notimpl;
}
jfl::Boolean ByteArray::jfl_g_shareable(fl::flash::utils::ByteArray* jfl_t){
	jfl_notimpl;
}
void ByteArray::jfl_s_shareable(fl::flash::utils::ByteArray* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
}
#endif
