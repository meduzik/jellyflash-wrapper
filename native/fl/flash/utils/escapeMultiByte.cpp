#if __has_include(<fl/flash/utils/escapeMultiByte.h>)
#include <jfl/common_impl.h>
#include <fl/flash/utils/escapeMultiByte.h>
namespace fl::flash::utils{

const uint8_t HexDigit[] = "0123456789ABCDEF";

jfl::String escapeMultiByte(jfl::String s){
	std::vector<uint8_t> enc;
	for (auto ch: s.to_string_view()) {		
		if (isalnum(ch) || ch == '-' || ch == '_' || ch == '.' || ch == '!' || ch == '~' || ch == '*' || ch == '\'' || ch == '(' || ch == ')') {
			enc.push_back(ch);
		} else {
			uint8_t val = (uint8_t)ch;
			enc.push_back('%');
			enc.push_back(HexDigit[val >> 4]);
			enc.push_back(HexDigit[val & 0xf]);
		}
	}
	return jfl::String::Make(enc.data(), enc.size());
}
}
#endif
