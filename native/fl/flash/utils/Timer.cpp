#if __has_include(<fl/flash/utils/Timer.h>)
#include <jfl/common_impl.h>
#include <fl/flash/utils/Timer.h>
#include <jfl/app/application.h>
namespace fl::flash::utils{
void Timer::jfl_p__start(fl::flash::utils::Timer* timer, jfl::Number delay){
	jfl::GetApplication()->createTimer(timer, delay / 1000.0);
}
void Timer::stop(fl::flash::utils::Timer* timer){
	jfl::GetApplication()->destroyTimer(timer);
}
}
#endif
