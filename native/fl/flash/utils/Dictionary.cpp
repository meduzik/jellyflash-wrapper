#if __has_include(<fl/flash/utils/Dictionary.h>)
#include <jfl/common_impl.h>
#include <fl/flash/utils/Dictionary.h>
namespace fl::flash::utils{

using namespace jfl;

Dictionary::~Dictionary(){

}

void Dictionary::jfl_GC(jfl::Object* jfl_this, void*){
	Dictionary* self = (Dictionary*)jfl_this;
	if ( self->weak ){
		jfl::gc_visit_weak_dynamic(self->jfl_dynamic);
	}else{
		jfl::gc_visit_dynamic(self->jfl_dynamic);
	}
}

void Dictionary::jfl_p_init(fl::flash::utils::Dictionary* self, jfl::Boolean weak){
	self->weak = weak;
}

jfl::Boolean Dictionary::jfl_Iterate(jfl::Object* jfl_this, jfl::Iterator* iter) {
	Dictionary* self = (Dictionary*)jfl_this;
	return jfl::Dynamic::iterate(&self->jfl_dynamic, iter);
}

jfl::Boolean Dictionary::jfl_Dynamic(jfl::Object* jfl_this, jfl::Any* key, jfl::Any* value, jfl::dynamic_op op) {
	Dictionary* self = (Dictionary*)jfl_this;
	if ( 
		op == jfl::dynamic_op::set 
		|| 
		op == jfl::dynamic_op::get
		||
		op == jfl::dynamic_op::in
		||
		op == jfl::dynamic_op::del
	){
		return self->jfl_dynamic.index(*key, value, op);
	}
	jfl_notimpl;
}

}
#endif
