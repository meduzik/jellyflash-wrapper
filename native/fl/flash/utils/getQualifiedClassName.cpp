#if __has_include(<fl/flash/utils/getQualifiedClassName.h>)
#include <jfl/common_impl.h>
#include <fl/flash/utils/getQualifiedClassName.h>
namespace fl::flash::utils{
jfl::String getQualifiedClassName(jfl::Any value){
	if ( jfl::any_is_Class(value, &jfl::Class_Class) ){
		return ((jfl::Class*)value.object)->name;
	}
	return jfl::any_class_of(value)->name;
}
}
#endif
