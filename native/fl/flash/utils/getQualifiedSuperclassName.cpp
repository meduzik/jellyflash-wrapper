#if __has_include(<fl/flash/utils/getQualifiedSuperclassName.h>)
#include <jfl/common_impl.h>
#include <fl/flash/utils/getQualifiedSuperclassName.h>
namespace fl::flash::utils{
jfl::String getQualifiedSuperclassName(jfl::Any value){
	if (jfl::any_is_Class(value, &jfl::Class_Class)) {
		auto super = jfl::super_of((const jfl::Class*)value.object);
		if ( super ){
			return super->name;
		}
		return nullptr;
	}
	const jfl::Class* class_ = jfl::any_class_of(value);
	auto super = jfl::super_of(class_);
	if (super) {
		return super->name;
	}
	return nullptr;
}
}
#endif
