#if __has_include(<fl/flash/desktop/NativeApplication.h>)
#include <jfl/common_impl.h>
#include <fl/flash/desktop/NativeApplication.h>
namespace fl::flash::desktop{
fl::flash::display::NativeWindow* NativeApplication::jfl_g_activeWindow(fl::flash::desktop::NativeApplication* jfl_t){
	jfl_notimpl;
}
fl::Array* NativeApplication::jfl_g_openedWindows(fl::flash::desktop::NativeApplication* jfl_t){
	jfl_notimpl;
}
jfl::String NativeApplication::jfl_g_systemIdleMode(fl::flash::desktop::NativeApplication* jfl_t){
	//jfl_notimpl();
	return jfl::StringSmall{4, "idle"};
}
void NativeApplication::jfl_s_systemIdleMode(fl::flash::desktop::NativeApplication* jfl_t, jfl::String jfl_a_value){
	//jfl_notimpl();
}
void NativeApplication::exit(fl::flash::desktop::NativeApplication* jfl_t){
	std::exit(0);
}
}
#endif
