#if __has_include(<fl/flash/desktop/NativeProcess.h>)
#include <jfl/common_impl.h>
#include <fl/flash/desktop/NativeProcess.h>
namespace fl::flash::desktop{
void NativeProcess::closeInput(fl::flash::desktop::NativeProcess* jfl_t){
	jfl_notimpl;
}
jfl::Boolean NativeProcess::jfl_g_running(fl::flash::desktop::NativeProcess* jfl_t){
	jfl_notimpl;
}
jfl::I<fl::flash::utils::IDataInput> NativeProcess::jfl_g_standardError(fl::flash::desktop::NativeProcess* jfl_t){
	jfl_notimpl;
}
jfl::I<fl::flash::utils::IDataOutput> NativeProcess::jfl_g_standardInput(fl::flash::desktop::NativeProcess* jfl_t){
	jfl_notimpl;
}
jfl::I<fl::flash::utils::IDataInput> NativeProcess::jfl_g_standardOutput(fl::flash::desktop::NativeProcess* jfl_t){
	jfl_notimpl;
}
void NativeProcess::exit(fl::flash::desktop::NativeProcess* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
void NativeProcess::jfl_p_internalStart(fl::flash::desktop::NativeProcess* jfl_t, jfl::String jfl_a_param1, jfl::Vector<jfl::String>* jfl_a_param2, jfl::String jfl_a_param3){
	jfl_notimpl;
}
jfl::Boolean NativeProcess::jfl_nflash_2einternal_nS_isValidExecutable(fl::flash::filesystem::File* jfl_a_param1){
	jfl_notimpl;
}
namespace _NativeProcess{void InboundPipe::readBytes(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::UInt jfl_a_param3){
	jfl_notimpl;
}
jfl::Boolean InboundPipe::readBoolean(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::Int InboundPipe::readByte(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::UInt InboundPipe::readUnsignedByte(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::Int InboundPipe::readShort(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::UInt InboundPipe::readUnsignedShort(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::Int InboundPipe::readInt(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::UInt InboundPipe::readUnsignedInt(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::Number InboundPipe::readFloat(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::Number InboundPipe::readDouble(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::String InboundPipe::readMultiByte(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t, jfl::UInt jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
jfl::String InboundPipe::readUTF(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::String InboundPipe::readUTFBytes(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
jfl::UInt InboundPipe::jfl_g_bytesAvailable(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::Any InboundPipe::readObject(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
jfl::UInt InboundPipe::jfl_g_objectEncoding(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
void InboundPipe::jfl_s_objectEncoding(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
jfl::String InboundPipe::jfl_g_endian(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t){
	jfl_notimpl;
}
void InboundPipe::jfl_s_endian(fl::flash::desktop::_NativeProcess::InboundPipe* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeBytes(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::UInt jfl_a_param3){
	jfl_notimpl;
}
void OutboundPipe::writeBoolean(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeByte(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeShort(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeInt(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeUnsignedInt(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeFloat(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeDouble(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::Number jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeMultiByte(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::String jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
void OutboundPipe::writeUTF(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeUTFBytes(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void OutboundPipe::writeObject(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::Any jfl_a_param1){
	jfl_notimpl;
}
jfl::UInt OutboundPipe::jfl_g_objectEncoding(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t){
	jfl_notimpl;
}
void OutboundPipe::jfl_s_objectEncoding(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
jfl::String OutboundPipe::jfl_g_endian(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t){
	jfl_notimpl;
}
void OutboundPipe::jfl_s_endian(fl::flash::desktop::_NativeProcess::OutboundPipe* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
}
}
#endif
