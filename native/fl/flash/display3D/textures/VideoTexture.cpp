#if __has_include(<fl/flash/display3D/textures/VideoTexture.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display3D/textures/VideoTexture.h>
namespace fl::flash::display3D::textures{
jfl::Int VideoTexture::jfl_g_videoWidth(fl::flash::display3D::textures::VideoTexture* jfl_t){
	jfl_notimpl;
}
jfl::Int VideoTexture::jfl_g_videoHeight(fl::flash::display3D::textures::VideoTexture* jfl_t){
	jfl_notimpl;
}
}
#endif
