#if __has_include(<fl/flash/display3D/textures/CubeTexture.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display3D/textures/CubeTexture.h>
namespace fl::flash::display3D::textures{
void CubeTexture::uploadFromBitmapData(fl::flash::display3D::textures::CubeTexture* jfl_t, fl::flash::display::BitmapData* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::UInt jfl_a_param3){
	jfl_notimpl;
}
void CubeTexture::uploadFromByteArray(fl::flash::display3D::textures::CubeTexture* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::UInt jfl_a_param3, jfl::UInt jfl_a_param4){
	jfl_notimpl;
}
void CubeTexture::uploadCompressedTextureFromByteArray(fl::flash::display3D::textures::CubeTexture* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::Boolean jfl_a_param3){
	jfl_notimpl;
}
}
#endif
