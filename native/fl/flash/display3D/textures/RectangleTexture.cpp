#if __has_include(<fl/flash/display3D/textures/RectangleTexture.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display3D/textures/RectangleTexture.h>
#include <fl/flash/display/BitmapData.h>
#include <jfl/render/Context3D.h>

namespace fl::flash::display3D::textures{
void RectangleTexture::uploadFromBitmapData(fl::flash::display3D::textures::RectangleTexture* self, fl::flash::display::BitmapData* data){
	if (!self->impl) {
		jfl::raise_message("disposed");
	}
	if ( data->transparent ){
		dynamic_cast<jfl::render::Texture2D*>(self->impl)->uploadPremultiply(0, data->width, data->height, data->data);
	}else{
		dynamic_cast<jfl::render::Texture2D*>(self->impl)->upload(0, data->width, data->height, data->transparent, 4, data->data);
	}
}
void RectangleTexture::uploadFromByteArray(fl::flash::display3D::textures::RectangleTexture* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::UInt jfl_a_param2){
	jfl_notimpl;
}
void RectangleTexture::uploadFromBitmapDataAsync(fl::flash::display3D::textures::RectangleTexture* jfl_t, fl::flash::display::BitmapData* jfl_a_param1){
	jfl_notimpl;
}
void RectangleTexture::uploadFromByteArrayAsync(fl::flash::display3D::textures::RectangleTexture* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::UInt jfl_a_param2){
	jfl_notimpl;
}
}
#endif
