#if __has_include(<fl/flash/display3D/textures/TextureBase.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display3D/textures/TextureBase.h>
#include <jfl/render/Context3D.h>

namespace fl::flash::display3D::textures{

TextureBase::~TextureBase(){
	dispose(this);
}

void TextureBase::dispose(fl::flash::display3D::textures::TextureBase* self){
	if (self->impl) {
		self->impl->dispose();
		self->impl = nullptr;
	}
}

}
#endif
