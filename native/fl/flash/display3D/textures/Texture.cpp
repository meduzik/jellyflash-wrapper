#if __has_include(<fl/flash/display3D/textures/Texture.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display3D/textures/Texture.h>
#include <fl/flash/display/BitmapData.h>
#include <jfl/render/Context3D.h>

namespace fl::flash::display3D::textures{
void Texture::uploadFromBitmapData(fl::flash::display3D::textures::Texture* self, fl::flash::display::BitmapData* data, jfl::UInt level){
	if (!self->impl) {
		jfl::raise_message("disposed");
	}
	if (data->transparent) {
		dynamic_cast<jfl::render::Texture2D*>(self->impl)->uploadPremultiply(level, data->width, data->height, data->data);
	} else {
		dynamic_cast<jfl::render::Texture2D*>(self->impl)->upload(level, data->width, data->height, data->transparent, 4, data->data);
	}
}
void Texture::uploadFromByteArray(fl::flash::display3D::textures::Texture* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::UInt jfl_a_param3){
	jfl_notimpl;
}
void Texture::uploadCompressedTextureFromByteArray(fl::flash::display3D::textures::Texture* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::Boolean jfl_a_param3){
	jfl_notimpl;
}
void Texture::uploadFromBitmapDataAsync(fl::flash::display3D::textures::Texture* jfl_t, fl::flash::display::BitmapData* jfl_a_param1, jfl::UInt jfl_a_param2){
	jfl_notimpl;
}
void Texture::uploadFromByteArrayAsync(fl::flash::display3D::textures::Texture* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::UInt jfl_a_param2, jfl::UInt jfl_a_param3){
	jfl_notimpl;
}
}
#endif
