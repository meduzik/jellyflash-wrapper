#if __has_include(<fl/flash/display3D/IndexBuffer3D.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display3D/IndexBuffer3D.h>
#include <fl/flash/utils/ByteArray.h>
#include <jfl/render/Context3D.h>

namespace fl::flash::display3D{
IndexBuffer3D::~IndexBuffer3D() {
	dispose(this);
}
void IndexBuffer3D::uploadFromVector(fl::flash::display3D::IndexBuffer3D* jfl_t, jfl::Vector<jfl::UInt>* jfl_a_param1, jfl::Int jfl_a_param2, jfl::Int jfl_a_param3){
	jfl_notimpl;
}
void IndexBuffer3D::uploadFromByteArray(fl::flash::display3D::IndexBuffer3D* self, fl::flash::utils::ByteArray* data, jfl::Int byteArrayOffset, jfl::Int startIndex, jfl::Int numIndices){
	if (!self->impl) {
		jfl::raise_message("disposed");
	}
	if ( numIndices < 0 ){
		jfl::raise_message("invalid upload range");
	}
	if (startIndex < 0) {
		jfl::raise_message("invalid upload range");
	}
	utils::ByteArray::check_size(data, byteArrayOffset + numIndices * 2);
	self->impl->upload(data->data.data() + byteArrayOffset, startIndex, numIndices);
}
void IndexBuffer3D::dispose(fl::flash::display3D::IndexBuffer3D* self){
	if (self->impl){
		self->impl->dispose();
		self->impl = nullptr;
	}
}
}
#endif
