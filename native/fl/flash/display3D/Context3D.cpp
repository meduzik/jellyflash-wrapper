#if __has_include(<fl/flash/display3D/Context3D.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display3D/Context3D.h>
#include <fl/flash/display3D/VertexBuffer3D.h>
#include <fl/flash/display3D/IndexBuffer3D.h>
#include <fl/flash/display3D/Program3D.h>
#include <fl/flash/display3D/Context3DBufferUsage.h>
#include <fl/flash/display3D/textures/RectangleTexture.h>
#include <fl/flash/display3D/textures/CubeTexture.h>
#include <fl/flash/display3D/textures/Texture.h>
#include <fl/flash/display3D/textures/TextureBase.h>
#include <fl/flash/geom/Matrix3D.h>
#include <fl/flash/geom/Rectangle.h>
#include <jfl/render/Context3D.h>
namespace fl::flash::display3D{

jfl::render::ProgramType ConvertProgramType(jfl::String type){
	if (type.tag == jfl::type_tag::Null) {
		jfl::raise_nullarg();
	}
	std::string_view view = type.to_string_view();
	if ( view == "vertex" ) {
		return jfl::render::ProgramType::Vertex;
	}else if ( view == "fragment" ){
		return jfl::render::ProgramType::Fragment;
	}else{
		jfl::raise_message("invalid program type");
	}
}

jfl::render::TriangleFace ConvertTriangleFace(jfl::String face) {
	if (face.tag == jfl::type_tag::Null) {
		jfl::raise_nullarg();
	}
	std::string_view view = face.to_string_view();
	if (view == "back") {
		return jfl::render::TriangleFace::Back;
	} else if (view == "front") {
		return jfl::render::TriangleFace::Front;
	} else if (view == "frontAndBack") {
		return jfl::render::TriangleFace::FrontAndBack;
	} else if (view == "none") {
		return jfl::render::TriangleFace::None;
	} else {
		jfl::raise_message("invalid triangle face");
	}
}

jfl::render::BufferFormat ConvertBufferFormat(jfl::String format) {
	if (format.tag == jfl::type_tag::Null) {
		jfl::raise_nullarg();
	}
	std::string_view view = format.to_string_view();
	if (view == "bytes4") {
		return jfl::render::BufferFormat::Bytes4;
	} else if (view == "float1") {
		return jfl::render::BufferFormat::Float1;
	} else if (view == "float2") {
		return jfl::render::BufferFormat::Float2;
	} else if (view == "float3") {
		return jfl::render::BufferFormat::Float3;
	} else if (view == "float4") {
		return jfl::render::BufferFormat::Float4;
	} else {
		jfl::raise_message("invalid buffer format");
	}
}

jfl::render::StencilAction ConvertStencilAction(jfl::String action){
	using jfl::render::StencilAction;

	if (action.tag == jfl::type_tag::Null ){
		jfl::raise_nullarg();
	}
	std::string_view view = action.to_string_view();
	if ( view == "decrementSaturate" ){
		return StencilAction::DecrementSaturate;
	} else if (view == "decrementWrap") {
		return StencilAction::DecrementWrap;
	} else if (view == "incrementSaturate") {
		return StencilAction::IncrementSaturate;
	} else if (view == "incrementWrap") {
		return StencilAction::IncrementWrap;
	} else if (view == "invert") {
		return StencilAction::Invert;
	} else if (view == "keep") {
		return StencilAction::Keep;
	} else if (view == "set") {
		return StencilAction::Set;
	} else if (view == "zero") {
		return StencilAction::Zero;
	} else {
		jfl::raise_message("invalid stencil action");
	}
}

jfl::render::CompareMode ConvertCompareMode(jfl::String mode){
	using jfl::render::CompareMode;

	if ( mode.tag == jfl::type_tag::Null ){
		jfl::raise_nullarg();
	}
	std::string_view view = mode.to_string_view();
	if ( view == "always" ){
		return CompareMode::Always;
	} else if (view == "equal") {
		return CompareMode::Equal;
	} else if (view == "greater") {
		return CompareMode::Greater;
	} else if (view == "greaterEqual") {
		return CompareMode::GreaterEqual;
	} else if (view == "less") {
		return CompareMode::Less;
	} else if (view == "lessEqual") {
		return CompareMode::LessEqual;
	} else if (view == "never") {
		return CompareMode::Never;
	} else if (view == "notEqual") {
		return CompareMode::NotEqual;
	} else {
		jfl::raise_message("invalid compare mode");
	}
}


jfl::render::BlendFactor ConvertBlendFactor(jfl::String factor) {
	using jfl::render::BlendFactor;

	if (factor.tag == jfl::type_tag::Null) {
		jfl::raise_nullarg();
	}
	std::string_view view = factor.to_string_view();
	if (view == "destinationAlpha") {
		return BlendFactor::DestAlpha;
	} else if (view == "destinationColor") {
		return BlendFactor::DestColor;
	} else if (view == "one") {
		return BlendFactor::One;
	} else if (view == "oneMinusDestinationAlpha") {
		return BlendFactor::OneMinusDestAlpha;
	} else if (view == "oneMinusDestinationColor") {
		return BlendFactor::OneMinusDestColor;
	} else if (view == "oneMinusSourceAlpha") {
		return BlendFactor::OneMinusSrcAlpha;
	} else if (view == "oneMinusSourceColor") {
		return BlendFactor::OneMinusSrcColor;
	} else if (view == "sourceAlpha") {
		return BlendFactor::SrcAlpha;
	} else if (view == "sourceColor") {
		return BlendFactor::SrcColor;
	} else if (view == "zero") {
		return BlendFactor::Zero;
	} else {
		jfl::raise_message("invalid blend factor");
	}
}


jfl::render::Wrap ConvertWrap(jfl::String wrap) {
	using jfl::render::Wrap;

	if (wrap.tag == jfl::type_tag::Null) {
		jfl::raise_nullarg();
	}
	std::string_view view = wrap.to_string_view();
	if (view == "clamp") {
		return Wrap::Clamp;
	} else if (view == "clamp_u_repeat_v") {
		return Wrap::ClampURepeatV;
	} else if (view == "repeat") {
		return Wrap::Repeat;
	} else if (view == "repeat_u_clamp_v") {
		return Wrap::RepeatUClampV;
	} else {
		jfl::raise_message("invalid wrap mode");
	}
}


jfl::render::TextureFilter ConvertTextureFilter(jfl::String filter) {
	using jfl::render::TextureFilter;

	if (filter.tag == jfl::type_tag::Null) {
		jfl::raise_nullarg();
	}
	std::string_view view = filter.to_string_view();
	if (view == "anisotropic16x") {
		return TextureFilter::Anisotropic16x;
	} else if (view == "anisotropic8x") {
		return TextureFilter::Anisotropic8x;
	} else if (view == "anisotropic4x") {
		return TextureFilter::Anisotropic4x;
	} else if (view == "anisotropic2x") {
		return TextureFilter::Anisotropic2x;
	} else if (view == "linear") {
		return TextureFilter::Linear;
	} else if (view == "nearest") {
		return TextureFilter::Nearest;
	} else {
		jfl::raise_message("invalid texture filter");
	}
}


jfl::render::MipFilter ConvertMipFilter(jfl::String filter) {
	using jfl::render::MipFilter;

	if (filter.tag == jfl::type_tag::Null) {
		jfl::raise_nullarg();
	}
	std::string_view view = filter.to_string_view();
	if (view == "miplinear") {
		return MipFilter::Linear;
	} else if (view == "mipnearest") {
		return MipFilter::Nearest;
	} else if (view == "mipnone") {
		return MipFilter::None;
	} else {
		jfl::raise_message("invalid mip filter");
	}
}


void Context3D::jfl_p_init(fl::flash::display3D::Context3D* self){
	self->impl = jfl::render::CreateContext3DImpl();
}

jfl::String Context3D::jfl_g_driverInfo(fl::flash::display3D::Context3D* self){
	return self->impl->getDriverInfo();
}
jfl::Number Context3D::jfl_g_totalGPUMemory(fl::flash::display3D::Context3D* jfl_t){
	jfl_notimpl;
}
void Context3D::dispose(fl::flash::display3D::Context3D* jfl_t, jfl::Boolean jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean Context3D::jfl_g_enableErrorChecking(fl::flash::display3D::Context3D* self){
	return self->impl->getErrorChecking();
}
void Context3D::jfl_s_enableErrorChecking(fl::flash::display3D::Context3D* self, jfl::Boolean value){
	self->impl->setErrorChecking(value);
}
void Context3D::configureBackBuffer(fl::flash::display3D::Context3D* self, jfl::Int width, jfl::Int height, jfl::Int antiAlias, jfl::Boolean depthAndStencil, jfl::Boolean wantsBestResolution, jfl::Boolean wantsBestResolutionOnBrowserZoom){
	self->impl->configureBackBuffer(width, height, depthAndStencil);
}
void Context3D::clear(fl::flash::display3D::Context3D* self, jfl::Number red, jfl::Number green, jfl::Number blue, jfl::Number alpha, jfl::Number depth, jfl::UInt stencil, jfl::UInt mask){
	self->impl->clear(red, green, blue, alpha, depth, stencil, (jfl::render::ClearMask)mask);
}
void Context3D::drawTriangles(fl::flash::display3D::Context3D* self, fl::flash::display3D::IndexBuffer3D* ibo, jfl::Int firstIndex, jfl::Int numTriangles){
	self->impl->drawTriangles(ibo->impl, firstIndex, numTriangles);
}
void Context3D::present(fl::flash::display3D::Context3D* self){
	self->impl->present();
}
void Context3D::setProgram(fl::flash::display3D::Context3D* self, fl::flash::display3D::Program3D* program){
	self->impl->setProgram(program->impl);
}
void Context3D::setProgramConstantsFromVector(fl::flash::display3D::Context3D* self, jfl::String type, jfl::Int firstIndex, jfl::Vector<jfl::Number>* vec, jfl::Int count){
	if (!vec) {
		jfl::raise_nullarg();
	}
	if ( count < 0 ){
		count = vec->length / 4;
	}
	if ( vec->length < (jfl::UInt)(count * 4) ){
		jfl::raise_message("not enough data in vector");
	}
	jfl::render::ProgramType programType = ConvertProgramType(type);
	for (int i = 0; i < count; i++) {
		self->impl->setProgramConstant(programType, firstIndex + i, vec->ptr + i * 4);
	}
}
void Context3D::setProgramConstantsFromMatrix(fl::flash::display3D::Context3D* self, jfl::String type, jfl::Int firstIndex, fl::flash::geom::Matrix3D* matrix, jfl::Boolean transpose){
	if ( !matrix ){
		jfl::raise_nullarg();
	}
	double data[4][4];
	memcpy(data, matrix->m, sizeof(data));
	if ( transpose ){
		for ( int i = 0; i < 4; i++ ){
			for ( int j = 0; j < i; j++ ){
				std::swap(data[i][j], data[j][i]);
			}
		}
	}
	jfl::render::ProgramType programType = ConvertProgramType(type);
	for (int i = 0; i < 4; i++) {
		self->impl->setProgramConstant(programType, firstIndex + i, data[i]);
	}
}
void Context3D::setProgramConstantsFromByteArray(fl::flash::display3D::Context3D* jfl_t, jfl::String jfl_a_param1, jfl::Int jfl_a_param2, jfl::Int jfl_a_param3, fl::flash::utils::ByteArray* jfl_a_param4, jfl::UInt jfl_a_param5){
	jfl_notimpl;
}
void Context3D::setVertexBufferAt(fl::flash::display3D::Context3D* self, jfl::Int index, fl::flash::display3D::VertexBuffer3D* vbo, jfl::Int offset, jfl::String format){
	if (!vbo) {
		self->impl->setVertexBufferAt(index, nullptr, 0, jfl::render::BufferFormat::Float1);
	} else {
		self->impl->setVertexBufferAt(index, vbo->impl, offset, ConvertBufferFormat(format));
	}
}
void Context3D::setBlendFactors(fl::flash::display3D::Context3D* self, jfl::String source, jfl::String dest){
	self->impl->setBlendFactors(ConvertBlendFactor(source), ConvertBlendFactor(dest));
}
void Context3D::setColorMask(fl::flash::display3D::Context3D* jfl_t, jfl::Boolean jfl_a_param1, jfl::Boolean jfl_a_param2, jfl::Boolean jfl_a_param3, jfl::Boolean jfl_a_param4){
	jfl_notimpl;
}
void Context3D::setDepthTest(fl::flash::display3D::Context3D* self, jfl::Boolean depthMask, jfl::String passCompareMode){
	jfl::render::CompareMode mode = ConvertCompareMode(passCompareMode);
	self->impl->setDepthTest(depthMask, mode);
}
void Context3D::setRenderToBackBuffer(fl::flash::display3D::Context3D* self){
	self->impl->setRenderToBackBuffer();
}
void Context3D::jfl_p_setRenderToTextureInternal(fl::flash::display3D::Context3D* self, fl::flash::display3D::textures::TextureBase* texture, jfl::Int jfl_a_param2, jfl::Boolean depth_and_stencil, jfl::Int antialias, jfl::Int surface, jfl::Int color_output){
	self->impl->setRenderToTexture(texture->impl, depth_and_stencil, antialias, surface, color_output);
}
void Context3D::setCulling(fl::flash::display3D::Context3D* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void Context3D::setStencilActions(fl::flash::display3D::Context3D* self, jfl::String face, jfl::String compare, jfl::String pass, jfl::String depthFail, jfl::String fail){
	self->impl->setStencilActions(
		ConvertTriangleFace(face), 
		ConvertCompareMode(compare), 
		ConvertStencilAction(pass), 
		ConvertStencilAction(depthFail), 
		ConvertStencilAction(fail)
	);
}
void Context3D::setStencilReferenceValue(fl::flash::display3D::Context3D* self, jfl::UInt ref, jfl::UInt read_mask, jfl::UInt write_mask){
	self->impl->setStencilReference(ref, read_mask, write_mask);
}
void Context3D::setScissorRectangle(fl::flash::display3D::Context3D* self, fl::flash::geom::Rectangle* rect){
	if ( rect ){
		self->impl->setScissor((int)rect->x, (int)rect->y, (int)rect->width, (int)rect->height);
	}else{
		self->impl->unsetScissor();
	}
}

fl::flash::display3D::VertexBuffer3D* Context3D::createVertexBuffer(fl::flash::display3D::Context3D* self, jfl::Int numVertices, jfl::Int dataPerVertex, jfl::String bufferUsage){
	jfl::render::BufferUsage usage;
	if ( jfl::str_eq(bufferUsage, Context3DBufferUsage::jfl_S_STATIC_DRAW) ){
		usage = jfl::render::BufferUsage::Static;
	}else if (jfl::str_eq(bufferUsage, Context3DBufferUsage::jfl_S_DYNAMIC_DRAW)) {
		usage = jfl::render::BufferUsage::Dynamic;
	}else{
		jfl::raise_message("invalid buffer usage");
	}
	VertexBuffer3D* vbo = VertexBuffer3D::jfl_New();
	vbo->impl = self->impl->createVertexBuffer(numVertices, dataPerVertex, usage);
	return vbo;
}

fl::flash::display3D::IndexBuffer3D* Context3D::createIndexBuffer(fl::flash::display3D::Context3D* self, jfl::Int numIndices, jfl::String bufferUsage){
	jfl::render::BufferUsage usage;
	if (jfl::str_eq(bufferUsage, Context3DBufferUsage::jfl_S_STATIC_DRAW)) {
		usage = jfl::render::BufferUsage::Static;
	} else if (jfl::str_eq(bufferUsage, Context3DBufferUsage::jfl_S_DYNAMIC_DRAW)) {
		usage = jfl::render::BufferUsage::Dynamic;
	} else {
		jfl::raise_message("invalid buffer usage");
	}
	IndexBuffer3D* ibo = IndexBuffer3D::jfl_New();
	ibo->impl = self->impl->createIndexBuffer(numIndices, usage);
	return ibo;
}
fl::flash::display3D::textures::Texture* Context3D::createTexture(fl::flash::display3D::Context3D* self, jfl::Int width, jfl::Int height, jfl::String format, jfl::Boolean optimizeForRender, jfl::Int streaming){
	textures::Texture* texture = textures::Texture::jfl_New();
	jfl::render::TextureFormat textureFormat;
	if (jfl::str_eq(format, jfl::StringSmall{4, "bgra"})) {
		textureFormat = jfl::render::TextureFormat::BGRA;
	} else if (jfl::str_eq(format, jfl::StringSmall{3, "bgr"})) {
		textureFormat = jfl::render::TextureFormat::BGR;
	} else {
		jfl::raise_message("unsupported texture format");
	}
	texture->impl = self->impl->createTexture2D(width, height, textureFormat, optimizeForRender, true);
	return texture;
}
fl::flash::display3D::textures::CubeTexture* Context3D::createCubeTexture(fl::flash::display3D::Context3D* jfl_t, jfl::Int jfl_a_param1, jfl::String jfl_a_param2, jfl::Boolean jfl_a_param3, jfl::Int jfl_a_param4){
	jfl_notimpl;
}
fl::flash::display3D::textures::RectangleTexture* Context3D::createRectangleTexture(fl::flash::display3D::Context3D* self, jfl::Int width, jfl::Int height, jfl::String format, jfl::Boolean optimizeForRender){
	textures::RectangleTexture* texture = textures::RectangleTexture::jfl_New();
	jfl::render::TextureFormat textureFormat;
	if ( jfl::str_eq(format, jfl::StringSmall{4, "bgra"}) ){
		textureFormat = jfl::render::TextureFormat::BGRA;
	}else if (jfl::str_eq(format, jfl::StringSmall{3, "bgr"})) {
		textureFormat = jfl::render::TextureFormat::BGR;
	}else{
		jfl::raise_message("unsupported texture format");
	}
	texture->impl = self->impl->createTexture2D(width, height, textureFormat, optimizeForRender, false);
	return texture;
}
fl::flash::display3D::Program3D* Context3D::createProgram(fl::flash::display3D::Context3D* self){
	Program3D* program = Program3D::jfl_New();
	program->impl = self->impl->createProgram();
	return program;
}
void Context3D::setSamplerStateAt(fl::flash::display3D::Context3D* self, jfl::Int sampler, jfl::String wrap, jfl::String filter, jfl::String mipfilter){
	self->impl->setSamplerStateAt(sampler, ConvertWrap(wrap), ConvertTextureFilter(filter), ConvertMipFilter(mipfilter));
}
jfl::String Context3D::jfl_g_profile(fl::flash::display3D::Context3D* jfl_t){
	jfl_notimpl;
}
void Context3D::jfl_p_setTextureInternal(fl::flash::display3D::Context3D* self, jfl::Int index, fl::flash::display3D::textures::Texture* texture){
	self->impl->setTextureAt(index, texture ? texture->impl : nullptr);
}
void Context3D::jfl_p_setCubeTextureInternal(fl::flash::display3D::Context3D* jfl_t, jfl::Int jfl_a_param1, fl::flash::display3D::textures::CubeTexture* jfl_a_param2){
	jfl_notimpl;
}
void Context3D::jfl_p_setRectangleTextureInternal(fl::flash::display3D::Context3D* self, jfl::Int index, fl::flash::display3D::textures::RectangleTexture* texture){
	self->impl->setTextureAt(index, texture ? texture->impl : nullptr);
}
void Context3D::jfl_p_setVideoTextureInternal(fl::flash::display3D::Context3D* jfl_t, jfl::Int jfl_a_param1, fl::flash::display3D::textures::VideoTexture* jfl_a_param2){
	jfl_notimpl;
}
jfl::Int Context3D::jfl_g_backBufferWidth(fl::flash::display3D::Context3D* self){
	return self->impl->getBackBufferWidth();
}
jfl::Int Context3D::jfl_g_backBufferHeight(fl::flash::display3D::Context3D* self){
	return self->impl->getBackBufferHeight();
}
jfl::Int Context3D::jfl_g_maxBackBufferWidth(fl::flash::display3D::Context3D* jfl_t){
	jfl_notimpl;
}
void Context3D::jfl_s_maxBackBufferWidth(fl::flash::display3D::Context3D* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
jfl::Int Context3D::jfl_g_maxBackBufferHeight(fl::flash::display3D::Context3D* jfl_t){
	jfl_notimpl;
}
void Context3D::jfl_s_maxBackBufferHeight(fl::flash::display3D::Context3D* jfl_t, jfl::Int jfl_a_param1){
	jfl_notimpl;
}
void Context3D::drawTrianglesInstanced(fl::flash::display3D::Context3D* jfl_t, fl::flash::display3D::IndexBuffer3D* jfl_a_param1, jfl::Int jfl_a_param2, jfl::Int jfl_a_param3, jfl::Int jfl_a_param4){
	jfl_notimpl;
}
fl::flash::display3D::VertexBuffer3D* Context3D::createVertexBufferForInstances(fl::flash::display3D::Context3D* jfl_t, jfl::Int jfl_a_param1, jfl::Int jfl_a_param2, jfl::Int jfl_a_param3, jfl::String jfl_a_param4){
	jfl_notimpl;
}
fl::flash::display3D::textures::VideoTexture* Context3D::createVideoTexture(fl::flash::display3D::Context3D* jfl_t){
	jfl_notimpl;
}
void Context3D::setFillMode(fl::flash::display3D::Context3D* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
void Context3D::drawToBitmapData(fl::flash::display3D::Context3D* jfl_t, fl::flash::display::BitmapData* jfl_a_param1, fl::flash::geom::Rectangle* jfl_a_param2, fl::flash::geom::Point* jfl_a_param3){
	jfl_notimpl;
}
jfl::Boolean Context3D::jfl_Sg_supportsVideoTexture(){
	jfl_notimpl;
}
}
#endif
