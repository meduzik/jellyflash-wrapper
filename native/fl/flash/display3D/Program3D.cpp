#if __has_include(<fl/flash/display3D/Program3D.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display3D/Program3D.h>
#include <fl/flash/utils/ByteArray.h>
#include <jfl/render/Context3D.h>
namespace fl::flash::display3D{
Program3D::~Program3D() {
	dispose(this);
}
void Program3D::upload(fl::flash::display3D::Program3D* self, fl::flash::utils::ByteArray* vertexData, fl::flash::utils::ByteArray* fragmentData){
	if ( !self->impl ){
		jfl::raise_message("disposed");
	}
	if ( !vertexData ){
		jfl::raise_message("Program3D.upload vertexData cannot be null");
	}
	if ( !fragmentData ){
		jfl::raise_message("Program3D.upload fragmentData cannot be null");
	}
	self->impl->upload(vertexData->data, fragmentData->data);
}
void Program3D::dispose(fl::flash::display3D::Program3D* self){
	if (self->impl) {
		self->impl->dispose();
		self->impl = nullptr;
	}
}
}
#endif
