#if __has_include(<fl/flash/display3D/VertexBuffer3D.h>)
#include <jfl/common_impl.h>
#include <fl/flash/display3D/VertexBuffer3D.h>
#include <fl/flash/utils/ByteArray.h>
#include <jfl/render/Context3D.h>

namespace fl::flash::display3D{
VertexBuffer3D::~VertexBuffer3D() {
	dispose(this);
}
void VertexBuffer3D::uploadFromVector(fl::flash::display3D::VertexBuffer3D* self, jfl::Vector<jfl::Number>* data, jfl::Int startVertex, jfl::Int numVertices){
	jfl::raise_message("uploading vertex buffer from vector is not allowed");
}

void VertexBuffer3D::uploadFromByteArray(fl::flash::display3D::VertexBuffer3D* self, fl::flash::utils::ByteArray* data, jfl::Int byteArrayOffset, jfl::Int startVertex, jfl::Int numVertices){
	if (!self->impl) {
		jfl::raise_message("disposed");
	}
	if (numVertices < 0) {
		jfl::raise_message("invalid upload range");
	}
	if (startVertex < 0) {
		jfl::raise_message("invalid upload range");
	}
	utils::ByteArray::check_size(data, byteArrayOffset + numVertices * self->impl->getBytesPerVertex());
	self->impl->upload(data->data.data() + byteArrayOffset, startVertex, numVertices);
}

void VertexBuffer3D::dispose(fl::flash::display3D::VertexBuffer3D* self){
	if (self->impl) {
		self->impl->dispose();
		self->impl = nullptr;
	}
}

}
#endif
