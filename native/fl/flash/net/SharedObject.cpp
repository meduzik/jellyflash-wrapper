#if __has_include(<fl/flash/net/SharedObject.h>)
#include <jfl/common_impl.h>
#include <fl/flash/net/SharedObject.h>
#include <fl/flash/utils/ByteArray.h>
namespace fl::flash::net {
using utils::ByteArray;
using namespace jfl;

#define SHARED_OBJECT_TAG_OBJECT 1
#define SHARED_OBJECT_TAG_STRING 2
#define SHARED_OBJECT_TAG_NULL 3
#define SHARED_OBJECT_TAG_INT 4
#define SHARED_OBJECT_TAG_UINT 5
#define SHARED_OBJECT_TAG_NUMBER 6
#define SHARED_OBJECT_TAG_BOOLEAN_TRUE 7
#define SHARED_OBJECT_TAG_BOOLEAN_FALSE 8
#define SHARED_OBJECT_TAG_ARRAY 9
#define SHARED_OBJECT_TAG_BYTE_ARRAY 10
#define SHARED_OBJECT_TAG_END 0

struct DataDecoder{
	DataDecoder(const arena_vec<uint8_t>& data):
		cur(data.data()),
		end(cur + data.size())
	{
	}

	jfl::UInt decode_uint(){
		advance();
		jfl::UInt val = 0;
		read_data(&val, sizeof(jfl::UInt));
		return val;
	}

	Any decode_any(){
		switch (peek()) {
		case SHARED_OBJECT_TAG_OBJECT:{
			advance();
			return (jfl::Object*)decode_object();
		}break;
		case SHARED_OBJECT_TAG_NULL:{
			advance();
			return nullptr;
		}break;
		case SHARED_OBJECT_TAG_BOOLEAN_FALSE: {
			advance();
			return false;
		}break;
		case SHARED_OBJECT_TAG_BOOLEAN_TRUE: {
			advance();
			return true;
		}break;
		case SHARED_OBJECT_TAG_INT: {
			advance();
			jfl::Int val = 0;
			read_data(&val, sizeof(jfl::Int));
			return val;
		}break;
		case SHARED_OBJECT_TAG_UINT: {
			return decode_uint();
		}break;
		case SHARED_OBJECT_TAG_NUMBER: {
			advance();
			jfl::Number val = 0;
			read_data(&val, sizeof(jfl::Number));
			return val;
		}break;
		case SHARED_OBJECT_TAG_STRING: {
			advance();
			return decode_string();
		}break;
		case SHARED_OBJECT_TAG_BYTE_ARRAY:{
			advance();
			jfl::UInt size = decode_uint();
			ByteArray* byte_array = ByteArray::jfl_New();
			byte_array->data.resize(size);
			read_data(byte_array->data.data(), size);
			return (jfl::Object*)byte_array;
		}break;
		case SHARED_OBJECT_TAG_ARRAY: {
			advance();
			Array* array = Array::jfl_New(params({}));
			while (peek() == SHARED_OBJECT_TAG_UINT){
				advance();
				jfl::UInt index;
				read_data(&index, sizeof(index));
				Any value = decode_any();
				Array::jfl_set(array, index, value);
			}
			advance();
			decode_dynamic(array->jfl_dynamic);
			return (jfl::Object*)array;
		}break;
		default:{
			return nullptr;
		}break;
		}
	}

	fl::Object* decode_object(){
		fl::Object* object = fl::Object::jfl_New();
		decode_dynamic(object->jfl_dynamic);
		return object;
	}

	jfl::String decode_string(){
		uint16_t strlen = 0;
		read_data(&strlen, sizeof(strlen));
		if (strlen <= (uint16_t)jfl::type_tag::SmallStringEnd) {
			jfl::StringSmall ss;
			ss.length = (uint8_t)strlen;
			read_data(ss.value, strlen);
			return jfl::String(ss);
		} else {
			jfl::StringObject* object = jfl::StringObject::New(strlen);
			read_data((void*)object->data, strlen);
			return jfl::String(object);
		}
	}

	void decode_dynamic(Dynamic& dynamic){
		while (peek() == SHARED_OBJECT_TAG_STRING) {
			advance();
			jfl::String key = decode_string();
			Any value = decode_any();
			dynamic.map[key] = value;
		}
		advance();
	}

	void read_data(void* buf, size_t len){
		if ((size_t)(end - cur) >= len) {
			memcpy(buf, cur, len);
			cur += len;
		}
	}

	int32_t peek(){
		if (cur == end) {
			return -1;
		}
		return *cur;
	}

	void advance(){
		cur++;
	}
private:
	const uint8_t* cur;
	const uint8_t* end;
};

struct DataEncoder{
	DataEncoder(Arena& arena):
		arena(arena),
		data(arena)
	{
	}

	void write_data(const void* src, size_t len){
		if (len > 0) {
			size_t n = data.size();
			data.resize(n + len);
			memcpy(data.data() + n, src, len);
		}
	}

	void encode_any(Any val){
		switch (val.tag){
		case type_tag::Null:
		case type_tag::Undefined:{
			encode_null();
		}break;
		case type_tag::Boolean: {
			encode_boolean(val.bval);
		}break;
		case type_tag::Int: {
			encode_int(val.ival);
		}break;
		case type_tag::UInt: {
			encode_uint(val.uval);
		}break;
		case type_tag::NumberBox: {
			encode_number(val.nval->value);
		}break;
		case type_tag::Float: {
			encode_number(val.fval);
		}break;
		case type_tag::Object:{
			auto object = val.object;
			auto klass = class_of(object);
			if (klass == &fl::Object::jfl_Class) {
				encode_object((fl::Object*)object);
			} else if (klass == &fl::Array::jfl_Class) {
				encode_array((fl::Array*)object);
			} else if (klass == &ByteArray::jfl_Class) {
				encode_byte_array((ByteArray*)object);
			} else {
				jfl::raise_message("non-encodable object inside SharedObject");
			}
		}break;
		default:{
			if (val.tag <= type_tag::StringEnd) {
				encode_string(*(jfl::String*)&val);
			}else{
				jfl::raise_message("non-encodable object inside SharedObject");
			}
		}break;
		}
	}

	void encode_null(){
		data.push_back(SHARED_OBJECT_TAG_NULL);
	}

	void encode_boolean(bool value) {
		if (value) {
			data.push_back(SHARED_OBJECT_TAG_BOOLEAN_TRUE);
		}else{
			data.push_back(SHARED_OBJECT_TAG_BOOLEAN_FALSE);
		}
	}

	void encode_int(jfl::Int value) {
		data.push_back(SHARED_OBJECT_TAG_INT);
		write_data(&value, sizeof(value));
	}

	void encode_uint(jfl::UInt value) {
		data.push_back(SHARED_OBJECT_TAG_UINT);
		write_data(&value, sizeof(value));
	}

	void encode_number(jfl::Number value) {
		data.push_back(SHARED_OBJECT_TAG_NUMBER);
		write_data(&value, sizeof(value));
	}

	void encode_string(jfl::String str){
		data.push_back(SHARED_OBJECT_TAG_STRING);
		auto view = str.to_string_view();
		uint16_t size = (uint16_t)view.size();
		write_data(&size, sizeof(uint16_t));
		write_data(view.data(), view.size());
	}

	void encode_array(fl::Array* array){
		data.push_back(SHARED_OBJECT_TAG_ARRAY);
		if (array->contents.is_contiguous) {
			auto& vec = array->contents.vec;
			for (size_t i = 0; i < vec.size(); i++) {
				encode_uint(i);
				encode_any(vec[i]);
			}
		} else {
			auto& map = array->contents.map;
			for (auto& it: map){
				encode_uint(it.first);
				encode_any(it.second);
			}
		}
		data.push_back(SHARED_OBJECT_TAG_END);
		encode_dynamic(array->jfl_dynamic);
	}

	void encode_byte_array(ByteArray* byte_array){
		data.push_back(SHARED_OBJECT_TAG_BYTE_ARRAY);
		jfl::UInt len = byte_array->data.size();
		encode_uint(len);
		write_data(byte_array->data.data(), len);
	}

	void encode_dynamic(Dynamic& dynamic){
		for (auto& it : dynamic.map) {
			jfl::String str = any_as_String(it.first);
			encode_string(str);
			encode_any(it.second);
		}
		data.push_back(SHARED_OBJECT_TAG_END);
	}

	void encode_object(fl::Object* object){
		data.push_back(SHARED_OBJECT_TAG_OBJECT);
		auto& dynamic = object->jfl_dynamic;
		encode_dynamic(dynamic);
	}

	Arena& arena;
	arena_vec<uint8_t> data;
};

SharedObject::~SharedObject() {
	if (impl) {
		jfl::net::SharedObjectImplDestroy(impl);
	}
}

void SharedObject::jfl_p_init(fl::flash::net::SharedObject* self, jfl::String name, jfl::String path) {
	self->impl = nullptr;
	self->impl = jfl::net::SharedObjectImplCreate(name.to_string_view(), path.tag == type_tag::Null ? "" : path.to_string_view());
	sync(self);
}

void SharedObject::sync(fl::flash::net::SharedObject* self) {
	Arena arena;
	arena_vec<uint8_t> data(arena);
	self->size = data.size();
	jfl::net::SharedObjectImplLoad(self->impl, data);
	DataDecoder decoder(data);
	self->jfl_p__data = decoder.decode_any();
	if (self->jfl_p__data.tag != type_tag::Object) {
		self->jfl_p__data = (jfl::Object*)fl::Object::jfl_New();
	}
}

jfl::String SharedObject::flush(fl::flash::net::SharedObject* self, jfl::Int minDiskSpace) {
	Arena arena;
	DataEncoder encoder(arena);
	encoder.encode_object((fl::Object*)self->jfl_p__data.object);
	self->size = encoder.data.size();
	jfl::net::SharedObjectImplSave(self->impl, encoder.data);
	return jfl::String::Make("flushed");
}

jfl::UInt SharedObject::jfl_g_size(fl::flash::net::SharedObject* self) {
	return self->size;
}

void SharedObject::jfl_p__clear(fl::flash::net::SharedObject* self) {
	self->size = 0;
	jfl::net::SharedObjectImplClear(self->impl);
}


}
#endif
