#if __has_include(<fl/flash/net/FileFilter.h>)
#include <jfl/common_impl.h>
#include <fl/flash/net/FileFilter.h>
namespace fl::flash::net{
jfl::String FileFilter::jfl_g_description(fl::flash::net::FileFilter* jfl_t){
	jfl_notimpl;
}
void FileFilter::jfl_s_description(fl::flash::net::FileFilter* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String FileFilter::jfl_g_extension(fl::flash::net::FileFilter* jfl_t){
	jfl_notimpl;
}
void FileFilter::jfl_s_extension(fl::flash::net::FileFilter* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
jfl::String FileFilter::jfl_g_macType(fl::flash::net::FileFilter* jfl_t){
	jfl_notimpl;
}
void FileFilter::jfl_s_macType(fl::flash::net::FileFilter* jfl_t, jfl::String jfl_a_param1){
	jfl_notimpl;
}
}
#endif
