#if __has_include(<fl/flash/net/URLStream.h>)
#include <jfl/common_impl.h>
#include <fl/flash/net/URLStream.h>
#include <fl/flash/net/URLRequest.h>
#include <fl/flash/utils/Endian.h>
#include <fl/flash/utils/ByteArray.h>
namespace fl::flash::net{

void URLStream::jfl_p_init(fl::flash::net::URLStream* self){
	self->begin = 0;
	self->executor = nullptr;
	self->nativeEndian = false;
}

const uint8_t* URLStream::read_raw(URLStream* self, size_t size){
	ensure(self, size);
	return self->buffer.data() + self->begin;
}

void URLStream::ensure(URLStream* self, size_t size){
	if ( self->buffer.size() - self->begin < size ){
		jfl::raise_message("URLStream underflow");
	}
}

void URLStream::load(fl::flash::net::URLStream* self, fl::flash::net::URLRequest* request){
	stop(self);
	self->begin = 0;
	self->buffer.clear();

	jfl::String url = request->jfl_p__url;
	auto view = url.to_string_view();
	if ( view.substr(0, 4) == "app:" ){
		self->executor = jfl::CreateLocalFileStream(self, jfl::PathBase::AppPackage, view.substr(4));
	}else if ( view.substr(0, 12) == "app-storage:" ){
		self->executor = jfl::CreateLocalFileStream(self, jfl::PathBase::AppPackage, view.substr(12));
	}else{
		self->executor = jfl::CreateHttpStream(self, request);
	}
}

void URLStream::readBytes(fl::flash::net::URLStream* self, fl::flash::utils::ByteArray* bytes, jfl::UInt offset, jfl::UInt length){
	size_t to_read = (size_t)length;
	if ( to_read == 0 ){
		to_read = jfl_g_bytesAvailable(self);
	}
	if ( to_read > 0 ){
		ensure(self, to_read);
		utils::ByteArray::ensure_size(bytes, offset + to_read);
		memcpy(bytes->data.data() + offset, self->buffer.data() + self->begin, to_read);
		self->begin += to_read;
	}
}

jfl::Boolean URLStream::readBoolean(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::Int URLStream::readByte(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::UInt URLStream::readUnsignedByte(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::Int URLStream::readShort(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::UInt URLStream::readUnsignedShort(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::UInt URLStream::readUnsignedInt(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::Int URLStream::readInt(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::Number URLStream::readFloat(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::Number URLStream::readDouble(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::String URLStream::readMultiByte(fl::flash::net::URLStream* jfl_t, jfl::UInt jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
jfl::String URLStream::readUTF(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::String URLStream::readUTFBytes(fl::flash::net::URLStream* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
jfl::Boolean URLStream::jfl_g_connected(fl::flash::net::URLStream* self){
	if ( !self->executor ){
		return false;
	}
	return self->connected;
}
jfl::UInt URLStream::jfl_g_bytesAvailable(fl::flash::net::URLStream* self){
	return (jfl::UInt)(self->buffer.size() - self->begin);
}
void URLStream::close(fl::flash::net::URLStream* self){
	stop(self);
	self->buffer.clear();
	self->buffer.shrink_to_fit();
	self->begin = 0;
}
jfl::Any URLStream::readObject(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::UInt URLStream::jfl_g_objectEncoding(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
void URLStream::jfl_s_objectEncoding(fl::flash::net::URLStream* jfl_t, jfl::UInt jfl_a_param1){
	jfl_notimpl;
}
jfl::String URLStream::jfl_g_endian(fl::flash::net::URLStream* self){
	return self->nativeEndian ? utils::Endian::jfl_S_LITTLE_ENDIAN : utils::Endian::jfl_S_BIG_ENDIAN;
}
void URLStream::jfl_s_endian(fl::flash::net::URLStream* self, jfl::String value){
	if ( jfl::str_eq(value, utils::Endian::jfl_S_LITTLE_ENDIAN) ){
		self->nativeEndian = true;
	}else if ( jfl::str_eq(value, utils::Endian::jfl_S_BIG_ENDIAN)) {
		self->nativeEndian = false;
	}else{
		jfl::raise_message("invalid URLStream.endian value");
	}
}
jfl::Boolean URLStream::jfl_g_diskCacheEnabled(fl::flash::net::URLStream* jfl_t){
	jfl_notimpl;
}
jfl::Number URLStream::jfl_g_position(fl::flash::net::URLStream* jfl_t){
	return 0;
}
void URLStream::jfl_s_position(fl::flash::net::URLStream* jfl_t, jfl::Number jfl_a_param1){
	jfl::raise_message("changing URLStream.position is not supported");
}
jfl::Number URLStream::jfl_g_length(fl::flash::net::URLStream* self){
	return (jfl::Number)(self->buffer.size() - self->begin);
}
void URLStream::stop(fl::flash::net::URLStream* self){
	if ( self->executor ){
		self->executor->detach();
		self->executor = nullptr;
	}
}
}
#endif
