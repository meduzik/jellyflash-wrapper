#if __has_include(<fl/flash/net/URLRequest.h>)
#include <jfl/common_impl.h>
#include <fl/flash/net/URLRequest.h>
namespace fl::flash::net{

jfl::String URLRequest::jfl_pS_InitUserAgent(){
	return jfl::String::Make("Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Jellyflash/0.1");
}
}
#endif
