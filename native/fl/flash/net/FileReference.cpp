#if __has_include(<fl/flash/net/FileReference.h>)
#include <jfl/common_impl.h>
#include <fl/flash/net/FileReference.h>
#include <fl/Error.h>
#include <fl/flash/errors/IOError.h>

namespace fl::flash::net {
void FileReference::jfl_p__init(fl::flash::net::FileReference* file) {
}
fl::Date* FileReference::jfl_g_creationDate(fl::flash::net::FileReference* jfl_t) {
	jfl_notimpl;
}
jfl::String FileReference::jfl_g_creator(fl::flash::net::FileReference* jfl_t) {
	jfl_notimpl;
}
fl::Date* FileReference::jfl_g_modificationDate(fl::flash::net::FileReference* jfl_t) {
	jfl_notimpl;
}
jfl::String FileReference::jfl_g_name(fl::flash::net::FileReference* file) {
#if defined(__ANDROID__)
	return jfl::String::Make(file->path);
#else
	return jfl::String::Make(file->path.filename().u8string());
#endif
}
jfl::Number FileReference::jfl_g_size(fl::flash::net::FileReference* file) {
#if defined(__ANDROID__)
	jfl_notimpl;
#else
	std::error_code errc;
	uintmax_t size = std::filesystem::file_size(file->path, errc);
	if (errc != std::error_code()) {
		fl::Error::jfl_S_throwError(&fl::flash::errors::IOError::jfl_Class, 0, jfl::params({}));
	}
	return (jfl::Number)size;
#endif
}
jfl::String FileReference::jfl_g_type(fl::flash::net::FileReference* jfl_t) {
	jfl_notimpl;
}
void FileReference::cancel(fl::flash::net::FileReference* jfl_t) {
	jfl_notimpl;
}
void FileReference::download(fl::flash::net::FileReference* jfl_t, fl::flash::net::URLRequest* jfl_a_param1, jfl::String jfl_a_param2) {
	jfl_notimpl;
}
void FileReference::upload(fl::flash::net::FileReference* jfl_t, fl::flash::net::URLRequest* jfl_a_param1, jfl::String jfl_a_param2, jfl::Boolean jfl_a_param3) {
	jfl_notimpl;
}
fl::flash::utils::ByteArray* FileReference::jfl_g_data(fl::flash::net::FileReference* jfl_t) {
	jfl_notimpl;
}
void FileReference::jfl_p__load(fl::flash::net::FileReference* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1) {
	jfl_notimpl;
}
void FileReference::jfl_p__save(fl::flash::net::FileReference* jfl_t, fl::flash::utils::ByteArray* jfl_a_param1, jfl::String jfl_a_param2) {
	jfl_notimpl;
}
void FileReference::requestPermission(fl::flash::net::FileReference* jfl_t) {
	jfl_notimpl;
}
jfl::Boolean FileReference::browse(fl::flash::net::FileReference* jfl_t, fl::Array* jfl_a_param1) {
	jfl_notimpl;
}
void FileReference::uploadUnencoded(fl::flash::net::FileReference* jfl_t, fl::flash::net::URLRequest* jfl_a_param1) {
	jfl_notimpl;
}
jfl::String FileReference::jfl_Sg_permissionStatus() {
	return jfl::String::Make("granted");
}
}

#endif

