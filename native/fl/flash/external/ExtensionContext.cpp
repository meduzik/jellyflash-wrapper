#if __has_include(<fl/flash/external/ExtensionContext.h>)
#include <jfl/common_impl.h>
#include <fl/flash/external/ExtensionContext.h>
namespace fl::flash::external{
jfl::Any ExtensionContext::jfl_p__call(fl::flash::external::ExtensionContext* jfl_t, jfl::String jfl_a_param1, fl::Array* jfl_a_param2){
	jfl_notimpl;
}
void ExtensionContext::dispose(fl::flash::external::ExtensionContext* jfl_t){
	jfl_notimpl;
}
jfl::Boolean ExtensionContext::jfl_nflash_2einternal_n__disposed(fl::flash::external::ExtensionContext* jfl_t){
	jfl_notimpl;
}
jfl::Any ExtensionContext::jfl_nflash_2einternal_n_getActionScriptData(fl::flash::external::ExtensionContext* jfl_t){
	jfl_notimpl;
}
void ExtensionContext::jfl_nflash_2einternal_n_setActionScriptData(fl::flash::external::ExtensionContext* jfl_t, jfl::Any jfl_a_param1){
	jfl_notimpl;
}
fl::flash::external::ExtensionContext* ExtensionContext::jfl_pS__createExtensionContext(jfl::String jfl_a_param1, jfl::String jfl_a_param2){
	jfl_notimpl;
}
jfl::String ExtensionContext::jfl_pS__getExtensionDirectory(jfl::String jfl_a_param1){
	jfl_notimpl;
}
}
#endif
