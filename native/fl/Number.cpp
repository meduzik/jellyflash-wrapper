#if __has_include(<fl/Number.h>)
#include <jfl/common_impl.h>
#include <fl/Number.h>
namespace fl{
jfl::String Number::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toString(jfl::Number num, jfl::Any jfl_a_radix){
	jfl::Int radix = jfl::any_to_int(jfl_a_radix);
	if ( radix == 10 ){
		char buf[256];
		sprintf(buf, "%.14g", num);
		return jfl::String::Make(std::string_view(buf));
	}else{
		jfl_notimpl;
	}
}
jfl::Number Number::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_valueOf(jfl::Number jfl_t){
	jfl_notimpl;
}
jfl::String Number::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toExponential(jfl::Number jfl_t, jfl::Any jfl_a_p){
	jfl_notimpl;
}
jfl::String Number::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toPrecision(jfl::Number jfl_t, jfl::Any jfl_a_p){
	jfl_notimpl;
}
jfl::String Number::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toFixed(jfl::Number num, jfl::Any precision){
	char buf[256];
	sprintf(buf, "%.*f", jfl::any_to_int(precision), num);
	return jfl::String::Make(std::string_view(buf));
}
jfl::Number Number::jfl_S_abs(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_acos(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_asin(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_atan(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_ceil(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_cos(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_exp(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_floor(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_log(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_round(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_sin(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_sqrt(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_tan(jfl::Number jfl_a_param1){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_atan2(jfl::Number jfl_a_param1, jfl::Number jfl_a_param2){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_pow(jfl::Number jfl_a_param1, jfl::Number jfl_a_param2){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_max(jfl::Number jfl_a_param1, jfl::Number jfl_a_param2, jfl::RestParams* jfl_a_rest){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_min(jfl::Number jfl_a_param1, jfl::Number jfl_a_param2, jfl::RestParams* jfl_a_rest){
	jfl_notimpl;
}
jfl::Number Number::jfl_S_random(){
	jfl_notimpl;
}
jfl::String Number::jfl_pS__numberToString(jfl::Number jfl_a_param1, jfl::Int jfl_a_param2){
	jfl_notimpl;
}
jfl::String Number::jfl_pS__convert(jfl::Number jfl_a_param1, jfl::Int jfl_a_param2, jfl::Int jfl_a_param3){
	jfl_notimpl;
}
jfl::Number Number::jfl_pS__minValue(){
	jfl_notimpl;
}
}
#endif
