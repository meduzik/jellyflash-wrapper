#if __has_include(<fl/Date.h>)
#include <jfl/common_impl.h>
#include <fl/Date.h>
namespace fl{
void Date::jfl_p_decompose(fl::Date* self, jfl::Number time){
	std::time_t timestamp = (std::time_t)(time / 1000);
	std::tm tminfo = *std::gmtime(&timestamp);
	self->jfl_p__year = tminfo.tm_year + 1900;
	self->jfl_p__month = tminfo.tm_mon;
	self->jfl_p__date = tminfo.tm_mday;
	self->jfl_p__day = tminfo.tm_wday;
	self->jfl_p__hour = tminfo.tm_hour;
	self->jfl_p__minute = tminfo.tm_min;
	self->jfl_p__second = tminfo.tm_sec;
}
jfl::String Date::jfl_p__toString(fl::Date* date, jfl::Int mode){
	jfl::Number time = jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_getTime(date);
	std::time_t timestamp = (std::time_t)(time / 1000);
	char buffer[256];
	size_t written = strftime(buffer, sizeof(buffer), "%a %b %d %T GMT%z %Y", std::localtime(&timestamp));
	return jfl::String::Make((const uint8_t*)buffer, (jfl::UInt)written);
}
jfl::Number Date::jfl_pS_get_now(){
	using namespace std::chrono;
	milliseconds ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch()
	);
	return (jfl::Number)ms.count();
}
jfl::Number Date::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_getTimezoneOffset(fl::Date* jfl_t) {
	return 0;
}
jfl::Number Date::jfl_S_parse(jfl::String jfl_a_string){
	jfl_notimpl;
}
jfl::Number Date::jfl_S_UTC(jfl::Int jfl_a_year, jfl::Int jfl_a_month, jfl::Int jfl_a_date, jfl::Int jfl_a_hour, jfl::Int jfl_a_minute, jfl::Int jfl_a_second, jfl::Int jfl_a_millisecond){
	jfl_notimpl;
}
}
#endif
