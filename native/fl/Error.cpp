#if __has_include(<fl/Error.h>)
#include <jfl/common_impl.h>
#include <fl/Error.h>
namespace fl{
jfl::String Error::getStackTrace(fl::Error* jfl_t){
	return jfl::String::Make("[stack trace]");
}
}
#endif
