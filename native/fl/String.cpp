#if __has_include(<fl/String.h>)
#include <jfl/common_impl.h>
#include <fl/String.h>
#include <fl/RegExp.h>
#include <fl/flash/utils/ByteArray.h>
using namespace jfl;
namespace fl{
jfl::Int String::jfl_g_length(jfl::String string){
	if(string.tag == jfl::type_tag::Null){
		jfl::raise_npe();
	}
	if ( string.tag <= jfl::type_tag::SmallStringEnd ){
		return (jfl::Int)string.tag;
	}else{
		return (jfl::Int)string.sb.object->length;
	}
}

jfl::Int String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_indexOf(jfl::String haystack, jfl::String needle, jfl::Number startIndex){
	if ( haystack.tag == jfl::type_tag::Null ) {
		jfl::raise_npe();
	}

	jfl::Int start;
	auto [haystack_ptr, haystack_len] = haystack.to_view();
	if ( startIndex <= 0 ){
		start = 0;
	}else if ( startIndex >= haystack_len ){
		return -1;
	}else{
		start = (jfl::Int)startIndex;
	}
	if ( needle.tag == jfl::type_tag::Null ){
		return -1;
	}
	auto[needle_ptr, needle_len] = needle.to_view();
	jfl::Int last_possible_index = (jfl::Int)(haystack_len - needle_len);
	for ( ; start <= last_possible_index; start++ ){
		bool failed = false;
		for ( jfl::UInt i = 0; i < needle_len; i++ ){
			if ( haystack_ptr[i + start] != needle_ptr[i] ){
				failed = true;
				break;
			}
		}
		if ( !failed ){
			return start;
		}
	}
	return -1;
}

jfl::Int String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_lastIndexOf(jfl::String haystack, jfl::String needle, jfl::Number startIndex){
	if (haystack.tag == jfl::type_tag::Null) {
		jfl::raise_npe();
	}

	jfl::Int start;
	auto[haystack_ptr, haystack_len] = haystack.to_view();
	if (startIndex <= 0) {
		return -1;
	} else if (startIndex > haystack_len) {
		start = haystack_len;
	} else {
		start = (jfl::Int)startIndex;
	}
	if (needle.tag == jfl::type_tag::Null) {
		return -1;
	}
	auto[needle_ptr, needle_len] = needle.to_view();
	for (jfl::Int pos = start - needle_len; pos >= 0; pos--) {
		bool failed = false;
		for (jfl::UInt i = 0; i < needle_len; i++) {
			if (haystack_ptr[i + pos] != needle_ptr[i]) {
				failed = true;
				break;
			}
		}
		if (!failed) {
			return pos;
		}
	}
	return -1;
}

jfl::String String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_charAt(jfl::String string, jfl::Number index){
	if (string.tag == jfl::type_tag::Null) {
		jfl::raise_npe();
	}
	auto [ptr, len] = string.to_view();
	if ( index >= len ){
		return jfl::StringEmpty;
	}
	if ( index <= -1 ){
		return jfl::StringEmpty;
	}
	return jfl::StringSmall{1, {ptr[(jfl::Int)index]}};
}

jfl::Number String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_charCodeAt(jfl::String string, jfl::Number index){
	jfl::String ch = jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_charAt(string, index);
	if ( (jfl::Int)ch.tag == (jfl::Int)jfl::type_tag::SmallStringBegin + 1 ){
		return ch.ss.value[0];
	}
	return jfl::NaN;
}

jfl::Int String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_localeCompare(jfl::String string1, jfl::Any string2){
	jfl_notimpl;
}

jfl::String String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_substring(jfl::String string, jfl::Number startIndex, jfl::Number endIndex){
	auto view = string.to_string_view();
	if ( startIndex < 0 ){
		startIndex = 0;
	}
	if ( endIndex < 0 ){
		endIndex = 0;
	}
	size_t start = (size_t)startIndex, end = (size_t)endIndex;
	if ( start > end ){
		std::swap(start, end);
	}
	if ( start >= view.size() ) {
		return jfl::StringEmpty;
	}
	if ( end > view.size() ){
		end = view.size();
	}
	return jfl::String::Make(view.substr(start, end - start));
}

jfl::String String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_substr(jfl::String string, jfl::Number startIndex, jfl::Number len){
	auto view = string.to_string_view();
	if ( startIndex < 0 ){
		startIndex = view.size() + startIndex;
	}
	jfl::UInt start = (jfl::UInt)startIndex;
	jfl::UInt count = (jfl::UInt)len;
	if ( start > view.size() ){
		return jfl::StringEmpty;
	}
	return jfl::String::Make(view.substr(start, count));
}

jfl::String String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_slice(jfl::String string, jfl::Number startIndex, jfl::Number endIndex){
	auto view = string.to_string_view();
	if ( startIndex >= view.size() ){
		return jfl::StringEmpty;
	}
	if ( -startIndex > view.size() ){
		startIndex = 0;
	}
	jfl::Int start = (jfl::Int)startIndex;
	if ( start < 0 ){
		start = (jfl::Int)view.size() + start;
	}
	if (endIndex >= view.size()) {
		endIndex = (jfl::Number)view.size();
	}
	if (-endIndex > view.size()) {
		return jfl::StringEmpty;
	}
	jfl::Int end = (jfl::Int)endIndex;
	if ( end <= start ){
		return jfl::StringEmpty;
	}
	return jfl::String::Make({view.data() + start, (size_t)(end - start)});
}

jfl::String String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toLowerCase(jfl::String string){
	jfl::Arena arena;
	jfl::arena_string ss(arena);
	for ( auto ch : string.to_string_view() ){
		if ( ch >= 'A' && ch <= 'Z' ){
			ss.push_back(ch - 'A' + 'a');
		}else{
			ss.push_back(ch);
		}
	}
	return jfl::String::Make(ss);
}

jfl::String String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toUpperCase(jfl::String string){
	Arena arena;
	jfl::arena_string ss(arena);
	for (auto ch : string.to_string_view()) {
		if (ch >= 'a' && ch <= 'z') {
			ss.push_back(ch - 'a' + 'A');
		} else {
			ss.push_back(ch);
		}
	}
	return jfl::String::Make(ss);
}

jfl::String String::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_nS_fromCharCode(jfl::RestParams* rest){
	Arena arena;
	jfl::arena_string ss(arena);
	size_t n = jfl::rest_size(rest);
	for ( size_t i = 0; i < n; i++ ){
		jfl::Number code = jfl::any_to_Number(jfl::rest_get(rest, (jfl::UInt)i));
		if ( code <= 127 && code >= 0 ){
			ss.push_back((char)code);
		}else{
			raise_message("invalid char code");
		}
	}
	return jfl::String::Make(ss);
}

fl::Array* String::jfl_pS__match(jfl::String haystack, jfl::Any needle){
	if (haystack.tag == jfl::type_tag::Null || haystack.tag == jfl::type_tag::Undefined) {
		return nullptr;
	}
	fl::RegExp* regexp = (fl::RegExp*)jfl::any_as_Class(needle, &fl::RegExp::jfl_Class);
	if (regexp) {
		if ( RegExp::jfl_g_global(regexp) ){
			auto view = haystack.to_string_view();
			const char* begin = &*view.begin();
			const char* end = &*view.end();
			fl::Array* arr = fl::Array::jfl_New(jfl::params({}));
			while ( true ){
				std::cmatch result;
				if ( !std::regex_search(begin, end, result, regexp->regex, std::regex_constants::match_default) ) {
					break;
				}
				jfl::String string = jfl::String::Make(std::string_view(begin + result.position(), (size_t)result.length()));
				Array::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(arr, jfl::params({string}));
				begin = begin + result.position() + result.length();
			}
			regexp->jfl_p__lastIndex = 0;
			return arr;
		}else{
			jfl::Any match = RegExp::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_exec(regexp, haystack);
			if ( match.tag != type_tag::Undefined && match.tag != type_tag::Null ){
				return (fl::Array*)match.object;
			}
			return nullptr;
		}
	} else {
		if ( jfl::str_eq(haystack, jfl::any_to_String(needle)) ){
			return fl::Array::jfl_New(jfl::params({haystack}));
		}else{
			return nullptr;
		}
	}
}

jfl::String String::jfl_pS__replace(jfl::String string, jfl::Any pattern, jfl::Any repl){
	using fl::flash::utils::ByteArray;

	fl::RegExp* re = (fl::RegExp*)jfl::any_as_Class(pattern, &fl::RegExp::jfl_Class);
	if ( re ) {
		auto view = string.to_string_view();
		Arena arena;
		arena_string output(arena);
		const char* begin = &*view.begin();
		const char* end = &*view.end();

		while ( begin != end ){
			std::cmatch result;
			if ( std::regex_search(begin, end, result, re->regex, std::regex_constants::match_default) ){
				const char* prefix_end = begin + result.position();
				if ( prefix_end != begin ){
					output += std::string_view(begin, prefix_end - begin);
				}
				if ((uint8_t)repl.tag & (uint8_t)type_tag::FunctionBit ){
					const size_t MaxStaticParams = 16;
					jfl::Any params[MaxStaticParams];
					size_t index = 0;
					for ( auto& subgroup : result ){
						params[index] = jfl::String::Make(
							(const uint8_t*)subgroup.first,
							(jfl::UInt)(subgroup.second - subgroup.first)
						);
						index++;
					}
					params[index] = (jfl::UInt)result.position();
					params[index + 1] = string;
					RestParams rest(params, (jfl::UInt)index + 2);
					jfl::String replacement = jfl::any_to_String(((jfl::Function*)&repl)->invoke(&rest));
					if ( replacement.tag != type_tag::Null ){
						output += replacement.to_string_view();
					}
				}else{
					if ( repl.tag <= type_tag::StringEnd){
						output += ((jfl::String*)&repl)->to_string_view();
					}
				}
				begin = prefix_end + result.length();
			}else{
				break;
			}
		}
		if ( begin != end ){
			output += std::string_view(begin, end - begin);
		}
		return jfl::String::Make(output);
	} else {
		jfl::String needle = jfl::any_to_String(pattern);
		jfl::Int index = jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_indexOf(string, needle, 0);
		if ( index < 0 ){
			return string;
		}
		Arena arena;
		jfl::arena_string ss(arena);
		auto view = string.to_string_view();
		ss.append(view.substr(0, index));
		ss.append(jfl::any_to_String(repl).to_string_view());
		ss.append(view.substr(index + needle.to_string_view().size()));
		return jfl::String::Make(ss);
	}
}

jfl::Int String::jfl_pS__search(jfl::String string, jfl::Any needle){
	fl::RegExp* regexp = (fl::RegExp*)jfl::any_as_Class(needle, &fl::RegExp::jfl_Class);
	if ( regexp ){
		auto view = string.to_string_view();
		const char* begin = &*view.begin();
		const char* end = &*view.end();
		std::cmatch result;
		if (std::regex_search(begin, end, result, regexp->regex, std::regex_constants::match_default)) {
			return (jfl::Int)result.position();
		}
		return -1;
	}else{
		return jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_indexOf(
			string,
			jfl::any_to_String(needle),
			0
		);
	}
}

fl::Array* String::jfl_pS__split(jfl::String string, jfl::Any pattern, jfl::UInt limit){
	if ( pattern.tag == jfl::type_tag::Undefined ) {
		return fl::Array::jfl_New(jfl::params({string}));
	}
	fl::RegExp* regexp = (fl::RegExp*)jfl::any_as_Class(pattern, &fl::RegExp::jfl_Class);
	if (regexp) {
		auto view = string.to_string_view();
		const char* begin = &*view.begin();
		const char* end = &*view.end();
		fl::Array* array = fl::Array::jfl_New(jfl::params({}));
		while ( begin != end ) {
			if (limit <= 0) {
				break;
			}
			std::cmatch result;
			if ( !std::regex_search(begin, end, result, regexp->regex, std::regex_constants::match_default) ) {
				break;
			}
			const char* prefix_end = begin + result.position();
			fl::Array::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(
				array,
				jfl::params({ jfl::String::Make({begin, (size_t)(prefix_end - begin)}) })
			);
			begin = prefix_end + result.length();
			limit--;
		}
		if (begin != end) {
			fl::Array::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(
				array,
				jfl::params({ jfl::String::Make({begin, (size_t)(end - begin)}) })
			);
		}
		return array;
	} else {
		jfl::String needle = jfl::any_to_String(pattern);
		if (pattern.tag == jfl::type_tag::Null || pattern.tag == jfl::type_tag::StringBegin) {
			// ???
			jfl_notimpl;
		}
		auto [ptr, len] = needle.to_view();
		auto[string_ptr, string_len] = string.to_view();
		fl::Array* array = fl::Array::jfl_New(jfl::params({}));
		jfl::Int begin = 0;
		while ((jfl::UInt)begin < string_len){
			if ( limit <= 0 ){
				break;
			}
			jfl::Int index = jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_indexOf(string, needle, begin);
			if ( index < 0 ){
				break;
			}
			fl::Array::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(
				array,
				jfl::params({jfl::String::Make(string_ptr + begin, index - begin)})
			);
			begin = index + (jfl::Int)len;
			limit--;
		}
		if ( (jfl::UInt)begin < string_len ){
			fl::Array::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(
				array,
				jfl::params({ jfl::String::Make(string_ptr + begin, (jfl::Int)string_len - begin) })
			);
		}
		return array;
	}
}

}
#endif
