#if __has_include(<fl/avmplus/getQualifiedClassName.h>)
#include <jfl/common_impl.h>
#include <fl/avmplus/getQualifiedClassName.h>
#include <fl/flash/utils/getQualifiedClassName.h>
namespace fl::avmplus{
using namespace jfl;

jfl::String getQualifiedClassName(jfl::Any value){
	return fl::flash::utils::getQualifiedClassName(value);
}
}
#endif
