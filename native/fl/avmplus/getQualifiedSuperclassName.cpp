#if __has_include(<fl/avmplus/getQualifiedSuperclassName.h>)
#include <jfl/common_impl.h>
#include <fl/avmplus/getQualifiedSuperclassName.h>
#include <fl/flash/utils/getQualifiedSuperclassName.h>
namespace fl::avmplus{
using namespace jfl;
	
jfl::String getQualifiedSuperclassName(jfl::Any value){
	return fl::flash::utils::getQualifiedSuperclassName(value);
}
}
#endif
