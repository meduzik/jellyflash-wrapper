#if __has_include(<fl/trace.h>)
#include <jfl/common_impl.h>
#include <fl/trace.h>

#include <cstdio>

namespace fl{
void trace(jfl::RestParams* jfl_a_rest){
	for ( jfl::UInt i = 0; i < jfl::rest_size(jfl_a_rest); i++ ){
		jfl::Any val = jfl::rest_get(jfl_a_rest, i);
		jfl::String str = jfl::any_to_String(val);
		auto view = str.to_view();
		if ( i != 0 ){
			putc(' ', stdout);
		}
		printf("%.*s", (int)view.second, view.first);
	}
	putc('\n', stdout);
}
}
#endif
