#if __has_include(<fl/isNaN.h>)
#include <jfl/common_impl.h>
#include <fl/isNaN.h>
namespace fl{
jfl::Boolean isNaN(jfl::Number x){
	return std::isnan(x);
}
}
#endif
