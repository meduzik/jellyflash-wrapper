#if __has_include(<fl/jellyflash/display3D/TextureExtensions.decl.h>)
#include <jfl/common_impl.h>
#include <fl/jellyflash/display3D/TextureExtensions.decl.h>
#include <fl/flash/display3D/textures/RectangleTexture.decl.h>
#include <fl/flash/display3D/textures/Texture.decl.h>
#include <fl/jellyflash/display3D/ExternalImage.decl.h>
#include <jfl/render/Context3D.h>

namespace fl::jellyflash::display3D {
void TextureExtensions::jfl_S_UploadTextureFromExternalImage(fl::flash::display3D::textures::Texture* jfl_a_texture, fl::jellyflash::display3D::ExternalImage* jfl_a_image, jfl::Int jfl_a_mipmap) {
	dynamic_cast<jfl::render::Texture2D*>(jfl_a_texture->impl)->uploadExternalImage(
		jfl_a_mipmap,
		jfl_a_image->jfl_p__width,
		jfl_a_image->jfl_p__height,
		jfl_a_image->jfl_p_handle
	);
}
void TextureExtensions::jfl_S_UploadRectangleTextureFromExternalImage(fl::flash::display3D::textures::RectangleTexture* jfl_a_texture, fl::jellyflash::display3D::ExternalImage* jfl_a_image, jfl::Int jfl_a_mipmap) {
	dynamic_cast<jfl::render::Texture2D*>(jfl_a_texture->impl)->uploadExternalImage(
		jfl_a_mipmap,
		jfl_a_image->jfl_p__width,
		jfl_a_image->jfl_p__height,
		jfl_a_image->jfl_p_handle
	);
}
}
#endif
