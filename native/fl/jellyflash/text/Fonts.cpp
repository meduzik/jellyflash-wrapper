#if __has_include(<fl/jellyflash/text/Fonts.h>)
#include <jfl/common_impl.h>
#include <fl/jellyflash/text/Fonts.h>
#include <fl/flash/utils/ByteArray.h>
#include <jfl/text/TextRendering.h>
namespace fl::jellyflash::text{
void Fonts::jfl_S_Embed(jfl::String name, fl::flash::utils::ByteArray* data){
	jfl::FontParse(name.to_string_view(), data->data.data(), data->data.size());
}
}
#endif
