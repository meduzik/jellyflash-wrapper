#if __has_include(<fl/jellyflash/utils/ClassUtils.h>)
#include <jfl/common_impl.h>
#include <fl/jellyflash/utils/ClassUtils.h>
namespace fl::jellyflash::utils{
const jfl::Class* ClassUtils::jfl_S_GetSuperclassOf(jfl::Any object){
	if (jfl::any_is_Class(object, &jfl::Class_Class)) {
		return jfl::super_of((const jfl::Class*)object.object);
	}
	return jfl::super_of(jfl::any_class_of(object));
}
}
#endif
