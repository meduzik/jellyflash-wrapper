#if __has_include(<fl/jellyflash/system/GC.h>)
#include <jfl/common_impl.h>
#include <fl/jellyflash/system/GC.h>
#include <jfl/app/Loop.h>
namespace fl::jellyflash::system{

void GC::hintCollection(fl::jellyflash::system::GC* self, jfl::Number threshold){
	jfl::gc_hint(threshold);
}

void GC::forceCollection(fl::jellyflash::system::GC* self){
	jfl::gc_perform();
}

}
#endif
