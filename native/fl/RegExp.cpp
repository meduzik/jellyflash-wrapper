#if __has_include(<fl/RegExp.h>)
#include <jfl/common_impl.h>
#include <fl/RegExp.h>
#include <iostream>
namespace fl{ 
using namespace jfl;
void RegExp::jfl_p_init(fl::RegExp* re, jfl::String pattern, jfl::String options){
	if ( pattern.tag == type_tag::Null ){
		raise_nullarg();
	}
	std::regex::flag_type flags = std::regex::ECMAScript | std::regex::optimize;
	if ( options.tag != type_tag::Null ){
		for ( auto ch : options.to_string_view() ){
			switch ( ch ){
			case 'i':{
				flags |= std::regex::icase;
				re->jfl_p__flags |= jfl_pS_FlagIgnoreCase;
			}break;
			case 'g':{
				re->jfl_p__flags |= jfl_pS_FlagGlobal;
			}break;
			default:{
				jfl_notimpl;
			}break;
			}
		}
	}
	auto source = pattern.to_string_view();
	re->regex.assign(source.begin(), source.end(), flags);
}
jfl::Any RegExp::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_exec(fl::RegExp* re, jfl::String string){
	auto view = string.to_string_view();
	const char* begin = &*view.begin();
	const char* end = &*view.end();
	jfl::UInt offset = 0;
	jfl::String index_key(jfl::StringSmall{5, "index"});
	jfl::String input_key(jfl::StringSmall{5, "input"});
	jfl::Any input_val(string);

	if ( jfl_g_global(re) ){
		offset = re->jfl_p__lastIndex;
	}
	std::cmatch result;
	if (offset >= view.size() && (offset > 0 || view.size() > 0)) {
		return nullptr;
	}
	if (std::regex_search(begin + offset, end, result, re->regex, std::regex_constants::match_default)) {
		Array* arr = Array::jfl_New(jfl::params({}));
		jfl::Any index_val((jfl::UInt)result.position() + offset);
		arr->jfl_dynamic.index(*(jfl::Any*)&index_key, &index_val, jfl::dynamic_op::set);
		arr->jfl_dynamic.index(*(jfl::Any*)&input_key, &input_val, jfl::dynamic_op::set);
		for (auto& subgroup : result) {
			Array::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(
				arr,
				jfl::params({
					jfl::String::Make(
						(const uint8_t*)subgroup.first,
						(jfl::UInt)(subgroup.second - subgroup.first)
					)
				})
			);
		}
		if (jfl_g_global(re)) {
			re->jfl_p__lastIndex = (jfl::UInt)(result.position() + result.length()) + offset;
		}
		
		return (jfl::Object*)arr;
	}else{
		return nullptr;
	}
}

jfl::Boolean RegExp::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_test(fl::RegExp* jfl_t, jfl::String jfl_a_string){
	jfl_notimpl;
}
}
#endif
