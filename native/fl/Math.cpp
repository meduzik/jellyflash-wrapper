#if __has_include(<fl/Math.h>)
#include <jfl/common_impl.h>
#include <fl/Math.h>
namespace fl{
jfl::Number Math::jfl_pS__min(jfl::Number x, jfl::Number y){
	return std::min(x, y);
}
jfl::Number Math::jfl_pS__max(jfl::Number x, jfl::Number y){
	return std::max(x, y);
}
jfl::Number Math::jfl_S_abs(jfl::Number x){
	return std::abs(x);
}
jfl::Number Math::jfl_S_acos(jfl::Number x){
	return std::acos(x);
}
jfl::Number Math::jfl_S_asin(jfl::Number x){
	return std::asin(x);
}
jfl::Number Math::jfl_S_atan(jfl::Number x){
	return std::atan(x);
}
jfl::Number Math::jfl_S_ceil(jfl::Number x){
	return std::ceil(x);
}
jfl::Number Math::jfl_S_cos(jfl::Number x){
	return std::cos(x);
}
jfl::Number Math::jfl_S_exp(jfl::Number x){
	return std::exp(x);
}
jfl::Number Math::jfl_S_floor(jfl::Number x){
	return std::floor(x);
}
jfl::Number Math::jfl_S_log(jfl::Number x){
	return std::log(x);
}
jfl::Number Math::jfl_S_round(jfl::Number x){
	return std::round(x);
}
jfl::Number Math::jfl_S_sin(jfl::Number x){
	return std::sin(x);
}
jfl::Number Math::jfl_S_sqrt(jfl::Number x){
	return std::sqrt(x);
}
jfl::Number Math::jfl_S_tan(jfl::Number x){
	return std::tan(x);
}
jfl::Number Math::jfl_S_atan2(jfl::Number y, jfl::Number x){
	return std::atan2(y, x);
}
jfl::Number Math::jfl_S_pow(jfl::Number x, jfl::Number y){
	return std::pow(x, y);
}
jfl::Number Math::jfl_S_max(jfl::Number x, jfl::Number y, jfl::RestParams* rest){
	x = std::max(x, y);
	size_t n = jfl::rest_size(rest);
	for ( size_t i = 0; i < n; i++ ){
		x = std::max(x, jfl::any_to_Number(jfl::rest_get(rest, (jfl::UInt)i)));
	}
	return x;
}
jfl::Number Math::jfl_S_min(jfl::Number x, jfl::Number y, jfl::RestParams* rest){
	x = std::min(x, y);
	size_t n = jfl::rest_size(rest);
	for (size_t i = 0; i < n; i++) {
		x = std::min(x, jfl::any_to_Number(jfl::rest_get(rest, (jfl::UInt)i)));
	}
	return x;
}

static std::seed_seq seed{
	(unsigned)clock(),
	(unsigned)std::chrono::high_resolution_clock::now().time_since_epoch().count()
};
static std::default_random_engine engine(seed);

jfl::Number Math::jfl_S_random(){
	std::uniform_real_distribution<double> unif(0.0, 1.0);
	return unif(engine);
}
}
#endif
