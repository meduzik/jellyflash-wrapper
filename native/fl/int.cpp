#if __has_include(<fl/int.h>)
#include <jfl/common_impl.h>
#include <fl/int.h>
namespace fl{
jfl::String jfl_k_int::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toString(jfl::Int value, jfl::Any radix){
	jfl::Int radix_value = 10;
	if ( radix.tag == jfl::type_tag::Undefined ){
	}else if ( !jfl::any_is_int(radix) ){
		radix_value = jfl::any_to_int(radix);
	}
	if ( radix_value != 10 ){
		jfl_notimpl;
	}
	return jfl::int_to_String(value);
}
jfl::Int jfl_k_int::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_valueOf(jfl::Int jfl_t){
	jfl_notimpl;
}
jfl::String jfl_k_int::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toExponential(jfl::Int jfl_t, jfl::Any jfl_a_p){
	jfl_notimpl;
}
jfl::String jfl_k_int::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toPrecision(jfl::Int jfl_t, jfl::Any jfl_a_p){
	jfl_notimpl;
}
jfl::String jfl_k_int::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_toFixed(jfl::Int jfl_t, jfl::Any jfl_a_p){
	jfl_notimpl;
}
}
#endif
