#if __has_include(<fl/parseInt.h>)
#include <jfl/common_impl.h>
#include <fl/parseInt.h>

namespace jfl{
Number parseInt(String str, Int radix);
}

namespace fl{

jfl::Number parseInt(jfl::String str, jfl::Int radix){
	return jfl::parseInt(str, radix);
}

}
#endif
