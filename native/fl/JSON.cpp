#if __has_include(<fl/JSON.h>)
#include <jfl/common_impl.h>
#include <fl/JSON.h>

using namespace jfl;

namespace {

const char hex_digits[] = "0123456789abcdef";

struct JSONPrinter {
	JSONPrinter(Arena& arena, arena_string& buffer, std::string_view space) :
		ss(buffer),
		space(space) {
	}

	void encode_string(std::string_view sv) {
		ss += '"';
		for (auto ch : sv) {
			if (ch < 0 || ch > 127) {
				ss += ch;
			} else if (ch < 32 || ch == 127) {
				switch (ch) {
				case '\n': {
					ss += "\\n";
				}break;
				case '\r': {
					ss += "\\r";
				}break;
				case '\t': {
					ss += "\\t";
				}break;
				default: {
					ss += "\\u00";
					ss += hex_digits[ch / 16];
					ss += hex_digits[ch % 16];
				}break;
				}
			} else {
				switch (ch) {
				case '"': {
					ss += "\\\"";
				}break;
				case '\\': {
					ss += "\\\\";
				}break;
				default: {
					ss += ch;
				}break;
				}
			}
		}
		ss += '"';
	}

	void print(Any object) {
		begin_value();
		switch (object.tag) {
		case type_tag::Null:
		case type_tag::Undefined: {
			write("null");
		}break;
		case type_tag::Boolean: {
			if (object.bval) {
				write("true");
			} else {
				write("false");
			}
		}break;
		case type_tag::Int: {
			write(std::to_string(object.ival));
		}break;
		case type_tag::UInt: {
			write(std::to_string(object.uval));
		}break;
		case type_tag::Float: {
			write(std::to_string(object.fval));
		}break;
		case type_tag::NumberBox: {
			write(std::to_string(object.nval->value));
		}break;
		case type_tag::Object: {
			auto ptr = object.object;
			if (ptr->jfl_vtable == (const VTable*)&fl::jfl_V_Array) {
				bool first = true;
				write("[");
				indent++;
				fl::Array* array = (fl::Array*)ptr;
				for (UInt i = 0; i < fl::Array::jfl_g_length(array); i++) {
					if (i != 0) {
						write(",");
					}
					first = false;
					print(fl::Array::jfl_get(array, i));
				}
				indent--;
				if (!first) {
					newline();
				}
				write("]");
			} else if (ptr->jfl_vtable == (const VTable*)&fl::jfl_V_Object) {
				Iterator iter;
				ptr->jfl_vtable->jfl_vth.iterate(ptr, &iter);
				Any key, value;
				bool first = true;
				write("{");
				indent++;
				while (iter.next(&iter, &key, &value)) {
					jfl::String kstr = any_to_String(key);
					if (kstr.tag > type_tag::StringEnd) {
						continue;
					}
					if (first) {
						first = false;
					} else {
						write(",");
					}
					encode_string(kstr.to_string_view());
					write(":");
					print(value);
				}
				indent--;
				if (!first) {
					newline();
				}
				write("}");
			} else {
				encode_string(jfl::class_of(ptr)->name.to_string_view());
			}
		}break;
		case type_tag::Rest: {
			bool first = true;
			write("[");
			indent++;
			UInt n = jfl::rest_size(object.rest);
			for (UInt i = 0; i < n; i++) {
				if (i != 0) {
					write(",");
				}
				first = false;
				print(jfl::rest_get(object.rest, i));
			}
			indent--;
			if (!first) {
				newline();
			}
			write("]");
		}break;
		default: {
			if (object.tag <= type_tag::StringEnd) {
				encode_string(((jfl::String*)&object)->to_string_view());
			} else if (object.tag >= type_tag::FunctionBit) {
				write("\"[Function]\"");
			} else {
				jfl_unreachable;
			}
		}break;
		}
		end_value();
	}

	void newline() {
		if (space.size() > 0) {
			write("\n");
			for (size_t i = 0; i < indent; i++) {
				write(space);
			}
		}
	}

	void write(std::string_view str) {
		ss += str;
	}

	void begin_value() {
		newline();
	}

	void end_value() {
	}

	jfl::String get() {
		return jfl::String::Make((const uint8_t*)ss.data(), (UInt)ss.size());
	}

	size_t indent = 0;
	arena_string& ss;
	std::string_view space;
};

}

struct JSONParser {
	JSONParser(jfl::Arena& arena, std::string_view ss) :
		tmp_string(arena),
		ptr((const uint8_t*)ss.data()),
		end((const uint8_t*)ss.data() + ss.size()) {
	}

	jfl::Any parse() {
		jfl::Any value = parse_value();
		skip_ws();
		if (ptr != end) {
			jfl::raise_message("extra characters after json stream");
		}
		return value;
	}

	void skip_ws() {
		while (true) {
			int ch = peek();
			if (ch == ' ' || ch == '\n' || ch == '\t' || ch == '\r') {
				advance();
			} else {
				break;
			}
		}
	}

	jfl::Any parse_value() {
		skip_ws();
		int ch = peek();
		if (ch == '[') {
			advance();
			return parse_array();
		} else if (ch == '{') {
			advance();
			return parse_object();
		} else if (ch == '"') {
			advance();
			return parse_string();
		} else if (ch == 't') {
			advance();
			return parse_true();
		} else if (ch == 'f') {
			advance();
			return parse_false();
		} else if (ch == 'n') {
			advance();
			return parse_null();
		} else if (isdigit(ch) || ch == '-') {
			return parse_number();
		} else {
			fail("unexpected character");
			return nullptr;
		}
	}

	jfl::Any parse_array() {
		skip_ws();
		fl::Array* array = fl::Array::jfl_New(params({}));
		if (peek() != ']') {
			while (true) {
				jfl::Any value = parse_value();
				fl::Array::push(array, value);
				skip_ws();
				if (peek() == ',') {
					advance();
					skip_ws();
				} else {
					break;
				}
			}
		}
		skip_ws();
		consume(']');
		return (jfl::Object*)array;
	}

	jfl::Any parse_object() {
		skip_ws();
		fl::Object* object = fl::Object::jfl_New();
		if (peek() != '}') {
			while (true) {
				consume('"');
				jfl::Any key = parse_string();
				skip_ws();
				consume(':');
				skip_ws();
				jfl::Any value = parse_value();
				object->jfl_dynamic.index(key, &value, dynamic_op::set);
				skip_ws();
				if (peek() == ',') {
					advance();
					skip_ws();
				} else {
					break;
				}
			}
		}
		skip_ws();
		consume('}');
		return (jfl::Object*)object;
	}

	uint32_t parse_hex() {
		auto ch = peek();
		if (ch >= '0' && ch <= '9') {
			advance();
			return ch - '0';
		} else if (ch >= 'a' && ch <= 'f') {
			advance();
			return ch - 'a' + 10;
		} else if (ch >= 'A' && ch <= 'F') {
			advance();
			return ch - 'A' + 10;
		} else {
			fail("invalid escape sequence");
			return 0;
		}
	}

	void parse_escape(jfl::arena_string& ss) {
		auto ch = peek();
		switch (ch) {
		case '"': {
			advance();
			ss.push_back('"');
		}break;
		case '\\': {
			advance();
			ss.push_back('\\');
		}break;
		case '/': {
			advance();
			ss.push_back('/');
		}break;
		case 'b': {
			advance();
			ss.push_back('\b');
		}break;
		case 'f': {
			advance();
			ss.push_back('\f');
		}break;
		case 'n': {
			advance();
			ss.push_back('\n');
		}break;
		case 'r': {
			advance();
			ss.push_back('\r');
		}break;
		case 't': {
			advance();
			ss.push_back('\t');
		}break;
		case 'u': {
			advance();
			uint32_t a = parse_hex();
			uint32_t b = parse_hex();
			uint32_t c = parse_hex();
			uint32_t d = parse_hex();
			uint32_t codepoint = (a << 24) | (b << 16) | (c << 8) | d;
			jfl_notimpl;
		}break;
		default: {
			fail("invalid escape sequence");
		}break;
		}
	}

	jfl::Any parse_string() {
		tmp_string.clear();
		while (true) {
			auto ch = peek();
			if (ch == '"') {
				advance();
				break;
			} else if (ch == '\\') {
				advance();
				parse_escape(tmp_string);
			} else if (ch < 32) {
				fail("invalid contents in string literal");
			} else {
				advance();
				tmp_string.push_back(ch);
			}
		}
		return jfl::String::Make(tmp_string);
	}

	jfl::Any parse_number() {
		const char* begin = (const char*)ptr;
		if (peek() == '-') {
			advance();
		}
		parse_int();
		if (peek() == '.') {
			advance();
			parse_digit();
			parse_num();
		}
		if (peek() == 'e' || peek() == 'E') {
			advance();
			if (peek() == '-' || peek() == '+') {
				advance();
			}
			parse_digit();
			parse_num();
		}
		const char* end = (const char*)ptr;
		double value;
		bool err;
		value = parseNumber(std::string_view(begin, end - begin), &err);
		if (err) {
			fail("bad number");
		}
		return value;
	}

	void parse_int() {
		if (peek() == '0') {
			advance();
		} else {
			parse_digit();
			parse_num();
		}
	}

	void parse_digit() {
		if (isdigit(peek())) {
			advance();
		} else {
			fail("expected digit");
		}
	}

	void parse_num() {
		while (isdigit(peek())) {
			advance();
		}
	}

	jfl::Any parse_true() {
		consume('r');
		consume('u');
		consume('e');
		return true;
	}

	jfl::Any parse_false() {
		consume('a');
		consume('l');
		consume('s');
		consume('e');
		return false;
	}

	jfl::Any parse_null() {
		consume('u');
		consume('l');
		consume('l');
		return nullptr;
	}
private:
	void consume(char ch) {
		if (peek() != ch) {
			fail("unexpected character");
		}
		advance();
	}

	void fail(const char* msg) {
		raise_message(msg);
	}

	int peek() {
		if (ptr == end) {
			return -1;
		}
		return *ptr;
	}

	void advance() {
		ptr++;
	}

	jfl::arena_string tmp_string;

	const uint8_t* ptr;
	const uint8_t* end;
};

namespace fl{


jfl::Any JSON::jfl_pS_parseCore(jfl::String string){
	Arena arena;
	JSONParser parser(arena, string.to_string_view());
	return parser.parse();
}

jfl::String JSON::jfl_pS_stringifyCore(jfl::Any object, jfl::String space){
	Arena arena;
	arena_string ss(arena);
	JSONPrinter printer(arena, ss, space.to_string_view());
	printer.print(object);
	return printer.get();
}

}

namespace jfl{

Any JSONDecode(std::string_view data) {
	Arena arena;
	JSONParser parser(arena, data);
	return parser.parse();
}

void JSONEncode(Any value, arena_string& data) {
	Arena arena;
	JSONPrinter printer(arena, data, "");
	printer.print(value);
}

}
#endif
