#include <jfl/common_impl.h>
#include <fl/parseFloat.h>
#include <jfl/number.h>

namespace fl{
jfl::Number parseFloat(jfl::String s){
	bool err = false;
	jfl::Number val = jfl::parseNumber(s.to_string_view(), &err);
	return val;
}
}
