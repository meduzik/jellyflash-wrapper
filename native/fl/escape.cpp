#if __has_include(<fl/escape.h>)
#include <jfl/common_impl.h>
#include <fl/escape.h>
namespace fl{

static const uint8_t hex[] = "0123456789ABCDEF";

jfl::String escape(jfl::String string){
	jfl::Arena arena;
	jfl::arena_string out(arena);
	for ( auto ch : string.to_string_view() ){
		if ( 
			(ch >= '0' && ch <= '9')
			||
			(ch >= 'a' && ch <= 'z')
			||
			(ch >= 'A' && ch <= 'Z')
			||
			ch == '@'
			||
			ch == '-'
			||
			ch == '_'
			||
			ch == '.'
			||
			ch == '*'
			||
			ch == '+'
			||
			ch == '/'
		){
			out.push_back(ch);
		}else{
			out.push_back('%');
			out.push_back(hex[ch / 16]);
			out.push_back(hex[ch % 16]);
		}
	}
	return jfl::String::Make(out);
}
}
#endif
