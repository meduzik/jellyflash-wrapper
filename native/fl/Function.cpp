#if __has_include(<fl/Function.h>)
#include <jfl/common_impl.h>
#include <fl/Function.h>
namespace fl{
jfl::Int Function::jfl_g_length(jfl::Function jfl_t){
	jfl_notimpl;
}
jfl::Any Function::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_call(jfl::Function func, jfl::Any jfl_a_param1, jfl::RestParams* jfl_a_rest){
	jfl_notimpl;
}
jfl::Any Function::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_apply(jfl::Function func, jfl::Any thisobj, jfl::Any args){
	if ( thisobj.tag != jfl::type_tag::Null ){
		jfl::raise_message("invoking Function with non-null this is not supported");
	}
	jfl::RestParams rest;
	rest.tag = jfl::type_tag::Object;
	rest.object = jfl::any_to_Class(args, &fl::Array::jfl_Class);
	return func.invoke(&rest);
}
}
#endif
