#if __has_include(<fl/avm2/intrinsics/memory/si32.h>)
#include <jfl/common_impl.h>
#include <fl/avm2/intrinsics/memory/si32.h>
#include <fl/flash/utils/ByteArray.h>
#include <fl/flash/system/ApplicationDomain.h>
namespace fl::avm2::intrinsics::memory {

using fl::flash::system::ApplicationDomain;
using fl::flash::utils::ByteArray;

void si16(jfl::Int val, jfl::Int addr) {
	ByteArray* bytes = ApplicationDomain::jfl_pS__domainMemory;
	ByteArray::check_size(bytes, addr + 2);
	uint16_t src = (uint16_t)val;
	memcpy(bytes->data.data() + addr, &src, sizeof(src));
}
}
#endif
