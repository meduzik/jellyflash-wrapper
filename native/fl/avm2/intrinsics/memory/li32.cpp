#if __has_include(<fl/avm2/intrinsics/memory/li32.h>)
#include <jfl/common_impl.h>
#include <fl/avm2/intrinsics/memory/li32.h>
#include <fl/flash/utils/ByteArray.h>
#include <fl/flash/system/ApplicationDomain.h>
namespace fl::avm2::intrinsics::memory{

using fl::flash::system::ApplicationDomain;
using fl::flash::utils::ByteArray;

jfl::Int li32(jfl::Int addr){
	ByteArray* bytes = ApplicationDomain::jfl_pS__domainMemory;
	return *(int32_t*)(bytes->data.data() + addr);
}
}
#endif
