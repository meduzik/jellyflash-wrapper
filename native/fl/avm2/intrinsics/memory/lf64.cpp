#if __has_include(<fl/avm2/intrinsics/memory/li32.h>)
#include <jfl/common_impl.h>
#include <fl/avm2/intrinsics/memory/li32.h>
#include <fl/flash/utils/ByteArray.h>
#include <fl/flash/system/ApplicationDomain.h>
namespace fl::avm2::intrinsics::memory{

using fl::flash::system::ApplicationDomain;
using fl::flash::utils::ByteArray;

jfl::Number lf64(jfl::Int addr){
	ByteArray* bytes = ApplicationDomain::jfl_pS__domainMemory;
	double out;
	memcpy(&out, bytes->data.data() + addr, sizeof(double));
	return out;
}
}
#endif
