#if __has_include(<fl/avm2/intrinsics/memory/sf32.h>)
#include <jfl/common_impl.h>
#include <fl/avm2/intrinsics/memory/sf32.h>
#include <fl/flash/utils/ByteArray.h>
#include <fl/flash/system/ApplicationDomain.h>

namespace fl::avm2::intrinsics::memory{
using fl::flash::system::ApplicationDomain;
using fl::flash::utils::ByteArray;

void sf64(jfl::Number val, jfl::Int addr){
	ByteArray* bytes = ApplicationDomain::jfl_pS__domainMemory;
	double src = (double)val;
	memcpy(bytes->data.data() + addr, &src, sizeof(src));
}
}
#endif
