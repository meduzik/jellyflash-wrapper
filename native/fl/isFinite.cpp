#if __has_include(<fl/isFinite.h>)
#include <jfl/common_impl.h>
#include <fl/isFinite.h>
namespace fl{
jfl::Boolean isFinite(jfl::Number x){
	return std::isfinite(x);
}
}
#endif
