#if __has_include(<fl/Array.h>)
#include <jfl/common_impl.h>
#include <fl/Array.h>
#include <fl/parseInt.h>
namespace fl{
Array::~Array(){
	if (contents.is_contiguous){
		std::destroy_at(&contents.vec);
	}else{
		std::destroy_at(&contents.map);
	}
}
void Array::jfl_GC_user(Array* self) {
	if (self->contents.is_contiguous) {
		for(auto& elt: self->contents.vec){
			jfl::gc_visit_any(elt);
		}
	}else{
		for (auto it : self->contents.map) {
			jfl::gc_visit_any(it.second);
		}
	}
}
jfl::Boolean Array::jfl_Dynamic(jfl::Object* jfl_this, jfl::Any* key, jfl::Any* value, jfl::dynamic_op op) {
	Array* arr = (Array*)jfl_this;
	switch ( op ){
	case jfl::dynamic_op::get:{
		if ( jfl::any_is_uint(*key) ){
			jfl::UInt idx = jfl::any_to_uint(*key);
			*value = jfl_get(arr, idx);
			return true;
		}else{
			jfl::String string = jfl::any_as_String(*key);
			if ( jfl::str_eq(string, jfl::StringSmall{6, "length"}) ){
				*value = fl::Array::jfl_g_length(arr);
				return true;
			}else{
				return arr->jfl_dynamic.index(*key, value, op);
			}
		}
	}break;
	case jfl::dynamic_op::del: {
		if (jfl::any_is_uint(*key)) {
			jfl::UInt idx = jfl::any_to_uint(*key);
			jfl_del(arr, idx);
			return true;
		}else{
			jfl::String string = jfl::any_as_String(*key);
			jfl::Number num = fl::parseInt(string, 10);
			if (jfl::Number_is_uint(num)) {
				jfl_del(arr, jfl::Number_to_uint(num));
				return true;
			} else {
				return arr->jfl_dynamic.index(*key, value, op);
			}
		}
	}break;
	case jfl::dynamic_op::set: {
		if (jfl::any_is_uint(*key)) {
			jfl::UInt idx = jfl::any_to_uint(*key);
			jfl_set(arr, idx, *value);
			return true;
		} else {
			jfl::String string = jfl::any_as_String(*key);
			if (jfl::str_eq(string, jfl::StringSmall{6, "length"})) {
				fl::Array::jfl_s_length(arr, jfl::any_to_uint(*value));
				return true;
			} else {
				jfl::Number num = fl::parseInt(string, 10);
				if ( jfl::Number_is_uint(num) ){
					jfl_set(arr, jfl::Number_to_uint(num), *value);
					return true;
				}else{
					return arr->jfl_dynamic.index(*key, value, op);
				}
			}
		}
	}break;
	case jfl::dynamic_op::in: {
		if (jfl::any_is_uint(*key)) {
			jfl::UInt idx = jfl::any_to_uint(*key);
			*value = jfl_get(arr, idx);
			return value->tag != jfl::type_tag::Undefined;
		} else {
			jfl::String string = jfl::any_as_String(*key);
			if (jfl::str_eq(string, jfl::StringSmall{6, "length"})) {
				return true;
			} else {
				return arr->jfl_dynamic.index(*key, value, op);
			}
		}
	}break;
	default:{
		jfl_notimpl;
	}break;
	}

}

void Array::push(Array* array, jfl::Any value){
	jfl_set(array, array->contents.len, value);
}

void Array::jfl_set(Array* array, jfl::UInt index, jfl::Any value) {
	if (array->contents.is_contiguous) {
		if (index < array->contents.vec.size()) {
			array->contents.vec[index] = value;
		} else if ( index == array->contents.vec.size()){
			array->contents.vec.push_back(value);
		} else {
			// make non-contiguous
			array->contents.is_contiguous = false;
			jfl::um_vec<jfl::Any> old_data = std::move(array->contents.vec);
			std::destroy_at(&array->contents.vec);
			new(&array->contents.map)jfl::um_map<jfl::UInt, jfl::Any>();
			for ( size_t i = 0; i < old_data.size(); i++ ){
				array->contents.map[(jfl::UInt)i] = old_data[i];
			}
		}
	}
	if (!array->contents.is_contiguous) {
		array->contents.map[index] = value;
	}
	if ( index >= array->contents.len ){
		array->contents.len = index + 1;
	}
}
void Array::jfl_del(Array* array, jfl::UInt index) {
	if (array->contents.is_contiguous) {
		if (index > array->contents.vec.size()) {
			// do nothing
		} else if (index + 1 == array->contents.vec.size()) {
			array->contents.vec.pop_back();
		} else {
			// make non-contiguous
			array->contents.is_contiguous = false;
			jfl::um_vec<jfl::Any> old_data = std::move(array->contents.vec);
			std::destroy_at(&array->contents.vec);
			new(&array->contents.map)jfl::um_map<jfl::UInt, jfl::Any>();
			for (size_t i = 0; i < old_data.size(); i++) {
				array->contents.map[(jfl::UInt)i] = old_data[i];
			}
		}
	}
	if (!array->contents.is_contiguous) {
		array->contents.map.erase(index);
	}
}
jfl::Any Array::jfl_get(Array* array, jfl::UInt index){
	if (array->contents.is_contiguous) {
		if (index < array->contents.vec.size()) {
			return array->contents.vec[index];
		} else {
			return jfl::undefined;
		}
		return true;
	} else {
		auto it = array->contents.map.find(index);
		if ( it == array->contents.map.end() ){
			return jfl::undefined;
		}
		return it->second;
	}
}

struct ArrayIterator{
	jfl::Boolean(*next) (jfl::Iterator* iterator, jfl::Any* key, jfl::Any* value);
	Array* array;
	jfl::UInt idx;
};

jfl::Boolean Array::jfl_Iterate(jfl::Object* object, jfl::Iterator* iter) {
	ArrayIterator* array_iter = (ArrayIterator*)iter;
	array_iter->array = (fl::Array*)object;
	array_iter->idx = 0;
	array_iter->next = [](jfl::Iterator* iterator, jfl::Any* key, jfl::Any* value) -> jfl::Boolean{
		ArrayIterator* array_iter = (ArrayIterator*)iterator;
		Array* array = array_iter->array;
		if ( array->contents.is_contiguous ){
			if ( array_iter->idx < array->contents.vec.size() ){
				*key = array_iter->idx;
				*value = array->contents.vec[array_iter->idx];
				array_iter->idx++;
				return true;
			}
		}else{
			auto it = array->contents.map.lower_bound(array_iter->idx);
			if ( it != array->contents.map.end() ){
				*key = it->first;
				*value = it->second;
				array_iter->idx = it->first + 1;
				return true;
			}
		}

		array->jfl_dynamic.iterate(&array->jfl_dynamic, iterator);
		return iterator->next(iterator, key, value);
	};
	return true;
}
void Array::jfl_p__init(fl::Array* arr){
	arr->contents.len = 0;
	arr->contents.is_contiguous = true;
	new(&arr->contents.vec)std::vector<jfl::Any>();
}

jfl::UInt Array::jfl_g_length(fl::Array* arr){
	return arr->contents.len;
}
void Array::jfl_s_length(fl::Array* arr, jfl::UInt len){
	if ( arr->contents.len == len ){
		return;
	}
	if ( len < arr->contents.len ){
		if ( arr->contents.is_contiguous ){
			arr->contents.vec.resize(len);
			arr->contents.len = len;
		}else{
			auto& map = arr->contents.map;
			auto it = map.lower_bound(len);
			map.erase(it, map.end());
			arr->contents.len = len;
		}
	}else{
		arr->contents.len = len;
	}

}
jfl::Any Array::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_pop(fl::Array* arr){
	if (arr->contents.len == 0 ){
		return jfl::undefined;
	}
	jfl::Any ret = jfl_get(arr, arr->contents.len - 1);
	jfl_s_length(arr, arr->contents.len - 1);
	return ret;
}
jfl::UInt Array::jfl_n_Qhttp_3a_S_Sadobe_2ecom_SAS3_S2006_Sbuiltin_Q_n_push(fl::Array* arr, jfl::RestParams* params){
	for ( size_t idx = 0; idx < params->size; idx++ ){
		jfl_set(arr, arr->contents.len, params->ptr[idx]);
	}
	return arr->contents.len;
}

}
#endif
