package
{
   [native(instance="RangeErrorObject",methods="auto",cls="RangeErrorClass",gc="exact")]
   public dynamic class RangeError extends Error
   {
      
      public static const length:int = 1;
      
      
      public function RangeError(message:* = "", id:* = 0)
      {
      }
   }
}
