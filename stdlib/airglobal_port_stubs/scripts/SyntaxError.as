package
{
   [native(instance="SyntaxErrorObject",methods="auto",cls="SyntaxErrorClass",gc="exact")]
   public dynamic class SyntaxError extends Error
   {
      
      public static const length:int = 1;
      
      
      public function SyntaxError(message:* = "", id:* = 0)
      {
      }
   }
}
