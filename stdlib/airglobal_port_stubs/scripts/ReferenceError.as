package
{
   [native(instance="ReferenceErrorObject",methods="auto",cls="ReferenceErrorClass",gc="exact")]
   public dynamic class ReferenceError extends Error
   {
      
      public static const length:int = 1;
      
      
      public function ReferenceError(message:* = "", id:* = 0)
      {
      }
   }
}
