package flash.security
{
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.utils.ByteArray;
   import flash.utils.IDataInput;
   
   [API("661")]
   [native(instance="XMLSignatureValidatorObject",methods="auto",cls="XMLSignatureValidatorClass",construct="check",gc="exact")]
   [Event(name="error",type="flash.events.ErrorEvent")]
   [Event(name="complete",type="flash.events.Event")]
   public class XMLSignatureValidator extends EventDispatcher
   {
      
      private static const XML_DSIG_NS:Namespace;
      
      private static const XML_XADES_NAMESPACE:String = "http://uri.etsi.org/01903/v1.1.1#";
      
      private static const XMLDIGESTMETHOD_SHA256:String = "http://www.w3.org/2001/04/xmlenc#sha256";
      
      private static const XML_TRANSFORM_C14N:String = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
      
      private static const XML_TRANSFORM_ENVELOPED_SIGNATURE:String = "http://www.w3.org/2000/09/xmldsig#enveloped-signature";
      
      private static const MAX_NUM_TRANSFORMS:uint = 20;
      
      private static const kInvalidParamError:uint = 2004;
      
      private static const kNullPointerError:uint = 2007;
      
      private static const kInvalidCallError:uint = 2037;
      
      private static const VALID_IDENTITY:String = "validIdentity";
      
      private static const VALID_OR_UNKNOWN_IDENTITY:String = "validOrUnknownIdentity";
      
      private static const NEVER:String = "never";
      
      
      private var m_uriDereferencer:IURIDereferencer;
      
      private var m_isValidating:Boolean;
      
      private var m_referencesValidationSetting:String;
      
      private var m_cryptContext:CryptContext;
      
      private var m_referenceStatus:String;
      
      private var m_signedDataString:String;
      
      private var m_sigValueString:String;
      
      private var m_timestampUris:Array;
      
      private var _ignoreCertTime:Boolean = false;
      
      private var stubProperties:Object;
      
      public function XMLSignatureValidator()
      {
      }
      
      [Version("air2.0")]
      public static function get isSupported() : Boolean
      {
         return null; //autogenerated
      }
      
      native private static function _checkSupported() : Boolean;
      
      public function get uriDereferencer() : IURIDereferencer
      {
         return null; //autogenerated
      }
      
      public function set uriDereferencer(uriDerefer:IURIDereferencer) : void
      {
      }
      
      public function addCertificate(cert:ByteArray, trusted:Boolean) : *
      {
         return null; //autogenerated
      }
      
      public function get useSystemTrustStore() : Boolean
      {
         return null; //autogenerated
      }
      
      public function set useSystemTrustStore(trusted:Boolean) : void
      {
      }
      
      public function get revocationCheckSetting() : String
      {
         return ""; //autogenerated
      }
      
      public function set revocationCheckSetting(setting:String) : void
      {
      }
      
      [API("663")]
      public function get referencesValidationSetting() : String
      {
         return ""; //autogenerated
      }
      
      [API("663")]
      public function set referencesValidationSetting(setting:String) : void
      {
      }
      
      private function cryptContextCodeToStatus(cryptContextCode:uint) : String
      {
         return ""; //autogenerated
      }
      
      private function throwIfValidating() : *
      {
         return null; //autogenerated
      }
      
      public function get referencesStatus() : String
      {
         return ""; //autogenerated
      }
      
      public function get digestStatus() : String
      {
         return ""; //autogenerated
      }
      
      public function get identityStatus() : String
      {
         return ""; //autogenerated
      }
      
      public function get validityStatus() : String
      {
         return ""; //autogenerated
      }
      
      public function get signerTrustSettings() : Array
      {
         return null; //autogenerated
      }
      
      public function get signerExtendedKeyUsages() : Array
      {
         return null; //autogenerated
      }
      
      private function applyTransforms(param1:IDataInput, param2:XMLList, param3:XML) : IDataInput
      {
         return null; //autogenerated
      }
      
      private function verifyReferences(param1:XML) : Boolean
      {
         return null; //autogenerated
      }
      
      public function verify(signature:XML) : void
      {
      }
      
      private function validateReferences(signature:XML) : void
      {
      }
      
      private function verifyCommon(signature:XML) : void
      {
      }
      
      private function verifyTimestamp(signature:XML) : void
      {
      }
      
      private function readSigKeyInfo(signature:XML) : void
      {
      }
      
      [cppcall]
      private function _verifySync(signature:XML) : void
      {
      }
      
      [cppcall]
      private function _setTimestampUris(uris:Array) : void
      {
      }
      
      [cppcall]
      private function _setUseCodeSigningValidationRules() : void
      {
      }
      
      [cppcall]
      private function _addTimestampingRoot(rootCert:ByteArray) : void
      {
      }
      
      [cppcall]
      private function _setTimestampRevocationCheckSetting(setting:String) : void
      {
      }
      
      private function getTimestampRevocationCheckSetting() : String
      {
         return ""; //autogenerated
      }
      
      private function getCryptContext() : CryptContext
      {
         return null; //autogenerated
      }
      
      public function get signerCN() : String
      {
         return ""; //autogenerated
      }
      
      public function get signerDN() : String
      {
         return ""; //autogenerated
      }
      
      [cppcall]
      private function _getPublicKey(signature:XML) : ByteArray
      {
         return null; //autogenerated
      }
      
      [cppcall]
      private function _getSignerValidEndTime() : uint
      {
         return 0; //autogenerated
      }
      
      [cppcall]
      private function _getVerificationTime() : uint
      {
         return 0; //autogenerated
      }
      
      [cppcall]
      private function _getSignerIDSummary(version:uint) : String
      {
         return ""; //autogenerated
      }
      
      [cppcall]
      private function _guessSignerIDSummary(signature:XML, version:uint) : String
      {
         return ""; //autogenerated
      }
      
      native private function constructAVMPlusDigest() : AVMPlusDigest;
      
      native private function constructCryptContext() : CryptContext;
      
      native private function constructXMLCanonicalizer() : XMLCanonicalizer;
      
      native private function constructEnvelopedTransformer() : XMLSignatureEnvelopedTransformer;
      
      [cppcall]
      private function _setIgnoreCertTime(ignoreCertTime:Boolean) : void
      {
      }
      
      private function initStubProperties() : void
      {
      }
      
      private function setStubProperty(key:String, value:*) : *
      {
         return null; //autogenerated
      }
      
      private function getStubProperty(key:String) : *
      {
         return null; //autogenerated
      }
   }
}
