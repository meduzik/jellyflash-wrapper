package flash.security
{
   public final class SignerTrustSettings
   {
      
      public static const SIGNING:String = "signing";
      
      public static const CODE_SIGNING:String = "codeSigning";
      
      public static const PLAYLIST_SIGNING:String = "playlistSigning";
      
      
      public function SignerTrustSettings()
      {
      }
   }
}
