package flash.desktop
{
   import flash.display.NativeMenu;
   
   [API("661")]
   [native(instance="SystemTrayIconObject",methods="auto",cls="SystemTrayIconClass",construct="native")]
   [Event(name="rightClick",type="flash.events.ScreenMouseEvent")]
   [Event(name="rightMouseUp",type="flash.events.ScreenMouseEvent")]
   [Event(name="rightMouseDown",type="flash.events.ScreenMouseEvent")]
   [Event(name="click",type="flash.events.ScreenMouseEvent")]
   [Event(name="mouseUp",type="flash.events.ScreenMouseEvent")]
   [Event(name="mouseDown",type="flash.events.ScreenMouseEvent")]
   public class SystemTrayIcon extends InteractiveIcon
   {
      
      public static const MAX_TIP_LENGTH:Number = 63;
      
      
      public function SystemTrayIcon()
      {
      }
      
      native override public function set bitmaps(param1:Array) : void;
      
      native override public function get bitmaps() : Array;
      
      native override public function get width() : int;
      
      native override public function get height() : int;
      
      native public function get tooltip() : String;
      
      native public function set tooltip(param1:String) : void;
      
      native public function get menu() : NativeMenu;
      
      native public function set menu(param1:NativeMenu) : void;
   }
}
