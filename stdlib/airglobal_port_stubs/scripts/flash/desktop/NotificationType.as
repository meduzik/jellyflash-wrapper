package flash.desktop
{
   [API("661")]
   public final class NotificationType
   {
      
      public static const INFORMATIONAL:String = "informational";
      
      public static const CRITICAL:String = "critical";
      
      
      public function NotificationType()
      {
      }
   }
}
