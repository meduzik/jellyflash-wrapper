package flash.desktop
{
   public final class InvokeEventReason
   {
      
      public static const LOGIN:String = "login";
      
      [API("685")]
      public static const NOTIFICATION:String = "notification";
      
      [API("685")]
      public static const OPEN_URL:String = "openUrl";
      
      public static const STANDARD:String = "standard";
      
      
      public function InvokeEventReason()
      {
      }
   }
}
