package flash.desktop
{
   [API("661")]
   [native(instance="WindowIconObject",methods="auto",cls="WindowIconClass")]
   public class NativeWindowIcon extends InteractiveIcon
   {
      
      
      public function NativeWindowIcon()
      {
      }
      
      native override public function set bitmaps(param1:Array) : void;
      
      native override public function get bitmaps() : Array;
      
      native override public function get width() : int;
      
      native override public function get height() : int;
   }
}
