package flash.desktop
{
   import flash.display.NativeMenu;
   
   [API("661")]
   [native(instance="DockIconObject",methods="auto",cls="DockIconClass",construct="native")]
   public class DockIcon extends InteractiveIcon
   {
      
      
      public function DockIcon()
      {
      }
      
      native override public function set bitmaps(param1:Array) : void;
      
      native override public function get bitmaps() : Array;
      
      native override public function get width() : int;
      
      native override public function get height() : int;
      
      native public function get menu() : NativeMenu;
      
      native public function set menu(param1:NativeMenu) : void;
      
      native public function bounce(param1:String = "informational") : void;
   }
}
