package flash.printing
{
   [Version("air2.0")]
   public final class PrintMethod
   {
      
      public static const AUTO:String = "auto";
      
      public static const VECTOR:String = "vector";
      
      public static const BITMAP:String = "bitmap";
      
      
      public function PrintMethod()
      {
      }
   }
}
