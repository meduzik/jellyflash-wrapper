package flash.sampler
{
   [API("732")]
   final class ClassFactory
   {
      
      public static const StackFrameClass:Class;
      
      public static const SampleClass:Class;
      
      public static const DeleteObjectSampleClass:Class;
      
      public static const NewObjectSampleClass:Class;
      
      
      function ClassFactory()
      {
      }
   }
}
