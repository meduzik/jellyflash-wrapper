package flash.net
{
   [Version("air2.0")]
   public final class IPVersion
   {
      
      public static const IPV4:String = "IPv4";
      
      public static const IPV6:String = "IPv6";
      
      
      public function IPVersion()
      {
      }
   }
}
