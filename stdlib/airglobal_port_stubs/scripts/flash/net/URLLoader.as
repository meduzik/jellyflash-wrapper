package flash.net
{
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.ProgressEvent;
   import flash.utils.ByteArray;
   
   [native(instance="URLLoaderObject",methods="auto",cls="URLLoaderClass",gc="exact")]
   [Event(name="httpResponseStatus",type="flash.events.HTTPStatusEvent")]
   [Event(name="httpStatus",type="flash.events.HTTPStatusEvent")]
   [Event(name="securityError",type="flash.events.SecurityErrorEvent")]
   [Event(name="ioError",type="flash.events.IOErrorEvent")]
   [Event(name="progress",type="flash.events.ProgressEvent")]
   [Event(name="complete",type="flash.events.Event")]
   [Event(name="open",type="flash.events.Event")]
   public class URLLoader extends EventDispatcher
   {
      
      
      public var data;
      
      public var dataFormat:String = "text";
      
      private var stream:URLStream;
      
      public var bytesLoaded:uint = 0;
      
      public var bytesTotal:uint = 0;
      
      public function URLLoader(request:URLRequest = null)
      {
      }
      
      override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false) : void
      {
      }
      
      public function load(request:URLRequest) : void
      {
      }
      
      public function close() : void
      {
      }
      
      private function redirectEvent(event:Event) : void
      {
      }
      
      private function onComplete(event:Event) : void
      {
      }
      
      private function onProgress(event:ProgressEvent) : void
      {
      }
   }
}
