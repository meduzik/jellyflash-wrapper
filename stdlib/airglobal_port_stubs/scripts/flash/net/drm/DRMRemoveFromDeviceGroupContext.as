package flash.net.drm
{
   import flash.events.DRMDeviceGroupErrorEvent;
   import flash.events.DRMDeviceGroupEvent;
   
   [native(instance="DRMRemoveFromDeviceGroupContextObject",methods="auto",cls="DRMRemoveFromDeviceGroupContextClass")]
   class DRMRemoveFromDeviceGroupContext extends DRMManagerSession
   {
      
      
      private var m_deviceGroup:DRMDeviceGroup;
      
      function DRMRemoveFromDeviceGroupContext()
      {
      }
      
      public function removeFromDeviceGroup(deviceGroup:DRMDeviceGroup) : void
      {
      }
      
      override public function onSessionComplete() : void
      {
      }
      
      override public function onSessionError() : void
      {
      }
      
      native private function removeFromDeviceGroupInner(param1:DRMDeviceGroup) : uint;
   }
}
