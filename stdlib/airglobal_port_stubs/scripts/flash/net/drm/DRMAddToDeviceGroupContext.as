package flash.net.drm
{
   import flash.events.DRMDeviceGroupErrorEvent;
   import flash.events.DRMDeviceGroupEvent;
   
   [native(instance="DRMAddToDeviceGroupContextObject",methods="auto",cls="DRMAddToDeviceGroupContextClass")]
   class DRMAddToDeviceGroupContext extends DRMManagerSession
   {
      
      
      private var m_deviceGroup:DRMDeviceGroup;
      
      function DRMAddToDeviceGroupContext()
      {
      }
      
      public function addToDeviceGroup(deviceGroup:DRMDeviceGroup, forceRefresh:Boolean) : void
      {
      }
      
      override public function onSessionComplete() : void
      {
      }
      
      native private function getResultInner() : String;
      
      override public function onSessionError() : void
      {
      }
      
      native private function addToDeviceGroupInner(param1:DRMDeviceGroup, param2:Boolean) : uint;
   }
}
