package flash.net.drm
{
   import flash.events.DRMReturnVoucherCompleteEvent;
   import flash.events.DRMReturnVoucherErrorEvent;
   
   [native(instance="DRMReturnVoucherContextObject",methods="auto",cls="DRMReturnVoucherContextClass")]
   class DRMReturnVoucherContext extends DRMManagerSession
   {
      
      
      private var m_serverURL:String;
      
      private var m_licenseID:String;
      
      private var m_policyID:String;
      
      private var m_immediateCommit:Boolean;
      
      function DRMReturnVoucherContext()
      {
      }
      
      public function returnVoucher(inServerURL:String, immediateCommit:Boolean, licenseID:String, policyID:String) : void
      {
      }
      
      override public function onSessionComplete() : void
      {
      }
      
      override public function onSessionError() : void
      {
      }
      
      native private function returnVoucherInner(param1:String, param2:Boolean, param3:String, param4:String) : uint;
      
      native private function getResultInner() : int;
   }
}
