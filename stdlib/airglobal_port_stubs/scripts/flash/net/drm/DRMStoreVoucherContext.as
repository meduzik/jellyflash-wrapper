package flash.net.drm
{
   import flash.utils.ByteArray;
   
   [native(instance="DRMStoreVoucherContextObject",methods="auto",cls="DRMStoreVoucherContextClass")]
   class DRMStoreVoucherContext extends DRMManagerSession
   {
      
      
      private var m_voucher:ByteArray;
      
      function DRMStoreVoucherContext(voucher:ByteArray)
      {
      }
      
      override public function onSessionComplete() : void
      {
      }
      
      override public function onSessionError() : void
      {
      }
      
      public function doStoreVoucher() : void
      {
      }
      
      native private function storeVoucherInner(param1:ByteArray) : uint;
   }
}
