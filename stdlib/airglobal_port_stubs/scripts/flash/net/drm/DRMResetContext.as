package flash.net.drm
{
   [native(instance="DRMResetContextObject",methods="auto",cls="DRMResetContextClass")]
   class DRMResetContext extends DRMManagerSession
   {
      
      
      public var m_isAutoReset:Boolean;
      
      function DRMResetContext(isAutoReset:Boolean)
      {
      }
      
      override public function onSessionComplete() : void
      {
      }
      
      override public function onSessionError() : void
      {
      }
      
      public function doReset() : void
      {
      }
      
      native private function resetInner(param1:Boolean) : uint;
   }
}
