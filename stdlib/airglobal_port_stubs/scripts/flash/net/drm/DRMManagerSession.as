package flash.net.drm
{
   import flash.events.EventDispatcher;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   
   [native(instance="DRMManagerSessionObject",methods="auto",cls="DRMManagerSessionClass")]
   class DRMManagerSession extends EventDispatcher
   {
      
      static const STATUS_READY:uint = 0;
      
      static const STATUS_NOTREADY:uint = 1;
      
      static const STATUS_FAILED:uint = 2;
      
      static const STATUS_UNKNOWN:uint = 3;
      
      
      private var m_metadata:DRMContentData;
      
      private var m_checkStatusTimer:Timer;
      
      public var m_isInSession:Boolean;
      
      function DRMManagerSession()
      {
      }
      
      public function onSessionError() : void
      {
      }
      
      public function onSessionComplete() : void
      {
      }
      
      public function setTimerUp() : void
      {
      }
      
      public function get metadata() : DRMContentData
      {
         return null; //autogenerated
      }
      
      public function set metadata(inData:DRMContentData) : void
      {
      }
      
      public function checkStatus() : uint
      {
         return 0; //autogenerated
      }
      
      native private function isAutoResetError(param1:uint) : Boolean;
      
      private function doAutoReset() : void
      {
      }
      
      private function onCheckStatus(ev:TimerEvent) : *
      {
         return null; //autogenerated
      }
      
      native public function getLastError() : uint;
      
      native public function getLastSubErrorID() : uint;
      
      native public function getLastServerErrorString() : String;
      
      public function issueDRMStatusEvent(inMetadata:DRMContentData, voucher:DRMVoucher) : *
      {
         return null; //autogenerated
      }
      
      native private function issueDRMStatusEventInner(param1:String, param2:DRMContentData, param3:DRMVoucher, param4:Boolean) : void;
      
      native public function issueDRMErrorEvent(param1:DRMContentData, param2:int, param3:int, param4:String) : void;
      
      native public function errorCodeToThrow(param1:uint) : void;
      
      native private function checkStatusInner() : uint;
   }
}
