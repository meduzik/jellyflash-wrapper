package flash.net
{
   import flash.events.EventDispatcher;
   import flash.utils.ByteArray;
   
   [native(instance="DatagramSocketObject",methods="auto",cls="DatagramSocketClass")]
   [Version("air2.0")]
   [Event(name="ioError",type="flash.events.IOErrorEvent")]
   [Event(name="data",type="flash.events.DatagramSocketDataEvent")]
   [Event(name="close",type="flash.events.Event")]
   public class DatagramSocket extends EventDispatcher
   {
      
      
      public function DatagramSocket()
      {
      }
      
      native public static function get isSupported() : Boolean;
      
      native public function bind(param1:int = 0, param2:String = "0.0.0.0") : void;
      
      native public function connect(param1:String, param2:int) : void;
      
      native public function receive() : void;
      
      native public function close() : void;
      
      native public function send(param1:ByteArray, param2:uint = 0, param3:uint = 0, param4:String = null, param5:int = 0) : void;
      
      native public function get connected() : Boolean;
      
      native public function get bound() : Boolean;
      
      native public function get localAddress() : String;
      
      native public function get localPort() : int;
      
      native public function get remoteAddress() : String;
      
      native public function get remotePort() : int;
   }
}
