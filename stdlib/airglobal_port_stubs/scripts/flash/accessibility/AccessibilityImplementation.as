package flash.accessibility
{
   import flash.geom.Rectangle;
   
   public class AccessibilityImplementation
   {
      
      
      public var stub:Boolean;
      
      public var errno:uint;
      
      public function AccessibilityImplementation()
      {
      }
      
      public function get_accRole(childID:uint) : uint
      {
         return 0; //autogenerated
      }
      
      public function get_accName(childID:uint) : String
      {
         return ""; //autogenerated
      }
      
      public function get_accValue(childID:uint) : String
      {
         return ""; //autogenerated
      }
      
      public function get_accState(childID:uint) : uint
      {
         return 0; //autogenerated
      }
      
      public function get_accDefaultAction(childID:uint) : String
      {
         return ""; //autogenerated
      }
      
      public function accDoDefaultAction(childID:uint) : void
      {
      }
      
      public function isLabeledBy(labelBounds:Rectangle) : Boolean
      {
         return null; //autogenerated
      }
      
      public function getChildIDArray() : Array
      {
         return null; //autogenerated
      }
      
      public function accLocation(childID:uint) : *
      {
         return null; //autogenerated
      }
      
      public function get_accSelection() : Array
      {
         return null; //autogenerated
      }
      
      public function get_accFocus() : uint
      {
         return 0; //autogenerated
      }
      
      public function accSelect(operation:uint, childID:uint) : void
      {
      }
      
      [cppcall]
      [API("732")]
      public function get_selectionAnchorIndex() : *
      {
         return null; //autogenerated
      }
      
      [cppcall]
      [API("732")]
      public function get_selectionActiveIndex() : *
      {
         return null; //autogenerated
      }
   }
}
