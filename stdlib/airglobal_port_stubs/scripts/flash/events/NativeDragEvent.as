package flash.events
{
   import flash.desktop.Clipboard;
   import flash.desktop.NativeDragOptions;
   import flash.display.InteractiveObject;
   
   public class NativeDragEvent extends MouseEvent
   {
      
      public static const NATIVE_DRAG_ENTER:String = "nativeDragEnter";
      
      public static const NATIVE_DRAG_OVER:String = "nativeDragOver";
      
      public static const NATIVE_DRAG_DROP:String = "nativeDragDrop";
      
      public static const NATIVE_DRAG_EXIT:String = "nativeDragExit";
      
      public static const NATIVE_DRAG_START:String = "nativeDragStart";
      
      public static const NATIVE_DRAG_UPDATE:String = "nativeDragUpdate";
      
      public static const NATIVE_DRAG_COMPLETE:String = "nativeDragComplete";
      
      
      public var clipboard:Clipboard;
      
      public var allowedActions:NativeDragOptions;
      
      public var dropAction:String;
      
      public function NativeDragEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = true, localX:Number = undefined, localY:Number = undefined, relatedObject:InteractiveObject = null, clipboard:Clipboard = null, allowedActions:NativeDragOptions = null, dropAction:String = null, controlKey:Boolean = false, altKey:Boolean = false, shiftKey:Boolean = false, commandKey:Boolean = false)
      {
      }
      
      override public function clone() : Event
      {
         return null; //autogenerated
      }
      
      override public function toString() : String
      {
         return ""; //autogenerated
      }
   }
}
