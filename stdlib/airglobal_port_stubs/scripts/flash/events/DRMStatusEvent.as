package flash.events
{
   import flash.net.drm.DRMContentData;
   import flash.net.drm.DRMVoucher;
   
   [API("667")]
   public class DRMStatusEvent extends Event
   {
      
      public static const DRM_STATUS:String = "drmStatus";
      
      
      private var m_detail:String;
      
      private var m_voucher:DRMVoucher;
      
      private var m_metadata:DRMContentData;
      
      private var m_isLocal:Boolean;
      
      public function DRMStatusEvent(type:String = "drmStatus", bubbles:Boolean = false, cancelable:Boolean = false, inMetadata:DRMContentData = null, inVoucher:DRMVoucher = null, inLocal:Boolean = false)
      {
      }
      
      override public function clone() : Event
      {
         return null; //autogenerated
      }
      
      override public function toString() : String
      {
         return ""; //autogenerated
      }
      
      [API("667")]
      public function get contentData() : DRMContentData
      {
         return null; //autogenerated
      }
      
      [API("667")]
      public function set contentData(value:DRMContentData) : void
      {
      }
      
      [API("667")]
      public function get voucher() : DRMVoucher
      {
         return null; //autogenerated
      }
      
      [API("667")]
      public function set voucher(value:DRMVoucher) : void
      {
      }
      
      [API("667")]
      public function get isLocal() : Boolean
      {
         return null; //autogenerated
      }
      
      [API("667")]
      public function set isLocal(value:Boolean) : void
      {
      }
      
      public function get isAvailableOffline() : Boolean
      {
         return null; //autogenerated
      }
      
      public function get isAnonymous() : Boolean
      {
         return null; //autogenerated
      }
      
      public function get voucherEndDate() : Date
      {
         return null; //autogenerated
      }
      
      [API("661")]
      public function get offlineLeasePeriod() : uint
      {
         return 0; //autogenerated
      }
      
      public function get policies() : Object
      {
         return null; //autogenerated
      }
      
      public function get detail() : String
      {
         return ""; //autogenerated
      }
   }
}
