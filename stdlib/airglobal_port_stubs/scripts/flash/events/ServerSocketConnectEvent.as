package flash.events
{
   import flash.net.Socket;
   
   [Version("air2.0")]
   public class ServerSocketConnectEvent extends Event
   {
      
      public static const CONNECT:String = "connect";
      
      
      private var _socket:Socket = null;
      
      public function ServerSocketConnectEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, socket:Socket = null)
      {
      }
      
      override public function clone() : Event
      {
         return null; //autogenerated
      }
      
      override public function toString() : String
      {
         return ""; //autogenerated
      }
      
      public function get socket() : Socket
      {
         return null; //autogenerated
      }
      
      public function set socket(value:Socket) : void
      {
      }
   }
}
