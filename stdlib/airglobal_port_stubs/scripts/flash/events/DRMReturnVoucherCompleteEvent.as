package flash.events
{
   [API("690")]
   public class DRMReturnVoucherCompleteEvent extends Event
   {
      
      public static const RETURN_VOUCHER_COMPLETE:String = "returnVoucherComplete";
      
      
      private var m_serverURL:String;
      
      private var m_licenseID:String;
      
      private var m_policyID:String;
      
      private var m_numberOfVouchersReturned:int;
      
      public function DRMReturnVoucherCompleteEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, inServerURL:String = null, inLicenseID:String = null, inPolicyID:String = null, inNumberOfVouchersReturned:int = 0)
      {
      }
      
      override public function clone() : Event
      {
         return null; //autogenerated
      }
      
      public function get serverURL() : String
      {
         return ""; //autogenerated
      }
      
      public function set serverURL(value:String) : void
      {
      }
      
      public function get licenseID() : String
      {
         return ""; //autogenerated
      }
      
      public function set licenseID(value:String) : void
      {
      }
      
      public function get policyID() : String
      {
         return ""; //autogenerated
      }
      
      public function set policyID(value:String) : void
      {
      }
      
      public function get numberOfVouchersReturned() : int
      {
         return 0; //autogenerated
      }
      
      public function set numberOfVouchersReturned(value:int) : void
      {
      }
   }
}
