package flash.events
{
   [API("661")]
   public class NativeWindowDisplayStateEvent extends Event
   {
      
      public static const DISPLAY_STATE_CHANGING:String = "displayStateChanging";
      
      public static const DISPLAY_STATE_CHANGE:String = "displayStateChange";
      
      
      private var m_beforeDisplayState:String;
      
      private var m_afterDisplayState:String;
      
      public function NativeWindowDisplayStateEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false, beforeDisplayState:String = "", afterDisplayState:String = "")
      {
      }
      
      override public function clone() : Event
      {
         return null; //autogenerated
      }
      
      public function get beforeDisplayState() : String
      {
         return ""; //autogenerated
      }
      
      public function get afterDisplayState() : String
      {
         return ""; //autogenerated
      }
      
      override public function toString() : String
      {
         return ""; //autogenerated
      }
   }
}
