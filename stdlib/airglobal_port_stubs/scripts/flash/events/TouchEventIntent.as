package flash.events
{
   public final class TouchEventIntent
   {
      
      public static const UNKNOWN:String = "unknown";
      
      public static const PEN:String = "pen";
      
      public static const ERASER:String = "eraser";
      
      
      public function TouchEventIntent()
      {
      }
   }
}
