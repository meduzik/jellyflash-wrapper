package flash.events
{
   [native(instance="EventDispatcherObject",methods="auto",cls="EventDispatcherClass",gc="exact")]
   [Event(name="deactivate",type="flash.events.Event")]
   [Event(name="activate",type="flash.events.Event")]
   public class EventDispatcher implements IEventDispatcher
   {
      
      
      public function EventDispatcher(target:IEventDispatcher = null)
      {
      }
      
      private static function trimHeaderValue(headerValue:String) : String
      {
         return ""; //autogenerated
      }
      
      native private function ctor(param1:IEventDispatcher) : void;
      
      public function toString() : String
      {
         return ""; //autogenerated
      }
      
      native public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void;
      
      native public function removeEventListener(param1:String, param2:Function, param3:Boolean = false) : void;
      
      [cppcall]
      public function dispatchEvent(event:Event) : Boolean
      {
         return null; //autogenerated
      }
      
      native public function hasEventListener(param1:String) : Boolean;
      
      native public function willTrigger(param1:String) : Boolean;
      
      native private function dispatchEventFunction(param1:Event) : Boolean;
      
      [cppcall]
      private function dispatchHttpStatusEvent(status:uint, redirected:Boolean, responseLocation:String, headers:String) : void
      {
      }
   }
}
