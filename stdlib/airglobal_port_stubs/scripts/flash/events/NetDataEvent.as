package flash.events
{
   [API("672")]
   public class NetDataEvent extends Event
   {
      
      public static const MEDIA_TYPE_DATA:String = "mediaTypeData";
      
      
      private var m_info:Object;
      
      private var m_timestamp:Number;
      
      public function NetDataEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, timestamp:Number = 0, info:Object = null)
      {
      }
      
      override public function clone() : Event
      {
         return null; //autogenerated
      }
      
      override public function toString() : String
      {
         return ""; //autogenerated
      }
      
      public function get timestamp() : Number
      {
         return null; //autogenerated
      }
      
      public function get info() : Object
      {
         return null; //autogenerated
      }
   }
}
