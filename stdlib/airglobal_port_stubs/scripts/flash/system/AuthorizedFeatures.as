package flash.system
{
   import flash.net.URLStream;
   import flash.utils.ByteArray;
   
   [API("682")]
   [native(instance="AuthorizedFeaturesObject",methods="auto",cls="AuthorizedFeaturesClass",construct="native",gc="exact")]
   [ExcludeClass]
   public final class AuthorizedFeatures
   {
      
      
      public function AuthorizedFeatures()
      {
      }
      
      native public function createApplicationInstaller(param1:XML, param2:ByteArray) : ApplicationInstaller;
      
      native public function enableDiskCache(param1:URLStream) : Boolean;
      
      native function isFeatureEnabled(param1:String, param2:String = null) : Boolean;
      
      native function isNegativeToken() : Boolean;
   }
}
