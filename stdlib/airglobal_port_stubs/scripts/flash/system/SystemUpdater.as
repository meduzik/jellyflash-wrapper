package flash.system
{
   import adobe.utils.ProductManager;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   
   [native(instance="SystemUpdaterObject",methods="auto",cls="SystemUpdaterClass",construct="check")]
   [API("667")]
   [Version("10.1")]
   [Event(name="cancel",type="flash.events.Event")]
   [Event(name="complete",type="flash.events.Event")]
   [Event(name="securityError",type="flash.events.SecurityErrorEvent")]
   [Event(name="ioError",type="flash.events.IOErrorEvent")]
   [Event(name="progress",type="flash.events.ProgressEvent")]
   [Event(name="status",type="flash.events.StatusEvent")]
   [Event(name="open",type="flash.events.Event")]
   public class SystemUpdater extends EventDispatcher
   {
      
      
      private var _pm:ProductManager;
      
      public function SystemUpdater()
      {
      }
      
      public function update(type:String) : void
      {
      }
      
      public function cancel() : void
      {
      }
      
      private function onProductManagerEvent(e:Event) : void
      {
      }
      
      native private function _update(param1:String, param2:ProductManager) : Boolean;
      
      native private function _cancel(param1:Boolean, param2:ProductManager) : void;
   }
}
