package flash.display
{
   import flash.events.EventDispatcher;
   import flash.events.KeyboardEvent;
   
   [native(instance="MenuItemObject",methods="auto",cls="MenuItemClass",gc="exact")]
   [API("667")]
   [Event(name="preparing",type="flash.events.Event")]
   [Event(name="displaying",type="flash.events.Event")]
   [Event(name="select",type="flash.events.Event")]
   public class NativeMenuItem extends EventDispatcher
   {
      
      private static const kInvalidCallError:uint = 2037;
      
      
      public function NativeMenuItem(label:String = "", isSeparator:Boolean = false)
      {
      }
      
      native public function get enabled() : Boolean;
      
      native public function set enabled(param1:Boolean) : void;
      
      native private function ctor(param1:String, param2:Boolean) : void;
      
      native public function get menu() : NativeMenu;
      
      native public function get name() : String;
      
      native public function set name(param1:String) : void;
      
      native public function get isSeparator() : Boolean;
      
      native public function get checked() : Boolean;
      
      native public function set checked(param1:Boolean) : void;
      
      native public function get label() : String;
      
      native public function set label(param1:String) : void;
      
      native public function get keyEquivalent() : String;
      
      native public function set keyEquivalent(param1:String) : void;
      
      native public function get keyEquivalentModifiers() : Array;
      
      native public function set keyEquivalentModifiers(param1:Array) : void;
      
      native public function get mnemonicIndex() : int;
      
      public function set mnemonicIndex(index:int) : void
      {
      }
      
      native private function set _mnemonicIndex(param1:int) : void;
      
      native public function get submenu() : NativeMenu;
      
      public function set submenu(submenu:NativeMenu) : void
      {
      }
      
      native private function set _submenu(param1:NativeMenu) : void;
      
      native public function get data() : Object;
      
      native public function set data(param1:Object) : void;
      
      public function clone() : NativeMenuItem
      {
         return null; //autogenerated
      }
      
      [cppcall]
      private function willPerformKeyEquivalent(event:KeyboardEvent) : Boolean
      {
         return null; //autogenerated
      }
      
      native private function _menuWillPerformKeyEquivalent(param1:NativeMenu, param2:KeyboardEvent) : Boolean;
      
      native private function _compareCharToKeyCode(param1:uint, param2:uint) : Boolean;
      
      [cppcall]
      private function performKeyEquivalent(event:KeyboardEvent, usingCharcode:int) : Boolean
      {
         return null; //autogenerated
      }
      
      native private function _menuPerformKeyEquivalent(param1:NativeMenu, param2:KeyboardEvent, param3:int) : Boolean;
      
      native private function select() : void;
      
      private function formatToString(className:String, ... arguments) : String
      {
         return ""; //autogenerated
      }
      
      override public function toString() : String
      {
         return ""; //autogenerated
      }
      
      private function get keyEquivalentChar() : int
      {
         return 0; //autogenerated
      }
   }
}
