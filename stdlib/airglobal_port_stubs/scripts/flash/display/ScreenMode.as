package flash.display
{
   [API("729")]
   [native(instance="ScreenModeObject",methods="auto",cls="ScreenModeClass",construct="native")]
   public class ScreenMode
   {
      
      
      public function ScreenMode()
      {
      }
      
      native public function get width() : int;
      
      native public function get height() : int;
      
      native public function get refreshRate() : int;
      
      native public function get colorDepth() : int;
   }
}
