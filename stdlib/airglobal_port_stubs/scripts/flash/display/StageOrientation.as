package flash.display
{
   [Version("air2.0")]
   public final class StageOrientation
   {
      
      public static const DEFAULT:String = "default";
      
      public static const ROTATED_RIGHT:String = "rotatedRight";
      
      public static const ROTATED_LEFT:String = "rotatedLeft";
      
      public static const UPSIDE_DOWN:String = "upsideDown";
      
      public static const UNKNOWN:String = "unknown";
      
      
      public function StageOrientation()
      {
      }
   }
}
