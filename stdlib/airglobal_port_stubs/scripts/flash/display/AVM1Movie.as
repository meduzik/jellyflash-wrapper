package flash.display
{
   import flash.utils.ByteArray;
   
   [native(instance="AVM1MovieObject",methods="auto",cls="AVM1MovieClass",construct="native")]
   public class AVM1Movie extends DisplayObject
   {
      
      
      private var callbackTable:Object;
      
      public function AVM1Movie()
      {
      }
      
      [Inspectable(environment="none")]
      public function call(functionName:String, ... arguments) : *
      {
         return null; //autogenerated
      }
      
      native private function get _interopAvailable() : Boolean;
      
      native private function _callAS2(param1:String, param2:ByteArray) : void;
      
      native private function _setCallAS3(param1:Function) : void;
      
      private function _callAS3(functionName:String, data:ByteArray) : void
      {
      }
      
      [Inspectable(environment="none")]
      public function addCallback(functionName:String, closure:Function) : void
      {
      }
   }
}
