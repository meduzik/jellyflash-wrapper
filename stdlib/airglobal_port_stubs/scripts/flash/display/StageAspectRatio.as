package flash.display
{
   [Version("air2.0")]
   public final class StageAspectRatio
   {
      
      public static const PORTRAIT:String = "portrait";
      
      public static const LANDSCAPE:String = "landscape";
      
      [API("681")]
      public static const ANY:String = "any";
      
      
      public function StageAspectRatio()
      {
      }
   }
}
