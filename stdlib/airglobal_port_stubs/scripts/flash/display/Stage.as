package flash.display
{
   import flash.accessibility.AccessibilityImplementation;
   import flash.accessibility.AccessibilityProperties;
   import flash.events.Event;
   import flash.geom.Rectangle;
   import flash.geom.Transform;
   import flash.media.StageVideo;
   import flash.text.TextSnapshot;
   
   [native(instance="StageObject",methods="auto",cls="StageClass",construct="native",gc="exact")]
   [Event(name="stageVideoAvailability",type="flash.events.StageVideoAvailabilityEvent")]
   [Event(name="orientationChange",type="flash.events.StageOrientationEvent")]
   [Event(name="orientationChanging",type="flash.events.StageOrientationEvent")]
   [Event(name="VsyncStateChangeAvailability",type="flash.events.VsyncStateChangeAvailabilityEvent")]
   [Event(name="fullScreen",type="flash.events.FullScreenEvent")]
   [Event(name="browserZoomChange",type="flash.events.Event")]
   [Event(name="resize",type="flash.events.Event")]
   [Event(name="mouseLeave",type="flash.events.Event")]
   public class Stage extends DisplayObjectContainer
   {
      
      private static const kInvalidParamError:uint = 2004;
      
      
      public function Stage()
      {
      }
      
      [Version("air2.0")]
      native public static function get supportsOrientationChange() : Boolean;
      
      [Exclude(kind="property",name="y")]
      [Exclude(kind="property",name="x")]
      [Exclude(kind="property",name="visible")]
      [Exclude(kind="property",name="transform")]
      [Exclude(kind="property",name="tabIndex")]
      [Exclude(kind="property",name="tabEnabled")]
      [Exclude(kind="property",name="scrollRect")]
      [Exclude(kind="property",name="scaleY")]
      [Exclude(kind="property",name="scaleX")]
      [Exclude(kind="property",name="scale9Grid")]
      [Exclude(kind="property",name="rotation")]
      [Exclude(kind="property",name="opaqueBackground")]
      [Exclude(kind="property",name="name")]
      [Exclude(kind="property",name="mouseEnabled")]
      [Exclude(kind="property",name="mask")]
      [Exclude(kind="property",name="loaderInfo")]
      [Exclude(kind="property",name="focusRect")]
      [Exclude(kind="property",name="filters")]
      [Exclude(kind="property",name="contextMenu")]
      [Exclude(kind="property",name="cacheAsBitmap")]
      [Exclude(kind="property",name="blendMode")]
      [Exclude(kind="property",name="alpha")]
      [Exclude(kind="property",name="accessibilityProperties")]
      [Exclude(kind="property",name="accessibilityImplementation")]
      native public function get frameRate() : Number;
      
      native public function set frameRate(param1:Number) : void;
      
      native public function invalidate() : void;
      
      native public function get scaleMode() : String;
      
      native public function set scaleMode(param1:String) : void;
      
      native public function get align() : String;
      
      native public function set align(param1:String) : void;
      
      native public function get stageWidth() : int;
      
      native public function set stageWidth(param1:int) : void;
      
      native public function get stageHeight() : int;
      
      native public function set stageHeight(param1:int) : void;
      
      native public function get showDefaultContextMenu() : Boolean;
      
      native public function set showDefaultContextMenu(param1:Boolean) : void;
      
      native public function get focus() : InteractiveObject;
      
      native public function set focus(param1:InteractiveObject) : void;
      
      [Version("10")]
      native public function get colorCorrection() : String;
      
      [Version("10")]
      native public function set colorCorrection(param1:String) : void;
      
      [Version("10")]
      native public function get colorCorrectionSupport() : String;
      
      native public function isFocusInaccessible() : Boolean;
      
      native public function get stageFocusRect() : Boolean;
      
      native public function set stageFocusRect(param1:Boolean) : void;
      
      native public function get quality() : String;
      
      native public function set quality(param1:String) : void;
      
      native public function get displayState() : String;
      
      native public function set displayState(param1:String) : void;
      
      native public function get fullScreenSourceRect() : Rectangle;
      
      native public function set fullScreenSourceRect(param1:Rectangle) : void;
      
      [API("678")]
      native public function get mouseLock() : Boolean;
      
      [API("678")]
      native public function set mouseLock(param1:Boolean) : void;
      
      [API("667")]
      native public function get stageVideos() : Vector.<StageVideo>;
      
      [API("674")]
      native public function get stage3Ds() : Vector.<Stage3D>;
      
      [API("670")]
      native public function get color() : uint;
      
      [API("670")]
      native public function set color(param1:uint) : void;
      
      native public function get fullScreenWidth() : uint;
      
      native public function get fullScreenHeight() : uint;
      
      [Version("10.0.32")]
      native public function get wmodeGPU() : Boolean;
      
      [API("670")]
      native public function get softKeyboardRect() : Rectangle;
      
      override public function set name(value:String) : void
      {
      }
      
      override public function set mask(value:DisplayObject) : void
      {
      }
      
      override public function set visible(value:Boolean) : void
      {
      }
      
      override public function set x(value:Number) : void
      {
      }
      
      override public function set y(value:Number) : void
      {
      }
      
      [Version("10")]
      override public function set z(value:Number) : void
      {
      }
      
      override public function set scaleX(value:Number) : void
      {
      }
      
      override public function set scaleY(value:Number) : void
      {
      }
      
      [Version("10")]
      override public function set scaleZ(value:Number) : void
      {
      }
      
      override public function set rotation(value:Number) : void
      {
      }
      
      [Version("10")]
      override public function set rotationX(value:Number) : void
      {
      }
      
      [Version("10")]
      override public function set rotationY(value:Number) : void
      {
      }
      
      [Version("10")]
      override public function set rotationZ(value:Number) : void
      {
      }
      
      override public function set alpha(value:Number) : void
      {
      }
      
      override public function set cacheAsBitmap(value:Boolean) : void
      {
      }
      
      override public function set opaqueBackground(value:Object) : void
      {
      }
      
      override public function set scrollRect(value:Rectangle) : void
      {
      }
      
      override public function set filters(value:Array) : void
      {
      }
      
      override public function set blendMode(value:String) : void
      {
      }
      
      override public function set transform(value:Transform) : void
      {
      }
      
      override public function set accessibilityProperties(value:AccessibilityProperties) : void
      {
      }
      
      override public function set scale9Grid(value:Rectangle) : void
      {
      }
      
      override public function set tabEnabled(value:Boolean) : void
      {
      }
      
      override public function set tabIndex(value:int) : void
      {
      }
      
      override public function set focusRect(value:Object) : void
      {
      }
      
      override public function set mouseEnabled(value:Boolean) : void
      {
      }
      
      override public function set accessibilityImplementation(value:AccessibilityImplementation) : void
      {
      }
      
      override public function addChild(child:DisplayObject) : DisplayObject
      {
         return null; //autogenerated
      }
      
      override public function addChildAt(child:DisplayObject, index:int) : DisplayObject
      {
         return null; //autogenerated
      }
      
      override public function setChildIndex(child:DisplayObject, index:int) : void
      {
      }
      
      override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false) : void
      {
      }
      
      override public function dispatchEvent(event:Event) : Boolean
      {
         return null; //autogenerated
      }
      
      override public function hasEventListener(type:String) : Boolean
      {
         return null; //autogenerated
      }
      
      override public function willTrigger(type:String) : Boolean
      {
         return null; //autogenerated
      }
      
      override public function get width() : Number
      {
         return null; //autogenerated
      }
      
      override public function set width(value:Number) : void
      {
      }
      
      override public function get height() : Number
      {
         return null; //autogenerated
      }
      
      override public function set height(value:Number) : void
      {
      }
      
      override public function get textSnapshot() : TextSnapshot
      {
         return null; //autogenerated
      }
      
      override public function get mouseChildren() : Boolean
      {
         return null; //autogenerated
      }
      
      override public function set mouseChildren(value:Boolean) : void
      {
      }
      
      override public function get numChildren() : int
      {
         return 0; //autogenerated
      }
      
      override public function get tabChildren() : Boolean
      {
         return null; //autogenerated
      }
      
      override public function set tabChildren(value:Boolean) : void
      {
      }
      
      [API("670")]
      native public function get allowsFullScreen() : Boolean;
      
      [API("680")]
      native public function get allowsFullScreenInteractive() : Boolean;
      
      native override public function removeChildAt(param1:int) : DisplayObject;
      
      native override public function swapChildrenAt(param1:int, param2:int) : void;
      
      [API("682")]
      native public function get contentsScaleFactor() : Number;
      
      [API("700")]
      native public function get browserZoomFactor() : Number;
      
      native private function requireOwnerPermissions() : void;
      
      [API("674")]
      native public function get displayContextInfo() : String;
      
      public function get constructor() : *
      {
         return null; //autogenerated
      }
      
      public function set constructor(c:*) : *
      {
         return null; //autogenerated
      }
      
      [API("661")]
      public function assignFocus(objectToFocus:InteractiveObject, direction:String) : void
      {
      }
      
      native private function _assignFocus(param1:InteractiveObject, param2:String) : void;
      
      [API("661")]
      public function get nativeWindow() : NativeWindow
      {
         return null; //autogenerated
      }
      
      native private function get _nativeWindow() : NativeWindow;
      
      [Version("air2.0")]
      native public function get orientation() : String;
      
      [Version("air2.0")]
      public function setOrientation(newOrientation:String) : void
      {
      }
      
      native private function set _orientation(param1:String) : void;
      
      [Version("air2.0")]
      public function setAspectRatio(newAspectRatio:String) : void
      {
      }
      
      native private function set _aspectRatio(param1:String) : void;
      
      [Version("air2.0")]
      native public function get deviceOrientation() : String;
      
      [Version("air2.0")]
      native public function get autoOrients() : Boolean;
      
      [API("671")]
      public function set autoOrients(value:Boolean) : void
      {
      }
      
      native private function setAutoOrients(param1:Boolean) : void;
      
      [API("671")]
      native public function get supportedOrientations() : Vector.<String>;
      
      [API("671")]
      private function _supportedOrientations_stubImpl() : Vector.<String>
      {
         return null; //autogenerated
      }
      
      override public function set contextMenu(value:NativeMenu) : void
      {
      }
      
      [API("729")]
      native public function get vsyncEnabled() : Boolean;
      
      [API("729")]
      native public function set vsyncEnabled(param1:Boolean) : void;
   }
}
