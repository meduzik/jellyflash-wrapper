package flash.data
{
   import flash.events.EventDispatcher;
   import flash.net.Responder;
   
   [API("661")]
   [native(instance="SQLStatementObject",methods="auto",cls="SQLStatementClass")]
   [Event(name="error",type="flash.events.SQLErrorEvent")]
   [Event(name="result",type="flash.events.SQLEvent")]
   public class SQLStatement extends EventDispatcher
   {
      
      
      private var _sql:String = "";
      
      private var _params:Object;
      
      private var _finalize:Boolean = false;
      
      public function SQLStatement()
      {
      }
      
      public function get executing() : Boolean
      {
         return null; //autogenerated
      }
      
      native public function get sqlConnection() : SQLConnection;
      
      native public function set sqlConnection(param1:SQLConnection) : void;
      
      native public function get itemClass() : Class;
      
      native public function set itemClass(param1:Class) : void;
      
      public function get parameters() : Object
      {
         return null; //autogenerated
      }
      
      public function get text() : String
      {
         return ""; //autogenerated
      }
      
      public function set text(value:String) : void
      {
      }
      
      public function cancel() : void
      {
      }
      
      public function clearParameters() : void
      {
      }
      
      public function execute(prefetch:int = -1, responder:Responder = null) : void
      {
      }
      
      native public function getResult() : SQLResult;
      
      public function next(prefetch:int = -1, responder:Responder = null) : void
      {
      }
      
      private function checkAllowed() : void
      {
      }
      
      private function checkComplete() : void
      {
      }
      
      private function checkReady() : void
      {
      }
      
      native private function get prepared() : Boolean;
      
      native private function internalCancel() : void;
      
      native private function internalExecute(param1:String, param2:Object, param3:Boolean, param4:int, param5:Responder) : void;
      
      native private function internalNext(param1:int, param2:Responder) : void;
      
      native private function isSQLComplete(param1:String) : Boolean;
      
      native private function isExecuting() : Boolean;
   }
}
