package flash.data
{
   [API("661")]
   public class SQLColumnNameStyle
   {
      
      public static const DEFAULT:String = "default";
      
      public static const LONG:String = "long";
      
      public static const SHORT:String = "short";
      
      
      public function SQLColumnNameStyle()
      {
      }
   }
}
