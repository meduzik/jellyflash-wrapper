package flash.data
{
   [API("661")]
   public class SQLMode
   {
      
      public static const CREATE:String = "create";
      
      public static const READ:String = "read";
      
      public static const UPDATE:String = "update";
      
      
      public function SQLMode()
      {
      }
   }
}
