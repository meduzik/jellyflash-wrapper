package flash.data
{
   import flash.utils.ByteArray;
   
   [API("661")]
   [native(instance="EncryptedLocalStoreObject",methods="auto",cls="EncryptedLocalStoreClass",construct="native")]
   public class EncryptedLocalStore
   {
      
      private static const ENCRYPTEDLOCALSTORE_DATABASE_ACCESS_ERROR = 1;
      
      private static const ENCRYPTEDLOCALSTORE_INTERNAL_ERROR = 2;
      
      private static const ENCRYPTEDLOCALSTORE_OUTOFMEMORY_ERROR = 3;
      
      private static const ENCRYPTEDLOCALSTORE_PUBLISHERIDERROR_SIGINVALID = 4;
      
      private static const ENCRYPTEDLOCALSTORE_PUBLISHERIDERROR_PASSEDIN_PUBID = 5;
      
      private static const ENCRYPTEDLOCALSTORE_APPHASH_COMPUTATION_ERROR = 6;
      
      private static const ENCRYPTEDLOCALSTORE_APPHASH_CHECK_ERROR = 7;
      
      private static const ENCRYPTEDLOCALSTORE_NOT_SUPPORTED_ERROR = 8;
      
      private static const ENCRYPTEDLOCALSTORE_VERSION_MISMATCH = 9;
      
      
      public function EncryptedLocalStore()
      {
      }
      
      public static function setItem(name:String, data:ByteArray, stronglyBound:Boolean = false) : void
      {
      }
      
      public static function getItem(name:String) : ByteArray
      {
         return null; //autogenerated
      }
      
      public static function removeItem(name:String) : void
      {
      }
      
      public static function reset() : void
      {
      }
      
      [Version("air2.0")]
      native public static function get isSupported() : Boolean;
      
      native private static function setItemNative(param1:String, param2:ByteArray, param3:Boolean) : uint;
      
      native private static function getItemNative(param1:String, param2:ByteArray) : uint;
      
      native private static function removeItemNative(param1:String) : uint;
      
      native private static function resetNative() : uint;
      
      private static function processErrorCode(errorCode:uint) : void
      {
      }
      
      private static function checkName(name:String) : void
      {
      }
   }
}
