package flash.html
{
   import flash.display.NativeWindow;
   import flash.display.NativeWindowInitOptions;
   import flash.events.Event;
   import flash.events.NativeWindowBoundsEvent;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   
   [API("661")]
   [native(instance="HTMLPopupWindowObject",methods="IHTMLPopupWindowObject<HTMLPopupWindowObject>",cls="HTMLPopupWindowClass",construct="native")]
   public final class HTMLPopupWindow
   {
      
      private static const MIN_POPUP_HEIGHT:uint = 20;
      
      private static const MIN_POPUP_WIDTH:uint = 50;
      
      private static const MAX_POPUP_HEIGHT:uint = 400;
      
      private static const POPUP_WIDTH_DELTA:uint = 16;
      
      
      private var m_popupWindowWidth:uint;
      
      private var m_ownerHtmlControl:HTMLLoader;
      
      private var m_activePopupWindow:NativeWindow;
      
      private var m_popupContent:XML;
      
      private var m_popupHtmlControl:HTMLLoader;
      
      private var m_allowSelectedItemUpdate:Boolean;
      
      private var m_closePopupWindowIfNeededClosure:Function;
      
      private var m_setDeactivateClosure:Function;
      
      private var m_itemsWidth:Number;
      
      private var m_itemsHeight:Number;
      
      public function HTMLPopupWindow(owner:HTMLLoader, closePopupWindowIfNeededClosure:Function, setDeactivateClosure:Function, computedFontSize:Number)
      {
      }
      
      private function getCSSPropertyAsFloat(element:*, propertyName:String) : Number
      {
         return null; //autogenerated
      }
      
      private function popupClickHandler(itemIndex:int) : void
      {
      }
      
      private function popupItemsRootBorderSize() : Point
      {
         return null; //autogenerated
      }
      
      private function clampToActivePopupWindowLimits(width:Number, height:Number) : Rectangle
      {
         return null; //autogenerated
      }
      
      private function popupCompleteHandler(evt:Event) : void
      {
      }
      
      private function show(windowX:int, windowY:int, popupWindowWidth:int) : void
      {
      }
      
      private function onDeactivate(evt:Event) : void
      {
      }
      
      private function onResize(evt:NativeWindowBoundsEvent) : void
      {
      }
      
      private function onOrientationChange(evt:Event) : void
      {
      }
      
      private function setPopupHtmlControlSize(width:int, height:int) : *
      {
         return null; //autogenerated
      }
      
      private function doDismiss() : void
      {
      }
      
      public function close() : void
      {
      }
      
      public function isActive() : Boolean
      {
         return null; //autogenerated
      }
      
      private function beginPopulate(computedFontSize:Number) : void
      {
      }
      
      private function escapeStringIfNeeded(str:String) : String
      {
         return ""; //autogenerated
      }
      
      private function addOption(optionText:String, itemIndex:int, isSelected:Boolean, isDisabled:Boolean, isChildOfGroup:Boolean) : void
      {
      }
      
      private function addGroupLabel(optionText:String, itemIndex:int, isDisabled:Boolean) : void
      {
      }
      
      private function addSeparator(itemIndex:int) : void
      {
      }
      
      private function updateSelectedItem(itemIndex:int) : void
      {
      }
      
      native private function nativeOnItemClicked(param1:int) : void;
      
      native private function nativeOnPopupClosed() : void;
      
      native private function get nativeHasXMLSerializationBug() : Boolean;
      
      native private function nativeModifyActiveWindowMinWidth(param1:NativeWindow, param2:uint) : void;
      
      native private function createHTMLLoader() : HTMLLoader;
      
      native private function nativeShowPopupControl() : void;
      
      native private function nativeUpdatePopupControlMatrix() : void;
      
      native private function nativeHidePopupControl() : void;
   }
}
