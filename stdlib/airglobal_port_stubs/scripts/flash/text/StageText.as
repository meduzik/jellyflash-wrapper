package flash.text
{
   import flash.display.BitmapData;
   import flash.display.Stage;
   import flash.events.EventDispatcher;
   import flash.geom.Rectangle;
   
   [API("675")]
   [native(instance="StageTextObject",methods="auto",cls="StageTextClass",construct="check")]
   [Event(name="complete",type="flash.events.Event")]
   [Event(name="keyUp",type="flash.events.KeyboardEvent")]
   [Event(name="keyDown",type="flash.events.KeyboardEvent")]
   [Event(name="softKeyboardDeactivate",type="flash.events.SoftKeyboardEvent")]
   [Event(name="softKeyboardActivate",type="flash.events.SoftKeyboardEvent")]
   [Event(name="softKeyboardActivating",type="flash.events.SoftKeyboardEvent")]
   [Event(name="focusOut",type="flash.events.FocusEvent")]
   [Event(name="focusIn",type="flash.events.FocusEvent")]
   [Event(name="change",type="flash.events.Event")]
   public final class StageText extends EventDispatcher
   {
      
      
      public function StageText(initOptions:StageTextInitOptions = null)
      {
      }
      
      native private function init(param1:StageTextInitOptions) : void;
      
      native public function set editable(param1:Boolean) : void;
      
      native public function get editable() : Boolean;
      
      native public function set text(param1:String) : void;
      
      native public function get text() : String;
      
      [API("717")]
      native public function set clearButtonMode(param1:String) : void;
      
      native public function set fontFamily(param1:String) : void;
      
      native public function get fontFamily() : String;
      
      native public function set fontSize(param1:int) : *;
      
      native public function get fontSize() : int;
      
      native public function set color(param1:uint) : *;
      
      native public function get color() : uint;
      
      native public function set fontWeight(param1:String) : void;
      
      native public function get fontWeight() : String;
      
      native public function set fontPosture(param1:String) : void;
      
      native public function get fontPosture() : String;
      
      native public function get displayAsPassword() : Boolean;
      
      native public function set displayAsPassword(param1:Boolean) : void;
      
      native public function set maxChars(param1:int) : void;
      
      native public function get maxChars() : int;
      
      native public function get multiline() : Boolean;
      
      native public function set restrict(param1:String) : void;
      
      native public function get restrict() : String;
      
      native public function set returnKeyLabel(param1:String) : void;
      
      native public function get returnKeyLabel() : String;
      
      native public function set textAlign(param1:String) : void;
      
      native public function get textAlign() : String;
      
      native public function get softKeyboardType() : String;
      
      native public function set softKeyboardType(param1:String) : void;
      
      native public function set autoCapitalize(param1:String) : void;
      
      native public function get autoCapitalize() : String;
      
      native public function set autoCorrect(param1:Boolean) : void;
      
      native public function get autoCorrect() : Boolean;
      
      native public function set locale(param1:String) : void;
      
      native public function get locale() : String;
      
      native public function assignFocus() : void;
      
      native public function selectRange(param1:int, param2:int) : void;
      
      native public function get selectionActiveIndex() : int;
      
      native public function get selectionAnchorIndex() : int;
      
      native public function set stage(param1:Stage) : void;
      
      native public function get stage() : Stage;
      
      native public function set viewPort(param1:Rectangle) : void;
      
      native public function get viewPort() : Rectangle;
      
      native public function drawViewPortToBitmapData(param1:BitmapData) : void;
      
      native public function dispose() : void;
      
      native public function get visible() : Boolean;
      
      native public function set visible(param1:Boolean) : void;
   }
}
