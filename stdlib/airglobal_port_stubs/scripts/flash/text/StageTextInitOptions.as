package flash.text
{
   [API("675")]
   [native(instance="StageTextInitOptionsObject",methods="auto",cls="StageTextInitOptionsClass")]
   public class StageTextInitOptions
   {
      
      
      public function StageTextInitOptions(multiline:Boolean = false)
      {
      }
      
      native public function set multiline(param1:Boolean) : *;
      
      native public function get multiline() : Boolean;
   }
}
