package flash.text
{
   [API("717")]
   public final class StageTextClearButtonMode
   {
      
      public static const NEVER:String = "never";
      
      public static const WHILE_EDITING:String = "whileEditing";
      
      public static const UNLESS_EDITING:String = "unlessEditing";
      
      public static const ALWAYS:String = "always";
      
      
      public function StageTextClearButtonMode()
      {
      }
   }
}
