package flash.text
{
   [API("675")]
   public final class ReturnKeyLabel
   {
      
      public static const DEFAULT:String = "default";
      
      public static const DONE:String = "done";
      
      public static const GO:String = "go";
      
      public static const NEXT:String = "next";
      
      public static const SEARCH:String = "search";
      
      
      public function ReturnKeyLabel()
      {
      }
   }
}
