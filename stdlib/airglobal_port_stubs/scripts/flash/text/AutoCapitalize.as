package flash.text
{
   [API("675")]
   public final class AutoCapitalize
   {
      
      public static const NONE:String = "none";
      
      public static const WORD:String = "word";
      
      public static const SENTENCE:String = "sentence";
      
      public static const ALL:String = "all";
      
      
      public function AutoCapitalize()
      {
      }
   }
}
