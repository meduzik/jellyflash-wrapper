package flash.filters
{
   import flash.display.Shader;
   import flash.geom.Rectangle;
   
   [native(instance="ShaderFilterObject",methods="auto",cls="ShaderFilterClass")]
   [Version("10")]
   public class ShaderFilter extends BitmapFilter
   {
      
      
      public function ShaderFilter(shader:Shader = null)
      {
      }
      
      native public function get shader() : Shader;
      
      native public function set shader(param1:Shader) : void;
      
      public function get leftExtension() : int
      {
         return 0; //autogenerated
      }
      
      public function set leftExtension(v:int) : void
      {
      }
      
      public function get topExtension() : int
      {
         return 0; //autogenerated
      }
      
      public function set topExtension(v:int) : void
      {
      }
      
      public function get rightExtension() : int
      {
         return 0; //autogenerated
      }
      
      public function set rightExtension(v:int) : void
      {
      }
      
      public function get bottomExtension() : int
      {
         return 0; //autogenerated
      }
      
      public function set bottomExtension(v:int) : void
      {
      }
      
      native private function get _extendBy() : Rectangle;
      
      native private function set _extendBy(param1:Rectangle) : void;
   }
}
