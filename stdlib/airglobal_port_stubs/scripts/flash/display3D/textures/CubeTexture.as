package flash.display3D.textures
{
   import flash.display.BitmapData;
   import flash.utils.ByteArray;
   
   [native(instance="TextureCube3DObject",methods="auto",cls="TextureCube3DClass",construct="native")]
   public final class CubeTexture extends TextureBase
   {
      
      
      public function CubeTexture()
      {
      }
      
      [API("674")]
      native public function uploadFromBitmapData(param1:BitmapData, param2:uint, param3:uint = 0) : void;
      
      [API("674")]
      native public function uploadFromByteArray(param1:ByteArray, param2:uint, param3:uint, param4:uint = 0) : void;
      
      [API("674")]
      native public function uploadCompressedTextureFromByteArray(param1:ByteArray, param2:uint, param3:Boolean = false) : void;
   }
}
