package flash.display3D.textures
{
   import flash.display.BitmapData;
   import flash.utils.ByteArray;
   
   [native(instance="TextureRectangle3DObject",methods="auto",cls="TextureRectangle3DClass",construct="native")]
   public final class RectangleTexture extends TextureBase
   {
      
      
      public function RectangleTexture()
      {
      }
      
      [API("690")]
      native public function uploadFromBitmapData(param1:BitmapData) : void;
      
      [API("690")]
      native public function uploadFromByteArray(param1:ByteArray, param2:uint) : void;
      
      [API("719")]
      native public function uploadFromBitmapDataAsync(param1:BitmapData) : void;
      
      [API("719")]
      native public function uploadFromByteArrayAsync(param1:ByteArray, param2:uint) : void;
   }
}
