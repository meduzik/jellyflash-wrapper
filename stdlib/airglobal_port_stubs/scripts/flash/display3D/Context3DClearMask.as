package flash.display3D
{
   public final class Context3DClearMask
   {
      
      public static const COLOR:int;
      
      public static const DEPTH:int;
      
      public static const STENCIL:int;
      
      public static const ALL:int;
      
      
      public function Context3DClearMask()
      {
      }
   }
}
