package flash.media
{
   import flash.events.EventDispatcher;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   
   [API("669")]
   [native(instance="CameraUIObject",methods="auto",cls="CameraUIClass")]
   [Event(name="cancel",type="flash.events.Event")]
   [Event(name="error",type="flash.events.ErrorEvent")]
   [Event(name="permissionStatus",type="flash.events.PermissionEvent")]
   [Event(name="complete",type="flash.events.MediaEvent")]
   public class CameraUI extends EventDispatcher
   {
      
      private static const kInvalidParamError:uint = 2004;
      
      private static const kFeatureNotAvailableError:uint = 2014;
      
      
      private var errorTimer:Timer = null;
      
      public function CameraUI()
      {
      }
      
      native public static function get isSupported() : Boolean;
      
      [API("719")]
      native public static function get permissionStatus() : String;
      
      public function launch(requestedMediaType:String) : void
      {
      }
      
      native private function internalLaunch(param1:String) : void;
      
      [cppcall]
      private function handleUnavailableFeature() : void
      {
      }
      
      private function fireUnavailableError(e:TimerEvent) : void
      {
      }
      
      [API("719")]
      native public function requestPermission() : void;
   }
}
