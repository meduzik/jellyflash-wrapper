package flash.media
{
   [API("669")]
   public final class MediaType
   {
      
      public static const IMAGE:String = "image";
      
      public static const VIDEO:String = "video";
      
      
      public function MediaType()
      {
      }
   }
}
