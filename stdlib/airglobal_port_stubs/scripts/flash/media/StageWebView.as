package flash.media
{
   import flash.display.BitmapData;
   import flash.display.Stage;
   import flash.events.EventDispatcher;
   import flash.geom.Rectangle;
   
   [API("669")]
   [native(instance="StageWebViewObject",methods="auto",cls="StageWebViewClass",construct="check")]
   [Event(name="focusOut",type="flash.events.FocusEvent")]
   [Event(name="focusIn",type="flash.events.FocusEvent")]
   [Event(name="error",type="flash.events.ErrorEvent")]
   [Event(name="complete",type="flash.events.Event")]
   [Event(name="locationChanging",type="flash.events.LocationChangeEvent")]
   [Event(name="locationChange",type="flash.events.LocationChangeEvent")]
   public final class StageWebView extends EventDispatcher
   {
      
      
      public function StageWebView(useNative:Boolean = false, mediaPlaybackRequiresUserAction:Boolean = true)
      {
      }
      
      native public static function get isSupported() : Boolean;
      
      native private function init(param1:Boolean, param2:Boolean) : void;
      
      native public function loadURL(param1:String) : void;
      
      native public function loadString(param1:String, param2:String = "text/html") : void;
      
      native public function stop() : void;
      
      native public function reload() : void;
      
      native public function get isHistoryBackEnabled() : Boolean;
      
      native public function historyBack() : void;
      
      native public function get isHistoryForwardEnabled() : Boolean;
      
      native public function historyForward() : void;
      
      native public function get location() : String;
      
      native public function get title() : String;
      
      native public function get viewPort() : Rectangle;
      
      native public function set viewPort(param1:Rectangle) : void;
      
      native public function get stage() : Stage;
      
      native public function set stage(param1:Stage) : void;
      
      native public function dispose() : void;
      
      native public function assignFocus(param1:String = "none") : void;
      
      [API("671")]
      native public function drawViewPortToBitmapData(param1:BitmapData) : void;
      
      [API("713")]
      native public function get mediaPlaybackRequiresUserAction() : Boolean;
      
      [API("713")]
      native public function set mediaPlaybackRequiresUserAction(param1:Boolean) : void;
   }
}
