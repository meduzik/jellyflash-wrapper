package flash.media
{
   import flash.display.Sprite;
   import flash.display.Stage;
   import flash.events.FocusEvent;
   import flash.events.KeyboardEvent;
   
   [API("669")]
   [native(instance="StageWebViewImplObject",methods="auto",cls="StageWebViewImplClass",construct="native")]
   class StageWebViewImpl extends Sprite
   {
      
      
      function StageWebViewImpl()
      {
      }
      
      private function onKeyFocusChange(evt:FocusEvent) : void
      {
      }
      
      private function onFocusOut(ev:FocusEvent) : void
      {
      }
      
      private function onKeyUp(evt:KeyboardEvent) : void
      {
      }
      
      private function onKeyDown(evt:KeyboardEvent) : void
      {
      }
      
      native private function onNativeFocusOut() : void;
   }
}
