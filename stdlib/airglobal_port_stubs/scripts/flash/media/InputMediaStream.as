package flash.media
{
   import flash.events.EventDispatcher;
   import flash.utils.ByteArray;
   
   [API("671")]
   [native(instance="InputMediaStreamObject",methods="auto",cls="InputMediaStreamClass")]
   [Event(name="complete",type="flash.events.Event")]
   [Event(name="progress",type="flash.events.ProgressEvent")]
   [Event(name="ioError",type="flash.events.IOErrorEvent")]
   [Event(name="close",type="flash.events.Event")]
   class InputMediaStream extends EventDispatcher implements IDataInput
   {
      
      
      function InputMediaStream()
      {
      }
      
      native public function get bytesAvailable() : uint;
      
      native public function get endian() : String;
      
      native public function set endian(param1:String) : void;
      
      native public function get objectEncoding() : uint;
      
      native public function set objectEncoding(param1:uint) : void;
      
      native public function readBoolean() : Boolean;
      
      native public function readByte() : int;
      
      native public function readBytes(param1:ByteArray, param2:uint = 0, param3:uint = 0) : void;
      
      native public function readDouble() : Number;
      
      native public function readFloat() : Number;
      
      native public function readInt() : int;
      
      native public function readMultiByte(param1:uint, param2:String) : String;
      
      native public function readObject() : *;
      
      native public function readShort() : int;
      
      native public function readUnsignedByte() : uint;
      
      native public function readUnsignedInt() : uint;
      
      native public function readUnsignedShort() : uint;
      
      native public function readUTF() : String;
      
      native public function readUTFBytes(param1:uint) : String;
      
      native public function open() : *;
      
      native public function close() : *;
   }
}
