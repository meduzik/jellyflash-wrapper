package flash.media
{
   import flash.geom.Rectangle;
   
   [API("675")]
   [native(instance="CameraRollBrowseOptionsObject",methods="auto",cls="CameraRollBrowseOptionsClass")]
   public class CameraRollBrowseOptions
   {
      
      
      public function CameraRollBrowseOptions()
      {
      }
      
      native public function get width() : Number;
      
      native public function set width(param1:Number) : void;
      
      native public function get height() : Number;
      
      native public function set height(param1:Number) : void;
      
      native public function get origin() : Rectangle;
      
      native public function set origin(param1:Rectangle) : void;
   }
}
