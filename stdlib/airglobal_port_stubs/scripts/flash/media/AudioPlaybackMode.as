package flash.media
{
   [API("675")]
   public final class AudioPlaybackMode
   {
      
      [API("683")]
      public static const AMBIENT:String = "ambient";
      
      public static const MEDIA:String = "media";
      
      public static const VOICE:String = "voice";
      
      
      public function AudioPlaybackMode()
      {
      }
   }
}
