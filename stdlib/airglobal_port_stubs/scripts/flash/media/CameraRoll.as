package flash.media
{
   import flash.display.BitmapData;
   import flash.events.EventDispatcher;
   
   [API("668")]
   [native(instance="CameraRollObject",methods="auto",cls="CameraRollClass")]
   [Event(name="cancel",type="flash.events.Event")]
   [Event(name="select",type="flash.events.MediaEvent")]
   [Event(name="error",type="flash.events.ErrorEvent")]
   [Event(name="permissionStatus",type="flash.events.PermissionEvent")]
   [Event(name="complete",type="flash.events.Event")]
   public class CameraRoll extends EventDispatcher
   {
      
      
      public function CameraRoll()
      {
      }
      
      native public static function get supportsAddBitmapData() : Boolean;
      
      [API("669")]
      native public static function get supportsBrowseForImage() : Boolean;
      
      [API("719")]
      native public static function get permissionStatus() : String;
      
      native public function addBitmapData(param1:BitmapData) : void;
      
      [API("669")]
      native public function browseForImage(param1:CameraRollBrowseOptions = null) : void;
      
      [API("719")]
      native public function requestPermission() : void;
   }
}
