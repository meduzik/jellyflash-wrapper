package flash.geom
{
   [native(instance="PointObject",methods="auto",cls="PointClass",gc="exact")]
   public class Point
   {
      
      
      public var x:Number;
      
      public var y:Number;
      
      public function Point(x:Number = 0, y:Number = 0)
      {
      }
      
      public static function interpolate(pt1:Point, pt2:Point, f:Number) : Point
      {
         return null; //autogenerated
      }
      
      public static function distance(pt1:Point, pt2:Point) : Number
      {
         return null; //autogenerated
      }
      
      public static function polar(len:Number, angle:Number) : Point
      {
         return null; //autogenerated
      }
      
      public function get length() : Number
      {
         return null; //autogenerated
      }
      
      public function clone() : Point
      {
         return null; //autogenerated
      }
      
      public function offset(dx:Number, dy:Number) : void
      {
      }
      
      public function equals(toCompare:Point) : Boolean
      {
         return null; //autogenerated
      }
      
      public function subtract(v:Point) : Point
      {
         return null; //autogenerated
      }
      
      public function add(v:Point) : Point
      {
         return null; //autogenerated
      }
      
      public function normalize(thickness:Number) : void
      {
      }
      
      public function toString() : String
      {
         return ""; //autogenerated
      }
      
      [API("674")]
      public function copyFrom(sourcePoint:Point) : void
      {
      }
      
      [API("674")]
      public function setTo(xa:Number, ya:Number) : void
      {
      }
   }
}
