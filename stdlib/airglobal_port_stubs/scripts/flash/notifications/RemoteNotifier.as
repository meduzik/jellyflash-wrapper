package flash.notifications
{
   import flash.events.EventDispatcher;
   
   [API("683")]
   [native(instance="RemoteNotifierObject",methods="auto",cls="RemoteNotifierClass")]
   [Event(name="status",type="flash.events.StatusEvent")]
   [Event(name="token",type="flash.events.RemoteNotificationEvent")]
   [Event(name="notification",type="flash.events.RemoteNotificationEvent")]
   public class RemoteNotifier extends EventDispatcher
   {
      
      
      public function RemoteNotifier()
      {
      }
      
      native public static function get supportedNotificationStyles() : Vector.<String>;
      
      native public function subscribe(param1:RemoteNotifierSubscribeOptions = null) : void;
      
      native public function unsubscribe() : void;
   }
}
