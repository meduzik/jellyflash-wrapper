package
{
   [native(instance="UninitializedErrorObject",methods="auto",cls="UninitializedErrorClass",gc="exact")]
   public dynamic class UninitializedError extends Error
   {
      
      public static const length:int = 1;
      
      
      public function UninitializedError(message:* = "", id:* = 0)
      {
      }
   }
}
