package
{
   use namespace AS3;
   
   [native(instance="String",classgc="exact",methods="auto",cls="StringClass",construct="override")]
   public final class String
   {
      
      public static const length:int = 1;
      
      
      public function String(value:* = "")
      {
      }
      
      native AS3 static function fromCharCode(... rest) : String;
      
      native private static function _match(param1:String, param2:*) : Array;
      
      native private static function _replace(param1:String, param2:*, param3:*) : String;
      
      native private static function _search(param1:String, param2:*) : int;
      
      native private static function _split(param1:String, param2:*, param3:uint) : Array;
      
      native public function get length() : int;
      
      native private function _indexOf(param1:String, param2:int = 0) : int;
      
      native AS3 function indexOf(param1:String = "undefined", param2:Number = 0) : int;
      
      native private function _lastIndexOf(param1:String, param2:int = 2147483647) : int;
      
      native AS3 function lastIndexOf(param1:String = "undefined", param2:Number = 2147483647) : int;
      
      native AS3 function charAt(param1:Number = 0) : String;
      
      native AS3 function charCodeAt(param1:Number = 0) : Number;
      
      AS3 function concat(... args) : String
      {
         return ""; //autogenerated
      }
      
      native AS3 function localeCompare(param1:* = undefined) : int;
      
      AS3 function match(p:* = undefined) : Array
      {
         return null; //autogenerated
      }
      
      AS3 function replace(p:* = undefined, repl:* = undefined) : String
      {
         return ""; //autogenerated
      }
      
      AS3 function search(p:* = undefined) : int
      {
         return 0; //autogenerated
      }
      
      native private function _slice(param1:int = 0, param2:int = 2147483647) : String;
      
      native AS3 function slice(param1:Number = 0, param2:Number = 2147483647) : String;
      
      AS3 function split(delim:* = undefined, limit:* = 4.294967295E9) : Array
      {
         return null; //autogenerated
      }
      
      native private function _substring(param1:int = 0, param2:int = 2147483647) : String;
      
      native AS3 function substring(param1:Number = 0, param2:Number = 2147483647) : String;
      
      native private function _substr(param1:int = 0, param2:int = 2147483647) : String;
      
      native AS3 function substr(param1:Number = 0, param2:Number = 2147483647) : String;
      
      native AS3 function toLowerCase() : String;
      
      AS3 function toLocaleLowerCase() : String
      {
         return ""; //autogenerated
      }
      
      native AS3 function toUpperCase() : String;
      
      AS3 function toLocaleUpperCase() : String
      {
         return ""; //autogenerated
      }
      
      AS3 function toString() : String
      {
         return ""; //autogenerated
      }
      
      AS3 function valueOf() : String
      {
         return ""; //autogenerated
      }
   }
}
