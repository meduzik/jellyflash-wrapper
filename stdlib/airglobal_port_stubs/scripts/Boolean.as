package
{
   [jfl_native(instance_type="Boolean")]
   public final class Boolean
   {
      
      public static const length:int = 1;
      
      
      public function Boolean(value:* = undefined)
      {
      }
      
      AS3 function toString() : String
      {
         return this ? "true" : "false";
      }
      
      AS3 function valueOf() : Boolean
      {
         return this;
      }
   }
}
