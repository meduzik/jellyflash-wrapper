package
{
   [native(instance="DefinitionErrorObject",methods="auto",cls="DefinitionErrorClass",gc="exact")]
   public dynamic class DefinitionError extends Error
   {
      
      public static const length:int = 1;
      
      
      public function DefinitionError(message:* = "", id:* = 0)
      {
      }
   }
}
