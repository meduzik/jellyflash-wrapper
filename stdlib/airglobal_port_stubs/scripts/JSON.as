package
{
   use namespace AS3;
   
   [native(classgc="exact",methods="auto",cls="JSONClass",construct="none")]
   [API("674")]
   public final class JSON
   {
      
      private namespace as3ns:Namespace = "http://adobe.com/AS3/2006/builtin";
      
      
      public function JSON()
      {
      }
      
      native private static function parseCore(param1:String) : Object;
      
      native private static function stringifySpecializedToString(param1:Object, param2:Array, param3:Function, param4:String) : String;
      
      public static function parse(text:String, reviver:Function = null) : Object
      {
         return null; //autogenerated
      }
      
      public static function stringify(value:Object, replacer:* = null, space:* = null) : String
      {
         return ""; //autogenerated
      }
      
      private static function computePropertyList(r:Array) : Array
      {
         return null; //autogenerated
      }
   }
}
