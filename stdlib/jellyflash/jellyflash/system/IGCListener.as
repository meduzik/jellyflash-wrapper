package jellyflash.system {

	public interface IGCListener {
		function onPreGC():void;
		function onPostGC():void;
	}
	
}