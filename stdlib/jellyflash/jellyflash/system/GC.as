package jellyflash.system {
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author 
	 */
	public class GC {
		public static const Instance:GC = new GC();
		
		private var listeners:Dictionary = new Dictionary(true);
		private var to_notify:Vector.<IGCListener> = new Vector.<IGCListener>();
		
		public function GC() {
		}
		
		public function addListener(listener:IGCListener):void{
			var value:int = listeners[listener];
			listeners[listener] = value + 1;
		}
		
		public function removeListener(listener:IGCListener):void{
			var value:int = listeners[listener];
			if ( value == 1 ){
				delete listeners[listener];
			}else{
				listeners[listener] = value - 1;
			}
		}
		
		native public function hintCollection(threshold:Number):void;
		native public function forceCollection():void;
		
		private function notifyPostGC():void{
			for ( var listener:IGCListener in listeners ){
				to_notify[to_notify.length] = listener;
			}
			for each ( listener in to_notify ){
				listener.onPostGC();
			}
			to_notify.length = 0;
		}
		
		private function notifyPreGC():void{
			for ( var listener:IGCListener in listeners ){
				to_notify[to_notify.length] = listener;
			}
			for each ( listener in to_notify ){
				listener.onPreGC();
			}
			to_notify.length = 0;
		}
	}

}