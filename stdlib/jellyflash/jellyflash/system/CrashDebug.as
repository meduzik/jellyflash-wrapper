package jellyflash.system 
{
	import flash.utils.Dictionary;
	public class CrashDebug 
	{
		public static const Instance:CrashDebug = new CrashDebug();
		private var listeners:Dictionary = new Dictionary(true);
		private var to_notify:Vector.<Function> = new Vector.<Function>();
		
		public function addListener(listener:Function):void{
			var value:int = listeners[listener];
			listeners[listener] = value + 1;
		}
		
		public function removeListener(listener:Function):void{
			var value:int = listeners[listener];
			if ( value == 1 ){
				delete listeners[listener];
			}else{
				listeners[listener] = value - 1;
			}
		}
		
		public function onCrash(err:*):void{
			for ( var listener:Function in listeners ){
				to_notify[to_notify.length] = listener;
			}
			for each ( listener in to_notify ){
				listener(err);
			}
			to_notify.length = 0;
		}
	}

}