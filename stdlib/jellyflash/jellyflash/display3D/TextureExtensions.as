package jellyflash.display3D{
	import flash.display3D.textures.RectangleTexture;
	import flash.display3D.textures.Texture;
	import flash.display3D.textures.TextureBase;
	public class TextureExtensions {
		native public static function UploadTextureFromExternalImage(texture: Texture, image: ExternalImage, mipmap: int): void;
		native public static function UploadRectangleTextureFromExternalImage(texture: RectangleTexture, image: ExternalImage, mipmap: int): void;
	}
}
