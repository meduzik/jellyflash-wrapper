package jellyflash.display3D{
	public class ExternalImage {
		private var url:String;
		private var handle: int;
		private var loaded: Boolean;
		private var onSuccess: Function;
		private var onError: Function;
		private var _width: int;
		private var _height: int;
		
		private function ExternalImage(url: String, onSuccess: Function, onError: Function) {
			this.url = url;
			this.onSuccess = onSuccess;
			this.onError = onError;
		}
		
		public function dispose(): void {
			_dispose();
			disposeCallbacks();
			loaded = false;
			handle = 0;
		}
		
		private function reportSuccess(): void {
			loaded = true;
			onSuccess();
		}
		
		private function reportError(message: String): void {
			onError(message);
		}
		
		private function disposeCallbacks():void {
			onSuccess = null;
			onError = null;
		}
		
		public static function Load(url: String, onSuccess: Function, onError: Function): ExternalImage {
			var image: ExternalImage = new ExternalImage(url, onSuccess, onError);
			image.load();
			return image;
		}
		
		public function get width():int {
			return _width;
		}
		
		public function get height():int {
			return _height;
		}
		
		private native function load(): void;
		private native function _dispose(): void;
	}
}
