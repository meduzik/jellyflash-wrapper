package com.meduzik.audio {
	public class MusicPlayer {		
		public static const TRACK_COMPLETE_FADED: String = "track_complete_faded";
		public static const TRACK_COMPLETE_FINISHED: String = "track_complete_finished";
		public static const TRACK_COMPLETE_NOT_STARTED: String = "track_complete_not_started";
		public static const TRACK_COMPLETE_ERROR: String = "track_complete_error";
		
		public function MusicPlayer() {
		}
		
		native public function set_volume(volume: Number): void;
		native public function play_track(uri: String, on_complete: Function): void;
		native public function stop(): void;
		
		private static var _Instance: MusicPlayer;
		
		public static function get Instance(): MusicPlayer {
			if (!_Instance) {
				_Instance = new MusicPlayer();
			}
			return _Instance;
		}
	}
}
