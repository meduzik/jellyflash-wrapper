package
{
   use namespace AS3;
   
   [jfl_native()]
   public final class int
   {
      
      public static const MIN_VALUE:int = -2147483648;
      
      public static const MAX_VALUE:int = 2147483647;
      
      native public function int(value:* = 0);
      
      native AS3 function toString(radix:* = 10) : String;
      native AS3 function valueOf() : int;
      native AS3 function toExponential(p:* = 0) : String;
      native AS3 function toPrecision(p:* = 0) : String;
      native AS3 function toFixed(p:* = 0) : String;
   }
}
