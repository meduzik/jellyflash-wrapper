package
{
	use namespace AS3;
	
	[native(classgc="exact",methods="auto",cls="JSONClass",construct="none")]
	[API("674")]
	public final class JSON
	{
		public function JSON()
		{
			super();
		}
		
		native private static function parseCore(string:String) : Object;
		native private static function stringifyCore(object:Object, space:String) : String;
		
		public static function parse(text:String, reviver:Function = null) : Object
		{
			if( text == null )
			{
				Error.throwError(SyntaxError,1132);
			}
			var unfiltered:Object = parseCore(text);
			if( reviver == null )
			{
				return unfiltered;
			}
			Error.throwError(TypeError,1131);
			return null;
		}
		
		public static function stringify(value:Object, replacer:* = null, space:* = null) : String
		{
			if( replacer != null )
			{
				Error.throwError(TypeError,1131);
			}
			if ( space is String ){
				space = (space as String).substr(0, 10);
			}else if ( space is Number ){
				space = "          ".substr(0, space);
			}else if ( space == null ){
				space = "";
			}
			return stringifyCore(value, space);
		}
	}
}
