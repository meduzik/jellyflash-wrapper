package 
{
   use namespace AS3;
   
   final dynamic class Vector
   {
      function Vector(length:uint = 0, fixed:Boolean = false)
      {
      }
      
      native public function get length() : uint;
      
      native public function set length(param1:uint) : void;
      
      native public function set fixed(param1:Boolean) : void;
      
      native public function get fixed() : Boolean;
      
      AS3 function toString() : String
      {
         return this.join();
      }
	  
      native AS3 function toLocaleString() : String;
      native AS3 function join(separator:String = ",") : String;
      native AS3 function every(checker:Function, thisObj:Object = null) : Boolean;
      native AS3 function forEach(eacher:Function, thisObj:Object = null) : void;
      native AS3 function map(mapper:Function, thisObj:Object = null) : *;
      native AS3 function push(... rest) : uint;
      native AS3 function some(checker:*, thisObj:Object = null) : Boolean;      
      native AS3 function unshift(... rest) : uint;
      native AS3 function concat(... items) : Vector;      
      native AS3 function filter(checker:Function, thisObj:Object = null) : Vector;
      native AS3 function pop() : $$;
      native AS3 function reverse() : Vector;
      native AS3 function shift() : $$;
      native AS3 function slice(start:int = 0, end:int = 2147483647) : Vector;
      native AS3 function sort(comparefn:*) : Vector;
      native AS3 function splice(start:int, deleteCount:int, ... items) : Vector;
      native AS3 function insertAt(param1:int, param2:$$) : void;
      native AS3 function removeAt(param1:int) : $$;
      native AS3 function indexOf(value:$$, from:int = 0) : int;
      native AS3 function lastIndexOf(value:$$, from:int = 2147483647) : int;
   }
}
