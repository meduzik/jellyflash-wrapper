package
{
   [native(instance="TypeErrorObject",methods="auto",cls="TypeErrorClass",gc="exact")]
   public dynamic class TypeError extends Error
   {
      public function TypeError(message:* = "", id:* = 0)
      {
         super(message,id);
         this.name = "TypeError";
      }
   }
}
