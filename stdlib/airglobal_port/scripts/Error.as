package
{
	import flash.utils.getQualifiedClassName;

	[native(instance="ErrorObject",methods="auto",cls="ErrorClass",gc="exact")]
	public dynamic class Error
	{
		private static const ParamRegexp:RegExp = new RegExp("%[0-9]", "g"); ///%[0-9]/g
		
		public var message;
		
		public var name;
		
		private var _errorID:int;
		
		public function Error(message:* = "", id:* = 0)
		{
			super();
			this.message = message;
			this._errorID = id;
			this.name = getQualifiedClassName(this);
		}
		
		public static function getErrorMessage(param1:int) : String{
			return "ERR" + String(param1);
		}
		
		public static function throwError(type:Class, index:uint, ... _rest) : *
		{
			var i:* = 0;
			var rest:Array = _rest as Array;
			var f:* = function(match:String, pos:*, string:*):*{
				var arg_num:* = -1;
				switch(match.charAt(1))
				{
					case "1":
						arg_num = 0;
						break;
					case "2":
						arg_num = 1;
						break;
					case "3":
						arg_num = 2;
						break;
					case "4":
						arg_num = 3;
						break;
					case "5":
						arg_num = 4;
						break;
					case "6":
						arg_num = 5;
				}
				if(arg_num > -1 && rest.length > arg_num)
				{
					return rest[arg_num];
				}
				return "";
			};
			throw new type(Error.getErrorMessage(index).replace(ParamRegexp,f),index);
		}
		
		native public function getStackTrace() : String;
		
		public function get errorID() : int
		{
			return this._errorID;
		}
	}
}
