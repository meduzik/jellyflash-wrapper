package
{
	use namespace AS3;
	
	[native(instance="XMLListObject",methods="auto",cls="XMLListClass",construct="override",gc="exact")]
	public final dynamic class XMLList
	{
		public function XMLList(value:* = undefined)
		{
			super();
		}
		
		native AS3 function toString() : String;
		
		AS3 function valueOf() : XMLList
		{
			return this;
		}
		
		native AS3 function attribute(param1:*) : XMLList;
		
		native AS3 function attributes() : XMLList;
		
		native AS3 function child(param1:*) : XMLList;
		
		native AS3 function children() : XMLList;
		
		native AS3 function comments() : XMLList;
		
		native AS3 function contains(param1:*) : Boolean;
		
		native AS3 function copy() : XMLList;
		
		native AS3 function descendants(param1:* = "*") : XMLList;
		
		native AS3 function elements(param1:* = "*") : XMLList;
		
		native AS3 function hasComplexContent() : Boolean;
		
		native AS3 function hasSimpleContent() : Boolean;
		
		native AS3 function length() : int;
		
		native AS3 function name() : Object;
		
		native AS3 function normalize() : XMLList;
		
		native AS3 function parent() : *;
		
		native AS3 function processingInstructions(param1:* = "*") : XMLList;
		
		native AS3 function text() : XMLList;
		
		native AS3 function toXMLString() : String;
		
		native AS3 function addNamespace(param1:*) : XML;
		
		native AS3 function appendChild(param1:*) : XML;
		
		native AS3 function childIndex() : int;
		
		native AS3 function inScopeNamespaces() : Array;
		
		native AS3 function insertChildAfter(param1:*, param2:*) : *;
		
		native AS3 function insertChildBefore(param1:*, param2:*) : *;
		
		native AS3 function nodeKind() : String;
		
		native private function _namespace(param1:*, param2:int) : *;
		
		AS3 function namespace(prefix:* = null) : *
		{
			return null;
		}
		
		native AS3 function localName() : Object;
		
		native AS3 function namespaceDeclarations() : Array;
		
		native AS3 function prependChild(param1:*) : XML;
		
		native AS3 function removeNamespace(param1:*) : XML;
		
		native AS3 function replace(param1:*, param2:*) : XML;
		
		native AS3 function setChildren(param1:*) : XML;
		
		native AS3 function setLocalName(param1:*) : void;
		
		native AS3 function setName(param1:*) : void;
		
		native AS3 function setNamespace(param1:*) : void;
		
		AS3 function toJSON(k:String) : *
		{
			return this.toJSON(k);
		}
	}
}
