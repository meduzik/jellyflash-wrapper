package
{
   use namespace AS3;
   
   [jfl_native()]
   public final class uint
   {
      
      public static const MIN_VALUE:uint = 0;
      
      public static const MAX_VALUE:uint = 4294967295;
      
      native public function uint(value:* = 0);      
      native AS3 function toString(radix:* = 10) : String;
      native AS3 function valueOf() : uint;      
      native AS3 function toExponential(p:* = 0) : String;
      native AS3 function toPrecision(p:* = 0) : String;
      native AS3 function toFixed(p:* = 0) : String;
   }
}
