package
{
	[jfl_native(invoke="Invoke", extra_gc=true, destructor=true, iter=false, dyn=false, payload="Array_PAYLOAD")]
	public final dynamic class Array
	{
		public static const CASEINSENSITIVE:uint = 1;
		
		public static const DESCENDING:uint = 2;
		
		public static const UNIQUESORT:uint = 4;
		
		public static const RETURNINDEXEDARRAY:uint = 8;
		
		public static const NUMERIC:uint = 16;
		
		private static const SmallArraySize:int = 10;
		
		public function Array(... args)
		{
			_init();
			var dlen:Number = NaN;
			var ulen:uint = 0;
			var i:uint = 0;
			super();
			var n:uint = args.length;
			if(n == 1 && args[0] is Number)
			{
				dlen = args[0];
				ulen = dlen;
				if(ulen != dlen)
				{
					Error.throwError(RangeError,1005,dlen);
				}
				this.length = ulen;
			}
			else
			{
				for(i = 0; i < n; i++)
				{
					this[i] = args[i];
				}
			}
		}
	  
		public static function Invoke(... args): Array{
			var arr:Array = new Array();
			var dlen:Number = NaN;
			var ulen:uint = 0;
			var i:uint = 0;
			var n:uint = args.length;
			if(n == 1 && args[0] is Number)
			{
				dlen = args[0];
				ulen = dlen;
				if(ulen != dlen)
				{
					Error.throwError(RangeError,1005,dlen);
				}
				arr.length = ulen;
			}
			else
			{
				arr.length = n;
				for(i = 0; i < n; i++)
				{
					arr[i] = args[i];
				}
			}
			return arr;
		}
		
		native private function _init():void;
		
		/*
		[API("708")]
		native AS3 function insertAt(param1:int, param2:*) : void;
		
		[API("708")]
		native AS3 function removeAt(param1:int) : *;
		*/
		native public function get length() : uint;
		
		native public function set length(param1:uint) : void;
		
		AS3 function join(sep:* = undefined) : String
		{
			var x:* = undefined;
			var s:String = sep === undefined?",":String(sep);
			var out:String = "";
			for(var i:uint = 0, n:uint = length; i < n; i++)
			{
				x = this[i];
				if(x != null)
				{
					out = out + x;
				}
				if(i + 1 < n)
				{
					out = out + s;
				}
			}
			return out;
		}
		
		native AS3 function pop() : *;
		
		native AS3 function push(... rest) : uint;
		
		AS3 function reverse() : Array
		{
			var n: int = length;
			var m: int = n / 2;
			for ( var i:int = 0; i < m; i++ ){
				var tmp:* = this[i];
				this[i] = this[n - i - 1];
				this[n - i - 1] = tmp;
			}
			return this;
		}
		
		AS3 function concat(... args) : Array
		{
			var r:Array = [];
			for ( var j:int = 0; j < length; j++ ){
				r.push(this[j]);
			}
			
			for ( var i:int = 0; i < args.length; i++ ){
				var arg:* = args[i];
				if ( arg is Array ){
					var merge:Array = arg as Array;
					for ( j = 0; j < merge.length; j++ ){
						r.push(merge[j]);
					}
				}else{
					r.push(arg);
				}
			}
			return r;
		}
		
		AS3 function slice(A:uint = 0, B:uint = 0x7fffffff) : Array
		{
			var res:Array = [];
			if ( B > length ){
				B = length;
			}
			for ( var i:uint = A; i < B; i++ ){
				res.push(this[i]);
			}
			return res;
		}
		
		/*
		native AS3 function unshift(... rest) : uint;
		
		native private static function _shift(param1:*) : *;
		native private static function _splice(param1:*, param2:Array) : Array;
		
		AS3 function shift() : *
		{
			return _shift(this);
		}
		AS3 function splice(... args) : *
		{
			if(!args.length)
			{
				return undefined;
			}
			return _splice(this,args);
		}
		*/
		
		AS3 function sort(comparator:Function = null, opts:int = 0) : *{
			if ( opts & (CASEINSENSITIVE | RETURNINDEXEDARRAY | UNIQUESORT) ){
				throw new Error("unsupported sort options");
			}
			if ( !comparator ){
				if ( opts & NUMERIC ){
					comparator = CompareNum;
				}else{
					comparator = CompareStr;
				}
			}
			var n:int = length;
			if ( n >= 30 ) {
				qsort(comparator, 0, n);
			}else {
				insertion_sort(comparator, 0, n);
			}
			return this;
		}
		
		private static function CompareStr(x:*, y:*):Number{
			var xval:String = String(x);
			var yval:String = String(y);
			if ( xval < yval ){
				return -1;
			}
			if ( xval > yval ){
				return 1;
			}
			return 0;
		}
		
		private static function CompareNum(x:*, y:*):Number{
			var xval:Number = Number(x);
			var yval:Number = Number(y);
			if ( xval < yval ){
				return -1;
			}
			if ( xval > yval ){
				return 1;
			}
			return 0;
		}
		
		private function qsort(comparator:Function, begin:int, end:int):void 
		{
		tailcall:
			var x1:* = this[begin];
			var x2:* = this[(begin + end) >> 1];
			var x3:* = this[end - 1];
			
			var c1:Boolean = comparator(x1, x2) < 0;	
			var c3:Boolean = comparator(x2, x3) < 0;
			
			var pivot:*;
			
			if ( c1 == c3 ) {
				pivot = x2;
			}else{
				var c2:Boolean = comparator(x1, x3) < 0;
				if ( c2 != c3 ) {
					pivot = x3;
				}else {
					pivot = x1;
				}
			}
			
			var low:int = begin;
			var mid:int = begin;
			var high:int = end;
			for ( var i:int = begin; i < high; i++ ) {
				var cmp:Number = comparator(this[i], pivot);
				if ( cmp < 0 ) {
					var tmp:* = this[low];
					this[low] = this[i];
					this[i] = tmp;
					
					low++;
				}else if ( cmp > 0 ) {
					--high;
					tmp = this[high];
					this[high] = this[i];
					this[i] = tmp;
					i--;
				}
			}
			
			var first:int = low - begin;
			var second:int = end - high;
			
			if ( first <= 8 ) {
				insertion_sort(comparator, begin, low);
				if ( second <= 8 ){
					insertion_sort(comparator, high, end);
				}else {
					begin = high;
					goto tailcall;
				}
			}else if ( second <= 8 ) {
				insertion_sort(comparator, high, end);				
				end = low;
				goto tailcall;
			}else {
				if ( first < second ) {
					qsort(comparator, begin, low);
					begin = high;
				}else {
					qsort(comparator, high, end);
					end = low;
				}
				goto tailcall;
			}
		}

		private function insertion_sort(comparator:Function, begin:int, end:int):void 
		{
			var n:int = end - begin;
			for ( var i:int = 0; i < n; i++ ) {
				var j:int = i + begin;
				var x:* = this[j];
				while ( j > begin && comparator(x, this[j - 1]) < 0 ) {
					this[j] = this[j - 1];
					j--;
				}
				this[j] = x;
			}
		}
		
		/*
		AS3 function sortOn(names:*, options:* = 0, ... ignored) : *
		{
			return _sortOn(this,names,options);
		}
		*/
		
		AS3 function indexOf(searchElement:*, fromIndex:* = 0) : int
		{
			var n:int = length;
			for (var i:int = (fromIndex >= 0 ? fromIndex : 0); i < n; i++){
				if (this[i] == searchElement){
					return i;
				}
			}
			return -1;
		}
		
		AS3 function lastIndexOf(searchElement:*, fromIndex:* = 2147483647) : int
		{
			var n:int = length;
			if ( fromIndex < n ){
				n = fromIndex;
			}
			for (var i:int = n - 1; i >= 0; i--){
				if (this[i] == searchElement){
					return i;
				}
			}
			return -1;
		}
		
		/*
		AS3 function every(callback:Function, thisObject:* = null) : Boolean
		{
			return _every(this,callback,thisObject);
		}
		
		AS3 function filter(callback:Function, thisObject:* = null) : Array
		{
			return _filter(this,callback,thisObject);
		}
		
		AS3 function forEach(callback:Function, thisObject:* = null) : void
		{
			_forEach(this,callback,thisObject);
		}
		
		AS3 function map(callback:Function, thisObject:* = null) : Array
		{
			return _map(this,callback,thisObject);
		}
		
		AS3 function some(callback:Function, thisObject:* = null) : Boolean
		{
			return _some(this,callback,thisObject);
		}
		*/
	}
}
