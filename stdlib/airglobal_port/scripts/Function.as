package
{
   use namespace AS3;
   
   [jfl_native(payload="Function_PAYLOAD")]
   final public class Function
   {
      
      public function Function()
      {
         super();
      }
      
      native public function get length() : int;
      
      native AS3 function call(param1:* = undefined, ... rest) : *;
      native AS3 function apply(param1:* = undefined, param2:* = undefined) : *;
   }
}
