package
{
   [native(instance="SecurityErrorObject",methods="auto",cls="SecurityErrorClass",gc="exact")]
   public dynamic class SecurityError extends Error
   {
      public function SecurityError(message:* = "", id:* = 0)
      {
         super(message,id);
         this.name = "SecurityError";
      }
   }
}
