package
{
	use namespace AS3;
	
	[jfl_native(payload="RegExp_PAYLOAD")]
	public class RegExp
	{
		private static const FlagGlobal:int = 1 << 0;
		private static const FlagIgnoreCase:int = 1 << 1;
		private static const FlagMultiline:int = 1 << 2;
		private static const FlagDotAll:int = 1 << 3;
		private static const FlagExtended:int = 1 << 4;
	
		private var _lastIndex:int = 0;
		private var _source:String;
		private var _flags:int = 0;
	
		public function RegExp(pattern:String, options:String = null)
		{
			_source = pattern;
			init(pattern, options);
		}
		
		native private function init(pattern:String, options:String):void;
		
		public function get source() : String{
			return _source;
		}
		
		public function get global() : Boolean{
			return _flags & FlagGlobal;
		}
		
		public function get ignoreCase() : Boolean{
			return _flags & FlagIgnoreCase;
		}
		
		public function get multiline() : Boolean{
			return _flags & FlagMultiline;
		}
		
		public function get lastIndex() : int{
			return _lastIndex;
		}
		
		public function set lastIndex(value:int) : void{
			_lastIndex = value;
		}
		
		public function get dotall() : Boolean{
			return _flags & FlagDotAll;
		}
		
		public function get extended() : Boolean{
			return _flags & FlagExtended;
		}
		
		native AS3 function exec(string:String) : *;
		native AS3 function test(string:String) : Boolean;
	}
}
