package
{
   import flash.display.Sprite;
   import flash.system.*;
   
   [ExcludeClass]
   public class _63696af7d6bba4d5e9bc029be0dceb12f07487544eafec6eeeb3f4b63d0a95a3_flash_display_Sprite extends Sprite
   {
       
      
      public function _63696af7d6bba4d5e9bc029be0dceb12f07487544eafec6eeeb3f4b63d0a95a3_flash_display_Sprite()
      {
         super();
      }
      
      public function allowDomainInRSL(... rest) : void
      {
         Security.allowDomain(rest);
      }
      
      public function allowInsecureDomainInRSL(... rest) : void
      {
         Security.allowInsecureDomain(rest);
      }
   }
}
