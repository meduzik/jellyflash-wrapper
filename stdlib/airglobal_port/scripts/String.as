package
{
	use namespace AS3;
	
	[jfl_native(payload="String_PAYLOAD")]
	public final class String
	{
		public function String()
		{
		}
		
		native public function get length() : int;
		
		native AS3 static function fromCharCode(... rest) : String;
		
		native AS3 function indexOf(param1:String = "undefined", param2:Number = 0) : int;
		
		native AS3 function lastIndexOf(param1:String = "undefined", param2:Number = 2147483647) : int;
		
		native AS3 function charAt(param1:Number = 0) : String;
		
		native AS3 function charCodeAt(param1:Number = 0) : Number;
		
		AS3 function concat(... args) : String
		{
			var s:String = this;
			for(var i:uint = 0, n:uint = args.length; i < n; i++)
			{
				s = s + String(args[i]);
			}
			return s;
		}
		
		native AS3 function localeCompare(param1:* = undefined) : int;
		
		native private static function _match(param1:String, param2:*) : Array;
		AS3 function match(p:* = undefined) : Array
		{
			return _match(this,p);
		}
		
		native private static function _replace(param1:String, param2:*, param3:*) : String;
		AS3 function replace(p:* = undefined, repl:* = undefined) : String
		{
			return _replace(this,p,repl);
		}
		
		native private static function _search(param1:String, param2:*) : int;
		AS3 function search(p:* = undefined) : int
		{
			return _search(this,p);
		}
		
		native private static function _split(param1:String, param2:*, param3:uint) : Array;
		AS3 function split(delim:* = undefined, limit:* = 4.294967295E9) : Array
		{
			if(limit == undefined)
			{
				limit = 4294967295;
			}
			return _split(this,delim,limit);
		}
		
		native AS3 function substring(param1:Number = 0, param2:Number = 2147483647) : String;
		native AS3 function substr(param1:Number = 0, param2:Number = 2147483647) : String;
		native AS3 function slice(param1:Number = 0, param2:Number = 2147483647) : String;
		
		native AS3 function toLowerCase() : String;
		native AS3 function toUpperCase() : String;
		
		AS3 function toLocaleLowerCase() : String
		{
			return this.toLowerCase();
		}
		
		AS3 function toLocaleUpperCase() : String
		{
			return this.toUpperCase();
		}
		
		AS3 function toString() : String
		{
			return this;
		}
		
		AS3 function valueOf() : String
		{
			return this;
		}
	}
}
