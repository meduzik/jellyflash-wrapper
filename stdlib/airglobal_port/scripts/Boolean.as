package
{
   [jfl_native(payload="Boolean_PAYLOAD")]
   public final class Boolean
   {
      native public function Boolean(value:* = undefined);
      
      AS3 function toString() : String
      {
         return !!this?"true":"false";
      }
      
      AS3 function valueOf() : Boolean
      {
         return this;
      }
   }
}
