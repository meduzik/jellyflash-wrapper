package flash.permissions
{
   public final class PermissionStatus
   {
      
      public static const UNKNOWN:String = "unknown";
      
      public static const GRANTED:String = "granted";
      
      public static const DENIED:String = "denied";
      
      public static const ONLY_WHEN_IN_USE:String = "onlyWhenInUse";
       
      
      public function PermissionStatus()
      {
         super();
      }
   }
}
