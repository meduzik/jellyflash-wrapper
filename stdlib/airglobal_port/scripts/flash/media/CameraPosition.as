package flash.media
{
   [API("675")]
   public final class CameraPosition
   {
      
      public static const FRONT:String = "front";
      
      public static const BACK:String = "back";
      
      public static const UNKNOWN:String = "unknown";
       
      
      public function CameraPosition()
      {
         super();
      }
   }
}
