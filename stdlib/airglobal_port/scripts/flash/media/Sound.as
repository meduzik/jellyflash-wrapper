package flash.media
{
	import flash.events.EventDispatcher;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	[jfl_native(destructor=true, payload="jfl_flash_media_Sound_PAYLOAD")]
	[Event(name="progress",type="flash.events.ProgressEvent")]
	[Event(name="open",type="flash.events.Event")]
	[Event(name="ioError",type="flash.events.IOErrorEvent")]
	[Event(name="id3",type="flash.events.Event")]
	[Event(name="complete",type="flash.events.Event")]
	[Event(name="sampleData",type="flash.events.SampleDataEvent")]
	public class Sound extends EventDispatcher
	{
		public function Sound(stream:URLRequest = null, context:SoundLoaderContext = null)
		{
			super();
			init();
			this.load(stream,context);
		}
		
		native private function init(): void;
		
		public function load(stream:URLRequest, context:SoundLoaderContext = null) : void
		{
			if (stream) {
				throw new Error("Sound load not implemented");
			}
		}

		native public function play(startTime:Number = 0, loops:int = 0, sndTransform:SoundTransform = null):SoundChannel;
		
		[API("674")]
		native public function loadCompressedDataFromByteArray(data:ByteArray, dataLength:uint) : void;

		native public function get bytesLoaded() : uint;
		
		native public function get bytesTotal() : int;
		
		native public function get id3() : ID3Info;
		
		native public function close() : void;
	}
}
