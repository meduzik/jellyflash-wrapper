package flash.media
{
   [API("724")]
   public final class AudioOutputChangeReason
   {
      
      public static const USER_SELECTION:String = "userSelection";
      
      public static const DEVICE_CHANGE:String = "deviceChange";
       
      
      public function AudioOutputChangeReason()
      {
         super();
      }
   }
}
