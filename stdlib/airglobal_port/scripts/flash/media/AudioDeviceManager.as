package flash.media
{
   import flash.events.EventDispatcher;
   
   [native(instance="AudioDeviceManagerObject",methods="auto",cls="AudioDeviceManagerClass",construct="native")]
   [API("724")]
   [Event(name="audioOutputChange",type="flash.events.AudioOutputChangeEvent")]
   public final class AudioDeviceManager extends EventDispatcher
   {
       
      
      public function AudioDeviceManager()
      {
         super();
      }
      
      public static function get audioDeviceManager() : AudioDeviceManager
      {
         return getInstance();
      }
      
      native private static function getInstance() : AudioDeviceManager;
      
      native public static function get isSupported() : Boolean;
      
      native public function get deviceNames() : Array;
      
      native public function get selectedDeviceIndex() : int;
      
      native public function set selectedDeviceIndex(param1:int) : void;
   }
}
