package flash.media
{
	import flash.utils.ByteArray;
	
	public final class SoundMixer
	{
		private static const transform: SoundTransform = new SoundTransform();
		
		public function SoundMixer()
		{
			super();
		}
		
		native public static function stopAll() : void;
		
		native public static function computeSpectrum(param1:ByteArray, param2:Boolean = false, param3:int = 0) : void;
		
		native public static function get bufferTime() : int;
		
		native public static function set bufferTime(param1:int) : void;
		
		public static function get soundTransform() : SoundTransform {
			return transform.clone();
		}
		
		public static function set soundTransform(param1:SoundTransform) : void {
			transform.copyFrom(param1);
			updateTransform();
		}
		
		native private static function updateTransform(): void;
		
		native public static function areSoundsInaccessible() : Boolean;
		
		[API("675")]
		native public static function get audioPlaybackMode() : String;
		
		[API("675")]
		native public static function set audioPlaybackMode(param1:String) : void;
		
		[API("675")]
		native public static function get useSpeakerphoneForVoice() : Boolean;
		
		[API("675")]
		native public static function set useSpeakerphoneForVoice(param1:Boolean) : void;
	}
}
