package flash.media
{
   import flash.display.Sprite;
   import flash.display.Stage;
   import flash.events.FocusEvent;
   import flash.events.KeyboardEvent;
   
   [API("669")]
   [native(instance="StageWebViewImplObject",methods="auto",cls="StageWebViewImplClass",construct="native")]
   class StageWebViewImpl extends Sprite
   {
       
      
      function StageWebViewImpl()
      {
         super();
         focusRect = false;
         tabIndex = -1;
         tabEnabled = true;
         addEventListener(FocusEvent.KEY_FOCUS_CHANGE,this.onKeyFocusChange);
         addEventListener(FocusEvent.FOCUS_OUT,this.onFocusOut);
         addEventListener(KeyboardEvent.KEY_DOWN,this.onKeyDown,false,int.MAX_VALUE);
         addEventListener(KeyboardEvent.KEY_UP,this.onKeyUp,false,int.MAX_VALUE);
      }
      
      private function onKeyFocusChange(evt:FocusEvent) : void
      {
         evt.preventDefault();
      }
      
      private function onFocusOut(ev:FocusEvent) : void
      {
         if(ev.relatedObject && contains(ev.relatedObject))
         {
            if(stage)
            {
               stage.focus = this;
            }
         }
         else
         {
            this.onNativeFocusOut();
         }
      }
      
      private function onKeyUp(evt:KeyboardEvent) : void
      {
         var stage:Stage = null;
         if(evt.keyCode == Keyboard.BACK || evt.keyCode == Keyboard.MENU || evt.keyCode == Keyboard.SEARCH)
         {
            stage = this.stage;
            if(stage)
            {
               stage.dispatchEvent(evt);
            }
            evt.stopImmediatePropagation();
         }
      }
      
      private function onKeyDown(evt:KeyboardEvent) : void
      {
         var stage:Stage = null;
         var handled:Boolean = false;
         if(evt.keyCode == Keyboard.BACK || evt.keyCode == Keyboard.MENU || evt.keyCode == Keyboard.SEARCH)
         {
            stage = this.stage;
            if(stage)
            {
               handled = stage.dispatchEvent(evt);
            }
            evt.stopImmediatePropagation();
            if(!handled)
            {
               evt.preventDefault();
            }
         }
      }
      
      native private function onNativeFocusOut() : void;
   }
}
