package flash.media
{
   import flash.events.ErrorEvent;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import flash.events.IOErrorEvent;
   import flash.events.ProgressEvent;
   import flash.filesystem.File;
   import flash.utils.IDataInput;
   
   [API("669")]
   [native(instance="MediaPromiseObject",methods="auto",cls="MediaPromiseClass",construct="native")]
   [Event(name="complete",type="flash.events.Event")]
   [Event(name="progress",type="flash.events.ProgressEvent")]
   [Event(name="ioError",type="flash.events.IOErrorEvent")]
   [Event(name="close",type="flash.events.Event")]
   public class MediaPromise extends EventDispatcher implements IFilePromise
   {
       
      
      private var _stream:IDataInput;
      
      public function MediaPromise()
      {
         super();
      }
      
      public function get isAsync() : Boolean
      {
         return true;
      }
      
      public function open() : IDataInput
      {
         var _inputMediaStream:InputMediaStream = null;
         var _fileuri:String = this.getURI();
         if(_fileuri)
         {
            this._stream = new FileStream() as IDataInput;
            this.registerEventHandlers();
            (this._stream as FileStream).openAsync(new File(_fileuri),FileMode.READ);
            return this._stream;
         }
         _inputMediaStream = this.getMediaStreamObject();
         if(_inputMediaStream)
         {
            this._stream = _inputMediaStream;
            this.registerEventHandlers();
            _inputMediaStream.open();
            return this._stream;
         }
         return null;
      }
      
      public function close() : void
      {
         if(this._stream is InputMediaStream)
         {
            (this._stream as InputMediaStream).close();
         }
         else if(this._stream is FileStream)
         {
            (this._stream as FileStream).close();
         }
      }
      
      public function reportError(e:ErrorEvent) : void
      {
         dispatchEvent(e);
      }
      
      public function get file() : File
      {
         var _fileuri:String = this.getURI();
         if(_fileuri)
         {
            return new File(_fileuri);
         }
         return null;
      }
      
      private function registerEventHandlers() : void
      {
         var _iED:IEventDispatcher = null;
         if(this._stream)
         {
            _iED = this._stream as IEventDispatcher;
            _iED.addEventListener(Event.COMPLETE,this.reflectURLStreamEvent);
            _iED.addEventListener(ProgressEvent.PROGRESS,this.reflectURLStreamEvent);
            _iED.addEventListener(IOErrorEvent.IO_ERROR,this.reflectURLStreamEvent);
            _iED.addEventListener(Event.CLOSE,this.reflectURLStreamEvent);
         }
      }
      
      private function unregisterEventHandlers() : void
      {
         var _iED:IEventDispatcher = null;
         if(this._stream)
         {
            _iED = this._stream as IEventDispatcher;
            _iED.removeEventListener(Event.COMPLETE,this.reflectURLStreamEvent);
            _iED.removeEventListener(ProgressEvent.PROGRESS,this.reflectURLStreamEvent);
            _iED.removeEventListener(IOErrorEvent.IO_ERROR,this.reflectURLStreamEvent);
            _iED.removeEventListener(Event.CLOSE,this.reflectURLStreamEvent);
         }
      }
      
      private function reflectURLStreamEvent(e:Event) : void
      {
         if(!(e is IOErrorEvent))
         {
            dispatchEvent(e);
            if(e.type == Event.CLOSE)
            {
               this.unregisterEventHandlers();
            }
         }
         else
         {
            this.reportError(e as IOErrorEvent);
         }
      }
      
      native private function getURI() : String;
      
      native public function get mediaType() : String;
      
      native public function get relativePath() : String;
      
      native private function getMediaStreamObject() : InputMediaStream;
   }
}
