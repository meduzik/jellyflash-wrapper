package flash.media
{
   import flash.geom.Rectangle;
   
   [API("675")]
   [native(instance="CameraRollBrowseOptionsObject",methods="auto",cls="CameraRollBrowseOptionsClass")]
   public class CameraRollBrowseOptions
   {
       
      
      public function CameraRollBrowseOptions()
      {
         super();
         this.width = 0;
         this.height = 0;
         this.origin = new Rectangle(0,0,0,0);
      }
      
      native public function get width() : Number;
      
      native public function set width(param1:Number) : void;
      
      native public function get height() : Number;
      
      native public function set height(param1:Number) : void;
      
      native public function get origin() : Rectangle;
      
      native public function set origin(param1:Rectangle) : void;
   }
}
