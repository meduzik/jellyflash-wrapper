package flash.media
{
	public final class SoundTransform
	{
		private var ltl: Number = 1, ltr: Number = 0;
		private var rtl: Number = 0, rtr: Number = 1;
		private var _volume: Number = 1;
		
		public function SoundTransform(vol:Number = 1, panning:Number = 0)
		{
			super();
			this.volume = vol;
			this.pan = panning;
		}
		
		public function get volume() : Number {
			return _volume;
		}
		
		public function set volume(param1:Number) : void {
			_volume = param1;
		}
		
		public function get leftToLeft() : Number {
			return ltl;
		}
		
		public function set leftToLeft(param1:Number) : void {
			ltl = param1;
		}
		
		public function get leftToRight() : Number {
			return ltr;
		}
		
		public function set leftToRight(param1:Number) : void {
			ltr = param1;
		}
		
		public function get rightToRight() : Number {
			return rtr;
		}
		
		public function set rightToRight(param1:Number) : void {
			rtr = param1;
		}
		
		public function get rightToLeft() : Number {
			return rtl;
		}
		
		public function set rightToLeft(param1:Number) : void {
			rtl = param1;
		}
		
		public function get pan() : Number
		{
			if(this.leftToRight == 0 && this.rightToLeft == 0)
			{
				return 1 - this.leftToLeft * this.leftToLeft;
			}
			return 0;
		}
		
		public function set pan(panning:Number) : void
		{
			this.leftToLeft = Math.sqrt(1 - panning);
			this.leftToRight = 0;
			this.rightToRight = Math.sqrt(1 + panning);
			this.rightToLeft = 0;
		}
		
		internal function copyFrom(other: SoundTransform): void {
			ltl = other.ltl;
			ltr = other.ltr;
			rtl = other.rtl;
			rtr = other.rtr;
			_volume = other._volume;
		}
		
		internal function clone(): SoundTransform {
			var other: SoundTransform = new SoundTransform();
			other.copyFrom(this);
			return other;
		}
	}
}
