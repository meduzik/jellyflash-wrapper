package flash.media
{
	import flash.events.EventDispatcher;
	
	[jfl_native(destructor=true, payload="jfl_flash_media_SoundChannel_PAYLOAD")]
	[Event(name="soundComplete",type="flash.events.Event")]
	public final class SoundChannel extends EventDispatcher
	{
		private var transform: SoundTransform = new SoundTransform();
		
		public function SoundChannel()
		{
			super();
		}
		
		native public function get position() : Number;
		
		public function get soundTransform() : SoundTransform {
			return transform.clone();
		}
		
		public function set soundTransform(param: SoundTransform) : void {
			transform.copyFrom(param);
			updateTransfrom();
		}
		
		native public function stop() : void;
		
		native private function updateTransfrom(): void;
	}
}
