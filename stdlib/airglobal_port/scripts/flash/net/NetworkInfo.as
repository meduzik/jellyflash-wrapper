package flash.net
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.system.Security;
	import flash.desktop.NativeApplication;
	
	[native(instance="NetworkInfoObject",methods="auto",cls="NetworkInfoClass",construct="native")]
	[Version("air2.0")]
	[Event(name="networkChange",type="flash.events.Event")]
	public class NetworkInfo extends EventDispatcher
	{
		private static var _Instance:NetworkInfo = new NetworkInfo();
		
		public function NetworkInfo()
		{
			super();
			NativeApplication.nativeApplication.addEventListener(Event.NETWORK_CHANGE,this.onNetworkChange);
		}
		
		public static function get isSupported() : Boolean
		{
			return true;
		}
		
		public static function get networkInfo() : NetworkInfo
		{
			var sandboxType:String = Security.sandboxType;
			if(sandboxType != Security.APPLICATION)
			{
				Error.throwError(SecurityError,3205);
			}
			return getInstance();
		}
		
		private static function getInstance() : NetworkInfo{
			return _Instance;
		}
		
		[API("729")]
		native public static function get permissionStatus() : String;
		
		private function onNetworkChange(event:Event) : void
		{
			dispatchEvent(event);
		}
		
		native public function findInterfaces() : Vector.<NetworkInterface>;
	}
}
