package flash.net
{
	import flash.events.EventDispatcher;
	
	[jfl_native(destructor=true, payload="flash_net_SharedObject_PAYLOAD")]
	[Event(name="sync",type="flash.events.SyncEvent")]
	[Event(name="netStatus",type="flash.events.NetStatusEvent")]
	[Event(name="asyncError",type="flash.events.AsyncErrorEvent")]
	public class SharedObject extends EventDispatcher
	{
		private static const Instances:HashMap = new HashMap();
				
		private var _data:Object = {};
		
		public function SharedObject()
		{
			super();
			
		}
		
		public static function getLocal(name:String, path:String = null, param3:Boolean = false) : SharedObject{
			var key:String = name + "@" + path;
			if ( !(key in Instances) ){
				var so:SharedObject = new SharedObject();
				so.init(name, path);
				Instances[key] = so;
			}
			return Instances[key];
		}
		
		native private function init(name:String, path:String):void;
		
		native public function sync():void;
		
		// native public static function getRemote(param1:String, param2:String = null, param3:Object = false, param4:Boolean = false) : SharedObject;
		// native public static function deleteAll(param1:String) : int;
		// native public static function getDiskUsage(param1:String) : int;
		// native public static function get defaultObjectEncoding() : uint;
		// native public static function set defaultObjectEncoding(param1:uint) : void;
		// native public static function get preventBackup() : Boolean;
		// native public static function set preventBackup(value:Boolean) : void;
		
		public function get data() : Object{
			return _data;
		}
		
		native public function flush(minDiskSpace:int = 0) : String;
		native public function get size() : uint;
		
		public function clear() : void{
			_data = {};
			_clear();
		}
		
		native private function _clear(): void;
		
		// native public function close() : void;
		
		// native public function set fps(updatesPerSecond:Number) : void;
		// native public function send(... arguments) : void;
	
		//native public function get objectEncoding() : uint;
		
		//native public function set objectEncoding(param1:uint) : void;
		
		//native public function get client() : Object;
		
		//native public function set client(param1:Object) : void;
		
		//native public function setDirty(param1:String) : void;
		
		public function setProperty(propertyName:String, value:Object = null) : void
		{
			if(this.data[propertyName] != value)
			{
				this.data[propertyName] = value;
			}
		}
		
		//native private function invoke(param1:uint, ... rest) : *;
		
		//native private function invokeWithArgsArray(param1:uint, param2:Array) : *;
	}
}
