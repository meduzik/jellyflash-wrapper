package flash.net
{
	import flash.events.EventDispatcher;
	import flash.utils.ByteArray;
	
	[jfl_native(payload="flash_net_FileReference_PAYLOAD")]
	[Event(name="uploadCompleteData",type="flash.events.DataEvent")]
	[Event(name="httpResponseStatus",type="flash.events.HTTPStatusEvent")]
	[Event(name="httpStatus",type="flash.events.HTTPStatusEvent")]
	[Event(name="select",type="flash.events.Event")]
	[Event(name="securityError",type="flash.events.SecurityErrorEvent")]
	[Event(name="progress",type="flash.events.ProgressEvent")]
	[Event(name="open",type="flash.events.Event")]
	[Event(name="ioError",type="flash.events.IOErrorEvent")]
	[Event(name="complete",type="flash.events.Event")]
	[Event(name="permissionStatus",type="flash.events.PermissionEvent")]
	[Event(name="cancel",type="flash.events.Event")]
	public class FileReference extends EventDispatcher
	{
		public function FileReference()
		{	
			_init();
		}
		
		native private function _init():void;
		
		native public static function get permissionStatus() : String;
		
		native public function get creationDate() : Date;
		
		native public function get creator() : String;
		
		native public function get modificationDate() : Date;
		
		native public function get name() : String;
		
		native public function get size() : Number;
		
		native public function get type() : String;
		
		native public function cancel() : void;
		
		native public function download(param1:URLRequest, param2:String = null) : void;
		
		native public function upload(param1:URLRequest, param2:String = "Filedata", param3:Boolean = false) : void;
		
		[Version("10")]
		native public function get data() : ByteArray;
		
		native private function _load(param1:ByteArray) : void;
		
		native private function _save(param1:ByteArray, param2:String) : void;
		
		[Version("10")]
		public function load() : void
		{
			this._load(new ByteArray());
		}
		
		[Version("10")]
		public function save(data:*, defaultFileName:String = null) : void
		{
			var d:ByteArray = new ByteArray();
			if(data == null)
			{
				throw new ArgumentError("data");
			}
			if(data is String)
			{
				d.writeUTFBytes(data as String);
			}
			else if(data is ByteArray)
			{
				d.writeBytes(data as ByteArray);
			}
			else
			{
				try
				{
					d.writeUTFBytes(data);
				}
				catch(e:Error)
				{
					throw new ArgumentError("data");
				}
			}
			d.position = 0;
			if(defaultFileName == null)
			{
				defaultFileName = "";
			}
			this._save(d,defaultFileName);
		}
		
		public function get extension() : String
		{
			var nm:String = this.name;
			if(!nm)
			{
				return null;
			}
			var extnPos:int = nm.lastIndexOf(".");
			if(extnPos < 0)
			{
				return null;
			}
			return nm.substr(extnPos + 1);
		}
		
		[API("719")]
		native public function requestPermission() : void;
		
		native public function browse(param1:Array = null) : Boolean;
		
		[API("661")]
		native public function uploadUnencoded(param1:URLRequest) : void;
	}
}
