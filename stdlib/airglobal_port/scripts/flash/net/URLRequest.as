package flash.net
{
	import flash.utils.ByteArray;

	[native(instance="URLRequestObject",methods="auto",cls="URLRequestClass",gc="exact")]
	public final class URLRequest
	{
		private static const DefaultUserAgent:String = InitUserAgent();
		
		private var _url:String = "";
		private var _data:Object;
		private var _method:String = "GET";
		private var _contentType:String;
		private var _userAgent:String = DefaultUserAgent;
		private var _headers:Array;
		private var _idleTimeout:Number = 0;
		
		native private static function InitUserAgent():String;
		
		public function URLRequest(url:String = null)
		{
			super();
			this.url = url;
		}
		
		public function get url() : String{
			return _url;
		}
		
		public function set url(value:String) : void{
			_url = value;
			if ( _url == null ){
				_url = "";
			}
		}
		
		public function get data() : Object{
			return _data;
		}
		
		public function set data(value:Object) : void{
			_data = value;
		}
		
		private function encodeData():ByteArray{
			if ( !_data ){
				return null;
			}
			if ( _data is ByteArray ){
				return _data as ByteArray;
			}else if ( _data is String ){
				var ba:ByteArray = new ByteArray();
				ba.writeUTFBytes(_data as String);
				return ba;
			}else if ( _data is URLVariables ){
				ba = new ByteArray();
				ba.writeUTFBytes(URLVariables(_data).toString());
				return ba;
			}else{
				throw new Error("bad URLRequest data");
			}
		}
		
		public function get method() : String{
			return _method;
		}
		
		public function set method(value:String) : void
		{
			_method = value;
		}
		
		public function get contentType() : String{
			return _contentType;
		}
		
		public function set contentType(value:String) : void{
			_contentType = value;
		}
		
		public function get requestHeaders() : Array{
			if ( !_headers ){
				_headers = [];
			}
			return _headers;
		}
		
		public function set requestHeaders(value:Array) : void
		{
			var n:int = value.length;
			for ( var i:int = 0; i < n; i++ ){
				if ( !(value[i] is URLRequestHeader) ){
					throw new Error("invalid request headers array element " + i);
				}
			}
			if ( !_headers ){
				_headers = [];
			}
			_headers.length = 0;
			for ( i = 0; i < n; i++ ){
				_headers[i] = value[i];
			}
		}
		
		public function get digest() : String{
			return null;
		}
		
		public function set digest(param1:String) : void{
			throw new Error("URLRequest.digest is not supported");
		}
		
		public function useRedirectedURL(param1:URLRequest, param2:Boolean = false, param3:* = null, param4:String = null) : void{
			throw new Error("URLRequest.useRedirectedURL is not supported");
		}
		
		public function get followRedirects() : Boolean{
			return true;
		}
		
		public function set followRedirects(value:Boolean) : void{
			if ( !value ){
				throw new Error("URLRequest.followRedirects must be true");
			}
		}
		
		public function get userAgent() : String{
			return _userAgent;
		}
		
		public function set userAgent(value:String) : void{
			if ( value == null ){
				throw new Error("URLRequest.userAgent cannot be null");
			}
			_userAgent = value;
		}
		
		public function get manageCookies() : Boolean{
			return true;
		}
		
		public function set manageCookies(value:Boolean) : void{
			if ( !value ){
				throw new Error("URLRequest.manageCookies must be true");
			}
		}
		
		public function get useCache() : Boolean{
			return true;
		}
		
		public function set useCache(value:Boolean) : void{
			if ( !value ){
				throw new Error("non-cached URLRequests aren't supported");
			}
		}
		
		public function get cacheResponse() : Boolean{
			return true;
		}
		
		public function set cacheResponse(value:Boolean) : void{
			if ( !value ){
				throw new Error("non-cached URLRequests aren't supported");
			}
		}
		
		public function get idleTimeout() : Number{
			return _idleTimeout;
		}
		
		public function set idleTimeout(value:Number) : void{
			_idleTimeout = value;
		}
		
		public function get authenticate() : Boolean{
			return false;
		}
		
		public function set authenticate(param1:Boolean) : void{
			throw new Error("URLRequest.authenticate is not supported");
		}
	}
}
