package flash.net.dns
{
   import flash.events.EventDispatcher;
   
   [native(instance="DNSResolverObject",methods="auto",cls="DNSResolverClass")]
   [Version("air2.0")]
   [Event(name="error",type="flash.events.ErrorEvent")]
   [Event(name="lookup",type="flash.events.DNSResolverEvent")]
   public class DNSResolver extends EventDispatcher
   {
       
      
      public function DNSResolver()
      {
         super();
      }
      
      native public static function get isSupported() : Boolean;
      
      public function lookup(host:String, recordType:Class) : void
      {
         var host2:String = null;
         if(host == null)
         {
            Error.throwError(ArgumentError,2007,"host");
         }
         if(host.length == 0)
         {
            Error.throwError(ArgumentError,2004,"host");
         }
         switch(recordType)
         {
            case ARecord:
               this.doLookup(host,host,1);
               break;
            case AAAARecord:
               this.doLookup(host,host,28);
               break;
            case MXRecord:
               this.doLookup(host,host,15);
               break;
            case PTRRecord:
               host2 = this.getPtrHost(host);
               if(host2 == null)
               {
                  Error.throwError(ArgumentError,2004,"host");
               }
               this.doLookup(host,host2,12);
               break;
            case SRVRecord:
               this.doLookup(host,host,33);
               break;
            default:
               Error.throwError(ArgumentError,2004,"recordType");
         }
      }
      
      native private function doLookup(param1:String, param2:String, param3:int) : void;
      
      private function getPtrHost(param1:String) : String
      {
         var _loc3_:RegExp = null;
         var _loc4_:RegExp = null;
         var _loc5_:RegExp = null;
         var _loc6_:RegExp = null;
         var _loc7_:RegExp = null;
         var _loc8_:Array = null;
         var _loc9_:String = null;
         var _loc10_:RegExp = null;
         var _loc11_:Array = null;
         var _loc12_:* = undefined;
         var _loc13_:int = 0;
         var _loc14_:Array = null;
         var _loc15_:Boolean = false;
         var _loc16_:int = 0;
         var _loc17_:String = null;
         var _loc18_:String = null;
         var _loc2_:String = null;
         if(param1 != null)
         {
            _loc3_ = /\.in-addr\.arpa$/;
            _loc4_ = /\.ip6\.arpa$/;
            _loc5_ = /^\d{1,3}(\.\d{1,3}){3}$/;
            _loc6_ = /^(::){0,1}[0-9a-f]{1,4}(:{1,2}[0-9a-f]{1,4}){0,7}(::){0,1}$/i;
            if(param1.search(_loc3_) != -1 || param1.search(_loc4_) != -1)
            {
               _loc2_ = param1;
            }
            else if(param1.search(_loc5_) == 0)
            {
               _loc7_ = /\d+/g;
               _loc8_ = param1.match(_loc7_);
               if(_loc8_ != null && _loc8_.length == 4)
               {
                  _loc9_ = new String();
                  while(_loc8_.length > 0)
                  {
                     _loc9_ = _loc9_ + _loc8_.pop();
                     _loc9_ = _loc9_ + ".";
                  }
                  _loc2_ = _loc9_ + "in-addr.arpa";
               }
            }
            else if(param1.search(_loc6_) == 0)
            {
               _loc10_ = /(:+)/g;
               _loc11_ = param1.split(_loc10_);
               _loc12_ = _loc11_ != null?_loc11_.length:0;
               if(_loc12_ > 0 && _loc12_ <= 15 && (_loc12_ & 1) == 1)
               {
                  _loc13_ = 15 - _loc12_ >> 1;
                  _loc14_ = new Array();
                  _loc15_ = true;
                  while(_loc15_ && _loc16_ < _loc12_)
                  {
                     _loc17_ = _loc11_.shift();
                     if(_loc17_ != ":")
                     {
                        if(_loc17_ == "::")
                        {
                           _loc15_ = _loc13_ > 0;
                           if(_loc15_)
                           {
                              while(_loc13_--)
                              {
                                 _loc14_.push("0000");
                              }
                           }
                        }
                        else
                        {
                           while(_loc17_.length < 4)
                           {
                              _loc17_ = "0" + _loc17_;
                           }
                           _loc14_.push(_loc17_);
                        }
                     }
                     _loc16_++;
                  }
                  if(_loc15_ && _loc14_.length == 8)
                  {
                     _loc18_ = _loc14_.join("");
                     _loc9_ = new String();
                     for(_loc16_ = _loc18_.length - 1; _loc16_ >= 0; _loc16_--)
                     {
                        _loc9_ = _loc9_ + _loc18_.charAt(_loc16_);
                        _loc9_ = _loc9_ + ".";
                     }
                     _loc2_ = _loc9_ + "ip6.arpa";
                  }
               }
            }
            else
            {
               _loc2_ = null;
            }
         }
         return _loc2_;
      }
   }
}
