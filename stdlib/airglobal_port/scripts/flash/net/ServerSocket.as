package flash.net
{
   import flash.events.EventDispatcher;
   
   [native(instance="ServerSocketObject",methods="auto",cls="ServerSocketClass")]
   [Version("air2.0")]
   [Event(name="connect",type="flash.events.ServerSocketConnectEvent")]
   [Event(name="close",type="flash.events.Event")]
   public class ServerSocket extends EventDispatcher
   {
       
      
      public function ServerSocket()
      {
         super();
      }
      
      native public static function get isSupported() : Boolean;
      
      public function bind(localPort:int = 0, localAddress:String = "0.0.0.0") : void
      {
         this.internalBind(localPort,localAddress);
      }
      
      native private function internalBind(param1:int, param2:String) : void;
      
      public function close() : void
      {
         this.internalClose();
      }
      
      native private function internalClose() : void;
      
      public function listen(backlog:int = 0) : void
      {
         this.internalListen(backlog);
      }
      
      native private function internalListen(param1:int) : void;
      
      native public function get localAddress() : String;
      
      native public function get localPort() : int;
      
      public function get listening() : Boolean
      {
         return this.internalListening();
      }
      
      native private function internalListening() : Boolean;
      
      public function get bound() : Boolean
      {
         return this.internalBound();
      }
      
      native private function internalBound() : Boolean;
   }
}
