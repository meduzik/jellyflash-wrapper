package flash.display3D
{
   import flash.utils.ByteArray;
   
   [jfl_native(payload="flash_display3D_VertexBuffer3D_PAYLOAD", destructor=true)]
   public class VertexBuffer3D
   {
       
      
      public function VertexBuffer3D()
      {
         super();
      }
      
      native public function uploadFromVector(param1:Vector.<Number>, param2:int, param3:int) : void;
      
      native public function uploadFromByteArray(param1:ByteArray, param2:int, param3:int, param4:int) : void;
      
      native public function dispose() : void;
   }
}
