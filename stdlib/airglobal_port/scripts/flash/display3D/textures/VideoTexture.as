package flash.display3D.textures
{   
   [jfl_native(payload="flash_display3D_textures_VideoTexture_PAYLOAD")]
   [Event(name="renderState",type="flash.events.TextureVideoEvent")]
   public final class VideoTexture extends TextureBase
   {
       
      
      public function VideoTexture()
      {
         super();
      }
      
      [API("705")]
      native public function get videoWidth() : int;
      
      [API("705")]
      native public function get videoHeight() : int;
   }
}
