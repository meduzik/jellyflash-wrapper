package flash.display3D.textures
{
   import flash.display.BitmapData;
   import flash.utils.ByteArray;
   
   [jfl_native(payload="flash_display3D_textures_RectangleTexture_PAYLOAD")]
   public final class RectangleTexture extends TextureBase
   {
       
      
      public function RectangleTexture()
      {
         super();
      }
      
      [API("690")]
      native public function uploadFromBitmapData(param1:BitmapData) : void;
      
      [API("690")]
      native public function uploadFromByteArray(param1:ByteArray, param2:uint) : void;
      
      [API("719")]
      native public function uploadFromBitmapDataAsync(param1:BitmapData) : void;
      
      [API("719")]
      native public function uploadFromByteArrayAsync(param1:ByteArray, param2:uint) : void;
   }
}
