package flash.display3D.textures
{
   import flash.display.BitmapData;
   import flash.utils.ByteArray;
   
   [jfl_native(payload="flash_display3D_textures_Texture_PAYLOAD")]
   public final class Texture extends TextureBase
   {
       
      
      public function Texture()
      {
         super();
      }
      
      [API("674")]
      native public function uploadFromBitmapData(param1:BitmapData, param2:uint = 0) : void;
      
      [API("674")]
      native public function uploadFromByteArray(param1:ByteArray, param2:uint, param3:uint = 0) : void;
      
      [API("674")]
      native public function uploadCompressedTextureFromByteArray(param1:ByteArray, param2:uint, param3:Boolean = false) : void;
      
      [API("719")]
      native public function uploadFromBitmapDataAsync(param1:BitmapData, param2:uint = 0) : void;
      
      [API("719")]
      native public function uploadFromByteArrayAsync(param1:ByteArray, param2:uint, param3:uint = 0) : void;
   }
}
