package flash.display3D.textures
{
   import flash.display.BitmapData;
   import flash.utils.ByteArray;
   
   [jfl_native(payload="flash_display3D_textures_CubeTexture_PAYLOAD")]
   public final class CubeTexture extends TextureBase
   {
       
      
      public function CubeTexture()
      {
         super();
      }
      
      [API("674")]
      native public function uploadFromBitmapData(param1:BitmapData, param2:uint, param3:uint = 0) : void;
      
      [API("674")]
      native public function uploadFromByteArray(param1:ByteArray, param2:uint, param3:uint, param4:uint = 0) : void;
      
      [API("674")]
      native public function uploadCompressedTextureFromByteArray(param1:ByteArray, param2:uint, param3:Boolean = false) : void;
   }
}
