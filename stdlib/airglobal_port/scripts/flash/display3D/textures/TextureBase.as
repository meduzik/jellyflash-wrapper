package flash.display3D.textures
{
   import flash.events.EventDispatcher;
   
   [jfl_native(payload="flash_display3D_textures_TextureBase_PAYLOAD", destructor=true)]
   public class TextureBase extends EventDispatcher
   {
       
      
      public function TextureBase()
      {
         super();
      }
      
      native public function dispose() : void;
   }
}
