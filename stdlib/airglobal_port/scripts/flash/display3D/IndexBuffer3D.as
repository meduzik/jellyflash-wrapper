package flash.display3D
{
   import flash.utils.ByteArray;
   
   [jfl_native(payload="flash_display3D_IndexBuffer3D_PAYLOAD", destructor=true)]
   public final class IndexBuffer3D
   {
       
      
      public function IndexBuffer3D()
      {
         super();
      }
      
      native public function uploadFromVector(param1:Vector.<uint>, param2:int, param3:int) : void;
      
      native public function uploadFromByteArray(param1:ByteArray, param2:int, param3:int, param4:int) : void;
      
      native public function dispose() : void;
   }
}
