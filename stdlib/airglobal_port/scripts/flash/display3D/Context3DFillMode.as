package flash.display3D
{
   [API("703")]
   public final class Context3DFillMode
   {
      
      public static const WIREFRAME:String = "wireframe";
      
      public static const SOLID:String = "solid";
       
      
      public function Context3DFillMode()
      {
         super();
      }
   }
}
