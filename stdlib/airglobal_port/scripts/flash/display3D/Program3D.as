package flash.display3D
{
   import flash.utils.ByteArray;
   
   [jfl_native(payload="flash_display3D_Program3D_PAYLOAD", destructor=true)]
   public final class Program3D
   {
       
      
      public function Program3D()
      {
         super();
      }
      
      native public function upload(param1:ByteArray, param2:ByteArray) : void;
      
      native public function dispose() : void;
   }
}
