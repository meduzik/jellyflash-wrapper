package flash.sampler
{
   [native(methods="auto",cls="ScriptSamplerClass",construct="none")]
   public final class ScriptSampler
   {
       
      
      public function ScriptSampler()
      {
         super();
      }
      
      native public static function getType(param1:Number) : String;
      
      native public static function getSize(param1:Number) : Number;
      
      native public static function getMembers(param1:Number) : Vector.<ScriptMember>;
      
      native public static function getInvocationCount(param1:Number) : Number;
      
      native public static function getName(param1:Number) : String;
      
      native public static function getFilename(param1:Number) : String;
   }
}
