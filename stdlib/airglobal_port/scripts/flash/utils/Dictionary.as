package flash.utils
{
	[jfl_native(gc=false, destructor=true, iter=false, dyn=false, payload="flash_utils_Dictionary_PAYLOAD")]
	public dynamic class Dictionary
	{
		public function Dictionary(weakKeys:Boolean = false)
		{
			super();
			this.init(weakKeys);
		}
		
		native private function init(param1:Boolean) : void;
	}
}
