package flash.system
{
	import flash.utils.ByteArray;
	
	[native(instance="ApplicationDomainObject",methods="auto",cls="ApplicationDomainClass",gc="exact")]
	public final class ApplicationDomain
	{
		private static const Instance:ApplicationDomain = new ApplicationDomain(null);
		private static var _domainMemory:ByteArray;
		
		public function ApplicationDomain(parentDomain:ApplicationDomain = null)
		{
		}
		
		public static function get currentDomain() : ApplicationDomain{
			return Instance;
		}
		
		public static function get MIN_DOMAIN_MEMORY_LENGTH() : uint{
			return 1024;
		}
		
		public function get parentDomain() : ApplicationDomain{
			return null;
		}
		
		native public function getDefinition(param1:String) : Object;
		native public function hasDefinition(param1:String) : Boolean;
		native public function getQualifiedDefinitionNames() : Vector.<String>;
		
		public function get domainMemory() : ByteArray{
			return _domainMemory;
		}
		
		public function set domainMemory(value:ByteArray) : void{
			_domainMemory = value;
		}
	}
}
