package flash.system
{
   public class SecurityPrivilege
   {
      
      public static const FILE = "file";
      
      public static const FILE_READ = "fileRead";
      
      public static const FILE_WRITE = "fileWrite";
      
      public static const FILE_APPSTORE = "fileAppstore";
      
      public static const FILE_PATHACCESS = "filePathAccess";
      
      public static const FILE_TEMP = "fileTemp";
      
      public static const FILE_WRITE_RESOURCE = "fileWriteResource";
      
      public static const HTTP_ALL = "httpAll";
      
      public static const HTML = "html";
      
      public static const WINDOW = "window";
      
      public static const SCREEN = "screen";
       
      
      public function SecurityPrivilege()
      {
         super();
      }
   }
}
