package flash.notifications
{
   [API("683")]
   public final class NotificationStyle
   {
      
      public static const ALERT:String = "alert";
      
      public static const SOUND:String = "sound";
      
      public static const BADGE:String = "badge";
       
      
      public function NotificationStyle()
      {
         super();
      }
   }
}
