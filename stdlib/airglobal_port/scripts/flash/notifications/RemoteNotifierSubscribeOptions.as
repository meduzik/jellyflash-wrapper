package flash.notifications
{
   [API("683")]
   [native(instance="RemoteNotifierSubscribeOptionsObject",methods="auto",cls="RemoteNotifierSubscribeOptionsClass")]
   public final class RemoteNotifierSubscribeOptions
   {
       
      
      public function RemoteNotifierSubscribeOptions()
      {
         super();
         this.notificationStyles = null;
      }
      
      native public function get notificationStyles() : Vector.<String>;
      
      native public function set notificationStyles(param1:Vector.<String>) : void;
   }
}
