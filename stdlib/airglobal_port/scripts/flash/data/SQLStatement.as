package flash.data
{
   import flash.events.EventDispatcher;
   import flash.net.Responder;
   
   [API("661")]
   [native(instance="SQLStatementObject",methods="auto",cls="SQLStatementClass")]
   [Event(name="error",type="flash.events.SQLErrorEvent")]
   [Event(name="result",type="flash.events.SQLEvent")]
   public class SQLStatement extends EventDispatcher
   {
       
      
      private var _sql:String = "";
      
      private var _params:Object;
      
      private var _finalize:Boolean = false;
      
      public function SQLStatement()
      {
         this._params = {};
         super();
         if(Security.sandboxType != Security.APPLICATION)
         {
            Error.throwError(SecurityError,3205);
         }
      }
      
      public function get executing() : Boolean
      {
         return this.isExecuting();
      }
      
      native public function get sqlConnection() : SQLConnection;
      
      native public function set sqlConnection(param1:SQLConnection) : void;
      
      native public function get itemClass() : Class;
      
      native public function set itemClass(param1:Class) : void;
      
      public function get parameters() : Object
      {
         return this._params;
      }
      
      public function get text() : String
      {
         return this._sql;
      }
      
      public function set text(value:String) : void
      {
         if(this._sql != value)
         {
            if(this.executing)
            {
               Error.throwError(IllegalOperationError,3106);
            }
            this._finalize = this.prepared;
            this._sql = value.replace(/[\r]/g,"\n");
         }
      }
      
      public function cancel() : void
      {
         if(this.executing)
         {
            this.internalCancel();
         }
      }
      
      public function clearParameters() : void
      {
         this._params = {};
      }
      
      public function execute(prefetch:int = -1, responder:Responder = null) : void
      {
         this.checkReady();
         if(this._finalize)
         {
            this.checkComplete();
         }
         this.internalExecute(this._sql,this._params,this._finalize,prefetch,responder);
         this._finalize = false;
      }
      
      native public function getResult() : SQLResult;
      
      public function next(prefetch:int = -1, responder:Responder = null) : void
      {
         if(!this.executing)
         {
            Error.throwError(IllegalOperationError,3107,"SQLStatement.next");
         }
         this.internalNext(prefetch,responder);
      }
      
      private function checkAllowed() : void
      {
         if(this._sql == null || this._sql.length == 0)
         {
            Error.throwError(IllegalOperationError,3108);
         }
         if(this.sqlConnection == null)
         {
            Error.throwError(IllegalOperationError,3109);
         }
         if(!this.sqlConnection.connected)
         {
            Error.throwError(IllegalOperationError,3104);
         }
      }
      
      private function checkComplete() : void
      {
         if(!this.isSQLComplete(this._sql))
         {
            this._sql = this._sql + ";";
         }
      }
      
      private function checkReady() : void
      {
         if(this.executing)
         {
            Error.throwError(IllegalOperationError,3110);
         }
         this.checkAllowed();
      }
      
      native private function get prepared() : Boolean;
      
      native private function internalCancel() : void;
      
      native private function internalExecute(param1:String, param2:Object, param3:Boolean, param4:int, param5:Responder) : void;
      
      native private function internalNext(param1:int, param2:Responder) : void;
      
      native private function isSQLComplete(param1:String) : Boolean;
      
      native private function isExecuting() : Boolean;
   }
}
