package flash.text
{
   import flash.display.Sprite;
   import flash.display.Stage;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.FocusEvent;
   import flash.events.KeyboardEvent;
   import flash.events.SoftKeyboardEvent;
   
   [API("675")]
   [native(instance="StageTextImplObject",methods="auto",friend="StageTextObject",cls="StageTextImplClass",construct="native")]
   class StageTextImpl extends Sprite
   {
       
      
      function StageTextImpl()
      {
         super();
         focusRect = false;
         tabIndex = -1;
         tabEnabled = true;
         addEventListener(FocusEvent.KEY_FOCUS_CHANGE,this.onKeyFocusChange);
         addEventListener(KeyboardEvent.KEY_DOWN,this.onKeyDown,false,int.MAX_VALUE);
         addEventListener(KeyboardEvent.KEY_UP,this.onKeyUp,false,int.MAX_VALUE);
         this.addMyListeners();
      }
      
      private function onKeyFocusChange(evt:FocusEvent) : void
      {
         evt.preventDefault();
      }
      
      private function onKeyUp(evt:KeyboardEvent) : void
      {
         var stage:Stage = null;
         if(evt.keyCode == Keyboard.BACK || evt.keyCode == Keyboard.MENU || evt.keyCode == Keyboard.SEARCH)
         {
            stage = this.stage;
            if(stage)
            {
               stage.dispatchEvent(evt);
            }
            evt.stopImmediatePropagation();
         }
      }
      
      private function onKeyDown(evt:KeyboardEvent) : void
      {
         var stage:Stage = null;
         var handled:Boolean = false;
         if(evt.keyCode == Keyboard.BACK || evt.keyCode == Keyboard.MENU || evt.keyCode == Keyboard.SEARCH)
         {
            stage = this.stage;
            if(stage)
            {
               handled = stage.dispatchEvent(evt);
            }
            evt.stopImmediatePropagation();
            if(!handled)
            {
               evt.preventDefault();
            }
         }
      }
      
      private function addListenersForASImpl(st:StageText, obj:EventDispatcher) : void
      {
         this.removeEventListener(FocusEvent.FOCUS_OUT,this.focusOutHandler);
         obj.addEventListener(FocusEvent.FOCUS_OUT,function(evt:FocusEvent):void
         {
            st.dispatchEvent(new FocusEvent(evt.type,false,evt.cancelable,evt.relatedObject,evt.shiftKey,evt.keyCode));
            evt.stopImmediatePropagation();
         });
         obj.addEventListener(FocusEvent.FOCUS_IN,function(evt:FocusEvent):void
         {
            st.dispatchEvent(new FocusEvent(evt.type,false,evt.cancelable,evt.relatedObject,evt.shiftKey,evt.keyCode));
            evt.stopImmediatePropagation();
         });
         obj.addEventListener(FocusEvent.KEY_FOCUS_CHANGE,function(evt:FocusEvent):void
         {
            st.dispatchEvent(new FocusEvent(evt.type,false,evt.cancelable,evt.relatedObject,evt.shiftKey,evt.keyCode));
            evt.stopImmediatePropagation();
         });
         obj.addEventListener(FocusEvent.MOUSE_FOCUS_CHANGE,function(evt:FocusEvent):void
         {
            var handled:Boolean = false;
            handled = st.dispatchEvent(new FocusEvent(evt.type,false,evt.cancelable,evt.relatedObject,evt.shiftKey,evt.keyCode));
            evt.stopImmediatePropagation();
            if(!handled)
            {
               evt.preventDefault();
            }
         });
         obj.addEventListener(Event.CHANGE,function(evt:Event):void
         {
            st.dispatchEvent(new Event(evt.type,false,evt.cancelable));
            evt.stopImmediatePropagation();
         });
         obj.addEventListener(KeyboardEvent.KEY_DOWN,function(evt:KeyboardEvent):void
         {
            var handled:Boolean = false;
            if(!(evt.ctrlKey == true && (evt.keyCode == Keyboard.A || evt.keyCode == Keyboard.X || evt.keyCode == Keyboard.C || evt.keyCode == Keyboard.V)))
            {
               handled = false;
               handled = st.dispatchEvent(new KeyboardEvent(evt.type,false,evt.cancelable,evt.charCode,evt.keyCode,evt.keyLocation,evt.ctrlKey,evt.altKey,evt.shiftKey));
               evt.stopImmediatePropagation();
               if(!handled)
               {
                  evt.preventDefault();
               }
            }
         });
         obj.addEventListener(KeyboardEvent.KEY_UP,function(evt:KeyboardEvent):void
         {
            st.dispatchEvent(new KeyboardEvent(evt.type,false,evt.cancelable,evt.charCode,evt.keyCode,evt.keyLocation,evt.ctrlKey,evt.altKey,evt.shiftKey));
            evt.stopImmediatePropagation();
         });
         obj.addEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_ACTIVATE,function(evt:SoftKeyboardEvent):void
         {
            st.dispatchEvent(new SoftKeyboardEvent(evt.type,false,evt.cancelable,evt.relatedObject,evt.triggerType));
            evt.stopImmediatePropagation();
         });
         obj.addEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_ACTIVATING,function(evt:SoftKeyboardEvent):void
         {
            var handled:Boolean = false;
            handled = st.dispatchEvent(new SoftKeyboardEvent(evt.type,false,evt.cancelable,evt.relatedObject,evt.triggerType));
            evt.stopImmediatePropagation();
            if(!handled)
            {
               evt.preventDefault();
            }
         });
         obj.addEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_DEACTIVATE,function(evt:SoftKeyboardEvent):void
         {
            st.dispatchEvent(new SoftKeyboardEvent(evt.type,false,evt.cancelable,evt.relatedObject,evt.triggerType));
            evt.stopImmediatePropagation();
         });
      }
      
      [cppcall]
      private function addFullScreenListeners() : *
      {
         stage.addEventListener(FullScreenEvent.FULL_SCREEN,this.onFullScreenEvent);
      }
      
      [cppcall]
      private function removeFullScreenListeners() : *
      {
         stage.removeEventListener(FullScreenEvent.FULL_SCREEN,this.onFullScreenEvent);
      }
      
      private function onFullScreenEvent(evt:Event) : *
      {
         this.onNativeDisplayStateChangeForStage();
      }
      
      private function focusOutHandler(evt:Event) : void
      {
         evt.stopImmediatePropagation();
         this.onNativeFocusOut();
      }
      
      private function addMyListeners() : void
      {
         addEventListener(FocusEvent.FOCUS_OUT,this.focusOutHandler);
         addEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_ACTIVATE,function(evt:SoftKeyboardEvent):void
         {
            evt.stopImmediatePropagation();
            onNativeSoftKeyboardActivate();
         });
         addEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_ACTIVATING,function(evt:SoftKeyboardEvent):void
         {
            evt.stopImmediatePropagation();
            if(onNativeSoftKeyboardActivating())
            {
               evt.preventDefault();
            }
         });
         addEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_DEACTIVATE,function(evt:SoftKeyboardEvent):void
         {
            evt.stopImmediatePropagation();
            onNativeSoftKeyboardDeactivate(evt.triggerType);
         });
      }
      
      native private function onNativeFocusOut() : void;
      
      native private function onNativeSoftKeyboardActivate() : void;
      
      native private function onNativeSoftKeyboardActivating() : Boolean;
      
      native private function onNativeSoftKeyboardDeactivate(param1:String) : void;
      
      native private function onNativeDisplayStateChangeForStage() : void;
   }
}
