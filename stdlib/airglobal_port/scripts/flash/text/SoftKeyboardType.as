package flash.text
{
   [API("675")]
   public final class SoftKeyboardType
   {
      
      public static const DEFAULT:String = "default";
      
      public static const PUNCTUATION:String = "punctuation";
      
      public static const URL:String = "url";
      
      public static const NUMBER:String = "number";
      
      public static const CONTACT:String = "contact";
      
      public static const EMAIL:String = "email";
      
      [API("729")]
      public static const PHONE:String = "phone";
      
      [API("729")]
      public static const DECIMAL:String = "decimalpad";
       
      
      public function SoftKeyboardType()
      {
         super();
      }
   }
}
