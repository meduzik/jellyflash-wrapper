package flash.text
{
	[native(instance="TextFormatObject",methods="auto",cls="TextFormatClass",gc="exact")]
	public class TextFormat
	{
		public function TextFormat(
			font:String = null, 
			size:Object = null, 
			color:Object = null, 
			bold:Object = null, 
			italic:Object = null, 
			underline:Object = null, 
			url:String = null, 
			target:String = null, 
			align:String = null, 
			leftMargin:Object = null, 
			rightMargin:Object = null, 
			indent:Object = null, 
			leading:Object = null
		)
		{
			if(font != null)
			{
				this.font = font;
			}
			if(size != null)
			{
				this.size = int(size);
			}
			if(color != null)
			{
				this.color = uint(color);
			}
			if(bold != null)
			{
				this.bold = bold;
			}
			if(italic != null)
			{
				this.italic = italic;
			}
			if(underline != null)
			{
				this.underline = underline;
			}
			if(url != null)
			{
				this.url = url;
			}
			if(target != null)
			{
				this.target = target;
			}
			if(align != null)
			{
				this.align = align;
			}
			if(leftMargin != null)
			{
				this.leftMargin = int(leftMargin);
			}
			if(rightMargin != null)
			{
				this.rightMargin = int(rightMargin);
			}
			if(indent != null)
			{
				this.indent = int(indent);
			}
			if(leading != null)
			{
				this.leading = int(leading);
			}
		}
		
		public function copyFrom(other:TextFormat):void{
			_font = other._font;
			_align = other._align;
			_blockIndent = other._blockIndent;
			_bold = other._bold;
			_italic = other._italic;
			_kerning = other._kerning;
			_color = other._color;
			_indent = other._indent;
			_leading = other._leading;
			_leftMargin = other._leftMargin;
			_rightMargin = other._rightMargin;
			_letterSpacing = other._letterSpacing;
			_size = other._size;
		}
		
		private var _font:String = "default";
		public function set font(value:String): void{
			if ( value != null ){
				_font = value;
			}
		}
		public function get font(): String{
			return _font;
		}
		
		private var _align:String = TextFormatAlign.LEFT;
		public function set align(value:String): void{
			switch(value){
			case TextFormatAlign.LEFT:
			case TextFormatAlign.RIGHT:
			case TextFormatAlign.CENTER:
			case TextFormatAlign.JUSTIFY:
			case TextFormatAlign.START:
			case TextFormatAlign.END:{
				_align = value;
			}break;
			default:{
				throw new Error("invalid TextFormatAlign");
			}break;
			}
		}
		public function get align(): String{
			return _align;
		}
		
		private var _blockIndent : int = 0;
		public function set blockIndent(value:int): void{
			_blockIndent = value;
		}
		public function get blockIndent(): int{
			return _blockIndent;
		}
		
		private var _bold: Boolean = false;
		public function set bold(value:Boolean): void{
			_bold = value;
		}
		public function get bold(): Boolean{
			return _bold;
		}
		
		private var _italic: Boolean = false;
		public function set italic(value:Boolean): void{
			_italic = value;
		}
		public function get italic(): Boolean{
			return _italic;
		}
		
		private var _kerning: Boolean = false;
		public function set kerning(value:Boolean): void{
			_kerning = value;
		}
		public function get kerning(): Boolean{
			return _kerning;
		}
		
		private var _color: uint = 0x000000;
		public function set color(value:uint): void{
			_color = value;
		}
		public function get color(): uint{
			return _color;
		}
		
		private var _indent: int = 0;
		public function set indent(value:int): void{
			_indent = value;
		}
		public function get indent(): int{
			return _indent;
		}
		
		private var _leading: int = 0;
		public function set leading(value:int): void{
			_leading = value;
		}
		public function get leading(): int{
			return _leading;
		}
		
		private var _leftMargin: int = 0;
		public function set leftMargin(value:int): void{
			_leftMargin = value;
		}
		public function get leftMargin(): int{
			return _leftMargin;
		}
		
		private var _rightMargin: int = 0;
		public function set rightMargin(value:int): void{
			_rightMargin = value;
		}
		public function get rightMargin(): int{
			return _rightMargin;
		}
		
		private var _letterSpacing: Number = 0;
		public function set letterSpacing(value:Number): void{
			_letterSpacing = value;
		}
		public function get letterSpacing(): Number{
			return _letterSpacing;
		}
		
		private var _size: int = 12;
		public function set size(value:int): void{
			_size = value;
		}
		public function get size(): int{
			return _size;
		}
		
		public function set bullet(value:Boolean): void{
			if (value) {
				throw new Error("TextFormat.bullet is not supported");
			}
		}
		public function get bullet(): Boolean{
			return false;
		}
		
		public function set underline(value:Boolean): void{
			if (value) {
				throw new Error("TextFormat.underline is not supported");
			}
		}
		public function get underline(): Boolean{
			return false;
		}
		
		public function set url(value:String): void{
			if (value != null) {
				throw new Error("TextFormat.url is not supported");
			}
		}
		public function get url(): String{
			return null;
		}
		
		public function set target(value:String): void{
			if (value != null) {
				throw new Error("TextFormat.target is not supported");
			}
		}
		public function get target(): String{
			return null;
		}
		
		public function set tabStops(value:Array): void{
			if (value != null) {
				throw new Error("TextFormat.tabStops is not supported");
			}
		}
		public function get tabStops(): Array{
			return null;
		}
	}
}
