package flash.text
{
	import flash.desktop.Clipboard;
	import flash.desktop.NativeDragOptions;
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.events.NativeDragEvent;
	import flash.geom.Rectangle;
	
	[jfl_native(payload="flash_text_TextField_PAYLOAD")]
	[Event(name="textInteractionModeChange",type="flash.events.Event")]
	[Event(name="textInput",type="flash.events.TextEvent")]
	[Event(name="scroll",type="flash.events.Event")]
	[Event(name="link",type="flash.events.TextEvent")]
	[Event(name="change",type="flash.events.Event")]
	public class TextField extends InteractiveObject
	{
		private var _wordWrap:Boolean = false;
		private var _multiline:Boolean = false;
		private var _defaultFormat:TextFormat = new TextFormat();
		
		public function TextField()
		{
			super();
		}
		
		native override public function get width() : Number;
		native override public function set width(value:Number) : void;
		native override public function get height() : Number;
		native override public function set height(value:Number) : void;
		
		[Version("10")]
		native public static function isFontCompatible(param1:String, param2:String) : Boolean;
		
		native public function get alwaysShowSelection() : Boolean;
		
		native public function set alwaysShowSelection(param1:Boolean) : void;
		
		native public function get antiAliasType() : String;
		
		native public function set antiAliasType(param1:String) : void;
		
		native public function get autoSize() : String;
		
		native public function set autoSize(param1:String) : void;
		
		native public function get background() : Boolean;
		
		native public function set background(param1:Boolean) : void;
		
		native public function get backgroundColor() : uint;
		
		native public function set backgroundColor(param1:uint) : void;
		
		native public function get border() : Boolean;
		
		native public function set border(param1:Boolean) : void;
		
		native public function get borderColor() : uint;
		
		native public function set borderColor(param1:uint) : void;
		
		native public function get bottomScrollV() : int;
		
		native public function get caretIndex() : int;
		
		native public function get condenseWhite() : Boolean;
		
		native public function set condenseWhite(param1:Boolean) : void;
		
		public function get defaultTextFormat() : TextFormat{
			var format:TextFormat = new TextFormat();
			format.copyFrom(_defaultFormat);
			return format;
		}
		
		public function set defaultTextFormat(value:TextFormat) : void{
			if (!value){
				throw new Error("TextFormat is null");
			}
			_defaultFormat.copyFrom(value);
			_updateNativeFormat();
		}
		
		native private function _updateNativeFormat():void;
		
		public function get embedFonts() : Boolean{
			return true;
		}
		
		public function set embedFonts(param1:Boolean) : void{
			// do nothing
		}
		
		native public function get multiline() : Boolean;
		native public function set multiline(value:Boolean) : void;
		
		native public function get wordWrap() : Boolean;
		native public function set wordWrap(value:Boolean) : void;
		
		native public function get htmlText() : String;
		native public function set htmlText(param1:String) : void;
		
		native public function get text() : String;
		native public function set text(param1:String) : void;
		
		override native public function getBounds(param1:DisplayObject) : Rectangle;
		
		native public function get gridFitType() : String;
		
		native public function set gridFitType(param1:String) : void;
		
		native public function get length() : int;
		
		[API("670")]
		native public function get textInteractionMode() : String;
		
		native public function get maxChars() : int;
		
		native public function set maxChars(param1:int) : void;
		
		native public function get maxScrollH() : int;
		
		native public function get maxScrollV() : int;
		
		native public function get mouseWheelEnabled() : Boolean;
		
		native public function set mouseWheelEnabled(param1:Boolean) : void;
		
		native public function get numLines() : int;
		
		native public function get displayAsPassword() : Boolean;
		
		native public function set displayAsPassword(param1:Boolean) : void;
		
		native public function get restrict() : String;
		
		native public function set restrict(param1:String) : void;
		
		native public function get scrollH() : int;
		
		native public function set scrollH(param1:int) : void;
		
		native public function get scrollV() : int;
		
		native public function set scrollV(param1:int) : void;
		
		native public function get selectable() : Boolean;
		
		native public function set selectable(param1:Boolean) : void;
		
		[Inspectable(environment="none")]
		public function get selectedText() : String
		{
			return this.text.substring(this.selectionBeginIndex,this.selectionEndIndex);
		}
		
		native public function get selectionBeginIndex() : int;
		
		native public function get selectionEndIndex() : int;
		
		native public function get sharpness() : Number;
		native public function set sharpness(param1:Number) : void;
		
		public function get textColor() : uint{
			return _defaultFormat.color;
		}
		
		public function set textColor(value:uint) : void{
			_defaultFormat.color = value;
			_updateNativeFormat();
		}
		
		native public function get textHeight() : Number;
		
		native public function get textWidth() : Number;
		
		native public function get thickness() : Number;
		
		native public function set thickness(param1:Number) : void;
		
		native public function get type() : String;
		
		native public function set type(param1:String) : void;
		
		native public function appendText(newText:String) : void;
		
		native public function getCharBoundaries(param1:int) : Rectangle;
		
		native public function getCharIndexAtPoint(param1:Number, param2:Number) : int;
		
		native private function getCharIndexNearestPoint(param1:Number, param2:Number) : int;
		
		native public function getFirstCharInParagraph(param1:int) : int;
		
		native public function getLineIndexAtPoint(param1:Number, param2:Number) : int;
		
		native public function getLineIndexOfChar(param1:int) : int;
		
		native public function getLineLength(param1:int) : int;
		
		native public function getLineMetrics(param1:int) : TextLineMetrics;
		
		native public function getLineOffset(param1:int) : int;
		
		native public function getLineText(param1:int) : String;
		
		native public function getParagraphLength(param1:int) : int;
		
		native public function getTextFormat(param1:int = -1, param2:int = -1) : TextFormat;
		
		[Inspectable(environment="none")]
		native public function getTextRuns(param1:int = 0, param2:int = 2147483647) : Array;
		
		[Inspectable(environment="none")]
		native public function getRawText() : String;
		
		native public function replaceSelectedText(param1:String) : void;
		
		native public function replaceText(param1:int, param2:int, param3:String) : void;
		
		native public function setSelection(param1:int, param2:int) : void;
		
		native public function setTextFormat(param1:TextFormat, param2:int = -1, param3:int = -1) : void;
		
		native public function getImageReference(param1:String) : DisplayObject;
		
		native public function get useRichTextClipboard() : Boolean;
		
		native public function set useRichTextClipboard(param1:Boolean) : void;
		
		native private function get dragCaretVisible() : Boolean;
		
		native private function set dragCaretVisible(param1:Boolean) : void;
		
		native private function get dragCaretIndex() : int;
		
		native private function set dragCaretIndex(param1:int) : void;
		
		native private function setSelectionToDragCaret() : void;
		
		native private function isDragCaretSurrogateTrail(param1:int) : Boolean;
	}
}
