package flash.text
{
	public class StageTextInitOptions
	{
		private var _multiline:Boolean;
		
		public function StageTextInitOptions(multiline:Boolean = false)
		{
			super();
			this.multiline = multiline;
		}
		
		public function set multiline(param1:Boolean) : void{
			_multiline = param1;
		}
		
		public function get multiline() : Boolean{
			return _multiline;
		}
	}
}
