package flash.display
{
   [API("661")]
   public final class NativeWindowType
   {
      
      public static const NORMAL:String = "normal";
      
      public static const LIGHTWEIGHT:String = "lightweight";
      
      public static const UTILITY:String = "utility";
       
      
      public function NativeWindowType()
      {
         super();
      }
   }
}
