package flash.display
{
	public class Bitmap extends DisplayObject
	{
		private var _bitmapData:BitmapData;
		private var _pixelSnapping:String;
		private var _smoothing:Boolean;
	
		public function Bitmap(bitmapData:BitmapData = null, pixelSnapping:String = "auto", smoothing:Boolean = false)
		{
			_bitmapData = bitmapData;
			_pixelSnapping = pixelSnapping;
			_smoothing = smoothing;
		}
		
		public function get pixelSnapping() : String{
			return _pixelSnapping;
		}
		
		public function set pixelSnapping(value:String) : void{
			switch ( value ){
			case PixelSnapping.NEVER:
			case PixelSnapping.ALWAYS:
			case PixelSnapping.AUTO:{
				_pixelSnapping = value;
			}break;
			default:{
				throw new Error("invalid pixelSnapping value");
			}break;
			}
		}
		
		public function get smoothing() : Boolean{
			return _smoothing;
		}
		
		public function set smoothing(value:Boolean) : void{
			_smoothing = value;
		}
		
		public function get bitmapData() : BitmapData{
			return _bitmapData;
		}
		
		public function set bitmapData(value:BitmapData) : void{
			_bitmapData = value;
		}
	}
}
