package flash.display
{
	import flash.display3D.Context3D;
	import flash.events.EventDispatcher;
	
	[native(instance="Stage3DObject",methods="auto",cls="Stage3DClass",construct="native")]
	[API("674")]
	[Event(name="error",type="flash.events.ErrorEvent")]
	[Event(name="context3DCreate",type="flash.events.Event")]
	public class Stage3D extends EventDispatcher
	{
		private var _context:Context3D;
		
		public function Stage3D()
		{
			super();
		}
		
		public function get context3D() : Context3D{
			return _context;
		}
		
		native public function requestContext3D(param1:String = "auto", param2:String = "baseline") : void;
		native public function requestContext3DMatchingProfiles(param1:Vector.<String>) : void;
		
		public function get x() : Number{
			return 0;
		}
		
		public function set x(param1:Number) : void{
			throw new Error("changing stage3d x not supported");
		}
		
		public function get y() : Number{
			return 0;
		}
		
		public function set y(param1:Number) : void{
			throw new Error("changing stage3d y not supported");
		}
		
		public function get visible() : Boolean{
			return true;
		}
		
		public function set visible(param1:Boolean) : void{
			throw new Error("changing stage3d visible not supported");
		}
	}
}
