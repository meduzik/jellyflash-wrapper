package flash.display
{
   import flash.events.EventDispatcher;
   import flash.geom.Rectangle;
   
   [API("661")]
   [native(instance="ScreenObject",methods="auto",cls="ScreenClass",construct="native")]
   public final class Screen extends EventDispatcher
   {
       
      
      public function Screen()
      {
         super();
      }
      
      native public static function get screens() : Array;
      
      native public static function get mainScreen() : Screen;
      
      native public static function getScreensForRectangle(param1:Rectangle) : Array;
      
      native public function get bounds() : Rectangle;
      
      native public function get visibleBounds() : Rectangle;
      
      native public function get colorDepth() : int;
      
      native public function get modes() : Array;
      
      native public function get mode() : ScreenMode;
      
      native public function set mode(param1:ScreenMode) : void;
   }
}
