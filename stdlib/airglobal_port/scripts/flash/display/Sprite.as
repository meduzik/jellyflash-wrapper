package flash.display
{
	import flash.geom.Rectangle;
	import flash.media.SoundTransform;
	
	[native(instance="SpriteObject",methods="auto",cls="SpriteClass",gc="exact")]
	public class Sprite extends DisplayObjectContainer
	{
		public function Sprite()
		{
		}
		
		public function get graphics() : Graphics{
			return null;
		}
	}
}
