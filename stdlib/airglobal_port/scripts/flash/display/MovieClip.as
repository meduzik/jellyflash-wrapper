package flash.display
{
	[native(instance="MovieClipObject",methods="auto",cls="MovieClipClass",gc="exact")]
	public dynamic class MovieClip extends Sprite
	{
		public function MovieClip()
		{
			super();
		}
	}
}
