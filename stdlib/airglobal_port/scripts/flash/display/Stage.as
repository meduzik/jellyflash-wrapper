package flash.display
{
	import flash.accessibility.AccessibilityImplementation;
	import flash.accessibility.AccessibilityProperties;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.geom.Transform;
	import flash.media.StageVideo;
	import flash.text.TextSnapshot;
	import flash.errors.IllegalOperationError;
	
	[native(instance="StageObject",methods="auto",cls="StageClass",construct="native",gc="exact")]
	[Event(name="stageVideoAvailability",type="flash.events.StageVideoAvailabilityEvent")]
	[Event(name="orientationChange",type="flash.events.StageOrientationEvent")]
	[Event(name="orientationChanging",type="flash.events.StageOrientationEvent")]
	[Event(name="VsyncStateChangeAvailability",type="flash.events.VsyncStateChangeAvailabilityEvent")]
	[Event(name="fullScreen",type="flash.events.FullScreenEvent")]
	[Event(name="browserZoomChange",type="flash.events.Event")]
	[Event(name="resize",type="flash.events.Event")]
	[Event(name="mouseLeave",type="flash.events.Event")]
	public class Stage extends DisplayObjectContainer
	{
		
		private static const kInvalidParamError:uint = 2004;
		
		public function Stage()
		{
			super();
		}
		
		private var _stageWidth:int;
		private var _stageHeight:int;
		private var _stage3D:Stage3D;
		private var _contentsScaleFactor:Number = 1;
		
		[Version("air2.0")]
		native public static function get supportsOrientationChange() : Boolean;
		
		native public function get frameRate() : Number;
		
		native public function set frameRate(param1:Number) : void;
		
		native public function invalidate() : void;
		
		public function get scaleMode() : String{
			return StageScaleMode.NO_SCALE;
		}
		
		public function set scaleMode(value:String) : void{
			if ( value != StageScaleMode.NO_SCALE ){
				throw new Error("unsupported stage.scaleMode");
			}
		}
		
		public function get align() : String{
			return StageAlign.TOP_LEFT;
		}
		
		public function set align(value:String) : void{
			if ( value != StageAlign.TOP_LEFT ){
				throw new Error("unsupported stage.align");
			}
		}
		
		public function get stageWidth() : int{
			return _stageWidth;
		}
		
		public function set stageWidth(param1:int) : void{
			throw new Error("modifying stageWidth is not supported");
		}
		
		public function get stageHeight() : int{
			return _stageHeight;
		}
		
		public function set stageHeight(param1:int) : void{
			throw new Error("modifying stageHeight is not supported");
		}
		
		native public function get showDefaultContextMenu() : Boolean;
		
		native public function set showDefaultContextMenu(param1:Boolean) : void;
		
		native public function get focus() : InteractiveObject;
		
		native public function set focus(param1:InteractiveObject) : void;
		
		[Version("10")]
		native public function get colorCorrection() : String;
		
		[Version("10")]
		native public function set colorCorrection(param1:String) : void;
		
		[Version("10")]
		native public function get colorCorrectionSupport() : String;
		
		native public function isFocusInaccessible() : Boolean;
		
		native public function get stageFocusRect() : Boolean;
		
		native public function set stageFocusRect(param1:Boolean) : void;
		
		native public function get quality() : String;
		
		native public function set quality(param1:String) : void;
		
		native public function get displayState() : String;
		
		native public function set displayState(param1:String) : void;
		
		native public function get fullScreenSourceRect() : Rectangle;
		
		native public function set fullScreenSourceRect(param1:Rectangle) : void;
		
		[API("678")]
		native public function get mouseLock() : Boolean;
		
		[API("678")]
		native public function set mouseLock(param1:Boolean) : void;
		
		[API("667")]
		native public function get stageVideos() : Vector.<StageVideo>;
		
		public function get stage3Ds() : Vector.<Stage3D>{
			return Vector.<Stage3D>([_stage3D]);
		}
		
		[API("670")]
		native public function get color() : uint;
		
		[API("670")]
		native public function set color(param1:uint) : void;
		
		native public function get fullScreenWidth() : uint;
		
		native public function get fullScreenHeight() : uint;
		
		[Version("10.0.32")]
		native public function get wmodeGPU() : Boolean;
		
		[API("670")]
		native public function get softKeyboardRect() : Rectangle;
		
		[API("670")]
		native public function get allowsFullScreen() : Boolean;
		
		[API("680")]
		native public function get allowsFullScreenInteractive() : Boolean;
		
		native override public function removeChildAt(param1:int) : DisplayObject;
		
		native override public function swapChildrenAt(param1:int, param2:int) : void;
		
		public function get contentsScaleFactor() : Number {
			return _contentsScaleFactor;
		}
		
		[API("700")]
		native public function get browserZoomFactor() : Number;
	  
		[API("674")]
		native public function get displayContextInfo() : String;
		
		public function get constructor() : *
		{
			return null;
		}
		
		public function set constructor(c:*) : void
		{
		}
		
		[API("661")]
		public function assignFocus(objectToFocus:InteractiveObject, direction:String) : void
		{
			if(direction != FocusDirection.TOP && direction != FocusDirection.BOTTOM && direction != FocusDirection.NONE)
			{
				Error.throwError(ArgumentError,kInvalidParamError);
				return;
			}
			this._assignFocus(objectToFocus,direction);
		}
		
		native private function _assignFocus(param1:InteractiveObject, param2:String) : void;
		
		[API("661")]
		public function get nativeWindow() : NativeWindow
		{
			return this._nativeWindow;
		}
		
		native private function get _nativeWindow() : NativeWindow;
		
		[Version("air2.0")]
		native public function get orientation() : String;
		
		[Version("air2.0")]
		public function setOrientation(newOrientation:String) : void
		{
			switch(newOrientation)
			{
				case StageOrientation.DEFAULT:
				case StageOrientation.ROTATED_LEFT:
				case StageOrientation.ROTATED_RIGHT:
				case StageOrientation.UPSIDE_DOWN:
					this._orientation = newOrientation;
					break;
				default:
					Error.throwError(ArgumentError,kInvalidParamError);
			}
		}
		
		native private function set _orientation(param1:String) : void;
		
		[Version("air2.0")]
		public function setAspectRatio(newAspectRatio:String) : void
		{
			switch(newAspectRatio)
			{
				case StageAspectRatio.PORTRAIT:
				case StageAspectRatio.LANDSCAPE:
				case StageAspectRatio.ANY:
					this._aspectRatio = newAspectRatio;
					break;
				default:
					Error.throwError(ArgumentError,kInvalidParamError);
			}
		}
		
		native private function set _aspectRatio(param1:String) : void;
		
		[Version("air2.0")]
		native public function get deviceOrientation() : String;
		
		[Version("air2.0")]
		native public function get autoOrients() : Boolean;
		
		[API("671")]
		public function set autoOrients(value:Boolean) : void
		{
			this.setAutoOrients(value);
		}
		
		native private function setAutoOrients(param1:Boolean) : void;
		
		[API("671")]
		native public function get supportedOrientations() : Vector.<String>;
		
		[API("671")]
		private function _supportedOrientations_stubImpl() : Vector.<String>
		{
			return new Vector.<String>();
		}
		
		[API("729")]
		native public function get vsyncEnabled() : Boolean;
		
		[API("729")]
		native public function set vsyncEnabled(param1:Boolean) : void;
	}
}
