package flash.display
{
   [API("661")]
   public final class NativeWindowDisplayState
   {
      
      public static const NORMAL:String = "normal";
      
      public static const MAXIMIZED:String = "maximized";
      
      public static const MINIMIZED:String = "minimized";
       
      
      public function NativeWindowDisplayState()
      {
         super();
      }
   }
}
