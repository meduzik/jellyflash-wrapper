package flash.display
{
   [API("661")]
   public final class NativeWindowResize
   {
      
      public static const TOP:String = "T";
      
      public static const LEFT:String = "L";
      
      public static const BOTTOM:String = "B";
      
      public static const RIGHT:String = "R";
      
      public static const TOP_LEFT:String = "TL";
      
      public static const TOP_RIGHT:String = "TR";
      
      public static const BOTTOM_LEFT:String = "BL";
      
      public static const BOTTOM_RIGHT:String = "BR";
      
      public static const NONE:String = "";
       
      
      public function NativeWindowResize()
      {
         super();
      }
   }
}
