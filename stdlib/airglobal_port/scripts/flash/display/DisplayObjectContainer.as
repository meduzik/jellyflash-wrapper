package flash.display
{
	import flash.geom.Point;
	import flash.text.TextSnapshot;
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	
	[native(instance="ContainerObject",methods="auto",cls="ContainerClass",construct="abstract-restricted",gc="exact")]
	public class DisplayObjectContainer extends InteractiveObject
	{
		private var children:Vector.<DisplayObject> = new Vector.<DisplayObject>();
		private var _mouseChildren:Boolean = true;
		private var _tabChildren:Boolean = true;
		
		public function DisplayObjectContainer()
		{
		}
		
		public function addChild(object:DisplayObject) : DisplayObject{
			return addChildAt(object, children.length);
		}
		
		public function addChildAt(object:DisplayObject, index:int) : DisplayObject{
			if ( index > children.length || index < 0 ){
				Error.throwError(RangeError, 1001);
			}
			if ( object.parent == this ){
				swapChildrenAt(getChildIndex(object), index);
				return object;
			}
			if ( object.parent ){
				object.parent.removeChild(object);
			}
			children.splice(index, 0, object);
			object._parent = this;
			if ( object.hasEventListener(Event.ADDED) ){
				object.dispatchEvent(new Event(Event.ADDED));
			}
			var stage:Stage = this.stage;
			if ( stage ){
				DispatchToHierarchy(object, Event.ADDED_TO_STAGE);
			}
			return object;
		}
		
		public function removeChild(object:DisplayObject) : DisplayObject{
			var index:int = children.indexOf(object);
			if ( index < 0 ){
				Error.throwError(ArgumentError, 1001);
			}
			return removeChildAt(index);
		}
		
		public function removeChildAt(index:int) : DisplayObject{
			if ( index < 0 || index >= children.length ){
				Error.throwError(ArgumentError, 1001);
			}
			var object:DisplayObject = children[index];
			object._parent = null;
			children.splice(index, 1);
			if ( object.hasEventListener(Event.REMOVED) ){
				object.dispatchEvent(new Event(Event.REMOVED));
			}
			if ( stage ){
				DispatchToHierarchy(object, Event.REMOVED_FROM_STAGE);
			}
			return object;
		}
		
		public function getChildIndex(object:DisplayObject) : int{
			var index:int = children.indexOf(object);
			if ( index < 0 ){
				Error.throwError(ArgumentError, 1001);
			}
			return index;
		}
		
		public function setChildIndex(param1:DisplayObject, param2:int) : void{
			Error.throwError(IllegalOperationError, 1001);
		}
		
		public function getChildAt(index:int) : DisplayObject{
			if ( index < 0 || index >= children.length ){
				Error.throwError(ArgumentError, 1001);
			}
			return children[index];
		}
		
		public function getChildByName(name:String) : DisplayObject{
			for ( var i:int = 0; i < children.length; i++ ){
				if ( children[i].name == name ){
					return children[i];
				}
			}
			return null;
		}
		
		public function get numChildren() : int{
			return children.length;
		}
		
		public function get textSnapshot() : TextSnapshot{
			Error.throwError(IllegalOperationError, 1001);
			return null;
		}
		
		public function getObjectsUnderPoint(param1:Point) : Array{
			Error.throwError(IllegalOperationError, 1001);
			return null;
		}
		
		public function areInaccessibleObjectsUnderPoint(param1:Point) : Boolean{
			Error.throwError(IllegalOperationError, 1001);
			return null;
		}
		
		public function get tabChildren() : Boolean{
			return _tabChildren;
		}
		
		public function set tabChildren(param1:Boolean) : void{
			_tabChildren = param1;
		}
		
		public function get mouseChildren() : Boolean{
			return _mouseChildren;
		}
		
		public function set mouseChildren(param1:Boolean) : void{
			_mouseChildren = param1;
		}
		
		public function contains(object:DisplayObject) : Boolean{
			if ( object == this ){
				return true;
			}
			for ( var i:int = 0; i < children.length; i++ ){
				var child:DisplayObject = children[i];
				if ( child == object ){
					return true;
				}
				if ( child is DisplayObjectContainer && (child as DisplayObjectContainer).contains(object) ){
					return true;
				}
			}
			return false;
		}
		
		public function swapChildrenAt(index1:int, index2:int) : void{
			if ( index1 < 0 || index1 >= children.length ){
				Error.throwError(ArgumentError, 1001);
			}
			if ( index2 < 0 || index2 >= children.length ){
				Error.throwError(ArgumentError, 1001);
			}
			var tmp:DisplayObject = children[index1];
			children[index1] = children[index2];
			children[index2] = tmp;
		}
		
		public function swapChildren(param1:DisplayObject, param2:DisplayObject) : void{
			swapChildrenAt(getChildIndex(param1), getChildIndex(param2));
		}
		
		public function removeChildren(param1:int = 0, param2:int = 2147483647) : void{
			Error.throwError(IllegalOperationError, 1001);
		}
		
		public function stopAllMovieClips() : void{
			Error.throwError(IllegalOperationError, 1001);
		}
		
		private static function DispatchToHierarchy(object:DisplayObject, type:String):void{
			if ( object.hasEventListener(type) ){
				object.dispatchEvent(new Event(type));
			}
			if ( object is DisplayObjectContainer ){
				var container:DisplayObjectContainer = object as DisplayObjectContainer;
				for ( var i:int = 0; i < container.numChildren; i++ ){
					var child:DisplayObject = container.children[i];
					if ( child.hasEventListener(type) ){
						child.dispatchEvent(new Event(type));
					}
					DispatchToHierarchy(child, type);
				}
			}
		}
	}
}
