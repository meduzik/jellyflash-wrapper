package flash.display
{
   [API("661")]
   [native(instance="WindowInitOptionsObject",methods="auto",cls="WindowInitOptionsClass")]
   public class NativeWindowInitOptions
   {
       
      
      public function NativeWindowInitOptions()
      {
         super();
         this.systemChrome = NativeWindowSystemChrome.STANDARD;
         this.transparent = false;
         this.resizable = true;
         this.maximizable = true;
         this.minimizable = true;
      }
      
      native public function get systemChrome() : String;
      
      native public function set systemChrome(param1:String) : void;
      
      native public function get transparent() : Boolean;
      
      native public function set transparent(param1:Boolean) : void;
      
      native public function get type() : String;
      
      native public function set type(param1:String) : void;
      
      native public function get minimizable() : Boolean;
      
      native public function set minimizable(param1:Boolean) : void;
      
      native public function get maximizable() : Boolean;
      
      native public function set maximizable(param1:Boolean) : void;
      
      native public function get resizable() : Boolean;
      
      native public function set resizable(param1:Boolean) : void;
      
      [API("671")]
      native public function get owner() : NativeWindow;
      
      [API("671")]
      native public function set owner(param1:NativeWindow) : void;
      
      [API("675")]
      native public function get renderMode() : String;
      
      [API("675")]
      native public function set renderMode(param1:String) : void;
   }
}
