package flash.display
{
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.UncaughtErrorEvents;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.SecurityDomain;
	import flash.utils.ByteArray;
	import flash.utils.IDataInput;
	import flash.errors.IOError;
	import flash.errors.IllegalOperationError;
	
	[jfl_native(payload="jfl_flash_display_Loader_PAYLOAD")]
	public class Loader extends DisplayObjectContainer
	{
		private var _content:DisplayObject;
		private var _contentLoaderInfo:LoaderInfo = new LoaderInfo();
	
		public function Loader()
		{
			super();
		}
		
		native public function load(request:URLRequest, context:LoaderContext = null) : void;
		native public function loadBytes(bytes:ByteArray, context:LoaderContext = null) : void;
		native public function close() : void;
		native public function unload() : void;
		
		native public function unloadAndStop(gc:Boolean = true) : void;
		
		public function get content() : DisplayObject{
			return _content;
		}
		
		public function get contentLoaderInfo() : LoaderInfo{
			return _contentLoaderInfo;
		}
	}
}
