package flash.filesystem
{
	import flash.events.EventDispatcher;
	import flash.utils.ByteArray;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	[jfl_native(payload="flash_filesystem_FileStream_PAYLOAD")]
	[Event(name="complete",type="flash.events.Event")]
	[Event(name="outputProgress",type="flash.events.OutputProgressEvent")]
	[Event(name="progress",type="flash.events.ProgressEvent")]
	[Event(name="ioError",type="flash.events.IOErrorEvent")]
	[Event(name="close",type="flash.events.Event")]
	public class FileStream extends EventDispatcher implements IDataInput, IDataOutput
	{
		public function FileStream()
		{
			super();
		}
		
		native public function open(param1:File, param2:String) : void;
		
		native public function openAsync(param1:File, param2:String) : void;
		
		native public function truncate() : void;
		
		native public function get position() : Number;
		
		native public function set position(param1:Number) : void;
		
		native public function close() : void;
		
		native public function get readAhead() : Number;
		
		native public function set readAhead(param1:Number) : void;
		
		native public function get bytesAvailable() : uint;
		
		native public function get endian() : String;
		
		native public function set endian(param1:String) : void;
		
		native public function get objectEncoding() : uint;
		
		native public function set objectEncoding(param1:uint) : void;
		
		native public function readBoolean() : Boolean;
		
		native public function readByte() : int;
		
		native public function readBytes(param1:ByteArray, param2:uint = 0, param3:uint = 0) : void;
		
		native public function readDouble() : Number;
		
		native public function readFloat() : Number;
		
		native public function readInt() : int;
		
		native public function readMultiByte(param1:uint, param2:String) : String;
		
		native public function readObject() : *;
		
		native public function readShort() : int;
		
		native public function readUnsignedByte() : uint;
		
		native public function readUnsignedInt() : uint;
		
		native public function readUnsignedShort() : uint;
		
		native public function readUTF() : String;
		
		native public function readUTFBytes(param1:uint) : String;
		
		native public function writeBoolean(param1:Boolean) : void;
		
		native public function writeByte(param1:int) : void;
		
		native public function writeBytes(param1:ByteArray, param2:uint = 0, param3:uint = 0) : void;
		
		native public function writeDouble(param1:Number) : void;
		
		native public function writeFloat(param1:Number) : void;
		
		native public function writeInt(param1:int) : void;
		
		native public function writeMultiByte(param1:String, param2:String) : void;
		
		native public function writeObject(param1:*) : void;
		
		native public function writeShort(param1:int) : void;
		
		native public function writeUnsignedInt(param1:uint) : void;
		
		native public function writeUTF(param1:String) : void;
		
		native public function writeUTFBytes(param1:String) : void;
	}
}
