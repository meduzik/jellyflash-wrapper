package flash.filesystem
{
   [API("661")]
   public class FileMode
   {
      
      public static const READ:String = "read";
      
      public static const WRITE:String = "write";
      
      public static const UPDATE:String = "update";
      
      public static const APPEND:String = "append";
       
      
      public function FileMode()
      {
         super();
      }
   }
}
