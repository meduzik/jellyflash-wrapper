package flash.filesystem
{
	import flash.desktop.Icon;
	import flash.net.FileReference;
	import flash.errors.IllegalOperationError;
	import flash.errors.IOError;
	
	[jfl_native(payload="flash_filesystem_File_PAYLOAD")]
	[Event(name="directoryListing",type="flash.events.FileListEvent")]
	[Event(name="selectMultiple",type="flash.events.FileListEvent")]
	[Event(name="select",type="flash.events.Event")]
	[Event(name="permissionStatus",type="flash.events.PermissionEvent")]
	[Event(name="securityError",type="flash.events.SecurityErrorEvent")]
	[Event(name="ioError",type="flash.events.IOErrorEvent")]
	[Event(name="complete",type="flash.events.Event")]
	[Event(name="cancel",type="flash.events.Event")]
	public class File extends FileReference
	{
		native private static function initSystemCharset():String;
		native private static function initSeparator():String;
		native private static function initLineEnding():String;
		native private static function initUserDirectory():String;
		native private static function initDocumentsDirectory():String;
		native private static function initDesktopDirectory():String;
		native private static function initApplicationStorageDirectory():String;
		native private static function initApplicationDirectory():String;
		native private static function initCacheDirectory():String;
	
		private static const _systemCharset:String = initSystemCharset();
		private static const _separator:String = initSeparator();
		private static const _lineEnding:String = initLineEnding();
		private static const _userDirectory:File = new File(initUserDirectory());
		private static const _documentsDirectory:File = new File(initDocumentsDirectory());
		private static const _desktopDirectory:File = new File(initDesktopDirectory());
		private static const _applicationStorageDirectory:File = new File(initApplicationStorageDirectory());
		private static const _applicationDirectory:File = new File(initApplicationDirectory());
		private static const _cacheDirectory:File = new File(initCacheDirectory());
	
		public static function get systemCharset() : String{
			return _systemCharset;
		}
		public static function get separator() : String{
			return _separator;
		}
		public static function get lineEnding() : String{
			return _lineEnding;
		}
		public static function get userDirectory() : File{
			return _userDirectory;
		}
		public static function get documentsDirectory() : File{
			return _documentsDirectory;
		}
		public static function get desktopDirectory() : File{
			return _desktopDirectory;
		}
		public static function get applicationStorageDirectory() : File{
			return _applicationStorageDirectory;
		}
		public static function get applicationDirectory() : File{
			return _applicationDirectory;
		}
		public static function get cacheDirectory() : File{
			return _cacheDirectory;
		}
		
		native public static function createTempFile() : File;
		native public static function createTempDirectory() : File;
		
		[ArrayElementType("flash.filesystem.File")]
		native public static function getRootDirectories() : Array;

		public static function get permissionStatus() : String{
			return "granted";
		}
		
		public function File(path:String = null){
			if ( !path ){
				_init();
			}else if ( path.indexOf("app:") == 0 ){
				_initURL(0, path);
			}else if ( path.indexOf("app-storage:") == 0 ){
				_initURL(1, path);
			}else if ( path.indexOf("file:") == 0 ){
				_initURL(2, path);
			}else{
				_initPath(path);
			}
		}
		
		native private function _init():void;
		native private function _initURL(scheme:int, path:String):void;
		native private function _initPath(path:String):void;
		
		native override public function toString() : String;
		native public function get exists() : Boolean;
		native public function get isHidden() : Boolean;
		native public function get isDirectory() : Boolean;
		native public function get isPackage() : Boolean;
		native public function get isSymbolicLink() : Boolean;
		native override public function cancel() : void;		
		native public function resolvePath(path:String) : File;
		native public function getRelativePath(ref:FileReference, useDotDot:Boolean = false) : String;
		native public function get parent() : File;
		native public function get nativePath() : String;		
		native public function set nativePath(value:String) : void;
		native public function canonicalize() : void;
		native public function get url() : String;
		native public function set url(value:String) : void;
		native public function browseForOpen(param1:String, param2:Array = null) : void;
		native public function browseForOpenMultiple(param1:String, param2:Array = null) : void;
		native public function browseForSave(param1:String) : void;
		native public function browseForDirectory(param1:String) : void;
		native public function deleteFile() : void;
		native public function deleteFileAsync() : void;
		native public function deleteDirectory(param1:Boolean = false) : void;
		native public function deleteDirectoryAsync(param1:Boolean = false) : void;
		native public function copyTo(param1:FileReference, param2:Boolean = false) : void;
		native public function copyToAsync(param1:FileReference, param2:Boolean = false) : void;
		native public function moveTo(param1:FileReference, param2:Boolean = false) : void;
		native public function moveToAsync(param1:FileReference, param2:Boolean = false) : void;
		native public function moveToTrash() : void;
		native public function moveToTrashAsync() : void;
		native public function createDirectory() : void;
		[ArrayElementType("flash.filesystem.File")]
		native public function getDirectoryListing() : Array;
		native public function getDirectoryListingAsync() : void;
		native public function clone() : File;
		native public function get icon() : Icon;
		native public function get spaceAvailable() : Number;
		native public function openWithDefaultApplication() : void;
		native public function get downloaded() : Boolean;
		native public function set downloaded(value:Boolean) : void;
		native public function get preventBackup() : Boolean;
		native public function set preventBackup(param1:Boolean) : void;
		native override public function requestPermission() : void;
	}
}
