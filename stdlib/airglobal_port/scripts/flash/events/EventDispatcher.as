package flash.events
{
	import flash.net.URLRequestHeader;
	import flash.display.DisplayObject;
	import flash.utils.Dictionary;

	[native(instance="EventDispatcherObject",methods="auto",cls="EventDispatcherClass",gc="exact")]
	[Event(name="deactivate",type="flash.events.Event")]
	[Event(name="activate",type="flash.events.Event")]
	public class EventDispatcher implements IEventDispatcher
	{
		private static const ActiveListeners:Vector.<Listener> = new Vector.<Listener>();
	
		private var _target:IEventDispatcher;
		private var _weak_listeners:HashMap = null;
		private var _strong_listeners:HashMap = null;
		
		public function EventDispatcher(target:IEventDispatcher = null)
		{
			_target = target ? target : this;
		}
		
		public function addEventListener(type:String, handler:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false) : void{
			if ( useCapture ){
				type = "@c" + type;
			}
			removeFromList(_weak_listeners, type, handler);
			removeFromList(_strong_listeners, type, handler);
			var _listeners:HashMap = useWeakReference ? _weak_listeners : _strong_listeners;
			if ( !_listeners ){
				if ( useWeakReference ){
					_listeners = _weak_listeners = new HashMap();
				}else{
					_listeners = _strong_listeners = new HashMap();
				}
			}
			if ( !(type in _listeners) ){
				_listeners[type] = new Dictionary(useWeakReference);
			}
			var dict:Dictionary = _listeners[type] as Dictionary;
			var listener:Listener = new Listener();
			listener.handler = handler;
			listener.capture = useCapture;
			listener.priority = priority;
			listener.target = _target ? _target : this;
			dict[handler] = listener;
		}
		
		public function removeEventListener(type:String, handler:Function, useCapture:Boolean = false) : void{
			if ( useCapture ){
				type = "@c" + type;
			}
			removeFromList(_weak_listeners, type, handler);
			removeFromList(_strong_listeners, type, handler);
		}
		
		private function removeFromList(listeners:HashMap, type:String, handler:Function):void{
			if ( !listeners ){
				return;
			}
			if ( !(type in listeners) ){
				return;
			}
			delete (listeners[type] as Dictionary)[handler];
		}
		
		public function dispatchEvent(event:Event) : Boolean
		{
			if(event.target)
			{
				return this.dispatchEventFunction(event.clone());
			}
			return this.dispatchEventFunction(event);
		}
	  
		public function toString():String{
			return "[object EventDispatcher]";
		}
		
		public function hasEventListener(type:String) : Boolean{
			return hasListenerInList(_weak_listeners, type) || hasListenerInList(_strong_listeners, type);
		}
		
		private function hasListenerInList(list:HashMap, type:String):Boolean{
			if ( !list ){
				return false;
			}
			if ( !(type in list) ){
				return false;
			}
			for ( var key:* in list[type] ){
				return true;
			}
			return false;
		}
		
		public function willTrigger(type:String) : Boolean{
			if ( hasEventListener(type) ){
				return true;
			}
			var displayObject:DisplayObject = this as DisplayObject;
			while ( displayObject ){
				if ( displayObject.hasEventListener(type) ){
					return true;
				}
				displayObject = displayObject.parent;
			}
			return false;
		}
		
		private static function Collect(displayObject:DisplayObject, type:String):void{
			if ( displayObject.parent ){
				Collect(displayObject.parent, type);
			}
			(displayObject as EventDispatcher).collectListeners(type);
		}
		
		private function dispatchEventFunction(evt:Event) : Boolean{
			var stackPos:int = ActiveListeners.length;
			var type:String = evt.type;
			try{
				if ( evt.bubbles && this is DisplayObject ){
					var displayObject:DisplayObject = this as DisplayObject;
					Collect(displayObject, '@c' + type);
					while ( displayObject ){
						collectListeners(type);
						displayObject = displayObject.parent;
					}
				}else{
					collectListeners(type);
				}
				evt.setTarget(_target);
				DoDispatch(stackPos, ActiveListeners.length, evt);
			}finally{
				ActiveListeners.length = stackPos;
			}
			return !evt.isDefaultPrevented();
		}
		
		private function DoDispatch(begin:int, end:int, evt:Event):void{
			var currentTarget:Object;
			for ( var i:int = begin; i < end; i++ ){
				if ( evt.isImmediatePropagationStopped() ){
					return;
				}
				var listener:Listener = ActiveListeners[i];
				if ( listener.target != currentTarget ){
					if ( evt.isPropagationStopped() ){
						return;
					}
					evt.visitTarget(listener.target);
					listener.handler(evt);
				}
			}
		}
		
		private function collectListenersInList(list:HashMap, type:String):void{
			if ( !list ){
				return;
			}
			if ( !(type in list) ){
				return;
			}
			var dict:Dictionary = list[type] as Dictionary;
			for each ( var listener:Listener in dict ){
				ActiveListeners[ActiveListeners.length] = listener;
			}
		}
		
		private function collectListeners(type:String):void{
			collectListenersInList(_weak_listeners, type);
			collectListenersInList(_strong_listeners, type);
		}
	}
}

import flash.events.IEventDispatcher;

class Listener{
	public var handler:Function;
	public var capture:Boolean;
	public var priority:int;
	public var target:IEventDispatcher;
}

