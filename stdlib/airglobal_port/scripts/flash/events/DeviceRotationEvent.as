package flash.events
{
   [API("723")]
   public class DeviceRotationEvent extends Event
   {
      
      public static const UPDATE:String = "update";
       
      
      private var m_roll:Number;
      
      private var m_pitch:Number;
      
      private var m_yaw:Number;
      
      private var m_timestamp:Number;
      
      private var m_quaternion:Array = null;
      
      public function DeviceRotationEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, timestamp:Number = 0, roll:Number = 0, pitch:Number = 0, yaw:Number = 0, quaternion:Array = null)
      {
         super(type,bubbles,cancelable);
         this.m_roll = roll;
         this.m_pitch = pitch;
         this.m_yaw = yaw;
         this.m_timestamp = timestamp;
         this.m_quaternion = quaternion;
      }
      
      override public function clone() : Event
      {
         return new DeviceRotationEvent(type,bubbles,cancelable,this.m_timestamp,this.m_roll,this.m_pitch,this.m_yaw,this.m_quaternion);
      }
      
      override public function toString() : String
      {
         return formatToString("DeviceRotationEvent","type","bubbles","cancelable","timestamp","roll","pitch","yaw","quaternion");
      }
      
      public function get timestamp() : Number
      {
         return this.m_timestamp;
      }
      
      public function set timestamp(value:Number) : void
      {
         this.m_timestamp = value;
      }
      
      public function get roll() : Number
      {
         return this.m_roll;
      }
      
      public function set roll(value:Number) : void
      {
         this.m_roll = value;
      }
      
      public function get pitch() : Number
      {
         return this.m_pitch;
      }
      
      public function set pitch(value:Number) : void
      {
         this.m_pitch = value;
      }
      
      public function get yaw() : Number
      {
         return this.m_yaw;
      }
      
      public function set yaw(value:Number) : void
      {
         this.m_yaw = value;
      }
      
      public function get quaternion() : Array
      {
         return this.m_quaternion;
      }
      
      public function set quaternion(value:Array) : void
      {
         this.m_quaternion = value;
      }
   }
}
