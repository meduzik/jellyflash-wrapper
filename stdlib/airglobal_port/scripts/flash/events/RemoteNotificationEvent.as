package flash.events
{
   [API("683")]
   public final class RemoteNotificationEvent extends Event
   {
      
      public static const NOTIFICATION:String = "notification";
      
      public static const TOKEN:String = "token";
       
      
      private var m_data:Object;
      
      private var m_tokenId:String;
      
      public function RemoteNotificationEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, data:Object = null, tokenId:String = null)
      {
         super(type,bubbles,cancelable);
         this.m_data = data;
         this.m_tokenId = tokenId;
      }
      
      public function get data() : Object
      {
         return this.m_data;
      }
      
      public function get tokenId() : String
      {
         return this.m_tokenId;
      }
   }
}
