package flash.events
{
   public final class PermissionEvent extends Event
   {
      
      public static const PERMISSION_STATUS:String = "permissionStatus";
       
      
      private var m_status:String;
      
      public function PermissionEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, status:String = "denied")
      {
         super(type,bubbles,cancelable);
         this.m_status = status;
      }
      
      override public function clone() : Event
      {
         return new PermissionEvent(type,bubbles,cancelable,this.m_status);
      }
      
      override public function toString() : String
      {
         return formatToString("PermissionEvent","type","bubbles","cancelable","permission","status");
      }
      
      public function get status() : String
      {
         return this.m_status;
      }
   }
}
