package flash.events
{
   [API("729")]
   public class VsyncStateChangeAvailabilityEvent extends Event
   {
      
      public static const VSYNC_STATE_CHANGE_AVAILABILITY:String = "vSyncStateChangeAvailability";
       
      
      private var m_available:Boolean;
      
      public function VsyncStateChangeAvailabilityEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, available:Boolean = false)
      {
         super(type,bubbles,cancelable);
         this.m_available = available;
      }
      
      override public function clone() : Event
      {
         return new VsyncStateChangeAvailabilityEvent(type,bubbles,cancelable,this.m_available);
      }
      
      override public function toString() : String
      {
         return formatToString("VsyncStateChangeAvailabilityEvent","type","bubbles","cancelable","available");
      }
      
      public function get available() : Boolean
      {
         return this.m_available;
      }
   }
}
