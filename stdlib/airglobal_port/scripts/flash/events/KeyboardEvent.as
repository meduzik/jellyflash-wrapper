package flash.events
{
	[native(instance="KeyboardEventObject",methods="auto",cls="KeyboardEventClass",gc="exact")]
	public class KeyboardEvent extends Event
	{
		
		public static const KEY_DOWN:String = "keyDown";
		
		public static const KEY_UP:String = "keyUp";
		 
		
		private var m_keyLocation:uint;		
		private var m_keyCode:uint;
		private var m_controlKey:Boolean;		
		private var m_charCode:uint;
		private var m_commandKey:Boolean;
		private var m_shiftKey:Boolean;
		private var m_altKey:Boolean;
		private var m_ctrlKey:Boolean;
		
		public function KeyboardEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false, charCodeValue:uint = 0, keyCodeValue:uint = 0, keyLocationValue:uint = 0, ctrlKeyValue:Boolean = false, altKeyValue:Boolean = false, shiftKeyValue:Boolean = false, controlKeyValue:Boolean = false, commandKeyValue:Boolean = false)
		{
			super(type,bubbles,cancelable);
			this.charCode = charCodeValue;
			this.keyCode = keyCodeValue;
			this.keyLocation = keyLocationValue;
			this.ctrlKey = ctrlKeyValue;
			this.altKey = altKeyValue;
			this.shiftKey = shiftKeyValue;
			this.controlKey = controlKeyValue;
			this.commandKey = commandKeyValue;
		}
		
		override public function clone() : Event
		{
			return new KeyboardEvent(type,bubbles,cancelable,this.charCode,this.keyCode,this.keyLocation,this.ctrlKey,this.altKey,this.shiftKey,this.controlKey,this.commandKey);
		}
		
		override public function toString() : String
		{
			return formatToString("KeyboardEvent","type","bubbles","cancelable","eventPhase","charCode","keyCode","keyLocation","ctrlKey","altKey","shiftKey","controlKey","commandKey");
		}
		
		public function get charCode() : uint{
			return this.m_charCode;
		}
		
		public function set charCode(param1:uint) : void{
			this.m_charCode = param1;
		}
		
		public function get keyCode() : uint
		{
			return this.m_keyCode;
		}
		
		public function set keyCode(value:uint) : void
		{
			this.m_keyCode = value;
		}
		
		public function get keyLocation() : uint
		{
			return this.m_keyLocation;
		}
		
		public function set keyLocation(value:uint) : void
		{
			this.m_keyLocation = value;
		}
		
		public function get ctrlKey() : Boolean{
			return this.m_ctrlKey;
		}
		
		public function set ctrlKey(param1:Boolean) : void{
			this.m_ctrlKey = param1;
		}
		
		public function get altKey() : Boolean{
			return this.m_altKey;
		}
		
		public function set altKey(param1:Boolean) : void{
			this.m_altKey = param1;
		}
		
		public function get shiftKey() : Boolean{
			return this.m_shiftKey;
		}
		
		public function set shiftKey(param1:Boolean) : void{
			this.m_shiftKey = param1;
		}
		
		public function get controlKey() : Boolean
		{
			return this.m_controlKey;
		}
		
		public function set controlKey(value:Boolean) : void
		{
			this.m_controlKey = value;
		}
		
		public function get commandKey() : Boolean{
			return this.m_commandKey;
		}
		
		public function set commandKey(param1:Boolean) : void{
			this.m_commandKey = param1;
		}
	}
}
