package flash.events
{
	[Version("10.1")]
	[Event(name="uncaughtError",type="flash.events.UncaughtErrorEvent")]
	public class UncaughtErrorEvents extends EventDispatcher
	{
		private static const Instance:UncaughtErrorEvents = new UncaughtErrorEvents();
		
		public function UncaughtErrorEvents()
		{
			super();
		}
	}
}
