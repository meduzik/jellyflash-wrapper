package flash.errors
{
   [API("719")]
   public class PermissionError extends Error
   {
       
      
      public function PermissionError(message:String, id:int)
      {
         super(message,id);
      }
      
      public function toString() : String
      {
         var str:String = "PermissionError: \'" + message + "\'";
         return str;
      }
   }
}
