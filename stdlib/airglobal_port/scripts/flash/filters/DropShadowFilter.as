package flash.filters
{
	[native(instance="DropShadowFilterObject",methods="auto",cls="DropShadowFilterClass")]
	public final class DropShadowFilter extends BitmapFilter
	{
		private var _distance:Number;
		private var _angle:Number;
		private var _color:uint;
		private var _alpha:Number;
		private var _blurX:Number;
		private var _blurY:Number;
		private var _quality:int;
		private var _strength:Number;
		private var _inner:Boolean;
		private var _knockout:Boolean;
		private var _hideObject:Boolean;
		
		public function DropShadowFilter(distance:Number = 4.0, angle:Number = 45, color:uint = 0, alpha:Number = 1.0, blurX:Number = 4.0, blurY:Number = 4.0, strength:Number = 1.0, quality:int = 1, inner:Boolean = false, knockout:Boolean = false, hideObject:Boolean = false)
		{
			super();
			this.distance = distance;
			this.angle = angle;
			this.color = color;
			this.alpha = alpha;
			this.blurX = blurX;
			this.blurY = blurY;
			this.quality = quality;
			this.strength = strength;
			this.inner = inner;
			this.knockout = knockout;
			this.hideObject = hideObject;
		}
		
		public function get distance() : Number{
			return _distance;
		}
		
		public function set distance(value:Number) : void{
			_distance = value;
		}
		
		public function get angle() : Number{
			return _angle;
		}
		
		public function set angle(value:Number) : void{
			_angle = value;
		}
		
		public function get color() : uint{
			return _color;
		}
		
		public function set color(value:uint) : void{
			_color = value;
		}
		
		public function get alpha() : Number{
			return _alpha;
		}
		
		public function set alpha(value:Number) : void{
			_alpha = value;
		}
		
		public function get blurX() : Number{
			return _blurX;
		}
		
		public function set blurX(value:Number) : void{
			_blurX = value;
		}
		
		public function get blurY() : Number{
			return _blurY;
		}
		
		public function set blurY(value:Number) : void{
			_blurY = value;
		}
		
		public function get hideObject() : Boolean{
			return _hideObject;
		}
		
		public function set hideObject(value:Boolean) : void{
			_hideObject = value;
		}
		
		public function get inner() : Boolean{
			return _inner;
		}
		
		public function set inner(value:Boolean) : void{
			_inner = value;
		}
		
		public function get knockout() : Boolean{
			return _knockout;
		}
		
		public function set knockout(value:Boolean) : void{
			_knockout = value;
		}
		
		public function get quality() : int{
			return _quality;
		}
		
		public function set quality(value:int) : void{
			_quality = value;
		}
		
		public function get strength() : Number{
			return _strength;
		}
		
		public function set strength(value:Number) : void{
			_strength = value;
		}
		
		override public function clone() : BitmapFilter
		{
			return new DropShadowFilter(
				this.distance,
				this.angle,
				this.color,
				this.alpha,
				this.blurX,
				this.blurY,
				this.strength,
				this.quality,
				this.inner,
				this.knockout,
				this.hideObject
			);
		}
	}
}
