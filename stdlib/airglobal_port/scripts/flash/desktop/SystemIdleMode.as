package flash.desktop
{
   [Version("air2.0")]
   public final class SystemIdleMode
   {
      
      public static const NORMAL:String = "normal";
      
      public static const KEEP_AWAKE:String = "keepAwake";
       
      
      public function SystemIdleMode()
      {
         super();
      }
   }
}
