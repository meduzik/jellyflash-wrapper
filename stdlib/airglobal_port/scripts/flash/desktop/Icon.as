package flash.desktop
{
   import flash.events.EventDispatcher;
   
   [API("661")]
   [native(instance="IconObject",methods="auto",cls="IconClass")]
   public class Icon extends EventDispatcher
   {
       
      
      private var _bitmaps:Array;
      
      public function Icon()
      {
         super();
      }
      
      public function get bitmaps() : Array
      {
         return this._bitmaps;
      }
      
      public function set bitmaps(ba:Array) : void
      {
         this._bitmaps = ba;
      }
   }
}
