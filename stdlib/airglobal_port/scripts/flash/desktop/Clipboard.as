package flash.desktop
{
	import flash.display.BitmapData;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.errors.IllegalOperationError;
	import flash.filesystem.File;
	
	public class Clipboard
	{
		private static const _instance:Clipboard = new Clipboard();
		
		public function Clipboard()
		{
			super();
		}
		
		public static function get generalClipboard() : Clipboard{
			return _instance;
		}
		
		native public function get formats() : Array;
		
		native public function clear() : void;
		
		native public function clearData(param1:String) : void;
		
		native public function setData(format:String, data:Object, serializable:Boolean = true) : Boolean;
		
		native public function getData(param1:String, param2:String = "originalPreferred") : Object;
		
		native public function hasFormat(format:String) : Boolean;
	}
}
