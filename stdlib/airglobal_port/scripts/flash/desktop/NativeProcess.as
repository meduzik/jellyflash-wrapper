package flash.desktop
{
   import flash.events.EventDispatcher;
   import flash.filesystem.File;
   import flash.utils.IDataInput;
   import flash.utils.IDataOutput;
   import flash.errors.IllegalOperationError;
   
   [native(instance="NativeProcessObject",methods="auto",cls="NativeProcessClass")]
   [Version("air2.0")]
   [Event(name="exit",type="flash.events.NativeProcessExitEvent")]
   [Event(name="standardInputIoError",type="flash.events.IOErrorEvent")]
   [Event(name="standardOutputIoError",type="flash.events.IOErrorEvent")]
   [Event(name="standardErrorIoError",type="flash.events.IOErrorEvent")]
   [Event(name="standardInputClose",type="flash.events.Event")]
   [Event(name="standardInputProgress",type="flash.events.ProgressEvent")]
   [Event(name="standardErrorClose",type="flash.events.Event")]
   [Event(name="standardErrorData",type="flash.events.ProgressEvent")]
   [Event(name="standardOutputClose",type="flash.events.Event")]
   [Event(name="standardOutputData",type="flash.events.ProgressEvent")]
   public class NativeProcess extends EventDispatcher
   {
       
      
      public function NativeProcess()
      {
         super();
      }
      
      public static function get isSupported() : Boolean
      {
         return true;
      }
      
      [API("732")]
      native static function isValidExecutable(param1:File) : Boolean;
      
      native public function closeInput() : void;
      
      native public function get running() : Boolean;
      
      native public function get standardError() : IDataInput;
      
      native public function get standardInput() : IDataOutput;
      
      native public function get standardOutput() : IDataInput;
      
      public function start(info:NativeProcessStartupInfo) : void
      {
         if(this.running)
         {
            Error.throwError(IllegalOperationError,3213);
         }
         if(info.executable == null || !isValidExecutable(info.executable))
         {
            Error.throwError(ArgumentError,3214);
         }
         var workingDirString:String = "";
         if(info.workingDirectory != null)
         {
            if(info.workingDirectory.exists == false || info.workingDirectory.isDirectory == false)
            {
               Error.throwError(ArgumentError,3215);
            }
            workingDirString = info.workingDirectory.nativePath;
         }
         this.internalStart(info.executable.nativePath,info.arguments,workingDirString);
      }
      
      private function escapeArgumentIfNeeded(orignalArgument:String) : String
      {
		/*
         var newArgument:String = orignalArgument;
         var QUOTE:String = "\"";
         var QUOTEPATTERN:RegExp = /\"/g;
         var SPACEPATTERN:RegExp = / /g;
         newArgument = newArgument.replace(QUOTEPATTERN,"\\\"");
         if(newArgument.search(SPACEPATTERN) != -1)
         {
            newArgument = QUOTE + newArgument + QUOTE;
         }
         return newArgument;
		 */
		 return "invalid";
      }
      
      native public function exit(param1:Boolean = false) : void;
      
      native private function internalStart(param1:String, param2:Vector.<String>, param3:String) : void;
   }
}

import flash.utils.ByteArray;
import flash.utils.IDataInput;
import flash.utils.IDataOutput;

[native(instance="InboundPipeObject",methods="auto",cls="InboundPipeClass")]
class InboundPipe implements IDataInput
{
    
   
   function InboundPipe()
   {
      super();
   }
   
   native public function readBytes(param1:ByteArray, param2:uint = 0, param3:uint = 0) : void;
   
   native public function readBoolean() : Boolean;
   
   native public function readByte() : int;
   
   native public function readUnsignedByte() : uint;
   
   native public function readShort() : int;
   
   native public function readUnsignedShort() : uint;
   
   native public function readInt() : int;
   
   native public function readUnsignedInt() : uint;
   
   native public function readFloat() : Number;
   
   native public function readDouble() : Number;
   
   native public function readMultiByte(param1:uint, param2:String) : String;
   
   native public function readUTF() : String;
   
   native public function readUTFBytes(param1:uint) : String;
   
   native public function get bytesAvailable() : uint;
   
   native public function readObject() : *;
   
   native public function get objectEncoding() : uint;
   
   native public function set objectEncoding(param1:uint) : void;
   
   native public function get endian() : String;
   
   native public function set endian(param1:String) : void;
}

import flash.utils.ByteArray;

[native(instance="OutboundPipeObject",methods="auto",cls="OutboundPipeClass")]
class OutboundPipe implements IDataOutput
{
    
   
   function OutboundPipe()
   {
      super();
   }
   
   native public function writeBytes(param1:ByteArray, param2:uint = 0, param3:uint = 0) : void;
   
   native public function writeBoolean(param1:Boolean) : void;
   
   native public function writeByte(param1:int) : void;
   
   native public function writeShort(param1:int) : void;
   
   native public function writeInt(param1:int) : void;
   
   native public function writeUnsignedInt(param1:uint) : void;
   
   native public function writeFloat(param1:Number) : void;
   
   native public function writeDouble(param1:Number) : void;
   
   native public function writeMultiByte(param1:String, param2:String) : void;
   
   native public function writeUTF(param1:String) : void;
   
   native public function writeUTFBytes(param1:String) : void;
   
   native public function writeObject(param1:*) : void;
   
   native public function get objectEncoding() : uint;
   
   native public function set objectEncoding(param1:uint) : void;
   
   native public function get endian() : String;
   
   native public function set endian(param1:String) : void;
}
