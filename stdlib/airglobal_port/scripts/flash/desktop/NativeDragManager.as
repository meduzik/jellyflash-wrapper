package flash.desktop
{
   import flash.display.BitmapData;
   import flash.display.InteractiveObject;
   import flash.geom.Point;
   
   [API("661")]
   [native(methods="auto",cls="DragManagerClass",construct="none")]
   public class NativeDragManager
   {
       
      
      public function NativeDragManager()
      {
         super();
      }
      
      native public static function get dropAction() : String;
      
      native public static function set dropAction(param1:String) : void;
      
      native public static function acceptDragDrop(param1:InteractiveObject) : void;
      
      native public static function get isDragging() : Boolean;
      
      native public static function get dragInitiator() : InteractiveObject;
      
      native public static function doDrag(param1:InteractiveObject, param2:Clipboard, param3:BitmapData = null, param4:Point = null, param5:NativeDragOptions = null) : void;
      
      [Version("air2.0")]
      native public static function get isSupported() : Boolean;
   }
}
