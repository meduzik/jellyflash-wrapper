package flash.desktop
{
	import flash.display.NativeWindow;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.system.Security;
	import flash.utils.Timer;
	
	[native(instance="ApplicationObject",methods="auto",cls="ApplicationClass",construct="native")]
	[Event(name="suspend",type="flash.events.Event")]
	[Event(name="keyUp",type="flash.events.KeyboardEvent")]
	[Event(name="keyDown",type="flash.events.KeyboardEvent")]
	[Event(name="userPresent",type="flash.events.Event")]
	[Event(name="userIdle",type="flash.events.Event")]
	[Event(name="networkChange",type="flash.events.Event")]
	[Event(name="exiting",type="flash.events.Event")]
	[Event(name="deactivate",type="flash.events.Event")]
	[Event(name="activate",type="flash.events.Event")]
	[Event(name="browserInvoke",type="flash.events.BrowserInvokeEvent")]
	[Event(name="invoke",type="flash.events.InvokeEvent")]
	public final class NativeApplication extends EventDispatcher
	{
		private static var _Instance:NativeApplication = new NativeApplication();
		
		public static function get nativeApplication():NativeApplication{
			return _Instance;
		}
		
		native public function get activeWindow():NativeWindow;
		native public function get openedWindows():Array;
		native public function get systemIdleMode():String;
		native public function set systemIdleMode(value:String):void;
		native public function exit():void;
	}
}
