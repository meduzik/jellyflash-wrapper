package flash.desktop
{
   import flash.filesystem.File;
   
   [native(instance="UpdaterObject",methods="auto",cls="UpdaterClass",construct="check")]
   public final class Updater
   {
       
      
      public function Updater()
      {
         super();
      }
      
      [Version("air2.0")]
      public static function get isSupported() : Boolean
      {
         return _checkSupported();
      }
      
      native private static function _checkSupported() : Boolean;
      
      public function update(airFile:File, version:String) : void
      {
         if(this._isADL)
         {
            throw new IllegalOperationError("This method is not supported if the application is launched from ADL.");
         }
         var result:Boolean = this._launch("-update \"" + this._thisApplication + "\" \"" + airFile.nativePath + "\" \"" + version + "\"");
         if(!result)
         {
            throw new IllegalOperationError("This method is not supported if Adobe AIR is not installed.");
         }
         NativeApplication.nativeApplication.exit();
      }
      
      native private function _launch(param1:String) : Boolean;
      
      native private function get _isADL() : Boolean;
      
      native private function get _thisApplication() : String;
   }
}
