package flash.security
{
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.utils.ByteArray;
   import flash.utils.IDataInput;
   
   [API("661")]
   [native(instance="XMLSignatureValidatorObject",methods="auto",cls="XMLSignatureValidatorClass",construct="check",gc="exact")]
   [Event(name="error",type="flash.events.ErrorEvent")]
   [Event(name="complete",type="flash.events.Event")]
   public class XMLSignatureValidator extends EventDispatcher
   {
      
      private static const XML_DSIG_NS:Namespace = new Namespace("http://www.w3.org/2000/09/xmldsig#");
      
      private static const XML_XADES_NAMESPACE:String = "http://uri.etsi.org/01903/v1.1.1#";
      
      private static const XMLDIGESTMETHOD_SHA256:String = "http://www.w3.org/2001/04/xmlenc#sha256";
      
      private static const XML_TRANSFORM_C14N:String = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
      
      private static const XML_TRANSFORM_ENVELOPED_SIGNATURE:String = "http://www.w3.org/2000/09/xmldsig#enveloped-signature";
      
      private static const MAX_NUM_TRANSFORMS:uint = 20;
      
      private static const kInvalidParamError:uint = 2004;
      
      private static const kNullPointerError:uint = 2007;
      
      private static const kInvalidCallError:uint = 2037;
      
      private static const VALID_IDENTITY:String = "validIdentity";
      
      private static const VALID_OR_UNKNOWN_IDENTITY:String = "validOrUnknownIdentity";
      
      private static const NEVER:String = "never";
       
      
      private var m_uriDereferencer:IURIDereferencer;
      
      private var m_isValidating:Boolean;
      
      private var m_referencesValidationSetting:String;
      
      private var m_cryptContext:CryptContext;
      
      private var m_referenceStatus:String;
      
      private var m_signedDataString:String;
      
      private var m_sigValueString:String;
      
      private var m_timestampUris:Array;
      
      private var _ignoreCertTime:Boolean = false;
      
      private var stubProperties:Object;
      
      public function XMLSignatureValidator()
      {
         this.m_timestampUris = ["SignatureValue","#PackageSignatureValue"];
         super();
         this.m_isValidating = false;
         this.m_referencesValidationSetting = VALID_IDENTITY;
         this.m_cryptContext = this.constructCryptContext();
         this.m_referenceStatus = SignatureStatus.UNKNOWN;
      }
      
      [Version("air2.0")]
      public static function get isSupported() : Boolean
      {
         return _checkSupported();
      }
      
      native private static function _checkSupported() : Boolean;
      
      public function get uriDereferencer() : IURIDereferencer
      {
         return this.m_uriDereferencer;
      }
      
      public function set uriDereferencer(uriDerefer:IURIDereferencer) : void
      {
         this.throwIfValidating();
         this.m_uriDereferencer = uriDerefer;
      }
      
      public function addCertificate(cert:ByteArray, trusted:Boolean) : *
      {
         this.throwIfValidating();
         this.m_cryptContext.addChainBuildingCertRaw(cert,trusted);
      }
      
      public function get useSystemTrustStore() : Boolean
      {
         return this.m_cryptContext.getUseSystemTrustStore();
      }
      
      public function set useSystemTrustStore(trusted:Boolean) : void
      {
         this.throwIfValidating();
         this.m_cryptContext.useSystemTrustStore(trusted);
      }
      
      public function get revocationCheckSetting() : String
      {
         return this.m_cryptContext.getRevCheckSetting();
      }
      
      public function set revocationCheckSetting(setting:String) : void
      {
         this.throwIfValidating();
         this.m_cryptContext.setRevCheckSetting(setting);
      }
      
      [API("663")]
      public function get referencesValidationSetting() : String
      {
         return this.m_referencesValidationSetting;
      }
      
      [API("663")]
      public function set referencesValidationSetting(setting:String) : void
      {
         this.throwIfValidating();
         if(!(setting == NEVER || setting == VALID_OR_UNKNOWN_IDENTITY || setting == VALID_IDENTITY))
         {
            Error.throwError(ArgumentError,kInvalidParamError);
         }
         this.m_referencesValidationSetting = setting;
         if(setting == VALID_OR_UNKNOWN_IDENTITY)
         {
            this.m_cryptContext.useCodeSigningValidationRules();
         }
      }
      
      private function cryptContextCodeToStatus(cryptContextCode:uint) : String
      {
         switch(cryptContextCode)
         {
            case XMLSignatureValidator.STATUS_VALID:
               return SignatureStatus.VALID;
            case XMLSignatureValidator.STATUS_INVALID:
               return SignatureStatus.INVALID;
            case XMLSignatureValidator.STATUS_UNKNOWN:
            case XMLSignatureValidator.STATUS_TROUBLE:
            default:
               return SignatureStatus.UNKNOWN;
         }
      }
      
      private function throwIfValidating() : *
      {
         if(this.m_isValidating)
         {
            Error.throwError(IllegalOperationError,kInvalidCallError);
         }
      }
      
      public function get referencesStatus() : String
      {
         this.throwIfValidating();
         return this.m_referenceStatus;
      }
      
      public function get digestStatus() : String
      {
         this.throwIfValidating();
         return this.cryptContextCodeToStatus(this.m_cryptContext.getDataTBVStatus());
      }
      
      public function get identityStatus() : String
      {
         this.throwIfValidating();
         return this.cryptContextCodeToStatus(this.m_cryptContext.getIDStatus());
      }
      
      public function get validityStatus() : String
      {
         this.throwIfValidating();
         if(this.referencesStatus == SignatureStatus.VALID)
         {
            return this.cryptContextCodeToStatus(this.m_cryptContext.getOverallStatus());
         }
         if(this.referencesStatus == SignatureStatus.INVALID)
         {
            return SignatureStatus.INVALID;
         }
         return this.m_cryptContext.getOverallStatus() == XMLSignatureValidator.STATUS_INVALID?SignatureStatus.INVALID:SignatureStatus.UNKNOWN;
      }
      
      public function get signerTrustSettings() : Array
      {
         this.throwIfValidating();
         return this.m_cryptContext.getSignerTrustSettings();
      }
      
      public function get signerExtendedKeyUsages() : Array
      {
         this.throwIfValidating();
         return this.m_cryptContext.getSignerExtendedKeyUsages();
      }
      
      private function applyTransforms(param1:IDataInput, param2:XMLList, param3:XML) : IDataInput
      {
         var _loc5_:XMLCanonicalizer = null;
         var _loc6_:XML = null;
         var _loc7_:String = null;
         var _loc8_:XML = null;
         var _loc9_:ByteArray = null;
         var _loc10_:XML = null;
         var _loc11_:XMLSignatureEnvelopedTransformer = null;
         var _loc12_:XML = null;
         if(param2 != null && param2.length() > MAX_NUM_TRANSFORMS)
         {
            throw new Error("too many transforms to process");
         }
         var _loc4_:IDataInput = param1;
         if(param2 != null && param2.length() > 0)
         {
            _loc5_ = this.constructXMLCanonicalizer();
            for each(_loc6_ in param2)
            {
               if(_loc6_.@Algorithm == XML_TRANSFORM_C14N)
               {
                  _loc7_ = _loc4_.readUTFBytes(_loc4_.bytesAvailable);
                  _loc8_ = new XML(_loc7_);
                  _loc9_ = new ByteArray();
                  _loc9_.writeUTFBytes(_loc5_.CanonicalizeXML(_loc8_));
                  _loc9_.position = 0;
                  _loc4_ = _loc9_;
                  continue;
               }
               if(_loc6_.@Algorithm == XML_TRANSFORM_ENVELOPED_SIGNATURE)
               {
                  _loc7_ = _loc4_.readUTFBytes(_loc4_.bytesAvailable);
                  _loc10_ = new XML(_loc7_);
                  _loc11_ = this.constructEnvelopedTransformer();
                  _loc12_ = _loc11_.transform(param3,_loc10_);
                  _loc9_ = new ByteArray();
                  _loc9_.writeUTFBytes(_loc5_.CanonicalizeXML(_loc12_));
                  _loc4_ = _loc9_;
                  continue;
               }
               throw new Error("unsupported XML transform: " + _loc6_.@Algorithm);
            }
         }
         return _loc4_;
      }
      
      private function verifyReferences(param1:XML) : Boolean
      {
         var _loc4_:XML = null;
         var _loc7_:String = null;
         var _loc8_:AVMPlusDigest = null;
         var _loc9_:uint = 0;
         var _loc10_:XMLList = null;
         var _loc11_:XML = null;
         var _loc12_:IDataInput = null;
         var _loc13_:* = undefined;
         var _loc14_:XML = null;
         var _loc15_:ByteArray = null;
         var _loc16_:XML = null;
         var _loc17_:XMLSignatureEnvelopedTransformer = null;
         var _loc18_:XML = null;
         var _loc2_:XML = XML(param1.XML_DSIG_NS::SignedInfo);
         var _loc3_:XMLList = _loc2_.XML_DSIG_NS::Reference;
         this.m_referenceStatus = SignatureStatus.UNKNOWN;
         var _loc5_:Boolean = true;
         var _loc6_:XMLCanonicalizer = this.constructXMLCanonicalizer();
         for each(_loc4_ in _loc3_)
         {
            _loc7_ = _loc4_.XML_DSIG_NS::DigestValue.toString();
            _loc8_ = this.constructAVMPlusDigest();
            _loc9_ = 0;
            if(_loc4_.XML_DSIG_NS::DigestMethod.@Algorithm != XMLDIGESTMETHOD_SHA256)
            {
               throw new Error("unsupported XML Digest algorithm: " + _loc4_.XML_DSIG_NS::DigestMethod.@Algorithm);
            }
            _loc8_.Init(XMLSignatureValidator.DIGESTMETHOD_SHA256);
            _loc10_ = _loc4_.XML_DSIG_NS::Transforms.XML_DSIG_NS::Transform;
            _loc12_ = this.m_uriDereferencer.dereference(_loc4_.@URI);
            if(_loc12_ == null)
            {
               throw new Error("IURIDereferencer.dereference() returned null",kNullPointerError);
            }
            if(_loc10_.length() > MAX_NUM_TRANSFORMS)
            {
               throw new Error("too many transforms to process");
            }
            if(_loc10_ != null && _loc10_.length() > 0)
            {
               for each(_loc11_ in _loc10_)
               {
                  if(_loc11_.@Algorithm == XML_TRANSFORM_C14N)
                  {
                     _loc13_ = _loc12_.readUTFBytes(_loc12_.bytesAvailable);
                     _loc14_ = new XML(_loc13_);
                     _loc15_ = new ByteArray();
                     _loc15_.writeUTFBytes(_loc6_.CanonicalizeXML(_loc14_));
                     _loc12_ = _loc15_;
                     continue;
                  }
                  if(_loc11_.@Algorithm == XML_TRANSFORM_ENVELOPED_SIGNATURE)
                  {
                     _loc13_ = _loc12_.readUTFBytes(_loc12_.bytesAvailable);
                     _loc16_ = new XML(_loc13_);
                     _loc17_ = this.constructEnvelopedTransformer();
                     _loc18_ = _loc17_.transform(param1,_loc16_);
                     _loc15_ = new ByteArray();
                     _loc15_.writeUTFBytes(_loc6_.CanonicalizeXML(_loc18_));
                     _loc12_ = _loc15_;
                     continue;
                  }
                  throw new Error("unsupported XML transform: " + _loc11_.@Algorithm);
               }
            }
            _loc8_.Update(_loc12_);
            if(_loc8_.FinishDigest(_loc7_) != 0)
            {
               _loc5_ = false;
            }
         }
         return _loc5_;
      }
      
      public function verify(signature:XML) : void
      {
         try
         {
            this.verifyCommon(signature);
         }
         catch(e:Error)
         {
            m_isValidating = false;
            throw e;
         }
         this.m_cryptContext.addEventListener(Event.COMPLETE,function(event:Event):void
         {
            var error:Error = null;
            try
            {
               validateReferences(signature);
            }
            catch(e:Error)
            {
               error = e;
            }
            m_isValidating = false;
            if(error != null)
            {
               m_referenceStatus = SignatureStatus.INVALID;
               dispatchEvent(new ErrorEvent(ErrorEvent.ERROR,false,false,error.message,error.errorID));
            }
            else
            {
               dispatchEvent(new Event(Event.COMPLETE));
            }
         });
         this.m_cryptContext.addEventListener(ErrorEvent.ERROR,function(event:Event):void
         {
            m_isValidating = false;
            dispatchEvent(event.clone());
         });
         this.m_cryptContext.VerifySigASync(this.m_sigValueString,this.m_signedDataString,this._ignoreCertTime);
      }
      
      private function validateReferences(signature:XML) : void
      {
         var idIsOK:Boolean = false;
         var digestStatus:String = this.cryptContextCodeToStatus(this.m_cryptContext.getDataTBVStatus());
         var identityStatus:String = this.cryptContextCodeToStatus(this.m_cryptContext.getIDStatus());
         idIsOK = false;
         if(this.m_referencesValidationSetting != NEVER)
         {
            idIsOK = false;
            if(this.m_referencesValidationSetting == VALID_OR_UNKNOWN_IDENTITY)
            {
               idIsOK = identityStatus != SignatureStatus.INVALID;
            }
            else
            {
               idIsOK = identityStatus == SignatureStatus.VALID;
            }
            if(idIsOK)
            {
               try
               {
                  if(this.verifyReferences(signature))
                  {
                     this.m_referenceStatus = SignatureStatus.VALID;
                  }
                  else
                  {
                     this.m_referenceStatus = SignatureStatus.INVALID;
                  }
               }
               catch(e:Error)
               {
                  m_referenceStatus = SignatureStatus.INVALID;
                  throw e;
               }
            }
         }
      }
      
      private function verifyCommon(signature:XML) : void
      {
         this.throwIfValidating();
         this.m_isValidating = true;
         this.m_referenceStatus = SignatureStatus.UNKNOWN;
         var signedData:XML = XML(signature.XML_DSIG_NS::SignedInfo);
         var sigValue:XMLList = signature.XML_DSIG_NS::SignatureValue;
         if(signedData.XML_DSIG_NS::CanonicalizationMethod.@Algorithm != XML_TRANSFORM_C14N)
         {
            this.m_isValidating = false;
            throw new Error("Unsupported canonicalization method: " + signedData.XML_DSIG_NS::CanonicalizationMethod.@Algorithm);
         }
         this.readSigKeyInfo(signature);
         var c14ner:XMLCanonicalizer = this.constructXMLCanonicalizer();
         this.m_signedDataString = c14ner.CanonicalizeXML(signedData);
         this.m_sigValueString = sigValue.toString();
         this.verifyTimestamp(signature);
      }
      
      private function verifyTimestamp(signature:XML) : void
      {
         var xmlTimestamp:XML = null;
         var foundUri:Boolean = false;
         var tsUri:String = null;
         var okUris:Array = null;
         var selector:Function = null;
         var selected:Array = null;
         var timestampToken:XMLList = null;
         var timestampTokenBase64:String = null;
         var transforms:XMLList = null;
         var transformedSigValue:IDataInput = null;
         var transformedSigValueString:String = null;
         var xades:Namespace = new Namespace(XML_XADES_NAMESPACE);
         var xmlTimestamps:XMLList = signature.XML_DSIG_NS::Object.xades::QualifyingProperties.xades::UnsignedProperties.xades::UnsignedSignatureProperties.xades::SignatureTimeStamp.xades::HashDataInfo;
         if(xmlTimestamps == null)
         {
            return;
         }
         var sigValue:XMLList = signature.XML_DSIG_NS::SignatureValue;
         if(sigValue.length() == 0)
         {
            return;
         }
         var sigValueBytes:ByteArray = new ByteArray();
         sigValueBytes.writeUTFBytes(sigValue[0].toXMLString());
         sigValueBytes.position = 0;
         for each(xmlTimestamp in xmlTimestamps)
         {
            foundUri = false;
            tsUri = xmlTimestamp.@uri;
            okUris = this.m_timestampUris;
            selector = function(item:*, index:int, array:Array):*
            {
               var equiv:Boolean = item == tsUri;
               return equiv;
            };
            selected = okUris.filter(selector);
            foundUri = selected.length > 0;
            if(foundUri)
            {
               timestampToken = xmlTimestamp.xades::EncapsulatedTimeStamp;
               if(timestampToken != null && timestampToken.length() == 1)
               {
                  timestampTokenBase64 = timestampToken[0].toString();
                  transforms = xmlTimestamp.XML_DSIG_NS::Transforms.XML_DSIG_NS::Transform;
                  transformedSigValue = this.applyTransforms(sigValueBytes,transforms,signature);
                  transformedSigValueString = transformedSigValue.readUTFBytes(transformedSigValue.bytesAvailable);
                  this.m_cryptContext.verifyTimestamp(timestampTokenBase64,transformedSigValueString,this._ignoreCertTime);
                  if(this.m_cryptContext.verificationTime != 0)
                  {
                     break;
                  }
               }
            }
         }
      }
      
      private function readSigKeyInfo(signature:XML) : void
      {
         var certificate:XMLList = null;
         var crls:XMLList = null;
         var oneEmbeddedCert:XML = null;
         var oneEmbeddedCRL:XML = null;
         var signerDN:XMLList = null;
         var certString:String = null;
         try
         {
            certificate = signature.XML_DSIG_NS::KeyInfo.XML_DSIG_NS::X509Data.XML_DSIG_NS::X509Certificate;
            crls = signature.XML_DSIG_NS::KeyInfo.XML_DSIG_NS::X509Data.XML_DSIG_NS::X509CRL;
            for each(oneEmbeddedCert in certificate)
            {
               this.m_cryptContext.addChainBuildingCertBase64(oneEmbeddedCert.toString(),false);
            }
            for each(oneEmbeddedCRL in crls)
            {
               this.m_cryptContext.addCRLRevEvidenceBase64(oneEmbeddedCRL.toString());
            }
         }
         catch(e:Error)
         {
            throw new Error("Encountered an error while parsing KeyInfo.X509Data ");
         }
         try
         {
            signerDN = signature.XML_DSIG_NS::KeyInfo.XML_DSIG_NS::X509Data.XML_DSIG_NS::X509SubjectName;
            certString = certificate[0].toString();
            if(signerDN != null && signerDN.length() > 0)
            {
               this.m_cryptContext.setSignerCertDN(signerDN[0].toString());
            }
            else
            {
               this.m_cryptContext.setSignerCert(certString);
            }
         }
         catch(e:Error)
         {
            throw new Error("Encountered an error while parsing signer\'s certificate");
         }
      }
      
      [cppcall]
      private function _verifySync(signature:XML) : void
      {
         try
         {
            this.verifyCommon(signature);
            this.m_cryptContext.VerifySigSync(this.m_sigValueString,this.m_signedDataString,this._ignoreCertTime);
            this.validateReferences(signature);
         }
         catch(e:Error)
         {
            m_isValidating = false;
            throw e;
         }
         this.m_isValidating = false;
      }
      
      [cppcall]
      private function _setTimestampUris(uris:Array) : void
      {
         this.m_timestampUris = uris;
      }
      
      [cppcall]
      private function _setUseCodeSigningValidationRules() : void
      {
         this.m_cryptContext.useCodeSigningValidationRules();
         this.m_referencesValidationSetting = VALID_OR_UNKNOWN_IDENTITY;
      }
      
      [cppcall]
      private function _addTimestampingRoot(rootCert:ByteArray) : void
      {
         this.m_cryptContext.addTimestampingRootRaw(rootCert);
      }
      
      [cppcall]
      private function _setTimestampRevocationCheckSetting(setting:String) : void
      {
         this.throwIfValidating();
         this.m_cryptContext.setTimestampRevCheckSetting(setting);
      }
      
      private function getTimestampRevocationCheckSetting() : String
      {
         return this.m_cryptContext.getTimestampRevCheckSetting();
      }
      
      private function getCryptContext() : CryptContext
      {
         return this.m_cryptContext;
      }
      
      public function get signerCN() : String
      {
         return this.m_cryptContext.signerCN;
      }
      
      public function get signerDN() : String
      {
         return this.m_cryptContext.signerDN;
      }
      
      [cppcall]
      private function _getPublicKey(signature:XML) : ByteArray
      {
         var cert:XMLList = null;
         var result:ByteArray = null;
         try
         {
            cert = signature.XML_DSIG_NS::KeyInfo.XML_DSIG_NS::X509Data.XML_DSIG_NS::X509Certificate;
            result = this.m_cryptContext.getPublicKey(cert[0].toString());
         }
         catch(e:Error)
         {
            throw new Error("Encountered an error while parsing KeyInfo.X509Data ");
         }
         return result;
      }
      
      [cppcall]
      private function _getSignerValidEndTime() : uint
      {
         return this.m_cryptContext.signerValidEnd;
      }
      
      [cppcall]
      private function _getVerificationTime() : uint
      {
         return this.m_cryptContext.verificationTime;
      }
      
      [cppcall]
      private function _getSignerIDSummary(version:uint) : String
      {
         return this.m_cryptContext.getSignerIDSummary(version);
      }
      
      [cppcall]
      private function _guessSignerIDSummary(signature:XML, version:uint) : String
      {
         this.readSigKeyInfo(signature);
         return this.m_cryptContext.getIDSummaryFromSigChain(version);
      }
      
      native private function constructAVMPlusDigest() : AVMPlusDigest;
      
      native private function constructCryptContext() : CryptContext;
      
      native private function constructXMLCanonicalizer() : XMLCanonicalizer;
      
      native private function constructEnvelopedTransformer() : XMLSignatureEnvelopedTransformer;
      
      [cppcall]
      private function _setIgnoreCertTime(ignoreCertTime:Boolean) : void
      {
         this._ignoreCertTime = ignoreCertTime;
      }
      
      private function initStubProperties() : void
      {
         this.stubProperties = new Object();
      }
      
      private function setStubProperty(key:String, value:*) : *
      {
         this.stubProperties[key] = value;
      }
      
      private function getStubProperty(key:String) : *
      {
         return this.stubProperties[key];
      }
   }
}
