package flash.security
{
   public final class SignatureStatus
   {
      
      public static const VALID:String = "valid";
      
      public static const INVALID:String = "invalid";
      
      public static const UNKNOWN:String = "unknown";
       
      
      public function SignatureStatus()
      {
         super();
      }
   }
}
