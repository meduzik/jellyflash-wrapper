package flash.security
{
   import flash.utils.ByteArray;
   import flash.utils.IDataInput;
   
   [API("661")]
   [native(instance="AVMPlusDigestObject",methods="auto",cls="AVMPlusDigestClass",construct="native")]
   class AVMPlusDigest
   {
      
      static const DIGESTMETHOD_SHA256:uint = 1;
       
      
      function AVMPlusDigest()
      {
         super();
      }
      
      public function Init(algorithm:uint) : void
      {
         if(algorithm != DIGESTMETHOD_SHA256)
         {
            throw new Error("Unrecognized digest method");
         }
         this.initInternal(algorithm);
      }
      
      public function Update(data:IDataInput) : uint
      {
         this.clearErrorLog();
         var retval:uint = this.iUpdate(data as ByteArray);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
         return retval;
      }
      
      public function UpdateWithString(data:String) : uint
      {
         this.clearErrorLog();
         var retval:uint = this.iUpdateWithString(data);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
         return retval;
      }
      
      public function FinishDigest(inDigestToCompare:String) : uint
      {
         this.clearErrorLog();
         var retval:uint = this.iFinishDigest(inDigestToCompare);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
         return retval;
      }
      
      native private function initInternal(param1:uint) : void;
      
      native private function iUpdate(param1:ByteArray) : uint;
      
      native private function iUpdateWithString(param1:String) : uint;
      
      native private function iFinishDigest(param1:String) : uint;
      
      native private function getLastError() : String;
      
      native private function clearErrorLog() : void;
   }
}
