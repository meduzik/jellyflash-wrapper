package flash.security
{
   [API("663")]
   public final class ReferencesValidationSetting
   {
      
      public static const VALID_IDENTITY:String = "validIdentity";
      
      public static const VALID_OR_UNKNOWN_IDENTITY:String = "validOrUnknownIdentity";
      
      public static const NEVER:String = "never";
       
      
      public function ReferencesValidationSetting()
      {
         super();
      }
   }
}
