package flash.security
{
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.net.URLRequest;
   import flash.net.URLStream;
   import flash.utils.ByteArray;
   
   [API("661")]
   [native(instance="CryptContextObject",methods="auto",cls="CryptContextClass",construct="native")]
   class CryptContext extends EventDispatcher
   {
      
      static const STATUS_VALID:uint = 0;
      
      static const STATUS_INVALID:uint = 1;
      
      static const STATUS_UNKNOWN:uint = 2;
      
      static const STATUS_TROUBLE:uint = 3;
      
      static const REVCHECK_NEVER:uint = 0;
      
      static const REVCHECK_BEST_EFFORT:uint = 1;
      
      static const REVCHECK_REQUIRED_IF_AVAILABLE:uint = 2;
      
      static const REVCHECK_ALWAYSREQUIRED:uint = 3;
      
      static const TRUSTFLAG_SIGNING:uint = 1;
      
      static const TRUSTFLAG_PLAYLISTSIGNING:uint = 16;
      
      static const TRUSTFLAG_CODESIGNING:uint = 256;
       
      
      function CryptContext()
      {
         super();
      }
      
      native private static function setSynchronousFlag(param1:URLRequest) : void;
      
      public function VerifySigASync(sig:String, data:String, ignoreCertTime:Boolean) : void
      {
         this.clearErrorLog();
         this.iVerifySigASync(sig,data,ignoreCertTime);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
      }
      
      public function VerifySigSync(sig:String, data:String, ignoreCertTime:Boolean) : void
      {
         this.clearErrorLog();
         this.iVerifySigSync(sig,data,ignoreCertTime);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
      }
      
      public function setSignerCert(cert:String) : *
      {
         this.clearErrorLog();
         this.isetSignerCert(cert);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
      }
      
      public function setSignerCertDN(dn:String) : *
      {
         this.clearErrorLog();
         this.isetSignerCertDN(dn);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
      }
      
      public function addChainBuildingCertBase64(cert:String, trusted:Boolean) : void
      {
         this.clearErrorLog();
         this.iaddChainBuildingCertBase64(cert,trusted);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
      }
      
      public function addChainBuildingCertRaw(cert:ByteArray, trusted:Boolean) : void
      {
         this.clearErrorLog();
         this.iaddChainBuildingCertRaw(cert,trusted);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
      }
      
      public function addTimestampingRootRaw(cert:ByteArray) : void
      {
         this.clearErrorLog();
         this.iaddTimestampingRootRaw(cert);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
      }
      
      public function addCRLRevEvidenceBase64(crl:String) : void
      {
         this.clearErrorLog();
         this.iaddCRLRevEvidenceBase64(crl);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
      }
      
      public function addCRLRevEvidenceRaw(crl:ByteArray) : void
      {
         this.clearErrorLog();
         this.iaddCRLRevEvidenceRaw(crl);
         var errorStr:String = this.getLastError();
         if(errorStr != null)
         {
            this.clearErrorLog();
            throw new Error(errorStr);
         }
      }
      
      native public function verifyTimestamp(param1:String, param2:String, param3:Boolean) : void;
      
      native private function iVerifySigASync(param1:String, param2:String, param3:Boolean) : void;
      
      native private function iVerifySigSync(param1:String, param2:String, param3:Boolean) : void;
      
      native private function isetSignerCert(param1:String) : void;
      
      native private function isetSignerCertDN(param1:String) : void;
      
      native private function iaddChainBuildingCertBase64(param1:String, param2:Boolean) : void;
      
      native private function iaddChainBuildingCertRaw(param1:ByteArray, param2:Boolean) : void;
      
      native private function iaddCRLRevEvidenceBase64(param1:String) : void;
      
      native private function iaddCRLRevEvidenceRaw(param1:ByteArray) : void;
      
      native private function iaddTimestampingRootRaw(param1:ByteArray) : void;
      
      native public function HasValidVerifySession() : Boolean;
      
      native public function getDataTBVStatus() : uint;
      
      native public function getIDStatus() : uint;
      
      native public function getOverallStatus() : uint;
      
      native public function useSystemTrustStore(param1:Boolean) : void;
      
      native public function getUseSystemTrustStore() : Boolean;
      
      native public function setRevCheckSetting(param1:String) : void;
      
      native public function getRevCheckSetting() : String;
      
      native public function setTimestampRevCheckSetting(param1:String) : void;
      
      native public function getTimestampRevCheckSetting() : String;
      
      native public function useCodeSigningValidationRules() : void;
      
      native private function getLastError() : String;
      
      native private function clearErrorLog() : void;
      
      native public function getSignerTrustFlags() : uint;
      
      public function getSignerTrustSettings() : Array
      {
         var trustFlags:uint = 0;
         var trustSettings:Array = new Array();
         if(this.HasValidVerifySession() && this.getOverallStatus() == STATUS_VALID)
         {
            trustFlags = this.getSignerTrustFlags();
            if((trustFlags & TRUSTFLAG_SIGNING) == TRUSTFLAG_SIGNING)
            {
               trustSettings.push(SignerTrustSettings.SIGNING);
            }
            if((trustFlags & TRUSTFLAG_PLAYLISTSIGNING) == TRUSTFLAG_PLAYLISTSIGNING)
            {
               trustSettings.push(SignerTrustSettings.PLAYLIST_SIGNING);
            }
            if((trustFlags & TRUSTFLAG_CODESIGNING) == TRUSTFLAG_CODESIGNING)
            {
               trustSettings.push(SignerTrustSettings.CODE_SIGNING);
            }
         }
         return trustSettings;
      }
      
      public function getSignerExtendedKeyUsages() : Array
      {
         var ekus:Array = new Array();
         this.getSignerExtendedKeyUsagesInternal(ekus);
         return ekus;
      }
      
      native private function getSignerExtendedKeyUsagesInternal(param1:Array) : void;
      
      private function onAsyncDownloadComplete(event:Event) : void
      {
         var results:ByteArray = new ByteArray();
         var urlStream:URLStream = event.target as URLStream;
         urlStream.readBytes(results);
         this.returnDownloadDataToTransportLayer(results);
      }
      
      private function onAsyncDownloadError(event:Event) : void
      {
         this.returnDownloadDataToTransportLayer(null);
      }
      
      native private function returnDownloadDataToTransportLayer(param1:ByteArray) : void;
      
      [cppcall]
      private function sendDownloadRequestASync(url:String) : void
      {
         var urlRequest:URLRequest = new URLRequest(url);
         var urlStream:URLStream = new URLStream();
         urlStream.addEventListener(Event.COMPLETE,this.onAsyncDownloadComplete);
         urlStream.addEventListener(IOErrorEvent.IO_ERROR,this.onAsyncDownloadError);
         urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR,this.onAsyncDownloadError);
         urlStream.load(urlRequest);
      }
      
      [cppcall]
      private function postAndReceiveASync(url:String, header:String, data:ByteArray) : void
      {
         var urlRequest:URLRequest = new URLRequest(url);
         urlRequest.method = URLRequestMethod.POST;
         urlRequest.data = data;
         var rhArray:Array = new Array(new URLRequestHeader("Content-Type",header));
         urlRequest.requestHeaders = rhArray;
         var urlStream:URLStream = new URLStream();
         urlStream.addEventListener(Event.COMPLETE,this.onAsyncDownloadComplete);
         urlStream.addEventListener(IOErrorEvent.IO_ERROR,this.onAsyncDownloadError);
         urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR,this.onAsyncDownloadError);
         urlStream.load(urlRequest);
      }
      
      private function onSyncDownloadError(event:Event) : void
      {
      }
      
      [cppcall]
      private function sendDownloadRequestSync(url:String) : ByteArray
      {
         var urlRequest:URLRequest = new URLRequest(url);
         setSynchronousFlag(urlRequest);
         var urlStream:URLStream = new URLStream();
         urlStream.addEventListener(IOErrorEvent.IO_ERROR,this.onSyncDownloadError);
         urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR,this.onSyncDownloadError);
         urlStream.load(urlRequest);
         var results:ByteArray = new ByteArray();
         urlStream.readBytes(results);
         return results;
      }
      
      [cppcall]
      private function postAndReceiveSync(url:String, header:String, data:ByteArray) : ByteArray
      {
         var urlRequest:URLRequest = new URLRequest(url);
         setSynchronousFlag(urlRequest);
         urlRequest.method = URLRequestMethod.POST;
         urlRequest.data = data;
         var rhArray:Array = new Array(new URLRequestHeader("Content-Type",header));
         urlRequest.requestHeaders = rhArray;
         var urlStream:URLStream = new URLStream();
         urlStream.addEventListener(IOErrorEvent.IO_ERROR,this.onSyncDownloadError);
         urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR,this.onSyncDownloadError);
         urlStream.load(urlRequest);
         var results:ByteArray = new ByteArray();
         urlStream.readBytes(results);
         return results;
      }
      
      native public function get signerCN() : String;
      
      native public function get signerDN() : String;
      
      native public function getPublicKey(param1:String) : ByteArray;
      
      native public function get signerValidEnd() : uint;
      
      native public function get verificationTime() : uint;
      
      native public function getSignerIDSummary(param1:uint) : String;
      
      native public function getIDSummaryFromSigChain(param1:uint) : String;
   }
}
