package flash.html
{
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   
   [native(instance="HTMLScriptProxy::HTMLScriptFunctionObject",methods="auto",instancebase="HTMLScriptProxy::HTMLScriptFunctionObjectBase",cls="HTMLScriptFunctionClass",construct="native")]
   final dynamic class __HTMLScriptFunction extends Function
   {
      
      private static var s_UpdateDirtyDocumentsTimer:Timer = null;
       
      
      function __HTMLScriptFunction()
      {
         super();
         this.nativeInit(this.throwJSError,this.throwJSOperationFailed,this.throwJSObjectDead,this.handleNSSetProperty,this.operationComplete);
      }
      
      native private static function nativeUpdateDirtyDocuments() : void;
      
      private static function onUpdateDirtyDocumentsTimer(ev:TimerEvent) : void
      {
         s_UpdateDirtyDocumentsTimer.stop();
         nativeUpdateDirtyDocuments();
      }
      
      private function operationComplete() : void
      {
         if(s_UpdateDirtyDocumentsTimer == null)
         {
            s_UpdateDirtyDocumentsTimer = new Timer(1,0);
            s_UpdateDirtyDocumentsTimer.addEventListener(TimerEvent.TIMER,onUpdateDirtyDocumentsTimer);
         }
         s_UpdateDirtyDocumentsTimer.start();
      }
      
      native private function nativeInit(param1:Function, param2:Function, param3:Function, param4:Function, param5:Function) : void;
      
      private function throwJSError(jsErrorObj:*) : void
      {
         throw jsErrorObj;
      }
      
      private function throwJSOperationFailed() : void
      {
         throw new Error("HTMLScript operation failed.");
      }
      
      private function throwJSObjectDead() : void
      {
         throw new Error(Error.getErrorMessage(3210),3210);
      }
      
      private function handleNSSetProperty() : void
      {
      }
      
      native private function nativeEnumProperties(param1:Function) : void;
      
      native private function nativeGetJSProperty(param1:String) : *;
      
      native private function nativeApply(param1:__HTMLScriptFunction, param2:* = undefined, param3:* = undefined) : *;
      
      override public function get prototype() : *
      {
         return this.nativeGetJSProperty("prototype");
      }
      
      override public function set prototype(p:*) : *
      {
         throw new Error("You cannot set the prototype of a __HTMLScriptFunction from ActionScript");
      }
      
      override public function get length() : int
      {
         return int(this.nativeGetJSProperty("length"));
      }
      
      override AS3 function call(thisArg:* = undefined, ... args) : *
      {
         var thisRef:__HTMLScriptFunction = this;
         return thisRef.apply(thisArg,args);
      }
      
      override AS3 function apply(thisArg:* = undefined, argArray:* = undefined) : *
      {
         var jsApply:__HTMLScriptFunction = this.nativeGetJSProperty("apply");
         return this.nativeApply(jsApply,thisArg,argArray);
      }
   }
}
