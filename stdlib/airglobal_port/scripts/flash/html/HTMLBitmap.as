package flash.html
{
   import flash.display.BitmapData;
   
   [native(instance="HTMLBitmapObject",methods="IHTMLBitmapObject<HTMLBitmapObject>",cls="HTMLBitmapClass",construct="native")]
   class HTMLBitmap
   {
       
      
      function HTMLBitmap(bitmap:BitmapData)
      {
         super();
         this.nativeInit(bitmap);
      }
      
      native private function nativeInit(param1:BitmapData) : void;
   }
}
