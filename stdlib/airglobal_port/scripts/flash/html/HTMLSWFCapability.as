package flash.html
{
   [Version("air2.0")]
   public final class HTMLSWFCapability
   {
      
      public static const STATUS_OK:int = 0;
      
      public static const ERROR_INSTALLED_PLAYER_NOT_FOUND:int = 3221;
      
      public static const ERROR_INSTALLED_PLAYER_TOO_OLD:int = 3222;
       
      
      public function HTMLSWFCapability()
      {
         super();
      }
   }
}
