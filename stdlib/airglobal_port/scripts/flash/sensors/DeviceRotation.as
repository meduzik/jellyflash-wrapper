package flash.sensors
{
   import flash.events.EventDispatcher;
   
   [API("723")]
   [native(instance="DeviceRotationObject",methods="auto",cls="DeviceRotationClass")]
   [Event(name="status",type="flash.events.StatusEvent")]
   [Event(name="update",type="flash.events.DeviceRotationEvent")]
   public class DeviceRotation extends EventDispatcher
   {
       
      
      public function DeviceRotation()
      {
         super();
      }
      
      native public static function get isSupported() : Boolean;
      
      native public function setRequestedUpdateInterval(param1:Number) : void;
      
      native public function get muted() : Boolean;
   }
}
