package flash.sensors
{
   import flash.events.EventDispatcher;
   
   [API("668")]
   [native(instance="GeolocationObject",methods="auto",cls="GeolocationClass")]
   [Event(name="permissionStatus",type="flash.events.PermissionEvent")]
   [Event(name="status",type="flash.events.StatusEvent")]
   [Event(name="update",type="flash.events.GeolocationEvent")]
   public class Geolocation extends EventDispatcher
   {
       
      
      public function Geolocation()
      {
         super();
      }
      
      native public static function get isSupported() : Boolean;
      
      [API("719")]
      native public static function get permissionStatus() : String;
      
      native public function setRequestedUpdateInterval(param1:Number) : void;
      
      native public function get muted() : Boolean;
      
      [API("719")]
      native public function requestPermission() : void;
      
      [API("729")]
      native public function set locationAlwaysUsePermission(param1:Boolean) : void;
      
      [API("729")]
      native public function get locationAlwaysUsePermission() : Boolean;
   }
}
