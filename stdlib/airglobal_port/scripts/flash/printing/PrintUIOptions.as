package flash.printing
{
   [Version("air2.0")]
   public final class PrintUIOptions
   {
       
      
      public var minPage:uint = 0;
      
      public var maxPage:uint = 0;
      
      public var disablePageRange:Boolean = false;
      
      public function PrintUIOptions()
      {
         super();
      }
   }
}
