package flash.ui
{
   import flash.display.NativeMenu;
   import flash.display.NativeMenuItem;
   import flash.display.Stage;
   import flash.net.URLRequest;
   
   [native(instance="ContextMenuObject",methods="auto",cls="ContextMenuClass",construct="check",gc="exact")]
   [Event(name="menuSelect",type="flash.events.ContextMenuEvent")]
   public final class ContextMenu extends NativeMenu
   {
       
      
      public function ContextMenu()
      {
         super();
         this.builtInItems = new ContextMenuBuiltInItems();
         this.customItems = new Array();
      }
      
      [API("667")]
      public static function get isSupported() : Boolean
      {
         return _checkSupported();
      }
      
      native private static function _checkSupported() : Boolean;
      
      [cppcall]
      public function hideBuiltInItems() : void
      {
         var items:ContextMenuBuiltInItems = this.builtInItems;
         if(items != null)
         {
            items.save = false;
            items.zoom = false;
            items.quality = false;
            items.play = false;
            items.loop = false;
            items.rewind = false;
            items.forwardAndBack = false;
            items.print = false;
         }
      }
      
      native public function get builtInItems() : ContextMenuBuiltInItems;
      
      native public function set builtInItems(param1:ContextMenuBuiltInItems) : void;
      
      native public function get customItems() : Array;
      
      native public function set customItems(param1:Array) : void;
      
      [Version("10")]
      native public function get link() : URLRequest;
      
      [Version("10")]
      native public function set link(param1:URLRequest) : void;
      
      [Version("10")]
      native public function get clipboardMenu() : Boolean;
      
      [Version("10")]
      native public function set clipboardMenu(param1:Boolean) : void;
      
      [Version("10")]
      native public function get clipboardItems() : ContextMenuClipboardItems;
      
      [Version("10")]
      native public function set clipboardItems(param1:ContextMenuClipboardItems) : void;
      
      native private function cloneLinkAndClipboardProperties(param1:ContextMenu) : void;
      
      override public function clone() : NativeMenu
      {
         var i:uint = 0;
         var newMenu:ContextMenu = new ContextMenu();
         for(i = 0; i < this.items.length; i++)
         {
            newMenu.addItem(this.items[i].clone());
         }
         this.cloneLinkAndClipboardProperties(newMenu);
         return newMenu;
      }
      
      override public function get numItems() : int
      {
         return this.items.length;
      }
      
      override public function get items() : Array
      {
         return this.customItems;
      }
      
      override public function set items(itemArray:Array) : void
      {
         this.customItems = itemArray;
      }
      
      override public function addItemAt(item:NativeMenuItem, index:int) : NativeMenuItem
      {
         return null;
      }
      
      override public function containsItem(item:NativeMenuItem) : Boolean
      {
         if(item == null)
         {
            Error.throwError(ArgumentError,2007,"item");
         }
         return this.items.indexOf(item) != -1;
      }
      
      override public function getItemAt(index:int) : NativeMenuItem
      {
         if(index >= this.numItems || index < 0)
         {
            Error.throwError(RangeError,2006);
         }
         return this.items[index];
      }
      
      override public function removeItemAt(index:int) : NativeMenuItem
      {
		return null;
      }
      
      override public function getItemIndex(item:NativeMenuItem) : int
      {
         if(item == null)
         {
            Error.throwError(ArgumentError,2007,"item");
         }
         return this.items.indexOf(item);
      }
      
      override public function removeAllItems() : void
      {
         var i:int = 0;
         var nbItems:int = this.numItems;
         for(i = 0; i < nbItems; i++)
         {
            this.removeItemAt(0);
         }
      }
      
      override public function display(stage:Stage, stageX:Number, stageY:Number) : void
      {
         this.customItems = this.configureCustomItems(this.customItems,this.link != null,this.clipboardMenu,this.clipboardItems);
         super.display(stage,stageX,stageY);
      }
      
      [cppcall]
      private function configureCustomItems(param1:Array, param2:Boolean, param3:Boolean, param4:ContextMenuClipboardItems) : Array
      {
         var _loc5_:int = 0;
         var _loc7_:Object = null;
         var _loc8_:ContextMenuItem = null;
         var _loc9_:NativeMenuItem = null;
         if(param1 == null)
         {
            Error.throwError(ArgumentError,2005,"value");
         }
         var _loc6_:uint = super.numItems;
         for(_loc5_ = 0; _loc5_ < _loc6_; _loc5_++)
         {
            super.removeItemAt(0);
         }
         if(param1 != null)
         {
            for(_loc5_ = 0; _loc5_ < param1.length; _loc5_++)
            {
               _loc7_ = param1[_loc5_];
               if(_loc7_ is ContextMenuItem)
               {
                  _loc8_ = _loc7_ as ContextMenuItem;
                  if(_loc8_.visible)
                  {
                     if(_loc8_.separatorBefore)
                     {
                        _loc9_ = new NativeMenuItem("",true);
                        super.addItemAt(_loc9_,super.numItems);
                     }
                     super.addItemAt(_loc8_,super.numItems);
                  }
               }
               else
               {
                  super.addItemAt(NativeMenuItem(_loc7_),super.numItems);
               }
            }
         }
         if(param2)
         {
            if(param1 && param1.length > 0)
            {
               _loc8_.separatorBefore = true;
               _loc9_ = new NativeMenuItem("",true);
               super.addItemAt(_loc9_,super.numItems);
            }
            _loc8_ = ContextMenuItem.systemOpenLinkMenuItem();
            super.addItemAt(_loc8_,super.numItems);
            _loc8_ = ContextMenuItem.systemCopyLinkMenuItem();
            super.addItemAt(_loc8_,super.numItems);
         }
         if(param3)
         {
            _loc8_ = ContextMenuItem.systemCutMenuItem();
            _loc8_.enabled = param4 && param4.cut;
            if(param1 && param1.length > 0 || param2)
            {
               _loc8_.separatorBefore = true;
               _loc9_ = new NativeMenuItem("",true);
               super.addItemAt(_loc9_,super.numItems);
            }
            super.addItemAt(_loc8_,super.numItems);
            _loc8_ = ContextMenuItem.systemCopyMenuItem();
            _loc8_.enabled = param4 && param4.copy;
            super.addItemAt(_loc8_,super.numItems);
            _loc8_ = ContextMenuItem.systemPasteMenuItem();
            _loc8_.enabled = param4 && param4.paste;
            super.addItemAt(_loc8_,super.numItems);
            _loc8_ = ContextMenuItem.systemClearMenuItem();
            _loc8_.enabled = param4 && param4.clear;
            super.addItemAt(_loc8_,super.numItems);
            _loc8_ = ContextMenuItem.systemSelectAllMenuItem();
            _loc8_.enabled = param4 && param4.selectAll || param4 == null;
            super.addItemAt(_loc8_,super.numItems);
         }
         return param1;
      }
   }
}
