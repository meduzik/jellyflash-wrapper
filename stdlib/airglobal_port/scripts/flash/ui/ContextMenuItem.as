package flash.ui
{
   import flash.display.InteractiveObject;
   import flash.display.NativeMenu;
   import flash.display.NativeMenuItem;
   import flash.events.ContextMenuEvent;
   import flash.events.Event;
   
   [native(instance="ContextMenuItemObject",methods="auto",cls="ContextMenuItemClass",construct="check",gc="exact")]
   [Event(name="menuItemSelect",type="flash.events.ContextMenuEvent")]
   public final class ContextMenuItem extends NativeMenuItem
   {
       
      
      public function ContextMenuItem(caption:String, separatorBefore:Boolean = false, enabled:Boolean = true, visible:Boolean = true)
      {
         super();
         this.caption = caption;
         this.separatorBefore = separatorBefore;
         this.enabled = enabled;
         this.visible = visible;
      }
      
      native private static function _getMenuString(param1:String) : String;
      
      static function systemCutMenuItem() : ContextMenuItem
      {
         var menuItem:ContextMenuItem = new ContextMenuItem(_getMenuString("EDIT__CUT"));
         menuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,menuItem.cut,false,int.MAX_VALUE,true);
         return menuItem;
      }
      
      static function systemCopyMenuItem() : ContextMenuItem
      {
         var menuItem:ContextMenuItem = new ContextMenuItem(_getMenuString("EDIT__COPY"));
         menuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,menuItem.copy,false,int.MAX_VALUE,true);
         return menuItem;
      }
      
      static function systemPasteMenuItem() : ContextMenuItem
      {
         var menuItem:ContextMenuItem = new ContextMenuItem(_getMenuString("EDIT__PASTE"));
         menuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,menuItem.paste,false,int.MAX_VALUE,true);
         return menuItem;
      }
      
      native private static function _setUserRequestingPaste(param1:Boolean) : void;
      
      static function systemClearMenuItem() : ContextMenuItem
      {
         var menuLabel:String = _getMenuString("EDIT__DELETE");
         var menuItem:ContextMenuItem = new ContextMenuItem(Boolean(menuLabel)?menuLabel:"Delete");
         menuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,menuItem.clear,false,int.MAX_VALUE,true);
         return menuItem;
      }
      
      static function systemSelectAllMenuItem() : ContextMenuItem
      {
         var menuItem:ContextMenuItem = new ContextMenuItem(_getMenuString("EDIT__SELECT_ALL"));
         menuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,menuItem.selectAll,false,int.MAX_VALUE,true);
         return menuItem;
      }
      
      static function systemOpenLinkMenuItem() : ContextMenuItem
      {
         var menuItem:ContextMenuItem = new ContextMenuItem(_getMenuString("HTML_CONTEXTMENU__NAVIGATION__OPEN_LINK"));
         menuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,menuItem.openLink,false,int.MAX_VALUE,true);
         return menuItem;
      }
      
      static function systemCopyLinkMenuItem() : ContextMenuItem
      {
         var menuItem:ContextMenuItem = new ContextMenuItem(_getMenuString("HTML_CONTEXTMENU__EDIT__COPY_LINK"));
         menuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,menuItem.copyLink,false,int.MAX_VALUE,true);
         return menuItem;
      }
      
      native private static function _isTextEditor(param1:InteractiveObject) : Boolean;
      
      native public function get caption() : String;
      
      native public function set caption(param1:String) : void;
      
      native public function get separatorBefore() : Boolean;
      
      native public function set separatorBefore(param1:Boolean) : void;
      
      native public function get visible() : Boolean;
      
      native public function set visible(param1:Boolean) : void;
      
      private function cut(ev:ContextMenuEvent) : void
      {
         var menuItem:ContextMenuItem = ev.target as ContextMenuItem;
         var owner:InteractiveObject = ev.contextMenuOwner;
         if(_isTextEditor(owner))
         {
            this.nativeCut();
         }
         else
         {
            ev.contextMenuOwner.dispatchEvent(new Event(Event.CUT,false,true));
         }
      }
      
      private function copy(ev:ContextMenuEvent) : void
      {
         var menuItem:ContextMenuItem = ev.target as ContextMenuItem;
         var owner:InteractiveObject = ev.contextMenuOwner;
         if(_isTextEditor(owner))
         {
            this.nativeCopy();
         }
         else
         {
            ev.contextMenuOwner.dispatchEvent(new Event(Event.COPY,false,true));
         }
      }
      
      private function paste(ev:ContextMenuEvent) : void
      {
         var menuItem:ContextMenuItem = ev.target as ContextMenuItem;
         var owner:InteractiveObject = ev.contextMenuOwner;
         _setUserRequestingPaste(true);
         if(_isTextEditor(owner))
         {
            this.nativePaste();
         }
         else
         {
            ev.contextMenuOwner.dispatchEvent(new Event(Event.PASTE,false,true));
         }
         _setUserRequestingPaste(false);
      }
      
      private function clear(ev:ContextMenuEvent) : void
      {
         var menuItem:ContextMenuItem = ev.target as ContextMenuItem;
         var owner:InteractiveObject = ev.contextMenuOwner;
         if(_isTextEditor(owner))
         {
            this.nativeClear();
         }
         else
         {
            ev.contextMenuOwner.dispatchEvent(new Event(Event.CLEAR,false,true));
         }
      }
      
      private function selectAll(ev:ContextMenuEvent) : void
      {
         var menuItem:ContextMenuItem = ev.target as ContextMenuItem;
         var owner:InteractiveObject = ev.contextMenuOwner;
         if(_isTextEditor(owner))
         {
            this.nativeSelectAll();
         }
         else
         {
            ev.contextMenuOwner.dispatchEvent(new Event(Event.SELECT_ALL,false,true));
         }
      }
      
      native private function openLink(param1:ContextMenuEvent) : void;
      
      native private function copyLink(param1:ContextMenuEvent) : void;
      
      native private function nativeClear() : void;
      
      native private function nativeCopy() : void;
      
      native private function nativeCut() : void;
      
      native private function nativePaste() : void;
      
      native private function nativeSelectAll() : void;
      
      override public function clone() : NativeMenuItem
      {
         var newMenuItem:ContextMenuItem = new ContextMenuItem(label,isSeparator);
         newMenuItem.name = name;
         newMenuItem.enabled = enabled;
         newMenuItem.checked = checked;
         newMenuItem.keyEquivalent = keyEquivalent;
         newMenuItem.keyEquivalentModifiers = keyEquivalentModifiers;
         newMenuItem.mnemonicIndex = mnemonicIndex;
         newMenuItem.submenu = Boolean(submenu)?submenu.clone():null;
         newMenuItem.separatorBefore = this.separatorBefore;
         newMenuItem.visible = this.visible;
         return newMenuItem;
      }
   }
}
