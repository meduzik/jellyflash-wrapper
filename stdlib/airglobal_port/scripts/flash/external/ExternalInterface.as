package flash.external
{
	import flash.system.Capabilities;

	[native(methods="auto",cls="ExternalInterfaceClass",construct="none")]
	public final class ExternalInterface
	{
		public static var marshallExceptions:Boolean = true;
		
		private static const Callbacks:HashMap = new HashMap();
		
		public function ExternalInterface()
		{
			super();
		}
		
		native public static function get available() : Boolean;
		
		public static function addCallback(functionName:String, closure:Function) : void{
			Callbacks[functionName] = closure;
		}
		
		public static function call(functionName:String, ... arguments) : * {
			var request:String = JSON.stringify({
				func: functionName,
				args: (arguments as Array)
			});
			var response:Object = JSON.parse(_submit(request));
			if (response.res == 'ok'){
				return response.val;
			}else if (response.res == 'err'){
				throw new Error(response.exc);
			}else{
				throw new Error("bad ExternalInterface invocation result");
			}
		}
		
		private static function callback(requestString:String): String{
			try{
				var request:Object = JSON.parse(requestString);
				var funcName:String = request.func;
				var args:Array = request.args;
				var func:Function = Callbacks[funcName];
				if (!func){
					throw new Error("no such function");
				}
				var result:* = func.apply(null, args);
				return JSON.stringify({
					res: 'ok',
					val: result
				});
			}catch(err:*){
				return JSON.stringify({
					res: 'err',
					exc: String(err)
				});
			}
		}
		
		native private static function _submit(request:String):String;
		
		//native public static function get objectID() : String;
	}
}
