package flash.external
{
   import flash.events.EventDispatcher;
   import flash.filesystem.File;
   import flash.errors.IllegalOperationError;
   
   [API("669")]
   [native(instance="ExtensionContextObject",methods="auto",cls="ExtensionContextClass")]
   [Event(name="status",type="flash.events.StatusEvent")]
   public final class ExtensionContext extends EventDispatcher
   {
       
      
      public function ExtensionContext()
      {
         super();
      }
      
      public static function createExtensionContext(extensionID:String, contextType:String) : ExtensionContext
      {
         if(extensionID == null)
         {
            Error.throwError(ArgumentError,2004,"extensionID");
         }
         return _createExtensionContext(extensionID,contextType);
      }
      
      native private static function _createExtensionContext(param1:String, param2:String) : ExtensionContext;
      
      public static function getExtensionDirectory(extensionID:String) : File
      {
         return new File(_getExtensionDirectory(extensionID));
      }
      
      native private static function _getExtensionDirectory(param1:String) : String;
      
      public function call(functionName:String, ... args) : Object
      {
         return this._call(functionName,args);
      }
      
      native private function _call(param1:String, param2:Array) : Object;
      
      public function get actionScriptData() : Object
      {
         if(this._disposed())
         {
            Error.throwError(IllegalOperationError,3501);
         }
         return this.getActionScriptData();
      }
      
      public function set actionScriptData(object:Object) : void
      {
         if(this._disposed())
         {
            Error.throwError(IllegalOperationError,3501);
         }
         this.setActionScriptData(object);
      }
      
      native public function dispose() : void;
      
      native function _disposed() : Boolean;
      
      native function getActionScriptData() : Object;
      
      native function setActionScriptData(param1:Object) : void;
   }
}
