package
{
	use namespace AS3;
	
	[native(instance="DateObject",methods="auto",cls="DateClass",construct="override",gc="exact")]
	public dynamic class Date
	{
		private static const QueryDate:Date = new Date(0);
	
		private var _time:Number;
		private var _year:int;
		private var _month:int;
		private var _date:int;
		private var _day:int;
		private var _hour:int;
		private var _minute:int;
		private var _second:int;
		private var _millisecond:int;
		private var _valid:int = 0;
	
		public function Date(
			...args
		)
		{
			if ( args.length == 0 ){
				setTime(get_now());
			}else if ( args.length == 1 ){
				if ( args[0] is Number ){
					_setTime(args[0]);
				}else if ( args[0] is String ){
					_setTime(parse(args[0]));
				}else{
					throw new Error("invalid parameters to Date constructor");
				}
			}else{
				_setTime(UTC(args[0], args[1], args[2], args[3], args[4], args[5], args[6]) + getTimezoneOffset());
			}
		}
		
		private function validateTime():void{
			if ( !(_valid & 1) ){
				recompose();
				_valid = 3;
			}
		}
		
		private function validateFields():void{
			if ( !(_valid & 2) ){
				decompose(getTimezoneOffset() + _time);
				_valid = 3;
			}
		}
		
		private function recompose():void{
			_time = UTC(_year, _month, _date, _hour, _minute, _second, _millisecond) + getTimezoneOffset();
		}
		
		native private function decompose(time:Number):void;
		native private static function get_now():Number;
		native public static function parse(string:String) : Number;
		native public static function UTC(year:int, month:int, date:int = 1, hour:int = 0, minute:int = 0, second:int = 0, millisecond:int = 0) : Number;
		native private function _toString(param1:int) : String;
		
		AS3 function valueOf() : Number{
			validateTime();
			return _time;
		}
		
		private function _setTime(time:Number) : Number{
			_time = time;
			_valid = 1;
			return time;
		}
		
		AS3 function setTime(t:* = undefined) : Number
		{
			return this._setTime(t);
		}
		
		AS3 function toString() : String
		{
			return this._toString(0);
		}
		
		AS3 function toDateString() : String
		{
			return this._toString(1);
		}
		
		AS3 function toTimeString() : String
		{
			return this._toString(2);
		}
		
		AS3 function toLocaleString() : String
		{
			return this._toString(3);
		}
		
		AS3 function toLocaleDateString() : String
		{
			return this._toString(4);
		}
		
		AS3 function toLocaleTimeString() : String
		{
			return this._toString(5);
		}
		
		AS3 function toUTCString() : String
		{
			return this._toString(6);
		}
		
		AS3 function getFullYear() : Number{
			validateFields();
			return _year;
		}
		
		AS3 function getMonth() : Number{
			validateFields();
			return _month;
		}
		
		AS3 function getDate() : Number{
			validateFields();
			return _date;
		}
		
		AS3 function getDay() : Number{
			validateFields();
			return _day;
		}
		
		AS3 function getHours() : Number{
			validateFields();
			return _hour;
		}
		
		AS3 function getMinutes() : Number{
			validateFields();
			return _minute;
		}
		
		AS3 function getSeconds() : Number{
			validateFields();
			return _second;
		}
		
		AS3 function getMilliseconds() : Number{
			validateFields();
			return _millisecond;
		}
		
		native AS3 function getTimezoneOffset() : Number;
		
		AS3 function getTime() : Number{
			validateTime();
			return _time;
		}
		
		AS3 function setFullYear(year:* = undefined, month:* = undefined, date:* = undefined) : Number{
			validateFields();
			if ( year !== undefined ){
				_year = year;
			}
			if ( month !== undefined ){
				_month = month;
			}
			if ( date !== undefined ){
				_date = date;
			}
			_valid = 2;
			return getTime();
		}
		
		AS3 function setMonth(month:* = undefined, date:* = undefined) : Number{
			return setFullYear(undefined, month, date);
		}
		
		AS3 function setDate(date:* = undefined) : Number{
			return setFullYear(undefined, undefined, date);
		}
		
		AS3 function setHours(hour:* = undefined, min:* = undefined, sec:* = undefined, ms:* = undefined) : Number{
			validateFields();
			if ( hour !== undefined ){
				_hour = hour;
			}
			if ( min !== undefined ){
				_minute = min;
			}
			if ( sec !== undefined ){
				_second = sec;
			}
			if ( ms !== undefined ){
				_millisecond = ms;
			}
			_valid = 2;
			return getTime();
		}
		
		AS3 function setMinutes(min:* = undefined, sec:* = undefined, ms:* = undefined) : Number{
			return setHours(undefined, min, sec, ms);
		}
		
		AS3 function setSeconds(sec:* = undefined, ms:* = undefined) : Number{
			return setHours(undefined, undefined, sec, ms);
		}
		
		AS3 function setMilliseconds(ms:* = undefined) : Number{
			return setHours(undefined, undefined, undefined, ms);
		}
		
		AS3 function getUTCFullYear() : Number{
			QueryDate.decompose(getTime());
			return QueryDate._year;
		}
		
		AS3 function getUTCMonth() : Number{
			QueryDate.decompose(getTime());
			return QueryDate._month;
		}
		
		AS3 function getUTCDate() : Number{
			QueryDate.decompose(getTime());
			return QueryDate._date;
		}
		
		AS3 function getUTCDay() : Number{
			QueryDate.decompose(getTime());
			return QueryDate._day;
		}
		
		AS3 function getUTCHours() : Number{
			QueryDate.decompose(getTime());
			return QueryDate._hour;
		}
		
		AS3 function getUTCMinutes() : Number{
			QueryDate.decompose(getTime());
			return QueryDate._minute;
		}
		
		AS3 function getUTCSeconds() : Number{
			QueryDate.decompose(getTime());
			return QueryDate._second;
		}
		
		AS3 function getUTCMilliseconds() : Number{
			QueryDate.decompose(getTime());
			return QueryDate._millisecond;
		}
		
		AS3 function setUTCFullYear(year:* = undefined, month:* = undefined, date:* = undefined) : Number{
			QueryDate.decompose(getTime());
			QueryDate.setFullYear(year, month, date);
			QueryDate.recompose();
			setTime(QueryDate._time);
			return getTime();
		}

		AS3 function setUTCMonth(month:* = undefined, date:* = undefined) : Number{
			return setUTCFullYear(undefined, month, date);
		}
		
		AS3 function setUTCDate(date:* = undefined) : Number{
			return setUTCFullYear(undefined, month, date);
		}
		
		AS3 function setUTCHours(hour:* = undefined, min:* = undefined, sec:* = undefined, ms:* = undefined) : Number{
			QueryDate.decompose(getTime());
			QueryDate.setHours(hour, min, sec, ms);
			QueryDate.recompose();
			setTime(QueryDate._time);
			return getTime();
		}
		
		AS3 function setUTCMinutes(min:* = undefined, sec:* = undefined, ms:* = undefined) : Number{
			return setUTCHours(undefined, min, sec, ms);
		}
		
		AS3 function setUTCSeconds(sec:* = undefined, ms:* = undefined) : Number{
			return setUTCHours(undefined, undefined, sec, ms);
		}
		
		AS3 function setUTCMilliseconds(ms:* = undefined) : Number{
			return setUTCHours(undefined, undefined, undefined, ms);
		}
		
		public function get fullYear() : Number
		{
			return this.getFullYear();
		}
		
		public function set fullYear(value:Number) : void
		{
			this.setFullYear(value);
		}
		
		public function get month() : Number
		{
			return this.getMonth();
		}
		
		public function set month(value:Number) : void
		{
			this.setMonth(value);
		}
		
		public function get date() : Number
		{
			return this.getDate();
		}
		
		public function set date(value:Number) : void
		{
			this.setDate(value);
		}
		
		public function get hours() : Number
		{
			return this.getHours();
		}
		
		public function set hours(value:Number) : void
		{
			this.setHours(value);
		}
		
		public function get minutes() : Number
		{
			return this.getMinutes();
		}
		
		public function set minutes(value:Number) : void
		{
			this.setMinutes(value);
		}
		
		public function get seconds() : Number
		{
			return this.getSeconds();
		}
		
		public function set seconds(value:Number) : void
		{
			this.setSeconds(value);
		}
		
		public function get milliseconds() : Number
		{
			return this.getMilliseconds();
		}
		
		public function set milliseconds(value:Number) : void
		{
			this.setMilliseconds(value);
		}
		
		public function get fullYearUTC() : Number
		{
			return this.getUTCFullYear();
		}
		
		public function set fullYearUTC(value:Number) : void
		{
			this.setUTCFullYear(value);
		}
		
		public function get monthUTC() : Number
		{
			return this.getUTCMonth();
		}
		
		public function set monthUTC(value:Number) : void
		{
			this.setUTCMonth(value);
		}
		
		public function get dateUTC() : Number
		{
			return this.getUTCDate();
		}
		
		public function set dateUTC(value:Number) : void
		{
			this.setUTCDate(value);
		}
		
		public function get hoursUTC() : Number
		{
			return this.getUTCHours();
		}
		
		public function set hoursUTC(value:Number) : void
		{
			this.setUTCHours(value);
		}
		
		public function get minutesUTC() : Number
		{
			return this.getUTCMinutes();
		}
		
		public function set minutesUTC(value:Number) : void
		{
			this.setUTCMinutes(value);
		}
		
		public function get secondsUTC() : Number
		{
			return this.getUTCSeconds();
		}
		
		public function set secondsUTC(value:Number) : void
		{
			this.setUTCSeconds(value);
		}
		
		public function get millisecondsUTC() : Number
		{
			return this.getUTCMilliseconds();
		}
		
		public function set millisecondsUTC(value:Number) : void
		{
			this.setUTCMilliseconds(value);
		}
		
		public function get time() : Number
		{
			return this.getTime();
		}
		
		public function set time(value:Number) : void
		{
			this.setTime(value);
		}
		
		public function get timezoneOffset() : Number
		{
			return this.getTimezoneOffset();
		}
		
		public function get day() : Number
		{
			return this.getDay();
		}
		
		public function get dayUTC() : Number
		{
			return this.getUTCDay();
		}
	}
}
